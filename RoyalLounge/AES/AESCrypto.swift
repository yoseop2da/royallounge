//
//  AESCrypto.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import CryptoSwift

class AESCrypto: NSObject {
    static let shared = AESCrypto()
    let AES_IV = "!@#0@ROYALLOUNGE"
//    - KEY =“UZT3J4gcgdPI73SAP1a9XoYv2v1Xuuio”; // Must be a 33 bytes

    func aesEncrypt(_ message: String) -> String? {
        guard let aesKey = RLUserDefault.shared.aeskey else {
            if let key = MyKey.shared.getKey(), !key.isEmpty {
                RLUserDefault.shared.aeskey = key
                return aesEncrypt(message)
            }else{
                print("""
                    ========== Error ===========\n
                     1. MyKey.shared.generate()
                     2. add 'RoyalRoungeKey' file to project
                    ============================
                """)
                return nil
            }
        }
        return aesEncrypt(message, key: aesKey, iv: AES_IV)
    }
    
    func aesDecrypt(_ message: String) -> String? {
        guard let aesKey = RLUserDefault.shared.aeskey else {
            if let key = MyKey.shared.getKey(), !key.isEmpty {
                RLUserDefault.shared.aeskey = key
                return aesDecrypt(message)
            }else{
                print("""
                    ========== Error ===========\n
                     1. MyKey.shared.generate()
                     2. add 'RoyalRoungeKey' file to project
                    ============================
                """)
                return nil
            }
        }
        return aesDecrypt(message, key: aesKey, iv: AES_IV)
    }
}

extension String {
    /**
        let encryptedString = "암호화할문자".aesEncrypted
    */
    var aesEncrypted: String? {
        return AESCrypto.shared.aesEncrypt(self)
    }

    /**
        let decryptedString = "복호화할문자".aesDecrypted
    */
    var aesDecrypted: String? {
        return AESCrypto.shared.aesDecrypt(self)
    }
}

private extension AESCrypto {
    func aesEncrypt(_ message: String, key: String, iv: String) -> String? {
        do {
            let aes = try AES.init(key: Array(key.utf8), blockMode: CBC(iv: Array(iv.utf8)), padding: .pkcs7)
            let data = message.data(using: String.Encoding.utf8)
            let encrypted = try aes.encrypt(data!.bytes)
            let encData = Data(bytes: UnsafePointer<UInt8>(encrypted), count: Int(encrypted.count))
            let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            let str = String(base64String)
            return str
                .replacingOccurrences(of: "+", with: "-")
                .replacingOccurrences(of: "/", with: "_")
                .replacingOccurrences(of: "\r\n", with: "")
                .toURLSafeBase64()
        } catch let e as NSError {
            return e.localizedDescription
        } catch {
            return nil
        }
    }
    
    func aesDecrypt(_ message: String, key: String, iv: String) -> String? {
        do {
            let aes = try AES.init(key: Array(key.utf8), blockMode: CBC(iv: Array(iv.utf8)), padding: .pkcs7)
                let _message = message
                    .replacingOccurrences(of: "-", with: "+")
                    .replacingOccurrences(of: "_", with: "/")
            let dataDe = Data(base64Encoded: _message, options: NSData.Base64DecodingOptions(rawValue: 0))
            guard let decDataBytes = dataDe?.bytes, decDataBytes.count > 0 else { return "" }
            let decrypted = try aes.decrypt(decDataBytes)
            let decData = Data(bytes: UnsafePointer<UInt8>(decrypted), count: Int(decrypted.count))
            
            return NSString(data: decData, encoding: String.Encoding.utf8.rawValue) as String? ?? ""
        } catch let e as NSError {
            return e.localizedDescription
        } catch {
            return nil
        }
    }
    
}

fileprivate extension String {
    func toURLSafeBase64() -> String {
//        var base64 = self.replacingOccurrences(of: "-", with: "+").replacingOccurrences(of: "_", with: "/")
        var base64 = self
        if base64.count % 4 != 0 { base64.append(String(repeating: "=", count: 4 - base64.count % 4)) }
        return base64
    }
}
                                                                                              
