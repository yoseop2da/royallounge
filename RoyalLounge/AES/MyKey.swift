//
//  MyKey.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import CryptoSwift

class MyKey: NSObject {
    static let shared = MyKey()
    
    var AESkey = "_YOSEOPGD49QoSPJeUoW01089997677_"
    var AESiv = "MY_FIRST_AES@_IV"

    func generate() {
        let key = "UZT3J4gcgdPI73SAP1a9XoYv2v1Xuuio".myKeyEncrypted!
//        let key = "Dl3zX7toeTY9Xjj7NUcGD49QoSPJeUoW".myKeyEncrypted!
        print("""
            ------------------------------------------------------
            Generated_Encrypt_String
            : \(key)
            
            - 만들어진 Encrypt String를 복호화해서 사용할것
            - MyKey.shared.decrypte(encryptedKey: "Generated_Encrypt_String")
            - RoyalRoungeKey 파일을 불러와서 사용하기
            
            ------------------------------------------------------
        """)
        writeToFile(string: key)
    }
    
    func getKey() -> String? {
        let filePathUrl = Bundle.main.url(forResource: "RoyalRoungeKey", withExtension: nil, subdirectory: nil, localization: nil)!
        if let data = try? Data.init(contentsOf: filePathUrl), let encryptedKey = String.init(data: data, encoding: .utf8) {
            let str = decrypte(encryptedKey: encryptedKey)
            print("복호화 완료 : \(str) << 이 값을 저장 후 사용하기")
            return str
        }
        
        return nil
    }
}

fileprivate extension String {
    /**
        let encryptedString = "암호화할문자".myKeyEncrypted
    */
    var myKeyEncrypted: String? {
        return MyKey.shared.aesEncrypt(self)
    }

    /**
        let decryptedString = "복호화할문자".myKeyDecrypted
    */
    var myKeyDecrypted: String? {
        return MyKey.shared.aesDecrypt(self)
    }
}

fileprivate extension MyKey {
    
    func writeToFile(string: String) {
        let data = string.data(using: .utf8)!
        var path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        path.appendPathComponent("RoyalRoungeKey")
        print("-----------path: \(path)")
        try? data.write(to: path)
        
        print("""
            ------------------------------------------------------
            파일로 저장되었습니다.
            path: \(path)
            ------------------------------------------------------
        """)
    }

    func decrypte(encryptedKey: String) -> String {
        return encryptedKey.myKeyDecrypted!
    }
    
    func aesEncrypt(_ message: String, urlSafe: Bool = false) -> String? {
        do {
            let aes = try AES(key: Array(self.AESkey.utf8), blockMode: CBC(iv: Array(self.AESiv.utf8)), padding: .pkcs7)
            let data = message.data(using: String.Encoding.utf8)
            let encrypted = try aes.encrypt(data!.bytes)

            let encData = Data(bytes: UnsafePointer<UInt8>(encrypted), count: Int(encrypted.count))
            let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            if urlSafe {
                return base64String.toURLSafeBase64()
            } else {
                return String(base64String)
            }
        } catch let e as NSError {
            return e.localizedDescription
        } catch {
            return nil
        }
    }

    func aesDecrypt(_ message: String, urlSafe: Bool = false) -> String? {
        do {
            let aes = try AES.init(key: Array(self.AESkey.utf8), blockMode: CBC(iv: Array(self.AESiv.utf8)), padding: .pkcs7)
            let encodedMessage: String? = urlSafe ? message.removingPercentEncoding : message
            let dataDe = Data(base64Encoded: encodedMessage!, options: NSData.Base64DecodingOptions(rawValue: 0))
            guard let decDataBytes = dataDe?.bytes, decDataBytes.count > 0 else { return "" }
            let decrypted = try aes.decrypt(decDataBytes)
            let decData = Data(bytes: UnsafePointer<UInt8>(decrypted), count: Int(decrypted.count))
            return NSString(data: decData, encoding: String.Encoding.utf8.rawValue) as String? ?? ""
        } catch {
            return nil
        }
    }
}

fileprivate extension String {
    func toURLSafeBase64() -> String {
        var base64 = self.replacingOccurrences(of: "-", with: "+").replacingOccurrences(of: "_", with: "/")
        if base64.count % 4 != 0 { base64.append(String(repeating: "=", count: 4 - base64.count % 4)) }
        return base64
    }
}
