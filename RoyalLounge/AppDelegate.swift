//
//  AppDelegate.swift
//  RoyalRounge
//
//  Created by yoseop park on 2019/12/20.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var blockView: UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: UIScreen.height))
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        let noncontainsEmoji = "Party Hard".containsEmoji()
//        let containsEmoji = "😆 😅 😂 🤣✨Party✨ ✨Hard✨".containsEmoji()
//        let stringByRemovingEmoji = "😆 😅 😂 🤣 ✨Party✨ ✨Hard✨".removingEmoji()
//        print("noncontainsEmoji :\(noncontainsEmoji)")
//        print("containsEmoji :\(containsEmoji)")
//        print("stringByRemovingEmoji :\(stringByRemovingEmoji)")
        
//        let json: [String: AnyObject] = ["resultCd":"B000","resultMsg":"본인인증 완료","name":"박요섭","gender":"M","birthday":"19860106","localYn":"Y","telCo":"01","mbNo":"01089997677","ci":"ickEbutKH/fsADJv65R4QyP4iswUT4uk3BI+ISuB+lLJKcZ9c9vud1+dcZY48AW1IUPr5UAaxExnnUDvXY3ulg=="] as [String: AnyObject]
//        if let auth = AuthModel.init(json: json) {
//            let vc = RLStoryboard.join.accountViewController(
//                auth: auth,
//                eventNotiYn: true,
//                infoPeriod: "YEAR")
//
//            self.push(vc)
//        }
        
            
//        Coordinator.shared.toMain()
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let viewController = storyboard.instantiateViewController(withIdentifier: MainTabbarViewController.className)
//        self.window?.rootViewController = viewController
//        self.window?.makeKeyAndVisible()

        
        initFireBase(application, xlaunchOptions: launchOptions)
        
//        Appearance.configure()
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().backgroundImage = UIImage.init(color: .white)
        UITabBar.appearance().shadowImage = UIImage.init(color: .white)
        
        Repository.shared.main.updateUrl(url: "https://dev-api.royallounge.co.kr/")
//        MyKey.shared.generate()
        
//        RLUserDefault.shared.removeallKeys()
        
        Validation.shared.updateProhibitWord()

        blockView.backgroundColor = .clear
        self.window?.addSubview(blockView)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // 백그라운드 진입시 페북로그인 캐시 제거하기.
        //        FacebookSignInManager.shared.removeCachedToken()
//        disconnectFirebase()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
//        connectFirebase()
        application.applicationIconBadgeNumber = 0
        RLNotificationCenter.shared.post(notiType: .applicationBecomeActive)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) { }
    
    func applicationWillTerminate(_ application: UIApplication) { }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
//        return ApplicationDelegate.shared.application(app, open: url, options: options)
//    }
    
    func showBlockView() {
        self.window?.bringSubviewToFront(blockView)
        self.blockView.isHidden = false
    }
    
    func hideBlockView() {
        self.blockView.isHidden = true
    }
}
