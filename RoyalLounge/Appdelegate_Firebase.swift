//
//  Appdelegate_Firebase.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/06.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseCrashlytics

extension AppDelegate {

    func initFireBase(_ application: UIApplication, xlaunchOptions: [UIApplication.LaunchOptionsKey: Any]?) {

        FirebaseApp.configure()
        Crashlytics.crashlytics()

        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { granted, error in
                guard granted else {
                    // 유저가 승인을 안한경우
                    return
                }
                mainAsync {
                    application.registerForRemoteNotifications()
                }
            })
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
    }

    /*
     1. 앱이 백그라운드에 있는경우 : content-available 값이 1로 설정되면 호출됨 ex> 유저 상태 변경시
     2. 앱이 포어그라운드에 있는 경우 호출됨.
     */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let pushModel = PushModel.init(json: userInfo)
        self.handlePush(pushModel: pushModel)
        completionHandler(UIBackgroundFetchResult.newData)
//        completionHandler(UIBackgroundFetchResult.noData)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {

    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        #if DEBUG
            Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        #else
            Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        #endif
    }

//    func connectFirebase() {
//        Messaging.messaging().shouldEstablishDirectChannel = true
//    }

//    func disconnectFirebase() {
//        Messaging.messaging().shouldEstablishDirectChannel = false
//    }
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {

    /*
     info.plist의 FirebaseAppDelegateProxyEnabled 값이 YES일 경우 willPresent 에서 포어그라운드 푸시 내용을 처리해준다.
     */
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //        handlePush(notification.request.content.userInfo)
    //    }
    /*
     1. 앱이 포어그라운드 상태
     - 채팅은 커스텀 노티로
     - 채팅이외의 푸시는 시스템노티로
     - content-available 값이 1로 셋팅된 푸시가 올경우
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let pushModel = PushModel.init(json: notification.request.content.userInfo)

        // 알림센터 및 하단 배찌 업데이트
        MainInformation.shared.updateRedDot()
        
        if pushModel.displayCustomPopup {
            self.handlePush(pushModel: pushModel)
            return
        }

        // 시스템노티
        completionHandler([.alert, .badge, .sound])
    }
    
    /*
     1. 앱이 실행중일때 시스템노티 클릭시
     2. 앱이 백그라운드에서 진입시 시스템노티 클릭시
     3. 앱이 종료상태일때 시스템 노티 클릭시
     >>> 유저가 시스템 노티를 터치해서 진입한경우.
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let pushModel = PushModel.init(json: response.notification.request.content.userInfo)
        delay(0.3) {
            // 백그라운드에서 진입 후 바로 호출할경우 백그라운드쓰레드에서 호출하게 되어 종종 Fail 53이 발생하여 딜레이 추가.
            self.handlePush(pushModel: pushModel, fromUserAction: true)
        }
        completionHandler()
    }
}

// Firebase Message Delegate
extension AppDelegate: MessagingDelegate {
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        let pushModel = PushModel.init(json: remoteMessage.appData)
//        self.handlePush(pushModel: pushModel)
//    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        registerToken()
//        connectFirebase()
    }
}

extension AppDelegate {
    func registerToken() {
        InstanceID.instanceID().instanceID { (instanceIDResult, err) in
            if let refreshedToken = instanceIDResult?.token {
                RLUserDefault.shared.pushId = refreshedToken
                Debug.print("""

                    ┏[ FIREBASE TOKEN ]━━━━━━━━━━
                    ┃
                    ┃ \(refreshedToken)
                    ┃
                    ┗👾👾👾👾👾

                    """)
                if let userNoAes = MainInformation.shared.userNoAes,
                    let pushId = RLUserDefault.shared.pushId,
                    let _ = RLUserDefault.shared.token {
                    Repository.shared.graphQl
                        .updateDevice(userNo: userNoAes, pushId: pushId)
                }
                
                Messaging.messaging().subscribe(toTopic: "NON_MEMBER") { error in
                    print("Subscribed to NON_MEMBER topic")
                }
            }
        }
    }

    func handlePush(pushModel: PushModel, fromUserAction: Bool = false) {
        switch pushModel.pushType {
        case .joinApproval, .joinRejcet:
            // 상태 조회후 이동
            if fromUserAction {
                Coordinator.shared.moveByUserStatus()
            } else {
                RLPopup.shared.showNormal(description: pushModel.message, rightButtonTitle: "확인", rightAction: {
                    Coordinator.shared.moveByUserStatus()
                })
            }
        case .statusSuspend, .statusSuspendRelease, .statusBlock, .statusBlockOverAge, .statusOut, .adminLogout:
            RLNotificationCenter.shared.post(notiType: .logout)
        case .liveMatchComplete:
            //라이브매칭 탭으로 이동시켜줘야함
            Coordinator.shared.toThirdTab()
        default: ()
        }
//        case .memberJoinApproval, .memberReject, .memNormalReject, .memReport:
//            // 상태 조회후 이동
//            if fromUserAction {
//                mainInformation.moveByUserStatus()
//            } else {
//                CustomPopup.shared.show(title: MAIN_TITLE, body: pushModel.message, topButtonTitle: "확인", topButtonCb: {
//                    self.mainInformation.moveByUserStatus()
//                }, bottomButtonTitle: nil, bottomButtonCb: nil)
//            }
//        case .chatMessage, .chatMessageBackground:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            guard mainInformation.userInformation != nil else { return }
//            if fromUserAction {
//                MainCoordinator.shared.toMain()
//                if let roomNo = pushModel.roomNo {
//                    // 채팅룸으로 이동
//                    delay(0.5) { MainCoordinator.shared.toFourthTab(roomNo: roomNo) }
//                }
//            } else {
//                MainCoordinator.shared.refreshTabbarBadge()
//                if MainCoordinator.shared.selectedTabbarIdx == 3 {
//                    // 채팅방 업데이트 하기
//                    RTNotificationCenter.shared.post(notiType: .chatRoomUpdate, object: pushModel)
//                } else {
//                    if pushModel.pushType == .chatMessageBackground { return }
//                    NotiView.shared.showNotiView(attrStr: pushModel.attrStr, imageUrl: pushModel.mediaUrl, touchAction: {
//                        MainCoordinator.shared.toMain()
//                        if let roomNo = pushModel.roomNo {
//                            // 채팅룸으로 이동
//                            delay(0.5) { MainCoordinator.shared.toFourthTab(roomNo: roomNo) }
//                        }
//                    })
//                }
//            }
//        case .cardToday:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            MainCoordinator.shared.toFirstTab()
//        case .cardOppLike, .cardOppSupLike, .cardOppLikeView, .cardOppSupLikeView:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            // 카드상세로 이동
//            if fromUserAction {
//                MainCoordinator.shared.toMain(didFinishCallback: {
//                    if let cardUserNoAes = pushModel.oppUserNo, let cardMatchNoAes = pushModel.matchNo {
//                        MainCoordinator.shared.toCardDetail(parent: UIViewController.keyController(), viewType: .fromMain, cardUserNoAes: cardUserNoAes, cardMatchNoAes: cardMatchNoAes, cardDetailWillDisappear: { _ in })
//                    }
//                })
//            } else {
//                CustomPopup.shared.show(title: MAIN_TITLE, body: pushModel.message, topButtonTitle: "프로필 확인", topButtonCb: {
//                    MainCoordinator.shared.toMain(didFinishCallback: {
//                        if let cardUserNoAes = pushModel.oppUserNo, let cardMatchNoAes = pushModel.matchNo {
//                            MainCoordinator.shared.toCardDetail(parent: UIViewController.keyController(), viewType: .fromMain, cardUserNoAes: cardUserNoAes, cardMatchNoAes: cardMatchNoAes, cardDetailWillDisappear: { _ in })
//                        }
//                    })
//                }, bottomButtonCb: nil)
//            }
//        case .cardOppHigh, .evaluationOppHigh:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            if fromUserAction {
//                // 나를 좋아하는 카드 탭으로 이동 ( 높은 별점 받은 섹션 )
//                MainCoordinator.shared.toFirstTab(cardType: .oppHigh)
//            } else {
//                CustomPopup.shared.show(title: MAIN_TITLE, body: pushModel.message, topButtonTitle: "확인", topButtonCb: {
//                    // 나를 좋아하는 카드 탭으로 이동 ( 높은 별점 받은 섹션 )
//                    MainCoordinator.shared.toFirstTab(cardType: .oppHigh)
//                }, bottomButtonCb: nil)
//            }
//        case .matchSuccess, .chatStart:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            if fromUserAction {
//                MainCoordinator.shared.toMain(didFinishCallback: {
//                    // 채팅룸으로 이동
//                    MainCoordinator.shared.toFourthTab(roomNo: pushModel.roomNo)
//                })
//            } else {
//                CustomPopup.shared.show(title: MAIN_TITLE, body: pushModel.message, topButtonTitle: "매칭 확인", topButtonCb: {
//                    MainCoordinator.shared.toMain(didFinishCallback: {
//                        // 채팅룸으로 이동
//                        MainCoordinator.shared.toFourthTab(roomNo: pushModel.roomNo)
//                    })
//                }, bottomButtonCb: nil)
//            }
//        case .chatOppLeave:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            if fromUserAction {
//                // 채팅 리스트로 이동
//                MainCoordinator.shared.toFourthTab()
//            } else {
//                CustomPopup.shared.show(title: MAIN_TITLE, body: pushModel.message, topButtonTitle: "확인", topButtonCb: {
//                    MainCoordinator.shared.toMain(didFinishCallback: {
//                        // 채팅 리스트로 이동
//                        MainCoordinator.shared.toFourthTab()
//                    })
//                }, bottomButtonCb: nil)
//            }
//
//        case .evaluationEnd:
//            guard MainCoordinator.shared.isMainEntered else {
//                delay(1.0) { self.handlePush(pushModel: pushModel, fromUserAction: fromUserAction) }
//                return
//            }
//            if fromUserAction {
//                // 내 점수 확인 페이지로 이동
//                MainCoordinator.shared.toFifthTab(toMyScorePage: true)
//            } else {
//                CustomPopup.shared.show(title: MAIN_TITLE, body: pushModel.message, topButtonTitle: "매력지수 확인", topButtonCb: {
//                    MainCoordinator.shared.toMain(didFinishCallback: {
//                        MainCoordinator.shared.toFifthTab(toMyScorePage: true)
//                    })
//                }, bottomButtonCb: nil)
//            }
//        default: ()
//        }
    }
}
