//
//  Appearance.swift
//  RoyalRounge
//
//  Created by yoseop park on 2020/01/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class Appearance {
    
    static func configure() {
        let naviProxy = UINavigationBar.appearance()
        naviProxy.barStyle = .default
        naviProxy.tintColor = .clear//.creamwhiteBG
        naviProxy.titleTextAttributes = [
            .font: UIFont.spoqaHanSansRegular(ofSize: 20.0)
        ]
        
        
        naviProxy.setBackgroundImage(UIImage(), for: .default)
        naviProxy.shadowImage = UIImage()
        naviProxy.isTranslucent = true
        
        
        //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationBar.isTranslucent = true
        //        self.navigationController?.setNavigationBarHidden(false, animated: true)
        //        self.title = ""
    }
}

extension UIColor {
    
    static let rlOrangeBG238 = rgb(238.0, 229.0, 221.0, 1.0)
    static let rlOrangePressed = rgb(255.0, 100.0, 34.0, 1.0)
    static let rlOrange126 = rgb(126.0, 61.0, 9.0, 1.0)
    
    static let rlBrown238 = rgb(238.0, 229.0, 221.0, 1.0)
    static let rlBrown230 = rgb(230.0, 220.0, 211.0, 1.0)
    static let rlBrown223 = rgb(223.0, 205.0, 190.0, 1.0)
    static let rlBrown215 = rgb(215.0, 203.0, 194.0, 1.0)
    
    static let rlBlack21 = rgbAll(21.0, 1.0)
    
    static let rlLightGray140 = rgbAll(140.0, 1.0)
    static let rlLightGray155 = rgbAll(155.0, 1.0)
    static let rlLightGray199 = rgbAll(199.0, 1.0)
    static let rlWhtie251 = rgbAll(251.0, 1.0)
    
    class func rgbAll(_ all: CGFloat, _ alpha: CGFloat) -> UIColor {
        return UIColor(red: all/255.0, green: all/255.0, blue: all/255.0, alpha: alpha)
    }
    
    class func rgb(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ alpha: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alpha)
    }
}

extension UIColor {
    
    @nonobjc class var primary100: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 112.0 / 255.0, blue: 34.0 / 255.0, alpha: 1.0)
    }
    //
    //  @nonobjc class var primary75: UIColor {
    //    return UIColor(red: 246.0 / 255.0, green: 148.0 / 255.0, blue: 90.0 / 255.0, alpha: 1.0)
    //  }
    //
    @nonobjc class var primary25: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 219.0 / 255.0, blue: 200.0 / 255.0, alpha: 1.0)
    }
    //
    @nonobjc class var brown100: UIColor {
        return UIColor(red: 158.0 / 255.0, green: 82.0 / 255.0, blue: 21.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var brown75: UIColor {
        return UIColor(red: 181.0 / 255.0, green: 138.0 / 255.0, blue: 103.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var brown25: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 236.0 / 255.0, blue: 225.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var dark: UIColor {
        return UIColor(white: 33.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class func dark(alpha: CGFloat) -> UIColor { rgbAll(33.0, alpha) }
    
    
    @nonobjc class var gray75: UIColor {
        return UIColor(red: 67.0 / 255.0, green: 68.0 / 255.0, blue: 70.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var gray50: UIColor {
        return UIColor(white: 144.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var gray50Alpha40: UIColor {
        return UIColor.gray50.withAlphaComponent(0.4)
    }
    
    @nonobjc class var gray25: UIColor {
        return UIColor(white: 199.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var gray10: UIColor {
        return UIColor(white: 232.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var creamwhiteBG: UIColor {
        return UIColor(white: 247.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var errerColor: UIColor {
        return UIColor(red: 1.0, green: 22.0 / 255.0, blue: 22.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var successColor: UIColor {
        return UIColor(red: 0.0, green: 122.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var warningColor: UIColor {
        return UIColor(red: 1.0, green: 207.0 / 255.0, blue: 92.0 / 255.0, alpha: 1.0)
    }
    
}
