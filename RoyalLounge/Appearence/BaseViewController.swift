//
//  BaseViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/01/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

// Global
@_exported import RxSwift
@_exported import RxCocoa

import RxSwift
import RxCocoa


// base : 공통 기본
// LoggedBase : 공통 + 로그인
// clearNaviBase : 공통 + 클리어 네비
// clearNaviLoggedBase : 공통 + 로그인 + 클리어네비

class BaseViewController: UIViewController {
    
    let bag = DisposeBag()
    var isFirstTime = true
//    let errorMsg = PublishSubject<(errorCd: String, msg: String)>.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.creamwhiteBG
        detectScreenShot()
//        errorMsg.subscribe(onNext: { errorCd, msg in
//            alertDialogOneButton(message: msg)
//        })
    }
    
    func swipeBackAnyWhere() {
        if let targets = self.navigationController?.interactivePopGestureRecognizer?.value(forKey: "targets") as? NSMutableArray {
            let gestureRecognizer = UIPanGestureRecognizer()
            gestureRecognizer.setValue(targets, forKey: "targets")
            self.view.addGestureRecognizer(gestureRecognizer)
        }
    }
    
    func detectScreenShot() {
        NotificationCenter.default.addObserver(self, selector: #selector(detectedScreenShot), name: UIApplication.userDidTakeScreenshotNotification, object: nil)
    }
    
    @objc func detectedScreenShot() {
        print("------------catpure detected 캡처 캡쳐")
//        alertDialogOneButton(title: "무단 캡처를 주의해주세요!", message: "조금 전 캡처하신 이미지에는 타인의 사진, 개인정보 등이 포함되어 있을 수 있습니다. 타인의 동의 없이 무단으로 이를 캡처하여 저장, 공유하는 행위 등은 관련 법령에 근거하여 형사 처벌을 받을 수 있습니다. 무단 캡처에 대한 기록은 발생 즉시 모두 저장되며, 수사기관 요청시 관련 자료가 전달될 수 있으므로 각별히 주의하시기 바랍니다.", okTitle: "위 내용에 동의합니다.", okAction: { _ in })
    }
    
    func keyboardHeight() -> Observable<(height: CGFloat, duration: Double)> {
        return Observable
            .from([
                NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
//                NotificationCenter.default.rx.notification(UIResponder.keyboardWillChangeFrameNotification)
                    .map {
                        if let frame = $0.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
                            let duration = $0.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double {
//                            let curve = $0.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt
                            let height = frame.cgRectValue.height - UIScreen.safeAreaBottom
                            return (height: height, duration: duration)
                        }else{
                            return (height: 0.0, duration: 0.5)
                        }
                }
                ,NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
                    .map { _ -> (height: CGFloat, duration: Double) in
                        (height: 0.0, duration: 0.5)
                }
            ])
            .merge()
    }
    
    func keyboardWillShow() -> Observable<(height: CGFloat, duration: Double)> {
            return Observable
                .from([
                    NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
    //                NotificationCenter.default.rx.notification(UIResponder.keyboardWillChangeFrameNotification)
                        .map {
                            if let frame = $0.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
                                let duration = $0.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double {
    //                            let curve = $0.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt
                                let height = frame.cgRectValue.height - UIScreen.safeAreaBottom
                                return (height: height, duration: duration)
                            }else{
                                return (height: 0.0, duration: 0.5)
                            }
                    }
                ])
                .merge()
        }
    
    func keyboardWillHide() -> Observable<(height: CGFloat, duration: Double)> {
            return Observable
                .from([
                    NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
                        .map { _ -> (height: CGFloat, duration: Double) in
                            (height: 0.0, duration: 0.5)
                    }
                ])
                .merge()
        }
    
    func addTapToCloseKeyboard() {
        view.rx.tapGesture()
            .filter { $0.state == .ended }
            .subscribe(onNext: { _ in
                self.view.endEditing(true)
            })
            .disposed(by: bag)
    }
    
    func naviColorToBackgroundColor() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color: self.view.backgroundColor!), for: .default)
    }
}
