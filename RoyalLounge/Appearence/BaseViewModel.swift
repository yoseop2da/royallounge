//
//  BaseViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/01/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseViewModel: NSObject {
    lazy var bag = DisposeBag()
    let onRLError = PublishSubject<(msg: String?, error: Error?, resultCD: String?)>.init()
}
