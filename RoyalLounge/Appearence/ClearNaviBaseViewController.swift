//
//  ClearNaviBaseViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ClearNaviBaseViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = ""
    }
}
