//
//  LoggedBaseViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

// 로그인 후 페이지 전용

class LoggedBaseViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.rx.notification(RLNotiType.logout.notiName)
            .subscribe(onNext: { _ in
                //
                MainInformation.shared.logout()
                // facebook session out
//                FacebookSignInManager.shared.removeCachedToken()
            })
        .disposed(by: bag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}



