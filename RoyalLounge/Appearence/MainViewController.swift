
//
//  MainViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class MainViewController: ClearNaviLoggedBaseViewController {
    var profileBarbutton: RLBarButtonItem!
    var crownBarbutton: RLBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileBarbutton = RLBarButtonItem.init(barType: .profile(action: {
            let vc = RLStoryboard.myPage.myPageViewController()
            if MainInformation.shared.isSleep {
                self.push(vc)
            }else{
                self.tabBarController?.push(vc)
            }
        }))
        self.navigationItem.leftBarButtonItem = profileBarbutton
        
        crownBarbutton = RLBarButtonItem.init(barType: .crown(count: 0, action: {
            self.moveToStore()
        }))
        self.navigationItem.rightBarButtonItem = crownBarbutton
        
        // 프로필 변경시점에 사진 업데이트 해주기
        NotificationCenter.default.rx.notification(RLNotiType.profileChanged.notiName)
            .subscribe(onNext: { noti in
                if let urlString = noti.object as? String {
                    self.profileBarbutton.profileView?.setImage(urlString: urlString)
                }
            })
            .disposed(by: bag)
        
        MainInformation.shared.myCrown
            .subscribe(onNext: { crown in
                self.crownBarbutton.crownButton?.setCrownLabel(text: "\(crown)")
            }).disposed(by: bag)
        
        MainInformation.shared.redDot
            .subscribe(onNext: { redDot in
                self.profileBarbutton.profileView?.isBadgeHidden = redDot.isProfileAllFalse
            })
            .disposed(by: bag)
        
        
        refreshMyProfilePhoto()
        MainInformation.shared.updateMyCrownNew()
        MainInformation.shared.updateRedDot()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        self.tabBarController?.navigationController?.setNavigationBarHidden(true, animated: true)
        hideNavigationControllerBottomLine()
    }
    
    /// 프로필 뉴뱃지
    func profileBadge(isHidden: Bool) {
        
    }
    
    /// 스토어로 이동
    func moveToStore() {
        let vc = RLStoryboard.main.storeViewController()
        if MainInformation.shared.isSleep {
            self.push(vc)
        }else{
            self.tabBarController?.push(vc)
        }
    }
    
    /// 마이페이지로 이동
    func moveToMyPage() {
        let vc = RLStoryboard.myPage.myPageViewController()
        self.tabBarController?.push(vc)
    }
    
    /// 프로필 사진 갱신
    func refreshMyProfilePhoto() {
        if let userNo = MainInformation.shared.userNoAes {
            Repository.shared.graphQl.getMyPhotoUrl(userNo: userNo)
                .subscribe(onNext: {
                    if let photoUrl = $0 {
                        RLNotificationCenter.shared.post(notiType: .profileChanged, object: photoUrl)
                    }
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }
    }
}
