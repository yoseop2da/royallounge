//
//  Str.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

class Str: NSObject {
//    static let routedate = "routedate"
//    static let facebook = "facebook"
//    static let apple = "apple"

    static let normal = "NORMAL"
    static let reject = "REJECT"
    static let ready = "READY"
    static let editing = "EDITING"
    
    static let editButtonTitle = "수정하기"
    static let removeButtonTitle = "삭제하기"
    static let cancel = "취소"
    static let telecom = "통신사"

    static let logout = "로그아웃"
    static let leaveMembership = "계정 탈퇴"

    static let outChatting = "대화방 나가기"
    static let reportUser = "신고하기"

    static let serviceAgree = "본인확인서비스 이용 동의"
    static let servicePrivacyAgree = "개인정보 수집/이용/취급위탁 동의"
    static let serviceUniqueAgree = "고유식별정보처리 동의"
    static let serviceTermsAgree = "통신사 이용약관 동의"

//    static let highLabelMsg = "내가 높은 호감을 준 상대입니다."
//    static let superLikeLabelMsg = "내가 슈퍼좋아요를 보낸 상대입니다."
//    static let likeLabelMsg = "내가 좋아요를 보낸 상대입니다."
//    static let oppSuperLikeLabelMsg = "나에게 슈퍼좋아요를 보낸 상대입니다."
//    static let oppLikeLabelMsg = "나에게 좋아요를 보낸 상대입니다."
//    static let oppHighLabelMsg = "나에게 호감을 표시한 상대입니다."
//    static let bothHighLabelMsg = "서로를 높게 평가했어요, 좋아요를 보내보세요!"
}
