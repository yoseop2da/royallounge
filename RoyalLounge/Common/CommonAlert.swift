//
//  CommonAlert.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Toaster

let MAIN_TITLE = "로얄라운지"
let SERVER_ERROR_E0000_MESSAGE = "서버와의 통신중에 문제가 발생하였습니다. \n고객센터로 문의해주시기 바랍니다."
let SERVER_ERROR_NETWORK_MESSAGE = "네트워크 상태가 원활하지 않거나 서버에 일시적으로 문제가 발생하였습니다.\n잠시후 다시 시도해주세요."

let ALERT_CANCEL = "취소"
let ALERT_CLOSE = "닫기"
let ALERT_OK = "확인"
let ALERT_UPDATE = "업데이트"

/*
 ┍━━━━━━━━━━━━━━━━┑
 ┃      title     ┃
 ┃     message    ┃
 ┣━━━━━━━━┳━━━━━━━┫
 ┃ cancel ┃ close ┃ + close Action
 ┗━━━━━━━━┻━━━━━━━┛
 */
func alertDialogTwoButton(title: String = MAIN_TITLE, message: String, okTitle: String = ALERT_CLOSE, okAction: @escaping (UIAlertAction) -> Void, cancelTitle: String = ALERT_CANCEL, cancelAction: @escaping (UIAlertAction) -> Void = { _ in }) {
    showAlert(UIViewController.keyController(), title: title, message: message, buttonTitle: okTitle, okAction: okAction, cancelTitle: cancelTitle, cancelAction: cancelAction, showCancelButton: true)
}

func alertDialogTwoButton(_ parent: UIViewController, title: String = MAIN_TITLE, message: String, okTitle: String = ALERT_CLOSE, okAction: @escaping (UIAlertAction) -> Void, cancelTitle: String = ALERT_CANCEL, cancelAction: @escaping (UIAlertAction) -> Void = { _ in }) {
    showAlert(parent, title: title, message: message, buttonTitle: okTitle, okAction: okAction, cancelTitle: cancelTitle, cancelAction: cancelAction, showCancelButton: true)
}

/*
 ┍━━━━━━━━━━━━━━━━┑
 ┃      title     ┃
 ┃     message    ┃
 ┣━━━━━━━━━━━━━━━━┫
 ┃      close     ┃  + close Action
 ┗━━━━━━━━━━━━━━━━┛
 */
func alertDialogOneButton(title: String = MAIN_TITLE, message: String, okTitle: String = ALERT_CLOSE, okAction: @escaping (UIAlertAction) -> Void = { _ in }) {
    alertDialogOneButton(UIViewController.keyController(), title: title, message: message, okTitle: okTitle, okAction: okAction)
}

func alertDialogOneButton(_ parent: UIViewController, title: String = MAIN_TITLE, message: String, okTitle: String = ALERT_CLOSE, okAction: ((UIAlertAction) -> Void)?) {
    showAlert(parent, title: title, message: message, buttonTitle: okTitle, okAction: okAction, cancelAction: nil, showCancelButton: false)

}

/*
 ┍━━━━━━━━━━━━━━━━┑
 ┃      title     ┃
 ┃     message    ┃ >>> seconds 후에 사라짐.
 ┗━━━━━━━━━━━━━━━━┛
 */

let mainAppDelegate = UIApplication.shared.delegate as! AppDelegate
var mainToastAlert: UIAlertController?

func alertInstantDialog(_ parent: UIViewController, title: String, message: String, seconds: Double, actionAfter: (() -> Void)?) {
    mainToastAlert?.dismiss(animated: false, completion: nil)
    let alert = toastAlert(parent, title: title, message: message, seconds: seconds, actionAfter: actionAfter)
    mainToastAlert = alert
}

func alertInstantDialog(title: String, message: String, seconds: Double, actionAfter: (() -> Void)?) {
    mainToastAlert?.dismiss(animated: false, completion: nil)
    let alert = toastAlert(UIViewController.keyController(), title: title, message: message, seconds: seconds, actionAfter: actionAfter)
    mainToastAlert = alert
}

var _toast: Toast?

func toast(message: String, seconds: Double, isOnKeyboard: Bool = false, backgroundColor: UIColor? = nil, actionAfter: (() -> Void)? = nil) {
    _toast?.cancel()
    
    let toast = Toast(text: message, delay: 0.0, duration: seconds)
    _toast = toast
    
    toast.view.font = UIFont.spoqaHanSansRegular(ofSize: 15.0)
    toast.view.textInsets = UIEdgeInsets.init(top: 16, left: 30, bottom: 16, right: 30)
    if let color = backgroundColor {
        toast.view.backgroundColor = color
    }
    
    
    if isOnKeyboard {
        toast.view.bottomOffsetPortrait = 350
        toast.view.bottomOffsetLandscape = 200
    }else{
        toast.view.bottomOffsetPortrait = 100
        toast.view.bottomOffsetLandscape = 50
    }
    
    toast.view.cornerRadius = 6.0
    toast.show()
    
    if let cb = actionAfter { delay(seconds, closure: cb) }

}

func toastAlert(_ parent: UIViewController, title: String, message: String, seconds: Double, actionAfter: (() -> Void)?) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    parent.present(alertController, animated: true, completion: nil)

    delay(seconds, closure: {
        alertController.dismiss(animated: true, completion: actionAfter)
    })

    return alertController
}

func alertERR_E0000Dialog(closeAction: @escaping () -> Void = {}) {
    if #available(iOS 10.0, *) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
//    alertDialogOneButton(UIViewController.keyController(), message: SERVER_ERROR_E0000_MESSAGE, okTitle: ALERT_OK) { _ in
//        closeAction()
//    }
    toast(message: SERVER_ERROR_E0000_MESSAGE, seconds: 1.5) {
        closeAction()
    }
}

func alertNETWORK_ERR(msg: String = SERVER_ERROR_NETWORK_MESSAGE, closeAction: @escaping () -> Void = {}) {
    if #available(iOS 10.0, *) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }

    RLPopup.shared.showNormal(parent: UIViewController.keyController(), description: msg, isLeftButtonGray: true, leftButtonTitle: "고객센터로 문의", rightButtonTitle: "확인", leftAction: {
        RLMailSender.shared.sendEmail(.네트워크오류, finishCallback: { _ in
            closeAction()
        })
    }, rightAction: {
        closeAction()
    })
}


private func showAlert(_ parent: UIViewController, title: String, message: String, buttonTitle: String, okAction: ((UIAlertAction) -> Void)?, cancelTitle: String = "취소", cancelAction: ((UIAlertAction) -> Void)?, showCancelButton: Bool) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alertController.addAction( UIAlertAction(title: buttonTitle, style: .default, handler: okAction) )
    if showCancelButton { alertController.addAction( UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction)) }
    parent.present(alertController, animated: true, completion: nil)
}

private func showToastAlert(_ parent: UIViewController, title: String, message: String, seconds: Double, actionAfter: (() -> Void)?) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    parent.present(alertController, animated: true, completion: nil)

    delay(seconds, closure: {
        alertController.dismiss(animated: true, completion: actionAfter)
    })
}

func delay(_ delayTime: Double, closure: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: .now()+Double(Int64(delayTime * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func globalAsync(_ closure: @escaping () -> Void) {
    DispatchQueue.global().async(execute: closure)
}

func mainAsync(_ closure: @escaping () -> Void) {
    DispatchQueue.main.async(execute: closure)
}

func alert401Status(closeAction: @escaping () -> Void = {}) {
    if #available(iOS 10.0, *) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
    let str = """
                다음의 경우 재로그인이 필요합니다.
                - 비밀번호가 변경된 경우
                - 일정기간이 경과하여 세션이 만료된 경우
                - 계정이 정지된 경우

                불편이 있으실 경우
                고객센터로 문의해주시기 바랍니다.
              """
    alertDialogOneButton(UIViewController.keyController(), message: str, okTitle: ALERT_CLOSE) { _ in
        closeAction()
    }
}

func alert503Status(headerString: String, closeAction: @escaping () -> Void = {}) {
    if #available(iOS 10.0, *) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
    alertDialogOneButton(UIViewController.keyController(), title: "시스템 점검중입니다.", message: headerString, okTitle: "종료") { _ in
        closeAction()
    }
}

func alertTextfield(title: String = MAIN_TITLE, message: String,
                    textFieldPlaceHolder: String,
                    textFieldInitialText: String,
                    cancelButtonTitle: String,
                    cancelAction: (() -> Void)?,
                    okButtonTitle: String,
                    okAction: ((String) -> Void)?) {

    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.addTextField { textFieldName in textFieldName.placeholder = textFieldPlaceHolder }
    let textField = alertController.textFields?.first
    //
    let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .default) { _ in cancelAction?() }

    //
    let okAction = UIAlertAction(title: okButtonTitle, style: .default) { _ in
        if let txfield = textField, let txfieldText = txfield.text { okAction?(txfieldText) }
    }
    okAction.isEnabled = false
    textField?.text = textFieldInitialText
    _ = NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: .main) { notification in
        if let txfield = textField, let txfieldText = txfield.text {
            okAction.isEnabled = txfieldText.length > 0
        }
    }
    alertController.addAction(cancelAction)
    alertController.addAction(okAction)
    UIViewController.keyController().present(alertController, animated: true, completion: nil)
}

//------------ DEBUG

func debugAlertDialogOneButton(title: String = MAIN_TITLE, message: String, okTitle: String = ALERT_CLOSE, okAction: @escaping (UIAlertAction) -> Void = { _ in }) {
    alertDialogOneButton(UIViewController.keyController(), title: title, message: message, okTitle: okTitle, okAction: okAction)
}

func showGalleryOptionActionSheet(_ parent: UIViewController, editCallback: @escaping () -> Void, removeCallback: @escaping () -> Void) {
    let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet/*.alert*/)
    let editAction = UIAlertAction(title: Str.editButtonTitle, style: .default, handler: { _ in
        editCallback()
    })
    alert.addAction(editAction)

    let removeAction = UIAlertAction(title: Str.removeButtonTitle, style: .destructive, handler: { _ in
        removeCallback()
    })
    alert.addAction(removeAction)

    let action = UIAlertAction(title: Str.cancel, style: .cancel, handler: nil)
    alert.addAction(action)
    parent.present(alert, animated: true, completion: nil)
}

//func showLogoutActionSheet(_ parent: UIViewController, logoutCallback: @escaping () -> Void) {
//    let alert = UIAlertController.init(title: "로그아웃 하시겠습니까?", message: nil, preferredStyle: .actionSheet/*.alert*/)
//    let reportAction = UIAlertAction(title: Str.logout, style: .default, handler: { _ in
//        logoutCallback()
//    })
//    alert.addAction(reportAction)
//    let action = UIAlertAction(title: Str.cancel, style: .cancel, handler: nil)
//    alert.addAction(action)
//    parent.present(alert, animated: true, completion: nil)
//}

func showMyProfileActionSheet(_ parent: UIViewController, previewCallback: @escaping () -> Void, editCallback: @escaping () -> Void) {
    let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet/*.alert*/)
    let previewAction = UIAlertAction(title: "내 프로필 미리보기", style: .default, handler: { _ in
        previewCallback()
    })
    alert.addAction(previewAction)
    
    let editAction = UIAlertAction(title: "프로필 수정", style: .default, handler: { _ in
        editCallback()
    })
    alert.addAction(editAction)
    
    let action = UIAlertAction(title: Str.cancel, style: .cancel, handler: nil)
    alert.addAction(action)
    parent.present(alert, animated: true, completion: nil)
}

func showReportActionSheet(_ parent: UIViewController, reportCallback: @escaping () -> Void) {
    let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet/*.alert*/)
    let reportAction = UIAlertAction(title: Str.reportUser, style: .destructive, handler: { _ in
        reportCallback()
    })
    alert.addAction(reportAction)
    let action = UIAlertAction(title: Str.cancel, style: .cancel, handler: nil)
    alert.addAction(action)
    parent.present(alert, animated: true, completion: nil)
}

func showWebView(_ parent: UIViewController, title: String, url: String, scale: Bool = true, webViewClosedCallback: @escaping () -> Void = {  }) {
    let web = WKWebViewViewController(nibName: "WKWebViewViewController", bundle: nil)
    web.webTitle = title
    web.webUrl = url
    web.webViewClosedCallback = webViewClosedCallback
    web.webScaleValue = scale
//    let navigationController: UINavigationController = UINavigationController(rootViewController: web)
    parent.present(web, animated: true, completion: nil)
}

func showRemovePhoto(_ parent: UIViewController, callback: (() -> Void)?) {
    let alert = UIAlertController.init(title: "정말로, 삭제 하시겠습니까?", message: nil, preferredStyle: .actionSheet/*.alert*/)
    let reportAction = UIAlertAction(title: "삭제하기", style: .destructive, handler: { _ in
        callback?()
    })
    alert.addAction(reportAction)
    let action = UIAlertAction(title: Str.cancel, style: .cancel, handler: nil)
    alert.addAction(action)
    parent.present(alert, animated: true, completion: nil)
}
