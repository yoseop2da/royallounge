//
//  CrownButton.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CrownButton: UIButton {
    enum CrownButtonType {
        case sendOk
        case sendSuperOk
        case sendSuperOkOneMore
        case receiveOk
        case receiveSuperOk
        case remove
        case sleep
    }
    
    var crownButtonType: CrownButtonType = .sendOk
    var crown: Int = 0
    var crownMode: Bool = false
    
    var timer: Timer?
    
    func setButton(type: CrownButtonType) {
        crownButtonType = type
        crownMode = false
        setColor(type: type)
        
        switch type {
        case .sendOk: self.setTitle("OK 보내기", for: .normal)
        case .sendSuperOk: self.setTitle("슈퍼OK 보내기", for: .normal)
        case .sendSuperOkOneMore:
            let fullStr = "한번 더 슈퍼OK 보내기 (상대 무료 수락 가능)"
            let attrStr = NSMutableAttributedString.init(string: fullStr, attributes: [.font : UIFont.spoqaHanSansBold(ofSize: 16)])
            attrStr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 12), .foregroundColor: UIColor.white.withAlphaComponent(0.6)], range: (fullStr as NSString).range(of: "(상대 무료 수락 가능)"))
            self.setAttributedTitle(attrStr, for: .normal)
        case .receiveOk: self.setTitle("상대의 OK 수락하기", for: .normal)
        case .receiveSuperOk: self.setTitle("상대의 슈퍼OK 무료 수락하기", for: .normal)
        case .remove: self.setTitle("삭제하기", for: .normal)
        case .sleep: self.setTitle("휴면계정 설정하기", for: .normal)
            
        }
    }
    
    func setColor(type: CrownButtonType) {
        self.setBorder(color: .clear)
        switch type {
        case .sendOk, .receiveOk:
            self.layer.cornerRadius = 6.0
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
            self.setTitleColor(.brown100, for: .normal)
            self.setTitleColor(.brown100, for: .highlighted)
            self.backgroundColor = .brown25
        case .sendSuperOk, .sendSuperOkOneMore, .receiveSuperOk:
            self.layer.cornerRadius = 6.0
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
            self.setTitleColor(.white, for: .normal)
            self.setTitleColor(.white, for: .highlighted)
            self.backgroundColor = .brown100
        case .remove, .sleep:
            self.layer.cornerRadius = 6.0
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
            self.setTitleColor(.white, for: .normal)
            self.setTitleColor(.white, for: .highlighted)
            self.backgroundColor = .primary100
        }
    }
    
    func setCrownCountButton() {
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.75, target: self, selector: #selector(finishCrownButtonMode), userInfo: nil, repeats: false)
        crownMode = true
        self.layer.cornerRadius = 6.0
        
        switch crownButtonType {
        case .sendSuperOkOneMore:
            self.backgroundColor = .white
            let attrStr = NSAttributedString.init(string: "크라운 \(crown)", attributes: [.foregroundColor : UIColor.brown100])
            self.setAttributedTitle(attrStr, for: .normal)
            self.setBorder(color: .brown100)
        case .sendOk, .sendSuperOk, .receiveOk, .receiveSuperOk:
            if crown == 0 {
                self.setTitle("크라운 무료", for: .normal)
            }else{
                self.setTitle("크라운 \(crown)", for: .normal)
            }
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
            self.setTitleColor(.brown100, for: .normal)
            self.setTitleColor(.brown100, for: .highlighted)
            self.backgroundColor = .white
            self.setBorder(color: .brown100)
        case .remove, .sleep:
            if crown == 0 {
                self.setTitle("크라운 무료", for: .normal)
            }else{
                self.setTitle("크라운 \(crown)", for: .normal)
            }
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
            self.setTitleColor(.primary100, for: .normal)
            self.setTitleColor(.primary100, for: .highlighted)
            self.backgroundColor = .white
            self.setBorder(color: .primary100)
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc
    func finishCrownButtonMode() {
        stopTimer()
        setButton(type: self.crownButtonType)
    }
}

