//
//  LiveMatchButton.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Lottie
import SnapKit

class LiveMatchButton: UIView {
    
    private var normalView = UIView.init()
    private var normalLabel = UILabel.init()
    
    private var crownView = UIView.init()
    private var crownLabel = UILabel.init()
    
    private var findingView = UIView.init()
    private var findingCenterView = UIView.init()
    private var findingLabel = UILabel.init()
    private var findingLottiWrapView = UIView.init()
    private var lottiView: AnimationView?
    
    private var button: UIButton = UIButton.init()

    private var bag = DisposeBag()
    
    enum ButtonMode {
        case normal
        case crown
        case finding
    }
    
    var crown: Int = 0
    var buttonMode: ButtonMode = .normal
    var touchAction: (()->Void)?
    private var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    init() {
        super.init(frame: .zero)
        self.setBorder(color: .brown75)
        self.clipsToBounds = true
        self.layer.cornerRadius = 6.0
        
        // 기본
        normalView.isHidden = true
        self.addSubview(normalView)
        normalView.addSubview(normalLabel)
        normalView.snp.makeConstraints{ $0.top.bottom.leading.trailing.equalToSuperview() }
        normalLabel.snp.makeConstraints{ $0.top.bottom.leading.trailing.equalToSuperview() }
        
        // 크라운
        crownView.isHidden = true
        self.addSubview(crownView)
        crownView.addSubview(crownLabel)
        crownView.snp.makeConstraints{ $0.top.bottom.leading.trailing.equalToSuperview() }
        crownLabel.snp.makeConstraints{ $0.top.bottom.leading.trailing.equalToSuperview() }
        
        // 찾기
        findingView.isHidden = true
        self.addSubview(findingView)
        findingView.addSubview(findingCenterView)
        findingCenterView.addSubview(findingLabel)
        findingCenterView.addSubview(findingLottiWrapView)
        
        findingView.snp.makeConstraints{ $0.top.bottom.leading.trailing.equalToSuperview() }
        findingCenterView.snp.makeConstraints{ $0.center.equalToSuperview() }
        
        findingLabel.snp.makeConstraints{
            $0.top.bottom.leading.equalToSuperview()
            $0.height.equalTo(25)
        }
        
        findingLottiWrapView.snp.makeConstraints{
            $0.top.bottom.trailing.equalToSuperview()
            $0.width.height.equalTo(25)
            $0.leading.equalTo(findingLabel.snp.trailing).offset(5.0)
        }
        
        normalView.backgroundColor = .brown75
        normalLabel.textAlignment = .center
        normalLabel.textColor = .white
        normalLabel.font = UIFont.spoqaHanSansBold(ofSize: 16)
        
        crownView.backgroundColor = .white
        crownLabel.textAlignment = .center
        crownLabel.textColor = .brown100
        crownLabel.font = UIFont.spoqaHanSansBold(ofSize: 16)
        
        findingView.backgroundColor = .brown75
        findingLabel.textColor = .white
        findingLabel.font = UIFont.spoqaHanSansBold(ofSize: 16)
        
        // 버튼 추가
        self.addSubview(button)
        button.snp.makeConstraints{ $0.top.bottom.leading.trailing.equalToSuperview() }

        button.rx.tap
            .rxTouchAnimation(button: self, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.buttonMode == .normal {
                    self.setCrownCountButton()
                }else if self.buttonMode == .crown {
                    self.touchAction?()
                }else if self.buttonMode == .finding {
                    // userInteraction을 막아줘서 여기 호출안됨, 호출되게 하려면 이부분 풀어주기
                    self.touchAction?()
                }
            })
        .disposed(by: bag)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setNormalButton(text: String?) {
        buttonMode = .normal
        normalView.isHidden = false
        crownView.isHidden = true
        findingView.isHidden = true

        if let _text = text {
            self.normalLabel.text = _text
        }
    }
    
    func setCrownCountButton() {
        buttonMode = .crown
        normalView.isHidden = true
        crownView.isHidden = false
        findingView.isHidden = true
        
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.75, target: self, selector: #selector(finishCrownButtonMode), userInfo: nil, repeats: false)

        if crown == 0 {
            self.crownLabel.text = "크라운 무료"
        }else{
            self.crownLabel.text = "크라운 \(crown)"
        }
    }
    
    func setFindingButton() {
        stopTimer()
        buttonMode = .finding
        normalView.isHidden = true
        crownView.isHidden = true
        findingView.isHidden = false
        
        findingLabel.text = "결과 수집 중"
        
        findingLottiWrapView.removeAllSubView()
        let lottiView = AnimationView.init(name: "00_motion_btn_loading")
        self.lottiView = lottiView
        lottiView.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        lottiView.center = CGPoint.init(x: 12.5, y: 12.5)
        lottiView.loopMode = .loop
        findingLottiWrapView.addSubview(lottiView)
        lottiView.play()
        
        button.isUserInteractionEnabled = false
    }
    
    func findingLottiPlay() {
        if let lotti = lottiView{
            if !lotti.isAnimationPlaying {
                lotti.play()
            }
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }

    @objc
    func finishCrownButtonMode() {
        stopTimer()
        setNormalButton(text: nil)
    }
}
