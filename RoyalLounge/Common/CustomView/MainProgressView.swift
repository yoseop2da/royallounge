//
//  MainProgressView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class MainProgressView: UIView {

    private let progress = UIActivityIndicatorView()
    private var timer: Timer?

    override init(frame: CGRect) {
        super.init(frame: frame)
        drawView(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func start(timeout: TimeInterval = 15.0) {
        // 애니메이션
        mainAsync { self.progress.startAnimating() }
        if timer == nil { addTimer(timeout: timeout) }
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1.0
        }
    }

    func stop() {
        progressTimeout()
    }
}

private extension MainProgressView {
    func drawView(frame: CGRect) {
        self.frame = frame
        self.backgroundColor = UIColor.clear // .rgbAll(200, 0.5)
        progress.style = .gray//whiteLarge
        progress.tintColor = .primary100
        progress.center = self.center
        self.addSubview(progress)
    }

    @objc func progressTimeout() {
        removeTimer()
        progress.stopAnimating()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0.0
        }
    }

    func addTimer(timeout: TimeInterval) {
        removeTimer()
        timer = Timer.scheduledTimer(timeInterval: timeout, target: self, selector: #selector(progressTimeout), userInfo: nil, repeats: false)
    }

    func removeTimer() {
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
    }
}
