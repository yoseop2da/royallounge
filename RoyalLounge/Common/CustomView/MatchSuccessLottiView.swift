
//
//  MatchSuccessLottiView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Lottie

class MatchSuccessLottiView: UIView {
    
    private lazy var lopttiView = AnimationView(name: "motion_matching_effect")
    init() {
        let frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: UIScreen.height)
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear

        lopttiView.frame = frame
        lopttiView.loopMode = .playOnce
        
        self.addSubview(lopttiView)
        self.isUserInteractionEnabled = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start(finishCallback: (() -> Void)?) {
        self.lopttiView.play { _ in
            finishCallback?()
        }
    }
}
