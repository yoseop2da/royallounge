
//
//  MobileNoButton.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class MobileNoButton: UIButton {
    enum ButtonMode {
        case normal
        case crown
        case opened
    }
    var crown: Int = 0
    
    var buttonMode: ButtonMode = .normal
    
    var timer: Timer?
    
    private var mbNo: String?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setNormalButton(mbNo: String?) {
        self.mbNo = mbNo
        
        self.setBorder(color: .brown75)
        self.layer.cornerRadius = 6.0
        self.tintColor = .brown75
//        self.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 8)
        
        self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
        self.setTitleColor(.brown75, for: .normal)
        self.setTitleColor(.brown75, for: .highlighted)
        self.backgroundColor = .white
        
        let image = UIImage.init(named: "icon16PxPhone")
        self.setImage(image, for: .normal)
        if let _mbNo = mbNo {
            buttonMode = .opened
            self.setTitle(_mbNo, for: .normal)
        }else{
            buttonMode = .normal
            self.setTitle("상대의 연락처 확인하기", for: .normal)
        }
        
    }
    
    func setCrownCountButton() {
        buttonMode = .crown
        
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.75, target: self, selector: #selector(finishCrownButtonMode), userInfo: nil, repeats: false)
        
        self.setBorder(color: .clear)
        self.layer.cornerRadius = 6.0
        
        self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(.white, for: .highlighted)
        self.backgroundColor = .brown75
        
        self.setImage(nil, for: .normal)
        if crown == 0 {
            self.setTitle("크라운 무료", for: .normal)
        }else{
            self.setTitle("크라운 \(crown)", for: .normal)
        }
        
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc
    func finishCrownButtonMode() {
        stopTimer()
        setNormalButton(mbNo: self.mbNo)
    }
}

