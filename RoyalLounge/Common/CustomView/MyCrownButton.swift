
//
//  MyCrownButton.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class MyCrownButton: UIButton {
    init(count: Int) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: 140, height: 40))
        setTitle("\(count)", for: .normal)
        setImage(UIImage.init(named: "icons24PxCrown"), for: .normal)
        titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 20)
        titleLabel?.textAlignment = .right
        setTitleColor(.primary100, for: .normal)
        contentHorizontalAlignment = .right
        contentEdgeInsets.right = 1
        imageEdgeInsets.right = 4
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setCrownLabel(text: String) {
        setTitle(text, for: .normal)
    }
}
