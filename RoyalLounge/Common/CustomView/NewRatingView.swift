//
//  NewRatingView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Lottie

class NewRatingView: UIView {

    private var lopttiView1: AnimationView!
    private var lopttiView2: AnimationView!
    private var lopttiView3: AnimationView!
    private var lopttiView4: AnimationView!
    private var lopttiView5: AnimationView!
    

    fileprivate var starButton1 = StarButton()
    fileprivate var starButton2 = StarButton()
    fileprivate var starButton3 = StarButton()
    fileprivate var starButton4 = StarButton()
    fileprivate var starButton5 = StarButton()

//    fileprivate var starInfoLabel1 = StarInfoLabel()
//    fileprivate var starInfoLabel2 = StarInfoLabel()
//    fileprivate var starInfoLabel3 = StarInfoLabel()
//    fileprivate var starInfoLabel4 = StarInfoLabel()
//    fileprivate var starInfoLabel5 = StarInfoLabel()

    // 버튼 누른 직후
    fileprivate var starButtonBeginCallback: (() -> Void)?
    // 애니메이션 종료시 콜백
    fileprivate var starButtonEndCallback: ((Int) -> Void)?

    init(frame: CGRect, starButtonBeginCallback: (() -> Void)?, starButtonEndCallback: ((Int) -> Void)? ) {
        super.init(frame: frame)
        // 시작시 콜백
        self.starButtonBeginCallback = starButtonBeginCallback
        // 애니메이션 종료시 콜백
        self.starButtonEndCallback = starButtonEndCallback
        self.addStarButtons(viewSize: frame.size)
        
        
        
//        if !self.lopttiView.isAnimationPlaying {
//            self.lopttiView.play()
//        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func starButtonTouched(sender: StarButton) {
//        sender.isSelected = true
//        sender.touchAnimation(scaleValue: 1.30) {
            // startCB
            self.starButtonBeginCallback?()
            // endCB
//            delay(0.5) { self.starButtonEndCallback?(Int(sender.tag)) }

            self.starButtonUserInteraction(enabled: false)
            switch Int(sender.tag) {
            case 1:
                self.starButton1.setImage(nil, for: .normal)
                self.starButton1.setImage(nil, for: .selected)
                self.starButton1.addSubview(self.lopttiView1)
                self.lopttiView1.play()
            case 2:
                self.starButton1.setImage(nil, for: .normal)
                self.starButton1.setImage(nil, for: .selected)
                self.starButton1.addSubview(self.lopttiView1)
                self.lopttiView1.play()
                
                delay(0.3) {
                    self.starButton2.setImage(nil, for: .normal)
                    self.starButton2.setImage(nil, for: .selected)
                    self.starButton2.addSubview(self.lopttiView2)
                    self.lopttiView2.play()
                }
            case 3:
                self.starButton1.setImage(nil, for: .normal)
                self.starButton1.setImage(nil, for: .selected)
                self.starButton1.addSubview(self.lopttiView1)
                self.lopttiView1.play()
                
                delay(0.3) {
                    self.starButton2.setImage(nil, for: .normal)
                    self.starButton2.setImage(nil, for: .selected)
                    self.starButton2.addSubview(self.lopttiView2)
                    self.lopttiView2.play()
                }
                delay(0.6) {
                    self.starButton3.setImage(nil, for: .normal)
                    self.starButton3.setImage(nil, for: .selected)
                    self.starButton3.addSubview(self.lopttiView3)
                    self.lopttiView3.play()
                }
            case 4:
                self.starButton1.setImage(nil, for: .normal)
                self.starButton1.setImage(nil, for: .selected)
                self.starButton1.addSubview(self.lopttiView1)
                self.lopttiView1.play()
                
                delay(0.3) {
                    self.starButton2.setImage(nil, for: .normal)
                    self.starButton2.setImage(nil, for: .selected)
                    self.starButton2.addSubview(self.lopttiView2)
                    self.lopttiView2.play()
                }
                delay(0.6) {
                    self.starButton3.setImage(nil, for: .normal)
                    self.starButton3.setImage(nil, for: .selected)
                    self.starButton3.addSubview(self.lopttiView3)
                    self.lopttiView3.play()
                }
                delay(0.9) {
                    self.starButton4.setImage(nil, for: .normal)
                    self.starButton4.setImage(nil, for: .selected)
                    self.starButton4.addSubview(self.lopttiView4)
                    self.lopttiView4.play()
                }
            case 5:
                self.starButton1.setImage(nil, for: .normal)
                self.starButton1.setImage(nil, for: .selected)
                self.starButton1.addSubview(self.lopttiView1)
                self.lopttiView1.play()
                
                delay(0.3) {
                    self.starButton2.setImage(nil, for: .normal)
                    self.starButton2.setImage(nil, for: .selected)
                    self.starButton2.addSubview(self.lopttiView2)
                    self.lopttiView2.play()
                }
                delay(0.6) {
                    self.starButton3.setImage(nil, for: .normal)
                    self.starButton3.setImage(nil, for: .selected)
                    self.starButton3.addSubview(self.lopttiView3)
                    self.lopttiView3.play()
                }
                delay(0.9) {
                    self.starButton4.setImage(nil, for: .normal)
                    self.starButton4.setImage(nil, for: .selected)
                    self.starButton4.addSubview(self.lopttiView4)
                    self.lopttiView4.play()
                }
                delay(1.2) {
                    self.starButton5.setImage(nil, for: .normal)
                    self.starButton5.setImage(nil, for: .selected)
                    self.starButton5.addSubview(self.lopttiView5)
                    self.lopttiView5.play()
                }
            default: break
            }
            // startCB
            self.starButtonBeginCallback?()
            // endCB
//        let time = 0.3 * Double(sender.tag)
//            delay(0.8) { self.starButtonEndCallback?(Int(sender.tag)) }
//        }
    }

    func starButtonInitialize() {
        self.starButton1.isSelected = false
        self.starButton2.isSelected = false
        self.starButton3.isSelected = false
        self.starButton4.isSelected = false
        self.starButton5.isSelected = false
        self.starButtonUserInteraction(enabled: true)
    }
}

extension NewRatingView {
    fileprivate func addStarButtons(viewSize: CGSize) {
        mainAsync {
            // 버튼 추가
            self.addSubview(self.starButton1)
            self.addSubview(self.starButton2)
            self.addSubview(self.starButton3)
            self.addSubview(self.starButton4)
            self.addSubview(self.starButton5)

//            self.addSubview(self.starInfoLabel1)
//            self.addSubview(self.starInfoLabel2)
//            self.addSubview(self.starInfoLabel3)
//            self.addSubview(self.starInfoLabel4)
//            self.addSubview(self.starInfoLabel5)

            // 버튼 frame 잡기
            let buttonWidth: CGFloat = UIScreen.underSE ? CGFloat(viewSize.width - 20.0 * 2) / 5.0 : 60
            let buttonHeight: CGFloat = viewSize.height // - 15 라벨
            let button3Frame = CGRect(x: (self.frame.width/2.0 - buttonWidth/2.0), y: 0, width: buttonWidth, height: buttonHeight)
            let button1Frame = CGRect(x: button3Frame.origin.x - buttonWidth*2, y: 0, width: buttonWidth, height: buttonHeight)
            let button2Frame = CGRect(x: button3Frame.origin.x - buttonWidth, y: 0, width: buttonWidth, height: buttonHeight)
            let button4Frame = CGRect(x: button3Frame.origin.x + buttonWidth, y: 0, width: buttonWidth, height: buttonHeight)
            let button5Frame = CGRect(x: button3Frame.origin.x + buttonWidth*2, y: 0, width: buttonWidth, height: buttonHeight)

            self.starButton1.setFrame(frame: button1Frame, tag: 1)
            self.starButton2.setFrame(frame: button2Frame, tag: 2)
            self.starButton3.setFrame(frame: button3Frame, tag: 3)
            self.starButton4.setFrame(frame: button4Frame, tag: 4)
            self.starButton5.setFrame(frame: button5Frame, tag: 5)

            self.starButton1.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton2.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton3.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton4.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton5.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)

            self.lopttiView1 = AnimationView.init(name: "_star0")
            self.lopttiView1.frame = CGRect.init(x: 0, y: -5, width: buttonWidth+10, height: buttonHeight+5)
            self.lopttiView1.animationSpeed = 3.0
            self.lopttiView1.loopMode = .loop

            self.lopttiView2 = AnimationView.init(name: "_star1")
            self.lopttiView2.frame = CGRect.init(x: 0, y: -5, width: buttonWidth+10, height: buttonHeight+5)
            self.lopttiView2.animationSpeed = 3.0
            self.lopttiView2.loopMode = .loop
            
            self.lopttiView3 = AnimationView.init(name: "_star2")
            self.lopttiView3.frame = CGRect.init(x: 0, y: -5, width: buttonWidth+10, height: buttonHeight+5)
            self.lopttiView3.animationSpeed = 3.0
            self.lopttiView3.loopMode = .loop
            
            self.lopttiView4 = AnimationView.init(name: "_star3")
            self.lopttiView4.frame = CGRect.init(x: 0, y: -5, width: buttonWidth+10, height: buttonHeight+5)
            self.lopttiView4.animationSpeed = 3.0
            self.lopttiView4.loopMode = .loop
            
            self.lopttiView5 = AnimationView.init(name: "_star4")
            self.lopttiView5.frame = CGRect.init(x: 0, y: -5, width: buttonWidth+10, height: buttonHeight+5)
            self.lopttiView5.animationSpeed = 3.0
            self.lopttiView5.loopMode = .loop
            
            
            
//            self.starInfoLabel1.frame = CGRect.init(x: button1Frame.origin.x, y: button1Frame.origin.y + buttonHeight + 2, width: button1Frame.width, height: 15)
//            self.starInfoLabel2.frame = CGRect.init(x: button2Frame.origin.x, y: button2Frame.origin.y + buttonHeight + 2, width: button2Frame.width, height: 15)
//            self.starInfoLabel3.frame = CGRect.init(x: button3Frame.origin.x, y: button3Frame.origin.y + buttonHeight + 2, width: button3Frame.width, height: 15)
//            self.starInfoLabel4.frame = CGRect.init(x: button4Frame.origin.x, y: button4Frame.origin.y + buttonHeight + 2, width: button4Frame.width, height: 15)
//            self.starInfoLabel5.frame = CGRect.init(x: button5Frame.origin.x, y: button5Frame.origin.y + buttonHeight + 2, width: button5Frame.width, height: 15)
//
//            self.starInfoLabel1.text = "별로"
//            self.starInfoLabel2.text = "보통"
//            self.starInfoLabel3.text = "괜찮아요"
//            self.starInfoLabel4.text = "호감"
//            self.starInfoLabel5.text = "좋아요!"
        }
    }

    fileprivate func starButtonUserInteraction(enabled: Bool) {
        starButton1.isUserInteractionEnabled = enabled
        starButton2.isUserInteractionEnabled = enabled
        starButton3.isUserInteractionEnabled = enabled
        starButton4.isUserInteractionEnabled = enabled
        starButton5.isUserInteractionEnabled = enabled
    }

}
