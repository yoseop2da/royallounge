
//
//  ProfileScrollView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ProfileScrollView: UIView, UIScrollViewDelegate {
//    private var mainScrollView: UIScrollView?
    private var pageLabel: UILabel?
    private var touchButton: UIButton?
    private let bag = DisposeBag()
    private var scoreStatusWrapView: UIView?
    private var scoreStatusLabel: UILabel?
    private var imageTotalCount: Int = 0
    
    var touchAction: (()->Void)?
    var currentPageIdx: Int = 0 {
        didSet {
            pageLabel?.text = "\(currentPageIdx+1) / \(imageTotalCount)"
            currentPageCallback?(currentPageIdx)
        }
    }
    
    var currentPageCallback: ((Int)->Void)?
    
    init(imageUrls: [String], statusLabelOn: Bool = true, bgColor: UIColor = .gray10, countLabelOn: Bool = true, touchActionOn: Bool = false) {
        let width = UIScreen.width
        super.init(frame: CGRect.init(x: 0, y: 0, width: width, height: width))
        let scrollView = UIScrollView.init(frame: self.frame)
        scrollView.backgroundColor = bgColor
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        self.addSubview(scrollView)
        
        let statusWrapView = UIView.init()
        scoreStatusWrapView = statusWrapView
        scoreStatusWrapView?.isHidden = true
        self.addSubview(statusWrapView)
        statusWrapView.snp.makeConstraints {
            $0.bottom.leading.trailing.equalToSuperview()
            $0.height.equalTo(20)
        }
        
        if statusLabelOn {
            let label = UILabel.init()
            scoreStatusLabel = label
            label.textAlignment = .center
            label.textColor = .white
            label.font = UIFont.spoqaHanSansBold(ofSize: 12)
            statusWrapView.addSubview(label)
            label.snp.makeConstraints {
                $0.top.leading.trailing.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-2)
            }
        }
        
        imageTotalCount = imageUrls.count
        if imageTotalCount > 0 {
            if countLabelOn {
                let labelWarpView = UIView()
                labelWarpView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
                self.addSubview(labelWarpView)
                labelWarpView.layer.cornerRadius = 12.0
                
                labelWarpView.snp.makeConstraints {
                    $0.bottom.equalTo(-10)
                    $0.trailing.equalTo(-20)
                    $0.width.equalTo(45)
                    $0.height.equalTo(24)
                }
                
                let label = UILabel.init()
                pageLabel = label
                label.textAlignment = .center
                label.textColor = .white
                label.font = UIFont.spoqaHanSansBold(ofSize: 12)
                label.text = "1 / \(imageTotalCount)"
                labelWarpView.addSubview(label)
                label.snp.makeConstraints {
                    $0.top.bottom.leading.trailing.equalToSuperview()
                }
            }
            
            for (idx, imageUrl) in imageUrls.enumerated() {
                let imageFrame = CGRect.init(x: width * CGFloat(idx), y: 0, width: width, height: width)
                let imageView = UIImageView.init(frame: imageFrame)
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                imageView.setImage(urlString: imageUrl)
                scrollView.addSubview(imageView)
                
                if touchActionOn {
                    imageView.rx.tapGesture()
                        .filter{ $0.state == .ended }
                        .subscribe(onNext: { _ in
                            self.touchAction?()
                        })
                        .disposed(by: bag)
                }
            }
            scrollView.contentSize = CGSize.init(width: width * CGFloat(imageTotalCount), height: width)
        }
        
        currentPageIdx = 1
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var previousPage = 0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.bounds.width
        let pageFraction = scrollView.contentOffset.x/pageWidth

        let page = Int((round(pageFraction)))
        if previousPage != page {
            previousPage = page
            currentPageIdx = page
        }
    }
    
    func drawScoreLabel(scoreType: String?) {
        mainAsync {
            let scoreType = ScoreType.type(fromString: scoreType)
            if scoreType == .bothHigh {
                self.scoreStatusWrapView?.setHighScoreGradient(start: UIColor.rgb(255, 207, 92, 0.01), end: UIColor.rgb(255, 207, 92, 0.9))
                self.scoreStatusWrapView?.isHidden = false
                self.scoreStatusLabel?.text = "서로 높은 별점 준 상대"
            }else if scoreType == .sendHigh {
                self.scoreStatusWrapView?.setHighScoreGradient(start: UIColor.rgb(181, 138, 103, 0.01), end: UIColor.rgb(181, 138, 103, 0.9))
                self.scoreStatusWrapView?.isHidden = false
                self.scoreStatusLabel?.text = "내가 높은 별점 준 상대"
            }else if scoreType == .receiveHigh {
                self.scoreStatusWrapView?.setHighScoreGradient(start: UIColor.rgb(158, 82, 21, 0.01), end: UIColor.rgb(158, 82, 21, 0.9))
                self.scoreStatusWrapView?.isHidden = false
                self.scoreStatusLabel?.text = "나에게 높은 별점 준 상대"
            }else{
                self.scoreStatusWrapView?.isHidden = true
                self.scoreStatusLabel?.text = ""
            }
        }
    }
}

