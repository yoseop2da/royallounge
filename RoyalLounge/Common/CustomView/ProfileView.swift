
//
//  ProfileView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class ProfileView: UIView {
    private var imageView = UIImageView()
    private var button = UIButton()
    private var badge = UIView()
    private let bag = DisposeBag()
    
    var isBadgeHidden: Bool = true {
        willSet(new) {
            badge.isHidden = new
        }
    }
    
    init(urlString: String?, action: (()->Void)?) {
        let frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        super.init(frame: frame)
        self.addSubview(imageView)
        self.addSubview(badge)
        self.addSubview(button)
        
        imageView.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        imageView.contentMode = .scaleAspectFill
        imageView.setImage(urlString: urlString ?? "", placeholderImage: UIImage.init(named: "avataCirclePlaceHolder"))
        
        imageView.layer.cornerRadius = 20.0
        imageView.clipsToBounds = true
        imageView.setBorder(color: .gray10)
        
        badge.frame = CGRect.init(x: 30, y: 0, width: 10, height: 10)
        badge.layer.cornerRadius = 5.0
        badge.backgroundColor = .primary100
        badge.setBorder(width: 0.5, color: UIColor.rgb(238, 229, 221, 1.0))
        isBadgeHidden = false
        
        button.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        button.rx.tap
            .rxTouchAnimation(button: self, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                action?()
            })
            .disposed(by: bag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(urlString: String) {
        imageView.setImage(urlString: urlString, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"))
    }
}
