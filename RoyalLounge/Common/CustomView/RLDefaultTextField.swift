//
//  RLDefaultTextField.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/30.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RLDefaultTextField: UIView {
    
    var bag = DisposeBag()
    
    lazy var textField: RLTextField = {
        let field = RLTextField()
        field.backgroundColor = .clear
        field.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
        field.placeholder = ""
        return field
    }()
    
    var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .gray50Alpha40
        return view
    }()
    
    var bottomMsg: UILabel = {
        let label = UILabel()
        label.font = UIFont.spoqaHanSansRegular(ofSize: 12.0)
        return label
    }()
    
    var placeholder: String? {
        get { textField.placeholder }
        set(value) { textField.placeholder = value }
    }
    
    var returnKeyType: UIReturnKeyType {
        get { textField.returnKeyType }
        set(value) { textField.returnKeyType = value }
    }
    
    var keyboardType: UIKeyboardType {
        get { textField.keyboardType }
        set(value) {
            textField.keyboardType = value
        }
    }
    
    var isSecureTextEntry: Bool {
        get { textField.isSecureTextEntry }
        set(value) {
            textField.isSecureTextEntry = value
        }
    }

    var becomeFirst: Bool = false {
        willSet(new) {
            if new {
                textField.becomeFirstResponder()
                textObserver.accept(textField.text ?? "")
            }
        }
    }
    
    var maxLength: Int = 1000
    var hasSpace: Bool = true
    var containEmoji: Bool = true
    let textObserver = BehaviorRelay<String>.init(value: "")
    var text: String = "" {
        willSet(str) {
            textObserver.accept(str)
            textField.text = str
        }
    }
    
    var isOnlyKorOrNum = false
    var tallInput = false
    var returnKeyCallback: (() -> Void)?
    
    var textAlignment: NSTextAlignment {
        get { textField.textAlignment }
        set(value) { textField.textAlignment = value }
    }
    
    init(placeholder: String, isPassword: Bool = false, maxLength: Int = 1000, hasSpace: Bool = true, tallInput: Bool = false, isOnlyKorOrNum: Bool = false, containEmoji: Bool = true, textAlignment: NSTextAlignment = .left) {

        super.init(frame: .zero)
        
        self.addSubview(textField)
        self.addSubview(bottomLine)
        self.addSubview(bottomMsg)
        
        self.tallInput = tallInput
        self.isOnlyKorOrNum = isOnlyKorOrNum
        self.containEmoji = containEmoji
        self.textAlignment = textAlignment
        self.maxLength = maxLength
        self.hasSpace = hasSpace
        
        textField.rx.text.orEmpty
            .bind(to: textObserver)
            .disposed(by: bag)
        
        textField.placeholder = placeholder
        textField.textAlignment = textAlignment
        bottomMsg.textAlignment = textAlignment
        
        let rightView = UIView()
        let clear = UIButton()
        clear.setImage(UIImage.init(named: "icons24PxRemove"), for: .normal)
        clear.rx.tap
            .subscribe(onNext: { _ in
                self.emptyMode()
            })
            .disposed(by: bag)
        
        if isPassword {
            let showPassword = UIButton()
            showPassword.setImage(UIImage.init(named: "icons24PxEye"), for: .normal)
            showPassword.setImage(UIImage.init(named: "icons24PxEyeHide"), for: .selected)
            showPassword.rx.tap
                .subscribe(onNext: { _ in
                    self.textField.isSecureTextEntry.toggle()
                    showPassword.isSelected.toggle()
                })
                .disposed(by: bag)
            
            rightView.frame = CGRect.init(x: 0, y: 30, width: 60, height: 30)
            rightView.addSubview(showPassword)
            rightView.addSubview(clear)
            showPassword.snp.makeConstraints {
                $0.top.leading.bottom.equalToSuperview()
                $0.width.equalTo(30)
                $0.trailing.equalTo(clear.snp.leading)
            }
            clear.snp.makeConstraints {
                $0.top.trailing.bottom.equalToSuperview()
                $0.width.equalTo(30)
                $0.leading.equalTo(showPassword.snp.trailing)
            }
            textField.rightView = rightView
            textField.fieldType = .password
        }else{
            rightView.frame = CGRect.init(x: 0, y: 30, width: 30, height: 30)
            rightView.addSubview(clear)
            clear.snp.makeConstraints {
                $0.top.leading.trailing.bottom.equalToSuperview()
                $0.width.equalTo(30)
            }
            textField.rightView = rightView
            textField.fieldType = .clear
        }
        
        textField.rightViewMode = .never
        textField.rx
            .controlEvent(.editingDidBegin)
            .subscribe(onNext: { _ in
                if let text = self.textField.text {
                    self.textField.rightViewMode = text.length > 0 ? .always : .never
                }
                if self.bottomLine.backgroundColor == .warningColor { return }
                self.bottomLine.backgroundColor = .dark
            })
            .disposed(by: bag)
        
        textField.rx
            .controlEvent(.editingDidEnd)
            .subscribe(onNext: { _ in
                self.textField.rightViewMode = .never
                if self.bottomLine.backgroundColor == .warningColor { return }
                self.bottomLine.backgroundColor = .gray50Alpha40
                self.bottomMsg.text = ""
            })
            .disposed(by: bag)
        
        textField.rx.controlEvent(.editingChanged)
            .subscribe(onNext: { [unowned self] in
                if let text = self.textField.text {
                    self.textField.rightViewMode = text.length > 0 ? .always : .never
                }
            })
            .disposed(by: bag)

        
        bottomLine.snp.makeConstraints {
            $0.top.equalTo(textField.snp.bottom).offset(4.0)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(2)
        }
        
        bottomMsg.snp.makeConstraints {
            $0.top.equalTo(bottomLine.snp.bottom)
            $0.leading.equalToSuperview().offset(15.0)
            $0.trailing.equalToSuperview().offset(-15.0)
            $0.height.equalTo(18)
            $0.bottom.equalToSuperview()
        }
        
        textField.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func emptyMode() {
        self.text = ""
        bottomLine.backgroundColor = .dark
        bottomMsg.text = ""
        self.textField.rightViewMode = .never
    }
}

extension RLDefaultTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.first == nil {return true} // delete
        if string == "\n" {
            returnKeyCallback?()
        }
        if !containEmoji {
            return !Validation.shared.containsEmoji(word: string)
        }
        if isOnlyKorOrNum {
            return Validation.shared.isOnlyKorOrNum(word: string)
        }
        if tallInput {
            if let text = textField.text {
                if text.isEmpty {
                    if let char = string.first {
                        return char == "1" || char == "2"
                    }
                }
            }
        }
        if !hasSpace && (string == " ") { return false }
        if textField.text!.count < maxLength { return true }
        return false
    }
}
