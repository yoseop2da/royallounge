//
//  RLLoadingView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Lottie

class RLLoadingView: UIView {

//    private lazy var rTImageView = RTImageView()
    private lazy var lopttiView = AnimationView.init(name: "loading")
    
    private var timeOutTimer: Timer?

    init() {
        super.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: UIScreen.height))
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)

        
        lopttiView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        lopttiView.center = CGPoint.init(x: UIScreen.width/2.0, y: UIScreen.height/2.0)
        lopttiView.loopMode = .loop//repeat(100)
        
        self.addSubview(lopttiView)
        self.alpha = 0.0
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func start(timeout: TimeInterval) {
        removeTimeOutTimer()

//        self.rTImageView.startAnimating()
        self.lopttiView.play()
        UIView.animate(withDuration: 0.3) { self.alpha = 1.0 }

        timeOutTimer = Timer.scheduledTimer(timeInterval: timeout, target: self, selector: #selector(timeOutTimeFinished), userInfo: nil, repeats: false)
    }

    func stop(delaySec: Double, completion: (() -> Void)?) {
        delay(delaySec) {
//            self.rTImageView.stopAnimating()
            self.lopttiView.stop()
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0.0
            }, completion: { _ in
                completion?()
            })
        }
    }

    @objc func timeOutTimeFinished() {
        removeTimeOutTimer()
        stop(delaySec: 1.0, completion: nil)
    }

    func removeTimeOutTimer() {
        self.timeOutTimer?.invalidate()
        self.timeOutTimer = nil
    }
}

//class RTImageView: UIView {
//
//    private lazy var imageView = UIImageView()
//    private var images = [UIImage.init(named: "loadingIcon01"), UIImage.init(named: "loadingIcon02"), UIImage.init(named: "loadingIcon03")]
//
//    private var imageChangeTimer: Timer?
//    private var imageIdx: Int = 0
//
//    init(width: CGFloat = 80.0) {
//        super.init(frame: CGRect.init(x: 0, y: 0, width: width, height: width))
//        self.backgroundColor = .primary100
//        self.layer.cornerRadius = CGFloat(width/2.0)
//        self.layer.masksToBounds = true
//        self.clipsToBounds = true
//
//        let imageWidth = CGFloat(40.0)
//        let offsetY = CGFloat((width - imageWidth)/2.0)
//        imageView.frame = CGRect.init(x: offsetY, y: offsetY, width: imageWidth, height: imageWidth)
//        imageView.image = images[imageIdx]
//        self.addSubview(imageView)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    @objc func imageChanged() {
//        imageIdx += 1
//        if imageIdx > 2 {
//            imageIdx = 0
//        }
//        imageView.image = images[imageIdx]
//    }
//
//    func startAnimating() {
//        removeImageChangeTimer()
//        imageChangeTimer = Timer.scheduledTimer(timeInterval: 0.18, target: self, selector: #selector(imageChanged), userInfo: nil, repeats: true)
//    }
//
//    func stopAnimating() {
//        self.removeImageChangeTimer()
//    }
//
//    func removeImageChangeTimer() {
//        self.imageChangeTimer?.invalidate()
//        self.imageChangeTimer = nil
//    }
//}
