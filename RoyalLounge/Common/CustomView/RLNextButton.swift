
//
//  RLNextButton.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/08.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class RLNextButton: UIButton {
    enum RoundType {
        case round
        case roundWith(radius: CGFloat)
        case square
    }
    
    var bag = DisposeBag()
    
    var title: String? {
        get { self.titleLabel?.text }
        set(value) {
            self.setTitle(value, for: .normal)
            self.setTitle(value, for: .highlighted)
            self.setTitle(value, for: .selected)
        }
    }

    init(attributedText: NSAttributedString, roundType: RoundType = .round, bgColor: UIColor = .primary100) {
        super.init(frame: .zero)
        switch roundType {
        case .round:
            layer.cornerRadius = 6.0
        case .roundWith(let radius):
            layer.cornerRadius = radius
        default: ()
        }
        
        titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        backgroundColor = bgColor
        setAttributedTitle(attributedText, for: .normal)
        setAttributedTitle(attributedText, for: .highlighted)
        setAttributedTitle(attributedText, for: .selected)
    }
    
    init(title: String, roundType: RoundType = .round, bgColor: UIColor = .primary100, titleColor: UIColor = .white) {
        super.init(frame: .zero)
//        self.buttonType = UIButton.ButtonType.system
        switch roundType {
        case .round:
            layer.cornerRadius = 6.0
        case .roundWith(let radius):
            layer.cornerRadius = radius
        default: ()
        }
        
        titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        backgroundColor = bgColor
        setTitle(title, for: .normal)
        setTitle(title, for: .highlighted)
        setTitle(title, for: .selected)
        
        setTitleColor(titleColor, for: .normal)
        setTitleColor(titleColor, for: .highlighted)
        setTitleColor(titleColor, for: .selected)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func roundCorner() {
        layer.cornerRadius = 6.0
        self.snp.updateConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
    }
    
    func squareCorner() {
        layer.cornerRadius = 0.0
        self.snp.updateConstraints {
            $0.top.bottom.leading.trailing.equalToSuperview()
        }
    }
}
