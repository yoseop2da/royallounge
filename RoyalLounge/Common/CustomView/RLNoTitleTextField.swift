//
//  RLDefaultTextField.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/05.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

import SnapKit

class RLNoTitleTextField: RLDefaultTextField {

    enum FieldType {
        case big
        case small
    }
    
    init(placeholder: String, type: RLTextField.SizeType = .small, isPassword: Bool = false, maxLength: Int = 1000, hasSpace: Bool = true, tallInput: Bool = false, isOnlyKorOrNum: Bool = false, containEmoji: Bool = true, textAlignment: NSTextAlignment = .left) {
        
        super.init(placeholder: placeholder, isPassword: isPassword, maxLength: maxLength, hasSpace: hasSpace, tallInput: tallInput, isOnlyKorOrNum: isOnlyKorOrNum, containEmoji: containEmoji, textAlignment: textAlignment)
        
        textField.sizeType = type
        
        if type == .small {
            textField.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
        }else{
            textField.font = UIFont.spoqaHanSansRegular(ofSize: 26.0)
        }
        
        textField.rx.text.orEmpty
            .subscribe(onNext: { str in
                if type == .big && str.length > 11 {
                    self.textField.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
                }else{
                    if type == .small {
                        self.textField.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
                    }else{
                        self.textField.font = UIFont.spoqaHanSansRegular(ofSize: 26.0)
                    }
                }
            })
            .disposed(by: bag)
        
        textField.snp.makeConstraints {
            if type == .small {
                $0.top.equalToSuperview().offset(8.0)
                $0.leading.equalToSuperview().offset(15.0)
                $0.trailing.equalToSuperview().offset(-15.0)
                $0.height.equalTo(24)
            }else{
                $0.top.equalToSuperview().offset(0.0)
                $0.leading.equalToSuperview().offset(15.0)
                $0.trailing.equalToSuperview().offset(-15.0)
                $0.height.equalTo(35)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
