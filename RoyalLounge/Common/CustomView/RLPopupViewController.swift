//
//  RLPopupViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RLPopupViewController: BaseViewController {
    lazy var backBgButton = UIButton()
    lazy var wrapView = UIView()
    
    var cellInfo: [RLPopup.PopUpCell] = []
    var enableBackGroundCloseAction: Bool = true
    private var lastView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backBgButton.backgroundColor = .clear
        view.addSubview(backBgButton)
        backBgButton.snp.makeConstraints { $0.leading.trailing.top.bottom.equalToSuperview() }
        backBgButton.rx.tap
            .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .bind{
                if self.enableBackGroundCloseAction {
                    self.dismiss(animated: true, completion: nil)
                }
            }.disposed(by: bag)
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(wrapView)
        wrapView.backgroundColor = .white
        wrapView.layer.cornerRadius = 10.0
        wrapView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.equalTo(300)
        }
        let lastIndex = cellInfo.count - 1
        cellInfo.enumerated().forEach { index, cell in
            switch cell {
            case .image(let named, let height):
                let imageView = UIImageView()
                wrapView.addSubview(imageView)
                imageView.image = UIImage.init(named: named)
                imageView.contentMode = .scaleAspectFit
                imageView.snp.makeConstraints {
                    if let last = lastView {
                        $0.top.equalTo(last.snp.bottom).offset(20)
                    }else{
                        $0.top.equalToSuperview().offset(30)
                    }
                    $0.leading.trailing.equalToSuperview()
                    $0.height.equalTo(height)
                    $0.centerX.equalToSuperview()
                    if(index == lastIndex) { $0.bottom.equalToSuperview().offset(-20) }
                }
                lastView = imageView
            case .text(let _title, let _font):
                let label = UILabel()
                wrapView.addSubview(label)
                label.numberOfLines = 0
                label.font = _font// UIFont.spoqaHanSansBold(ofSize: 15.0)
                label.text = _title//"서비스 이용제한 알림"
                label.textAlignment = .left
                label.textColor = .dark
                label.lineBreakMode = .byCharWrapping
                label.snp.makeConstraints {
                    if let last = lastView {
                        $0.top.equalTo(last.snp.bottom).offset(20)
                    }else{
                        $0.top.equalToSuperview().offset(30)
                    }
                    $0.leading.equalToSuperview().offset(20)
                    $0.trailing.equalToSuperview().offset(-20)
                    if(index == lastIndex) { $0.bottom.equalToSuperview().offset(-20) }
                }
                lastView = label
            case .attrText(let attr):
                let label = UILabel()
                wrapView.addSubview(label)
                label.numberOfLines = 0
                label.attributedText = attr
                label.lineBreakMode = .byCharWrapping
                label.snp.makeConstraints {
                    if let last = lastView {
                        $0.top.equalTo(last.snp.bottom).offset(20)
                    }else{
                        $0.top.equalToSuperview().offset(30)
                    }
                    $0.leading.equalToSuperview().offset(20)
                    $0.trailing.equalToSuperview().offset(-20)
                    if(index == lastIndex) { $0.bottom.equalToSuperview().offset(-20) }
                }
                lastView = label
            case .boxAttrText(let attr):
                let boxView = UIView()
                boxView.backgroundColor = UIColor.creamwhiteBG
                boxView.layer.cornerRadius = 5.0
                wrapView.addSubview(boxView)
                boxView.snp.makeConstraints {
                    if let last = lastView {
                        $0.top.equalTo(last.snp.bottom).offset(20)
                    }else{
                        $0.top.equalToSuperview().offset(30)
                    }
                    $0.leading.equalToSuperview().offset(20)
                    $0.trailing.equalToSuperview().offset(-20)
                    $0.height.greaterThanOrEqualTo(50)
                    if(index == lastIndex) { $0.bottom.equalToSuperview().offset(-20) }
                }
                let label = UILabel()
                boxView.addSubview(label)
                label.numberOfLines = 0
                label.attributedText = attr
                label.layer.masksToBounds = true
                label.snp.makeConstraints {
                    $0.top.leading.equalToSuperview().offset(16)
                    $0.trailing.bottom.equalToSuperview().offset(-16)
                }
                lastView = boxView
            case .buttonBox(let leftButtonTuple, let rightButtonTuple):
                let buttonBox = UIView()
                wrapView.addSubview(buttonBox)
                buttonBox.snp.makeConstraints {
                    $0.top.equalTo(lastView!.snp.bottom).offset(20)
                    $0.leading.equalToSuperview().offset(20)
                    $0.trailing.equalToSuperview().offset(-10)
                    $0.height.greaterThanOrEqualTo(40)
                    if(index == lastIndex) { $0.bottom.equalToSuperview().offset(-20) }
                }
                let buttonRight = UIButton()
                buttonRight.setTitleColor(rightButtonTuple.color, for: .normal)
                buttonRight.titleLabel?.font = rightButtonTuple.font
                buttonRight.rx.tap
                    .subscribe { _ in
                        if rightButtonTuple.autoClose {
                            self.dismiss(animated: true, completion: {
                                rightButtonTuple.action?()
                            })
                        }else{
                            rightButtonTuple.action?()
                        }
                    }
                    .disposed(by: bag)
                buttonBox.addSubview(buttonRight)
                buttonRight.setTitle("   \(rightButtonTuple.title)   ", for: .normal)
                buttonRight.snp.makeConstraints {
                    $0.top.bottom.trailing.equalTo(buttonBox).offset(0)
                }
                
                if let _leftButtonTuple = leftButtonTuple {
                    let buttonLeft = UIButton()
                    buttonLeft.setTitleColor(_leftButtonTuple.color, for: .normal)
                    buttonLeft.titleLabel?.font = _leftButtonTuple.font
                    buttonBox.addSubview(buttonLeft)
                    buttonLeft.rx.tap
                        .subscribe { _ in
                            if _leftButtonTuple.autoClose {
                                self.dismiss(animated: true, completion: {
                                    _leftButtonTuple.action?()
                                })
                            }else{
                                _leftButtonTuple.action?()
                            }
                        }
                        .disposed(by: bag)
                    buttonLeft.setTitle("   \(_leftButtonTuple.title)   ", for: .normal)
                    buttonLeft.snp.makeConstraints {
                        $0.top.bottom.equalTo(buttonBox).offset(0)
                        $0.trailing.equalTo(buttonRight.snp.leading).offset(-15)
                        //            $0.width.equalTo(150)
                    }
                }
                lastView = buttonBox
                
            }
        }
    }
}
