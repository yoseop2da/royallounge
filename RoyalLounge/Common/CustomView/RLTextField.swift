//
//  RLTextField.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RLTextField: UITextField {
    enum SizeType {
        case big
        case small
    }
    enum FieldType: CGFloat {
        case normal = 5.0
        case clear = 30.0
        case password = 60.0
        var rightValue: CGFloat { self.rawValue }
    }
    var fieldType: FieldType = .normal
    var sizeType: SizeType = .small
    
    init(fieldType: FieldType = .normal, sizeType: SizeType = .small) {
        super.init(frame: .zero)
        self.fieldType = fieldType
        self.sizeType = sizeType
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var customInset = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: fieldType.rightValue)
    private lazy var placeholderInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: fieldType.rightValue)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        if self.sizeType == .small {
//            return bounds.inset(by: customInset)
//        }else{
            return super.textRect(forBounds: bounds)
//        }
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        if self.sizeType == .small {
//            return bounds.inset(by: customInset)
//        }else{
            return super.textRect(forBounds: bounds)
//        }
    }
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        if self.sizeType == .small {
//            return bounds.inset(by: customInset)
//        }else{
            return super.textRect(forBounds: bounds)
//        }
    }
}
