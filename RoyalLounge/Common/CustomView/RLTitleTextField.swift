//
//  RLTitleTextField.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/05.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

import SnapKit

class RLTitleTextField: RLDefaultTextField {
    
    private var topTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.alpha = 0.0
        label.text = ""
        label.textColor = .darkGray
        label.font = UIFont.spoqaHanSansRegular(ofSize: 11.0)
        return label
    }()
    
    var title: String? {
        get { return topTitleLabel.text }
        set(value) { topTitleLabel.text = value }
    }
    
    init(placeholder: String, title: String, isPassword: Bool = false, maxLength: Int = 1000, hasSpace: Bool = true, tallInput: Bool = false, isOnlyKorOrNum: Bool = false, containEmoji: Bool = true, textAlignment: NSTextAlignment = .left) {
        
        super.init(placeholder: placeholder, isPassword: isPassword, maxLength: maxLength, hasSpace: hasSpace, tallInput: tallInput, containEmoji: containEmoji, textAlignment: textAlignment)
        
        self.addSubview(topTitleLabel)
        topTitleLabel.text = title
        
        topTitleLabel.snp.makeConstraints {
            $0.top.equalTo(textField.snp.top).offset(0.0)
            $0.leading.equalToSuperview().offset(15.0)
            $0.trailing.equalToSuperview()
            $0.height.equalTo(13)
        }

        textField.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8.0)
            $0.leading.equalToSuperview().offset(15.0)
            $0.trailing.equalToSuperview().offset(-15.0)
            $0.height.equalTo(24)
        }
        
        textField.rx.text.orEmpty
            .subscribe(onNext: { text in
                if text.count == 0 {
                    self.hideTopTitle()
                }else{
                    self.showTopTitle()
                }
            })
            .disposed(by: bag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showTopTitle() {
        if topTitleLabel.alpha == 1.0 { return }
        UIView.animate(withDuration: 0.3) {
            self.topTitleLabel.alpha = 1.0
            self.topTitleLabel.snp.updateConstraints {
                $0.top.equalTo(self.textField.snp.top).offset(-5.0)
                $0.height.equalTo(13)
            }
            self.layoutIfNeeded()
        }
    }
    
    func hideTopTitle() {
        if topTitleLabel.alpha == 0.0 { return }
        UIView.animate(withDuration: 0.3) {
            self.topTitleLabel.alpha = 0.0
            self.topTitleLabel.snp.updateConstraints {
                $0.top.equalTo(self.textField.snp.top).offset(0)
                $0.height.equalTo(24)
            }
            
            self.layoutIfNeeded()
        }
    }

    override func emptyMode() {
        super.emptyMode()
        self.hideTopTitle()
    }
    
}
