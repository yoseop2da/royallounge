//
//  RatingView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RatingView: UIView {

    fileprivate var starButton1 = StarButton()
    fileprivate var starButton2 = StarButton()
    fileprivate var starButton3 = StarButton()
    fileprivate var starButton4 = StarButton()
    fileprivate var starButton5 = StarButton()

//    fileprivate var starInfoLabel1 = StarInfoLabel()
//    fileprivate var starInfoLabel2 = StarInfoLabel()
//    fileprivate var starInfoLabel3 = StarInfoLabel()
//    fileprivate var starInfoLabel4 = StarInfoLabel()
//    fileprivate var starInfoLabel5 = StarInfoLabel()

    // 버튼 누른 직후
    fileprivate var starButtonBeginCallback: (() -> Void)?
    // 애니메이션 종료시 콜백
    fileprivate var starButtonEndCallback: ((Int) -> Void)?

    init(frame: CGRect, starButtonBeginCallback: (() -> Void)?, starButtonEndCallback: ((Int) -> Void)? ) {
        super.init(frame: frame)
        // 시작시 콜백
        self.starButtonBeginCallback = starButtonBeginCallback
        // 애니메이션 종료시 콜백
        self.starButtonEndCallback = starButtonEndCallback
        self.addStarButtons(viewSize: frame.size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func starButtonTouched(sender: StarButton) {
        sender.isSelected = true
//        sender.touchAnimation(scaleValue: 1.30) {
            // startCB
            self.starButtonBeginCallback?()
            // endCB
//            delay(0.5) { self.starButtonEndCallback?(Int(sender.tag)) }

            self.starButtonUserInteraction(enabled: false)
            switch Int(sender.tag) {
            case 1: UIView.flip(self.starButton1)
            case 2: UIView.flip(self.starButton1, self.starButton2)
            case 3: UIView.flip(self.starButton1, self.starButton2, self.starButton3)
            case 4: UIView.flip(self.starButton1, self.starButton2, self.starButton3, self.starButton4)
            case 5: UIView.flip(self.starButton1, self.starButton2, self.starButton3, self.starButton4, self.starButton5)
            default: break
            }
            // startCB
            self.starButtonBeginCallback?()
            // endCB
            delay(0.8) { self.starButtonEndCallback?(Int(sender.tag)) }
//        }
    }

    func starButtonInitialize() {
        self.starButton1.isSelected = false
        self.starButton2.isSelected = false
        self.starButton3.isSelected = false
        self.starButton4.isSelected = false
        self.starButton5.isSelected = false
        self.starButtonUserInteraction(enabled: true)
    }
}

extension RatingView {
    fileprivate func addStarButtons(viewSize: CGSize) {
        mainAsync {
            // 버튼 추가
            self.addSubview(self.starButton1)
            self.addSubview(self.starButton2)
            self.addSubview(self.starButton3)
            self.addSubview(self.starButton4)
            self.addSubview(self.starButton5)

//            self.addSubview(self.starInfoLabel1)
//            self.addSubview(self.starInfoLabel2)
//            self.addSubview(self.starInfoLabel3)
//            self.addSubview(self.starInfoLabel4)
//            self.addSubview(self.starInfoLabel5)

            // 버튼 frame 잡기
            let buttonWidth: CGFloat = UIScreen.underSE ? CGFloat(viewSize.width - 20.0 * 2) / 5.0 : 60
            let buttonHeight: CGFloat = viewSize.height // - 15 라벨
            let button3Frame = CGRect(x: (self.frame.width/2.0 - buttonWidth/2.0), y: 0, width: buttonWidth, height: buttonHeight)
            let button1Frame = CGRect(x: button3Frame.origin.x - buttonWidth*2, y: 0, width: buttonWidth, height: buttonHeight)
            let button2Frame = CGRect(x: button3Frame.origin.x - buttonWidth, y: 0, width: buttonWidth, height: buttonHeight)
            let button4Frame = CGRect(x: button3Frame.origin.x + buttonWidth, y: 0, width: buttonWidth, height: buttonHeight)
            let button5Frame = CGRect(x: button3Frame.origin.x + buttonWidth*2, y: 0, width: buttonWidth, height: buttonHeight)

            self.starButton1.setFrame(frame: button1Frame, tag: 1)
            self.starButton2.setFrame(frame: button2Frame, tag: 2)
            self.starButton3.setFrame(frame: button3Frame, tag: 3)
            self.starButton4.setFrame(frame: button4Frame, tag: 4)
            self.starButton5.setFrame(frame: button5Frame, tag: 5)

            self.starButton1.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton2.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton3.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton4.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)
            self.starButton5.addTarget(self, action: #selector(self.starButtonTouched(sender:)), for: .touchUpInside)

//            self.starInfoLabel1.frame = CGRect.init(x: button1Frame.origin.x, y: button1Frame.origin.y + buttonHeight + 2, width: button1Frame.width, height: 15)
//            self.starInfoLabel2.frame = CGRect.init(x: button2Frame.origin.x, y: button2Frame.origin.y + buttonHeight + 2, width: button2Frame.width, height: 15)
//            self.starInfoLabel3.frame = CGRect.init(x: button3Frame.origin.x, y: button3Frame.origin.y + buttonHeight + 2, width: button3Frame.width, height: 15)
//            self.starInfoLabel4.frame = CGRect.init(x: button4Frame.origin.x, y: button4Frame.origin.y + buttonHeight + 2, width: button4Frame.width, height: 15)
//            self.starInfoLabel5.frame = CGRect.init(x: button5Frame.origin.x, y: button5Frame.origin.y + buttonHeight + 2, width: button5Frame.width, height: 15)
//
//            self.starInfoLabel1.text = "별로"
//            self.starInfoLabel2.text = "보통"
//            self.starInfoLabel3.text = "괜찮아요"
//            self.starInfoLabel4.text = "호감"
//            self.starInfoLabel5.text = "좋아요!"
        }
    }

    fileprivate func starButtonUserInteraction(enabled: Bool) {
        starButton1.isUserInteractionEnabled = enabled
        starButton2.isUserInteractionEnabled = enabled
        starButton3.isUserInteractionEnabled = enabled
        starButton4.isUserInteractionEnabled = enabled
        starButton5.isUserInteractionEnabled = enabled
    }

}

class StarButton: UIButton {
    func setFrame(frame: CGRect, tag: Int) {
        self.frame = frame
        self.tag = tag
        self.isSelected = false
        
        let outTester = MainInformation.shared.outTester
        let heartNormalImage = UIImage(named: outTester ? "compStarHeartNor" : "compStarNor") //icStarNor
        let heartSelectedImage = UIImage(named: outTester ? "compStarHeartSel" : "icStarSelected")
        self.setImage(heartNormalImage, for: .normal)
        self.setImage(heartSelectedImage, for: .highlighted)
        self.setImage(heartSelectedImage, for: .selected)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

//class StarInfoLabel: UILabel {
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        initializeLabel()
//    }
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        initializeLabel()
//    }
//
//    init() {
//        super.init(frame: .zero)
//        initializeLabel()
//    }
//
//    func initializeLabel() {
//        self.textAlignment = .center
//        self.font = UIFont.notoSansKrRegular(ofSize: 12.0)
//        self.textColor = UIColor.rtGray158
//    }
//}
