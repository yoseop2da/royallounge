
//
//  RedDotImageView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class RedDotImageView: UIView {
    private var imageView = UIImageView()
    private var button = UIButton()
    private var badge = UIView()
    private let bag = DisposeBag()
    
    var isBadgeHidden: Bool = true {
        willSet(new) {
            badge.isHidden = new
        }
    }
    
    init(named: String, action: (()->Void)?) {
        let frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        super.init(frame: frame)
        
//        let colors: [UIColor] = [.blue, .red, .orange, .green]
//        self.backgroundColor = colors[Int.random(in: 0..<colors.count)].withAlphaComponent(0.5)
        
        self.addSubview(imageView)
        self.addSubview(badge)
        self.addSubview(button)
        
        imageView.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        imageView.contentMode = .right
        imageView.image = UIImage.init(named: named)
        
        imageView.layer.cornerRadius = 12.0
        imageView.clipsToBounds = true
        
        badge.frame = CGRect.init(x: 18, y: 0, width: 10, height: 10)
        badge.layer.cornerRadius = 5.0
        badge.backgroundColor = .primary100
        badge.setBorder(color: UIColor.rgb(238, 229, 221, 1.0))
        isBadgeHidden = false
        
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        button.rx.tap
            .rxTouchAnimation(button: self, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                action?()
            })
            .disposed(by: bag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(urlString: String) {
        imageView.setImage(urlString: urlString)
    }
}
