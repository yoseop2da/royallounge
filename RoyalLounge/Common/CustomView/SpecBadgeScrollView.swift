//
//  SpecBadgeScrollView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/23.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class SpecBadgeScrollView: UIView {

    var scrollView = UIScrollView.init(frame: .zero)
    
    var touchAction: ((CardDetailSpec)->Void)?
    
    init(specList: [CardDetailSpec]) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width - 84, height: 134))
        self.addSubview(scrollView)
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.snp.makeConstraints{
            $0.top.bottom.leading.trailing.equalToSuperview()
        }
        
        updateSpecList(specList: specList)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateSpecList(specList: [CardDetailSpec]) {
        specList.enumerated().forEach({ idx, spec in
            let badgeView = BadgeView.init(idx: idx, spec: spec)
            badgeView.touchAction = { spec in
                self.touchAction?(spec)
            }
            self.scrollView.addSubview(badgeView)
        })
        
        let width = UIScreen.width - 84
        if specList.count > 0 && specList.count < 4 {
            let x = (width - ((66.0 + 20.0) * CGFloat(specList.count)))
            delay(0.0) { self.scrollView.contentOffset = CGPoint.init(x: -x, y: 0) }
        }else if specList.count >= 4 {
            let width = (66/*width*/ + 20/*padding*/) * specList.count
            self.scrollView.contentSize = CGSize.init(width: width, height: 134)
        }
        
        self.alpha = 0.0
        UIView.animate(withDuration: 0.1) {
            self.alpha = 1.0
        } // contentOffset 이동하는 텀에 대해서 효과주기
        
    }
}

class BadgeView: UIView {
    var imageView: UIImageView!
    var button = UIButton.init(frame: .zero)
    var label = UILabel.init(frame: .zero)
    
    private let bag = DisposeBag()
    var touchAction: ((CardDetailSpec)->Void)?
    init(idx: Int, spec: CardDetailSpec) {
        let x = (66 + (idx > 0 ? 20 : 0)) * idx
        super.init(frame: CGRect.init(x: x, y: 0, width: 66, height: 134))
        imageView = UIImageView.init(image: UIImage.init(named: "userProfileFlagLong"))
        imageView.contentMode = .bottom
        self.addSubview(imageView)
        self.addSubview(button)
        self.addSubview(label)
        
        imageView.snp.makeConstraints{
            $0.top.bottom.leading.trailing.equalToSuperview()
        }
        button.snp.makeConstraints{
            $0.top.bottom.leading.trailing.equalToSuperview()
        }
        label.snp.makeConstraints{
            $0.top.equalTo(98)
            $0.leading.equalTo(14)
            $0.trailing.equalTo(-14)
        }

        label.font = UIFont.spoqaHanSansBold(ofSize: 10)
        label.textColor = .white
        label.numberOfLines = 0
        
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.text = spec.title?.replacingOccurrences(of: " ", with: "\n")
        
//        let newStr = badge.replacingOccurrences(of: " ", with: "\n")
//        let paragraph = NSMutableParagraphStyle()
//        paragraph.minimumLineHeight = 10
//        paragraph.alignment = .center
//        paragraph.lineBreakMode = .byWordWrapping
//        label.attributedText = NSAttributedString.init(string: newStr, attributes: [.paragraphStyle: paragraph])
        
        button.rx.tap
            .rxTouchAnimation(button: self, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.touchAction?(spec)
            })
        .disposed(by: bag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
