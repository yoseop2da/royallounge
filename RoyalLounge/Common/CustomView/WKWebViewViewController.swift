//
//  WKWebViewViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//


import UIKit
import WebKit

class WKWebViewViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, UIScrollViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    var webView: WKWebView!

    var webTitle: String = ""
    var webUrl: String = ""
    var webScaleValue = true
    var webViewClosedCallback: () -> Void = {}

    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView = WKWebView()
        self.webView.contentMode = .scaleAspectFill

        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        self.webView = WKWebView(frame: self.view.bounds, configuration: wkWebConfig)

        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self

        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.webView)
        self.view.sendSubviewToBack(self.webView)

        if #available(iOS 11.0, *) {
            let safeArea = self.view.safeAreaLayoutGuide
            self.webView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
            self.webView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
            self.webView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
            self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        } else {
            self.webView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            self.webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            self.webView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }

        if webTitle.length > 0 {
            self.webView.scrollView.contentInset = UIEdgeInsets.init(top: 50, left: 0, bottom: 0, right: 0)
            self.titleLabel.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        }
        self.webView.scrollView.bouncesZoom = false
        if !self.webScaleValue {
            self.webView.scrollView.delegate = self
        }

        self.view.backgroundColor = UIColor.white
        if let url = URL(string: webUrl) {
            let requestObj = URLRequest(url: url)

            self.webView.load(requestObj)
        }

        self.title = webTitle
        self.titleLabel.text = webTitle
    }

    @IBAction func closeButtonTouched(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func closeTouched(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

    // Disable zooming in webView
    func viewForZooming(in: UIScrollView) -> UIView? {
        return nil
    }

    func webView(_ webView: WKWebView, didFinishLoading navigation: WKNavigation) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        print("didFinishLoadingNavigation: \(navigation)")
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let surl = navigationAction.request.url?.absoluteString {
            // 페이스북 좋아요 이후 창닫는 케이스.
            if surl.range(of: "https://www.facebook.com/plugins/close_popup.php") != nil {
                let favorit = surl.components(separatedBy: "&")
                if favorit.count > 0 {
                    // 좋아요를 누른상태
                } else {
                    // 취소를 누른상태
                }
                self.dismiss(animated: true, completion: {
                    self.webViewClosedCallback()
                })
            }
        }
        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        //if <a> tag does not contain attribute or has _blank then navigationAction.targetFrame will return nil
//        if navigationAction.targetFrame == nil {
//            self.webView.load(navigationAction.request)
//        }
        if let surl = navigationAction.request.url?.absoluteString {
            print("웹뷰 새창으로 이동 [\(surl)]")
            if surl.contains("itunes.apple.com") {
                if let url = URL(string: surl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:])
                    } else {
                        // Fallback on earlier versions
                    }
                }
                return nil
            }
            showWebView(self, title: " ", url: surl)
        }
        return nil
    }
}
