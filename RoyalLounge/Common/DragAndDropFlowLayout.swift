//
//  DragAndDropFlowLayout.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class DragAndDropFlowLayout: UICollectionViewFlowLayout {
    var longPressGesture: UILongPressGestureRecognizer!

    var originIndexPath: IndexPath?
    var dragIndexPath: IndexPath?

    var thumbnailView: UIView?

    override func prepare() {
        super.prepare()

        installGestureRecognizer()
    }

    func installGestureRecognizer() {
        if longPressGesture == nil {
            longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction(_:)))
            longPressGesture.minimumPressDuration = 0.2
            collectionView?.addGestureRecognizer(longPressGesture)
        }
    }

    @objc func longPressAction(_ longPress: UILongPressGestureRecognizer) {
        guard let profileCollectionView = collectionView else { return }
        let location = longPress.location(in: profileCollectionView)
        let windowLocation = longPress.location(in: mainAppDelegate.window!)
//        print("\t -------> \(windowLocation.x)|\(windowLocation.y)")
        switch longPress.state {
        case .began: startDrag(at: location, windowLocation: windowLocation)
        case .changed: updateDrag(at: location, windowLocation: windowLocation)
        case .ended: endDrag(at: location, windowLocation: windowLocation)
        default:
            break
        }
    }

    func startDrag(at location: CGPoint, windowLocation: CGPoint) {
        guard let profileCollectionView = collectionView else { return }
        guard let indexPath = profileCollectionView.indexPathForItem(at: location) else { return }
        guard profileCollectionView.dataSource?.collectionView?(profileCollectionView, canMoveItemAt: indexPath) == true else { return }
        guard let cell = profileCollectionView.cellForItem(at: indexPath) else { return }
        guard let thumbView = cell.snapshotView(afterScreenUpdates: true) else { return }

        // 필터 적용 할경우.
        // import GPUImage
        //        if let filterImg = cell.profileImageView.image {
        //            let stretchDistortion = StretchDistortion()
        //            let filteredImage = filterImg.filterWithOperation(stretchDistortion)
        //            let imageView = UIImageView(frame: thumbView.frame)
        //            imageView.image = filteredImage
        //            imageView.backgroundColor = UIColor.red
        //            thumbView.addSubview(imageView)
        //        }

        originIndexPath = indexPath
        dragIndexPath = indexPath
        thumbnailView = thumbView

        cell.isHidden = true
        thumbView.frame = CGRect.init(origin: CGPoint.init(x: windowLocation.x - cell.frame.size.width/2.0, y: windowLocation.y - cell.frame.size.height/2.0), size: cell.frame.size)
        mainAppDelegate.window?.addSubview(thumbView)

        thumbView.setShadow(radius: 5, opacity: 0.8, color: UIColor.black)

        invalidateLayout()
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.0, options: [], animations: {
            thumbView.alpha = 0.65
            thumbView.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        }, completion: nil)
    }

    func updateDrag(at location: CGPoint, windowLocation: CGPoint) {
        guard let view = thumbnailView else { return }
        guard let profileCollectionView = collectionView else { return }

        view.center = windowLocation

        if let newIndexPath = profileCollectionView.indexPathForItem(at: location) {
            profileCollectionView.moveItem(at: dragIndexPath!, to: newIndexPath)
            dragIndexPath = newIndexPath
        }
    }

    func endDrag(at location: CGPoint, windowLocation: CGPoint) {
        guard let dragView = thumbnailView else { return }
        guard let indexPath = dragIndexPath else { return }
        guard let profileCollectionView = collectionView else { return }
        guard let datasource = profileCollectionView.dataSource else { return }
        guard let cell = profileCollectionView.cellForItem(at: indexPath) else { return }

        dragView.setShadow(radius: 5, opacity: 0.8, color: UIColor.black)

        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.0, options: [], animations: {
            dragView.center = windowLocation
            dragView.transform = CGAffineTransform.identity
            dragView.setShadow(radius: 0, color: UIColor.white)
        }) { (completed) in
            cell.isHidden = false
            if indexPath != self.originIndexPath! { datasource.collectionView?(profileCollectionView, moveItemAt: self.originIndexPath!, to: indexPath) }
            dragView.removeFromSuperview()
            self.dragIndexPath = nil
            self.thumbnailView = nil
            self.invalidateLayout()
        }
    }
}
