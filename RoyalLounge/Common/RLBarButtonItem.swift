//
//  RLBarButtonItem.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class RLBarButtonItem: UIBarButtonItem {

    enum BarType {
        case back
        case close
        case closeWhite
        case title(titleString: String)
        case attrTitle(attrString: NSAttributedString)
        case customButton(title: String, font: UIFont, textColor: UIColor)
        case crown(count: Int, action: (()->Void)?)
        case profile(action: (()->Void)?)
        case notification(action: (()->Void)?)
        case settings(action: (()->Void)?)
    }
    var bag = DisposeBag()
    
    var customButton: UIButton?
    var crownButton: MyCrownButton?
    var profileView: ProfileView?
    var notificationView: RedDotImageView?
    var settingsView: RedDotImageView?
    
    override init() {
        super.init()
        self.tintColor = UIColor.dark
    }
    
    convenience
    init(barType: BarType) {
        self.init()
        switch barType {
        case .back:
            self.image = UIImage.init(named: "icons24PxBack")
        case .close:
            self.image = UIImage.init(named: "icons24PxClose")
        case .closeWhite:
            self.image = UIImage.init(named: "icons24PxCloseWhite")
        case .title(let titleString):
            self.title = titleString
        case .attrTitle(let attrString):
            let label = UILabel()
            label.attributedText = attrString
            label.sizeToFit()
            self.customView = label
        case .customButton(let string, let font, let color):
            let btn = UIButton.init(type: .custom)
            customButton = btn
            btn.setTitle(string, for: .normal)
            btn.titleLabel?.font = font
            btn.setTitleColor(color, for: .normal)
            self.customView = btn
        case .crown(let count, let action):
            let btn = MyCrownButton(count: count)
            crownButton = btn
            btn.rx.tap
                .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    action?()
                })
                .disposed(by: bag)
            self.customView = btn
        case .profile(let action):
            let v = ProfileView.init(urlString: nil, action: action)
            profileView = v
            self.customView = v
            
        case .notification(let action):
            let v = RedDotImageView.init(named: "icons24PxNotification", action: action)
            notificationView = v
            self.customView = v
            
        case .settings(let action):
            let v = RedDotImageView.init(named: "icons24PxSetting", action: action)
            settingsView = v
            self.customView = v
        }
    }
    

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


