//
//  RLEnum.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

enum RLResultCD: String {
    case SUCCESS = "0000"  // 성공
    case NETWORK_ERROR = "NETWORK_ERROR"  // 성공
    case E00000_Error = "E00000"  // 정의되어있지 않은 에러
    case E01000_NoMember = "E01000"  // 일치하는 비밀번호 없음
    case E01001_AlreadyMember = "E01001"  // 이미 회원
    case E01002_blockMember = "E01002" // 이용 영구 정지
    case E01003_stopMember = "E01003" // 이용 일시 정지
    case E01004_AgeLimit = "E01004" // 나이 제한
    case E01005_IDPWError = "E01005" // 아디 비밀번호 입력 오류
    case E01006_NicknameDuplicated = "E01006" // 닉네임 중복
    case E01007_NicknameShortLong = "E01007" // 닉네임 짧거나 길때
    case E01018_CurrentNicknameNotEqual = "E01018" // 현재 비밀번호가 일치하지 않습니다.
    case E01023_CurrentNicknameNotEqual = "E01023" // 비밀번호가 일치하지 않습니다.
    case E01024_CanNotChange = "E01024" // 회원 스펙을 비노출로 변경할 수 없습니다.
    
    
    case E01301_CrownNotEnough = "E01301" // 닉네임 짧거나 길때
    case E01403_CanNotDelete = "E01403" // 더이상 카드 삭제 불가능
    case E01404_SleepMember = "E01404" // 휴면계정 상태로 확인할 수 없는 카드입니다.
    case E01405_OutMember = "E01405" // 회원탈퇴 상태로 확인할 수 없는 카드입니다.
    case E01406_ReportedMember = "E01406" // 신고 누적 회원으로 확인할 수 없는 카드입니다.
    case E01407_ReadyMember = "E01407" // 가입심사중으로, 승인완료시 열람할 수 있어요.
    case E01408_DeletedCard = "E01408" // 삭제된 상태로 확인할 수 없는 카드입니다.
    case E01603_CanNotRollback = "E01603" // 되돌릴 수 있는 카드가 없습니다.
    case E01605_CanNotRollback = "E01605" // 되돌릴 수 없는 카드입니다.
    
    case E01011_EmailDuplicated = "E01011" // 이메일 중복
    case E01012_EmailNotValidIDPWError = "E01012" // 유효하지 않은 형식
    
    case E01017_CanNotSleepTime = "E01017" //휴면계정 전환이 불가한 시간입니다.\n( 오전 11:00 ~ 오후 12:30 까지)
    
    
    
    var code: String { self.rawValue }
}
