//
//  RLImagePicker.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import YPImagePicker

class RLImagePicker: NSObject {

    enum ImageType {
        case square // 정사각형
        case rectangle // 직사각형
    }
    static let shared = RLImagePicker()

    fileprivate var picker = UIImagePickerController()

    let takePicture = "사진촬영"
    let fromAlbum = "갤러리"
    let cancel = "취소"
    
    // MARK: - 🍒load and upload
    func load(_ parent: UIViewController, imageType type: ImageType = .square, maxNum: Int = 1, imageCB: @escaping ([(image: UIImage, jpgData: Data)]) -> Void) {

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let cameraAction = UIAlertAction(title: takePicture, style: .default) { _ in
            self.loadYPImage(parent, imageType: type, isStartGallery: false, maxNum: maxNum, imageCB: imageCB)
        }

        let gallaryAction = UIAlertAction(title: fromAlbum, style: .default) { _ in
            self.loadYPImage(parent, imageType: type, isStartGallery: true, maxNum: maxNum, imageCB: imageCB)
        }

        let cancelAction = UIAlertAction(title: cancel, style: .cancel) { _ in

        }

        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the actionsheet
        parent.present(alert, animated: true, completion: nil)
    }
    
    // InstagramStyle
    func loadYPImage(_ parent: UIViewController, imageType: ImageType, isStartGallery: Bool, maxNum: Int, imageCB: @escaping ([(image: UIImage, jpgData: Data)]) -> Void) {
        var config = YPImagePickerConfiguration.init()
        config.isScrollToChangeModesEnabled = true
        config.usesFrontCamera = false
        config.showsPhotoFilters = false // 이거 테스트 해보자!!
        config.shouldSaveNewPicturesToAlbum = false
        config.startOnScreen = isStartGallery ? YPPickerScreen.library : YPPickerScreen.photo
        config.screens = [.library, .photo]
        config.showsCrop = .none
        config.targetImageSize = YPImageSize.cappedTo(size: 1000)
        config.overlayView = UIView()
        config.hidesStatusBar = true
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.wordings.libraryTitle = "모든 사진"
        config.wordings.cameraTitle = "사진"
        config.wordings.albumsTitle = "앨범"
        config.wordings.next = "완료"
        config.wordings.cancel = "취소"
        config.wordings.warningMaxItemsLimit = "최대 \(maxNum)개의 이미지를 선택할 수 있습니다."
        config.wordings.permissionPopup.title = "사진 접근 권한을 허용해주세요."
        config.wordings.permissionPopup.message = "설정 화면으로 이동합니다."
        config.wordings.permissionPopup.cancel = "취소"
        config.wordings.permissionPopup.grantPermission = "확인"

        config.onlySquareImagesFromCamera = (imageType == .square) ? true : false
        config.library.onlySquare = (imageType == .square) ? true : false
        config.library.defaultMultipleSelection = maxNum > 1 ? true : false
        config.library.maxNumberOfItems = maxNum

        let picker = YPImagePicker.init(configuration: config)
        picker.didFinishPicking { [unowned picker] items, cancelled in
            if cancelled { picker.dismiss(animated: true, completion: nil); return }
            imageCB(items.map{ item -> (image: UIImage, jpgData: Data)? in
                if case let .photo(photo) = item {
                    return (image: photo.image, jpgData: photo.image.jpegData(compressionQuality: 1.0)!)
                }else{
                    return nil
                }
            }.compactMap{$0})
//            if let photo = items.singlePhoto {
//                imageCB(photo.image, photo.image.jpegData(compressionQuality: 1.0)!)
//            }
            picker.dismiss(animated: true, completion: nil)
        }
        parent.present(picker, animated: true, completion: nil)
    }
}

//fileprivate extension RLImagePicker {
//    func openCamera(_ parent: UIViewController) {
//        if(UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)) {
//            picker.sourceType = .camera
////            picker.allowsEditing = true
//            picker.delegate = self
//            parent.present(picker, animated: true, completion: nil)
//        } else {
//            openGallary(parent)
//        }
//    }
//
//    func openGallary(_ parent: UIViewController) {
//        picker.sourceType = .photoLibrary
////        picker.allowsEditing = true
//        picker.delegate = self
//        parent.present(picker, animated: true, completion: nil)
//    }
//}
//
//extension RLImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate {
//    // - UIImagePickerControllerDelegate
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
//        picker.dismiss(animated: true, completion: nil)
//
//        guard var targetImage = info[picker.allowsEditing ? .editedImage : .originalImage] as? UIImage else { return }
//        if imageType == ..square && targetImage.size.width > targetImage.size.height {
//            // 가로가 세로보다 더 큰 케이스를 정사각형으로 잘라주자!!
//            targetImage = targetImage.cropToSquare()
//        }
//
//        var imageData: Data!
//        if targetImage.size.width > 3000 {
//            if let data = targetImage.jpegData(compressionQuality: 0.2) { imageData = data }
//        } else if targetImage.size.width > 2000 {
//            if let data = targetImage.jpegData(compressionQuality: 0.5) { imageData = data }
//        } else {
//            if let data = targetImage.jpegData(compressionQuality: 0.8) { imageData = data }
//        }
//        callBackImage(targetImage, imageData)
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        picker.dismiss(animated: true, completion: nil)
//    }
//}
