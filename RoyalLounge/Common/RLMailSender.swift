//
//  RLMailSender.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import MessageUI
import RxSwift

class RLMailSender: NSObject {
    enum 메일타입 {
        case 신고하기(nickname: String, userId: String, mbNo: String, cardNickname: String, reportNickname: String)
        case 서비스영구정지
        case 서비스나이제한
        case 서비스이용정지
        case 문의_미가입유저
        case 문의_가입유저(nickname: String, userId: String, mbNo: String)
        case 네트워크오류
        case 개선사항_버그(nickname: String, userId: String, mbNo: String)
        case 개선사항_신규(nickname: String, userId: String, mbNo: String)
    }
    static let shared = RLMailSender()
    private var finishCallback: ((Bool)->Void)?
    private let recipient = "help@royallounge.co.kr" //
    private let bag = DisposeBag()
    
    func sendSMS(bodyText: String) {
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.body = bodyText
            controller.messageComposeDelegate = self
            UIViewController.keyController().present(controller, animated: true)
        }
    }

    func sendEmail(_ mailType: 메일타입, finishCallback: ((Bool)->Void)?) {
        switch mailType {
        case .신고하기(let nickname, let userId, let mbNo, let cardNickname, let reportNickname):
            sendEmailReport(myNickname: nickname, userId: userId, mbNo: mbNo, cardNickname: cardNickname, title: reportNickname, finishCallback: finishCallback)
        case .서비스영구정지:
            sendEmailQuestion(subject: "[서비스 영구정지] 고객센터에 문의합니다.", finishCallback: finishCallback)
        case .서비스나이제한:
            sendEmailQuestion(subject: "[서비스 나이제한] 고객센터에 문의합니다.", finishCallback: finishCallback)
        case .서비스이용정지:
            sendEmailQuestion(subject: "[서비스 이용정지] 고객센터에 문의합니다.", finishCallback: finishCallback)
        case .문의_미가입유저:
            sendEmailQuestion(subject: "[문의] 고객센터에 문의합니다.", finishCallback: finishCallback)
        case .문의_가입유저(let nickname, let userId, let mbNo):
            sendEmailQuestion(subject: "[문의]'\(nickname)'님이 고객센터에 문의합니다.", myNickname: nickname, userId: userId, mbNo: mbNo, finishCallback: finishCallback)
        case .네트워크오류:
            sendEmailQuestion(subject: "[네트워크 오류]고객센터에 문의합니다.", finishCallback: finishCallback)
        case .개선사항_버그(let nickname, let userId, let mbNo):
            sendEmailQuestion(subject: "[개선사항]'\(nickname)'님이 고객센터에 아쉬운점을 전달합니다.", myNickname: nickname, userId: userId, mbNo: mbNo, bodyInformation: "* 진심으로 사과드립니다. 아쉬움점을 남겨주시면<br>개선해 나가는 로얄라운지가 되겠습니다.<br>", finishCallback: finishCallback)
        case .개선사항_신규(let nickname, let userId, let mbNo):
            sendEmailQuestion(subject: "[개선사항]'\(nickname)'님이 고객센터에 새로운 기능을 제시합니다.", myNickname: nickname, userId: userId, mbNo: mbNo, bodyInformation: "* 감사합니다. 소중한 의견을 귀담아<br>더 발전하는 로얄라운지가 되겠습니다.<br>", finishCallback: finishCallback)
        }
        
    }
    
    /// 문의 보내기
    /// - Parameters:
    ///   - subject: 제목
    ///   - myNickname: 닉네임
    ///   - userId: 유저이메일
    ///   - mbNo: 유저 전화번호
    ///   - bodyInformation: 안넣어줄경우 기본값 "* 답변까지 약 1-2일이 소요됩니다.<br>가능한 신속히 답변을 드리겠습니다.<br>"
    ///   - finishCallback: 완료 콜백
    private func sendEmailQuestion(subject: String, myNickname: String? = nil, userId: String? = nil, mbNo: String? = nil, bodyInformation: String? = nil, finishCallback: ((Bool)->Void)?) {
        self.finishCallback = finishCallback

        var bodyMessage = ""
        
        // 해당 문구 변경원할경우 호출할때 입력해주기
        let bodyInfo = bodyInformation ?? "* 답변까지 약 1-2일이 소요됩니다.<br>가능한 신속히 답변을 드리겠습니다.<br>"
        if let _myNickname = myNickname, let _userId = userId, let _mbNo = mbNo {
            bodyMessage = """
            <br>
            본인 닉네임 : \(_myNickname)<br>
            <hr>
            <b><br>
            
            문의내용 :<br><br><br>
            <hr>
            <b><br>
            \(bodyInfo)
            
            RoyalLounge ID : \(_userId)<br>
            Mobile Number : \(_mbNo)<br>
            RoyalLounge nickname : \(_myNickname)<br>
            RoyalLounge App Version : \(ApplicationInfo.appVersion)<br>
            Device Model : \(ApplicationInfo.deviceModel)<br>
            Device OS Version : \(ApplicationInfo.systemVersion)<br>
            <br></b>
            <br>
            """
        }else{
            bodyMessage = """
            <br>
            (유저 확인을 위해 아래 중 1개를 기재해주세요)<br>
            본인 닉네임 : 입력해주세요<br>
            가입 시 휴대폰 번호 : 입력해주세요<br>
            <hr>
            <b><br>
            
            문의내용 :<br><br><br>
            <hr>
            <b><br>
            \(bodyInfo)
            
            RoyalLounge App Version : \(ApplicationInfo.appVersion)<br>
            Device Model : \(ApplicationInfo.deviceModel)<br>
            Device OS Version : \(ApplicationInfo.systemVersion)<br>
            <br></b>
            <br>
            """
        }
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([self.recipient])
            mail.setSubject(subject)
            mail.setMessageBody(bodyMessage, isHTML: true)
            UIViewController.keyController().present(mail, animated: true)
        }
        else if let emailUrl = createEmailUrl(to: self.recipient, subject: subject, body: bodyMessage) {
            UIApplication.shared.open(emailUrl)
        } else {
            // show failure alert
            alertDialogOneButton(message: "휴대폰에 발신 가능한 기본 이메일 설정이\n되어 있지 않아 이메일 발송이 불가능합니다.\n기기 설정에서 이메일 계정을 연동해주세요.\n\n(설정 - 암호 및 계정 - 계정 추가)", okTitle: ALERT_OK)
        }
    }
    
    private func sendEmailReport(myNickname: String, userId: String, mbNo: String, cardNickname: String, title: String, finishCallback: ((Bool)->Void)?) {
        self.finishCallback = finishCallback
        
        let subject = "[\(title)]'\(myNickname)'님이 '\(cardNickname)'님을 신고합니다."
        let bodyMessage = """
        <hr>
        <b><br>
        
        신고내용 :<br><br>
        <hr>
        <b><br>
        ※회원신고에 대한 안내 및 주의사항<br>
        1. 적합한 신고시 소정의 크라운을 보상으로 제공합니다. 단, 반드시 증빙자료(대화내역 캡처 등)가 있어야 하며 매칭 후 15일 이내만 신고 가능합니다.<br><br>
        2. 비매너 행위자에 대한 신고의 경우, 신고자의 익명성은 보장합니다.<br><br>
        3. 매칭 후 연락두절 신고의 경우, 매칭 후 3일이 지나도록 본인이 먼저 연락하였음에도 상대의 응답이 없는 경우 신고가 가능하며 실제 연락두절 여부 파악을 위해 상대 회원에게 연락하여 확인할 수 있습니다. 허위 신고시 서비스 이용이 정지됩니다.<br><br>
        4. 신고 처리에는 최대 5일 이상의 시간이 소요될 수 있으므로 참고하시기 바랍니다.<br><br>
        <br><br><br>
        <hr>
        <b><br>
        RoyalLounge ID : \(userId)<br>
        Mobile Number : \(mbNo)<br>
        RoyalLounge nickname : \(myNickname)<br>
        RoyalLounge App Version : \(ApplicationInfo.appVersion)<br>
        Device Model : \(ApplicationInfo.deviceModel)<br>
        Device OS Version : \(ApplicationInfo.systemVersion)<br>
        <br></b>
        <br>
        """
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([self.recipient])
            mail.setSubject(subject)
            mail.setMessageBody(bodyMessage, isHTML: true)
            UIViewController.keyController().present(mail, animated: true)
        }
        else if let emailUrl = createEmailUrl(to: self.recipient, subject: subject, body: bodyMessage) {
            UIApplication.shared.open(emailUrl)
        } else {
            // show failure alert
            alertDialogOneButton(message: "휴대폰에 발신 가능한 기본 이메일 설정이\n되어 있지 않아 이메일 발송이 불가능합니다.\n기기 설정에서 이메일 계정을 연동해주세요.\n\n(설정 - 암호 및 계정 - 계정 추가)", okTitle: ALERT_OK)
        }
    }

    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")

        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }

        return defaultUrl
    }
}

extension RLMailSender: MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if error == nil && result == .sent {
            controller.dismiss(animated: true) {
                toast(message: "접수되었습니다", seconds: 1.0)
                self.finishCallback?(true)
            }
            return
        }
        
        controller.dismiss(animated: true, completion: nil)
        
    }

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true) {
            self.finishCallback?(false)
        }
    }
}
