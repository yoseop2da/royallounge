//
//  RLNotificationEnum.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/11.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

enum RLNotiType: String {
    case splash = "SPLASH"
    case logout = "LOGOUT"
    case profileChanged = "PROFILE_CHANGED"
    case crownChanged = "CROWN_CHANGED"
    case applicationBecomeActive = "BECOME_ACTIVE"

    var notiName: NSNotification.Name { return NSNotification.Name(rawValue: self.rawValue) }
}

class RLNotificationCenter: NSObject {
    static let shared = RLNotificationCenter()
    fileprivate var defaults = NotificationCenter.default

    func post(notiType: RLNotiType, object: Any? = nil) {
        defaults.post(name: notiType.notiName, object: object)
    }

    func addObserver(observer: Any, notiType: RLNotiType, selector: Selector) {
        defaults.addObserver(observer, selector: selector, name: notiType.notiName, object: nil)
    }

    func removeObserverAll(observer: Any) {
        defaults.removeObserver(observer)
    }

    func removeObserver(observer: Any, notiType: RLNotiType) {
        defaults.removeObserver(observer, name: notiType.notiName, object: nil)
    }

    func addObserver(observer: Any, selector: Selector, name: NSNotification.Name) {
        defaults.addObserver(observer, selector: selector, name: name, object: nil)
    }
}
