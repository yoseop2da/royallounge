//
//  RLPopup.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

/**
 ex>
 RLPopup.shared.show(leftAction: {
 RLMailSender.shared.sendEmail(mailType: .bugReport)
 }, rightAction: {
 })
 */

class RLPopup {
    enum PopUpCell {
        case image(named: String, imageHeight: CGFloat)
        case text(title: String, font: UIFont)
        case attrText(attrText: NSAttributedString)
        case boxAttrText(attrText: NSAttributedString)
        // 버튼이 한개일땐 right만 넣어주기
        // autoClose는 버튼 터치시 자동으로 close가됨
        case buttonBox(left: (title: String, color: UIColor, font: UIFont, autoClose: Bool, action: (()->Void)?)?, right: (title: String, color: UIColor, font: UIFont, autoClose: Bool, action: (()->Void)?))
    }
    
    static let shared = RLPopup()
    lazy var vc = RLPopupViewController()
}
    
extension RLPopup {
    // MARK: - 노멀 팝업
    func showNormal(parent: UIViewController? = nil, title: String? = nil, description: String, enableBackGroundCloseAction: Bool = true, isLeftButtonGray: Bool = false, leftButtonTitle: String? = nil, rightButtonTitle: String, leftAutoClose: Bool = true, rightAutoClose: Bool = true, leftAction: (()->Void)? = {}, rightAction: (()->Void)? = {}) {
        
        var popUpCellList: [PopUpCell] = []
        //======= Title
        if let titleStr = title {
            let title: PopUpCell = .text(title: titleStr, font: UIFont.spoqaHanSansBold(ofSize: 18.0))
            popUpCellList.append(title)
        }
        
        //======= attr
        let attrFullStr = description
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineBreakMode = .byCharWrapping
        
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph,
            ])
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        popUpCellList.append(attrText)
        
        //======= buttons
        let left: (title: String, color: UIColor, font: UIFont, autoClose: Bool, action: (()->Void)?)? = (leftButtonTitle == nil) ? nil : (title: leftButtonTitle!, color: isLeftButtonGray ? UIColor.gray50 : .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: leftAutoClose, action: leftAction!)
        let bottomBox: PopUpCell = .buttonBox(left: left, right: (title: rightButtonTitle, color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: rightAutoClose, action: rightAction))
        popUpCellList.append(bottomBox)
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = enableBackGroundCloseAction
        vc.cellInfo = popUpCellList
        if let _parent = parent {
            _parent.clearPresent(vc)
        }else{
            UIViewController.keyController().clearPresent(vc)
        }
    }
    
    func showNotice(title: String?, content: String?, noticeNo: String?) {
        guard let title = title,
              let content = content,
              let noticeNo = noticeNo else { return }
        
        if let cacheKey = RLUserDefault.shared.lastNoticeId {
            // 다시보지 않기했던 공지사항은 다시 띄우지 않는다
            if cacheKey == noticeNo { return }
        }

        let vc = MainNoticePopup.init(nibName: MainNoticePopup.className, bundle: nil)
        vc.input = (title: title, content: content, noticeNo: noticeNo)
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 스펙등록 팝업
    func showSpecAgree(rightAction: (()->Void)?) {
        
        //======= Title
        let title: PopUpCell = .text(title: "뱃지 인증 안내", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= image
        let image: PopUpCell = .image(named: "imgPopupSpecFlag", imageHeight: 90)
        //======= attr
        let attrFullStr = """
        로얄라운지 가입을 위해서는 1개 이상의 인증 뱃지를 등록하셔야 합니다.

        여러 개의 뱃지를 등록해서 상대에게
        어필해보세요.
        """
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: "1개 이상")
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: nil, right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, image, attrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    // MARK: - 싱글인증 팝업
    func showSingle(rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "법적 싱글이신가요?", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= image
//        let image: PopUpCell = .image(named: "icon56PxWarning", imageHeight: 56)
        //======= attr
        let attrFullStr = """
        기혼자는 가입이 엄격히 금지됩니다.

        이를 위반할 시 관련 법령에 따른
        민·형사상 고소 등 법적 조치가 취해질 수 있습니다.
        """
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: "민·형사상 고소 등 법적 조치")
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "취소", color: UIColor.gray50, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: {}), right: (title: "예, 동의합니다", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 캡처감지 팝업
    func detectCapture(rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "화면 캡처가 감지되었습니다", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= image
        let image: PopUpCell = .image(named: "icon56PxWarning", imageHeight: 56)
        //======= attr
        let attrFullStr = """
        타인의 사진 또는 정보가 포함된 사진을
        캡처하여 저장, 공유하는 것은 불법이며
        관련 법령에 근거하여 형사 처분을 받을
        수 있습니다.
        여러 건의 무단 캡처 기록은 저장되며
        수사기관 요청 시 전달될 수 있습니다.
        """
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
//        let range1 = (attrFullStr as NSString).range(of: "민·형사상 고소 등 법적 조치")
//        attributedString.addAttributes([
//            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
//            .foregroundColor: .primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: nil, right: (title: "위 내용에 동의합니다", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, image, attrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 라이브매칭 진행중 팝업
    func liveMatching(leftAction: (()->Void)?, rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "라이브 매칭이 진행중입니다", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= image
        let image: PopUpCell = .image(named: "icon56PxWarning", imageHeight: 56)
        //======= attr
        let attrFullStr = """
        나에게 호감있는 이성찾기가 진행중입니다.
        휴면/탈퇴 시 찾기가 종료되며
        사용한 크라운은 반환되지 않습니다.
        """
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
//        let range1 = (attrFullStr as NSString).range(of: "민·형사상 고소 등 법적 조치")
//        attributedString.addAttributes([
//            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
//            .foregroundColor: .primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "취소", color: .gray50, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, image, attrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 개인정보 유효기간 선택
    func showEvent(reward: String, leftAction: (()->Void)?, rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "이벤트 알림 수신 동의", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= attr
        let colorText = "약 \(reward) 상당의 무료 아이템"
        let attrFullStr = "이벤트 알림 수신 미동의 시 \(colorText)이 지급되지 않으며,\n스토어 할인 알림 등을 받을 수 없습니다.\n\n이벤트 알림 수신 동의를 하시겠습니까?"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: -0.2,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: colorText)
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "미동의", color: UIColor.dark(alpha: 0.5), font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "동의", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 개인정보 유효기간 선택
    func showAcceptTerms(leftAction: (()->Void)?, rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "개인정보 유효기간 선택", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= attr
        let attrFullStr = "1년 뒤 파기 선택 시 회원님께서 보유하신 모든 유료 결제 아이템 등이 환불없이 1년 경과 시 자동 삭제되며, 복구되지 않습니다."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: "환불없이 1년 경과 시 자동 삭제")
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "1년 뒤 파기", color: UIColor.dark(alpha: 0.5), font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "탈퇴 시 파기", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 휴대폰 번호 변경
    func showPhoneChangePopup(leftAction: (()->Void)?, rightAction: (()->Void)?) {
         
        //======= Title
        let title: PopUpCell = .text(title: "휴대폰번호 변경", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= attr
        let attrFullStr = "휴대폰번호가 변경되었나요?\n본인인증을 통해 휴대폰번호를 변경합니다."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        //======= attr2
        let attrFullStr2 = "휴대폰번호는 OK 매칭 성공 시 상대 이성에게 공개될\n수 있습니다."
        let paragraph2 = NSMutableParagraphStyle()
        paragraph2.alignment = .left
        let attributedString2 = NSMutableAttributedString(string: attrFullStr2, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 12),
            .foregroundColor: UIColor.dark.withAlphaComponent(0.5),
            .kern: 0.0,
            .paragraphStyle: paragraph2
            ])
        let attrText2: PopUpCell = .attrText(attrText: attributedString2)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "취소", color: UIColor.gray50, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "본인 인증하기", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, attrText2, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 서비스 이용정지 알림
    func showStopPopup(leftAction: (()->Void)?, rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "서비스 이용정지 알림", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        //======= attr
        let attrFullStr = "상대 회원의 신고 누적 및 서비스 이용약관 위반으로 인하여 회원님의 해당 계정 이용이 정지되었습니다."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        //======= attr2
        let attrFullStr2 = "문의사항 또는 기타 의견이 있는 경우,\n고객센터로 증빙서류와 함께 문의해주세요."
        let paragraph2 = NSMutableParagraphStyle()
        paragraph2.alignment = .left
        let attributedString2 = NSMutableAttributedString(string: attrFullStr2, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 12),
            .foregroundColor: UIColor.dark.withAlphaComponent(0.5),
            .kern: 0.0,
            .paragraphStyle: paragraph2
            ])
        let attrText2: PopUpCell = .attrText(attrText: attributedString2)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "고객센터로 문의", color: UIColor.gray50, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, attrText2, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    // MARK: - 크라운 부족
    func showMoveStore(action: (()->Void)?) {
        showNormal(title: "크라운이 부족합니다.", description: "스토어로 이동하시겠습니까?", isLeftButtonGray: true, leftButtonTitle: "닫기", rightButtonTitle: "스토어로 이동", rightAction: action)
    }
    
    func showMoveStore(needCrown: Int, reasonStr: String, action: (()->Void)?) {
        var popUpCellList: [PopUpCell] = []
        
        let title: PopUpCell = .text(title: "크라운이 부족합니다.", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        popUpCellList.append(title)
        
        //======= attr
        let crownStr = "크라운 \(needCrown)"
        let attrFullStr = "\(reasonStr)\n\(crownStr)개가 사용됩니다.\n\n스토어로 이동하시겠습니까?"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
        ])
        attributedString.addAttributes([
//            .font: UIFont.spoqaHanSansBold(ofSize: 16),
            .foregroundColor: UIColor.primary100
        ], range: (attrFullStr as NSString).range(of: crownStr))
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        popUpCellList.append(attrText)
        
        //======= buttons
        let left: (title: String, color: UIColor, font: UIFont, autoClose: Bool, action: (()->Void)?)? = (title: "닫기", color: UIColor.gray50, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: {})
        let bottomBox: PopUpCell = .buttonBox(left: left, right: (title: "스토어로 이동", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: action))
        popUpCellList.append(bottomBox)
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = popUpCellList
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 서비스 이용제한 알림
    func showSuspend(day: Int, date: String, leftAction: (()->Void)?, rightAction: (()->Void)?) {
        let title: PopUpCell = .text(title: "서비스 이용제한 알림", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        
        let attrFullStr = "상대 회원의 신고 누적으로 인하여 회원\n님의 해당 계정 이용이 \(day)일간 제한되었\n습니다."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: "\(day)일간")
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let boxAttrFullStr = "\(date)"
        let boxParagraph = NSMutableParagraphStyle()
        boxParagraph.alignment = .center
        let boxAttributedString = NSMutableAttributedString(string: boxAttrFullStr, attributes: [
            .font: UIFont.spoqaHanSansBold(ofSize: 12),
            .foregroundColor: UIColor.primary100,
            .kern: 0.0,
            .paragraphStyle: boxParagraph
            ])
        let boxAttrText: PopUpCell = .boxAttrText(attrText: boxAttributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "고객센터로 문의", color: UIColor.dark(alpha: 0.5), font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, boxAttrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 서비스 이용제한 알림
    func showOutMember(attrFullStr: String, day: Int, userId: String, rightAction: (()->Void)?) {
        let title: PopUpCell = .text(title: "탈퇴회원 알림", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: "(D-\(day))")
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let boxAttrFullStr = "\(userId)"
        let boxParagraph = NSMutableParagraphStyle()
        boxParagraph.alignment = .center
        let boxAttributedString = NSMutableAttributedString(string: boxAttrFullStr, attributes: [
            .font: UIFont.spoqaHanSansBold(ofSize: 12),
            .foregroundColor: UIColor.primary100,
            .kern: 0.0,
            .paragraphStyle: boxParagraph
            ])
        let boxAttrText: PopUpCell = .boxAttrText(attrText: boxAttributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: nil, right: (title: "계정 복구하기", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, boxAttrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 나이제한 알림
    func showAgeLimitPopup(minAge: String, maxAge: String, leftAction: (()->Void)?, rightAction: (()->Void)?) {
        let title: PopUpCell = .text(title: "사용자 계정 이용정지 알림", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        
        let ageStr = "\(minAge)세 미만 \(maxAge)세 이상"
        let attrFullStr = "로얄라운지 이용약관상 \(ageStr) 회원 서비스 제한을 하고있습니다."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range1 = (attrFullStr as NSString).range(of: ageStr)
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let boxAttrFullStr = "회원님은 로얄라운지 이용이 불가능합니다."
        let boxParagraph = NSMutableParagraphStyle()
        boxParagraph.alignment = .center
        let boxAttributedString = NSMutableAttributedString(string: boxAttrFullStr, attributes: [
            .font: UIFont.spoqaHanSansBold(ofSize: 12),
            .foregroundColor: UIColor.primary100,
            .kern: 0.0,
            .paragraphStyle: boxParagraph
            ])
        let boxAttrText: PopUpCell = .boxAttrText(attrText: boxAttributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "고객센터로 문의", color: UIColor.dark(alpha: 0.5), font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, boxAttrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }

    // MARK: - 50세 이상 회원 이용안내
    func showAgeMaxPopup(maxAge: String, leftAction: (()->Void)?, rightAction: (()->Void)?) {
        let title: PopUpCell = .text(title: "\(maxAge)세 이상 회원 이용안내", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        
        let attrFullStr = "로얄라운지 이용정책에 따라 \(maxAge)세 이상 회원 부터 서비스 제한을 하고있습니다."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let boxAttrFullStr = "회원님은 올해까지 로얄라운지\n이용이 가능합니다."
        let boxParagraph = NSMutableParagraphStyle()
        boxParagraph.alignment = .center
        let boxAttributedString = NSMutableAttributedString(string: boxAttrFullStr, attributes: [
            .font: UIFont.spoqaHanSansBold(ofSize: 12),
            .foregroundColor: UIColor.primary100,
            .kern: 0.0,
            .paragraphStyle: boxParagraph
            ])
        let boxAttrText: PopUpCell = .boxAttrText(attrText: boxAttributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: (title: "고객센터로 문의", color: UIColor.dark(alpha: 0.5), font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: leftAction), right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, boxAttrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 이미 회원
    func showAlreadyMember(email: String, rightAction: (()->Void)?) {
        let title: PopUpCell = .text(title: "회원가입 알림", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        
        let attrFullStr = "이미 회원가입 내역이 존재합니다.\n로그인 후 서비스를 이용하세요."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let attrText: PopUpCell = .attrText(attrText: attributedString)
        
        let boxAttrFullStr = email//"g**@ggg.com"
        let boxParagraph = NSMutableParagraphStyle()
        boxParagraph.alignment = .center
        let boxAttributedString = NSMutableAttributedString(string: boxAttrFullStr, attributes: [
            .font: UIFont.spoqaHanSansBold(ofSize: 12),
            .foregroundColor: UIColor.primary100,
            .kern: 0.0,
            .paragraphStyle: boxParagraph
            ])
        let boxAttrText: PopUpCell = .boxAttrText(attrText: boxAttributedString)
        
        let bottomBox: PopUpCell = .buttonBox(left: nil, right: (title: "로그인하기", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText, boxAttrText, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
    // MARK: - 외모 뱃지 승인 안내
    func showFaceSpec(rightAction: (()->Void)?) {
        //======= Title
        let title: PopUpCell = .text(title: "외모 뱃지 승인 안내", font: UIFont.spoqaHanSansBold(ofSize: 18.0))
        
        //======= attr
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        
        let attrFullStr1 = " 1.본 페이지에서는 추후 등록할 프로필  \n     사진이 본인 사진임을 증빙할 수 있는\n     증빙 서류만 올려주세요.\n       예:운전면허증, 주민등록증  \n       (단, 반드시 주민등록번호 뒷자리가 가림처리\n       되어야 합니다.)"
        let attributedString1 = NSMutableAttributedString(string: attrFullStr1, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        
        let range1 = (attrFullStr1 as NSString).range(of: "증빙 서류")
        attributedString1.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range1)
        let range1_1 = (attrFullStr1 as NSString).range(of: "1.")
        attributedString1.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 16)], range: range1_1)
        
        let paragraphSmall = NSMutableParagraphStyle()
        paragraphSmall.alignment = .left
        paragraphSmall.lineSpacing = 0.1
        
        attributedString1.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 12),
            .foregroundColor: UIColor.gray50,
            .paragraphStyle: paragraphSmall
        ], range: (attrFullStr1 as NSString).range(of: "       예:운전면허증, 주민등록증  \n       (단, 반드시 주민등록번호 뒷자리가 가림처리\n       되어야 합니다.)"))
        let attrText1: PopUpCell = .attrText(attrText: attributedString1)
        
//        let attrFullStrSub =
//        let attributedStringSub = NSMutableAttributedString(string: attrFullStrSub, attributes: [
//            .font: UIFont.spoqaHanSansRegular(ofSize: 12),
//            .foregroundColor: UIColor.gray50,
//            .kern: 0.0,
//            .paragraphStyle: paragraph
//            ])
//        let attrTextSub: PopUpCell = .attrText(attrText: attributedStringSub)
        
        //======= attr
        let attrFullStr2 = " 2. 프로필 사진은 뱃지 등록 단계 이후,  \n     프로필 사진 등록 단계에서 등록\n     하시면 됩니다."
        let attributedString2 = NSMutableAttributedString(string: attrFullStr2, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range2 = (attrFullStr2 as NSString).range(of: "프로필 사진 등록 단계에서 등록")
        attributedString2.addAttributes([
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.primary100], range: range2)
        let range2_1 = (attrFullStr2 as NSString).range(of: "2.")
        attributedString2.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 16)], range: range2_1)
        let attrText2: PopUpCell = .attrText(attrText: attributedString2)
        
        //======= attr
        let attrFullStr3 = " 3. 외모 뱃지는 신청 후 심사 승인을 보  \n     장하지 않으며, 기존 유저들의 평가\n     결과에 따라 승인 여부가 결정됩니다."
        let attributedString3 = NSMutableAttributedString(string: attrFullStr3, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0,
            .paragraphStyle: paragraph
            ])
        let range3_1 = (attrFullStr3 as NSString).range(of: "2.")
        attributedString3.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 16)], range: range3_1)
        let attrText3: PopUpCell = .attrText(attrText: attributedString3)
        
        let bottomBox: PopUpCell = .buttonBox(left: nil, right: (title: "확인", color: .primary100, font: UIFont.spoqaHanSansBold(ofSize: 16.0), autoClose: true, action: rightAction))
        
        vc = RLPopupViewController()
        vc.enableBackGroundCloseAction = true
        vc.cellInfo = [title, attrText1, attrText2, attrText3, bottomBox]
        UIViewController.keyController().clearPresent(vc)
    }
    
}
