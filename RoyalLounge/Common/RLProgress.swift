//
//  RLProgress.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Lottie

class RLProgress: NSObject {
    static let shared = RLProgress()

//    private var progressView: MainProgressView?
    private lazy var rLLoadingView = RLLoadingView()

    override init() {
        super.init()
        mainAppDelegate.window?.addSubview(self.rLLoadingView)
    }

//    func showProgress(timeout: TimeInterval = 10.0) {
//        if self.progressView == nil {
//            let progressView = MainProgressView(frame: mainAppDelegate.window!.frame)
//            self.progressView = progressView
//            mainAppDelegate.window?.addSubview(progressView)
//
//            if let window = mainAppDelegate.window {
//                window.bringSubviewToFront(progressView)
//                progressView.start(timeout: timeout)
//            }
//        } else {
//            if let window = mainAppDelegate.window, let progressView = progressView {
//                mainAsync {
//                    window.bringSubviewToFront(progressView)
//                    progressView.start(timeout: timeout)
//                }
//            }
//        }
//    }
//
//    func hideProgress() {
//        guard let progressView = self.progressView else { return }
//        progressView.stop()
//    }
    
    //커스텀 로딩
    func showCustomLoading(timeoutSec: TimeInterval = 10.0) {
        guard let window = mainAppDelegate.window else { return }
        window.bringSubviewToFront(self.rLLoadingView)
        self.rLLoadingView.start(timeout: timeoutSec)
    }

    func hideCustomLoading(delaySec: Double = 0.0, completion: (() -> Void)? = nil) {
        self.rLLoadingView.stop(delaySec: delaySec, completion: completion)
    }
}
