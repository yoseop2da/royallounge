//
//  RLUserDefault.swift
//  RoyalRounge
//
//  Created by yoseop park on 2020/01/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

private enum KeyType: String {
    case aesKey = "AESKEY"
    case baseUrlString = "BASE_URL_STRING"
    case authBaseUrlString = "AUTH_BASE_URL_STRING"
    
    case userId = "USER_ID"
    case userNoAes = "USER_NO_AES"
    
    case lastNoticeId = "LAST_NOTICE_ID"
    case lastUpdateAppVersion = "LAST_UPDATE_APP_VERSION"
    
    
    case pushId = "PUSH_ID"
    case token = "TOKEN"
    
    case isShownSpecRegisteredPopup = "IS_SHOWN_SPEC_REGISTERED_POPUP"
    
    case optionTallIdxList = "OPTION_TALL_IDX_LIST"
    case optionBodyStyleIdxList = "OPTION_BODY_STYLE_IDX_LIST"
    case optionSalaryIdxList = "OPTION_SALARY_IDX_LIST"
    case optionReligionIdxList = "OPTION_RELIGION_IDX_LIST"
    
    case optionAreaMinAge = "OPTION_AREA_MIN_AGE"
    case optionAreaMaxAge = "OPTION_AREA_MAX_AGE"
    
    case optionGeneralMinAge = "OPTION_GENERAL_MIN_AGE"
    case optionGeneralMaxAge = "OPTION_GENERAL_MAX_AGE"
    
    case optionReligionMinAge = "OPTION_RELIGION_MIN_AGE"
    case optionReligionMaxAge = "OPTION_RELIGION_MAX_AGE"
    
    case optionSalaryMinAge = "OPTION_SALARY_MIN_AGE"
    case optionSalaryMaxAge = "OPTION_SALARY_MAX_AGE"
    
    case optionStyleMinAge = "OPTION_STYLE_MIN_AGE"
    case optionStyleMaxAge = "OPTION_STYLE_MAX_AGE"
    
    case sendListRequestDate = "SEND_LIST_REQUEST_DATE"
    case receiveListRequestDate = "RECEIVE_LIST_REQUEST_DATE"
    
    case noticeBbsRequestDate = "NOTICE_BBS_REQUEST_DATE"
    case eventBbsRequestDate = "EVENT_BBS_REQUEST_DATE"
    case liveMatchViewDate = "EVENT_LIVE_MATECH_VIEW_DATE"
    
    
    
    
    
    case isFihishInappTransaction = "IS_FINISH_INAPP_TRANSACTION"
    
    var value: String { return self.rawValue }
}

class RLUserDefault: NSObject {
    static let shared = RLUserDefault()
    private let defaults = UserDefaults.standard

    var aeskey: String? {
        get { return defaults.value(forKey: KeyType.aesKey.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.aesKey.value)}
    }

    var baseUrlString: String {
        get { return defaults.value(forKey: KeyType.baseUrlString.value) as? String ?? "https://dev-api.royallounge.co.kr/" }
        set (value) { defaults.set(value, forKey: KeyType.baseUrlString.value)}
    }
    
    var authBaseUrlString: String {
        get { return defaults.value(forKey: KeyType.authBaseUrlString.value) as? String ?? "https://dev-auth.royallounge.co.kr/" }
        set (value) { defaults.set(value, forKey: KeyType.authBaseUrlString.value)}
    }
    
    var userId: String? {
        get { return defaults.value(forKey: KeyType.userId.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.userId.value)}
    }
    
    var userNoAes: String? {
        get { return defaults.value(forKey: KeyType.userNoAes.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.userNoAes.value)}
    }
    
    // 옵셔널 업데이트 캐싱
    var lastUpdateAppVersion: String? {
        get { return defaults.value(forKey: KeyType.lastUpdateAppVersion.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.lastUpdateAppVersion.value)}
    }
    
    var lastNoticeId: String? {
        get { return defaults.value(forKey: KeyType.lastNoticeId.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.lastNoticeId.value)}
    }
    
    
    var token: TokenModel? {
        get {
            if let tokenData = defaults.value(forKey: KeyType.token.value) as? Data {
                let decoder = JSONDecoder()
                if let token = try? decoder.decode(TokenModel.self, from: tokenData) {
                    return token
                }
            }
            return nil
        }
        
        set (value) {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(value) {
                defaults.set(encoded, forKey: KeyType.token.value)
            }
        }
    }
    
    var pushId: String? {
        get { return defaults.value(forKey: KeyType.pushId.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.pushId.value)}
    }
    
    var isShownSpecRegisteredPopup: Bool {
        get { return defaults.bool(forKey: KeyType.isShownSpecRegisteredPopup.value) }
        set (value) { defaults.set(value, forKey: KeyType.isShownSpecRegisteredPopup.value)}
    }
    
    var optionTallIdxList: [Int] {
        get { return defaults.value(forKey: KeyType.optionTallIdxList.value) as? [Int] ?? [0] }
        set (value) { defaults.set(value, forKey: KeyType.optionTallIdxList.value)}
    }
    
    var optionBodyStyleIdxList: [Int] {
        get { return defaults.value(forKey: KeyType.optionBodyStyleIdxList.value) as? [Int] ?? [0] }
        set (value) { defaults.set(value, forKey: KeyType.optionBodyStyleIdxList.value)}
    }
    
    var optionSalaryIdxList: [Int] {
        get { return defaults.value(forKey: KeyType.optionSalaryIdxList.value) as? [Int] ?? [0] }
        set (value) { defaults.set(value, forKey: KeyType.optionSalaryIdxList.value)}
    }
    
    var optionReligionIdxList: [Int] {
        get { return defaults.value(forKey: KeyType.optionReligionIdxList.value) as? [Int] ?? [0] }
        set (value) { defaults.set(value, forKey: KeyType.optionReligionIdxList.value)}
    }
    
    var optionAreaMinAge: Int {
        get { return defaults.value(forKey: KeyType.optionAreaMinAge.value) as? Int ?? 20 }
        set (value) { defaults.set(value, forKey: KeyType.optionAreaMinAge.value)}
    }
    var optionAreaMaxAge: Int {
        get { return defaults.value(forKey: KeyType.optionAreaMaxAge.value) as? Int ?? 49 }
        set (value) { defaults.set(value, forKey: KeyType.optionAreaMaxAge.value)}
    }
    var optionGeneralMinAge: Int {
        get { return defaults.value(forKey: KeyType.optionGeneralMinAge.value) as? Int ?? 20 }
        set (value) { defaults.set(value, forKey: KeyType.optionGeneralMinAge.value)}
    }
    var optionGeneralMaxAge: Int {
        get { return defaults.value(forKey: KeyType.optionGeneralMaxAge.value) as? Int ?? 49 }
        set (value) { defaults.set(value, forKey: KeyType.optionGeneralMaxAge.value)}
    }
    var optionReligionMinAge: Int {
        get { return defaults.value(forKey: KeyType.optionReligionMinAge.value) as? Int ?? 20 }
        set (value) { defaults.set(value, forKey: KeyType.optionReligionMinAge.value)}
    }
    var optionReligionMaxAge: Int {
        get { return defaults.value(forKey: KeyType.optionReligionMaxAge.value) as? Int ?? 49 }
        set (value) { defaults.set(value, forKey: KeyType.optionReligionMaxAge.value)}
    }
    var optionSalaryMinAge: Int {
        get { return defaults.value(forKey: KeyType.optionSalaryMinAge.value) as? Int ?? 20 }
        set (value) { defaults.set(value, forKey: KeyType.optionSalaryMinAge.value)}
    }
    var optionSalaryMaxAge: Int {
        get { return defaults.value(forKey: KeyType.optionSalaryMaxAge.value) as? Int ?? 49 }
        set (value) { defaults.set(value, forKey: KeyType.optionSalaryMaxAge.value)}
    }
    var optionStyleMinAge: Int {
        get { return defaults.value(forKey: KeyType.optionStyleMinAge.value) as? Int ?? 20 }
        set (value) { defaults.set(value, forKey: KeyType.optionStyleMinAge.value)}
    }
    var optionStyleMaxAge: Int {
        get { return defaults.value(forKey: KeyType.optionStyleMaxAge.value) as? Int ?? 49 }
        set (value) { defaults.set(value, forKey: KeyType.optionStyleMaxAge.value)}
    }
    
    var sendListRequestDate: String? {
        get { return defaults.value(forKey: KeyType.sendListRequestDate.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.sendListRequestDate.value)}
    }
    
    var receiveListRequestDate: String? {
        get { return defaults.value(forKey: KeyType.sendListRequestDate.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.sendListRequestDate.value)}
    }
    
    var noticeBbsRequestDate: String? {
        get { return defaults.value(forKey: KeyType.noticeBbsRequestDate.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.noticeBbsRequestDate.value)}
    }
    
    var eventBbsRequestDate: String? {
        get { return defaults.value(forKey: KeyType.eventBbsRequestDate.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.eventBbsRequestDate.value)}
    }
    
    var liveMatchViewDate: String? {
        get { return defaults.value(forKey: KeyType.liveMatchViewDate.value) as? String }
        set (value) { defaults.set(value, forKey: KeyType.liveMatchViewDate.value)}
    }
    
    var isFihishInappTransaction: Bool {
        get { return defaults.bool(forKey: KeyType.isFihishInappTransaction.value) }
        set (value) { defaults.set(value, forKey: KeyType.isFihishInappTransaction.value)}
    }

//    func optionMinAge(option: String) -> Int {
//        return defaults.value(forKey: "optionMinAge\(option)") as? Int ?? 20
//    }
//
//    func setOptionMinAge(value: Int, for option: String) {
//        defaults.set(value, forKey: "optionMinAge\(option)")
//    }
//
//    func optionMaxAge(option: String) -> Int {
//        return defaults.value(forKey: "optionMaxAge\(option)") as? Int ?? 49
//    }
//
//    func setOptionMaxAge(value: Int, for option: String) {
//        defaults.set(value, forKey: "optionMaxAge\(option)")
//    }

    /// 로그아웃시 캐시정보 지우기
    func removeCache() {
        userId = nil
        userNoAes = nil
        token = nil
        isShownSpecRegisteredPopup = false
        isFihishInappTransaction = false
    }
    
    func removeallKeys() {
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}
