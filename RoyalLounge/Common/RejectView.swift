//
//  RejectView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/05/11.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

class RejectView: UIView {
    var rejectMsg: String? {
        get { rejectLabel.text }
        set(value) {
            if let _value = value, let contain = value?.contains("•"), contain == true {
                let attrStr = NSMutableAttributedString.init(string: _value)
                let boldUserRange = (_value as NSString).range(of: "•")
                attrStr.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 17)], range: boldUserRange)
                rejectLabel.attributedText = attrStr
            }else{
                rejectLabel.text = value
            }
        }
    }
    
    lazy var rejectLabel = UILabel()
    
    init(rejectMsg: String, isUpArrow: Bool, textAlignment: NSTextAlignment = .left) {
        super.init(frame: .zero)
        self.setShadow(radius: 3.0, offsetX: 0.0, offsetY: 2.0, color: .lightGray)
        
        let arrow = UIImageView()
        self.addSubview(arrow)
        arrow.image = UIImage.init(named: "comPopoversTriangle\(isUpArrow ? "Up":"Down")" )
        arrow.contentMode = .scaleAspectFill

        let labelWrapView = UIView()
        labelWrapView.backgroundColor = .errerColor
        labelWrapView.clipsToBounds = true
        labelWrapView.layer.cornerRadius = 6.0
        self.addSubview(labelWrapView)
        
        labelWrapView.addSubview(rejectLabel)
        rejectLabel.textColor = .rlWhtie251
        rejectLabel.font = UIFont.spoqaHanSansBold(ofSize: 14.0)
        rejectLabel.numberOfLines = 0
        rejectLabel.text = rejectMsg
        rejectLabel.lineBreakMode = .byWordWrapping
        rejectLabel.textAlignment = textAlignment
        rejectLabel.snp.updateConstraints {
            $0.top.leading.trailing.equalToSuperview().offset(8)
            $0.trailing.equalToSuperview().offset(-8)
            $0.bottom.equalToSuperview().offset(-8)
        }
        
        if isUpArrow {
            arrow.snp.updateConstraints {
                $0.top.equalToSuperview().offset(12)
                $0.width.equalTo(16)
                $0.height.equalTo(7)
                $0.centerX.equalToSuperview()
            }
            
            labelWrapView.snp.updateConstraints {
                $0.top.equalTo(arrow.snp.bottom)
                $0.width.equalTo(258)
                $0.centerX.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
        }else{
            labelWrapView.snp.updateConstraints {
                $0.top.equalToSuperview()
                $0.width.equalTo(258)
                $0.centerX.equalToSuperview()
            }
            
            arrow.snp.updateConstraints {
                $0.top.equalTo(labelWrapView.snp.bottom)
                $0.width.equalTo(16)
                $0.height.equalTo(7)
                $0.centerX.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-12)
            }
        }
        
        self.isHidden = true
        self.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func isHidden(_ hide: Bool, animated: Bool) {
        if hide {
            self.hide(animated: animated)
        }else{
            self.show(animated: animated)
        }
    }
    private func show(animated: Bool) {
        if animated {
            self.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1.0
                self.transform = CGAffineTransform.identity
            }
        }else{
            self.isHidden = false
        }
        

    }
    
    private func hide(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0.0
                self.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
            }) { _ in
                self.isHidden = true
            }
        }else{
            self.isHidden = true
        }
    }
}
