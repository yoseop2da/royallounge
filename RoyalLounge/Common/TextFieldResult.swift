//
//  TextFieldResult.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

enum TextFieldResult {
    case ok(message: String)
//    case initial
    case empty
    case validating
    case failed(message: String)
    case failedProhibit(message: String)
}

extension TextFieldResult: CustomStringConvertible {
    var description: String {
        switch self {
        case let .ok(message): return message
        case .empty: return ""
        case .validating: return "loading...."
        case let .failed(message): return message
        case let .failedProhibit(message): return message
        }
    }
    
    var textViewTextColor: UIColor {
        switch self {
        case .ok, .validating: return .rlBlack21
        case .empty, .failed, .failedProhibit: return .errerColor
        }
    }
    
    var lineColor: UIColor {
        switch self {
        case .ok: return .successColor
        case .empty, .validating: return .dark
        case .failed, .failedProhibit: return .errerColor
        }
    }
    
    var textColor: UIColor {
        switch self {
        case .ok: return .successColor
        case .empty, .validating: return .dark
        case .failed, .failedProhibit: return .errerColor
        }
    }
    
    var isValid: Bool {
        if case .ok = self {
            return true
        }
        return false
    }
    
    var isFail: Bool {
        if case .failed(_) = self {
            return true
        }
        if case .failedProhibit(_) = self {
            return true
        }
        return false
    }
}
