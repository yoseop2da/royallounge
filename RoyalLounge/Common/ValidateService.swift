//
//  ValidateService.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class Validation {
    static let shared = Validation()
    
    private let prohibitWord = BehaviorRelay<[String]>.init(value: [])
    private let bag = DisposeBag()
    
    func validate(tall text: String) -> TextFieldResult {
//        3자릿수 작성
//        - 숫자 가능
//        - 특수문자 입력 불가
//        - 영문, 한글 변경 불가
        let minTall = 140
        let maxTall = 299
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters != 3 { return .failed(message: "")}
        let tall = Int(text)!
        if tall < minTall { return .failed(message: "가입 가능한 키 조건에 적합하지 않습니다")}
        if tall > maxTall { return .failed(message: "가입 가능한 키 조건에 적합하지 않습니다")}
        return .ok(message: "")
    }
    
    func validate(loginEmail text: String) -> TextFieldResult {
//        let minCount = 1
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
//        if numberOfCharacters < minCount { return .failed(message: "이메일은 최소 \(minCount)자리 이상이어야 합니다") }
        if text.contains("@") {
            return .ok(message: "")
        }else{
            return .failed(message: "이메일 형식이 올바르지 않습니다")
        }
        
    }
    
    func validate(email text: String) -> TextFieldResult {
//        let minCount = 5
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
//        if numberOfCharacters < minCount { return .failed(message: "이메일은 최소 \(minCount)자리 이상이어야 합니다") }

        if isValid(email: text) {
            return .ok(message: "사용 가능한 이메일 형식입니다")
        }else{
            return .failed(message: "이메일 형식이 올바르지 않습니다")
        }
    }
    
    func validate(nickname text: String) -> TextFieldResult {
        //        2자~10자 이내 작성
        //        - 한글 가능
        //        - 영문 가능
        //        - 숫자 가능
        //        - 특수문자 입력 불가
        let minCount = 2
        let maxCount = 10
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "최소 \(minCount)자리 이상이어야 합니다")}
        if numberOfCharacters > maxCount { return .failed(message: "최대 \(maxCount)자리 이하이어야 합니다")}
        return .ok(message: "")
    }
    
    func validate(interView text: String) -> TextFieldResult {
        let minCount = 1
        let maxCount = 1000
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "최소 \(minCount)자리 이상이어야 합니다")}
        if numberOfCharacters > maxCount { return .failed(message: "최대 \(maxCount)자리 이하이어야 합니다")}
        return .ok(message: "")
        }
    
    func validate(job text: String) -> TextFieldResult {
        let minCount = 2
        let maxCount = 15
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "최소 \(minCount)자리 이상이어야 합니다")}
        if numberOfCharacters > maxCount { return .failed(message: "최대 \(maxCount)자리 이하이어야 합니다")}
        return .ok(message: "")
    }
    
    func validate(optional text: String) -> TextFieldResult {
        let minCount = 1
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "추가정보는 최소 \(minCount)글자 이상이어야 합니다")}
        return .ok(message: "")
    }
    
    func validate(aboutMe text: String) -> TextFieldResult {
        let minCount = 10
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "자기소개는 최소 \(minCount)글자 이상이어야 합니다")}
        return .ok(message: "")
    }
    
    func validate(jobCareer text: String) -> TextFieldResult {
        let minCount = 10
        let maxCount = 1000
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "최소 \(minCount)글자 이상이어야 합니다")}
        if numberOfCharacters > maxCount { return .failed(message: "최대 \(maxCount)자리 이하이어야 합니다")}
        return .ok(message: "")
    }
    
    func validate(okMessage text: String, okType: OKType) -> TextFieldResult {
        let minCount = (okType == .receiveOk || okType == .receiveSuperOk) ? 5 : 10
        let maxCount = 500
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "최소 \(minCount)글자 이상이어야 합니다")}
        if numberOfCharacters > maxCount { return .failed(message: "최대 \(maxCount)자리 이하이어야 합니다")}
        return .ok(message: "")
    }
    
    
    func validate(loginPassword text: String) -> TextFieldResult {
        //        영문+숫자+특수문자 조합 8~16자리
        let minCount = 8
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: "최소 \(minCount)자리 이상이어야 합니다")}
        return .ok(message: "")
    }
    
//    func validate(currentPassword text: String) -> TextFieldResult {
//        //        영문+숫자+특수문자 조합 8~16자리
//        let minCount = 8
//        let maxCount = 16
//        let numberOfCharacters = text.trimmed.count
//        if numberOfCharacters == 0 { return .empty }
//        if numberOfCharacters < minCount { return .failed(message: "영문+숫자+특수문자 조합 8~16자 입력하세요")}
//        if numberOfCharacters > maxCount { return .failed(message: "영문+숫자+특수문자 조합 8~16자 입력하세요")}
//        if isValid(password: text) {
//            if let msg = successMessage {
//                return .ok(message: "")
//            }
//            return .ok(message: "사용 가능한 비밀번호 입니다")
//        }else{
//            return .failed(message: "영문+숫자+특수문자 조합 8~16자 입력하세요")
//        }
//    }
    
    func validate(password text: String, isEmptyMessage: Bool = false) -> TextFieldResult {
//        영문+숫자+특수문자 조합 8~16자리
        let minCount = 8
        let maxCount = 16
        let numberOfCharacters = text.trimmed.count
        if numberOfCharacters == 0 { return .empty }
        if numberOfCharacters < minCount { return .failed(message: isEmptyMessage ? "" :  "영문+숫자+특수문자 조합 8~16자 입력하세요")}
        if numberOfCharacters > maxCount { return .failed(message: isEmptyMessage ? "" : "영문+숫자+특수문자 조합 8~16자 입력하세요")}
        if isValid(password: text) {
            return .ok(message: isEmptyMessage ? "" : "사용 가능한 비밀번호 입니다")
        }else{
            return .failed(message: isEmptyMessage ? "" : "영문+숫자+특수문자 조합 8~16자 입력하세요")
        }
    }
    
    func validate(password: String, confirmPassword: String) -> TextFieldResult {
        let numberOfCharacters = confirmPassword.count
        if numberOfCharacters == 0 { return .empty }
        if confirmPassword != password { return .failed(message: "비밀번호가 일치하지 않습니다")}
        return .ok(message: "비밀번호가 일치 합니다")
    }
    
    // 자음모음이 아닌경우
    func containsEmoji(word text: String) -> Bool {
        return text.containsEmoji()
    }
    
    // 자음모음이 아닌경우
    func isOnlyKorOrNum(word text: String) -> Bool {
        let koNumRegEx = "[ㄱ-ㅎㆍㅏ-ㅣ가-힣0-9]+"
        let koNum = NSPredicate(format: "SELF MATCHES %@", koNumRegEx)
        if !koNum.evaluate(with: text) { return false }
        return true
    }
    
    // 자음모음이 아닌경우
    func isBad(word text: String) -> Bool {
        let pattern = "^[a-zA-Z0-9가-힣\\s]*$"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        let matchCounts = regex.numberOfMatches(in: text, options: [], range: NSRange(location: 0, length: text.count))
        if matchCounts > 0 { return false }
        return true
    }
    
    // 자음모음이 아닌경우
        func isValid(email text: String) -> Bool {
//            let pattern = "^[_a-zA-Z0-9-.]+@[.a-zA-Z0-9-]+\\.[a-zA-Z]+$"
            let pattern = "^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$"
            
            let regex = try! NSRegularExpression(pattern: pattern, options: [])
            let matchCounts = regex.numberOfMatches(in: text, options: [], range: NSRange(location: 0, length: text.count))
            if matchCounts > 0 { return true }
            return false
        }
    
    // 자음모음이 아닌경우
    func isValid(password text: String) -> Bool {

//        let pattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[\\.$@$!%*#?&])[A-Za-z\\d$@$\\.!%*#?&]{8,16}$"
        let pattern = "^.*(?=^.{8,16}$)(?=.*\\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()\\-_+=|\\[\\]{};:/?.><]).*$"
//        let pattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,16}$"
        
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        let matchCounts = regex.numberOfMatches(in: text, options: [], range: NSRange(location: 0, length: text.count))
        if matchCounts > 0 { return true }
        return false
    }

    func isOneStep(specNo: String) -> Bool {
        return SpecType.isOneStep(specNo: specNo)
    }
    
    func isTwoStep(specNo: String) -> Bool {
        return SpecType.isTwoStep(specNo: specNo)
    }
    
    func updateProhibitWord() {
        guard MainInformation.shared.memberStatus.value != nil else { return }
        Repository.shared.graphQl.getProhibitWordList()
            .filter{$0.count > 0}
            .subscribe(onNext: { words in
                self.prohibitWord.accept(words)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func prohibitCheck(word: String) -> TextFieldResult {
        let contain = prohibitWord.value.contains(word)
        if contain {
            return TextFieldResult.failedProhibit(message: "적절하지 않은 단어가 포함되었습니다")
        }else{
            if let _ = prohibitWord.value.first(where: { word.contains($0) }) {
                return TextFieldResult.failedProhibit(message: "적절하지 않은 단어가 포함되었습니다")
            }
            return TextFieldResult.ok(message: "")
            
        }
    }
    
}
