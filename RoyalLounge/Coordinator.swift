//
//  Coordinator.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//
//
import Foundation
import UIKit
import Firebase
import RxSwift
//

let RLStoryboard = Coordinator.shared.storyboard

enum StoryBoardType: String {
    case splash = "Splash"
    case join = "Join"
    case main = "Main"
    case todayCard = "TodayCard"
    case liveMatch = "LiveMatch"
    case myPage = "MyPage"
//    var storyBoard: UIStoryboard { return UIStoryboard(name: self.rawValue, bundle: nil) }
}

class Storyboard {
    var splash = UIStoryboard(name: StoryBoardType.splash.rawValue, bundle: nil)
    var join = UIStoryboard(name: StoryBoardType.join.rawValue, bundle: nil)
    var main = UIStoryboard(name: StoryBoardType.main.rawValue, bundle: nil)
    var todayCard = UIStoryboard(name: StoryBoardType.todayCard.rawValue, bundle: nil)
    var liveMatch = UIStoryboard(name: StoryBoardType.liveMatch.rawValue, bundle: nil)
    var myPage = UIStoryboard(name: StoryBoardType.myPage.rawValue, bundle: nil)
}

class Coordinator: NSObject {

    fileprivate enum TabType: Int {
        case firstTab = 0
        case secondTab = 1
        case thirdTab = 2
        case fourthTab = 3
        case fifthTab = 4

        var tabIndex: Int { return self.rawValue }
    }

    static let shared = Coordinator()
    fileprivate var mainTabbarController: MainTabbarViewController?
    fileprivate var mainNavigationController: UINavigationController?

    var storyboard: Storyboard = Storyboard()
    
    var isLoggedin: Bool { return mainTabbarController != nil }
    
    var navigationHeight: CGFloat { return self.mainNavigationController?.navigationBar.frame.height ?? 0.0}
    var selectedTabbarIdx: Int { return mainTabbarController?.selectedIndex ?? 0 }
    
    let bag = DisposeBag()
}

// MARK: - 메인탭바 액션
extension Coordinator {
    func moveByUserStatus(errorCallback: (()->Void)? = nil){
        guard let userNoAes = MainInformation.shared.userNoAes else { return }
        Repository.shared.graphQl
            .getMemberStatus(userNo: userNoAes)
            .subscribe(onNext: { memberStatus in
                MainInformation.shared.memberStatus.accept(memberStatus)
                if let pushId = RLUserDefault.shared.pushId {
                    Repository.shared.graphQl
                        .updateDevice(userNo: userNoAes, pushId: pushId)
                }
                if let _memberStatus = memberStatus {
                    // 탈퇴 거르기
                    if _memberStatus.status == .out { errorCallback?(); return }
                    // 유저 상태에 맞춰 화면 이동
                    self.moveBy(memberStatus: _memberStatus)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                errorCallback?()
            })
            .disposed(by: bag)
    }
    
    private func moveBy(memberStatus: MemberStatus) {
        // TESTCODE
//        Coordinator.shared.toJoinStep2(gender: .w)
//        Coordinator.shared.toJoinStep3()
//        Coordinator.shared.toJoinStep4()
//        Coordinator.shared.toJoinStep5()
//        Coordinator.shared.toReady()
//        Coordinator.shared.toReject()
//        Coordinator.shared.toMain()
//        return
        
        switch memberStatus.status {
            case .join:
                if memberStatus.step == 2 {
                    Coordinator.shared.toJoinStep2(gender: memberStatus.gender)
                } else if memberStatus.step == 3 {
                    Coordinator.shared.toJoinStep3()
                } else if memberStatus.step == 4 {
                    Coordinator.shared.toJoinStep4()
                } else if memberStatus.step == 5 {
                    Coordinator.shared.toJoinStep5()
                } else {
                    // 해당 케이스는 없는 케이스 예외처리!!
//                    Coordinator.shared.toSplash()
                }
        case .ready, .photoEval, .photoEvalEnd:
            Coordinator.shared.toReady()
        case .reject:
            Coordinator.shared.toReject()
        case .normal:
//            Repository.shared.main.attend(userNo: MainInformation.shared.userNoAes!)
            Coordinator.shared.toMain()
        case .sleep:
            // 메인 + 휴면 신청상태.
            Coordinator.shared.toSleepMain(didFinishCallback: {
//                APIManager.shared.attention_att_001()
//                mainInformation.addDevice()
            })
        case .out:
            Coordinator.shared.toSplash()
        default:
            // block, normalReady, normalReject, out, readyHold, report, sleep, suspend
            #if DEBUG
            toast(message: "회원상태 [\(memberStatus.status.rawValue)]에 대해서는 아직 개발되어있지 않습니다.", seconds: 3.0)
            #endif
            
            Coordinator.shared.toSplash()
//            alertInstantDialog(title: "[회원등급 \(status.rawValue)]", message: "아직 개발중입니다....", seconds: 2.0) {
//                Coordinator.shared.toMain()
//            }
        }
    }
//            case .ready, .reject:
//                // join, 5 단계에서 -> ready, reject 가입심사 반려.
//                Coordinator.shared.toJoinStep5()
//            case .normalReject, .report, .normalReady:
//                // Normal -> ready, reject, report 정상회원 리젝, 신고누적 회원, 정상회원 심사대기
//                Coordinator.shared.toNormalChecking()
//            case .normal:
//                // 메인으로 바로 이동
//                Coordinator.shared.toMain(didFinishCallback: {
//                    // NON_MEMBER 구독해제
//                    Messaging.messaging().unsubscribe(fromTopic: "NON_MEMBER") { error in
//                        print("unSubscribed to NON_MEMBER topic")
//                    }
//                    APIManager.shared.attention_att_001()
////                    mainInformation.addDevice()
//    if let userNoAes = MainInformation.shared.userNoAes,
//        let pushId = RLUserDefault.shared.pushId{
//        Repository.shared.graphQl
    //.updateDevice(userNo: userNoAes, pushId: pushId)
//    }
//                })
//            case .sleep:
//                // 메인 + 휴면 신청상태.
//                Coordinator.shared.toSleepMain(didFinishCallback: {
//                    APIManager.shared.attention_att_001()
////                    mainInformation.addDevice()
//    if let userNoAes = MainInformation.shared.userNoAes,
//        let pushId = RLUserDefault.shared.pushId{
//        Repository.shared.graphQl
   // .updateDevice(userNo: userNoAes, pushId: pushId)
//    }
//                })
//            default:
//                // 불필요한 로그아웃 제거 필요성이 있음.
//                Coordinator.shared.toSignUpMain()
//    //        case .out:
//    //            // admin에서 변경한 경우 이동시켜주기.
//    //            Coordinator.shared.toSignUpMain()
//    //        case .suspend:
//    //            // 로그인시 팝업(2.0_4)
//    //            Coordinator.shared.toSignUpMain()
//    //        case .block:
//    //            // 로그인시 팝업(2.0_5)
//    //            Coordinator.shared.toSignUpMain()
//            }
//        }
}

// MARK: mainRoot
extension Coordinator {

    /*
     스플래시 & 메인로그인
     */
    func toSplash() {
        mainAsync {
            let vc = self.storyboard.splash.splashViewController()
            self.mainTabbarController = nil
            self.toMainRootNaviViewController(vc, navigationHidden: true)
        }
    }

    /*
     시스템공지사항 진입
     */
    func systemNotice(content: String, didFinishCallback: (()->Void)? = nil) {
        mainAsync {
            let vc = self.storyboard.main.systemNoticeViewController(content: content)
            self.toMainRootNaviViewController(vc, navigationHidden: true, didFinishCallback: didFinishCallback)
        }
    }
    
    /*
     메인페이지 진입
     */
    func toMain(didFinishCallback: (()->Void)? = nil) {
        mainAsync {
            let vc = self.storyboard.main.mainTaBbarViewController()
            self.mainTabbarController = vc
//            vc.isSleep = false
            self.toMainRootNaviViewController(vc, navigationHidden: true, didFinishCallback: didFinishCallback)
        }
    }

    func toSleepMain(didFinishCallback: (()->Void)? = nil) {
        mainAsync {
            let vc = self.storyboard.main.sleepViewController()
//            self.mainTabbarController = vc
//            vc.isSleep = true
            self.toMainRootNaviViewController(vc, navigationHidden: true, didFinishCallback: didFinishCallback)
        }
    }

    /*
     회원가입 2단계
     */
    func toJoinStep2(gender: GenderType) {
        mainAsync {
            if gender == .m {
                let vc = RLStoryboard.join.specListViewController(isEditMode: false, backType: .empty)
                self.mainTabbarController = nil
                self.toMainRootNaviViewController(vc, navigationBottomLineHidden: true, didFinishCallback: {

                })
            }else{
                let vc = RLStoryboard.join.specWomanViewController(isEditMode: false)
                self.mainTabbarController = nil
                self.toMainRootNaviViewController(vc, navigationBottomLineHidden: true, didFinishCallback: {

                })
            }
        }
    }

    /*
     회원가입 3단계
     */
    func toJoinStep3() {
        mainAsync {
            let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .nickname, isEditMode: false)
            self.mainTabbarController = nil
            self.toMainRootNaviViewController(vc, navigationBottomLineHidden: true, didFinishCallback: {

            })
        }
    }

    /*
     회원가입 4단계
     */
    func toJoinStep4() {
        mainAsync {
            let vc = RLStoryboard.join.interviewViewController(interViewSeq: 1, isEditMode: false)
            self.mainTabbarController = nil
            self.toMainRootNaviViewController(vc, navigationBottomLineHidden: true, didFinishCallback: {

            })
        }
    }

    /*
     회원가입 5단계
     */
    func toJoinStep5() {
        mainAsync {
            let vc = self.storyboard.join.photoViewController(isEditMode: false)
            self.mainTabbarController = nil
            self.toMainRootNaviViewController(vc, navigationBottomLineHidden: true, didFinishCallback: {

            })
        }
    }
    
    /*
     심사중
     */
    func toReady() {
        mainAsync {
            let vc = self.storyboard.join.readyViewController()
            self.mainTabbarController = nil
            self.toMainRootNaviViewController(vc, navigationHidden: true, navigationBottomLineHidden: true, didFinishCallback: nil)
        }
    }
    
    /*
     심사 리젝
     */
    func toReject() {
        mainAsync {
            let vc = self.storyboard.join.rejectViewController()
            self.mainTabbarController = nil
            self.toMainRootNaviViewController(vc, navigationHidden: true, navigationBottomLineHidden: true, didFinishCallback: nil)
        }
    }
}

// Tab이동
extension Coordinator {
    func toFirstTab() {
        self.toTab(.firstTab)
//        cardType: CardType? = nil
//        self.mainTabbarController?.cardMainGetData(cardType: cardType)
    }
    
    func toSecondTab(idx: Int/*0: 받은, 1: 보낸, 2:성공*/ = 0) {
        toTab(.secondTab)
        delay(0.3) {
            self.mainTabbarController?.moveTapOnCardHistoryViewController(idx: idx)
        }
        
    }

    func toThirdTab() {
        toTab(.thirdTab)
    }

//    func toFourthTab(roomNo: String? = nil) {
//        self.mainTabbarController?.refreshChatRoomList()
//        toTab(.fourthTab)
//        if let _roomNo = roomNo {
//            delay(0.5) {
//                self.mainTabbarController?.toChatRoom(roomNo: _roomNo)
//            }
//        }
//    }

//    func toFifthTab(toMyScorePage: Bool = false) {
//        toTab(.fifthTab)
//        if toMyScorePage {
//            delay(0.5) {
//                let vc = self.storyboard.myPage.myScoreViewController()
//                vc.hideNavigationControllerBottomLine()
//                UIViewController.keyController().push(vc)
//            }
//        }
//    }
}

extension Coordinator {
    // 각종 화면 이동
//    func toCardDetail(parent: UIViewController,
//                       viewType: CardDetailViewController.ViewType,
//                       cardUserNoAes: String?,
//                       cardMatchNoAes: String?,
//                       cardDetailWillDisappear: ((_ refresh: Bool) -> Void)?) {
//        let vc = self.storyboard.mainCard.cardDetailViewController()
//        vc.viewType = viewType
//        vc.cardUserNoAes = cardUserNoAes
//        vc.cardMatchNoAes = cardMatchNoAes
//        vc.cardDetailWillDisappear = cardDetailWillDisappear
//        parent.navigationController?.pushViewController(vc, animated: true)
//    }
}

fileprivate extension Coordinator {
    /*
     Tab 이동
     */
    func toTab(_ tab: TabType) {
        guard let tabBarController = mainTabbarController, tabBarController.selectedIndex != tab.tabIndex else { return }
        UIView.transition(with: tabBarController.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            tabBarController.selectedIndex = tab.tabIndex
        }, completion: { completed in
            // maybe do something here
        })
    }

    /*
     메인 RootViewController + Navigation
     */
    func toMainRootNaviViewController(_ vc: UIViewController, navigationHidden: Bool = false, navigationBottomLineHidden: Bool = false, didFinishCallback: (()->Void)? = nil) {
        guard let mainWindow = mainAppDelegate.window else { return }
        let navigationController = UINavigationController(rootViewController: vc)
        
        
        self.mainNavigationController = navigationController
        navigationController.isNavigationBarHidden = navigationHidden
//        navigationController.setNavigationBarHidden(navigationHidden, animated: true)
//        mainWindow.rootViewController = navigationController

        if navigationBottomLineHidden {
            vc.hideNavigationControllerBottomLine()
        }
        delay(0.25) {
            didFinishCallback?()
        }

//        mainWindow.rootViewController = navigationController
//        didFinishCallback?()
        
//        UIView.transition(with: mainWindow, duration: 0.5, options: .transitionCrossDissolve, animations: {
//            mainWindow.rootViewController = navigationController
//        }, completion: { completed in
//            didFinishCallback?()
//        })
        
        // 이게 더 괜찮음
//        let snapShot = mainWindow.snapshotView(afterScreenUpdates: true)!
//        navigationController.viewControllers[0].view.addSubview(snapShot)
//        mainWindow.rootViewController = navigationController
//
//        UIView.animate(withDuration: 0.5, animations: {
//            snapShot.layer.opacity = 0
//        }) { _ in
//            snapShot.removeFromSuperview()
//        }
        
        let disAppearView = mainWindow.rootViewController?.view
        let transition = CATransition()
        transition.startProgress = 0
        transition.endProgress = 1.0
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.duration = 0.25
        disAppearView?.layer.add(transition, forKey: "transition")
        mainWindow.layer.add(transition, forKey: "transition")
        mainWindow.rootViewController = navigationController
    }

    /*
     메인 RootViewController
     */
    func toMainRootViewController(_ vc: UIViewController) {
        guard let mainWindow = mainAppDelegate.window else { return }
        mainWindow.rootViewController = vc
    }
}


