//
//  ArrayExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
