//
//  ErrorExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/30.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension Error {
    func printLog(position: String) {
        if let custom = self as? CustomError {
            print("😱😱😱😱😱😱😱😱 error [\(position)] : \(custom.errorMessage)")
        }else{
            print("😱😱😱😱😱😱😱😱 error [\(position)] : \(localizedDescription)")
        }
    }
}


