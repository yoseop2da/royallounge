//
//  IntExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/06.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

extension Int {
    func canPayable() -> Bool {
        return MainInformation.shared.canPayable(toPayCrown: self)
    }
}
