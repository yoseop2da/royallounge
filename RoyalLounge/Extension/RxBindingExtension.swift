//
//  RxBindingExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/19.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


extension Reactive where Base: UIView {
    var setResult: Binder<TextFieldResult> {
        return Binder(base) { label, result in
            label.backgroundColor = result.lineColor
        }
    }
}

extension Reactive where Base: UITextView {
    var setTextViewProhibitResult: Binder<TextFieldResult> {
        return Binder(base) { textView, result in
            switch result {
            case .failed:
                textView.textColor = .rlBlack21
            case .failedProhibit:
                textView.textColor = result.textViewTextColor
            default: textView.textColor = .rlBlack21
            }
        }
    }
}


extension Reactive where Base: UILabel {
    var setResult: Binder<TextFieldResult> {
        return Binder(base) { label, result in
            label.text = result.description
            label.textColor = result.lineColor
        }
    }
}

extension Reactive where Base: RLNoTitleTextField {
    var setResult: Binder<TextFieldResult> {
        return Binder(base) { textField, result in
            guard textField.textField.isFirstResponder else { return }
            textField.bottomLine.backgroundColor = result.lineColor
            textField.bottomMsg.text = result.description
            textField.bottomMsg.textColor = result.lineColor
        
        }
    }
    
    var setProfileTextResult: Binder<TextFieldResult> {
        return Binder(base) { textField, result in
            textField.bottomMsg.text = result.description
            textField.bottomMsg.textColor = result.lineColor
            
            switch result {
            case .ok, .empty, .validating:
                guard textField.textField.isFirstResponder else { return }
                textField.bottomLine.backgroundColor = .dark
            case .failed, .failedProhibit: textField.bottomLine.backgroundColor = .errerColor
            }          
        }
    }
    
    var setOptionalTextResult: Binder<TextFieldResult> {
        return Binder(base) { textField, result in
            guard textField.textField.isFirstResponder else { return }
            textField.bottomMsg.text = result.description
            textField.bottomMsg.textColor = result.lineColor
            switch result {
            case .ok, .empty, .validating: textField.bottomLine.backgroundColor = .dark
            case .failed, .failedProhibit: textField.bottomLine.backgroundColor = .errerColor
            }
        }
    }
}

extension Reactive where Base: RLTitleTextField {
    var setResult: Binder<TextFieldResult> {
        return Binder(base) { textField, result in
            guard textField.textField.isFirstResponder else { return }
            textField.bottomLine.backgroundColor = result.lineColor
            textField.bottomMsg.text = result.description
            textField.bottomMsg.textColor = result.lineColor
        
        }
    }
    
    var setResultRegardlessOfFocus: Binder<TextFieldResult> {
        return Binder(base) { textField, result in
            textField.bottomLine.backgroundColor = result.lineColor
            textField.bottomMsg.text = result.description
            textField.bottomMsg.textColor = result.lineColor
        }
    }
    
    var setResultPassword: Binder<TextFieldResult> {
        // 포커싱이 안된경우
        // fail만 에러처리해주고
        // ok,empty에 대해서는 초기화해주기
        return Binder(base) { textField, result in
            if textField.textField.isFirstResponder {
                textField.bottomLine.backgroundColor = result.lineColor
                textField.bottomMsg.text = result.description
                textField.bottomMsg.textColor = result.lineColor
            }else{
                switch result {
                case .ok:
                    textField.bottomLine.backgroundColor = .gray50Alpha40
                    textField.bottomMsg.text = ""
                    textField.bottomMsg.textColor = .gray50Alpha40
                case .empty, .validating:
                    textField.bottomLine.backgroundColor = .gray50Alpha40
                    textField.bottomMsg.text = ""
                    textField.bottomMsg.textColor = .gray50Alpha40
                case .failed, .failedProhibit:
                    textField.bottomLine.backgroundColor = result.lineColor
                    textField.bottomMsg.text = result.description
                    textField.bottomMsg.textColor = result.lineColor
                }
            }
            
        }
    }
}

extension Reactive where Base: UIButton {
    var mainButtonEnableState: Binder<Bool> {
        return Binder(base) { button, enable in
            button.isEnabled = enable
            button.isUserInteractionEnabled = enable
            button.backgroundColor = enable ? .primary100 : .gray10
            let color = enable ? UIColor.white : .dark(alpha: 0.1)
            button.setTitleColor(color, for: .normal)
//            button.alpha = enable ? 1.0 : 0.3
        }
    }
}

extension Reactive where Base: UIButton {
    var acceptTermsAgreeAll: Binder<Bool> {
        return Binder(base) { button, isSelected in
            button.isSelected = isSelected
//            button.backgroundColor = isSelected ? .primary25 : UIColor.white
//            button.setBorder(color: isSelected ? .primary25 : UIColor.gray10)
            button.setTitleColor(isSelected ? .primary100 : .gray75, for: .normal)
            button.setImage(isSelected ? UIImage.init(named: "icon16PxCheck") : nil, for: .normal)
        }
    }
}

extension Reactive where Base: UIButton {
    var buttonEnableState: Binder<Bool> {
        return Binder(base) { button, enable in
            button.isEnabled = enable
            button.isUserInteractionEnabled = enable
            button.backgroundColor = enable ? .primary100 : .gray10
            button.setTitleColor(enable ? .white : .gray25, for: .normal)
//            button.alpha = enable ? 1.0 : 0.3
        }
    }
}

extension Reactive where Base: RLMailSender {
    var sendSMS: Binder<String> {
        return Binder(base) { mailSender, message in
            mailSender.sendSMS(bodyText: message)
        }
    }
}
