//
//  RxFunction.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/05/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxSwift
import RxCocoa

extension ObservableType {
    public func rxTouchAnimation(button: UIView, throttleDuration: RxSwift.RxTimeInterval = .milliseconds(300), scheduler: RxSwift.SchedulerType) -> RxSwift.Observable<Self.Element> {
        return self
            .throttle(throttleDuration, latest: false, scheduler: MainScheduler.instance)
            .do(onNext: { _ in button.touchAnimation() })
            .delay(.milliseconds(150), scheduler: scheduler)
    }
}

