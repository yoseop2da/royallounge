//
//  UIButtonExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

extension UIButton {
    public func underLine(_ underLineText: String? = nil, underLineColor: UIColor? = nil) {
        var text: String?
        var color: UIColor?
        
        if let _text = underLineText {
            text = _text
        }else{
            if let _text = self.titleLabel?.text {
                text = _text
            }
        }
        
        if let _color = underLineColor {
            color = _color
        }else{
            if let _color = self.titleLabel?.textColor {
                color = _color
            }
        }
        
        guard let _underLineText = text else { return }
        let attrString = NSAttributedString(string: _underLineText, attributes: [
            NSAttributedString.Key.underlineStyle: NSNumber(value: NSUnderlineStyle.single.rawValue),
            NSAttributedString.Key.foregroundColor: color!,
            NSAttributedString.Key.underlineColor: color!
        ])
        self.setAttributedTitle(attrString, for: .normal)
    }
    
    public func enableState(isEnable: Bool) {
        isEnabled = isEnable
        isUserInteractionEnabled = isEnable
//        backgroundColor = isEnable ? .primary100 : .gray10
        setTitleColor(isEnable ? .primary100 : .gray10, for: .normal)
    }
}
