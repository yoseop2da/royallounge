
//
//  UICollectionViewExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension UICollectionView {
    func dequeueReusable<T: UICollectionViewCell>(for indexPath: IndexPath) -> T{
        return dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as! T
    }
}
