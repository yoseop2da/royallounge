//
//  UIFontExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension UIFont {
    /*
     기본 폰트 정의
     */
    static func spoqaHanSansRegular(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SpoqaHanSans-Regular", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }

    static func spoqaHanSansThin(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SpoqaHanSans-Thin", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }

    static func spoqaHanSansBold(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SpoqaHanSans-Bold", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }

    static func spoqaHanSansLight(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SpoqaHanSans-Light", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
}
