//
//  UIImageViewExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {

    func setImage(urlString: String, placeholderImage: UIImage? = nil, progress: @escaping (_ percentage: CGFloat) -> Void = { _ in }, completed: @escaping (UIImage?) -> Void  = { _ in }) {

        if let url = URL.init(string: urlString) {
            let splitArray = url.absoluteString.components(separatedBy: "&Expires")
            let cacheKey = splitArray.first ?? url.absoluteString
            
            if let image = SDImageCache.shared().imageFromDiskCache(forKey: cacheKey) {
                self.image = image
                completed(image)
            } else {
                self.sd_setImage(with: url, placeholderImage: placeholderImage, progress: { (currentSize, totalSize, _) in
                    let percentage = CGFloat(100.0) * CGFloat(currentSize) / CGFloat(totalSize)
                    progress(percentage)
                }, completed: { (image, error, type, url) in
                    // 캐시키에 데이터 새로 저장.
                    //                    SDImageCache.shared().store(image, imageData: image?.pngData(), forKey: cacheKey, toDisk: true, completion: nil)
                    // 저장된 캐시키만 변경.
                    SDWebImageManager.shared().cacheKeyFilter = { url in
                        let str = url?.absoluteString
                        let splitArray = str?.components(separatedBy: "&Expires")
                        let cacheKey = splitArray?.first ?? str
                        return cacheKey
                    }
                    completed(image)
                })
            }
        } else {
            self.image = placeholderImage
            progress(100)
            completed(placeholderImage)
        }
        
//        self.sd_setImage(with: URL(string: urlString), placeholderImage: placeholderImage, progress: { (currentSize, totalSize, _) in
//            let percentage = 100 * Double(currentSize) / Double(totalSize)
//            progress(CGFloat(percentage))
//        }, completed: { (image, error, type, url) in
//            // 캐시키에 데이터 새로 저장.
//            //                    SDImageCache.shared().store(image, imageData: image?.pngData(), forKey: cacheKey, toDisk: true, completion: nil)
//            // 저장된 캐시키만 변경.
////            SDWebImageManager.shared().cacheKeyFilter = { url in
////                let str = url?.absoluteString
////                let splitArray = str?.components(separatedBy: "&Expires")
////                let cacheKey = splitArray?.first ?? str
////                return cacheKey
////            }
//            completed(image)
//        })
    }
    
    func setBlurImage(urlString: String, placeholderImage: UIImage? = nil, progress: @escaping (_ percentage: CGFloat) -> Void = { _ in }, completed: @escaping (UIImage?) -> Void  = { _ in }) {
        if let url = URL.init(string: urlString) {
            let splitArray = url.absoluteString.components(separatedBy: "&Expires")
                let cacheKey = splitArray.first ?? url.absoluteString
                if let image = SDImageCache.shared().imageFromDiskCache(forKey: "\(cacheKey)_BLUR") {
                    print("------------- 블러처리 캐시 데이터")
                    self.image = image
                    completed(image)
                } else {
                    self.sd_setImage(with: url, placeholderImage: placeholderImage, progress: { (currentSize, totalSize, _) in
                        let percentage = CGFloat(100.0) * CGFloat(currentSize) / CGFloat(totalSize)
                        progress(percentage)
                    }, completed: { (image, error, type, url) in
                        // 캐시키에 데이터 새로 저장.
                        let blurImg = image?.toBlur(value: 50.0)//
                        self.image = blurImg
                        SDImageCache.shared().store(image, imageData: blurImg?.pngData(), forKey: "\(cacheKey)_BLUR", toDisk: true, completion: nil)
                        print("------------- 블러처리 캐시 데이터 저장 [\(cacheKey)_BLUR]")
                        // 저장된 캐시키만 변경.
                        SDWebImageManager.shared().cacheKeyFilter = { url in
                            let str = url?.absoluteString
                            let splitArray = str?.components(separatedBy: "&Expires")
                            let cacheKey = splitArray?.first ?? str
                            return cacheKey
                        }
                        completed(blurImg)
                    })
                }
            } else {
                self.image = placeholderImage
                progress(100)
                completed(placeholderImage)
            }
    }
}
