//
//  UIScreenExtesnion.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/24.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension UIScreen {
    /*
     414x736 8+
     375x667 8s
     320x568 SE
     320x480 4s iOS9.x
     */
    public static var width = UIScreen.main.bounds.size.width
    public static var height = UIScreen.main.bounds.size.height
    public static var iPhone4: Bool { return UIScreen.height <= 480 }
    public static var underSE: Bool { return UIScreen.width < 375 }
    public static var overIPhone8: Bool { return UIScreen.width >= 375 }
    public static var iPhone8: Bool { return UIScreen.width == 375 }
    public static var iPhone8Plus: Bool { return UIScreen.width == 414 }
    public static var iphoneX: Bool { return UIScreen.main.nativeBounds.height == 2436 }
}

extension UIScreen {
    public static var iPhoneXSMax: Bool { return UIScreen.height == 2688 }
    public static var iPhoneXR: Bool { return UIScreen.height == 1792 }
}

extension UIScreen {
    public static var statusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }

    public static var safeAreaTop: CGFloat {
        if #available(iOS 11.0, *) { return UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0 }
        return 0.0
    }

    public static var safeAreaBottom: CGFloat {
        if #available(iOS 11.0, *) { return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 }
        return 0.0
    }

    public static var hasNotch: Bool {
        if #available(iOS 11.0, *) { return (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) > 0 }
        return false
    }
}
