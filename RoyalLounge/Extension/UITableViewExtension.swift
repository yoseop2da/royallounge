
//
//  UITableViewExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueReusable<T: UITableViewCell>(for indexPath: IndexPath) -> T{
        return dequeueReusableCell(withIdentifier: T.className, for: indexPath) as! T
    }
}
