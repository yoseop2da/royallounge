//
//  UIViewControllerExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension UIViewController {

    static var className: String {
        return String(describing: self)
    }
    func removeBackTitle() {
        // 처음 진입시 이전 스택의 타이틀 제거( 연결되어서 백버튼까지 가지고 오는 경우를 막아줌? )
        // back
        self.navigationController?.navigationBar.backItem?.title = ""

        // back 버튼 제거
        self.navigationController?.title = ""

        // back 버튼 제거
        self.tabBarController?.title = ""

        let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtn
    }

    func hideNavigationControllerBottomLine() {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage(named: "")
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }

    func showNavigationControllerBottomLine() {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage(named: "")
        self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
    }

//    // 일반 화면 띄우기
//    func modal(_ viewController: UIViewController, presentationStyle: UIModalPresentationStyle = .fullScreen, animated: Bool = false, completion: (() -> Void)? = nil) {
//        self.modalPresentationStyle = presentationStyle
//        self.present(viewController, animated: animated, completion: nil)
//    }
//
//    // 투명하게 화면 띄우기
//    func clearPresent(_ viewController: UIViewController, animated: Bool = false, completion: (() -> Void)? = nil) {
//        self.modalPresentationStyle = .overCurrentContext
//        self.modalTransitionStyle = .crossDissolve
//        self.present(viewController, animated: animated, completion: nil)
//    }
    // 일반 화면 띄우기
    func modal(_ viewController: UIViewController, presentationStyle: UIModalPresentationStyle = .fullScreen, animated: Bool = false, completion: (() -> Void)? = nil) {
        viewController.modalPresentationStyle = presentationStyle
        self.present(viewController, animated: animated, completion: nil)
    }

    // 투명하게 화면 띄우기
    func clearPresent(_ viewController: UIViewController, animated: Bool = false, completion: (() -> Void)? = nil) {
        viewController.presentOption(style: .overCurrentContext)
        self.present(viewController, animated: animated, completion: nil)
    }

    fileprivate func presentOption(style: UIModalPresentationStyle) {
        self.modalPresentationStyle = style
        self.modalTransitionStyle = .crossDissolve
    }


    // 푸시하면서 상단 타이틀 제거해줌
    // viewwillappear에 타이틀 설정하는 코드를 넣어줘야함.
    // ex> self.tabBarController?.title = "상세보기"
    func push(_ viewController: UIViewController, navigationBarHidden: Bool = false, animated: Bool = true) {
        self.navigationController?.pushViewController(viewController, animated: animated)
//        self.navigationController?.isNavigationBarHidden = navigationBarHidden
        self.navigationController?.setNavigationBarHidden(navigationBarHidden, animated: true)
        self.tabBarController?.title = ""
    }
    
    func pushBack(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func pushBackTo(viewControllerClass: AnyClass, animated: Bool = true) {
        if let vc = self.navigationController!.viewControllers.filter({ $0.isKind(of: viewControllerClass) }).first {
            self.navigationController?.popToViewController(vc, animated: animated)
        }
    }
    
    
    /// 원하는 뎁스만큼 뒤로가기
    /// - Parameters:
    ///   - depth: 1이 1페이지 뒤로
    ///   - animated: 에니메이션 포함 여부
    func pushBackTo(depth: Int, animated: Bool = true) {
        guard let vcs = self.navigationController?.viewControllers else { return }
        let vc = vcs[vcs.count - (depth+1)]
        
        self.navigationController?.popToViewController(vc, animated: animated)
    }

    class func keyController() -> UIViewController {
        let window = UIApplication.shared.delegate!.window!!
        let vc = window.rootViewController!
        if let navVC = vc as? UINavigationController, let visibleVC = navVC.visibleViewController {
            return visibleVC
        } else {
            if let tabVC = vc as? UITabBarController, let selectedVC = tabVC.selectedViewController {
                return selectedVC
            }
        }
        if let presented = vc.presentedViewController {
            presented.removeFromParent()
        }
        return vc
    }
}
