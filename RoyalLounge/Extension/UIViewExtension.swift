//
//  UIViewExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

extension UIView {
    static var className: String {
        return String(describing: self)
    }

    func removeAllSubView() {
        self.subviews.forEach { $0.removeFromSuperview() }
    }

    public func removeShadow() {
        self.layer.shadowRadius = 0.0
        self.layer.shadowColor = UIColor.clear.cgColor
    }

    public func setShadow(radius: CGFloat, offsetX: Double = 0.0, offsetY: Double = 0.0, opacity: Double = 0.7, color: UIColor) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize.init(width: CGFloat(offsetX), height: CGFloat(offsetY))
        self.layer.shadowOpacity = Float(opacity)
        self.layer.shadowRadius = radius
    }

    public func sectionBottomShadow() {
        self.setShadow(radius: 1.0, offsetY: 1.0, opacity: 0.6, color: UIColor.lightGray.withAlphaComponent(0.7))
    }

    public func setRTShadow(radius: CGFloat, color: UIColor = UIColor.gray) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = radius
    }

    func removeAllGradientLayer() {
        self.layer.sublayers?.forEach {
            if let layer = $0 as? CAGradientLayer { layer.removeFromSuperlayer() }
        }
    }

    func setBorder(width: CGFloat = 1.0, color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }

    // [  ]
    func setHighScoreGradient(start: UIColor, end: UIColor) {
        setGradient(colors: [start, end, end, start], startPoint: CGPoint.init(x: 0.0, y: 0.5), endPoint: CGPoint.init(x: 1.0, y: 0.5), location: [0.05, 0.2, 0.8, 0.95])
    }
    
    func setSuperOKCardGradient() {
        let colors = [UIColor.black.withAlphaComponent(0.0),
                      UIColor.black.withAlphaComponent(0.15),
                      UIColor.black.withAlphaComponent(0.4)]
        setGradient(colors: colors, startPoint: CGPoint.init(x: 0.5, y: 0.0), endPoint: CGPoint.init(x: 0.5, y: 1.0), location: [0.0, 0.45, 1.0])
    }
    
    func setHistoryResultGradient() {
        let colors = [UIColor.rlBrown223, UIColor.rlBrown238]
        setGradient(colors: colors, startPoint: CGPoint.init(x: 0.5, y: 0.0), endPoint: CGPoint.init(x: 0.5, y: 1.0), location: [0.0, 1.0])
    }
    
    func setGradient(colors: [UIColor], startPoint: CGPoint = CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint = CGPoint(x: 1.0, y: 0.5), location: [NSNumber]? = [0.0, 1.0]) {

        removeAllGradientLayer()

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors.map{$0.cgColor}
        gradientLayer.borderColor = layer.borderColor
        gradientLayer.borderWidth = layer.borderWidth
        gradientLayer.cornerRadius = layer.cornerRadius

        /*
         0.0, 0.0 -- 0.5, 0.0 -- 1.0, 0.0
            |           |           |
         0.0, 0.5 -- 0.5, 0.5 -- 1.0, 0.5
            |           |           |
         0.0, 1.0 -- 0.5, 1.0 -- 1.0, 1.0
         */
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        gradientLayer.locations = location

        layer.insertSublayer(gradientLayer, at: 0)
    }

    func addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat) {
        self.layoutIfNeeded()
        let strokeColor = strokeColor.cgColor

        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2.0, y: frameSize.height/2.0)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round

        shapeLayer.lineDashPattern = [4, 2] // adjust to your liking
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: shapeRect.width, height: shapeRect.height), cornerRadius: 16).cgPath

        self.layer.addSublayer(shapeLayer)
    }

    func removeAllDashLayer() {
        self.layer.sublayers?.forEach {
            if let layer = $0 as? CAShapeLayer { layer.removeFromSuperlayer() }
        }
    }
}
extension UIView {
    private static let kRotationAnimationKey = "rotationanimationkey"
    func rotate(duration: Double = 1, isRight: Bool = true) {
        if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = isRight ? Float.pi * 2.0 : Float.pi * -2.0
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = .infinity

            rotationAnimation.timingFunction = CAMediaTimingFunction.init(name: .linear)
            layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
        }
    }

    func stopRotating() {
        if layer.animation(forKey: UIView.kRotationAnimationKey) != nil {
            layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
        }
    }
    
    // 별점 줄때 플립하기
    public func flipAnimation(durationTime: TimeInterval = 0.07) {
        mainAsync {
            // 애니메이션
            UIView.animate(withDuration: durationTime, delay: 0.0, options: [], animations: {
                self.transform = CGAffineTransform(scaleX: -1, y: 1)
                if let button = self as? UIButton {
                    button.isSelected = true
                }
            }, completion: { _ in
                UIView.animate(withDuration: durationTime, delay: 0.0, options: .autoreverse, animations: {
                    self.transform = CGAffineTransform.identity
                    if let button = self as? UIButton {
                        button.isSelected = true
                    }
                })
            })

        }
    }

    public static func flip(delayTime: Double = 0.05, _ views: UIView...) {
        for view in views {
            DispatchQueue.main.asyncAfter(deadline: .now()+Double(Int64(delayTime * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                view.flipAnimation()
            }
        }
    }
    
    public func wave(callback: (() -> Void)?) {
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.layoutIfNeeded()
            self.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
        })
        
        UIView.animate(withDuration: 0.1, delay: 0.3, options: [], animations: {
            self.layoutIfNeeded()
            self.transform = .identity
        }, completion: { _ in
            callback?()
        })
    }
    
//    public func wave(baseColor: UIColor, callback: (() -> Void)?) {
//        UIView.animate(withDuration: 1.3, delay: 0.0, options: [.curveEaseIn], animations: {
//            self.backgroundColor = baseColor.withAlphaComponent(0.1)
//            self.layoutIfNeeded()
//            self.layer.transform = CATransform3DMakeScale(1.25, 1.45, 1.25)
//        }, completion: { _ in
//            self.backgroundColor = baseColor
//            self.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
//            callback?()
//        })
//    }
    
//    // 별점 줄때 플립하기
//    public func flipAnimation(durationTime: TimeInterval = 0.07, completion: (()->Void)? = nil) {
//        mainAsync {
//            // 애니메이션
//            UIView.animate(withDuration: durationTime, delay: 0.0, options: [], animations: {
//                self.transform = CGAffineTransform(scaleX: -1, y: 1)
//                if let button = self as? UIButton {
//                    button.isSelected = true
//                }
//            }, completion: { _ in
//                UIView.animate(withDuration: durationTime, delay: 0.0, options: .autoreverse, animations: {
//                    self.transform = CGAffineTransform.identity
//                    if let button = self as? UIButton {
//                        button.isSelected = true
//                    }
//                }, completion: { _ in
//                    completion?()
//                })
//            })
//
//        }
//    }
//
//    public func flip(delayTime: Double = 0.05, completion: (()->Void)? = nil) {
//        DispatchQueue.main.asyncAfter(deadline: .now()+Double(Int64(delayTime * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
//            self.flipAnimation(completion: completion)
//        }
//    }

}


extension UIView {
    func touchAnimation(scaleValue: CGFloat = 1.05, isBigScale: Bool = false, afterAction: @escaping () -> Void = {}) {
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
        }

        UIView.animate(withDuration: 0.08, animations: {
            self.layoutIfNeeded()

            if isBigScale {
                self.layer.transform = CATransform3DMakeScale(1.3, 1.3, 1.3)
            } else {
                self.layer.transform = CATransform3DMakeScale(scaleValue, scaleValue, scaleValue)
            }
        }) { _ in
            UIView.animate(withDuration: 0.08, animations: {
                self.layoutIfNeeded()
                self.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
            }) { _ in
                afterAction()
            }

        }
    }
}

extension UIView {
    func setCornerRadius(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layoutIfNeeded()
    }
}
