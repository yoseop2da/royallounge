// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct MemberMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - aboutMe
  ///   - areaNo
  ///   - jobCareer
  ///   - religionCd
  ///   - job
  ///   - nickname
  ///   - tall
  ///   - drinkCd
  ///   - mbNo
  ///   - eduCd
  ///   - userNo
  ///   - bodyCd
  ///   - joinPathCd
  public init(aboutMe: Swift.Optional<String?> = nil, areaNo: Swift.Optional<String?> = nil, jobCareer: Swift.Optional<String?> = nil, religionCd: Swift.Optional<String?> = nil, job: Swift.Optional<String?> = nil, nickname: Swift.Optional<String?> = nil, tall: Swift.Optional<Int?> = nil, drinkCd: Swift.Optional<String?> = nil, mbNo: Swift.Optional<String?> = nil, eduCd: Swift.Optional<String?> = nil, userNo: Swift.Optional<String?> = nil, bodyCd: Swift.Optional<String?> = nil, joinPathCd: Swift.Optional<String?> = nil) {
    graphQLMap = ["aboutMe": aboutMe, "areaNo": areaNo, "jobCareer": jobCareer, "religionCd": religionCd, "job": job, "nickname": nickname, "tall": tall, "drinkCd": drinkCd, "mbNo": mbNo, "eduCd": eduCd, "userNo": userNo, "bodyCd": bodyCd, "joinPathCd": joinPathCd]
  }

  public var aboutMe: Swift.Optional<String?> {
    get {
      return graphQLMap["aboutMe"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "aboutMe")
    }
  }

  public var areaNo: Swift.Optional<String?> {
    get {
      return graphQLMap["areaNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "areaNo")
    }
  }

  public var jobCareer: Swift.Optional<String?> {
    get {
      return graphQLMap["jobCareer"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "jobCareer")
    }
  }

  public var religionCd: Swift.Optional<String?> {
    get {
      return graphQLMap["religionCd"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "religionCd")
    }
  }

  public var job: Swift.Optional<String?> {
    get {
      return graphQLMap["job"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "job")
    }
  }

  public var nickname: Swift.Optional<String?> {
    get {
      return graphQLMap["nickname"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "nickname")
    }
  }

  public var tall: Swift.Optional<Int?> {
    get {
      return graphQLMap["tall"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "tall")
    }
  }

  public var drinkCd: Swift.Optional<String?> {
    get {
      return graphQLMap["drinkCd"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "drinkCd")
    }
  }

  public var mbNo: Swift.Optional<String?> {
    get {
      return graphQLMap["mbNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "mbNo")
    }
  }

  public var eduCd: Swift.Optional<String?> {
    get {
      return graphQLMap["eduCd"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eduCd")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }

  public var bodyCd: Swift.Optional<String?> {
    get {
      return graphQLMap["bodyCd"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "bodyCd")
    }
  }

  public var joinPathCd: Swift.Optional<String?> {
    get {
      return graphQLMap["joinPathCd"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "joinPathCd")
    }
  }
}

public struct InterviewMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - interview
  ///   - interviewNo
  ///   - userNo
  ///   - questionNo
  public init(interview: Swift.Optional<String?> = nil, interviewNo: Swift.Optional<String?> = nil, userNo: Swift.Optional<String?> = nil, questionNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["interview": interview, "interviewNo": interviewNo, "userNo": userNo, "questionNo": questionNo]
  }

  public var interview: Swift.Optional<String?> {
    get {
      return graphQLMap["interview"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "interview")
    }
  }

  public var interviewNo: Swift.Optional<String?> {
    get {
      return graphQLMap["interviewNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "interviewNo")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }

  public var questionNo: Swift.Optional<String?> {
    get {
      return graphQLMap["questionNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "questionNo")
    }
  }
}

public struct CharacterMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - userNo
  ///   - list
  public init(userNo: Swift.Optional<String?> = nil, list: Swift.Optional<[CharacterModelInput?]?> = nil) {
    graphQLMap = ["userNo": userNo, "list": list]
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }

  public var list: Swift.Optional<[CharacterModelInput?]?> {
    get {
      return graphQLMap["list"] as? Swift.Optional<[CharacterModelInput?]?> ?? Swift.Optional<[CharacterModelInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "list")
    }
  }
}

public struct CharacterModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - charCd
  public init(charCd: Swift.Optional<String?> = nil) {
    graphQLMap = ["charCd": charCd]
  }

  public var charCd: Swift.Optional<String?> {
    get {
      return graphQLMap["charCd"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "charCd")
    }
  }
}

public enum StatusType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case block
  case join
  case normal
  case out
  case photoEval
  case photoEvalEnd
  case ready
  case readyHold
  case reject
  case report
  case sleep
  case suspend
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "BLOCK": self = .block
      case "JOIN": self = .join
      case "NORMAL": self = .normal
      case "OUT": self = .out
      case "PHOTO_EVAL": self = .photoEval
      case "PHOTO_EVAL_END": self = .photoEvalEnd
      case "READY": self = .ready
      case "READY_HOLD": self = .readyHold
      case "REJECT": self = .reject
      case "REPORT": self = .report
      case "SLEEP": self = .sleep
      case "SUSPEND": self = .suspend
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .block: return "BLOCK"
      case .join: return "JOIN"
      case .normal: return "NORMAL"
      case .out: return "OUT"
      case .photoEval: return "PHOTO_EVAL"
      case .photoEvalEnd: return "PHOTO_EVAL_END"
      case .ready: return "READY"
      case .readyHold: return "READY_HOLD"
      case .reject: return "REJECT"
      case .report: return "REPORT"
      case .sleep: return "SLEEP"
      case .suspend: return "SUSPEND"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: StatusType, rhs: StatusType) -> Bool {
    switch (lhs, rhs) {
      case (.block, .block): return true
      case (.join, .join): return true
      case (.normal, .normal): return true
      case (.out, .out): return true
      case (.photoEval, .photoEval): return true
      case (.photoEvalEnd, .photoEvalEnd): return true
      case (.ready, .ready): return true
      case (.readyHold, .readyHold): return true
      case (.reject, .reject): return true
      case (.report, .report): return true
      case (.sleep, .sleep): return true
      case (.suspend, .suspend): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [StatusType] {
    return [
      .block,
      .join,
      .normal,
      .out,
      .photoEval,
      .photoEvalEnd,
      .ready,
      .readyHold,
      .reject,
      .report,
      .sleep,
      .suspend,
    ]
  }
}

public enum GenderType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case m
  case w
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "M": self = .m
      case "W": self = .w
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .m: return "M"
      case .w: return "W"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: GenderType, rhs: GenderType) -> Bool {
    switch (lhs, rhs) {
      case (.m, .m): return true
      case (.w, .w): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [GenderType] {
    return [
      .m,
      .w,
    ]
  }
}

public enum UserType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case claim
  case friend
  case gradeExcept
  case inTester
  case outTester
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "CLAIM": self = .claim
      case "FRIEND": self = .friend
      case "GRADE_EXCEPT": self = .gradeExcept
      case "IN_TESTER": self = .inTester
      case "OUT_TESTER": self = .outTester
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .claim: return "CLAIM"
      case .friend: return "FRIEND"
      case .gradeExcept: return "GRADE_EXCEPT"
      case .inTester: return "IN_TESTER"
      case .outTester: return "OUT_TESTER"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: UserType, rhs: UserType) -> Bool {
    switch (lhs, rhs) {
      case (.claim, .claim): return true
      case (.friend, .friend): return true
      case (.gradeExcept, .gradeExcept): return true
      case (.inTester, .inTester): return true
      case (.outTester, .outTester): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [UserType] {
    return [
      .claim,
      .friend,
      .gradeExcept,
      .inTester,
      .outTester,
    ]
  }
}

public struct HobbyMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - list
  ///   - userNo
  public init(list: Swift.Optional<[HobbyModelInput?]?> = nil, userNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["list": list, "userNo": userNo]
  }

  public var list: Swift.Optional<[HobbyModelInput?]?> {
    get {
      return graphQLMap["list"] as? Swift.Optional<[HobbyModelInput?]?> ?? Swift.Optional<[HobbyModelInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "list")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }
}

public struct HobbyModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - hobbySeq
  ///   - hobby
  public init(hobbySeq: Swift.Optional<Int?> = nil, hobby: Swift.Optional<String?> = nil) {
    graphQLMap = ["hobbySeq": hobbySeq, "hobby": hobby]
  }

  public var hobbySeq: Swift.Optional<Int?> {
    get {
      return graphQLMap["hobbySeq"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "hobbySeq")
    }
  }

  public var hobby: Swift.Optional<String?> {
    get {
      return graphQLMap["hobby"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "hobby")
    }
  }
}

public struct InterestMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - list
  ///   - userNo
  public init(list: Swift.Optional<[InterestModelInput?]?> = nil, userNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["list": list, "userNo": userNo]
  }

  public var list: Swift.Optional<[InterestModelInput?]?> {
    get {
      return graphQLMap["list"] as? Swift.Optional<[InterestModelInput?]?> ?? Swift.Optional<[InterestModelInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "list")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }
}

public struct InterestModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - interestSeq
  ///   - interest
  public init(interestSeq: Swift.Optional<Int?> = nil, interest: Swift.Optional<String?> = nil) {
    graphQLMap = ["interestSeq": interestSeq, "interest": interest]
  }

  public var interestSeq: Swift.Optional<Int?> {
    get {
      return graphQLMap["interestSeq"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "interestSeq")
    }
  }

  public var interest: Swift.Optional<String?> {
    get {
      return graphQLMap["interest"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "interest")
    }
  }
}

public struct HaveMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - list
  ///   - userNo
  public init(list: Swift.Optional<[HaveModelInput?]?> = nil, userNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["list": list, "userNo": userNo]
  }

  public var list: Swift.Optional<[HaveModelInput?]?> {
    get {
      return graphQLMap["list"] as? Swift.Optional<[HaveModelInput?]?> ?? Swift.Optional<[HaveModelInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "list")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }
}

public struct HaveModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - haveSeq
  ///   - have
  public init(haveSeq: Swift.Optional<Int?> = nil, have: Swift.Optional<String?> = nil) {
    graphQLMap = ["haveSeq": haveSeq, "have": have]
  }

  public var haveSeq: Swift.Optional<Int?> {
    get {
      return graphQLMap["haveSeq"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "haveSeq")
    }
  }

  public var have: Swift.Optional<String?> {
    get {
      return graphQLMap["have"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "have")
    }
  }
}

public struct WantMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - list
  ///   - userNo
  public init(list: Swift.Optional<[WantModelInput?]?> = nil, userNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["list": list, "userNo": userNo]
  }

  public var list: Swift.Optional<[WantModelInput?]?> {
    get {
      return graphQLMap["list"] as? Swift.Optional<[WantModelInput?]?> ?? Swift.Optional<[WantModelInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "list")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }
}

public struct WantModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - wantSeq
  ///   - want
  public init(wantSeq: Swift.Optional<Int?> = nil, want: Swift.Optional<String?> = nil) {
    graphQLMap = ["wantSeq": wantSeq, "want": want]
  }

  public var wantSeq: Swift.Optional<Int?> {
    get {
      return graphQLMap["wantSeq"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "wantSeq")
    }
  }

  public var want: Swift.Optional<String?> {
    get {
      return graphQLMap["want"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "want")
    }
  }
}

public struct MemberAvoidMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - list
  ///   - userNo
  public init(list: Swift.Optional<[MemberAvoidModelInput?]?> = nil, userNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["list": list, "userNo": userNo]
  }

  public var list: Swift.Optional<[MemberAvoidModelInput?]?> {
    get {
      return graphQLMap["list"] as? Swift.Optional<[MemberAvoidModelInput?]?> ?? Swift.Optional<[MemberAvoidModelInput?]?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "list")
    }
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }
}

public struct MemberAvoidModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - name
  ///   - avoidNo
  ///   - mbNo
  public init(name: Swift.Optional<String?> = nil, avoidNo: Swift.Optional<String?> = nil, mbNo: Swift.Optional<String?> = nil) {
    graphQLMap = ["name": name, "avoidNo": avoidNo, "mbNo": mbNo]
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var avoidNo: Swift.Optional<String?> {
    get {
      return graphQLMap["avoidNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "avoidNo")
    }
  }

  public var mbNo: Swift.Optional<String?> {
    get {
      return graphQLMap["mbNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "mbNo")
    }
  }
}

public struct DeviceMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - userNo
  ///   - osVersion
  ///   - uuid
  ///   - appVersion
  ///   - pushId
  ///   - modelName
  ///   - deviceType
  public init(userNo: Swift.Optional<String?> = nil, osVersion: Swift.Optional<String?> = nil, uuid: Swift.Optional<String?> = nil, appVersion: Swift.Optional<String?> = nil, pushId: Swift.Optional<String?> = nil, modelName: Swift.Optional<String?> = nil, deviceType: Swift.Optional<String?> = nil) {
    graphQLMap = ["userNo": userNo, "osVersion": osVersion, "uuid": uuid, "appVersion": appVersion, "pushId": pushId, "modelName": modelName, "deviceType": deviceType]
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }

  public var osVersion: Swift.Optional<String?> {
    get {
      return graphQLMap["osVersion"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "osVersion")
    }
  }

  public var uuid: Swift.Optional<String?> {
    get {
      return graphQLMap["uuid"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "uuid")
    }
  }

  public var appVersion: Swift.Optional<String?> {
    get {
      return graphQLMap["appVersion"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "appVersion")
    }
  }

  public var pushId: Swift.Optional<String?> {
    get {
      return graphQLMap["pushId"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "pushId")
    }
  }

  public var modelName: Swift.Optional<String?> {
    get {
      return graphQLMap["modelName"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "modelName")
    }
  }

  public var deviceType: Swift.Optional<String?> {
    get {
      return graphQLMap["deviceType"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "deviceType")
    }
  }
}

public enum RejectType: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case aboutMe
  case interview
  case job
  case nickname
  case photo
  case spec
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "ABOUT_ME": self = .aboutMe
      case "INTERVIEW": self = .interview
      case "JOB": self = .job
      case "NICKNAME": self = .nickname
      case "PHOTO": self = .photo
      case "SPEC": self = .spec
      default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .aboutMe: return "ABOUT_ME"
      case .interview: return "INTERVIEW"
      case .job: return "JOB"
      case .nickname: return "NICKNAME"
      case .photo: return "PHOTO"
      case .spec: return "SPEC"
      case .__unknown(let value): return value
    }
  }

  public static func == (lhs: RejectType, rhs: RejectType) -> Bool {
    switch (lhs, rhs) {
      case (.aboutMe, .aboutMe): return true
      case (.interview, .interview): return true
      case (.job, .job): return true
      case (.nickname, .nickname): return true
      case (.photo, .photo): return true
      case (.spec, .spec): return true
      case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }

  public static var allCases: [RejectType] {
    return [
      .aboutMe,
      .interview,
      .job,
      .nickname,
      .photo,
      .spec,
    ]
  }
}

public struct PushSetMutationModelInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - userNo
  ///   - setNo
  ///   - sendYn
  public init(userNo: Swift.Optional<String?> = nil, setNo: Swift.Optional<String?> = nil, sendYn: Swift.Optional<Bool?> = nil) {
    graphQLMap = ["userNo": userNo, "setNo": setNo, "sendYn": sendYn]
  }

  public var userNo: Swift.Optional<String?> {
    get {
      return graphQLMap["userNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userNo")
    }
  }

  public var setNo: Swift.Optional<String?> {
    get {
      return graphQLMap["setNo"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "setNo")
    }
  }

  public var sendYn: Swift.Optional<Bool?> {
    get {
      return graphQLMap["sendYn"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "sendYn")
    }
  }
}

public final class MemberInfoMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation memberInfo($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "memberInfo"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class NicknameMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation nickname($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "nickname"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetNicknameQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getNickname($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        nickname
        nicknameRejectMsg
      }
    }
    """

  public let operationName: String = "getNickname"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("nicknameRejectMsg", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(nickname: String? = nil, nicknameRejectMsg: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "nickname": nickname, "nicknameRejectMsg": nicknameRejectMsg])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }

      public var nicknameRejectMsg: String? {
        get {
          return resultMap["nicknameRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nicknameRejectMsg")
        }
      }
    }
  }
}

public final class TallMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation tall($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "tall"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetTallQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getTall($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        tall
        nickname
      }
    }
    """

  public let operationName: String = "getTall"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("tall", type: .scalar(Int.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(tall: Int? = nil, nickname: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "tall": tall, "nickname": nickname])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var tall: Int? {
        get {
          return resultMap["tall"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "tall")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }
    }
  }
}

public final class BodyCdMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation bodyCd($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "bodyCd"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetBodyCdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getBodyCd($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        bodyCd
        nickname
      }
    }
    """

  public let operationName: String = "getBodyCd"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("bodyCd", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(bodyCd: String? = nil, nickname: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "bodyCd": bodyCd, "nickname": nickname])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var bodyCd: String? {
        get {
          return resultMap["bodyCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "bodyCd")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }
    }
  }
}

public final class DrinkCdMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation drinkCd($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "drinkCd"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetDrinkCdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getDrinkCd($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        drinkCd
        nickname
      }
    }
    """

  public let operationName: String = "getDrinkCd"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("drinkCd", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(drinkCd: String? = nil, nickname: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "drinkCd": drinkCd, "nickname": nickname])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var drinkCd: String? {
        get {
          return resultMap["drinkCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "drinkCd")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }
    }
  }
}

public final class ReligionCdMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation religionCd($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "religionCd"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetReligionCdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getReligionCd($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        religionCd
        nickname
      }
    }
    """

  public let operationName: String = "getReligionCd"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("religionCd", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(religionCd: String? = nil, nickname: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "religionCd": religionCd, "nickname": nickname])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var religionCd: String? {
        get {
          return resultMap["religionCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "religionCd")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }
    }
  }
}

public final class GetAreaListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getAreaList {
      areaGroupList {
        __typename
        name
        areaList {
          __typename
          areaNo
          name
        }
      }
    }
    """

  public let operationName: String = "getAreaList"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("areaGroupList", type: .list(.object(AreaGroupList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(areaGroupList: [AreaGroupList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "areaGroupList": areaGroupList.flatMap { (value: [AreaGroupList?]) -> [ResultMap?] in value.map { (value: AreaGroupList?) -> ResultMap? in value.flatMap { (value: AreaGroupList) -> ResultMap in value.resultMap } } }])
    }

    /// 지역그룹 리스트 조회
    public var areaGroupList: [AreaGroupList?]? {
      get {
        return (resultMap["areaGroupList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AreaGroupList?] in value.map { (value: ResultMap?) -> AreaGroupList? in value.flatMap { (value: ResultMap) -> AreaGroupList in AreaGroupList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [AreaGroupList?]) -> [ResultMap?] in value.map { (value: AreaGroupList?) -> ResultMap? in value.flatMap { (value: AreaGroupList) -> ResultMap in value.resultMap } } }, forKey: "areaGroupList")
      }
    }

    public struct AreaGroupList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AreaGroupModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("areaList", type: .list(.object(AreaList.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String? = nil, areaList: [AreaList?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "AreaGroupModel", "name": name, "areaList": areaList.flatMap { (value: [AreaList?]) -> [ResultMap?] in value.map { (value: AreaList?) -> ResultMap? in value.flatMap { (value: AreaList) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var areaList: [AreaList?]? {
        get {
          return (resultMap["areaList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [AreaList?] in value.map { (value: ResultMap?) -> AreaList? in value.flatMap { (value: ResultMap) -> AreaList in AreaList(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [AreaList?]) -> [ResultMap?] in value.map { (value: AreaList?) -> ResultMap? in value.flatMap { (value: AreaList) -> ResultMap in value.resultMap } } }, forKey: "areaList")
        }
      }

      public struct AreaList: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["AreaModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("areaNo", type: .scalar(String.self)),
            GraphQLField("name", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(areaNo: String? = nil, name: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "AreaModel", "areaNo": areaNo, "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var areaNo: String? {
          get {
            return resultMap["areaNo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "areaNo")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }
    }
  }
}

public final class AreaCdMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation areaCd($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "areaCd"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetAreaNoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getAreaNo($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        areaNo
      }
    }
    """

  public let operationName: String = "getAreaNo"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("areaNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(areaNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "areaNo": areaNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var areaNo: String? {
        get {
          return resultMap["areaNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "areaNo")
        }
      }
    }
  }
}

public final class EduCdMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation eduCd($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "eduCd"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetEduCdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getEduCd($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        eduCd
        nickname
      }
    }
    """

  public let operationName: String = "getEduCd"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("eduCd", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(eduCd: String? = nil, nickname: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "eduCd": eduCd, "nickname": nickname])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var eduCd: String? {
        get {
          return resultMap["eduCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "eduCd")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }
    }
  }
}

public final class JobMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation job($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "job"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetJobQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getJob($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        job
        jobRejectMsg
      }
    }
    """

  public let operationName: String = "getJob"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("job", type: .scalar(String.self)),
          GraphQLField("jobRejectMsg", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(job: String? = nil, jobRejectMsg: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "job": job, "jobRejectMsg": jobRejectMsg])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var job: String? {
        get {
          return resultMap["job"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "job")
        }
      }

      public var jobRejectMsg: String? {
        get {
          return resultMap["jobRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "jobRejectMsg")
        }
      }
    }
  }
}

public final class AboutMeMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation aboutMe($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "aboutMe"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetAboutMeQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getAboutMe($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        aboutMe
        aboutMeRejectMsg
      }
    }
    """

  public let operationName: String = "getAboutMe"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("aboutMe", type: .scalar(String.self)),
          GraphQLField("aboutMeRejectMsg", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(aboutMe: String? = nil, aboutMeRejectMsg: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "aboutMe": aboutMe, "aboutMeRejectMsg": aboutMeRejectMsg])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var aboutMe: String? {
        get {
          return resultMap["aboutMe"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "aboutMe")
        }
      }

      public var aboutMeRejectMsg: String? {
        get {
          return resultMap["aboutMeRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "aboutMeRejectMsg")
        }
      }
    }
  }
}

public final class InterviewMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation interview($interviewInput: InterviewMutationModelInput!) {
      saveMemberInterview(memberInterview: $interviewInput)
    }
    """

  public let operationName: String = "interview"

  public var interviewInput: InterviewMutationModelInput

  public init(interviewInput: InterviewMutationModelInput) {
    self.interviewInput = interviewInput
  }

  public var variables: GraphQLMap? {
    return ["interviewInput": interviewInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberInterview", arguments: ["memberInterview": GraphQLVariable("interviewInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberInterview: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberInterview": saveMemberInterview])
    }

    /// 인터뷰 저장
    public var saveMemberInterview: Bool {
      get {
        return resultMap["saveMemberInterview"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberInterview")
      }
    }
  }
}

public final class GetInterviewQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getInterview($userNo: String!) {
      memberInterviewList(userNo: $userNo) {
        __typename
        questionNo
        question
        interview
        example
        rejectMsg
      }
    }
    """

  public let operationName: String = "getInterview"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberInterviewList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberInterviewList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberInterviewList: [MemberInterviewList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberInterviewList": memberInterviewList.flatMap { (value: [MemberInterviewList?]) -> [ResultMap?] in value.map { (value: MemberInterviewList?) -> ResultMap? in value.flatMap { (value: MemberInterviewList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 인터뷰 리스트 조회
    public var memberInterviewList: [MemberInterviewList?]? {
      get {
        return (resultMap["memberInterviewList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberInterviewList?] in value.map { (value: ResultMap?) -> MemberInterviewList? in value.flatMap { (value: ResultMap) -> MemberInterviewList in MemberInterviewList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberInterviewList?]) -> [ResultMap?] in value.map { (value: MemberInterviewList?) -> ResultMap? in value.flatMap { (value: MemberInterviewList) -> ResultMap in value.resultMap } } }, forKey: "memberInterviewList")
      }
    }

    public struct MemberInterviewList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["InterviewModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("questionNo", type: .scalar(String.self)),
          GraphQLField("question", type: .scalar(String.self)),
          GraphQLField("interview", type: .scalar(String.self)),
          GraphQLField("example", type: .scalar(String.self)),
          GraphQLField("rejectMsg", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(questionNo: String? = nil, question: String? = nil, interview: String? = nil, example: String? = nil, rejectMsg: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "InterviewModel", "questionNo": questionNo, "question": question, "interview": interview, "example": example, "rejectMsg": rejectMsg])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var questionNo: String? {
        get {
          return resultMap["questionNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "questionNo")
        }
      }

      public var question: String? {
        get {
          return resultMap["question"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "question")
        }
      }

      public var interview: String? {
        get {
          return resultMap["interview"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "interview")
        }
      }

      public var example: String? {
        get {
          return resultMap["example"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "example")
        }
      }

      public var rejectMsg: String? {
        get {
          return resultMap["rejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "rejectMsg")
        }
      }
    }
  }
}

public final class CharListMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation charList($charListInput: CharacterMutationModelInput!) {
      saveMemberCharacterList(memberCharacter: $charListInput)
    }
    """

  public let operationName: String = "charList"

  public var charListInput: CharacterMutationModelInput

  public init(charListInput: CharacterMutationModelInput) {
    self.charListInput = charListInput
  }

  public var variables: GraphQLMap? {
    return ["charListInput": charListInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberCharacterList", arguments: ["memberCharacter": GraphQLVariable("charListInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberCharacterList: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberCharacterList": saveMemberCharacterList])
    }

    /// 회원 성격 리스트 저장
    public var saveMemberCharacterList: Bool {
      get {
        return resultMap["saveMemberCharacterList"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberCharacterList")
      }
    }
  }
}

public final class GetCharListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getCharList($userNo: String!) {
      memberCharacterList(userNo: $userNo) {
        __typename
        charCd
      }
    }
    """

  public let operationName: String = "getCharList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberCharacterList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberCharacterList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberCharacterList: [MemberCharacterList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberCharacterList": memberCharacterList.flatMap { (value: [MemberCharacterList?]) -> [ResultMap?] in value.map { (value: MemberCharacterList?) -> ResultMap? in value.flatMap { (value: MemberCharacterList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 성격 리스트 조회
    public var memberCharacterList: [MemberCharacterList?]? {
      get {
        return (resultMap["memberCharacterList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberCharacterList?] in value.map { (value: ResultMap?) -> MemberCharacterList? in value.flatMap { (value: ResultMap) -> MemberCharacterList in MemberCharacterList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberCharacterList?]) -> [ResultMap?] in value.map { (value: MemberCharacterList?) -> ResultMap? in value.flatMap { (value: MemberCharacterList) -> ResultMap in value.resultMap } } }, forKey: "memberCharacterList")
      }
    }

    public struct MemberCharacterList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CharacterModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("charCd", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(charCd: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "CharacterModel", "charCd": charCd])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var charCd: String? {
        get {
          return resultMap["charCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "charCd")
        }
      }
    }
  }
}

public final class GetJoinStepQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getJoinStep($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        status
        joinStep
        gender
        userType
      }
    }
    """

  public let operationName: String = "getJoinStep"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("status", type: .scalar(StatusType.self)),
          GraphQLField("joinStep", type: .nonNull(.scalar(Int.self))),
          GraphQLField("gender", type: .scalar(GenderType.self)),
          GraphQLField("userType", type: .scalar(UserType.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(status: StatusType? = nil, joinStep: Int, gender: GenderType? = nil, userType: UserType? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "status": status, "joinStep": joinStep, "gender": gender, "userType": userType])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var status: StatusType? {
        get {
          return resultMap["status"] as? StatusType
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var joinStep: Int {
        get {
          return resultMap["joinStep"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinStep")
        }
      }

      public var gender: GenderType? {
        get {
          return resultMap["gender"] as? GenderType
        }
        set {
          resultMap.updateValue(newValue, forKey: "gender")
        }
      }

      public var userType: UserType? {
        get {
          return resultMap["userType"] as? UserType
        }
        set {
          resultMap.updateValue(newValue, forKey: "userType")
        }
      }
    }
  }
}

public final class HobbyListMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation hobbyList($hobbyListInput: HobbyMutationModelInput!) {
      saveMemberHobbyList(memberHobby: $hobbyListInput)
    }
    """

  public let operationName: String = "hobbyList"

  public var hobbyListInput: HobbyMutationModelInput

  public init(hobbyListInput: HobbyMutationModelInput) {
    self.hobbyListInput = hobbyListInput
  }

  public var variables: GraphQLMap? {
    return ["hobbyListInput": hobbyListInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberHobbyList", arguments: ["memberHobby": GraphQLVariable("hobbyListInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberHobbyList: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberHobbyList": saveMemberHobbyList])
    }

    /// 회원 취미 리스트 저장
    public var saveMemberHobbyList: Bool {
      get {
        return resultMap["saveMemberHobbyList"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberHobbyList")
      }
    }
  }
}

public final class GetHobbyListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getHobbyList($userNo: String!) {
      memberHobbyList(userNo: $userNo) {
        __typename
        hobby
      }
    }
    """

  public let operationName: String = "getHobbyList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberHobbyList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberHobbyList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberHobbyList: [MemberHobbyList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberHobbyList": memberHobbyList.flatMap { (value: [MemberHobbyList?]) -> [ResultMap?] in value.map { (value: MemberHobbyList?) -> ResultMap? in value.flatMap { (value: MemberHobbyList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 취미 리스트 조회
    public var memberHobbyList: [MemberHobbyList?]? {
      get {
        return (resultMap["memberHobbyList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberHobbyList?] in value.map { (value: ResultMap?) -> MemberHobbyList? in value.flatMap { (value: ResultMap) -> MemberHobbyList in MemberHobbyList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberHobbyList?]) -> [ResultMap?] in value.map { (value: MemberHobbyList?) -> ResultMap? in value.flatMap { (value: MemberHobbyList) -> ResultMap in value.resultMap } } }, forKey: "memberHobbyList")
      }
    }

    public struct MemberHobbyList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["HobbyModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("hobby", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(hobby: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "HobbyModel", "hobby": hobby])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var hobby: String? {
        get {
          return resultMap["hobby"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "hobby")
        }
      }
    }
  }
}

public final class InterestListMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation interestList($interestListInput: InterestMutationModelInput!) {
      saveMemberInterestList(memberInterest: $interestListInput)
    }
    """

  public let operationName: String = "interestList"

  public var interestListInput: InterestMutationModelInput

  public init(interestListInput: InterestMutationModelInput) {
    self.interestListInput = interestListInput
  }

  public var variables: GraphQLMap? {
    return ["interestListInput": interestListInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberInterestList", arguments: ["memberInterest": GraphQLVariable("interestListInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberInterestList: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberInterestList": saveMemberInterestList])
    }

    /// 회원 관심사 리스트 저장
    public var saveMemberInterestList: Bool {
      get {
        return resultMap["saveMemberInterestList"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberInterestList")
      }
    }
  }
}

public final class GetInterestListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getInterestList($userNo: String!) {
      memberInterestList(userNo: $userNo) {
        __typename
        interest
      }
    }
    """

  public let operationName: String = "getInterestList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberInterestList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberInterestList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberInterestList: [MemberInterestList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberInterestList": memberInterestList.flatMap { (value: [MemberInterestList?]) -> [ResultMap?] in value.map { (value: MemberInterestList?) -> ResultMap? in value.flatMap { (value: MemberInterestList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 관심사 리스트 조회
    public var memberInterestList: [MemberInterestList?]? {
      get {
        return (resultMap["memberInterestList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberInterestList?] in value.map { (value: ResultMap?) -> MemberInterestList? in value.flatMap { (value: ResultMap) -> MemberInterestList in MemberInterestList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberInterestList?]) -> [ResultMap?] in value.map { (value: MemberInterestList?) -> ResultMap? in value.flatMap { (value: MemberInterestList) -> ResultMap in value.resultMap } } }, forKey: "memberInterestList")
      }
    }

    public struct MemberInterestList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["InterestModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("interest", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(interest: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "InterestModel", "interest": interest])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var interest: String? {
        get {
          return resultMap["interest"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "interest")
        }
      }
    }
  }
}

public final class HaveListMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation haveList($haveListInput: HaveMutationModelInput!) {
      saveMemberHaveList(memberHave: $haveListInput)
    }
    """

  public let operationName: String = "haveList"

  public var haveListInput: HaveMutationModelInput

  public init(haveListInput: HaveMutationModelInput) {
    self.haveListInput = haveListInput
  }

  public var variables: GraphQLMap? {
    return ["haveListInput": haveListInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberHaveList", arguments: ["memberHave": GraphQLVariable("haveListInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberHaveList: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberHaveList": saveMemberHaveList])
    }

    /// 회원 Have 리스트 저장
    public var saveMemberHaveList: Bool {
      get {
        return resultMap["saveMemberHaveList"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberHaveList")
      }
    }
  }
}

public final class GetHaveListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getHaveList($userNo: String!) {
      memberHaveList(userNo: $userNo) {
        __typename
        have
      }
    }
    """

  public let operationName: String = "getHaveList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberHaveList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberHaveList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberHaveList: [MemberHaveList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberHaveList": memberHaveList.flatMap { (value: [MemberHaveList?]) -> [ResultMap?] in value.map { (value: MemberHaveList?) -> ResultMap? in value.flatMap { (value: MemberHaveList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 Have 리스트 조회
    public var memberHaveList: [MemberHaveList?]? {
      get {
        return (resultMap["memberHaveList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberHaveList?] in value.map { (value: ResultMap?) -> MemberHaveList? in value.flatMap { (value: ResultMap) -> MemberHaveList in MemberHaveList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberHaveList?]) -> [ResultMap?] in value.map { (value: MemberHaveList?) -> ResultMap? in value.flatMap { (value: MemberHaveList) -> ResultMap in value.resultMap } } }, forKey: "memberHaveList")
      }
    }

    public struct MemberHaveList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["HaveModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("have", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(have: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "HaveModel", "have": have])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var have: String? {
        get {
          return resultMap["have"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "have")
        }
      }
    }
  }
}

public final class WantListMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation wantList($wantListInput: WantMutationModelInput!) {
      saveMemberWantList(memberWant: $wantListInput)
    }
    """

  public let operationName: String = "wantList"

  public var wantListInput: WantMutationModelInput

  public init(wantListInput: WantMutationModelInput) {
    self.wantListInput = wantListInput
  }

  public var variables: GraphQLMap? {
    return ["wantListInput": wantListInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberWantList", arguments: ["memberWant": GraphQLVariable("wantListInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberWantList: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberWantList": saveMemberWantList])
    }

    /// 회원 Want 리스트 저장
    public var saveMemberWantList: Bool {
      get {
        return resultMap["saveMemberWantList"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberWantList")
      }
    }
  }
}

public final class GetWantListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getWantList($userNo: String!) {
      memberWantList(userNo: $userNo) {
        __typename
        want
      }
    }
    """

  public let operationName: String = "getWantList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberWantList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberWantList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberWantList: [MemberWantList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberWantList": memberWantList.flatMap { (value: [MemberWantList?]) -> [ResultMap?] in value.map { (value: MemberWantList?) -> ResultMap? in value.flatMap { (value: MemberWantList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 Want 리스트 조회
    public var memberWantList: [MemberWantList?]? {
      get {
        return (resultMap["memberWantList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberWantList?] in value.map { (value: ResultMap?) -> MemberWantList? in value.flatMap { (value: ResultMap) -> MemberWantList in MemberWantList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberWantList?]) -> [ResultMap?] in value.map { (value: MemberWantList?) -> ResultMap? in value.flatMap { (value: MemberWantList) -> ResultMap in value.resultMap } } }, forKey: "memberWantList")
      }
    }

    public struct MemberWantList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["WantModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("want", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(want: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "WantModel", "want": want])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var want: String? {
        get {
          return resultMap["want"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "want")
        }
      }
    }
  }
}

public final class JobCareerMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation jobCareer($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "jobCareer"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetJobCareerQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getJobCareer($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        jobCareer
      }
    }
    """

  public let operationName: String = "getJobCareer"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("jobCareer", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(jobCareer: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "jobCareer": jobCareer])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var jobCareer: String? {
        get {
          return resultMap["jobCareer"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "jobCareer")
        }
      }
    }
  }
}

public final class GetPhotoListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getPhotoList($userNo: String!) {
      memberPhotoList(userNo: $userNo) {
        __typename
        photoNo
        photoUrl
        photoSeq
        rejectMsg
        detectYn
        photoStatus
        photoStatusName
      }
    }
    """

  public let operationName: String = "getPhotoList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberPhotoList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberPhotoList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberPhotoList: [MemberPhotoList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberPhotoList": memberPhotoList.flatMap { (value: [MemberPhotoList?]) -> [ResultMap?] in value.map { (value: MemberPhotoList?) -> ResultMap? in value.flatMap { (value: MemberPhotoList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 프로필 사진 리스트 조회
    public var memberPhotoList: [MemberPhotoList?]? {
      get {
        return (resultMap["memberPhotoList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberPhotoList?] in value.map { (value: ResultMap?) -> MemberPhotoList? in value.flatMap { (value: ResultMap) -> MemberPhotoList in MemberPhotoList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberPhotoList?]) -> [ResultMap?] in value.map { (value: MemberPhotoList?) -> ResultMap? in value.flatMap { (value: MemberPhotoList) -> ResultMap in value.resultMap } } }, forKey: "memberPhotoList")
      }
    }

    public struct MemberPhotoList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberPhotoModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("photoNo", type: .scalar(String.self)),
          GraphQLField("photoUrl", type: .scalar(String.self)),
          GraphQLField("photoSeq", type: .scalar(Int.self)),
          GraphQLField("rejectMsg", type: .scalar(String.self)),
          GraphQLField("detectYn", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("photoStatus", type: .scalar(String.self)),
          GraphQLField("photoStatusName", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(photoNo: String? = nil, photoUrl: String? = nil, photoSeq: Int? = nil, rejectMsg: String? = nil, detectYn: Bool, photoStatus: String? = nil, photoStatusName: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberPhotoModel", "photoNo": photoNo, "photoUrl": photoUrl, "photoSeq": photoSeq, "rejectMsg": rejectMsg, "detectYn": detectYn, "photoStatus": photoStatus, "photoStatusName": photoStatusName])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var photoNo: String? {
        get {
          return resultMap["photoNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoNo")
        }
      }

      public var photoUrl: String? {
        get {
          return resultMap["photoUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoUrl")
        }
      }

      public var photoSeq: Int? {
        get {
          return resultMap["photoSeq"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoSeq")
        }
      }

      public var rejectMsg: String? {
        get {
          return resultMap["rejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "rejectMsg")
        }
      }

      public var detectYn: Bool {
        get {
          return resultMap["detectYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "detectYn")
        }
      }

      public var photoStatus: String? {
        get {
          return resultMap["photoStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoStatus")
        }
      }

      public var photoStatusName: String? {
        get {
          return resultMap["photoStatusName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoStatusName")
        }
      }
    }
  }
}

public final class JoinPathMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation joinPath($memberInput: MemberMutationModelInput!) {
      saveMember(member: $memberInput)
    }
    """

  public let operationName: String = "joinPath"

  public var memberInput: MemberMutationModelInput

  public init(memberInput: MemberMutationModelInput) {
    self.memberInput = memberInput
  }

  public var variables: GraphQLMap? {
    return ["memberInput": memberInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMember", arguments: ["member": GraphQLVariable("memberInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMember: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMember": saveMember])
    }

    /// 회원프로필 저장
    public var saveMember: Bool {
      get {
        return resultMap["saveMember"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMember")
      }
    }
  }
}

public final class GetJoinPathQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getJoinPath($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        joinPathCd
      }
    }
    """

  public let operationName: String = "getJoinPath"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("joinPathCd", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(joinPathCd: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "joinPathCd": joinPathCd])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var joinPathCd: String? {
        get {
          return resultMap["joinPathCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinPathCd")
        }
      }
    }
  }
}

public final class AvoidListMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation avoidList($avoidListInput: MemberAvoidMutationModelInput!) {
      saveMemberAvoidList(memberAvoid: $avoidListInput) {
        __typename
        name
        mbNo
        avoidNo
      }
    }
    """

  public let operationName: String = "avoidList"

  public var avoidListInput: MemberAvoidMutationModelInput

  public init(avoidListInput: MemberAvoidMutationModelInput) {
    self.avoidListInput = avoidListInput
  }

  public var variables: GraphQLMap? {
    return ["avoidListInput": avoidListInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberAvoidList", arguments: ["memberAvoid": GraphQLVariable("avoidListInput")], type: .list(.object(SaveMemberAvoidList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberAvoidList: [SaveMemberAvoidList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberAvoidList": saveMemberAvoidList.flatMap { (value: [SaveMemberAvoidList?]) -> [ResultMap?] in value.map { (value: SaveMemberAvoidList?) -> ResultMap? in value.flatMap { (value: SaveMemberAvoidList) -> ResultMap in value.resultMap } } }])
    }

    /// 아는사람 피하기 리스트 등록
    public var saveMemberAvoidList: [SaveMemberAvoidList?]? {
      get {
        return (resultMap["saveMemberAvoidList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SaveMemberAvoidList?] in value.map { (value: ResultMap?) -> SaveMemberAvoidList? in value.flatMap { (value: ResultMap) -> SaveMemberAvoidList in SaveMemberAvoidList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [SaveMemberAvoidList?]) -> [ResultMap?] in value.map { (value: SaveMemberAvoidList?) -> ResultMap? in value.flatMap { (value: SaveMemberAvoidList) -> ResultMap in value.resultMap } } }, forKey: "saveMemberAvoidList")
      }
    }

    public struct SaveMemberAvoidList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberAvoidModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("mbNo", type: .scalar(String.self)),
          GraphQLField("avoidNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String? = nil, mbNo: String? = nil, avoidNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberAvoidModel", "name": name, "mbNo": mbNo, "avoidNo": avoidNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var mbNo: String? {
        get {
          return resultMap["mbNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "mbNo")
        }
      }

      public var avoidNo: String? {
        get {
          return resultMap["avoidNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avoidNo")
        }
      }
    }
  }
}

public final class GetMemberAvoidListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMemberAvoidList($userNo: String!) {
      memberAvoidList(userNo: $userNo) {
        __typename
        name
        mbNo
        avoidNo
      }
    }
    """

  public let operationName: String = "getMemberAvoidList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberAvoidList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberAvoidList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberAvoidList: [MemberAvoidList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberAvoidList": memberAvoidList.flatMap { (value: [MemberAvoidList?]) -> [ResultMap?] in value.map { (value: MemberAvoidList?) -> ResultMap? in value.flatMap { (value: MemberAvoidList) -> ResultMap in value.resultMap } } }])
    }

    /// 아는사람 피하기 리스트 조회
    public var memberAvoidList: [MemberAvoidList?]? {
      get {
        return (resultMap["memberAvoidList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberAvoidList?] in value.map { (value: ResultMap?) -> MemberAvoidList? in value.flatMap { (value: ResultMap) -> MemberAvoidList in MemberAvoidList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberAvoidList?]) -> [ResultMap?] in value.map { (value: MemberAvoidList?) -> ResultMap? in value.flatMap { (value: MemberAvoidList) -> ResultMap in value.resultMap } } }, forKey: "memberAvoidList")
      }
    }

    public struct MemberAvoidList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberAvoidModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("mbNo", type: .scalar(String.self)),
          GraphQLField("avoidNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String? = nil, mbNo: String? = nil, avoidNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberAvoidModel", "name": name, "mbNo": mbNo, "avoidNo": avoidNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var mbNo: String? {
        get {
          return resultMap["mbNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "mbNo")
        }
      }

      public var avoidNo: String? {
        get {
          return resultMap["avoidNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avoidNo")
        }
      }
    }
  }
}

public final class GetRecoCdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getRecoCd($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        recoCd
      }
    }
    """

  public let operationName: String = "getRecoCd"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("recoCd", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(recoCd: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "recoCd": recoCd])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var recoCd: String? {
        get {
          return resultMap["recoCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "recoCd")
        }
      }
    }
  }
}

public final class AddDeviceMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation addDevice($deviceInput: DeviceMutationModelInput!) {
      saveDevice(device: $deviceInput)
    }
    """

  public let operationName: String = "addDevice"

  public var deviceInput: DeviceMutationModelInput

  public init(deviceInput: DeviceMutationModelInput) {
    self.deviceInput = deviceInput
  }

  public var variables: GraphQLMap? {
    return ["deviceInput": deviceInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveDevice", arguments: ["device": GraphQLVariable("deviceInput")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveDevice: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveDevice": saveDevice])
    }

    /// 디바이스 저장
    public var saveDevice: Bool {
      get {
        return resultMap["saveDevice"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveDevice")
      }
    }
  }
}

public final class GetSpecGroupQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSpecGroup($userNo: String!) {
      memberSpecGroupList(userNo: $userNo) {
        __typename
        specGroupNo
        title
        description
        multiSelectYn
        specList {
          __typename
          specNo
          memberSpecNo
          title
          subTitle
          description
          badgeUrl
          regYn
          displayYn
          specStatus
          specStatusName
        }
      }
    }
    """

  public let operationName: String = "getSpecGroup"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberSpecGroupList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberSpecGroupList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberSpecGroupList: [MemberSpecGroupList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberSpecGroupList": memberSpecGroupList.flatMap { (value: [MemberSpecGroupList?]) -> [ResultMap?] in value.map { (value: MemberSpecGroupList?) -> ResultMap? in value.flatMap { (value: MemberSpecGroupList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 스펙 그룹 리스트 조회
    public var memberSpecGroupList: [MemberSpecGroupList?]? {
      get {
        return (resultMap["memberSpecGroupList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberSpecGroupList?] in value.map { (value: ResultMap?) -> MemberSpecGroupList? in value.flatMap { (value: ResultMap) -> MemberSpecGroupList in MemberSpecGroupList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberSpecGroupList?]) -> [ResultMap?] in value.map { (value: MemberSpecGroupList?) -> ResultMap? in value.flatMap { (value: MemberSpecGroupList) -> ResultMap in value.resultMap } } }, forKey: "memberSpecGroupList")
      }
    }

    public struct MemberSpecGroupList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpecGroupModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("specGroupNo", type: .scalar(String.self)),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("multiSelectYn", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("specList", type: .list(.object(SpecList.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(specGroupNo: String? = nil, title: String? = nil, description: String? = nil, multiSelectYn: Bool, specList: [SpecList?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SpecGroupModel", "specGroupNo": specGroupNo, "title": title, "description": description, "multiSelectYn": multiSelectYn, "specList": specList.flatMap { (value: [SpecList?]) -> [ResultMap?] in value.map { (value: SpecList?) -> ResultMap? in value.flatMap { (value: SpecList) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var specGroupNo: String? {
        get {
          return resultMap["specGroupNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "specGroupNo")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var multiSelectYn: Bool {
        get {
          return resultMap["multiSelectYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "multiSelectYn")
        }
      }

      public var specList: [SpecList?]? {
        get {
          return (resultMap["specList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SpecList?] in value.map { (value: ResultMap?) -> SpecList? in value.flatMap { (value: ResultMap) -> SpecList in SpecList(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [SpecList?]) -> [ResultMap?] in value.map { (value: SpecList?) -> ResultMap? in value.flatMap { (value: SpecList) -> ResultMap in value.resultMap } } }, forKey: "specList")
        }
      }

      public struct SpecList: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SpecModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("specNo", type: .scalar(String.self)),
            GraphQLField("memberSpecNo", type: .scalar(String.self)),
            GraphQLField("title", type: .scalar(String.self)),
            GraphQLField("subTitle", type: .scalar(String.self)),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("badgeUrl", type: .scalar(String.self)),
            GraphQLField("regYn", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("displayYn", type: .nonNull(.scalar(Bool.self))),
            GraphQLField("specStatus", type: .scalar(String.self)),
            GraphQLField("specStatusName", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(specNo: String? = nil, memberSpecNo: String? = nil, title: String? = nil, subTitle: String? = nil, description: String? = nil, badgeUrl: String? = nil, regYn: Bool, displayYn: Bool, specStatus: String? = nil, specStatusName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "SpecModel", "specNo": specNo, "memberSpecNo": memberSpecNo, "title": title, "subTitle": subTitle, "description": description, "badgeUrl": badgeUrl, "regYn": regYn, "displayYn": displayYn, "specStatus": specStatus, "specStatusName": specStatusName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var specNo: String? {
          get {
            return resultMap["specNo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "specNo")
          }
        }

        public var memberSpecNo: String? {
          get {
            return resultMap["memberSpecNo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "memberSpecNo")
          }
        }

        public var title: String? {
          get {
            return resultMap["title"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        public var subTitle: String? {
          get {
            return resultMap["subTitle"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "subTitle")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }

        public var badgeUrl: String? {
          get {
            return resultMap["badgeUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "badgeUrl")
          }
        }

        public var regYn: Bool {
          get {
            return resultMap["regYn"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "regYn")
          }
        }

        public var displayYn: Bool {
          get {
            return resultMap["displayYn"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "displayYn")
          }
        }

        public var specStatus: String? {
          get {
            return resultMap["specStatus"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "specStatus")
          }
        }

        public var specStatusName: String? {
          get {
            return resultMap["specStatusName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "specStatusName")
          }
        }
      }
    }
  }
}

public final class DelMemberAvoidMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation delMemberAvoid($memberAvoidInput: MemberAvoidMutationModelInput!) {
      deleteMemberAvoidList(memberAvoid: $memberAvoidInput) {
        __typename
        name
        mbNo
        avoidNo
      }
    }
    """

  public let operationName: String = "delMemberAvoid"

  public var memberAvoidInput: MemberAvoidMutationModelInput

  public init(memberAvoidInput: MemberAvoidMutationModelInput) {
    self.memberAvoidInput = memberAvoidInput
  }

  public var variables: GraphQLMap? {
    return ["memberAvoidInput": memberAvoidInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("deleteMemberAvoidList", arguments: ["memberAvoid": GraphQLVariable("memberAvoidInput")], type: .list(.object(DeleteMemberAvoidList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteMemberAvoidList: [DeleteMemberAvoidList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteMemberAvoidList": deleteMemberAvoidList.flatMap { (value: [DeleteMemberAvoidList?]) -> [ResultMap?] in value.map { (value: DeleteMemberAvoidList?) -> ResultMap? in value.flatMap { (value: DeleteMemberAvoidList) -> ResultMap in value.resultMap } } }])
    }

    /// 아는피하기 리스트 삭제
    public var deleteMemberAvoidList: [DeleteMemberAvoidList?]? {
      get {
        return (resultMap["deleteMemberAvoidList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [DeleteMemberAvoidList?] in value.map { (value: ResultMap?) -> DeleteMemberAvoidList? in value.flatMap { (value: ResultMap) -> DeleteMemberAvoidList in DeleteMemberAvoidList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [DeleteMemberAvoidList?]) -> [ResultMap?] in value.map { (value: DeleteMemberAvoidList?) -> ResultMap? in value.flatMap { (value: DeleteMemberAvoidList) -> ResultMap in value.resultMap } } }, forKey: "deleteMemberAvoidList")
      }
    }

    public struct DeleteMemberAvoidList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberAvoidModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("mbNo", type: .scalar(String.self)),
          GraphQLField("avoidNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String? = nil, mbNo: String? = nil, avoidNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberAvoidModel", "name": name, "mbNo": mbNo, "avoidNo": avoidNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var mbNo: String? {
        get {
          return resultMap["mbNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "mbNo")
        }
      }

      public var avoidNo: String? {
        get {
          return resultMap["avoidNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avoidNo")
        }
      }
    }
  }
}

public final class GetSpecOptionQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSpecOption($specNo: String!) {
      specOption(specNo: $specNo) {
        __typename
        title
        description
      }
    }
    """

  public let operationName: String = "getSpecOption"

  public var specNo: String

  public init(specNo: String) {
    self.specNo = specNo
  }

  public var variables: GraphQLMap? {
    return ["specNo": specNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("specOption", arguments: ["specNo": GraphQLVariable("specNo")], type: .object(SpecOption.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(specOption: SpecOption? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "specOption": specOption.flatMap { (value: SpecOption) -> ResultMap in value.resultMap }])
    }

    /// 스펙 옵션 조회
    public var specOption: SpecOption? {
      get {
        return (resultMap["specOption"] as? ResultMap).flatMap { SpecOption(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "specOption")
      }
    }

    public struct SpecOption: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Spec"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(title: String? = nil, description: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Spec", "title": title, "description": description])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }
    }
  }
}

public final class GetSpecPhotoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSpecPhoto($specNo: String!, $userNo: String!, $stepSeq: Int!) {
      memberSpecPhotoList(specNo: $specNo, userNo: $userNo, stepSeq: $stepSeq) {
        __typename
        photoNo
        photoSeq
        photoUrl
        stepSeq
        rejectMsg
        rejectYn
      }
    }
    """

  public let operationName: String = "getSpecPhoto"

  public var specNo: String
  public var userNo: String
  public var stepSeq: Int

  public init(specNo: String, userNo: String, stepSeq: Int) {
    self.specNo = specNo
    self.userNo = userNo
    self.stepSeq = stepSeq
  }

  public var variables: GraphQLMap? {
    return ["specNo": specNo, "userNo": userNo, "stepSeq": stepSeq]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberSpecPhotoList", arguments: ["specNo": GraphQLVariable("specNo"), "userNo": GraphQLVariable("userNo"), "stepSeq": GraphQLVariable("stepSeq")], type: .list(.object(MemberSpecPhotoList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberSpecPhotoList: [MemberSpecPhotoList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberSpecPhotoList": memberSpecPhotoList.flatMap { (value: [MemberSpecPhotoList?]) -> [ResultMap?] in value.map { (value: MemberSpecPhotoList?) -> ResultMap? in value.flatMap { (value: MemberSpecPhotoList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 스펙 사진 리스트 조회
    public var memberSpecPhotoList: [MemberSpecPhotoList?]? {
      get {
        return (resultMap["memberSpecPhotoList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberSpecPhotoList?] in value.map { (value: ResultMap?) -> MemberSpecPhotoList? in value.flatMap { (value: ResultMap) -> MemberSpecPhotoList in MemberSpecPhotoList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberSpecPhotoList?]) -> [ResultMap?] in value.map { (value: MemberSpecPhotoList?) -> ResultMap? in value.flatMap { (value: MemberSpecPhotoList) -> ResultMap in value.resultMap } } }, forKey: "memberSpecPhotoList")
      }
    }

    public struct MemberSpecPhotoList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SpecPhotoModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("photoNo", type: .scalar(String.self)),
          GraphQLField("photoSeq", type: .scalar(Int.self)),
          GraphQLField("photoUrl", type: .scalar(String.self)),
          GraphQLField("stepSeq", type: .scalar(Int.self)),
          GraphQLField("rejectMsg", type: .scalar(String.self)),
          GraphQLField("rejectYn", type: .nonNull(.scalar(Bool.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(photoNo: String? = nil, photoSeq: Int? = nil, photoUrl: String? = nil, stepSeq: Int? = nil, rejectMsg: String? = nil, rejectYn: Bool) {
        self.init(unsafeResultMap: ["__typename": "SpecPhotoModel", "photoNo": photoNo, "photoSeq": photoSeq, "photoUrl": photoUrl, "stepSeq": stepSeq, "rejectMsg": rejectMsg, "rejectYn": rejectYn])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var photoNo: String? {
        get {
          return resultMap["photoNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoNo")
        }
      }

      public var photoSeq: Int? {
        get {
          return resultMap["photoSeq"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoSeq")
        }
      }

      public var photoUrl: String? {
        get {
          return resultMap["photoUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoUrl")
        }
      }

      public var stepSeq: Int? {
        get {
          return resultMap["stepSeq"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "stepSeq")
        }
      }

      public var rejectMsg: String? {
        get {
          return resultMap["rejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "rejectMsg")
        }
      }

      public var rejectYn: Bool {
        get {
          return resultMap["rejectYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "rejectYn")
        }
      }
    }
  }
}

public final class DelSpecMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation delSpec($userNo: String!, $specNo: String!) {
      deleteMemberSpec(userNo: $userNo, specNo: $specNo)
    }
    """

  public let operationName: String = "delSpec"

  public var userNo: String
  public var specNo: String

  public init(userNo: String, specNo: String) {
    self.userNo = userNo
    self.specNo = specNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo, "specNo": specNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("deleteMemberSpec", arguments: ["userNo": GraphQLVariable("userNo"), "specNo": GraphQLVariable("specNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteMemberSpec: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteMemberSpec": deleteMemberSpec])
    }

    /// 회원 스펙 삭제
    public var deleteMemberSpec: Bool {
      get {
        return resultMap["deleteMemberSpec"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "deleteMemberSpec")
      }
    }
  }
}

public final class CompleteSpecMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation completeSpec($userNo: String!) {
      saveMemberSpecEnd(userNo: $userNo)
    }
    """

  public let operationName: String = "completeSpec"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberSpecEnd", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberSpecEnd: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberSpecEnd": saveMemberSpecEnd])
    }

    /// 회원 스펙완료 저장
    public var saveMemberSpecEnd: Bool {
      get {
        return resultMap["saveMemberSpecEnd"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberSpecEnd")
      }
    }
  }
}

public final class DelAllSpecMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation delAllSpec($userNo: String!) {
      deleteMemberSpecAll(userNo: $userNo)
    }
    """

  public let operationName: String = "delAllSpec"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("deleteMemberSpecAll", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteMemberSpecAll: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "deleteMemberSpecAll": deleteMemberSpecAll])
    }

    /// 모든 회원 스펙 삭제
    public var deleteMemberSpecAll: Bool {
      get {
        return resultMap["deleteMemberSpecAll"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "deleteMemberSpecAll")
      }
    }
  }
}

public final class CancelSpecMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation cancelSpec($userNo: String!) {
      saveMemberSpecCancel(userNo: $userNo)
    }
    """

  public let operationName: String = "cancelSpec"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberSpecCancel", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberSpecCancel: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberSpecCancel": saveMemberSpecCancel])
    }

    /// 회원 스펙취소 저장
    public var saveMemberSpecCancel: Bool {
      get {
        return resultMap["saveMemberSpecCancel"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberSpecCancel")
      }
    }
  }
}

public final class CompletePhotoMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation completePhoto($userNo: String!) {
      saveMemberPhotoEnd(userNo: $userNo)
    }
    """

  public let operationName: String = "completePhoto"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberPhotoEnd", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberPhotoEnd: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberPhotoEnd": saveMemberPhotoEnd])
    }

    /// 회원 사진 등록 완료 저장
    public var saveMemberPhotoEnd: Bool {
      get {
        return resultMap["saveMemberPhotoEnd"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberPhotoEnd")
      }
    }
  }
}

public final class GetProhibitWordQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getProhibitWord {
      prohibitWordList {
        __typename
        word
      }
    }
    """

  public let operationName: String = "getProhibitWord"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("prohibitWordList", type: .list(.object(ProhibitWordList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(prohibitWordList: [ProhibitWordList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "prohibitWordList": prohibitWordList.flatMap { (value: [ProhibitWordList?]) -> [ResultMap?] in value.map { (value: ProhibitWordList?) -> ResultMap? in value.flatMap { (value: ProhibitWordList) -> ResultMap in value.resultMap } } }])
    }

    /// 금칙어 리스트 조회
    public var prohibitWordList: [ProhibitWordList?]? {
      get {
        return (resultMap["prohibitWordList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProhibitWordList?] in value.map { (value: ResultMap?) -> ProhibitWordList? in value.flatMap { (value: ResultMap) -> ProhibitWordList in ProhibitWordList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [ProhibitWordList?]) -> [ResultMap?] in value.map { (value: ProhibitWordList?) -> ResultMap? in value.flatMap { (value: ProhibitWordList) -> ResultMap in value.resultMap } } }, forKey: "prohibitWordList")
      }
    }

    public struct ProhibitWordList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ProhibitWordModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("word", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(word: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ProhibitWordModel", "word": word])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var word: String? {
        get {
          return resultMap["word"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "word")
        }
      }
    }
  }
}

public final class GetRejectGroupListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getRejectGroupList($userNo: String!) {
      memberRejectGroupList(userNo: $userNo) {
        __typename
        rejectHeader
        rejectList {
          __typename
          title
          rejectType
          specNo
          interviewSeq
        }
      }
    }
    """

  public let operationName: String = "getRejectGroupList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberRejectGroupList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberRejectGroupList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberRejectGroupList: [MemberRejectGroupList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberRejectGroupList": memberRejectGroupList.flatMap { (value: [MemberRejectGroupList?]) -> [ResultMap?] in value.map { (value: MemberRejectGroupList?) -> ResultMap? in value.flatMap { (value: MemberRejectGroupList) -> ResultMap in value.resultMap } } }])
    }

    /// 반려 리스트 조회
    public var memberRejectGroupList: [MemberRejectGroupList?]? {
      get {
        return (resultMap["memberRejectGroupList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberRejectGroupList?] in value.map { (value: ResultMap?) -> MemberRejectGroupList? in value.flatMap { (value: ResultMap) -> MemberRejectGroupList in MemberRejectGroupList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberRejectGroupList?]) -> [ResultMap?] in value.map { (value: MemberRejectGroupList?) -> ResultMap? in value.flatMap { (value: MemberRejectGroupList) -> ResultMap in value.resultMap } } }, forKey: "memberRejectGroupList")
      }
    }

    public struct MemberRejectGroupList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["RejectGroupModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("rejectHeader", type: .scalar(String.self)),
          GraphQLField("rejectList", type: .list(.object(RejectList.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(rejectHeader: String? = nil, rejectList: [RejectList?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "RejectGroupModel", "rejectHeader": rejectHeader, "rejectList": rejectList.flatMap { (value: [RejectList?]) -> [ResultMap?] in value.map { (value: RejectList?) -> ResultMap? in value.flatMap { (value: RejectList) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var rejectHeader: String? {
        get {
          return resultMap["rejectHeader"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "rejectHeader")
        }
      }

      public var rejectList: [RejectList?]? {
        get {
          return (resultMap["rejectList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [RejectList?] in value.map { (value: ResultMap?) -> RejectList? in value.flatMap { (value: ResultMap) -> RejectList in RejectList(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [RejectList?]) -> [ResultMap?] in value.map { (value: RejectList?) -> ResultMap? in value.flatMap { (value: RejectList) -> ResultMap in value.resultMap } } }, forKey: "rejectList")
        }
      }

      public struct RejectList: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["RejectModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("title", type: .scalar(String.self)),
            GraphQLField("rejectType", type: .scalar(RejectType.self)),
            GraphQLField("specNo", type: .scalar(String.self)),
            GraphQLField("interviewSeq", type: .scalar(Int.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(title: String? = nil, rejectType: RejectType? = nil, specNo: String? = nil, interviewSeq: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "RejectModel", "title": title, "rejectType": rejectType, "specNo": specNo, "interviewSeq": interviewSeq])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var title: String? {
          get {
            return resultMap["title"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        public var rejectType: RejectType? {
          get {
            return resultMap["rejectType"] as? RejectType
          }
          set {
            resultMap.updateValue(newValue, forKey: "rejectType")
          }
        }

        public var specNo: String? {
          get {
            return resultMap["specNo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "specNo")
          }
        }

        public var interviewSeq: Int? {
          get {
            return resultMap["interviewSeq"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "interviewSeq")
          }
        }
      }
    }
  }
}

public final class CompleteSpecRejectMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation completeSpecReject($userNo: String!) {
      saveMemberSpecRejectEnd(userNo: $userNo)
    }
    """

  public let operationName: String = "completeSpecReject"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberSpecRejectEnd", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberSpecRejectEnd: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberSpecRejectEnd": saveMemberSpecRejectEnd])
    }

    /// 회원 스펙 반려 완료 저장
    public var saveMemberSpecRejectEnd: Bool {
      get {
        return resultMap["saveMemberSpecRejectEnd"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberSpecRejectEnd")
      }
    }
  }
}

public final class CompleteMemberPhotoRejectMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation completeMemberPhotoReject($userNo: String!) {
      saveMemberPhotoRejectEnd(userNo: $userNo)
    }
    """

  public let operationName: String = "completeMemberPhotoReject"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberPhotoRejectEnd", arguments: ["userNo": GraphQLVariable("userNo")], type: .scalar(Bool.self)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberPhotoRejectEnd: Bool? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberPhotoRejectEnd": saveMemberPhotoRejectEnd])
    }

    /// 회원 사진 리젝 완료 저장
    public var saveMemberPhotoRejectEnd: Bool? {
      get {
        return resultMap["saveMemberPhotoRejectEnd"] as? Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberPhotoRejectEnd")
      }
    }
  }
}

public final class GetUnivListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getUnivList($keyword: String!) {
      universityList(keyword: $keyword) {
        __typename
        univNo
        name
      }
    }
    """

  public let operationName: String = "getUnivList"

  public var keyword: String

  public init(keyword: String) {
    self.keyword = keyword
  }

  public var variables: GraphQLMap? {
    return ["keyword": keyword]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("universityList", arguments: ["keyword": GraphQLVariable("keyword")], type: .list(.object(UniversityList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(universityList: [UniversityList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "universityList": universityList.flatMap { (value: [UniversityList?]) -> [ResultMap?] in value.map { (value: UniversityList?) -> ResultMap? in value.flatMap { (value: UniversityList) -> ResultMap in value.resultMap } } }])
    }

    /// 학교 리스트 조회
    public var universityList: [UniversityList?]? {
      get {
        return (resultMap["universityList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [UniversityList?] in value.map { (value: ResultMap?) -> UniversityList? in value.flatMap { (value: ResultMap) -> UniversityList in UniversityList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [UniversityList?]) -> [ResultMap?] in value.map { (value: UniversityList?) -> ResultMap? in value.flatMap { (value: UniversityList) -> ResultMap in value.resultMap } } }, forKey: "universityList")
      }
    }

    public struct UniversityList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UniversityModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("univNo", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(univNo: String? = nil, name: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UniversityModel", "univNo": univNo, "name": name])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var univNo: String? {
        get {
          return resultMap["univNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "univNo")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class GetCompanyListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getCompanyList($keyword: String!) {
      companyList(keyword: $keyword) {
        __typename
        coNo
        name
      }
    }
    """

  public let operationName: String = "getCompanyList"

  public var keyword: String

  public init(keyword: String) {
    self.keyword = keyword
  }

  public var variables: GraphQLMap? {
    return ["keyword": keyword]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("companyList", arguments: ["keyword": GraphQLVariable("keyword")], type: .list(.object(CompanyList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(companyList: [CompanyList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "companyList": companyList.flatMap { (value: [CompanyList?]) -> [ResultMap?] in value.map { (value: CompanyList?) -> ResultMap? in value.flatMap { (value: CompanyList) -> ResultMap in value.resultMap } } }])
    }

    /// 직장 리스트 조회
    public var companyList: [CompanyList?]? {
      get {
        return (resultMap["companyList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CompanyList?] in value.map { (value: ResultMap?) -> CompanyList? in value.flatMap { (value: ResultMap) -> CompanyList in CompanyList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [CompanyList?]) -> [ResultMap?] in value.map { (value: CompanyList?) -> ResultMap? in value.flatMap { (value: CompanyList) -> ResultMap in value.resultMap } } }, forKey: "companyList")
      }
    }

    public struct CompanyList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CompanyModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("coNo", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(coNo: String? = nil, name: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "CompanyModel", "coNo": coNo, "name": name])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var coNo: String? {
        get {
          return resultMap["coNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "coNo")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class GetReadyCrownQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getReadyCrown($userNo: String!) {
      ready(userNo: $userNo) {
        __typename
        joinAddProfileCrown
        joinUnivCoCrown
      }
    }
    """

  public let operationName: String = "getReadyCrown"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("ready", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Ready.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(ready: Ready? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "ready": ready.flatMap { (value: Ready) -> ResultMap in value.resultMap }])
    }

    /// 심사대기 조회
    public var ready: Ready? {
      get {
        return (resultMap["ready"] as? ResultMap).flatMap { Ready(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "ready")
      }
    }

    public struct Ready: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ApproveReadyModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("joinAddProfileCrown", type: .nonNull(.scalar(Int.self))),
          GraphQLField("joinUnivCoCrown", type: .nonNull(.scalar(Int.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(joinAddProfileCrown: Int, joinUnivCoCrown: Int) {
        self.init(unsafeResultMap: ["__typename": "ApproveReadyModel", "joinAddProfileCrown": joinAddProfileCrown, "joinUnivCoCrown": joinUnivCoCrown])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var joinAddProfileCrown: Int {
        get {
          return resultMap["joinAddProfileCrown"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinAddProfileCrown")
        }
      }

      public var joinUnivCoCrown: Int {
        get {
          return resultMap["joinUnivCoCrown"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinUnivCoCrown")
        }
      }
    }
  }
}

public final class GetMemberUnivAndCoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMemberUnivAndCo($userNo: String!) {
      memberUniversityAndCompany(userNo: $userNo) {
        __typename
        univNo
        univ
        univStatus
        univRejectYn
        univRejectMsg
        univPhotos {
          __typename
          photoSeq
          photoUrl
          rejectMsg
          rejectYn
        }
        companyNo
        company
        companyStatus
        companyRejectYn
        companyRejectMsg
        companyPhotos {
          __typename
          photoSeq
          photoUrl
          rejectMsg
          rejectYn
        }
      }
    }
    """

  public let operationName: String = "getMemberUnivAndCo"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberUniversityAndCompany", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(MemberUniversityAndCompany.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberUniversityAndCompany: MemberUniversityAndCompany? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberUniversityAndCompany": memberUniversityAndCompany.flatMap { (value: MemberUniversityAndCompany) -> ResultMap in value.resultMap }])
    }

    /// 학교,직장 인증 조회
    public var memberUniversityAndCompany: MemberUniversityAndCompany? {
      get {
        return (resultMap["memberUniversityAndCompany"] as? ResultMap).flatMap { MemberUniversityAndCompany(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "memberUniversityAndCompany")
      }
    }

    public struct MemberUniversityAndCompany: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberUniversityAndCompanyModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("univNo", type: .scalar(String.self)),
          GraphQLField("univ", type: .scalar(String.self)),
          GraphQLField("univStatus", type: .scalar(String.self)),
          GraphQLField("univRejectYn", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("univRejectMsg", type: .scalar(String.self)),
          GraphQLField("univPhotos", type: .list(.object(UnivPhoto.selections))),
          GraphQLField("companyNo", type: .scalar(String.self)),
          GraphQLField("company", type: .scalar(String.self)),
          GraphQLField("companyStatus", type: .scalar(String.self)),
          GraphQLField("companyRejectYn", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("companyRejectMsg", type: .scalar(String.self)),
          GraphQLField("companyPhotos", type: .list(.object(CompanyPhoto.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(univNo: String? = nil, univ: String? = nil, univStatus: String? = nil, univRejectYn: Bool, univRejectMsg: String? = nil, univPhotos: [UnivPhoto?]? = nil, companyNo: String? = nil, company: String? = nil, companyStatus: String? = nil, companyRejectYn: Bool, companyRejectMsg: String? = nil, companyPhotos: [CompanyPhoto?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberUniversityAndCompanyModel", "univNo": univNo, "univ": univ, "univStatus": univStatus, "univRejectYn": univRejectYn, "univRejectMsg": univRejectMsg, "univPhotos": univPhotos.flatMap { (value: [UnivPhoto?]) -> [ResultMap?] in value.map { (value: UnivPhoto?) -> ResultMap? in value.flatMap { (value: UnivPhoto) -> ResultMap in value.resultMap } } }, "companyNo": companyNo, "company": company, "companyStatus": companyStatus, "companyRejectYn": companyRejectYn, "companyRejectMsg": companyRejectMsg, "companyPhotos": companyPhotos.flatMap { (value: [CompanyPhoto?]) -> [ResultMap?] in value.map { (value: CompanyPhoto?) -> ResultMap? in value.flatMap { (value: CompanyPhoto) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var univNo: String? {
        get {
          return resultMap["univNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "univNo")
        }
      }

      public var univ: String? {
        get {
          return resultMap["univ"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "univ")
        }
      }

      public var univStatus: String? {
        get {
          return resultMap["univStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "univStatus")
        }
      }

      public var univRejectYn: Bool {
        get {
          return resultMap["univRejectYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "univRejectYn")
        }
      }

      public var univRejectMsg: String? {
        get {
          return resultMap["univRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "univRejectMsg")
        }
      }

      public var univPhotos: [UnivPhoto?]? {
        get {
          return (resultMap["univPhotos"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [UnivPhoto?] in value.map { (value: ResultMap?) -> UnivPhoto? in value.flatMap { (value: ResultMap) -> UnivPhoto in UnivPhoto(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [UnivPhoto?]) -> [ResultMap?] in value.map { (value: UnivPhoto?) -> ResultMap? in value.flatMap { (value: UnivPhoto) -> ResultMap in value.resultMap } } }, forKey: "univPhotos")
        }
      }

      public var companyNo: String? {
        get {
          return resultMap["companyNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "companyNo")
        }
      }

      public var company: String? {
        get {
          return resultMap["company"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "company")
        }
      }

      public var companyStatus: String? {
        get {
          return resultMap["companyStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "companyStatus")
        }
      }

      public var companyRejectYn: Bool {
        get {
          return resultMap["companyRejectYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "companyRejectYn")
        }
      }

      public var companyRejectMsg: String? {
        get {
          return resultMap["companyRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "companyRejectMsg")
        }
      }

      public var companyPhotos: [CompanyPhoto?]? {
        get {
          return (resultMap["companyPhotos"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [CompanyPhoto?] in value.map { (value: ResultMap?) -> CompanyPhoto? in value.flatMap { (value: ResultMap) -> CompanyPhoto in CompanyPhoto(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [CompanyPhoto?]) -> [ResultMap?] in value.map { (value: CompanyPhoto?) -> ResultMap? in value.flatMap { (value: CompanyPhoto) -> ResultMap in value.resultMap } } }, forKey: "companyPhotos")
        }
      }

      public struct UnivPhoto: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["MemberUniversityPhotoModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("photoSeq", type: .scalar(Int.self)),
            GraphQLField("photoUrl", type: .scalar(String.self)),
            GraphQLField("rejectMsg", type: .scalar(String.self)),
            GraphQLField("rejectYn", type: .nonNull(.scalar(Bool.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(photoSeq: Int? = nil, photoUrl: String? = nil, rejectMsg: String? = nil, rejectYn: Bool) {
          self.init(unsafeResultMap: ["__typename": "MemberUniversityPhotoModel", "photoSeq": photoSeq, "photoUrl": photoUrl, "rejectMsg": rejectMsg, "rejectYn": rejectYn])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var photoSeq: Int? {
          get {
            return resultMap["photoSeq"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "photoSeq")
          }
        }

        public var photoUrl: String? {
          get {
            return resultMap["photoUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "photoUrl")
          }
        }

        public var rejectMsg: String? {
          get {
            return resultMap["rejectMsg"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "rejectMsg")
          }
        }

        public var rejectYn: Bool {
          get {
            return resultMap["rejectYn"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "rejectYn")
          }
        }
      }

      public struct CompanyPhoto: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["MemberCompanyPhotoModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("photoSeq", type: .scalar(Int.self)),
            GraphQLField("photoUrl", type: .scalar(String.self)),
            GraphQLField("rejectMsg", type: .scalar(String.self)),
            GraphQLField("rejectYn", type: .nonNull(.scalar(Bool.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(photoSeq: Int? = nil, photoUrl: String? = nil, rejectMsg: String? = nil, rejectYn: Bool) {
          self.init(unsafeResultMap: ["__typename": "MemberCompanyPhotoModel", "photoSeq": photoSeq, "photoUrl": photoUrl, "rejectMsg": rejectMsg, "rejectYn": rejectYn])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var photoSeq: Int? {
          get {
            return resultMap["photoSeq"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "photoSeq")
          }
        }

        public var photoUrl: String? {
          get {
            return resultMap["photoUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "photoUrl")
          }
        }

        public var rejectMsg: String? {
          get {
            return resultMap["rejectMsg"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "rejectMsg")
          }
        }

        public var rejectYn: Bool {
          get {
            return resultMap["rejectYn"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "rejectYn")
          }
        }
      }
    }
  }
}

public final class GetMainInfoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMainInfo($userNo: String!) {
      mainInfo(userNo: $userNo) {
        __typename
        joinPath
        reco
        mainNotice {
          __typename
          noticeNo
          title
          content
        }
      }
    }
    """

  public let operationName: String = "getMainInfo"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("mainInfo", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(MainInfo.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(mainInfo: MainInfo? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "mainInfo": mainInfo.flatMap { (value: MainInfo) -> ResultMap in value.resultMap }])
    }

    /// Main 조회
    public var mainInfo: MainInfo? {
      get {
        return (resultMap["mainInfo"] as? ResultMap).flatMap { MainInfo(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "mainInfo")
      }
    }

    public struct MainInfo: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MainModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("joinPath", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("reco", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("mainNotice", type: .object(MainNotice.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(joinPath: Bool, reco: Bool, mainNotice: MainNotice? = nil) {
        self.init(unsafeResultMap: ["__typename": "MainModel", "joinPath": joinPath, "reco": reco, "mainNotice": mainNotice.flatMap { (value: MainNotice) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var joinPath: Bool {
        get {
          return resultMap["joinPath"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinPath")
        }
      }

      public var reco: Bool {
        get {
          return resultMap["reco"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "reco")
        }
      }

      public var mainNotice: MainNotice? {
        get {
          return (resultMap["mainNotice"] as? ResultMap).flatMap { MainNotice(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "mainNotice")
        }
      }

      public struct MainNotice: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["MainNoticeModel"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("noticeNo", type: .scalar(String.self)),
            GraphQLField("title", type: .scalar(String.self)),
            GraphQLField("content", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(noticeNo: String? = nil, title: String? = nil, content: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "MainNoticeModel", "noticeNo": noticeNo, "title": title, "content": content])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var noticeNo: String? {
          get {
            return resultMap["noticeNo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "noticeNo")
          }
        }

        public var title: String? {
          get {
            return resultMap["title"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        public var content: String? {
          get {
            return resultMap["content"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }
      }
    }
  }
}

public final class SkipRecoMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation skipReco($userNo: String!) {
      saveMemberRecoSkip(userNo: $userNo)
    }
    """

  public let operationName: String = "skipReco"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberRecoSkip", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberRecoSkip: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberRecoSkip": saveMemberRecoSkip])
    }

    /// 회원 추천인 스킵 저장
    public var saveMemberRecoSkip: Bool {
      get {
        return resultMap["saveMemberRecoSkip"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberRecoSkip")
      }
    }
  }
}

public final class SkipJoinPathMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation skipJoinPath($userNo: String!) {
      saveMemberJoinPathSkip(userNo: $userNo)
    }
    """

  public let operationName: String = "skipJoinPath"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberJoinPathSkip", arguments: ["userNo": GraphQLVariable("userNo")], type: .nonNull(.scalar(Bool.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberJoinPathSkip: Bool) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberJoinPathSkip": saveMemberJoinPathSkip])
    }

    /// 가입경로 스킵 저장
    public var saveMemberJoinPathSkip: Bool {
      get {
        return resultMap["saveMemberJoinPathSkip"]! as! Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "saveMemberJoinPathSkip")
      }
    }
  }
}

public final class GetEventNotiMessageQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getEventNotiMessage {
      eventNotiMessage
    }
    """

  public let operationName: String = "getEventNotiMessage"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("eventNotiMessage", type: .scalar(String.self)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(eventNotiMessage: String? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "eventNotiMessage": eventNotiMessage])
    }

    /// 이벤트 알림 메시지 조회
    public var eventNotiMessage: String? {
      get {
        return resultMap["eventNotiMessage"] as? String
      }
      set {
        resultMap.updateValue(newValue, forKey: "eventNotiMessage")
      }
    }
  }
}

public final class AddUnivKeywordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation addUnivKeyword($keyword: String!) {
      saveUniversityKeyword(keyword: $keyword) {
        __typename
        univNo
      }
    }
    """

  public let operationName: String = "addUnivKeyword"

  public var keyword: String

  public init(keyword: String) {
    self.keyword = keyword
  }

  public var variables: GraphQLMap? {
    return ["keyword": keyword]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveUniversityKeyword", arguments: ["keyword": GraphQLVariable("keyword")], type: .object(SaveUniversityKeyword.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveUniversityKeyword: SaveUniversityKeyword? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveUniversityKeyword": saveUniversityKeyword.flatMap { (value: SaveUniversityKeyword) -> ResultMap in value.resultMap }])
    }

    /// 학교 키워드 등록
    public var saveUniversityKeyword: SaveUniversityKeyword? {
      get {
        return (resultMap["saveUniversityKeyword"] as? ResultMap).flatMap { SaveUniversityKeyword(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "saveUniversityKeyword")
      }
    }

    public struct SaveUniversityKeyword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UniversityModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("univNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(univNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UniversityModel", "univNo": univNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var univNo: String? {
        get {
          return resultMap["univNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "univNo")
        }
      }
    }
  }
}

public final class AddCompanyKeywordMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation addCompanyKeyword($keyword: String!) {
      saveCompanyKeyword(keyword: $keyword) {
        __typename
        coNo
      }
    }
    """

  public let operationName: String = "addCompanyKeyword"

  public var keyword: String

  public init(keyword: String) {
    self.keyword = keyword
  }

  public var variables: GraphQLMap? {
    return ["keyword": keyword]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveCompanyKeyword", arguments: ["keyword": GraphQLVariable("keyword")], type: .object(SaveCompanyKeyword.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveCompanyKeyword: SaveCompanyKeyword? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveCompanyKeyword": saveCompanyKeyword.flatMap { (value: SaveCompanyKeyword) -> ResultMap in value.resultMap }])
    }

    /// 직장 키워드 등록
    public var saveCompanyKeyword: SaveCompanyKeyword? {
      get {
        return (resultMap["saveCompanyKeyword"] as? ResultMap).flatMap { SaveCompanyKeyword(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "saveCompanyKeyword")
      }
    }

    public struct SaveCompanyKeyword: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CompanyModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("coNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(coNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "CompanyModel", "coNo": coNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var coNo: String? {
        get {
          return resultMap["coNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "coNo")
        }
      }
    }
  }
}

public final class GetMyCrownQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMyCrown($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        crown
      }
    }
    """

  public let operationName: String = "getMyCrown"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("crown", type: .scalar(Int.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(crown: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "crown": crown])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var crown: Int? {
        get {
          return resultMap["crown"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "crown")
        }
      }
    }
  }
}

public final class GetEmailInfoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getEmailInfo($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        userId
        mbNo
        nickname
      }
    }
    """

  public let operationName: String = "getEmailInfo"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("mbNo", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil, mbNo: String? = nil, nickname: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "userId": userId, "mbNo": mbNo, "nickname": nickname])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var mbNo: String? {
        get {
          return resultMap["mbNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "mbNo")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }
    }
  }
}

public final class GetMyPhotoUrlQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMyPhotoUrl($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        photoUrl
      }
    }
    """

  public let operationName: String = "getMyPhotoUrl"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("photoUrl", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(photoUrl: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "photoUrl": photoUrl])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var photoUrl: String? {
        get {
          return resultMap["photoUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoUrl")
        }
      }
    }
  }
}

public final class GetSettingsInfoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSettingsInfo($userNo: String) {
      member(userNo: $userNo) {
        __typename
        userId
        nickname
        mbNo
      }
    }
    """

  public let operationName: String = "getSettingsInfo"

  public var userNo: String?

  public init(userNo: String? = nil) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("mbNo", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil, nickname: String? = nil, mbNo: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "userId": userId, "nickname": nickname, "mbNo": mbNo])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }

      public var mbNo: String? {
        get {
          return resultMap["mbNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "mbNo")
        }
      }
    }
  }
}

public final class GetMemberRecoCdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMemberRecoCd($userNo: String!) {
      memberRecoCd(userNo: $userNo) {
        __typename
        recoCd
        doRecoCd
        receiveCount
      }
    }
    """

  public let operationName: String = "getMemberRecoCd"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberRecoCd", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(MemberRecoCd.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberRecoCd: MemberRecoCd? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberRecoCd": memberRecoCd.flatMap { (value: MemberRecoCd) -> ResultMap in value.resultMap }])
    }

    /// 회원 추천인코드 조회
    public var memberRecoCd: MemberRecoCd? {
      get {
        return (resultMap["memberRecoCd"] as? ResultMap).flatMap { MemberRecoCd(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "memberRecoCd")
      }
    }

    public struct MemberRecoCd: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["RecoModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("recoCd", type: .scalar(String.self)),
          GraphQLField("doRecoCd", type: .scalar(String.self)),
          GraphQLField("receiveCount", type: .scalar(Int.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(recoCd: String? = nil, doRecoCd: String? = nil, receiveCount: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "RecoModel", "recoCd": recoCd, "doRecoCd": doRecoCd, "receiveCount": receiveCount])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var recoCd: String? {
        get {
          return resultMap["recoCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "recoCd")
        }
      }

      public var doRecoCd: String? {
        get {
          return resultMap["doRecoCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "doRecoCd")
        }
      }

      public var receiveCount: Int? {
        get {
          return resultMap["receiveCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiveCount")
        }
      }
    }
  }
}

public final class GetMyPageInfoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMyPageInfo($userNo: String) {
      member(userNo: $userNo) {
        __typename
        photoUrl
        nickname
        joinStep
        crown
        recoCd
      }
    }
    """

  public let operationName: String = "getMyPageInfo"

  public var userNo: String?

  public init(userNo: String? = nil) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("photoUrl", type: .scalar(String.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("joinStep", type: .nonNull(.scalar(Int.self))),
          GraphQLField("crown", type: .scalar(Int.self)),
          GraphQLField("recoCd", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(photoUrl: String? = nil, nickname: String? = nil, joinStep: Int, crown: Int? = nil, recoCd: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "photoUrl": photoUrl, "nickname": nickname, "joinStep": joinStep, "crown": crown, "recoCd": recoCd])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var photoUrl: String? {
        get {
          return resultMap["photoUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoUrl")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }

      public var joinStep: Int {
        get {
          return resultMap["joinStep"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinStep")
        }
      }

      public var crown: Int? {
        get {
          return resultMap["crown"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "crown")
        }
      }

      public var recoCd: String? {
        get {
          return resultMap["recoCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "recoCd")
        }
      }
    }
  }
}

public final class GetMemberPushSetListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMemberPushSetList($userNo: String!) {
      memberPushSetList(userNo: $userNo) {
        __typename
        setNo
        title
        sendYn
      }
    }
    """

  public let operationName: String = "getMemberPushSetList"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("memberPushSetList", arguments: ["userNo": GraphQLVariable("userNo")], type: .list(.object(MemberPushSetList.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(memberPushSetList: [MemberPushSetList?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "memberPushSetList": memberPushSetList.flatMap { (value: [MemberPushSetList?]) -> [ResultMap?] in value.map { (value: MemberPushSetList?) -> ResultMap? in value.flatMap { (value: MemberPushSetList) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 푸시설정 리스트 조회
    public var memberPushSetList: [MemberPushSetList?]? {
      get {
        return (resultMap["memberPushSetList"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [MemberPushSetList?] in value.map { (value: ResultMap?) -> MemberPushSetList? in value.flatMap { (value: ResultMap) -> MemberPushSetList in MemberPushSetList(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [MemberPushSetList?]) -> [ResultMap?] in value.map { (value: MemberPushSetList?) -> ResultMap? in value.flatMap { (value: MemberPushSetList) -> ResultMap in value.resultMap } } }, forKey: "memberPushSetList")
      }
    }

    public struct MemberPushSetList: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PushSetModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("setNo", type: .scalar(String.self)),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("sendYn", type: .nonNull(.scalar(Bool.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(setNo: String? = nil, title: String? = nil, sendYn: Bool) {
        self.init(unsafeResultMap: ["__typename": "PushSetModel", "setNo": setNo, "title": title, "sendYn": sendYn])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var setNo: String? {
        get {
          return resultMap["setNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "setNo")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var sendYn: Bool {
        get {
          return resultMap["sendYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "sendYn")
        }
      }
    }
  }
}

public final class SaveMemberPushSetMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation saveMemberPushSet($input: PushSetMutationModelInput) {
      saveMemberPushSet(memberPushSet: $input) {
        __typename
        setNo
        title
        sendYn
      }
    }
    """

  public let operationName: String = "saveMemberPushSet"

  public var input: PushSetMutationModelInput?

  public init(input: PushSetMutationModelInput? = nil) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("saveMemberPushSet", arguments: ["memberPushSet": GraphQLVariable("input")], type: .list(.object(SaveMemberPushSet.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(saveMemberPushSet: [SaveMemberPushSet?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "saveMemberPushSet": saveMemberPushSet.flatMap { (value: [SaveMemberPushSet?]) -> [ResultMap?] in value.map { (value: SaveMemberPushSet?) -> ResultMap? in value.flatMap { (value: SaveMemberPushSet) -> ResultMap in value.resultMap } } }])
    }

    /// 회원 푸시설정 저장
    public var saveMemberPushSet: [SaveMemberPushSet?]? {
      get {
        return (resultMap["saveMemberPushSet"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [SaveMemberPushSet?] in value.map { (value: ResultMap?) -> SaveMemberPushSet? in value.flatMap { (value: ResultMap) -> SaveMemberPushSet in SaveMemberPushSet(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [SaveMemberPushSet?]) -> [ResultMap?] in value.map { (value: SaveMemberPushSet?) -> ResultMap? in value.flatMap { (value: SaveMemberPushSet) -> ResultMap in value.resultMap } } }, forKey: "saveMemberPushSet")
      }
    }

    public struct SaveMemberPushSet: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PushSetModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("setNo", type: .scalar(String.self)),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("sendYn", type: .nonNull(.scalar(Bool.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(setNo: String? = nil, title: String? = nil, sendYn: Bool) {
        self.init(unsafeResultMap: ["__typename": "PushSetModel", "setNo": setNo, "title": title, "sendYn": sendYn])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var setNo: String? {
        get {
          return resultMap["setNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "setNo")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var sendYn: Bool {
        get {
          return resultMap["sendYn"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "sendYn")
        }
      }
    }
  }
}

public final class GetLatestAppVersionQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getLatestAppVersion($deviceType: String!) {
      latestAppVersion(deviceType: $deviceType)
    }
    """

  public let operationName: String = "getLatestAppVersion"

  public var deviceType: String

  public init(deviceType: String) {
    self.deviceType = deviceType
  }

  public var variables: GraphQLMap? {
    return ["deviceType": deviceType]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("latestAppVersion", arguments: ["deviceType": GraphQLVariable("deviceType")], type: .scalar(String.self)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(latestAppVersion: String? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "latestAppVersion": latestAppVersion])
    }

    /// 최신 앱버전 조회
    public var latestAppVersion: String? {
      get {
        return resultMap["latestAppVersion"] as? String
      }
      set {
        resultMap.updateValue(newValue, forKey: "latestAppVersion")
      }
    }
  }
}

public final class GetMemberDetailQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMemberDetail($userNo: String!) {
      member(userNo: $userNo) {
        __typename
        gender
        nickname
        nicknameRejectMsg
        tall
        bodyCd
        drinkCd
        religionCd
        areaNo
        eduCd
        job
        jobRejectMsg
        aboutMe
        aboutMeRejectMsg
        jobCareer
        joinPathCd
        recoCd
        crown
        userId
        mbNo
        status
        joinStep
        photoUrl
      }
    }
    """

  public let operationName: String = "getMemberDetail"

  public var userNo: String

  public init(userNo: String) {
    self.userNo = userNo
  }

  public var variables: GraphQLMap? {
    return ["userNo": userNo]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("member", arguments: ["userNo": GraphQLVariable("userNo")], type: .object(Member.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(member: Member? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "member": member.flatMap { (value: Member) -> ResultMap in value.resultMap }])
    }

    /// 회원 조회
    public var member: Member? {
      get {
        return (resultMap["member"] as? ResultMap).flatMap { Member(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "member")
      }
    }

    public struct Member: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MemberModel"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("gender", type: .scalar(GenderType.self)),
          GraphQLField("nickname", type: .scalar(String.self)),
          GraphQLField("nicknameRejectMsg", type: .scalar(String.self)),
          GraphQLField("tall", type: .scalar(Int.self)),
          GraphQLField("bodyCd", type: .scalar(String.self)),
          GraphQLField("drinkCd", type: .scalar(String.self)),
          GraphQLField("religionCd", type: .scalar(String.self)),
          GraphQLField("areaNo", type: .scalar(String.self)),
          GraphQLField("eduCd", type: .scalar(String.self)),
          GraphQLField("job", type: .scalar(String.self)),
          GraphQLField("jobRejectMsg", type: .scalar(String.self)),
          GraphQLField("aboutMe", type: .scalar(String.self)),
          GraphQLField("aboutMeRejectMsg", type: .scalar(String.self)),
          GraphQLField("jobCareer", type: .scalar(String.self)),
          GraphQLField("joinPathCd", type: .scalar(String.self)),
          GraphQLField("recoCd", type: .scalar(String.self)),
          GraphQLField("crown", type: .scalar(Int.self)),
          GraphQLField("userId", type: .scalar(String.self)),
          GraphQLField("mbNo", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(StatusType.self)),
          GraphQLField("joinStep", type: .nonNull(.scalar(Int.self))),
          GraphQLField("photoUrl", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(gender: GenderType? = nil, nickname: String? = nil, nicknameRejectMsg: String? = nil, tall: Int? = nil, bodyCd: String? = nil, drinkCd: String? = nil, religionCd: String? = nil, areaNo: String? = nil, eduCd: String? = nil, job: String? = nil, jobRejectMsg: String? = nil, aboutMe: String? = nil, aboutMeRejectMsg: String? = nil, jobCareer: String? = nil, joinPathCd: String? = nil, recoCd: String? = nil, crown: Int? = nil, userId: String? = nil, mbNo: String? = nil, status: StatusType? = nil, joinStep: Int, photoUrl: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "MemberModel", "gender": gender, "nickname": nickname, "nicknameRejectMsg": nicknameRejectMsg, "tall": tall, "bodyCd": bodyCd, "drinkCd": drinkCd, "religionCd": religionCd, "areaNo": areaNo, "eduCd": eduCd, "job": job, "jobRejectMsg": jobRejectMsg, "aboutMe": aboutMe, "aboutMeRejectMsg": aboutMeRejectMsg, "jobCareer": jobCareer, "joinPathCd": joinPathCd, "recoCd": recoCd, "crown": crown, "userId": userId, "mbNo": mbNo, "status": status, "joinStep": joinStep, "photoUrl": photoUrl])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var gender: GenderType? {
        get {
          return resultMap["gender"] as? GenderType
        }
        set {
          resultMap.updateValue(newValue, forKey: "gender")
        }
      }

      public var nickname: String? {
        get {
          return resultMap["nickname"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nickname")
        }
      }

      public var nicknameRejectMsg: String? {
        get {
          return resultMap["nicknameRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nicknameRejectMsg")
        }
      }

      public var tall: Int? {
        get {
          return resultMap["tall"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "tall")
        }
      }

      public var bodyCd: String? {
        get {
          return resultMap["bodyCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "bodyCd")
        }
      }

      public var drinkCd: String? {
        get {
          return resultMap["drinkCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "drinkCd")
        }
      }

      public var religionCd: String? {
        get {
          return resultMap["religionCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "religionCd")
        }
      }

      public var areaNo: String? {
        get {
          return resultMap["areaNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "areaNo")
        }
      }

      public var eduCd: String? {
        get {
          return resultMap["eduCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "eduCd")
        }
      }

      public var job: String? {
        get {
          return resultMap["job"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "job")
        }
      }

      public var jobRejectMsg: String? {
        get {
          return resultMap["jobRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "jobRejectMsg")
        }
      }

      public var aboutMe: String? {
        get {
          return resultMap["aboutMe"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "aboutMe")
        }
      }

      public var aboutMeRejectMsg: String? {
        get {
          return resultMap["aboutMeRejectMsg"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "aboutMeRejectMsg")
        }
      }

      public var jobCareer: String? {
        get {
          return resultMap["jobCareer"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "jobCareer")
        }
      }

      public var joinPathCd: String? {
        get {
          return resultMap["joinPathCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinPathCd")
        }
      }

      public var recoCd: String? {
        get {
          return resultMap["recoCd"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "recoCd")
        }
      }

      public var crown: Int? {
        get {
          return resultMap["crown"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "crown")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var mbNo: String? {
        get {
          return resultMap["mbNo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "mbNo")
        }
      }

      public var status: StatusType? {
        get {
          return resultMap["status"] as? StatusType
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var joinStep: Int {
        get {
          return resultMap["joinStep"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinStep")
        }
      }

      public var photoUrl: String? {
        get {
          return resultMap["photoUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "photoUrl")
        }
      }
    }
  }
}
