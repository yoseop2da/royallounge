//
//  AcceptTermsViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AcceptTermsViewController: ClearNaviBaseViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var agreeAllButton: UIButton!

    @IBOutlet weak var agree0Button: UIButton! // 필수
    @IBOutlet weak var agree1Button: UIButton! // 필수
    @IBOutlet weak var agree2Button: UIButton! // 선택

    @IBOutlet weak var agree1WrapView: UIView! // 선택
    @IBOutlet weak var oneYearButton: UIButton!
    @IBOutlet weak var outButton: UIButton!

    @IBOutlet weak var terms0Button: UIButton!
    @IBOutlet weak var terms1Button: UIButton!
    
    @IBOutlet weak var optionViewHieght: NSLayoutConstraint!
    
    var viewModel: AcceptTermsViewModel!
    var auth: AuthModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        agree0Button.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 12.0)
        agree0Button.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 12.0)
        agree0Button.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 12.0)
        agree1WrapView.setBorder(color: UIColor.gray10)
        agree1WrapView.layer.cornerRadius = 7.0
        agreeAllButton.setBorder(color: UIColor.gray10)
        agreeAllButton.layer.cornerRadius = 6.0
        nextButton.layer.cornerRadius = 6.0
        
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }
        
        let barButton = RLBarButtonItem(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                alertDialogTwoButton(title: "", message: "뒤로가시면 본인인증부터\n다시 진행하셔야 합니다.", okTitle: "나가기", okAction: { _ in
                    self.dismiss(animated: true, completion: nil)
                }, cancelTitle: "취소", cancelAction: { _ in })
                
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = AcceptTermsViewModel.init(
            auth: auth,
            agree0SelectedOb: agree0Button.rx.tap.map { !self.agree0Button.isSelected },
            agree1SelectedOb: agree1Button.rx.tap.map { !self.agree1Button.isSelected },
            agree2SelectedOb: agree2Button.rx.tap.map { !self.agree2Button.isSelected },
            agreeAllOb: agreeAllButton.rx.tap
                .rxTouchAnimation(button: agreeAllButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .map {!self.agreeAllButton.isSelected },
            outSelectedOb: outButton.rx.tap.map { !self.outButton.isSelected },
            oneYearSelectedOb: oneYearButton.rx.tap.map { !self.outButton.isSelected },
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable())
        
        viewModel.agree0Selected
            .bind(to: agree0Button.rx.isSelected)
            .disposed(by: bag)

        viewModel.agree1Selected
            .do(onNext: { isSelected in
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                    if isSelected {
                        self.optionViewHieght.constant = 150
                        if UIScreen.underSE { self.setHeaderView(isOpen: true) }
                    } else {
                        self.optionViewHieght.constant = 0.0
                        if UIScreen.underSE { self.setHeaderView(isOpen: false) }
                    }
                    self.view.layoutIfNeeded()
                })
            })
            .bind(to: agree1Button.rx.isSelected)
            .disposed(by: bag)
        viewModel.outSelected
            .bind(to: outButton.rx.isSelected)
            .disposed(by: bag)

        viewModel.showPopup
            .subscribe(onNext: { selected in
                guard selected else { return }
                RLPopup.shared.showAcceptTerms(leftAction: {
                    // 1년
                    self.viewModel.selectedInPopup(isOut: false)
                }, rightAction: {
                    // 탈퇴시
                    self.viewModel.selectedInPopup(isOut: true)
                })
            })
            .disposed(by: bag)

        viewModel.oneYearSelected
            .bind(to: oneYearButton.rx.isSelected)
            .disposed(by: bag)

        viewModel.agree2Selected
            .bind(to: agree2Button.rx.isSelected)
            .disposed(by: bag)
        
        // 서비스 이용약관
        terms0Button.rx.tap
            .rxTouchAnimation(button: terms0Button, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .map { "https://www.routedate.com/policy/service" }
            .subscribe(onNext: { urlString in
                showWebView(self, title: "", url: urlString)
            })
            .disposed(by: bag)
        // 개인정보 취급방침
        terms1Button.rx.tap
            .rxTouchAnimation(button: terms1Button, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .map { "https://www.routedate.com/policy/privacy" }
            .subscribe(onNext: { urlString in
                showWebView(self, title: "", url: urlString)
            })
            .disposed(by: bag)
        
        viewModel.agreeAll
            .skipWhile{ _ in self.agreeAllButton.isSelected }
            .bind(to: agreeAllButton.rx.acceptTermsAgreeAll)
            .disposed(by: bag)

        viewModel.nextEnable
            .observeOn(MainScheduler.instance)
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)

        viewModel.nextSuccess
            .subscribe(onNext: { result in
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy.MM.dd"
                let formatdateStr = formatter.string(from: Date())
                if !result.eventNotiYn {
                    RLPopup.shared.showEvent(reward: self.viewModel.eventPopupString.value, leftAction: {
                        // 미동의
                        toast(message: "\(formatdateStr)\n이벤트 알림 수신 거부 처리되었습니다.", seconds: 1.5, actionAfter: {
                            let vc = RLStoryboard.join.accountViewController(
                                auth: result.auth,
                                eventNotiYn: result.eventNotiYn,
                                infoPeriod: result.infoPeriod)
                            
                            self.push(vc)
                        })
                    }, rightAction: {
                        toast(message: "\(formatdateStr)\n이벤트 알림 수신 동의 처리되었습니다.", seconds: 1.5, actionAfter: {
                            // 동의
                            self.viewModel.agree2Select()
                            self.agree2Button.isSelected = true
                            
                            let vc = RLStoryboard.join.accountViewController(
                                auth: result.auth,
                                eventNotiYn: true,
                                infoPeriod: result.infoPeriod)
                            
                            self.push(vc)
                        })
                    })
                    
                }else{
                    toast(message: "\(formatdateStr)\n이벤트 알림 수신 동의 처리되었습니다.", seconds: 1.5, actionAfter: {
                        let vc = RLStoryboard.join.accountViewController(
                            auth: result.auth,
                            eventNotiYn: result.eventNotiYn,
                            infoPeriod: result.infoPeriod)
                        self.push(vc)
                    })
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavigationControllerBottomLine()
    }
    
    func setHeaderView(isOpen: Bool) {
        if isOpen {
            var frame = self.mainTableView.frame
            frame.size.height += 120
            UIView.animate(withDuration: 0.3, animations: {
                self.headerView.frame = frame
                self.mainTableView.reloadData()
            })
            self.mainTableView.setContentOffset(CGPoint.init(x: 0, y: 50), animated: true)
        } else {
            mainAsync {
                UIView.animate(withDuration: 0.3, animations: {
                    self.headerView.frame = self.mainTableView.frame
                    self.mainTableView.reloadData()
                })
            }
        }
    }
    
}

