//
//  AcceptTermsViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/24.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AcceptTermsViewModel: BaseViewModel {
    let agree0Selected = BehaviorRelay.init(value: false)
    let agree1Selected = BehaviorRelay.init(value: false)
    let agree2Selected = BehaviorRelay.init(value: false)
    
    let showPopup = BehaviorRelay.init(value: false)
    let outSelected = BehaviorRelay.init(value: true)
    let oneYearSelected = BehaviorRelay.init(value: false)
    
    lazy var agreeAll = Observable.combineLatest(agree0Selected, agree1Selected, agree2Selected) { a0, a1, a2 in
        return a0 && a1 && a2
    }
    lazy var nextEnable = Observable.combineLatest(agree0Selected, agree1Selected) { a0, a1 in
        return a0 && a1
    }
//    var nextSuccess = PublishRelay<(auth: AuthModel, eventNoti: (display: Bool, msg: String), infoPeriod: String)>()
    var nextSuccess = PublishRelay<(auth: AuthModel, eventNotiYn: Bool, infoPeriod: String)>()
    let eventPopupString: BehaviorRelay<String> = BehaviorRelay.init(value: "1만원")
    
    init(auth: AuthModel,
         agree0SelectedOb: Observable<Bool>,
         agree1SelectedOb: Observable<Bool>,
         agree2SelectedOb: Observable<Bool>,
         agreeAllOb: Observable<Bool>,
         outSelectedOb: Observable<Bool>,
         oneYearSelectedOb: Observable<Bool>,
         nextButtonTap: Observable<Void>) {
        
        super.init()
        agree0SelectedOb
            .bind(to: agree0Selected)
            .disposed(by: bag)
        agree1SelectedOb
            .bind(to: agree1Selected)
            .disposed(by: bag)
        agree2SelectedOb
            .bind(to: agree2Selected)
            .disposed(by: bag)

        Repository.shared.graphQl
            .getEventNotiMessage()
            .subscribe(onNext: {
                self.eventPopupString.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        agreeAllOb
            .subscribe(onNext: { isSelected in
                self.agree0Selected.accept(true)
                self.agree1Selected.accept(true)
                self.agree2Selected.accept(true)
            })
            .disposed(by: bag)

        outSelectedOb
            .subscribe(onNext: { _ in
                if self.outSelected.value { return }
                self.outSelected.accept(true)
                self.oneYearSelected.accept(false)
            })
            .disposed(by: bag)
        
        oneYearSelectedOb
            .subscribe(onNext: { _ in
                if self.oneYearSelected.value { return }
                self.showPopup.accept(true)
            })
            .disposed(by: bag)
        
        let eventAndPeriod = Observable.combineLatest(agree2Selected, oneYearSelected) { (eventNotiYn: $0, isOneYear: $1)}
        
        nextButtonTap
        .withLatestFrom(eventAndPeriod)
            .map{ (auth: auth, eventNotiYn: $0.eventNotiYn, infoPeriod: $0.isOneYear ? "ONE_YEAR" : "OUT")}
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
    
    func selectedInPopup(isOut: Bool) {
        self.outSelected.accept(isOut)
        self.oneYearSelected.accept(!isOut)
    }
    
    func agree2Select() {
        agree2Selected.accept(true)
    }
}
