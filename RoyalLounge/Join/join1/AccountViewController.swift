//
//  AccountViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AccountViewController: ClearNaviBaseViewController {
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
//    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var idWrapView: UIView!
    @IBOutlet weak var passwordWrapView: UIView!
    @IBOutlet weak var passwordConfirmWrapView: UIView!

    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    let nextButton = RLNextButton.init(title: "계정 생성")
    
//    var infoPeriod = "ONE_YEAR"//, "OUT"
//    var eventNotiYn = false
//    var auth: AuthModel!
    
    var joinInfo: (auth: AuthModel, eventNotiYn: Bool, infoPeriod: String)!

    let idTextField: RLTitleTextField = {
        
        let field = RLTitleTextField.init(placeholder: "이메일 주소를 입력해주세요", title: "아이디", hasSpace: false)
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        return field
    }()

    let passwordTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 입력해주세요", title: "비밀번호", isPassword: true, hasSpace: false)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .next
        return field
    }()
    
    let passwordConfirmTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 한번 더 입력해주세요", title: "비밀번호 확인", isPassword: true, hasSpace: false)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .done
        return field
    }()

    var viewModel: AccountViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }

        idTextField.returnKeyCallback = { self.passwordTextField.becomeFirst = true }
        passwordTextField.returnKeyCallback = { self.passwordConfirmTextField.becomeFirst = true }
        
        titleLabel.text = "계정 생성"
        nextButton.layer.cornerRadius = 6.0

        idWrapView.addSubview(idTextField)
        idTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(idWrapView)
        }

        passwordWrapView.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(passwordWrapView)
        }
        
        passwordConfirmWrapView.addSubview(passwordConfirmTextField)
        passwordConfirmTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(passwordConfirmWrapView)
        }

        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }

        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton

        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.viewModel.join()
            })
            .disposed(by: bag)
        
        viewModel = AccountViewModel.init(
            joinInfo: joinInfo,
            userName: idTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            password: passwordTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            passwordConfirm: passwordConfirmTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.validatedId
            .bind(to: idTextField.rx.setResult)
            .disposed(by: bag)
        
        viewModel.emailError
            .bind(to: idTextField.rx.setResultRegardlessOfFocus)
            .disposed(by: bag)
        
        viewModel.validatedPassword
            .bind(to: passwordTextField.rx.setResult)
            .disposed(by: bag)
        
        viewModel.validatedPasswordConfirm
            .bind(to: passwordConfirmTextField.rx.setResult)
            .disposed(by: bag)

        viewModel.nextEnable
            .bind(to: nextButton.rx.mainButtonEnableState)
            .disposed(by: bag)

//        viewModel.nextSuccess
//            .subscribe(onNext: { joinStep  in
//                Coordinator.shared.moveBy(joinStep: joinStep)
//            })
//            .disposed(by: bag)

        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height == 0 ? 30 : keyboard.height
//                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 88)
                    
                    if keyboard.height == 0 {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }
                    
                    
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)

    }
}
