
//
//  AccountViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AccountViewModel: BaseViewModel {
    let validatedId: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let emailError = PublishSubject<TextFieldResult>.init()
    let validatedPassword: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let validatedPasswordConfirm: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    var nextEnable: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    
    private var userName = ""
    private var password = ""
    private var joinInfo: (auth: AuthModel, eventNotiYn: Bool, infoPeriod: String)!
    
    init(joinInfo: (auth: AuthModel, eventNotiYn: Bool, infoPeriod: String),
         userName: Observable<String>,
         password: Observable<String>,
         passwordConfirm: Observable<String>,
         nextButtonTap: Observable<Void>) {
        super.init()
        self.joinInfo = joinInfo
        userName
            .map {
                self.userName = $0
                return Validation.shared.validate(email: $0)
            }
            .bind(to: validatedId)
            .disposed(by: bag)
        
        password
            .map {
                self.password = $0
                return Validation.shared.validate(password: $0)
            }
            .bind(to: validatedPassword)
            .disposed(by: bag)
        
        Observable.combineLatest(password, passwordConfirm, resultSelector: { Validation.shared.validate(password: $0, confirmPassword: $1) })
            .bind(to: validatedPasswordConfirm)
            .disposed(by: bag)
        
        Observable.combineLatest(validatedId, validatedPassword, validatedPasswordConfirm) { userId, passwordStr, passwordConfirmStr in
            userId.isValid && passwordStr.isValid && passwordConfirmStr.isValid }
            .distinctUntilChanged()
            .bind(to: nextEnable)
            .disposed(by: bag)
    }

    func join() {
        /*
         순차적 진행
         1. 회원가입
         2. 토큰(accessToken & refreshToken) 획득
         3. 현재 가입중인 단계체크 >>>>>> 사실 이단계는 체크 안해도되긴한데,,, 바로 2단계로 이동하면됨!!
         4. 디바이스 정보 등록
         */
        var params: [String : Any] = [
            "userId": userName.aesEncrypted!,
            "pwd": password.aesEncrypted!,
            "name": joinInfo.auth.nameAES,
            "gender": joinInfo.auth.gender,
            "mbNo": joinInfo.auth.mbNoAES,
            "ci": joinInfo.auth.ciAES,
            "birthday": joinInfo.auth.birthdayAES,
            "tellCd": joinInfo.auth.telCo,
            "koreanYn": joinInfo.auth.koreanYn,
            "infoPeriod": joinInfo.infoPeriod,
            "eventNotiYn": joinInfo.eventNotiYn,
            "uuid": ApplicationInfo.adIdentifier.aesEncrypted!,
            "appVersion": ApplicationInfo.appVersion,
            "modelName": ApplicationInfo.deviceModel,
            "osVersion": ApplicationInfo.systemVersion]
        if let pushIdAes = RLUserDefault.shared.pushId?.aesEncrypted {
            params["pushId"] = pushIdAes
        }

        // 1. 회원가입
//        let result = Repository.shared.main
//            .join(params: params)
//            .observeOn(MainScheduler.instance)
//        result
//            .filter{
//            !$0.success && ($0.errorCode == RLResultCD.E01011_EmailDuplicated.code || $0.errorCode == RLResultCD.E01012_EmailNotValidIDPWError.code)
//            }.subscribe(onNext: {
//                self.emailError.on(.next(TextFieldResult.failed(message: $0.errorMsg)))
//            }, onError: {
//                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
//            })
//            .disposed(by: bag)
//
//        // 2. 토큰(accessToken & refreshToken) 획득
//        result
//            .filter{ $0.success && $0.userNoAes != nil }
//            .flatMapLatest{
//                Repository.shared.main
//                    .getToken(userNoAes: $0.userNoAes, userId: self.userName, password: self.password)
//            }
//            .filter { $0 != nil }
//            .subscribe(onNext: { _ in
//                // 3. 현재 가입중인 단계체크 >>>>>> 사실 이단계는 체크 안해도되긴한데,,, 바로 2단계로 이동하면됨!!
//                Coordinator.shared.moveByUserStatus()
//            }, onError: {
//                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
//            })
//            .disposed(by: bag)
        
        // 1. 회원가입
        Repository.shared.main
            .join(params: params)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                // 2. 토큰(accessToken & refreshToken) 획득
                Repository.shared.main
                    .getToken(userNoAes: $0, userId: self.userName, password: self.password)
                    .observeOn(MainScheduler.instance)
                    .filter { $0 != nil }
                    .subscribe(onNext: { _ in
                        // 3. 현재 가입중인 단계체크 >>>>>> 사실 이단계는 체크 안해도되긴한데,,, 바로 2단계로 이동하면됨!!
                        Coordinator.shared.moveByUserStatus()
                    }, onError: {
                        $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                        guard let custom = $0 as? CustomError else { return }
                        self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                    })
                    .disposed(by: self.bag)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                if custom.resultCD == RLResultCD.E01011_EmailDuplicated.code || custom.resultCD == RLResultCD.E01012_EmailNotValidIDPWError.code {
                    self.emailError.on(.next(TextFieldResult.failed(message: custom.errorMessage)))
                }else{
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                }
            })
            .disposed(by: bag)
    }
}
