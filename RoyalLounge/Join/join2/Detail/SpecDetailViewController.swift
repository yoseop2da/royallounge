//
//  SpecDetailViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SnapKit

class SpecDetailViewController: LoggedBaseViewController {

    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var bottomInformationLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<SpecDetailSection>

//    let removeSpecButton = RLBarButtonItem(barType: .title(titleString: "인증 초기화"))
    var removeSpecButton: RLBarButtonItem!
    private var rejectView: RejectView?
    
    let nextButton = RLNextButton.init(title: "저장")
    var info: (title: String, specNo: String, stepSeq: Int, isStep01Edited: Bool, specStatus: SpecModel.SpecStatus, containRejectInSpec: Bool)?
    var refreshCallback: (()->Void)?
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item in
            switch item {
            case .titleDescription(let title, let desc):
                let cell: TitleDescriptionCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = title
                cell.descriptionLabel.text = desc //"인증서 등록 방법안내"
                return cell
            case .headerTitle(let text):
                let cell: HeaderTitleCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = text //"인증서 등록 방법안내"
                return cell
            case .step2Description(let named, let subText, let smallText):
                let cell: Step2DescriptionCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = subText
                
                if UIScreen.underSE { cell.titleLabel.font = UIFont.spoqaHanSansRegular(ofSize: 12) }
                
                cell.subTitleLabel.text = smallText
                cell.stepImageView.image = UIImage.init(named: named)
                return cell
            case .stepTitle(let text, let selectType):
                let cell: StepTitleCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = text //"인증서 등록 방법안내"
                cell.setType(selectType: selectType)
                return cell
            case .stepDescription(let text, let subText):
                let cell: StepDescriptionCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = text
                cell.subTitleLabel.text = subText
                return cell
            case .stepSubDescription(let text, let subText):
                let cell: StepSubDescriptionCell = tableView.dequeueReusable(for: indexPath)
                cell.hiddenTitleLabel.text = text
                cell.subTitleLabel.text = subText
                return cell
            case .stepEnd:
                let cell: StepEndCell = tableView.dequeueReusable(for: indexPath)
                return cell
            case .exampleImage(let name, let imageText):
                let cell: ExampleImageCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = imageText
                cell.exampleImageView.image = UIImage.init(named: name)
                return cell
            case .addPhoto(let placeholders, let addImages):
                let cell: AddPhotoCell = tableView.dequeueReusable(for: indexPath)
                cell.setImage(placeholders: placeholders, addImages: addImages)
                cell.modifyPhoto = { idx, image, data in
                    self.viewModel.modifySpec(idx: idx, image: image, imageData: data)
                }
                cell.addPhoto = { image, data in
                    self.viewModel.addSpecPhoto(image: image, imageData: data)
                }
                cell.remove = { idx in
                    self.viewModel.removeSpecPhoto(idx: idx)
                }
                return cell
            }
        })
        return dataSource
    }
    
    var viewModel: SpecDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let _info = info else {
            // specNo가 없으면 해당 페이지 진입 자체를 막아야함!!!
            self.pushBack()
            return
        }
        let title = _info.title
        let specNo = _info.specNo
        let stepSeq = _info.stepSeq
        let isStep01Edited = _info.isStep01Edited
        let specStatus = _info.specStatus
        let containRejectInSpec = _info.containRejectInSpec
        
        if UIScreen.underSE {
            bottomInformationLabel.font = UIFont.spoqaHanSansRegular(ofSize: 10)
        }
        
        if SpecType.from(specNo: specNo) == .face_good_15 && stepSeq == 1 && !MainInformation.shared.outTester {
            RLPopup.shared.showFaceSpec {}
        }
        
        viewModel = SpecDetailViewModel.init(
            specNo: specNo,
            stepSeq: stepSeq,
            isStep01Edited: isStep01Edited,
            specStatus: specStatus,
            containRejectInSpec: containRejectInSpec,
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
//                self.pushBack()
            })
            .disposed(by: bag)
        
        let backButton = RLBarButtonItem(barType: .back)
        backButton.rx.tap
            .withLatestFrom(viewModel.registerType)
            .subscribe(onNext: { type in
                if stepSeq == 1 {
                    switch type {
                    case .newRegister, .editRegister:
                        self.pushBack()
                    case .newRegisterWithEditing:
                        self.removeSpecWithPopup()
                    case .editRegisterWithEditing:
                        self.backWithPopup(step: stepSeq)
                    }
                }else if stepSeq == 2 {
                    switch type {
                    case .newRegister:
                        self.removeSpecWithPopup()
                    case .editRegister:
                        if isStep01Edited {
                            self.backWithPopup(step: stepSeq)
                        }else{
                            self.pushBackTo(depth: 2)
                        }
                    case .newRegisterWithEditing:
                        self.removeSpecWithPopup()
                    case .editRegisterWithEditing:
                        self.backWithPopup(step: stepSeq)
                    }
                }
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = backButton
        
        removeSpecButton = RLBarButtonItem(barType: .customButton(title: "인증 초기화", font: UIFont.spoqaHanSansBold(ofSize: 14), textColor: .primary100))
        removeSpecButton.customButton?.rx.tap
            .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                RLPopup.shared.showNormal(title: "인증 문서 저장 초기화", description: "초기화 시 등록된 문서는 복원되지 않습니다. 신중하게 선택해주세요.", leftButtonTitle: "취소", rightButtonTitle: "인증 초기화", leftAction: {}, rightAction: {
                    self.viewModel.removeSpec()
                })
            })
            .disposed(by: bag)
        
//        if MainInformation.shared.isJoinOrReject {
//            if specStatus == .normal || specStatus == .ready || specStatus == .reject {
//                self.navigationItem.rightBarButtonItem = self.removeSpecButton
//            }
//        }else{
//            if specStatus == .reject {
//                self.navigationItem.rightBarButtonItem = self.removeSpecButton
//            }
//        }
        if specStatus == .normal || specStatus == .reject {
            self.navigationItem.rightBarButtonItem = self.removeSpecButton
        }
        
            
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        if Validation.shared.isOneStep(specNo: specNo) {
            nextButton.setTitle("저장", for: .normal)
        } else if Validation.shared.isTwoStep(specNo: specNo) {
            if stepSeq == 1 {
                nextButton.setTitle("다음", for: .normal)
            }else{
                nextButton.setTitle("저장", for: .normal)
            }
        } else {
            nextButton.setTitle("저장", for: .normal)
        }
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.removeSpecSuccess
            .subscribe(onNext: { success in
                if success {
                    self.pushBackTo(depth: stepSeq)
                    self.refreshCallback?()
                }
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { nextStepSeq, isStep01Edited in
                if let seq = nextStepSeq {
                    let vc = RLStoryboard.join.specDetailViewController(groupTitle: title, specNo: specNo, stepSeq: seq, isStep01Edited: isStep01Edited, specStatus: specStatus, containRejectInSpec: containRejectInSpec)
                    vc.refreshCallback = self.refreshCallback
                    self.push(vc)
                }else{
                    if !MainInformation.shared.isJoinOrReject {
                        toast(message: "승인 요청되었습니다", seconds: 1.5, actionAfter: nil)
                    }
                    self.pushBackTo(depth: stepSeq)
                    self.refreshCallback?()
                }
            })
            .disposed(by: bag)
        
        rejectView = RejectView(rejectMsg: "가나다라마바사\n아자차카타파하!!", isUpArrow: true)
        footerView.addSubview(rejectView!)
        rejectView!.snp.updateConstraints{
            $0.top.equalToSuperview().offset(-10)
            $0.leading.trailing.width.equalToSuperview()
        }
        
        viewModel.rejectInfo
            .subscribe(onNext: { info in
                self.rejectView?.rejectMsg = info.msg
//                self.rejectView?.isHidden(!info.display, animated: true)
                self.rejectView?.isHidden(!info.display, animated: false)
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = info?.title
    }
    
    func removeSpecWithPopup() {
        RLPopup.shared.showNormal(title: "정말 나가시겠습니까?", description: "등록중인 스펙 문서는 삭제됩니다.", leftButtonTitle: "취소", rightButtonTitle: "나가기", leftAction: {}, rightAction: {
            self.viewModel.removeSpec()
        })
    }
    
    func backWithPopup(step: Int) {
        RLPopup.shared.showNormal(title: "정말 나가시겠습니까?", description: "수정중인 스펙 문서가 있을경우\n저장되지 않습니다.", leftButtonTitle: "취소", rightButtonTitle: "나가기", leftAction: {}, rightAction: {
            self.pushBackTo(depth: step)
        })
    }
}

extension SpecDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
