//
//  SpecDetailViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SnapKit

class SpecDetailViewModel: BaseViewModel {
    enum RegisterType {
        case newRegister
        case newRegisterWithEditing
        case editRegister
        case editRegisterWithEditing
    }
    
    private let sections = BehaviorRelay<[SpecDetailSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let nextButtonEnabled = BehaviorRelay<Bool>.init(value: false)
    let nextSuccess = PublishSubject<(nextStepSeq: Int?, isStep01Edited: Bool)>.init()
    
//    let imageRegistered = BehaviorRelay<Bool>.init(value: false)
    let removeSpecSuccess = PublishSubject<Bool>.init()
    
    private var imageDataList = BehaviorRelay<[SpecPhotoData]>.init(value: [])
    private var specNo: String!

    let rejectInfo = BehaviorRelay<(display: Bool, msg: String?)>.init(value: (display: false, msg: nil))
    
    let registerType = BehaviorRelay<RegisterType>.init(value: .newRegister)
    
    private let specOption = PublishSubject<(title: String, description: String)>.init()
    private let specPhotos = PublishSubject<[(photoNo: String, photoSeq: Int, photoUrl: String, stepSeq: Int, rejectMsg: String, isReject: Bool)]>.init()
    
    // 진입 후 수정 했는지 여부
    private var isStep01Edited: Bool = false
    
    init(specNo: String,
         mustUploadCount: Int = 1,
         stepSeq: Int = 1,
         isStep01Edited: Bool,
         specStatus: SpecModel.SpecStatus,
         containRejectInSpec: Bool,
         nextButtonTap: Observable<Void>) {
        super.init()
        self.isStep01Edited = isStep01Edited
        self.specNo = specNo
        let specType = SpecType.from(specNo: specNo)
        
//        RLProgress.shared.showCustomLoading()
        
        let isTwoStep = Validation.shared.isTwoStep(specNo: specNo)
        imageDataList
            .map{
                let isReject = $0.filter{ $0.isReject }.count > 0
                if isReject {
                    print("-------------buttonStatus : false!!!")
                    return false
                }else{
                    let buttonStatus = self.isStep01Edited && $0.count >= mustUploadCount
                        || specType == .house_12 && stepSeq == 2 /*고가주택 && step 1*/
                        || isTwoStep && stepSeq == 1 && $0.count >= mustUploadCount
                        || isTwoStep && stepSeq == 2 && self.isStep01Edited && $0.count >= mustUploadCount
                    print("-------------buttonStatus : \(buttonStatus)")
                    return buttonStatus
                }
                
                // 2단계 & 1번 리젝포함 //
            }
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)

        nextButtonTap
            .withLatestFrom(imageDataList)
            .flatMapLatest{ image -> Observable<Bool> in
                if image.count > 0 {
                    let imageDatas = image.compactMap{ data -> Data in
                        if let data = data.imageData {
                            return data
                        }
                        if let url = data.photoUrl {
                            return try! Data(contentsOf: URL.init(string: url)!)
                        }
                        return Data()
                    }
                    return Repository.shared.main.addSpecPhoto(userNo: MainInformation.shared.userNoAes!, specNo: specNo, stepSeq: stepSeq, imageDatas: imageDatas)
                }else{
                    /*isHighPriceHouse*/
                    return Observable.just(true)
                }
            }
            .subscribe(onNext: {
                guard $0 else { return }
                if Validation.shared.isOneStep(specNo: specNo) {
                    self.nextSuccess.on(.next((nextStepSeq: nil, isStep01Edited: self.isStep01Edited)))
                } else if isTwoStep {
                    if stepSeq == 1 {
                        self.nextSuccess.on(.next((nextStepSeq: stepSeq + 1, isStep01Edited: self.isStep01Edited)))
                    }else{
                        self.nextSuccess.on(.next((nextStepSeq: nil, isStep01Edited: self.isStep01Edited)))
                    }
                } else {
                    self.nextSuccess.on(.next((nextStepSeq: nil, isStep01Edited: self.isStep01Edited)))
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getSpecPhoto(specNo: specNo, userNo: MainInformation.shared.userNoAes!, stepSeq: stepSeq)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                self.specPhotos.on(.next($0))
                // reject 데이터
                let specPhotos = $0.compactMap{ $0}
                let rejectList = $0.filter{ $0.isReject }
                let rejectMsg = rejectList.compactMap{ $0.rejectMsg }.removeDuplicates().map{ " • \($0)" }.joined(separator: "\n")
                self.rejectInfo.accept((display: rejectList.count > 0, msg: rejectMsg))
                
                // data binding
                self.imageDataList.accept(specPhotos.map{
                    SpecPhotoData.init(image: nil, photoUrl: $0.photoUrl, isReject: $0.isReject, imageData: nil, rejectMsg: nil)
                })
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getSpecOption(specNo: specNo)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                self.specOption.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)

        Observable.combineLatest(specOption, specPhotos)
            .subscribe(onNext: { option, photos in
                let title = option.title
                let description = option.description
                let addImages = photos.map{
                    SpecPhotoData.init(image: nil, photoUrl: $0.photoUrl, isReject: $0.isReject, imageData: nil, rejectMsg: nil)
                }
                let items = self.getItems(title: title, description: description, stepSeq: stepSeq, addImages: addImages, specType: specType)
                let _sections = [SpecDetailSection.init(items: items)]
                self.sections.accept(_sections)
            })
            .disposed(by: bag)

        imageDataList
            .subscribe(onNext: { imageList in
                if specStatus == .normal || specStatus == .ready || specStatus == .reject {
                    // 수정
                    if self.isStep01Edited {
                        self.registerType.accept(.editRegisterWithEditing)
                    }else{
                        self.registerType.accept(.editRegister)
                    }
                }else if specStatus == .none {
                    if imageList.count > 0 {
                        self.registerType.accept(.newRegisterWithEditing)
                    }else{
                        self.registerType.accept(.newRegister)
                    }
                }
            })
            .disposed(by: bag)
        
    }

    func modifySpec(idx: Int, image: UIImage?, imageData: Data) {
        if let specDetailSection = sections.value.first {
            var cellModels = specDetailSection.items
            if let cellModel = cellModels.last {
                var new: SpecDetailCellModel?
                switch cellModel {
                case .addPhoto(let placeholders, let addImages):
                    var newImages = addImages
                    if newImages[idx].isReject {
                        self.rejectInfo.accept((display: false, msg: nil))
                    }
                    newImages[idx] = SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil)
                    new = SpecDetailCellModel.addPhoto(placeholders: placeholders, addImages: newImages)
                default: ()
                }
                if let _new = new {
                    cellModels[cellModels.count-1] = _new
                }
                
            }
            self.isStep01Edited = true
            sections.accept([SpecDetailSection.init(items: cellModels)])
        }
        
        var newList = imageDataList.value
        newList[idx] = SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil)
        imageDataList.accept(newList)
    }
    
    func addSpecPhoto(image: UIImage?, imageData: Data) {
        if let specDetailSection = sections.value.first {
            var cellModels = specDetailSection.items
            if let cellModel = cellModels.last {
                var new: SpecDetailCellModel?
                switch cellModel {
                case .addPhoto(let placeholders, let addImages):
                    var newImages = addImages
                    newImages.append(SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil))
                    new = SpecDetailCellModel.addPhoto(placeholders: placeholders, addImages: newImages)
                default: ()
                }
                if let _new = new {
                    cellModels[cellModels.count-1] = _new
                }
                
            }
            self.isStep01Edited = true
            sections.accept([SpecDetailSection.init(items: cellModels)])
        }
        
        var newList = imageDataList.value
        newList.append(SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil))
        imageDataList.accept(newList)
    }
    
    func removeSpec() {
        Repository.shared.graphQl.removeSpec(userNo: MainInformation.shared.userNoAes!, specNo: specNo)
            .bind(to: removeSpecSuccess)
            .disposed(by: bag)
    }
    
    func removeSpecPhoto(idx: Int) {
        if let specDetailSection = sections.value.first {
            var cellModels = specDetailSection.items
            if let cellModel = cellModels.last {
                var new: SpecDetailCellModel?
                switch cellModel {
                case .addPhoto(let placeholders, let addImages):
                    var newImages = addImages
                    if newImages[idx].isReject {
                        self.rejectInfo.accept((display: false, msg: nil))
                    }
                    newImages.remove(at: idx)
                    new = SpecDetailCellModel.addPhoto(placeholders: placeholders, addImages: newImages)
                default: ()
                }
                if let _new = new {
                    cellModels[cellModels.count-1] = _new
                }
                
            }
            sections.accept([SpecDetailSection.init(items: cellModels)])
            self.isStep01Edited = true
        }
        var newList = imageDataList.value
        newList.remove(at: idx)
        imageDataList.accept(newList)
    }
    
    func getItems(title: String, description: String, stepSeq: Int, addImages: [SpecPhotoData], specType: SpecType) -> [SpecDetailCellModel] {
        var _items: [SpecDetailCellModel] = [.titleDescription(title: title, description: description)]
        switch specType {
        case SpecType.job_Profession_1:
            _items.append(contentsOf: [
            .headerTitle(text: "인증서류 등록 방법안내"),
            .stepTitle(text: "직업 인증", selectType: .none),
            .stepDescription(text: "선택 1", subText: "성명이 표시된 명함"),
            .stepDescription(text: "선택 2", subText: "성명+사진이 나온 전문직 자격증"),
            .stepEnd,
            .exampleImage(named: "spec01_01", imageText: "명함(예)"),
            .exampleImage(named: "spec01_02", imageText: "자격증(예)"),
            .headerTitle(text: "인증 확인용 사진 등록"),
            .addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])
        case SpecType.job_SelfBusiness_2:
            if stepSeq == 1 {
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "01", subText: "연매출 30억 이상임을 증빙할\n수 있는  공식 서류를 올려주세요."),
                    .exampleImage(named: "spec02_01", imageText: "공식 서류(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }else{
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "02", subText: "사업자 등록증 서류를 올려주세요."),
                    .exampleImage(named: "spec02_02", imageText: "사업자 등록증(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }
        case SpecType.salary_7k_3, SpecType.salary_1m_4, SpecType.salary_2m_5, SpecType.salary_3m_6:
            _items.append(contentsOf: [
            .headerTitle(text: "인증서류 등록 방법안내"),
            .stepTitle(text: "소득 인증", selectType: .none),
            .stepDescription(text: "선택 1", subText: "원천징수 영수증"),
            .stepDescription(text: "선택 2", subText: "소득 금액증명원(자영업자)"),
            .stepDescription(text: "선택 3", subText: "연봉 계약서"),
            .stepEnd,
            .exampleImage(named: "spec03_01", imageText: "원천징수 영수증(예)"),
            .exampleImage(named: "spec03_02", imageText: "소득 금액증명원(예)"),
            .exampleImage(named: "spec03_03", imageText: "연봉 계약서(예)"),
            .headerTitle(text: "인증 확인용 사진 등록"),
            .addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])
        case SpecType.car_high_7, SpecType.car_super_8:
            _items.append(contentsOf: [
            .headerTitle(text: "인증서류 등록 방법안내"),
            .stepTitle(text: "본인 차량 인증", selectType: .select1),
            .stepDescription(text: "선택 1", subText: "본인 성명이 기재된 자동차등록증"),
            .stepDescription(text: "선택 2", subText: "본인 성명이 기재된 자동차 보험계약서"),
            .stepEnd,
            .stepTitle(text: "법인 차량 인증", selectType: .select2),
            .stepDescription(text: "선택 1", subText: "법인 소유 자동차 등록증 또는 자동차\n보험계약서"),
            .stepDescription(text: "선택 2", subText: "명함 (해당 법인 소속)"),
            .stepEnd,
            .stepTitle(text: "리스/장기렌트 차량 인증", selectType: .select3),
            .stepDescription(text: "선택 1", subText: "장기렌트 또는 리스 계약서"),
            .stepDescription(text: "선택 2", subText: "명함 (해당 법인 소속)"),
            .stepEnd,
            .exampleImage(named: "spec07_01", imageText: "자동차 등록증(예)"),
            .exampleImage(named: "spec07_02", imageText: "자동차 보험계약서(예)"),
            .headerTitle(text: "인증 확인용 사진 등록"),
            .addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])
        case SpecType.edu_9:
            _items.append(contentsOf: [
            .headerTitle(text: "인증서류 등록 방법안내"),
            .stepTitle(text: "인증", selectType: .none),
            .stepDescription(text: "선택 1", subText: "재학증명서 또는 졸업증명서"),
            .stepDescription(text: "선택 2", subText: "학생증"),
            .stepDescription(text: "선택 3", subText: "포털 접속 화면을 캡쳐한 사진"),
            .stepSubDescription(text: "선택 3", subText: "본인 성명과 소속 표시된 화면"),
            .stepEnd,
            .exampleImage(named: "spec09_01", imageText: "재학 또는 졸업증명서(예)"),
            .exampleImage(named: "spec09_02", imageText: "학생증(예)"),
            .headerTitle(text: "인증 확인용 사진 등록"),
            .addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])
        case SpecType.money_high_10, SpecType.money_super_11:
            _items.append(contentsOf: [
            .headerTitle(text: "인증서류 등록 방법안내"),
            .stepTitle(text: "부동산 인증", selectType: .select1),
            .stepDescription(text: "필수 1", subText: "소유권을 증명할 수 있는 부동산 등기서류 혹은 매매계약서"),
            .stepDescription(text: "필수 2", subText: "네이버 부동산 실거래가 캡처화면"),
            .stepSubDescription(text: "필수 2", subText: "네이버 부동산으로 가액 파악이 어려운 토지나 건물의 경우 공식 감정평가 자료가 필요합니다."),
            .stepEnd,
            .stepTitle(text: "현금 인증", selectType: .select2),
            .stepDescription(text: "선택 1", subText: "시중 은행에서 발행한 잔고/잔액 증명서"),
            .stepSubDescription(text: "선택 1", subText: "본인 성명이 표시된 최근 30일 내 발급 서류"),
            .stepDescription(text: "선택 2", subText: "모바일뱅킹 앱 캡처화면"),
            .stepSubDescription(text: "선택 2", subText: "PC 캡처 화면은 불가능"),
            .stepEnd,
            .stepTitle(text: "기타 인증", selectType: .select3),
            .stepDescription(text: "필수 1", subText: "본인 명의의 자산임을 증빙할 수 있는 명확한 서류"),
            .stepEnd,
            .exampleImage(named: "spec10_01", imageText: "부동산 등기서류(예)"),
            .exampleImage(named: "spec10_02", imageText: "네이버 부동산(예)"),
            .exampleImage(named: "spec10_03", imageText: "은행 잔액 증명서(예)"),
            .headerTitle(text: "인증 확인용 사진 등록"),
            .addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])
        case SpecType.house_12:
            if stepSeq == 1 {
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "01", subText: "본인이 거주 중임을 증빙할 수 있는 서류를 올려주세요.",smallText: "(예: 신분증, 등본)"),
                    .exampleImage(named: "spec12_02", imageText: "신분증(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must, .optional], addImages: addImages)])
            }else{
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "02", subText: "네이버 부동산 실거래가 캡처 화면을 올려주세요.", smallText: "네이버 부동산에 나오지 않는 고가 개인 주택의 경우 생략 가능"),
                    .exampleImage(named: "spec10_02", imageText: "네이버 부동산(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.optional], addImages: addImages)])
            }
        case SpecType.family_elite_13:
            if stepSeq == 1 {
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "01", subText: "가족관계증명서 등 가족임을 증빙할 수 있는 서류를 올려주세요."),
                    .exampleImage(named: "spec13_01", imageText: "가족관계증명서(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }else{
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "02", subText: "부모님 직업을 증빙할 수 있는 서류를 올려주세요."),
                    .exampleImage(named: "spec13_02", imageText: "부모님 직업 서류(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }
        case SpecType.family_super_rich_14:
            if stepSeq == 1 {
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "01", subText: "가족관계증명서 등 가족임을 증빙할 수 있는 서류를 올려주세요."),
                    .exampleImage(named: "spec13_01", imageText: "가족관계증명서(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }else{
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "02", subText: "가족 자산 100억원 이상을 증명할 수 있는\n서류를 올려주세요."),
                    .stepTitle(text: "부동산 인증", selectType: .select1),
                    .stepDescription(text: "필수 1", subText: "소유권을 증명할 수 있는 부동산 등기서류 혹은 매매계약서"),
                    .stepDescription(text: "필수 2", subText: "네이버 부동산 실거래가 캡처화면"),
                    .stepSubDescription(text: "필수 2", subText: "네이버 부동산으로 가액 파악이 어려운 토지나 건물의 경우 공식 감정평가 자료가 필요합니다."),
                    .stepEnd,
                    .stepTitle(text: "현금 인증", selectType: .select2),
                    .stepDescription(text: "선택 1", subText: "시중 은행에서 발행한 잔고/잔액 증명서"),
                    .stepSubDescription(text: "선택 1", subText: "본인 성명이 표시된 최근 30일 내 발급 서류"),
                    .stepDescription(text: "선택 2", subText: "모바일뱅킹 앱 캡처화면"),
                    .stepSubDescription(text: "선택 2", subText: "PC 캡처 화면은 불가능"),
                    .stepEnd,
                    .stepTitle(text: "기타 인증", selectType: .select3),
                    .stepDescription(text: "필수 1", subText: "본인 명의의 자산임을 증빙할 수 있는 명확한 서류"),
                    .stepEnd,
                    .exampleImage(named: "spec10_01", imageText: "부동산 등기서류(예)"),
                    .exampleImage(named: "spec10_02", imageText: "네이버 부동산(예)"),
                    .exampleImage(named: "spec10_03", imageText: "은행 잔액 증명서(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])
            }
        case SpecType.face_good_15:
            if stepSeq == 1 {
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "01", subText: "중견 기업 또는 대기업 이상 기업에 재직 중임을 증빙할 수 있는 명함, 재직증명서 등의 서류를 올려주세요."),
                    .exampleImage(named: "spec15_01", imageText: "명함(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }else{
                _items.append(contentsOf: [
                    .headerTitle(text: "인증서류 등록 방법안내"),
                    .step2Description(named: "02", subText: "추후 올려주실 프로필 사진이 본인 사진\n임을 증빙할 수 있는 서류를 올려주세요.", smallText: "(예:신분증, 운전면허증, 여권 등)"),
                    .exampleImage(named: "spec12_02", imageText: "신분증(예)"),
                    .headerTitle(text: "인증 확인용 사진 등록"),
                    .addPhoto(placeholders: [.must], addImages: addImages)])
            }
        }
        return _items
    }
}

enum SpecDetailCellModel {
    case titleDescription(title: String, description: String)
    case headerTitle(text: String)
    
    case step2Description(named: String, subText: String, smallText: String = "")
    
    case stepTitle(text: String, selectType: StepTitleCell.SelectType = .none)
    case stepDescription(text: String, subText: String)
    case stepSubDescription(text: String, subText: String)
    case stepEnd
    
    case exampleImage(named: String, imageText: String)
    case addPhoto(placeholders: [AddPhotoCell.PlaceholderType], addImages: [SpecPhotoData])
}

struct SpecPhotoData {
    var image: UIImage?
    var photoUrl: String?
    var isReject: Bool = false
//    var photoStatus: String?
    var imageData: Data?
    
//    photoSeq
//    photoUrl
//    photoStatus
//    rejectYn
    var rejectMsg: String?
}

// Tableview Cell
struct SpecDetailSection {
    var items: [SpecDetailCellModel]
}

extension SpecDetailSection: SectionModelType {
    init(original: SpecDetailSection, items: [SpecDetailCellModel]) {
        self = original
        self.items = items
    }
}

