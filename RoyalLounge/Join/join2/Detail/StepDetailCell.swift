//
//  StepDetailCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/21.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class TitleDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionWrapView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionWrapView.layer.cornerRadius = 6.0
        
    }
}

class HeaderTitleCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
}

class StepTitleCell: UITableViewCell {
    enum SelectType {
        case select1
        case select2
        case select3
        case none
    }
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var boxWrapView: UIView!
    @IBOutlet weak var titleWrapView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        boxWrapView.layer.cornerRadius = 6.0
        titleWrapView.layer.cornerRadius = 4.0
    }
    
    func setType(selectType: SelectType) {
        
        selectedImageView.isHidden = false
        switch selectType {
        case .select1:
            selectedImageView.image = UIImage.init(named: "select01")
        case .select2:
            selectedImageView.image = UIImage.init(named: "select02")
        case .select3:
            selectedImageView.image = UIImage.init(named: "select03")
        case .none:
            selectedImageView.isHidden = true
        }
        
    }
}

class Step2DescriptionCell: UITableViewCell {
    
    @IBOutlet weak var stepImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
class StepDescriptionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
class StepSubDescriptionCell: UITableViewCell {
    @IBOutlet weak var hiddenTitleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class StepEndCell: UITableViewCell {
    @IBOutlet weak var boxWrapView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        boxWrapView.layer.cornerRadius = 6.0
    }
}

class ExampleImageCell: UITableViewCell {
    
    @IBOutlet weak var imageWrapView: UIView!
    @IBOutlet weak var titleWarpView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var exampleImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageWrapView.layer.cornerRadius = 6.0
        titleWarpView.layer.cornerRadius = 4.0
    }
}

class AddPhotoCell: UITableViewCell {

    enum PlaceholderType {
        case must
        case optional
    }
    @IBOutlet weak var image1WrapView: UIView!
    @IBOutlet weak var image2WrapView: UIView!
    @IBOutlet weak var image3WrapView: UIView!
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    
    @IBOutlet weak var removeButton1: UIButton!
    @IBOutlet weak var removeButton2: UIButton!
    @IBOutlet weak var removeButton3: UIButton!
    
    @IBOutlet weak var indicator1: UIActivityIndicatorView!
    @IBOutlet weak var indicator2: UIActivityIndicatorView!
    @IBOutlet weak var indicator3: UIActivityIndicatorView!
    
    let bag = DisposeBag()
    
    var modifyPhoto: ((Int,UIImage, Data)->Void)?
    var addPhoto: ((UIImage, Data)->Void)?
    
    var remove: ((Int)->Void)?
    
    var isRegistered = false
    
    private var regImages: [SpecPhotoData] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialize()
        
        indicator1.isHidden = true
        indicator1.hidesWhenStopped = true
        indicator2.isHidden = true
        indicator2.hidesWhenStopped = true
        indicator3.isHidden = true
        indicator3.hidesWhenStopped = true
        
        image1WrapView.layer.cornerRadius = 6.0
        image2WrapView.layer.cornerRadius = 6.0
        image3WrapView.layer.cornerRadius = 6.0
        
        image1WrapView.rx
            .tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { gesture in
                self.loadPhotoPicker(idx: 0)
            }).disposed(by: bag)
        image2WrapView.rx
            .tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
                self.loadPhotoPicker(idx: 1)
            }).disposed(by: bag)
        image3WrapView.rx
            .tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
                self.loadPhotoPicker(idx: 2)
            }).disposed(by: bag)
    }
    
    @IBAction func removeButton1Touched(_ sender: Any) {
        if isRegistered { return }
        remove?(0)
    }
    @IBAction func removeButton2Touched(_ sender: Any) {
        if isRegistered { return }
        remove?(1)
    }
    @IBAction func removeButton3Touched(_ sender: Any) {
        if isRegistered { return }
        remove?(2)
    }
    
    func loadPhotoPicker(idx: Int) {
        if isRegistered { return }
        RLImagePicker.shared.load(UIViewController.keyController(), imageType: .rectangle) { result in
            let (image, data) = result.first!
            if self.regImages.count > idx {
                let specPhoto = self.regImages[idx]
                if specPhoto.photoUrl != nil || specPhoto.imageData != nil {
                    self.modifyPhoto?(idx, image, data)
                }else{
                    self.addPhoto?(image, data)
                }
            }else{
                self.addPhoto?(image, data)
                [self.imageView1, self.imageView2, self.imageView3][idx].image = image
            }
        }
    }
    
    func initialize() {
        (0...2).forEach { idx in
            [imageView1, imageView2, imageView3][idx]?.image = UIImage.init(named: "compPhotoUploadDafualt1")
            [removeButton1, removeButton2, removeButton3][idx]?.isHidden = true
        }
    }
    
    func setImage(placeholders: [PlaceholderType], addImages: [SpecPhotoData], isRegistered: Bool = false) {
        self.regImages = addImages
        
        self.isRegistered = isRegistered // 학교직장 인증에서만 사용함
        
        let imageWrapViews = [image1WrapView, image2WrapView, image3WrapView]
        let imageViews = [imageView1, imageView2, imageView3]
        let removeButtons = [removeButton1, removeButton2, removeButton3]
        let indicators = [indicator1, indicator2, indicator3]
        image1WrapView.isHidden = true
        image2WrapView.isHidden = true
        image3WrapView.isHidden = true
        
        let typeMustImage = UIImage.init(named: "compPhotoUploadDafualt1")
        let typeOptionalImage = UIImage.init(named: "compPhotoUploadDafualt2")
        placeholders.enumerated().forEach { idx, type in
            imageWrapViews[idx]?.isHidden = false
            imageViews[idx]?.image = type == .must ? typeMustImage : typeOptionalImage
            removeButtons[idx]?.isHidden = true
        }
        
        imageWrapViews.forEach {
            $0?.contentMode = .scaleToFill
            $0?.layer.borderColor = UIColor.clear.cgColor
            $0?.layer.borderWidth = 0.0
            $0?.setBorder(color: .clear)
        }
        indicators.forEach { $0?.stopAnimating() }
        
        addImages.enumerated().forEach { (idx, addImage) in
            let imageWrapView = imageWrapViews[idx]
            let imageView = imageViews[idx]
            imageView?.image = addImage.image
            mainAsync {
                if let url = addImage.photoUrl {
                    let indicator = indicators[idx]
                    indicator?.startAnimating()
                    imageView?.setImage(urlString: url, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"), completed: { image in
                        indicator?.stopAnimating()
                        removeButtons[idx]?.isHidden = false
                        if isRegistered { removeButtons[idx]?.isHidden = true }
                        
                        if addImage.isReject {
                            imageWrapView?.layer.borderColor = UIColor.errerColor.cgColor
                            imageWrapView?.layer.borderWidth = 3.0
                        }else{
                            imageWrapView?.layer.borderColor = UIColor.gray10.cgColor
                            imageWrapView?.layer.borderWidth = 1.5
                        }
                    })
                }else{
                    removeButtons[idx]?.isHidden = false
                    if isRegistered { removeButtons[idx]?.isHidden = true }
                    if addImage.isReject {
                        imageWrapView?.layer.borderColor = UIColor.errerColor.cgColor
                        imageWrapView?.layer.borderWidth = 3.0
                    }else{
                        imageWrapView?.layer.borderColor = UIColor.gray10.cgColor
                        imageWrapView?.layer.borderWidth = 1.5
                    }
                }
            }
        }
    }
}
