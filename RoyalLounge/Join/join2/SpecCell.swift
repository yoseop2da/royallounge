//
//  SpecCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SpecCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var registeredLabel: UILabel!
    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var contentWrapView: UIView!
    
    private var spec: SpecModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentWrapView.clipsToBounds = true
        contentWrapView.layer.masksToBounds = true
        
        contentWrapView.layer.cornerRadius = 6.0
        contentWrapView.layer.borderWidth = 1.0
        contentWrapView.layer.borderColor = UIColor.gray10.cgColor
        
        registeredLabel.layer.cornerRadius = 4.0
        registeredLabel.layer.masksToBounds = true
        
        if UIScreen.underSE { titleLabel.font = UIFont.spoqaHanSansBold(ofSize: 16) }
    }
    
    func setModel(item: SpecModel) {
        spec = item
        titleLabel.text = item.title
        subTitleLabel.text = item.subTitle
        
        registeredLabel.isHidden = false
        if item.specStatus == .normal {
            registeredLabel.text = item.specStatusName//"등록 완료"
            registeredLabel.backgroundColor = .brown25
            registeredLabel.textColor = .brown75
        } else if item.specStatus == .reject {
            registeredLabel.text = item.specStatusName//"인증 반려"
            registeredLabel.backgroundColor = .errerColor
            registeredLabel.textColor = .white
        }else if item.specStatus == .ready {
            registeredLabel.text = item.specStatusName//"심사중.."
            registeredLabel.backgroundColor = .warningColor
            registeredLabel.textColor = .white
        }else{
            registeredLabel.isHidden = true
            registeredLabel.text = ""
        }
        
        mainAsync { self.badgeImageView.setImage(urlString: item.badgeImage) }
        
        contentWrapView.backgroundColor = item.specType == .face_good_15 ? .gray10 : .white
        
        if MainInformation.shared.isJoinOrReject {
            // 가입
            self.displayImageView.alpha = 0.0
            self.displayOn()
            self.setBorder(isOn: false)
        }else{
            if item.specStatus == .normal {
                if item.displayYn {
                    self.displayImageView.alpha = 0.0
                    self.displayOn()
                    self.setBorder(isOn: true)
                }else{
                    self.displayImageView.alpha = 1.0
                    self.displayOff()
                    self.setBorder(isOn: false)
                }
            }else{
                self.displayImageView.alpha = 0.0
                self.displayOn()
                self.setBorder(isOn: false)
            }
        }
    }
    
    func displayOnOffAnimation(callback: (()->Void)?) {
        if spec.displayYn {
            // Off 하기
            self.displayImageView.alpha = 1.0
            self.displayOff()
            self.setBorder(isOn: false)
            callback?()
        }else{
            // On 하기
            self.displayImageView.alpha = 0.0
            self.displayOn()
            self.setBorder(isOn: true)
            UIView.animate(withDuration: 0.8, animations: {
                self.displayImageView.alpha = 1.0
            }) { _ in
                UIView.animate(withDuration: 0.5, delay: 0.5, options: [], animations: {
                    self.displayImageView.alpha = 0.0
                }, completion: nil)
            }
            delay(0.3) {
                callback?()
            }
        }
    }
    
    func displayOn() {
        self.contentWrapView.alpha = 1.0
        self.displayImageView.image = UIImage.init(named: "icons40PxEye")
    }
    
    func displayOff() {
        self.contentWrapView.alpha = 0.4
        self.displayImageView.image = UIImage.init(named: "icons40PxEyeHide")
    }
    
    func setBorder(isOn: Bool) {
        if isOn {
            self.contentWrapView.setBorder(width: 2.0, color: .brown75)
        }else{
            self.contentWrapView.setBorder(width: 1.0, color: spec.specType == .face_good_15 ? .gray25 : .gray10)
        }
    }
}
