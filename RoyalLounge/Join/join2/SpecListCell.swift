//
//  SpecListCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class SpecListCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<SpecGroupModel>
    var modelSelected: ((SpecModel)->Void)?
    var displayOnOffAnimation: (()->Void)?
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, collectionView, indexPath, item -> SpecCell in
            
            let cell: SpecCell = collectionView.dequeueReusable(for: indexPath)
            cell.setModel(item: item)
            return cell
        })
        return dataSource
    }
    
    let bag = DisposeBag()
    var viewModel = SpecListCellViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let flowLayout = UICollectionViewFlowLayout()
        let count = 2
        let padding = CGFloat(15)
        let width = (UIScreen.width - 40.0 - CGFloat(15 * (count-1))) / CGFloat(count)
        
        flowLayout.scrollDirection = .horizontal
        // 헤더만 delegate 적용
//        collectionView.delegate = self
        flowLayout.headerReferenceSize = CGSize.zero
        flowLayout.itemSize = CGSize(width: width, height: width * CGFloat(170.0 / 160.0))
//        flowLayout.itemSize = CGSize(width: width, height: width)
        flowLayout.minimumLineSpacing = padding
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
        collectionView.collectionViewLayout = flowLayout
        collectionView.setContentOffset(.zero, animated: false)
        
        collectionView.rx.modelSelected(SpecModel.self)
            .throttle(.milliseconds(500), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { spec in
//                let spec = self.viewModel.item(indexPath: indexPath)
                self.modelSelected?(spec)
            })
            .disposed(by: bag)
        
        self.viewModel.sectionData
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
    }
    
    func displayOnOffAnimation(spec: SpecModel, callback: (()->Void)?) {
        if let indexPath = self.viewModel.indexPath(spec: spec), let cell = collectionView.cellForItem(at: indexPath) as? SpecCell {
            cell.displayOnOffAnimation(callback: callback)
        }
    }
    
//    func refreshSpecGroup(section: Int) {
//        collectionView.reloadSections(IndexSet(integer: section))
//    }
    
    func refreshCell(spec: SpecModel) {
        if let indexPath = self.viewModel.indexPath(spec: spec), let cell = collectionView.cellForItem(at: indexPath) as? SpecCell {
            cell.setModel(item: spec)
        }
    }
    
    func setItem(item: SpecGroupModel, animated: Bool, animatedCallback: (()->Void)?) {
        viewModel.refresh(item: item)
        if item.items.count > 2 {
//            let selectItem = item.items.filter{$0.specStatus == .reject}
            let padding = CGFloat(15)
            let count = 2
            let width = (UIScreen.width - 40.0 - padding * CGFloat(count-1)) / CGFloat(count)
            
//            if selectItem.count == 1 {
//                // 선택으로 이동
//                let _item = selectItem.first!
//                if let idx = item.items.firstIndex(where: { $0.specNo == _item.specNo }) {
//                    delay(0.1) {
//                        //                        self.collectionView.scrollToItem(at: IndexPath.init(row: idx, section: 0), at: .left, animated: true)
//                        self.collectionView.setContentOffset(CGPoint.init(x: CGFloat(20.0 + width) * CGFloat(idx), y: 0), animated: true)
//                    }
//                }
//            }else{
                if animated {
                    delay(0.0) {
                        self.collectionView.setContentOffset(CGPoint.init(x: 20 + width/2.0, y: 0), animated: false)
                        delay(0.5) {
                            self.collectionView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
                            animatedCallback?()
                        }
                    }
                }else{
                    //                    self.collectionView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
                }
//            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class SpecListCellViewModel: BaseViewModel {
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[SpecGroupModel]>.init(value: [])
    
    override init() {
        super.init()
    }
    
    func refresh(item: SpecGroupModel) {
        sections.accept([item])
    }
    
    func indexPath(spec: SpecModel) -> IndexPath? {
        for (section, value) in sections.value.enumerated() {
            for (row, _spec) in value.items.enumerated() {
                if _spec.specNo == spec.specNo {
                    return IndexPath.init(row: row, section: section)
                }
            }
        }
        return nil
    }
    
}
