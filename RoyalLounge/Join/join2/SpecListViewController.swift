//
//  SpecListViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SnapKit

class SpecListViewController: ClearNaviLoggedBaseViewController {
    enum BackType {
        case back
        case empty
    }
    typealias DataSource = RxTableViewSectionedReloadDataSource<SpecSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerStepView: UIView!
    
    @IBOutlet weak var nextButtonWrapView: UIView!

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextButtonHeight: NSLayoutConstraint!
    
    // 스펙 에니메이션 최초 1회만 발생시키기 위한 flag
    private var isFirstAnimated: Bool = true
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> SpecListCell in
            let cell: SpecListCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item: item, animated: self.isFirstAnimated) {
                self.isFirstAnimated = false
            }
            cell.modelSelected = { spec in
                if !MainInformation.shared.isJoinOrReject {
                    // 복수일경우 다른 스펙이 등록되어있느지 체크
                    if spec.specStatus == .ready {
                        toast(message: "심사중인 스펙으로 열람할 수 없습니다", seconds: 1.0)
                         return
                    }
                    if spec.specStatus == .normal {
                        cell.displayOnOffAnimation(spec: spec, callback: {
                            self.viewModel.specOnOff(spec, failCallback:{
                                // 이전상태로 돌려주기.
                                cell.refreshCell(spec: spec)
                            })
                        })
                        return
                    }
                }
                // 단일 & 다른항목이 등록되어있음 & 신규추가
                if !item.isMultiSelection && /*단일선택*/
                    (item.items.filter{$0.specStatus == .normal||$0.specStatus == .ready}.count > 0) && /*다른 항목이 등록중인게 되어있다면*/
                    spec.specStatus == .none /*지금 스펙이 등록이 안되어있고*/ {
                        RLPopup.shared.showNormal(title: "이미 \(item.title) 뱃지를 등록하였습니다", description: "운영자 승인 시 마지막 승인된 뱃지로 변경 저장됩니다.",leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                            let vc = RLStoryboard.join.specDetailViewController(groupTitle: item.title, specNo: spec.specNo, stepSeq: 1, isStep01Edited: false, specStatus: spec.specStatus, containRejectInSpec: spec.isRejected)
                            vc.refreshCallback = {
                                // 등록상태 업데이트
                                self.viewModel.refreshData()
                            }
                            self.push(vc)
                        })
                         return
                }
                
                let vc = RLStoryboard.join.specDetailViewController(groupTitle: item.title, specNo: spec.specNo, stepSeq: 1, isStep01Edited: false, specStatus: spec.specStatus, containRejectInSpec: spec.isRejected)
                vc.refreshCallback = {
                    // 등록상태 업데이트
                    self.viewModel.refreshData()
                }
                self.push(vc)
                
            }
            return cell
        })
        
        return dataSource
    }
    
    var viewModel: SpecListViewModel!
    var isEditMode: Bool = false
    var backType: BackType = .empty
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !RLUserDefault.shared.isShownSpecRegisteredPopup && MainInformation.shared.isJoinOrReject {
            RLUserDefault.shared.isShownSpecRegisteredPopup = true
            RLPopup.shared.showSpecAgree {}
        }
        
        if isEditMode {
            swipeBackAnyWhere()
            if MainInformation.shared.isJoinOrReject {
                self.nextButtonHeight.constant = 82
                nextButton.setTitle("완료", for: .normal)
            }else{
                self.nextButtonHeight.constant = 0
            }
            headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 0.5)
            mainTableView.reloadData()
            headerStepView.isHidden = true
        }
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.isEditMode {
                    self.viewModel.completeRejectSpec()
                }else{
                    self.viewModel.completeSpec()
                }
            })
            .disposed(by: bag)

        if isEditMode || backType == .back {
            let barButton = RLBarButtonItem(barType: .back)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.pushBack(animated: true)
                })
                .disposed(by: bag)
            self.navigationItem.leftBarButtonItem = barButton
        }
        
        viewModel = SpecListViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else if $0.resultCD == RLResultCD.E01024_CanNotChange.code {
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                    return
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                if self.isEditMode {
                    self.pushBack()
                }else{
                    Coordinator.shared.toJoinStep3()
                }
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "스펙 인증"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // 마이페이지에서 진입 이후에 업데이트 필요
        if !MainInformation.shared.isJoinOrReject {
            MainInformation.shared.updateRedDot()
        }
    }
}

extension SpecListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let count = 2
        let width = (UIScreen.width - 40.0 - CGFloat(15 * (count-1))) / CGFloat(count)
        return width * CGFloat(170.0 / 160.0)
//        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let wrapView = UIView.init()
        let titleLabel = UILabel.init()
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.spoqaHanSansLight(ofSize: 22)
        titleLabel.text = viewModel.headerTitle(sectionIdx: section)
        titleLabel.textColor = .dark
        wrapView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview().offset(12)
        }
        let rightLabel = UILabel.init()
        rightLabel.textAlignment = .right
        rightLabel.font = UIFont.spoqaHanSansLight(ofSize: 16)
        rightLabel.text = viewModel.headerRight(sectionIdx: section)
        rightLabel.textColor = .gray50
        wrapView.addSubview(rightLabel)
        rightLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-20)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview().offset(12)
        }
        return wrapView
    }
}

