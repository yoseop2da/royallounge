//
//  SpecListViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

class SpecListViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private let sections = BehaviorRelay<[SpecSection]>.init(value: [])
    private let result = BehaviorRelay<[GetSpecGroupQuery.Data.MemberSpecGroupList]>.init(value: [])
    
    let nextButtonEnabled = BehaviorRelay<Bool>.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    private var userNo: String!
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        refreshData()
        
        result
            .map{ list -> [SpecSection] in
                list.compactMap{ _group -> SpecSection? in
                    guard let title = _group.title,
                        let specGroupNo = _group.specGroupNo,
                    let specList = _group.specList else { return nil}
                    // 여자는 외모 카테고리를 삭제해줘야됨
                    if !(MainInformation.shared.gender == .m) && specGroupNo == "XqLHDobCRYldA8qmp_ptmA=="/*외모*/ { return nil }
                    let newSpecList = specList.compactMap{$0}.compactMap { SpecModel.init(spec: $0) }
                    return SpecSection.init(items: [SpecGroupModel.init(title: title, specGroupNo: specGroupNo, description: _group.description ?? "", isMultiSelection: _group.multiSelectYn ,items: newSpecList)])
                }
            }
            .subscribe(onNext: { sectionList in
                self.sections.accept(sectionList)
                
                // 등록완료가 있는지 여부 파악하기
                let isEnable = sectionList.filter{ $0.items.filter{ $0.items.filter{ $0.specStatus == .normal || $0.specStatus == .ready }.count > 0 }.count > 0 }.count > 0
                self.nextButtonEnabled.accept(isEnable)
            })
            .disposed(by: bag)
    }
    
    func completeRejectSpec() {
        Repository.shared.graphQl
            .completeSpecReject(userNo: userNo)
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
    
    func completeSpec() {
        Repository.shared.graphQl
            .completeSpec(userNo: userNo)
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
    
    func refreshData() {
        Repository.shared.graphQl
            .getSpecGroup(userNo: userNo)
            .catchError{ error in
                alertERR_E0000Dialog {
                    Coordinator.shared.toSplash()
                }
                return Observable.just([]) }
            .subscribe(onNext: {
                self.result.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func specOnOff(_ spec: SpecModel, failCallback: (()->Void)?) {
        let displayYn = !spec.displayYn
        guard let memberSpecNo = spec.memberSpecNo else { return }
        Repository.shared.main
            .specDisplay(memberSpecNo: memberSpecNo, displayYn: displayYn)
            .subscribe(onNext: {
                guard $0 else { return }
                self.refreshData()
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                failCallback?()
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func headerTitle(sectionIdx idx: Int) -> String {
        if let kind = sections.value[idx].items.first {
            return kind.title
        }
        return ""
    }
    
    func headerRight(sectionIdx idx: Int) -> String {
        if let kind = sections.value[idx].items.first {
            return kind.description
        }
        return ""
    }
}
