//
//  SpecSection.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxDataSources

enum SpecType: String {
    case job_Profession_1 = "1" /*전문직*/
    case job_SelfBusiness_2 = "2" /*사업가*/
    case salary_7k_3 = "3" /*7000+*/
    case salary_1m_4 = "4" /*1억+*/
    case salary_2m_5 = "5" /*2억+*/
    case salary_3m_6 = "6" /*3억+*/
    case car_high_7 = "7" /*고가차*/
    case car_super_8 = "8" /*슈퍼카*/
    case edu_9 = "9" /*명문대*/
    case money_high_10 = "10" /*고액*/
    case money_super_11 = "11" /*초고액*/
    case house_12 = "12" /*고가주택*/
    case family_elite_13 = "13" /*엘리트 패밀리*/
    case family_super_rich_14 = "14" /*슈퍼리치 패밀리*/
    case face_good_15 = "15" /*외모+직장*/
    
    static func isOneStep(specNo: String) -> Bool {
        let type = SpecType.from(specNo: specNo)
        return [.job_Profession_1, .salary_7k_3, .salary_1m_4, .salary_2m_5, .salary_3m_6, .car_high_7, .car_super_8, .edu_9, .money_high_10, .money_super_11].contains(type)
    }
    
    static func isTwoStep(specNo: String) -> Bool {
        let type = SpecType.from(specNo: specNo)
        return [.job_SelfBusiness_2, .house_12, .family_elite_13, .family_super_rich_14, .face_good_15].contains(type)
    }
    
    static func from(specNo: String) -> SpecType {
        switch specNo.aesDecrypted! {
        case SpecType.job_Profession_1.rawValue : return .job_Profession_1
        case SpecType.job_SelfBusiness_2.rawValue : return .job_SelfBusiness_2
        case SpecType.salary_7k_3.rawValue : return .salary_7k_3
        case SpecType.salary_1m_4.rawValue : return .salary_1m_4
        case SpecType.salary_2m_5.rawValue : return .salary_2m_5
        case SpecType.salary_3m_6.rawValue : return .salary_3m_6
        case SpecType.car_high_7.rawValue : return .car_high_7
        case SpecType.car_super_8.rawValue : return .car_super_8
        case SpecType.edu_9.rawValue : return .edu_9
        case SpecType.money_high_10.rawValue : return .money_high_10
        case SpecType.money_super_11.rawValue : return .money_super_11
        case SpecType.house_12.rawValue : return .house_12
        case SpecType.family_elite_13.rawValue : return .family_elite_13
        case SpecType.family_super_rich_14.rawValue : return .family_super_rich_14
        case SpecType.face_good_15.rawValue : return .face_good_15
        default:
            return .job_Profession_1
        }
    }
}

struct SpecModel {
//    specNo title subTitle description badgeUrl regYn specStatus specStatusName
    var specType: SpecType { SpecType.from(specNo: specNo) }
    var specNo: String
    var memberSpecNo: String?
    var title: String
    var subTitle: String
    var description: String
    var badgeImage: String
    var displayYn: Bool
    var specStatus: SpecStatus
    var specStatusName: String
    
    var isRegistered: Bool { specStatus == .normal }
    var isRejected: Bool { specStatus == .reject }
    
    enum SpecStatus {
        case none
        case normal // 등록 완료
        case ready // 심사중
        case reject // 반려
        
        static func convert(by value: String?) -> SpecStatus {
            guard let _value = value else { return .none }
            switch _value {
            case "NORMAL": return .normal // 등록 완료
            case "READY": return .ready // 심사중
            case "REJECT": return .reject // 반려
            default: return .none
            }
        }
    }
    
    init?(spec: GetSpecGroupQuery.Data.MemberSpecGroupList.SpecList) {
        guard let specNo = spec.specNo else { return nil}
        guard let title = spec.title else { return nil}
        guard let subTitle = spec.subTitle else { return nil}
        guard let description = spec.description else { return nil}
        guard let badgeUrl = spec.badgeUrl else { return nil}
        
        self.specNo = specNo
        self.memberSpecNo = spec.memberSpecNo
        self.title = title
        self.subTitle = subTitle
        self.description = description
        self.badgeImage = badgeUrl
        self.displayYn = spec.displayYn
        self.specStatus = SpecStatus.convert(by: spec.specStatus)
        self.specStatusName = spec.specStatusName ?? ""
    }
}

// Tableview Cell
struct SpecSection {
    var items: [SpecGroupModel]
}

extension SpecSection: SectionModelType {
    init(original: SpecSection, items: [SpecGroupModel]) {
        self = original
        self.items = items
    }
}

// CollectionView Cell
struct SpecGroupModel {
    var title: String
    var specGroupNo: String
    var description: String
    var isMultiSelection: Bool
    var items: [SpecModel]
}

extension SpecGroupModel: SectionModelType {
    typealias Item = SpecModel
    init(original: SpecGroupModel, items: [Item]) {
        self = original
        self.items = items
    }
}

