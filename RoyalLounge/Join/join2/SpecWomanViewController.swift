//
//  SpecWomanViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class SpecWomanViewController: LoggedBaseViewController {

    @IBOutlet weak var appearenceView: UIView!
    @IBOutlet weak var appearenceSubInfoLabel: UILabel!
    @IBOutlet weak var specView: UIView!
    @IBOutlet weak var specSubInfoLabel: UILabel!
    
    var isEditMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appearenceView.layer.cornerRadius = 6.0
        specView.layer.cornerRadius = 6.0
        
        appearenceView.layer.borderWidth = 1.0
        specView.layer.borderWidth = 1.0
        
        appearenceView.layer.borderColor = UIColor.gray10.cgColor
        specView.layer.borderColor = UIColor.gray10.cgColor
        
        let appearenceAttr = NSMutableAttributedString(string: "매력 점수 3점 이상", attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 14.0), .foregroundColor: UIColor.gray50,
          .kern: 0.0
        ])
        
        appearenceAttr.addAttribute(.foregroundColor, value: UIColor.primary100, range: NSRange(location: 6, length: 2))
        appearenceSubInfoLabel.attributedText = appearenceAttr
        
        let specAttr = NSMutableAttributedString(string: "스펙 인증 1개 이상", attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 14.0), .foregroundColor: UIColor.gray50,
          .kern: 0.0
        ])
        specAttr.addAttribute(.foregroundColor, value: UIColor.primary100, range: NSRange(location: 6, length: 2))
        specSubInfoLabel.attributedText = specAttr
        
        appearenceView.rx
            .tapGesture()
            .filter{ $0.state == .ended }
            .flatMapLatest{ _ in self.registerSpecFace() }
            .subscribe(onNext: { success in
                guard success else { return }
                let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .nickname, isEditMode: false, backType: .back)
                vc.nicknameBackToSpec = {
                    self.cancelSpec()
                        .subscribe(onNext: { _ in })
                        .disposed(by: self.bag)
                }
                self.push(vc)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                if custom.resultCD == RLResultCD.NETWORK_ERROR.code {
                    alertNETWORK_ERR(msg: custom.errorMessage) { }
                }else if custom.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    toast(message: custom.errorMessage, seconds: 1.5) { }
                }
            })
            .disposed(by: bag)
        
        
        specView.rx
            .tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.join.specListViewController(isEditMode: self.isEditMode, backType: .back)
                self.push(vc)
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "인증 심사"
    }
    
    func cancelSpec() -> Observable<Bool>{
        return Repository.shared.graphQl
            .cancelSpec(userNo: MainInformation.shared.userNoAes!)
    }
    
    func registerSpecFace() -> Observable<Bool> {
        return Repository.shared.main
            .addSpecFace(userNo: MainInformation.shared.userNoAes!)
    }
}
