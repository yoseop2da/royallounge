//
//  AboutMeViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/11.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AboutMeViewController: ClearNaviLoggedBaseViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textWrapView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeHolder: UILabel!
    @IBOutlet weak var limitTextLabel: UILabel!
//    @IBOutlet weak var limitTextBottom: NSLayoutConstraint!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    let nextButton = RLNextButton.init(title: "다음")
    
    var viewModel: AboutMeViewModel!
    var isEditMode: Bool = false
    
    private var rejectView: RejectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        addTapToCloseKeyboard() //복사 및 붙여넣기와 이벤트가 중복되어 제거
        
        nextButton.title = isEditMode ? "저장" : "다음"
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        
        let rejectViewPositionY = 38
        rejectView = RejectView(rejectMsg: "", isUpArrow: false, textAlignment: .center)
        mainTableView.addSubview(rejectView!)
        view.bringSubviewToFront(rejectView!)
        rejectView!.snp.updateConstraints{
            $0.top.equalToSuperview().offset(rejectViewPositionY)
            $0.leading.trailing.width.equalToSuperview()
        }
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }
               
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }

        let barButton = RLBarButtonItem(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                if !MainInformation.shared.isJoinOrReject && self.viewModel.isEdited(current: self.textView.text) {
                    RLPopup.shared.showNormal(title: "정말 나가시겠습니까?", description: "수정중인 자기소개가 있을 경우 저장되지 않습니다.", leftButtonTitle: "취소", rightButtonTitle: "나가기", leftAction: {}, rightAction: {
                        self.pushBack()
                    })
                    return
                }
                self.pushBack(animated: true)
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton

        textWrapView.layer.cornerRadius = 6.0
        textWrapView.setBorder(width: 1.0, color: .gray10)
        
        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: 4, bottom: 0, right: 4)
        textView.rx.text.orEmpty
            .map{ $0.count }
            .subscribe(onNext: { count in
                self.placeHolder.isHidden = count > 0
                if count >= 1000 {
                    self.limitTextLabel.text = "최대 1000자"
                    self.textView.text = (self.textView.text as NSString).substring(with: NSMakeRange(0, 1000))
                }else{
                    self.limitTextLabel.text = "최소 10자"
                }
            })
            .disposed(by: bag)

        viewModel = AboutMeViewModel.init(
            fieldString: textView.rx.text.orEmpty
                .distinctUntilChanged()
                .debounce(RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance).asObservable(),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.myAboutMeValue
            .bind(to: textView.rx.text)
            .disposed(by: bag)
        
        viewModel.rejectInfo
            .subscribe(onNext: { info in
                self.rejectView?.rejectMsg = info.msg
                self.rejectView?.isHidden(!info.display, animated: true)
            })
            .disposed(by: bag)
        
        viewModel.validated
            .subscribe(onNext: { result in
                print("result : \(result)")
                switch result {
                case .empty, .failed:
                    self.limitTextLabel.textColor = .primary100
                    self.textView.textColor = .rlBlack21
                case .failedProhibit:
                    self.limitTextLabel.textColor = .primary100
                    self.textView.textColor = result.textViewTextColor
                default:
                    self.limitTextLabel.textColor = .gray75
                    self.textView.textColor = .rlBlack21
                }
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.mainButtonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { _ in
                if self.isEditMode {
                    self.pushBack()
                }else{
                    Coordinator.shared.toJoinStep5()
                }
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                print("---------height: \(keyboard.height)")
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height == 0 ? 30 : keyboard.height
//                    self.limitTextBottom.constant = keyboard.height == 0 ? 73 : 12
                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : self.isEditMode ? 48 - 18 : 48)

                    if keyboard.height == 0 {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }
                    self.view.layoutIfNeeded()
                }
                mainAsync {
                    var frame = self.mainTableView.frame
                    frame.size = CGSize.init(width: self.mainTableView.frame.width, height: self.mainTableView.frame.height + (keyboard.height == 0 ? keyboard.height : 48))
                    self.headerView.frame = frame
                    self.mainTableView.reloadData()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "자기소개 등록"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    @IBAction func keyboardDownButton(_ sender: Any) {
        view.endEditing(true)
    }
    
}

