//
//  AboutMeViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AboutMeViewModel: BaseViewModel {
    let myAboutMeValue = PublishSubject<String>.init()
    let rejectInfo = BehaviorRelay<(display: Bool, msg: String?)>.init(value: (display: false, msg: nil))
    
    let validated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    var dbValue: String?
    
    init(fieldString: Observable<String>,
         nextButtonTap: Observable<Void>) {
        
        super.init()
        Validation.shared.updateProhibitWord()
        
        Repository.shared.graphQl
            .getAboutMe(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                let myValue = $0
                if let rejectMsg = myValue.rejectMsg {
                    self.rejectInfo.accept((display: true, msg: "\"\(rejectMsg)\"\n수정 후 다시 입력하세요!"))
                }
                self.dbValue = myValue.aboutMe
                self.myAboutMeValue.on(.next(myValue.aboutMe))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
//        let myValue = Repository.shared.graphQl
//            .getAboutMe(userNo: MainInformation.shared.userNoAes!)
//            .share(replay: 1)
//
//        // 반려메세지 표시에 사용.
//        myValue.filter{$0.rejectMsg != nil}
//            .map{$0.rejectMsg!}
//            .map { (display: true, msg: "\"\($0)\"\n수정 후 다시 입력하세요!") }
//            .bind(to: rejectInfo)
//            .disposed(by: bag)

        // 유저필드 변화 감지
        fieldString
            .map{
                let result = Validation.shared.prohibitCheck(word: $0)
                if case .ok = result { return Validation.shared.validate(aboutMe: $0) }
                return result }
            .bind(to: validated)
            .disposed(by: bag)

        fieldString
            .filter{!$0.isEmpty}
            .subscribe(onNext: { str in
                var reject = self.rejectInfo.value
                let isReject = reject.msg != nil
                if isReject {
                    reject.display = str == self.dbValue
                }else{
                    reject.display = false
                }
                self.rejectInfo.accept(reject)
            })
            .disposed(by: bag)

//        myValue.map{
//            self.dbValue = $0.aboutMe
//            return $0.aboutMe }
//            .bind(to: myAboutMeValue)
//            .disposed(by: bag)
        
//        validated
//            .map { $0.isValid }
//            .distinctUntilChanged()
//            .bind(to: nextButtonEnabled)
//            .disposed(by: bag)
        
        Observable
            .combineLatest(validated, rejectInfo, fieldString) {
                let isReject = ($1.msg != nil)
                let isEditedRejectValue = $2 != self.dbValue
                if isReject && !isEditedRejectValue { return false }
                return $0.isValid }
            .distinctUntilChanged()
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)
        
        nextButtonTap.withLatestFrom(fieldString)
            .flatMapLatest { value -> Observable<Bool> in
                let userNo = MainInformation.shared.userNoAes!
                return Repository.shared.graphQl
                    .updateAboutMe(userNo: userNo, aboutMe: value) }
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
    
    func isEdited(current: String) -> Bool {
        if let value = dbValue {
            return value != current
        }
        return false
    }
}
