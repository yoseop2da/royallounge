
//
//  AreaCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AreaCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    private var bag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setItem(item: AreaModel) {
        nameLabel.text = item.name  
        let bgColor = item.isSelected ? UIColor.gray25 : UIColor.gray10
        backgroundColor = bgColor
        
    }

}
