//
//  AreaData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

class AreaModel {
    var areaNo: String
    var name: String
    var isSelected = false
    init?(areaNo: String?, name: String?, isSelected: Bool = false) {
        guard let _areaNo = areaNo else { return nil }
        guard let _name = name else { return nil }
        self.areaNo = _areaNo
        self.name = _name
        self.isSelected = isSelected
    }
}

struct AreaSection {
    var header: String
    var areaNo: String?
    var isOpened: Bool
    var items: [AreaModel]
}

extension AreaSection: SectionModelType {
    typealias Item = AreaModel
    init(original: AreaSection, items: [Item]) {
        self = original
        self.items = items
    }
}
