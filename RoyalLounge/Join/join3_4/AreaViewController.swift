//
//  AreaViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/09.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class AreaViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<AreaSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
//    @IBOutlet weak var nextWarpViewHeight: NSLayoutConstraint!
    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!

    @IBOutlet weak var nextButton: UIButton!
//    let nextButton = RLNextButton.init(title: "저장")
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> AreaCell in
            let cell: AreaCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item: item)
            return cell
        })
        
        return dataSource
    }
    
    var viewModel: AreaViewModel!
    var isEditMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.nextWarpViewHeight.constant = self.isEditMode ? 48.0 : 0.0
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        
        let barButton = RLBarButtonItem(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack(animated: true)
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        if isEditMode {
//            nextButtonWrapView.addSubview(nextButton)
//            nextButton.squareCorner()
//            nextButton.snp.makeConstraints {
//                $0.top.leading.trailing.equalToSuperview()
//                $0.height.equalTo(48)
//            }
            nextButton.rx.tap
                .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance)
                .bind { _ in
                    self.viewModel.save()
                }.disposed(by: bag)
            
        }else{
            nextButtonWrapView.isHidden = true
        }
        
        viewModel = AreaViewModel.init()
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                if self.isEditMode {
                    // 뒤로가기
                    self.pushBack()
                }else {
                    guard success else { return }
                    // 다음 스텝으로 이동
                    let vc = RLStoryboard.join.profileTagViewController(optionType: (MainInformation.shared.gender == .m ? .charM : .charW), isEditMode: false)
                    self.push(vc)
                }
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
        
        mainTableView.rx.itemSelected
            .subscribe(onNext: { indexPath in
                self.viewModel.add(indexPath: indexPath, isEditMode: self.isEditMode)
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "프로필 등록"
    }
}

extension AreaViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.isOpened(idx: indexPath.section) {
            return UITableView.automaticDimension
        }else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init()
        let titleLabel = UILabel.init()
        let isSelected = viewModel.isSelected(idx: section)
        titleLabel.font = UIFont.spoqaHanSansBold(ofSize: 18.0)
        titleLabel.text = viewModel.headerTitle(idx: section)
        titleLabel.textColor = isSelected ? .primary100 : .gray75
        headerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.top.bottom.equalToSuperview()
        }
        let button = UIButton.init()
        headerView.addSubview(button)
        button.snp.makeConstraints { $0.leading.trailing.top.bottom.equalToSuperview() }
        button.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.viewModel.select(section: section, isEditMode: self.isEditMode)
            })
            .disposed(by: bag)
        
        if viewModel.canOpen(idx: section) {
            let img = UIImageView.init()
            img.image = UIImage.init(named: isSelected ? "icon16PxArrowUp" : "icon16PxArrowDown")
            img.tintColor = .dark
            headerView.addSubview(img)
            img.snp.makeConstraints {
                $0.width.equalTo(18)
                $0.height.equalTo(16)
                $0.centerY.equalToSuperview()
                $0.trailing.equalToSuperview().offset(-32)
            }
        }
        let line = UIView.init()
        line.backgroundColor = .gray10
        headerView.addSubview(line)
        line.snp.makeConstraints {
            $0.height.equalTo(1)
            $0.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        return headerView
    }
}
