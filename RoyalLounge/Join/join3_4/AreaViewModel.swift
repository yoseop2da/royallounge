//
//  AreaViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AreaViewModel: BaseViewModel {
    let myValue = PublishSubject<String>.init() // areaNo
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[AreaSection]>.init(value: [])
    
    let nextSuccess = PublishSubject<Bool>.init()
    
    override init() {
        super.init()
        let listData = Repository.shared.graphQl
            .getAreaList()
            .map{ tuple -> [AreaSection] in
                return tuple.compactMap {
                    let name = $0.name
                    let items = $0.areaList.compactMap{ AreaModel.init(areaNo: $0.areaNo, name: $0.name) }
                    if items.count == 0 { return nil }
                    if items.count == 1 {
                        let newname = "\(name) - \(items[0].name)"
                        let areaNo = items[0].areaNo
                        return AreaSection.init(header: newname, areaNo: areaNo, isOpened: false, items: [])
                    }else{
                        return AreaSection.init(header: name, areaNo: nil, isOpened: false, items: items)
                    }
                }
            }
        
        Repository.shared.graphQl
            .getAreaNo(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: { value in
                self.myValue.on(.next(value))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        Observable.combineLatest(listData, myValue)
            .subscribe(onNext: { list, myAreaNo in
                self.sections.accept(list.map { data -> AreaSection in
                    var newData = data
                    if let areaNo = newData.areaNo {
                        newData.isOpened = myAreaNo == areaNo
                    }else{
                        newData.items = newData.items.map {
                            $0.isSelected = $0.areaNo == myAreaNo
                            return $0
                        }
                        newData.isOpened = newData.items.filter{$0.isSelected}.count > 0
                    }
                    return newData
                })
        }, onError: {
            $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
        })
        .disposed(by: bag)
        
    }
    
    func select(section: Int, isEditMode: Bool) {
        var dataList = sections.value
        let data = dataList[section]
        dataList = dataList.map{
            var newData = $0
            if newData.isOpened {
                // 열려있음
                let willOpen = false
                var items: [AreaModel] = newData.items
                items = newData.items.compactMap {
                    let newItem = $0
                    newItem.isSelected = false
                    return newItem
                }
                newData.isOpened = willOpen
                newData.items = items
            }else{
                // 닫혀있음
                let willOpen = data.header == newData.header
                var items: [AreaModel] = newData.items
                if willOpen {
                }else{
                    items = newData.items.compactMap {
                        let newItem = $0
                        newItem.isSelected = false
                        return newItem
                    }
                }
                newData.isOpened = willOpen
                newData.items = items
            }
            
            return newData
        }
        sections.accept(dataList)
        if isEditMode { return }
        save()
    }
    
    func save() {
        var selectedAreaNo: String?
        for section in sections.value {
            if section.isOpened {
                if let areaNo = section.areaNo {
                    // 하위 뎁스 없음 (비 수도권 지역)
                    selectedAreaNo = areaNo
                }else{
                    // 하위뎁스 있음 (서울, 경기)
                    let selectedItem = section.items.filter{ $0.isSelected }.first
                    selectedAreaNo = selectedItem?.areaNo
                }
                break
            }
        }
        
        if let areaNo = selectedAreaNo {
            Repository.shared.graphQl
                .updateAreaNo(userNo: MainInformation.shared.userNoAes!, areaNo: areaNo)
                .bind(to: nextSuccess)
                .disposed(by: bag)
            return
        }
        nextSuccess.on(.next(false))
    }

    func add(indexPath: IndexPath, isEditMode: Bool) {
        let data = sections.value
        let area = data[indexPath.section].items[indexPath.row]
        sections.accept(data.map{
            var newData = $0
            newData.items = newData.items.compactMap {
                let newItem = $0
                newItem.isSelected = newItem.areaNo == area.areaNo
                return newItem
            }
            return newData
        })
        if isEditMode { return }
        save()
    }
    
    func headerTitle(idx: Int) -> String {
        let data = sections.value
        return data[idx].header
    }
    
    func isSelected(idx: Int) -> Bool {
        let data = sections.value
        let value = data[idx]
        return value.isOpened
    }
    
    func isOpened(idx: Int) -> Bool {
        let data = sections.value
        let value = data[idx]
        if value.items.count == 0 { return false }
        return value.isOpened
    }
    func canOpen(idx: Int) -> Bool {
        let data = sections.value
        return data[idx].items.count > 0
    }
}

