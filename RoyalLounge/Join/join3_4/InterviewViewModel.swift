//
//  InterviewViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class InterviewViewModel: BaseViewModel {
    let myInterviewTuple = PublishSubject<(guestionNo: String, question: String, answer: String, example: String)>.init()
    let rejectInfo = BehaviorRelay<(display: Bool, msg: String?)>.init(value: (display: false, msg: nil))
    
    let validated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    var dbValue: String?
    
    init(fieldString: Observable<String>,
         interViewSeq: Int,
         nextButtonTap: Observable<Void>) {
        
        super.init()
        Validation.shared.updateProhibitWord()
        
        Repository.shared.graphQl
            .getInterView(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                guard $0.count >= interViewSeq else { return }
                let myValue = $0[interViewSeq-1]
                if let rejectMsg = myValue.rejectMsg {
                    self.rejectInfo.accept((display: true, msg: "\"\(rejectMsg)\"\n수정 후 다시 입력하세요!"))
                }
                self.dbValue = myValue.interview ?? ""
                self.myInterviewTuple.on(.next((guestionNo: myValue.questionNo!, question: myValue.question!, answer: myValue.interview ?? "", example: myValue.example ?? "")))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
//        let myValue = Repository.shared.graphQl
//            .getInterView(userNo: MainInformation.shared.userNoAes!)
//            .filter{ ($0.count >= interViewSeq) }
//            .map{ ($0[interViewSeq-1]) } // count로 index비교
//            .share(replay: 1)
//
//
//        // 반려메세지 표시에 사용.
//        myValue.filter{$0.rejectMsg != nil}.map{$0.rejectMsg!}
//            .map { (display: true, msg: "\"\($0)\"\n수정 후 다시 입력하세요!") }
//            .bind(to: rejectInfo)
//            .disposed(by: bag)
        
        fieldString
            .map{
                let result = Validation.shared.prohibitCheck(word: $0)
                if case .ok = result { return Validation.shared.validate(interView: $0) }
                return result }
            .bind(to: validated)
            .disposed(by: bag)
        
        fieldString
            .filter{!$0.isEmpty}
            .subscribe(onNext: { str in
                var reject = self.rejectInfo.value
                let isReject = reject.msg != nil
                if isReject {
                    reject.display = str == self.dbValue
                }else{
                    reject.display = false
                }
                self.rejectInfo.accept(reject)
            })
            .disposed(by: bag)
        
//        // 화면 표시용
//        myValue.map{
//            self.dbValue = $0.interview ?? ""
//            return (guestionNo: $0.questionNo!, question: $0.question!, answer: $0.interview ?? "", example: $0.example ?? "") }
//            .bind(to: myInterviewTuple)
//            .disposed(by: bag)
        
        Observable.combineLatest(validated, rejectInfo, fieldString) {
            let isReject = ($1.msg != nil)
            let isEditedRejectValue = $2 != self.dbValue
            if isReject && !isEditedRejectValue { return false }
            return $0.isValid }
            .distinctUntilChanged()
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)

        nextButtonTap.withLatestFrom(fieldString)
            .flatMapLatest { value -> Observable<Bool> in
                let userNo = MainInformation.shared.userNoAes!
                return Repository.shared.graphQl
                    .updateInterview(userNo: userNo, interview: value, questionNo: interViewSeq) }
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
    
    func isEdited(current: String) -> Bool {
        if let value = dbValue {
            return value != current
        }
        return false
    }
}

//            .flatMapLatest{ loggedIn -> Observable<Bool> in
//                let message = loggedIn ? "Github에 로그인됨." : "로그인 실패!!!!"
//                return wireframe.promptFor(message, cancelAction: "OK", actions:[])
//                    .map { _ in loggedIn } }

