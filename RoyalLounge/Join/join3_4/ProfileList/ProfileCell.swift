//
//  ProfileCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/09.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ProfileCell: UITableViewCell {

    @IBOutlet weak var itemLabel: UILabel!
//    @IBOutlet weak var itemButton: UIButton!

    private var bag = DisposeBag()
    
    var selectItem: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        self.backgroundColor = .clear
        itemLabel.layer.cornerRadius = 6
        itemLabel.layer.borderWidth = 1.0
        itemLabel.layer.borderColor = UIColor.gray10.cgColor
        itemLabel.clipsToBounds = true
        
        layer.masksToBounds = false

        itemLabel.rx.tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
                self.itemLabel.touchAnimation {
                    self.selectItem?()
                }
            })
            .disposed(by: bag)
        
    }
    
    func setModel(item: AppOptionModel) {
        itemLabel.text = item.optionValue
        let borderColor = item.isSelected ? .primary100 : UIColor.gray10
        itemLabel.layer.borderColor = borderColor.cgColor
        
        let textColor = item.isSelected ? .primary100 : UIColor.dark
        itemLabel.textColor = textColor
        
//        let bgColor = item.isSelected ? .primary100 : UIColor.white
//        itemLabel.backgroundColor = bgColor
    }
}
