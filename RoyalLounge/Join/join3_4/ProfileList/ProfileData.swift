//
//  ProfileDataSource.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/09.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

import RxDataSources

struct ProfileSection {
    var header: String
    var items: [AppOptionModel]
}

extension ProfileSection: SectionModelType {
    typealias Item = AppOptionModel
    init(original: ProfileSection, items: [Item]) {
        self = original
        self.items = items
    }
}
