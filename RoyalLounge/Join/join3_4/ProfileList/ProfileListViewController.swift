//
//  ProfileListViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/09.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class ProfileListViewController: LoggedBaseViewController {
    typealias DataSource = RxTableViewSectionedReloadDataSource<ProfileSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var nextWarpViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    
    let nextButton = RLNextButton.init(title: "저장")

    var optionType: AppOptionType = .bodyM
    var isEditMode: Bool = false
    var viewModel: ProfileListViewModel!
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, tableView, indexPath, item -> ProfileCell in
            let cell: ProfileCell = tableView.dequeueReusable(for: indexPath)
                cell.setModel(item: item)
            cell.selectItem = {
                print("----------\(item)")
                self.viewModel.selected(item: item, isEditMode: self.isEditMode)
            }
            return cell
        })
        
        return dataSource
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch optionType {
        case .bodyM, .bodyW:
            titleLabel.text = "체형"
            subTitleLabel.text = "나의 체형은?"
        case .drink:
            titleLabel.text = "주량"
            subTitleLabel.text = "음주 성향은 어떻게 되세요?"
        case .religion:
            titleLabel.text = "종교"
            subTitleLabel.text = "현재 믿고 있는 종교가 있나요?"
        case .education:
            titleLabel.text = "학력"
            subTitleLabel.text = "최종 학력을 선택하세요"
        default:
            ()
        }
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.nextWarpViewHeight.constant = self.isEditMode ? 48.0 : 0.0
        self.bottomPadding.constant = self.isEditMode ? 30.0 : 0.0
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        
        if isEditMode {
            nextButtonWrapView.addSubview(nextButton)
            nextButton.snp.makeConstraints {
                $0.top.bottom.equalToSuperview()
                $0.leading.equalToSuperview().offset(20)
                $0.trailing.equalToSuperview().offset(-20)
            }
            
            viewModel = ProfileListViewModel(
                optionType: optionType,
                nextButtonTap: nextButton.rx.tap
                    .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance).asObservable()
            )
        }else{
            nextButtonWrapView.isHidden = true
            viewModel = ProfileListViewModel(optionType: optionType)
        }
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.nickname
            .map{
                switch self.optionType {
                case .bodyM, .bodyW:
                    return "\($0)님의 체형은?"
                case .drink:
                    return "음주 성향은 어떻게 되세요?"
                case .religion:
                    return "현재 믿고 있는 종교가 있나요?"
                case .education:
                    return "최종 학력을 선택하세요"
                default:
                    return ""
                }}
            .bind(to: subTitleLabel.rx.text)
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                if self.isEditMode {
                    // 뒤로가기
                    self.pushBack()
                }else {
                    if let nextType = self.optionType.nextType {
                        let vc = RLStoryboard.join.profileListViewController(optionType: nextType, isEditMode: false)
                        self.push(vc)
                    }else{
                        // 다음 스텝으로 이동
                        if self.optionType == .religion {
                            // 거주지
                            let vc = RLStoryboard.join.areaViewController(isEditMode: false)
                            self.push(vc)
                        }else if self.optionType == .education {
                            // 직업
                            let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .job, isEditMode: false)
                            self.push(vc)
                        }
                    }
                }
                
                
            })
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "프로필 등록"
    }
}

extension ProfileListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68//UITableView.automaticDimension
    }
}
