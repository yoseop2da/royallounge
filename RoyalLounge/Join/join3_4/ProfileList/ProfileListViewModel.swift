//
//  ProfileListViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/09.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ProfileListViewModel: BaseViewModel {

    var myValue = PublishSubject<String>.init()
    let nickname = PublishSubject<String>.init()
    
    var sections = BehaviorRelay<[ProfileSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    var selectValue = PublishRelay<AppOptionModel?>.init()
    let nextSuccess = PublishSubject<Bool>.init()
    var optionType: AppOptionType!
    
    init(optionType: AppOptionType,
         nextButtonTap: Observable<Void>? = nil) {
        super.init()
        self.optionType = optionType
        let listData = Repository.shared.main
            .appOption(appOptionType: optionType)
            .map{ [ProfileSection(header: "", items: $0)] }
        
        
        switch optionType {
        case .bodyM, .bodyW:
            Repository.shared.graphQl
                .getBody(userNo: MainInformation.shared.userNoAes!)
                .subscribe(onNext: {
                    self.myValue.on(.next($0.bodyCd))
                    self.nickname.on(.next($0.nickname))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        case .drink:
            Repository.shared.graphQl
                .getDrink(userNo: MainInformation.shared.userNoAes!)
                .subscribe(onNext: {
                    self.myValue.on(.next($0.drinkCd))
                    self.nickname.on(.next($0.nickname))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        case .religion:
            Repository.shared.graphQl
                .getReligion(userNo: MainInformation.shared.userNoAes!)
                .subscribe(onNext: {
                    self.myValue.on(.next($0.religionCd))
                    self.nickname.on(.next($0.nickname))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        case .education:
            Repository.shared.graphQl
                .getEducation(userNo: MainInformation.shared.userNoAes!)
                .subscribe(onNext: {
                    self.myValue.on(.next($0.eduCd))
                    self.nickname.on(.next($0.nickname))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        default: ()
        }
        
        Observable.combineLatest(listData, myValue)
            .subscribe(onNext: { list, selectedOptionCD in
                if selectedOptionCD.count == 0 {
                    self.sections.accept(list)
                }else{
                    let newItems = (list.first?.items.map{ _item -> AppOptionModel in
                        var newItem = _item
                        let isSelected = _item.optionCD == selectedOptionCD
                        newItem.isSelected = isSelected
                        if isSelected {
                            self.selectValue.accept(newItem)
                        }
                        return newItem
                        } ?? [])
                    self.sections.accept([ProfileSection.init(header: "", items: newItems)])
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        if let next = nextButtonTap {
            next.throttle(.milliseconds(300), latest: false, scheduler: MainScheduler.instance)
            .withLatestFrom(selectValue)
            .filter{$0 != nil}
            .flatMapLatest { value -> Observable<Bool> in
                let userNo = MainInformation.shared.userNoAes!
                switch optionType {
                case .bodyM, .bodyW:
                    return Repository.shared.graphQl
                        .updateBody(userNo: userNo, bodyCd: value!.optionCD)
                case .drink:
                    return Repository.shared.graphQl
                        .updateDrink(userNo: userNo, drinkCd: value!.optionCD)
                case .religion:
                    return Repository.shared.graphQl
                        .updateReligion(userNo: userNo, religionCd: value!.optionCD)
                case .education:
                    return Repository.shared.graphQl
                        .updateEducation(userNo: userNo, eduCd: value!.optionCD)
                default:
                    return Observable.just(false)
                }}
                .subscribe(onNext: {
                    self.nextSuccess.on(.next($0))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
        }
    }
    
    func selected(item: AppOptionModel, isEditMode: Bool) {
        if let firstSection = sections.value.first, firstSection.items.count > 0 {
            let items = firstSection.items
                .map { _item -> AppOptionModel in
                    var newItem = _item
                    newItem.isSelected = _item.optionCD == item.optionCD
                    return newItem
            }
            sections.accept([ProfileSection.init(header: firstSection.header, items: items)])
            selectValue.accept(item)
            
            if !isEditMode {
                let userNo = MainInformation.shared.userNoAes!
                switch optionType {
                case .bodyM, .bodyW:
                    Repository.shared.graphQl
                        .updateBody(userNo: userNo, bodyCd: item.optionCD)
                        .bind(to: nextSuccess)
                        .disposed(by: bag)
                case .drink:
                    Repository.shared.graphQl
                        .updateDrink(userNo: userNo, drinkCd: item.optionCD)
                        .bind(to: nextSuccess)
                        .disposed(by: bag)
                case .religion:
                    Repository.shared.graphQl
                        .updateReligion(userNo: userNo, religionCd: item.optionCD)
                        .bind(to: nextSuccess)
                        .disposed(by: bag)
                case .education:
                    Repository.shared.graphQl
                        .updateEducation(userNo: userNo, eduCd: item.optionCD)
                        .bind(to: nextSuccess)
                        .disposed(by: bag)
                default: ()
                    //Observable.just(false)
                }
            }
        }
    }
    
    
    
}
