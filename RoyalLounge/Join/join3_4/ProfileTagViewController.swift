//
//  ProfileTagViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileTagViewController: LoggedBaseViewController {

    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var nextWarpViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    
    let nextButton = RLNextButton.init(title: "저장")
    
    var isEditMode = false
    var optionType: AppOptionType = .charM
    
    var viewModel: ProfileTagViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch optionType {
        case .charM, .charW:
            titleLabel.text = "성격"
            subTitleLabel.text = "나의 매력적인 성격을 3가지 골라주세요"//"나의 매력적인 성격을 골라주세요."
        default: ()
        }
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.nextWarpViewHeight.constant = self.isEditMode ? 48.0 : 0.0
        self.bottomPadding.constant = self.isEditMode ? 30.0 : 0.0
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        
        tagListView.backgroundColor = .clear
        tagListView.delegate = self
        tagListView.textFont = UIFont.spoqaHanSansBold(ofSize: 14.0)
        tagListView.paddingX = 22
        tagListView.paddingY = 8
        tagListView.marginX = 8
        tagListView.marginY = 20
        tagListView.cornerRadius = 15
        tagListView.borderWidth = 1.0
        tagListView.borderColor = UIColor.brown75
        tagListView.textColor = UIColor.brown75
        tagListView.tagBackgroundColor = UIColor.clear
        tagListView.tagHighlightedBackgroundColor = UIColor.gray50
        tagListView.tagSelectedBackgroundColor = UIColor.brown75
        tagListView.alignment = .left
        
        if isEditMode {
            nextButtonWrapView.addSubview(nextButton)
            nextButton.snp.makeConstraints {
                $0.top.bottom.equalToSuperview()
                $0.leading.equalToSuperview().offset(20)
                $0.trailing.equalToSuperview().offset(-20)
            }
            viewModel = ProfileTagViewModel.init(
                optionType: optionType,
                nextButtonTap: nextButton.rx.tap
                .throttle(.milliseconds(300), latest: false, scheduler: MainScheduler.instance).asObservable()
            )
        }else{
            viewModel = ProfileTagViewModel.init(optionType: optionType)
        }
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.mainButtonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                if self.isEditMode {
                    // 뒤로가기
                    self.pushBack()
                }else {
                    // 다음 스텝으로 이동
                    if self.optionType == .charM || self.optionType == .charW {
                        let vc = RLStoryboard.join.profileListViewController(optionType: .education, isEditMode: false)
                        self.push(vc)
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.dataList
            .observeOn(MainScheduler.instance)
            .map{ tuple ->(optionValueList: [String], selectedList: [AppOptionModel]) in
                let optionList = tuple.allList
                let list = optionList.map { $0.optionValue }
                return (optionValueList: list, selectedList: tuple.selectedList) }
            .subscribe(onNext: { (optionValueList, selectedList) in
                self.tagListView.removeAllTags()
                let _ = self.tagListView.addTags(optionValueList)
                selectedList.forEach { option in
                    self.tagListView.tagViews
                        .filter { option.optionValue == $0.currentTitle }
                        .forEach { $0.isSelected = true }
                }
                mainAsync {
                    self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: self.tagListView.intrinsicContentSize.height + 150)
                    self.mainTableView.reloadData()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "프로필 등록"
    }
}

extension ProfileTagViewController: TagListViewDelegate {
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
        viewModel.selected(title: title, isEditMode: isEditMode)
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
