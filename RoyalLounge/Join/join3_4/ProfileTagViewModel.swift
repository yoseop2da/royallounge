//
//  ProfileTagViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ProfileTagViewModel: BaseViewModel {
    var dataList = BehaviorRelay<(allList: [AppOptionModel], selectedList: [AppOptionModel])>.init(value: (allList: [], selectedList: []))
    private let myValue = PublishSubject<[String]>.init()
    let nextSuccess = PublishSubject<Bool>.init()
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    
    var optionType: AppOptionType!
    init(optionType: AppOptionType, nextButtonTap: Observable<Void>? = nil) {
        super.init()
        self.optionType = optionType
        let optionList = Repository.shared.main
            .appOption(appOptionType: optionType)
            
        Repository.shared.graphQl
            .getCharCdList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                self.myValue.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        self.dataList
            .map{ $0.selectedList.count >= 3}
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)
        
        Observable.combineLatest(optionList, myValue)
            .subscribe(onNext: { list, selectedCDList in
                var _selectedList: [AppOptionModel] = []
                let newItems = (list.map{ _item -> AppOptionModel in
                    var newItem = _item
                    let isSelected = selectedCDList.contains(_item.optionCD)
                    newItem.isSelected = isSelected
                    if isSelected {
                        _selectedList.append(newItem)
                    }
                    return newItem
                } )
                self.dataList.accept((allList: newItems, selectedList: _selectedList))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        if let next = nextButtonTap {
            next.withLatestFrom(dataList)
                .flatMapLatest { value -> Observable<Bool> in
                    if optionType == .charM || optionType == .charW {
                        let userNo = MainInformation.shared.userNoAes!
                        return Repository.shared.graphQl
                            .updateCharList(userNo: userNo, list: value.selectedList.map{ $0.optionCD })
                    }else{
                        return Observable.just(false)
                    }}
                .subscribe(onNext: {
                    self.nextSuccess.on(.next($0))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
//                .bind(to: nextSuccess)
                .disposed(by: bag)
        }
        
    }
    
    func selected(title: String, isEditMode: Bool) {
        var (allList, selectedList) = dataList.value
        let fullCount = 3
        let model = allList.first { $0.optionValue == title }!
        
        if (selectedList.map{ $0.optionCD }).contains(model.optionCD) {
            // 기존 항목에 있는 아이템을 제거하는경우
            selectedList.removeAll(where: { $0.optionCD == model.optionCD })
            dataList.accept((allList: allList, selectedList: selectedList))
            return
        }
         
        // 이미 풀카운트인 경우
        print("selectedList.count: \(selectedList.count)")
        if selectedList.count >= fullCount {
            toast(message: "\(fullCount)개까지만 선택이 가능합니다", seconds: 1.5, actionAfter: {
                self.dataList.accept((allList: allList, selectedList: selectedList))
//                self.nextSuccess.on(.next(true))
            })
            return
        }
        
        // 추가후 max보다 많은경우 패스
        if selectedList.count + 1 > fullCount {
            dataList.accept((allList: allList, selectedList: selectedList))
            self.nextSuccess.on(.next(true))
            return
        }
        
        // 추가
        selectedList.append(model)
        dataList.accept((allList: allList, selectedList: selectedList))
        
        if !isEditMode {
            guard selectedList.count == fullCount else { return }
            if optionType == .charM || optionType == .charW {
                let userNo = MainInformation.shared.userNoAes!
                Repository.shared.graphQl
                    .updateCharList(userNo: userNo, list: selectedList.map{ $0.optionCD })
                .bind(to: nextSuccess)
                .disposed(by: bag)
            }else{

            }
        }
    }
}
