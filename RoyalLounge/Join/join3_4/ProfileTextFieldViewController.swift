//
//  ProfileTextFieldViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

 class ProfileTextFieldViewController: ClearNaviLoggedBaseViewController {
    enum BackType {
        case back
        case empty
    }
    enum ViewType {
        case nickname
        case job
    }
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var nicknameWrapView: UIView!
    
    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    private var rejectView: RejectView?
    
    let nextButton = RLNextButton.init(title: "다음")
    
    let defaultTextField: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "로얄라운지", type: .big, isOnlyKorOrNum: true)
        field.keyboardType = .default//namePhonePad
//        field.textField.textAlignment = .left
        return field
    }()
    
    var nicknameBackToSpec: (()->Void)?
    var backType: BackType = .empty
    var isEditMode: Bool = false
    var viewType: ViewType = .nickname
    
    var viewModel: ProfileTextFieldViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.title = isEditMode ? "저장" : "다음"
        addTapToCloseKeyboard()

        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        let naviHeight = UIScreen.statusBarHeight + (self.navigationController?.navigationBar.frame.size.height ?? 0.0)
        let rejectViewPositionY = naviHeight + (self.isEditMode ? 0 : 20) + nicknameWrapView.frame.origin.y + 25 + 12 //176
        rejectView = RejectView(rejectMsg: "", isUpArrow: true, textAlignment: .center)
        view.addSubview(rejectView!)
        view.bringSubviewToFront(rejectView!)
        rejectView!.snp.updateConstraints{
            $0.top.equalToSuperview().offset(rejectViewPositionY)
            $0.leading.trailing.width.equalToSuperview()
        }
        
        if viewType == .nickname {
            titleLabel.text = "닉네임"
            subTitleLabel.text = "나를 표현할 멋진 닉네임을 완성해주세요"
            defaultTextField.placeholder = "로얄라운지"
            defaultTextField.maxLength = 10
            defaultTextField.isOnlyKorOrNum = true
        }else{
            titleLabel.text = "직업"
            subTitleLabel.text = "현재 직업은 무엇인가요?"
            defaultTextField.placeholder = "회사원"
            defaultTextField.maxLength = 15
            defaultTextField.isOnlyKorOrNum = false
        }
        
        nicknameWrapView.addSubview(defaultTextField)
        defaultTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        if isEditMode || viewType == .job  || backType == .back{
            let barButton = RLBarButtonItem.init(barType: .back)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.nicknameBackToSpec?()
                    self.pushBack()
                })
                .disposed(by: bag)
            self.navigationItem.leftBarButtonItem = barButton
        }
        
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        
        viewModel = ProfileTextFieldViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            fieldString: defaultTextField.textObserver
                .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance),
            viewType: viewType)
        
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .bind { _ in
                // 닉네임 회원 상태에 따라 분기
                if self.viewType == .job || MainInformation.shared.isJoinOrReject {
                    self.viewModel.save()
                    return
                }
                
                if self.viewModel.nicknameCrown.canPayable() {
                    RLPopup.shared.showNormal(description: "크라운 \(self.viewModel.nicknameCrown)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                        self.viewModel.save()
                    })
                }else{
                    RLPopup.shared.showMoveStore(needCrown: self.viewModel.nicknameCrown, reasonStr: "닉네임 변경을 위해", action: {
                        let vc = RLStoryboard.main.storeViewController()
                        self.push(vc)
                    })
                }
            }
            .disposed(by: bag)
        
        
        viewModel.rejectInfo
            .subscribe(onNext: { info in
                self.rejectView?.rejectMsg = info.msg
                self.rejectView?.isHidden(!info.display, animated: true)
            })
            .disposed(by: bag)
        
        viewModel.myTextValue
            .subscribe(onNext: { str in
                self.defaultTextField.text = str
            })
            .disposed(by: bag)
        
        viewModel.validated
            .bind(to: defaultTextField.rx.setProfileTextResult)
            .disposed(by: bag)
//        fieldResultOb
//            .bind(to: defaultTextField.bottomLine.rx.setResult)
//            .disposed(by: bag)
//        fieldResultOb
//            .bind(to: defaultTextField.bottomMsg.rx.setResult)
//            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)

        viewModel.nextSuccess
            .subscribe(onNext: { success in
                if self.isEditMode {
                    self.pushBack()
                }else{
                    print("----------- \(success ? "성공" : "실패")")
                    if self.viewType == .nickname {
                        let vc = RLStoryboard.join.tallViewController(isEditMode: false)
                        self.push(vc)
                    }else{
                        // 인터뷰
                        Coordinator.shared.toJoinStep4()
                    }
                }
                
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                print("---------height: \(keyboard.height)")
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height == 0 ? 30 : keyboard.height
                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 100)
                    
                    if keyboard.height == 0 {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }
                    
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "프로필 등록"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
}

