
//
//  ProfileTextFieldViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ProfileTextFieldViewModel: BaseViewModel {
    let myTextValue = PublishSubject<String>.init()
    let rejectInfo = BehaviorRelay<(display: Bool, msg: String?)>.init(value: (display: false, msg: nil))
    
    // input
    let inputFieldValue: BehaviorRelay<String> = BehaviorRelay.init(value: "")
    private let _fieldValidated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let validated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    private var userNo: String!
    private var viewType: ProfileTextFieldViewController.ViewType!
    var dbValue: String?
    var nicknameCrown: Int = 30
    
    init(userNo: String, fieldString: Observable<String>, viewType: ProfileTextFieldViewController.ViewType) {
        super.init()
        self.userNo = userNo
        self.viewType = viewType
        
        fieldString
            .bind(to: self.inputFieldValue)
            .disposed(by: bag)
        
        Validation.shared.updateProhibitWord()
        
        if viewType == .nickname {
            Repository.shared.main
                .getCrown(userNo: userNo)
                .subscribe(onNext: { crown in
                    self.nicknameCrown = crown?.nickname ?? 30
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
            
            Repository.shared.graphQl
                .getNicknameWithRejectMsg(userNo: userNo)
                .bind {
                    self.dbValue = $0.nickname
                    self.myTextValue.on(.next($0.nickname))
            
                    // 반려메세지 표시에 사용.
                    if let rejectMsg = $0.rejectMsg {
                        self.rejectInfo.accept((display: true, msg: "\"\(rejectMsg)\"\n수정 후 다시 입력하세요!"))
                    }
                }
                .disposed(by: bag)
            
            inputFieldValue.map{ (Validation.shared.prohibitCheck(word: $0), $0) }
                .flatMapLatest{ tuple -> Observable<TextFieldResult> in
                    let (result, str) = tuple
                    if case .ok = result { return Repository.shared.main.checkNickname(nickname: str.trimmed, userNo: userNo) }
                    return Observable.just(result) }
                .subscribe(onNext: {
                    self._fieldValidated.accept($0)
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
        }else{
            Repository.shared.graphQl
                .getJob(userNo: userNo)
                .subscribe(onNext: {
                    self.dbValue = $0.job
                    self.myTextValue.on(.next($0.job))
            
                    // 반려메세지 표시에 사용.
                    if let rejectMsg = $0.rejectMsg {
                        self.rejectInfo.accept((display: true, msg: "\"\(rejectMsg)\"\n수정 후 다시 입력하세요!"))
                    }
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
            
            inputFieldValue.map {
                let result = Validation.shared.prohibitCheck(word: $0)
                if case .ok = result { return Validation.shared.validate(job: $0) }
                return result }
                .subscribe(onNext: {
                    self._fieldValidated.accept($0)
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }
        
        inputFieldValue
            .filter{!$0.isEmpty}
            .subscribe(onNext: { str in
                var reject = self.rejectInfo.value
                let isReject = reject.msg != nil
                if isReject {
                    reject.display = str == self.dbValue
                }else{
                    reject.display = false
                }
                self.rejectInfo.accept(reject)
            })
            .disposed(by: bag)
        
        Observable.combineLatest(_fieldValidated, rejectInfo, inputFieldValue) {
            let isEditedRejectValue = $2 != self.dbValue
            if MainInformation.shared.isJoinOrReject {
                let isReject = ($1.msg != nil)
                if isReject && !isEditedRejectValue { return .failed(message: "")}
            }else{
                // 수정하러 왔는데 이전값이랑 동일한경우
                if !isEditedRejectValue { return .empty }
            }
            return $0 }
            .subscribe(onNext: {
                self.validated.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        validated.map{ $0.isValid }
            .distinctUntilChanged()
            .subscribe(onNext: {
                self.nextButtonEnabled.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func save() {
        let value = self.inputFieldValue.value
        if viewType == .nickname {
            Repository.shared.graphQl
                .updateNickname(userNo: userNo, nickname: value.trimmed)
                .subscribe(onNext: {
                    self.nextSuccess.on(.next($0))
//                    MainInformation.shared.updateMyCrownNew()
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }else{
            Repository.shared.graphQl
                .updateJob(userNo: userNo, job: value)
                .subscribe(onNext: {
                    self.nextSuccess.on(.next($0))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }
    }
}


