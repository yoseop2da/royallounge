//
//  TallViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

 class TallViewController: LoggedBaseViewController {
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var tallTextFieldWrapView: UIView!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    
    
    let nextButton = RLNextButton.init(title: "다음")

    var isEditMode: Bool = false
    
    let defaultTextField: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "170", type: .big, maxLength: 3, tallInput: true)
        field.keyboardType = .numberPad
        return field
    }()
    
    var viewModel: TallViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTapToCloseKeyboard()
    
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        nextButton.title = isEditMode ? "저장" : "다음"
        
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }

        tallTextFieldWrapView.addSubview(defaultTextField)
        defaultTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }

        titleLabel.text = "키"
        subTitleLabel.text = "키는?"

        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton

        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .bind { _ in
                // 회원 상태에 따라 분기
                if MainInformation.shared.isJoinOrReject {
                    self.viewModel.save()
                }else{
                    if self.viewModel.tallCrown.canPayable() {
                        RLPopup.shared.showNormal(description: "크라운 \(self.viewModel.tallCrown)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                            self.viewModel.save()
                        })
                    }else{
                        RLPopup.shared.showMoveStore(needCrown: self.viewModel.tallCrown, reasonStr: "키 변경을 위해", action: {
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        })
                    }
                }
            }
            .disposed(by: bag)
        
        viewModel = TallViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            fieldString: defaultTextField.textObserver.debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance))

        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.myTextValue
            .subscribe(onNext: { str in
                self.defaultTextField.text = str
            })
            .disposed(by: bag)

        viewModel.nickname
            .map {"\($0)님의 키는?"}
            .bind(to: subTitleLabel.rx.text)
            .disposed(by: bag)

        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)

        viewModel.nextSuccess
            .subscribe(onNext: { success in
                if success {
                    if self.isEditMode {
                        self.pushBack()
                    }else{
                        let vc = RLStoryboard.join.profileListViewController(optionType: (MainInformation.shared.gender == .m ? .bodyM : .bodyW), isEditMode: false)
                        self.push(vc)
                    }
                }
            })
            .disposed(by: bag)

        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height == 0 ? 30 : keyboard.height
                    if keyboard.height == 0 {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }

                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "프로필 등록"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
}
