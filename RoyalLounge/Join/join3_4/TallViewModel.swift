//
//  TallViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class TallViewModel: BaseViewModel {
    let myTextValue = PublishSubject<String>.init()
    let nickname = PublishSubject<String>.init()
    
    // input
    let inputFieldValue: BehaviorRelay<String> = BehaviorRelay.init(value: "")
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    var tallCrown: Int = 5
    private var userNo: String!
    
    var dbValue: String?
    
    init(userNo: String, fieldString: Observable<String>) {
        super.init()
        self.userNo = userNo
        fieldString
            .bind(to: self.inputFieldValue)
            .disposed(by: bag)
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .subscribe(onNext: { crown in
                self.tallCrown = crown?.tall ?? 5
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getTall(userNo: userNo)
            .subscribe(onNext: {
                self.nickname.on(.next($0.nickname))
                self.dbValue = $0.tall
                self.myTextValue.on(.next($0.tall))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        // 유저필드 변화 감지
        inputFieldValue
            .map {
                let result = Validation.shared.validate(tall: $0)
                if case .ok = result {
                    if MainInformation.shared.isJoinOrReject {
                        return true
                    }else{
                        if $0 == self.dbValue {
                            return false
                        }else{
                            return true
                        }
                    }
                }
                return result.isValid
            }
            .distinctUntilChanged()
            .subscribe(onNext: {
                self.nextButtonEnabled.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func save() {
        Repository.shared.graphQl
            .updateTall(userNo: userNo, tall: Int(self.inputFieldValue.value)!)
            .subscribe(onNext: {
                self.nextSuccess.on(.next($0))
//                MainInformation.shared.updateMyCrownNew()
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
}

