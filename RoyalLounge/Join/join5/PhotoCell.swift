//
//  PhotoCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainLabel: UIView!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var detectView: UIView!
    @IBOutlet weak var inicator: UIActivityIndicatorView!
    @IBOutlet weak var readyView: UIView!
    @IBOutlet weak var readyLabel: UILabel!
    
    private let bag = DisposeBag()
    var item: PhotoModel?
    var remove: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 6.0
        layer.masksToBounds = true
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.clear.cgColor
        
        mainLabel.layer.cornerRadius = 4.0
        mainLabel.layer.masksToBounds = true
        
        self.inicator.hidesWhenStopped = true
        self.inicator.stopAnimating()
        self.layer.borderWidth = 1.0
        readyView.layer.cornerRadius = 4.0
    }
    
    @IBAction func removeButtonTouched(_ sender: Any) {
        remove?()
    }
    
    func setData(item: PhotoModel) {
        self.item = item
        self.inicator.stopAnimating()
        self.mainLabel.isHidden = !item.isMain
        self.imageView.image = item.defaultPhoto
        self.removeButton.isHidden = true
        self.readyView.isHidden = true
        detectView.isHidden = true
        layer.borderColor = UIColor.clear.cgColor
        
        if let photo = item.photoData, let url = photo.photoUrl {
            self.inicator.startAnimating()
            self.detectView.isHidden = !photo.detectYn
            self.imageView.setImage(urlString: url, placeholderImage: item.defaultPhoto, progress: { _ in }) { _ in
                self.inicator.stopAnimating()
                self.removeButton.isHidden = false
                self.readyView.isHidden = true
                self.layer.borderColor = UIColor.gray10.cgColor
                self.layer.borderWidth = 1.0
                
                if photo.photoStatus == Str.reject {
                    self.layer.borderColor = UIColor.errerColor.cgColor
                    self.layer.borderWidth = 3.0
                }else if photo.photoStatus == Str.ready {
                    if MainInformation.shared.isJoinOrReject { return }
                    self.readyLabel.text = photo.photoStatusName
                    self.removeButton.isHidden = true
                    self.readyView.isHidden = false
                }
            }
        }else{
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }
}
