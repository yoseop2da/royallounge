//
//  PhotoHeaderView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class PhotoHeaderView: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
}


class PhotoFooterView: UICollectionReusableView {
    
}
