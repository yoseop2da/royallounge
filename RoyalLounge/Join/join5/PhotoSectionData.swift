//
//  PhotoDataSource.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxDataSources

struct PhotoModel {
    var isMain: Bool = false
    var defaultPhoto: UIImage
    var photoData: GetPhotoListQuery.Data.MemberPhotoList?
}

struct PhotoSection {
    var header: String
    var items: [Item]
}

extension PhotoSection: SectionModelType {
    typealias Item = PhotoModel//
    
    init(original: PhotoSection, items: [Item]) {
        self = original
        self.items = items
    }
}
