//
//  PhotoTipsPopup.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/30.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class PhotoTipsPopup: UIViewController {
    
    @IBOutlet weak var bottomMainView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    private var ANIMATION_TIMEINTERVAL: TimeInterval = 0.2

    private var OUTBUTTON_HIDDEN: CGFloat = 0.0
    private var OUTBUTTON_SHOWN: CGFloat = 1.0

    private var BOTTOM_VIEW_HIDDEN: CGFloat = 500.0
    private var BOTTOM_VIEW_SHOWN: CGFloat = 0.0

    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        delay(0.01) {
            self.bottomMainView.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        
        initialize()
    }

    override var prefersStatusBarHidden: Bool {
        return !UIScreen.hasNotch
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomViewShowAnimation(timeInterval: ANIMATION_TIMEINTERVAL)
    }

    func initialize() {
        self.bottomPadding.constant = BOTTOM_VIEW_HIDDEN
    }
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.dismiss()
        }
    }
    @IBAction func backButtonTouched(_ sender: Any) {
        self.dismiss()
    }

    var startPositionY: CGFloat?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            startPositionY = position.y
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let _startPositionY = startPositionY {
            let position = touch.location(in: view)
            let gap = position.y - _startPositionY
            let isDownAction = gap > 0
            self.bottomPadding.constant = isDownAction ? gap : gap*(0.2)
            let percent = CGFloat(1.0 - (isDownAction ? ((gap)/BOTTOM_VIEW_HIDDEN) : 0.0))
            let colorAlpha = CGFloat(0.6/*MAX*/ * percent)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(colorAlpha)

        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if bottomPadding.constant > 150 {
            self.dismiss()
        } else {
            self.bottomViewShowAnimation(timeInterval: 0.1)
        }
    }
}

extension PhotoTipsPopup {

    func dismiss(after: (() -> Void)? = nil) {
        bottomViewHideAnimation(after: after)
    }
    
    func bottomViewShowAnimation(timeInterval: TimeInterval) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = 0.0
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func bottomViewHideAnimation(after: (() -> Void)?) {
        UIView.animate(withDuration: self.ANIMATION_TIMEINTERVAL, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = self.BOTTOM_VIEW_HIDDEN
            self.view.backgroundColor = .clear
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: true, completion: { after?() })
        })
    }

}
