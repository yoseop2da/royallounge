//
//  PhotoViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import BottomPopup

class PhotoViewController: LoggedBaseViewController {

    typealias DataSource = RxCollectionViewSectionedReloadDataSource<PhotoSection>
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var nextButton: UIButton!
    
    var isEditMode: Bool = false
    var isReject: Bool = false
    var viewModel: PhotoViewModel!
    
    private var rejectView: RejectView?
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, collectionView, indexPath, item -> PhotoCell in
            let cell: PhotoCell = collectionView.dequeueReusable(for: indexPath)
            cell.setData(item: item)
            cell.remove = {
//                if self.viewModel.canRemove() {
                    print("remove item : \(item)")
                    if let photoNo = item.photoData?.photoNo {
                        RLPopup.shared.showNormal(description: "선택하신 사진이 삭제됩니다.\n그래도 삭제하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "삭제", leftAction: {}, rightAction: {
                            self.viewModel.remove(photoNo: photoNo)
                        })
                    }
//                }else{
//                    toast(message: "3장 이하의 사진은 삭제할 수 없습니다.", seconds: 1.0)
//                }
            }
            return cell
        })
        
        dataSource.configureSupplementaryView = { dataSource, collectionView, kind, indexPath in
            if kind == UICollectionView.elementKindSectionHeader {
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: PhotoHeaderView.className, for: indexPath) as! PhotoHeaderView
                header.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
                header.headerLabel?.text = "필수 3장"
                return header
            }else{
//                return UICollectionReusableView()
                let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: PhotoFooterView.className, for: indexPath) as! PhotoFooterView
                return section
            }
        }
        
        dataSource.moveItem = { dataSource, sourceIndexPath, destinationIndexPath in
            let from = sourceIndexPath.row
            let to = destinationIndexPath.row
            self.viewModel.change(from, to)
        }
        
        dataSource.canMoveItemAtIndexPath = { dataSource, indexPath in
            return true
        }
        return dataSource
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isEditMode {
            swipeBackAnyWhere()
            if !MainInformation.shared.isJoinOrReject {
                nextButton.isHidden = true
            }
        }
        
        nextButton.layer.cornerRadius = 6.0
        if isEditMode {
            let barButton = RLBarButtonItem.init(barType: .back)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.pushBack()
                })
                .disposed(by: bag)
            self.navigationItem.leftBarButtonItem = barButton
        }
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.isReject {
                    self.viewModel.nextAction()
                    return
                }
                if self.isEditMode {
                    self.pushBack()
                }else{
                    self.viewModel.nextAction()
                }
            })
            .disposed(by: bag)
        
        let flowLayout = DragAndDropFlowLayout()
        let count = 3
        let headerHeight = CGFloat(148)
        let padding = CGFloat(20) //CGFloat(3 * count)
        let innerPadding = UIScreen.underSE ? CGFloat(5) : CGFloat(12) //CGFloat(3 * count)
        let linePadding = CGFloat(12)
        let width = ((UIScreen.width - (CGFloat(count-1) * innerPadding) - (padding * 2)) / CGFloat(count)) - 1.0
        flowLayout.headerReferenceSize = CGSize(width: collectionView.frame.width, height: headerHeight)/*HEADER HEIGHT SIZE*/
        flowLayout.footerReferenceSize = CGSize(width: collectionView.frame.width, height: 100)/*HEADER HEIGHT SIZE*/
        flowLayout.itemSize = CGSize(width: width, height: width)
        flowLayout.minimumLineSpacing = linePadding
        flowLayout.minimumInteritemSpacing = innerPadding
        flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: padding, bottom: 0, right: padding)
        collectionView.collectionViewLayout = flowLayout
        
        let naviHeight = UIScreen.statusBarHeight + (self.navigationController?.navigationBar.frame.size.height ?? 0.0)
        let rejectViewPositionY = naviHeight + headerHeight + width*2.0 + linePadding
        rejectView = RejectView(rejectMsg: "", isUpArrow: true, textAlignment: .center)
        view.addSubview(rejectView!)
        view.bringSubviewToFront(rejectView!)
        rejectView!.snp.updateConstraints{
            $0.top.equalToSuperview().offset(rejectViewPositionY)
            $0.leading.trailing.width.equalToSuperview()
        }
        
        collectionView.rx.itemSelected
            .throttle(.seconds(1), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { indexPath in
                guard let cell = self.collectionView.cellForItem(at: indexPath) as? PhotoCell else { return }
                guard let model = cell.item else { return }
                if !MainInformation.shared.isJoinOrReject {
                    if let status = model.photoData?.photoStatus, status == Str.ready {
                        // 심사중
                        toast(message: "심사중인 사진은 수정 할 수 없습니다.", seconds: 1.0)
                        return
                    }
                }
                print("수정할 사진 model : \(model)")
                RLImagePicker.shared.load(self, imageType: .square) { result in
                    let (image, data) = result.first!
                    if let photoNo = model.photoData?.photoNo {
                        // 수정
                        self.viewModel.modifyPhoto(idx: indexPath.row, photoNo: photoNo, imageData: data, uploadImage: image)
                    }else{
                        // 추가
                        self.viewModel.addPhoto(imageData: data, uploadImage: image)
                    }
                    self.rejectView?.isHidden(true, animated: false)
                }
            })
            .disposed(by: bag)
        
        viewModel = PhotoViewModel.init(isReject: isReject)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.sectionData
          .bind(to: collectionView.rx.items(dataSource: dataSource))
          .disposed(by: bag)
        
        viewModel.rejectInfo
            .subscribe(onNext: { info in
                self.rejectView?.rejectMsg = info.msg
                self.rejectView?.isHidden(!info.display, animated: false)
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                if self.isEditMode {
                    self.pushBack()
                }else{
                    Coordinator.shared.toReady()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isEditMode ? "" : "사진 등록"
    }
    
    @IBAction func tipButtonTouched(_ sender: Any) {
        let vc = PhotoTipsPopup.init(nibName: PhotoTipsPopup.className, bundle: nil)
        self.clearPresent(vc)
    }
}

