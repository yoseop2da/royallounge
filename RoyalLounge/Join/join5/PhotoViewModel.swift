//
//  PhotoViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxSwift
import RxCocoa

class PhotoViewModel: BaseViewModel {
    
    let rejectInfo = BehaviorRelay<(display: Bool, msg: String?)>.init(value: (display: false, msg: nil))
    var sections = BehaviorRelay<[PhotoSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let nextButtonEnabled = BehaviorRelay<Bool>.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    let placeholderImage1 = UIImage.init(named: "compPhotoUploadDafualt1")!
    let placeholderImage2 = UIImage.init(named: "compPhotoUploadDafualt2")!
    
    private var isRejectPhotos = false
    
    init(isReject: Bool) {
        super.init()
        self.isRejectPhotos = isReject
        
        sections.accept([
            PhotoSection(header: "1 section", items: [
                PhotoModel(defaultPhoto: placeholderImage1, photoData: nil),
                PhotoModel(defaultPhoto: placeholderImage1, photoData: nil),
                PhotoModel(defaultPhoto: placeholderImage1, photoData: nil),
                PhotoModel(defaultPhoto: placeholderImage2, photoData: nil),
                PhotoModel(defaultPhoto: placeholderImage2, photoData: nil),
                PhotoModel(defaultPhoto: placeholderImage2, photoData: nil)
            ])
        ])
        
        refreshList()
        
        sectionData.map{ $0.first!.items.compactMap{ $0.photoData } }
            .map { $0.count >= 3 && $0.filter{ $0.photoStatus == Str.reject }.count == 0 }
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)
    }
    
    func nextAction() {
        if self.isRejectPhotos {
            Repository.shared.graphQl
                .completeMemberPhotoReject(userNo: MainInformation.shared.userNoAes!)
                .bind(to: nextSuccess)
                .disposed(by: bag)
        }else{
            Repository.shared.graphQl
                .completePhoto(userNo: MainInformation.shared.userNoAes!)
                .bind(to: nextSuccess)
                .disposed(by: bag)
        }
    }
    
    func refreshList() {
        Repository.shared.graphQl
            .getPhotoList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                var photoList = $0.enumerated().map{ idx, photoData in
                    return PhotoModel(isMain: idx == 0, defaultPhoto: ((idx < 3) ? self.placeholderImage1 : self.placeholderImage2), photoData: photoData)
                }
                // 반려 메세지
                let msg = photoList
                    .filter{ if $0.photoData?.photoStatus == Str.reject { return true }; return false }
                    .compactMap{ $0.photoData?.rejectMsg }
                    .removeDuplicates()
                    .map{ "\"\($0)\"" }
                    .joined(separator: ", ")
                self.rejectInfo.accept((display: msg.count > 0, msg: "\(msg)\n사진을 변경하여 다시 도전하세요!"))
                // 6장 미만 처리
                if photoList.count < 6 {
                    let leftCount = 6 - photoList.count
                    (1...leftCount).forEach { idx in
                        photoList.append(PhotoModel(isMain: false, defaultPhoto: (leftCount - idx) >= 3 ? self.placeholderImage1 : self.placeholderImage2, photoData: nil))
                    }
                }
                // 데이터 binding
                self.sections.accept([PhotoSection(header: "1 section", items: photoList)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func addPhoto(imageData: Data, uploadImage: UIImage) {
        guard var items = self.sections.value.first?.items else { return }
        let idx = items.firstIndex(where: { $0.photoData == nil}) ?? 0
        items[idx] = PhotoModel.init(isMain: idx == 0, defaultPhoto: uploadImage, photoData: nil)
        let newPhotoModel = items[idx]
        RLProgress.shared.showCustomLoading()
        Repository.shared.main
            .addPhoto(userNo: MainInformation.shared.userNoAes!, imageData: imageData)
            .subscribe(onNext: {
                RLProgress.shared.hideCustomLoading()
                self.setPhotoList(idx: idx, photoModel: newPhotoModel, uploadImage: uploadImage, photoList: $0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func modifyPhoto(idx: Int, photoNo: String, imageData: Data, uploadImage: UIImage) {
        guard var items = self.sections.value.first?.items else { return }
        items[idx] = PhotoModel.init(isMain: idx == 0, defaultPhoto: uploadImage, photoData: nil)
        let newPhotoModel = items[idx]
        self.sections.accept([PhotoSection(header: "", items: items)])
        
        RLProgress.shared.showCustomLoading()
        Repository.shared.main
            .modifyPhoto(userNo: MainInformation.shared.userNoAes!, photoNo: photoNo, imageData: imageData)
            .subscribe(onNext: { result in
                RLProgress.shared.hideCustomLoading()
                self.mainProfileCheckAndUpdate(idxs: idx)
                self.setPhotoList(idx: idx, photoModel: newPhotoModel, uploadImage: uploadImage, photoList: result)
            },onError: {
                RLProgress.shared.hideCustomLoading()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func setPhotoList(idx: Int, photoModel: PhotoModel, uploadImage: UIImage, photoList: [Photo001Result]) {
        let msg = photoList
            .filter{ $0.photoStatus == Str.reject }
            .compactMap{ $0.rejectMsg }
            .map{ "\"\($0)\"" }
            .joined(separator: ", ")
        self.rejectInfo.accept((display: msg.count > 0, msg: "\(msg)\n사진을 변경하여 다시 도전하세요!"))
        
        var newList = photoList.enumerated().map{ _idx, item -> PhotoModel in
            let photoData = GetPhotoListQuery.Data.MemberPhotoList.init(photoNo: item.photoNo, photoUrl: item.photoURL, photoSeq: item.photoSeq, rejectMsg: item.rejectMsg, detectYn: item.detectYn, photoStatus: item.photoStatus, photoStatusName: item.photoStatusName)
            if _idx == idx {
                return PhotoModel(isMain: _idx == 0, defaultPhoto: photoModel.defaultPhoto, photoData: photoData)
            }else{
                let isLast = _idx == photoList.count - 1
                let defaultPhoto = isLast ? uploadImage : (_idx < 3 ? self.placeholderImage1 : self.placeholderImage2)
                return PhotoModel(isMain: _idx == 0, defaultPhoto: defaultPhoto, photoData: photoData)
            }
        }
        
        if photoList.count < 6 {
            let leftCount = 6 - newList.count
            (1...leftCount).forEach { idx in
                newList.append(PhotoModel(isMain: false, defaultPhoto: (leftCount - idx) >= 3 ? self.placeholderImage1 : self.placeholderImage2, photoData: nil))
            }
        }
        self.sections.accept([PhotoSection(header: "", items: newList)])
    }
    
    func change(_ from: Int,_ to: Int) {
        let old = sections.value
        if var models = old.first?.items {
            if !MainInformation.shared.isJoinOrReject && (models.filter{ $0.photoData?.photoStatus == Str.reject || $0.photoData?.photoStatus == Str.ready }).count > 0 {
                toast(message: "사진 승인 완료 후\n순서 변경이 가능합니다", seconds: 1.5)
                
                self.sections.accept(old)
                return
            }
            
            let changed = models[from]
            if models[from].photoData == nil && models[to].photoData == nil {
                // 사진이 없는 구간끼리 변경하는 경우
                self.sections.accept(old)
                return
            }
            models.remove(at: from) // 기존거 제거
            models.insert(changed, at: to) // 변경한 위치로 넣어주기.
            
            let photoNoList = models.compactMap{ $0.photoData?.photoNo }
            RLProgress.shared.showCustomLoading()
            Repository.shared.main
                .updateOrderPhoto(userNo: MainInformation.shared.userNoAes!, photoNoList: photoNoList)
                .subscribe(onNext: {
                    guard $0 else { return }
                    RLProgress.shared.hideCustomLoading()
                    self.mainProfileCheckAndUpdate(idxs: from, to)
                    self.refreshList()
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.sections.accept(old)
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
        }
    }
    
    func remove(photoNo: String) {
        RLProgress.shared.showCustomLoading()
        Repository.shared.main
            .removePhoto(userNo: MainInformation.shared.userNoAes!, photoNo: photoNo)
            .subscribe(onNext: { _ in
                RLProgress.shared.hideCustomLoading()
                self.refreshList()
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                RLProgress.shared.hideCustomLoading()
            })
            .disposed(by: bag)
    }
    
    func canRemove() -> Bool {
        let new = sections.value[0].items.filter { $0.photoData?.photoStatus == Str.normal }
        return new.count > 3
    }
    
    func mainProfileCheckAndUpdate(idxs: Int...) {
        guard let userNo = MainInformation.shared.userNoAes else { return }
        guard idxs.contains(0) else { return }
        Repository.shared.graphQl.getMyPhotoUrl(userNo: userNo)
            .subscribe(onNext: {
                if let photoUrl = $0 {
                    RLNotificationCenter.shared.post(notiType: .profileChanged, object: photoUrl)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
}
