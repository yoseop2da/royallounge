//
//  JobCareerViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class JobCareerViewController: ClearNaviLoggedBaseViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textWrapView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeHolder: UILabel!
    
    @IBOutlet weak var limitTextLabel: UILabel!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    @IBOutlet weak var textWrapViewBottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    
    let nextButton = RLNextButton.init(title: "완료")
    
    var isEditMode: Bool = false
    
    var viewModel: JobCareerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "추가 프로필"
//        addTapToCloseKeyboard()
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        nextButton.title = isEditMode ? "저장" : "완료"
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }

        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        if let count = self.navigationController?.viewControllers.count {
            if count > 1 {
                let lBarButton = RLBarButtonItem(barType: .back)
                lBarButton.rx.tap
                    .subscribe(onNext: { _ in
                        if !MainInformation.shared.isJoinOrReject && self.viewModel.isEdited(current: self.textView.text) {
                            RLPopup.shared.showNormal(title: "정말 나가시겠습니까?", description: "수정중인 Job Career가 있을 경우 저장되지 않습니다.", leftButtonTitle: "취소", rightButtonTitle: "나가기", leftAction: {}, rightAction: {
                                self.pushBack()
                            })
                            return
                        }
                        self.pushBack(animated: true)
                    })
                    .disposed(by: bag)
                self.navigationItem.leftBarButtonItem = lBarButton
            }
        }
        if !isEditMode {
            let rBarButton = RLBarButtonItem(barType: .close)
            rBarButton.rx.tap
                .subscribe(onNext: { _ in
                    RLPopup.shared.showNormal(description: "프로필 작성을 취소하시겠어요?", leftButtonTitle: "취소", rightButtonTitle: "확인", rightAction: {
                        self.dismiss(animated: true, completion: nil)
                    })
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = rBarButton
        }

        textWrapView.layer.cornerRadius = 6.0
        textWrapView.setBorder(color: .gray10)
        
        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: 4, bottom: 0, right: 4)
        textView.rx.text.orEmpty
            .subscribe(onNext: { text in
                let count = text.count
                self.placeHolder.isHidden = count > 0
                if count >= 1000 {
                    self.limitTextLabel.text = "최대 1000자"
                    self.limitTextLabel.textColor = .primary100
                    self.textView.text = (self.textView.text as NSString).substring(with: NSMakeRange(0, 1000))
                }else{
                    self.limitTextLabel.text = "최소 10자"
                    self.limitTextLabel.textColor = UIColor.gray75
                }
                if text.containsEmoji() {
                    self.textView.text = (text.removingEmoji() as String)
                }
            })
            .disposed(by: bag)
        
        
        viewModel = JobCareerViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            jobCareer: textView.rx.text.orEmpty.asObservable(),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.myValue
            .bind(to: textView.rx.text)
            .disposed(by: bag)
        
        viewModel.validated
            .subscribe(onNext: { result in
                print("result : \(result)")
                switch result {
                case .empty, .failed:
                    self.limitTextLabel.textColor = .primary100
                    self.textView.textColor = .rlBlack21
                case .failedProhibit:
                    self.limitTextLabel.textColor = .primary100
                    self.textView.textColor = result.textViewTextColor
                default:
                    self.limitTextLabel.textColor = .gray75
                    self.textView.textColor = .rlBlack21
                }
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.mainButtonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { _ in
                print("=======잡커리어 등록 완료")
                if self.isEditMode {
                    self.pushBack()
                }else{
                    toast(message: "크라운 \(self.viewModel.joinAddProfile)개가 적립되었습니다", seconds: 1.5, actionAfter: nil)
                    self.dismiss(animated: true, completion: nil)
                }
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                let keyboardHide = keyboard.height == 0
                print("---------keyboardHide: \(keyboardHide)")
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboardHide ? 30 : keyboard.height
                    self.textWrapViewBottomPadding.constant = keyboardHide ? 51 : 10 // 키보드 올라오면 하단 여백 줄여주기!
                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboardHide ? keyboard.height : self.isEditMode ? 48 - 18 : 48)
                    
                    if keyboardHide {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }
                    
                    self.view.layoutIfNeeded()
                }
                
                mainAsync {
                    self.headerView.frame = self.mainTableView.frame
                    self.mainTableView.reloadData()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "추가 프로필"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    @IBAction func keyboardDownButton(_ sender: Any) {
        view.endEditing(true)
    }
}
