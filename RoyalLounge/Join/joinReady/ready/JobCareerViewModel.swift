//
//  JobCareerViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class JobCareerViewModel: BaseViewModel {
    let myValue = PublishSubject<String>.init()
    
    let validated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    var joinAddProfile = 0

    var dbValue: String?
    
    init(userNo: String,
         jobCareer: Observable<String>,
         nextButtonTap: Observable<Void>) {
        
        super.init()
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .subscribe(onNext: { crown in
                self.joinAddProfile = crown?.joinAddProfile ?? 0
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getJobCareer(userNo: userNo)
            .subscribe(onNext: {
                self.dbValue = $0
                self.myValue.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        // 유저필드 변화 감지
        jobCareer
            .distinctUntilChanged()
            .map { Validation.shared.validate(jobCareer: $0) }
            .bind(to: validated)
            .disposed(by: bag)
        
        validated
            .map { $0.isValid }
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)
        
        nextButtonTap.withLatestFrom(jobCareer)
            .flatMapLatest { Repository.shared.graphQl .updateJobCareer(userNo: userNo, jobCareer: $0) }
            .subscribe(onNext: { success in
                self.nextSuccess.on(.next(success))
            })
            .disposed(by: bag)
    }
    
    func isEdited(current: String) -> Bool {
        if let value = dbValue {
            return value != current
        }
        return false
    }
}

