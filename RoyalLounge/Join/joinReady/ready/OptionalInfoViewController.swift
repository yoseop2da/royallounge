//
//  OptionalInfoViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/11.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class OptionalInfoViewController: ClearNaviLoggedBaseViewController {
    enum OptionalInfoType {
        case hobby
        case interest
        case have
        case want
        case jobCareer
        
        var nextType: OptionalInfoType? {
            switch self {
            case .hobby: return .interest
            case .interest: return .have
            case .have: return .want
            default: return nil
            }
        }
    }
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var progressbarView: UIView!
    @IBOutlet weak var progress1Image: UIImageView!
    @IBOutlet weak var progress2Image: UIImageView!
    @IBOutlet weak var progress3Image: UIImageView!
    @IBOutlet weak var progress4Image: UIImageView!
    @IBOutlet weak var progress5Image: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textFieldAllWarpView: UIView!
    @IBOutlet weak var textField1WrapView: UIView!
    @IBOutlet weak var textField2WrapView: UIView!
    @IBOutlet weak var textField3WrapView: UIView!
    @IBOutlet weak var textField4WrapView: UIView!
    @IBOutlet weak var textField5WrapView: UIView!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    @IBOutlet weak var progressViewHeight: NSLayoutConstraint!
    
    let nextButton = RLNextButton.init(title: "다음")
    
    private let progressValueView: UIView = {
        let bar = UIView()
        bar.backgroundColor = UIColor.gray10
        return bar
    }()
    
    let defaultTextField1: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "예시 1", type: .small, maxLength: 10, containEmoji: false)
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        return field
    }()
    let defaultTextField2: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "예시 2", type: .small, maxLength: 10, containEmoji: false)
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        return field
    }()
    let defaultTextField3: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "예시 3", maxLength: 10, containEmoji: false)
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        return field
    }()
    let defaultTextField4: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "", maxLength: 10, containEmoji: false)
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        return field
    }()
    let defaultTextField5: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "", maxLength: 10, containEmoji: false)
        field.keyboardType = .emailAddress
        field.returnKeyType = .done
        return field
    }()
    
    var optionalInfoType: OptionalInfoType = .hobby
    var viewModel: OptionalInfoViewModel!
    var isEditMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "추가 프로필"
        self.progressViewHeight.constant = self.isEditMode ? 0.0 : 18.0
        nextButton.title = isEditMode ? "저장" : "다음"
        
//        addTapToCloseKeyboard()
        
        textFieldAllWarpView.layer.cornerRadius = 6.0
        textFieldAllWarpView.setBorder(color: .gray10)
        
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        //
        textField1WrapView.addSubview(defaultTextField1)
        defaultTextField1.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        textField2WrapView.addSubview(defaultTextField2)
        defaultTextField2.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        textField3WrapView.addSubview(defaultTextField3)
        defaultTextField3.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        textField4WrapView.addSubview(defaultTextField4)
        defaultTextField4.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        textField5WrapView.addSubview(defaultTextField5)
        defaultTextField5.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        //        mainAsync {
        //            self.headerView.frame = self.mainTableView.frame
        //            self.mainTableView.reloadData()
        //        }
        
        defaultTextField1.returnKeyCallback = { self.defaultTextField2.becomeFirst = true }
        defaultTextField2.returnKeyCallback = { self.defaultTextField3.becomeFirst = true }
        defaultTextField3.returnKeyCallback = { self.defaultTextField4.becomeFirst = true }
        defaultTextField4.returnKeyCallback = { self.defaultTextField5.becomeFirst = true }
        defaultTextField5.returnKeyCallback = { }
        
        switch optionalInfoType {
        case .hobby:
            titleLabel.text = "취미는 뭐예요?"
            defaultTextField1.placeholder = "ex) 집에서 영화보기"
            defaultTextField2.placeholder = "ex) 집 앞 둘레길 걷기"
            defaultTextField3.placeholder = "ex) 전국 맛집 투어"
        case .interest:
            titleLabel.text = "관심사는 뭐예요?"
            defaultTextField1.placeholder = "ex) 반려동물"
            defaultTextField2.placeholder = "ex) 패션"
            defaultTextField3.placeholder = "ex) 스포츠"
        case .have:
            titleLabel.text = "I have 키워드"
            defaultTextField1.placeholder = "ex) 개인 자동차"
            defaultTextField2.placeholder = "ex) 탄탄한 몸매"
            defaultTextField3.placeholder = "ex) 센스있는 유머"
        case .want:
            titleLabel.text = "I want 키워드"
            defaultTextField1.placeholder = "ex) 대화가 잘 통하는 이성"
            defaultTextField2.placeholder = "ex) 함께 영화 볼 사람"
            defaultTextField3.placeholder = "ex) 꿀 같은 휴가"
        case .jobCareer:
            titleLabel.text = "My Job / Career"
        }
        
        if let count = self.navigationController?.viewControllers.count {
            if count > 1 {
                let lBarButton = RLBarButtonItem(barType: .back)
                lBarButton.rx.tap
                    .subscribe(onNext: { _ in
                        self.pushBack(animated: true)
                    })
                    .disposed(by: bag)
                self.navigationItem.leftBarButtonItem = lBarButton
            }
        }
        if !isEditMode {
            let rBarButton = RLBarButtonItem(barType: .close)
            rBarButton.rx.tap
                .subscribe(onNext: { _ in
                    RLPopup.shared.showNormal(description: "프로필 작성을 취소하시겠어요?", leftButtonTitle: "취소", rightButtonTitle: "확인", rightAction: {
                        self.dismiss(animated: true, completion: nil)
                    })
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = rBarButton
        }
        
        viewModel = OptionalInfoViewModel.init(
            type: optionalInfoType,
            field1: defaultTextField1.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            field2: defaultTextField2.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            field3: defaultTextField3.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            field4: defaultTextField4.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            field5: defaultTextField5.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.myValue
            .subscribe(onNext: { (values) in
                //
                let arr = [self.defaultTextField1, self.defaultTextField2, self.defaultTextField3, self.defaultTextField4, self.defaultTextField5]
                values.enumerated().forEach { idx, value in
                    arr[idx].text = value
                }
            })
            .disposed(by: bag)
        
        viewModel.validatedField1
            .bind(to: defaultTextField1.rx.setOptionalTextResult)
            .disposed(by: bag)
        
        viewModel.validatedField2
            .bind(to: defaultTextField2.rx.setOptionalTextResult)
            .disposed(by: bag)
        
        viewModel.validatedField3
            .bind(to: defaultTextField3.rx.setOptionalTextResult)
            .disposed(by: bag)
        
        viewModel.validatedField4
            .bind(to: defaultTextField4.rx.setOptionalTextResult)
            .disposed(by: bag)
        
        viewModel.validatedField5
            .bind(to: defaultTextField5.rx.setOptionalTextResult)
            .disposed(by: bag)
        
        
        
        viewModel.nextEnabled
            .bind(to: nextButton.rx.mainButtonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { nextType in
                if self.isEditMode {
                    self.pushBack()
                    return
                }
                //
                if let type = nextType {
                    let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: type, isEditMode: false)
                    self.push(vc)
                }else{
                    // jobCareer로 이동
                    print("=======jobCareer 스텝으로 이동")
                    let vc = RLStoryboard.join.jobCareerViewController(isEditMode: false)
                    self.push(vc)
                    
                }
            })
            .disposed(by: bag)
        
        let rightBarButton = RLBarButtonItem.init(barType: .title(titleString: "다음"))
        rightBarButton.rx.tap
            .subscribe(onNext: { _ in
                self.view.endEditing(true)
            })
            .disposed(by: self.bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                print("---------height: \(keyboard.height)")
                
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height == 0 ? 30 : keyboard.height
//                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 20)
                    
                    if keyboard.height == 0 {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }
                    
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "추가 프로필"
        switch optionalInfoType {
        case .hobby:
            setProgressView(step: 1)
        case .interest:
            setProgressView(step: 2)
        case .have:
            setProgressView(step: 3)
        case .want:
            setProgressView(step: 4)
        case .jobCareer:
            ()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    //1~5
    func setProgressView(step: Int) {
        // 노출 색상
        progressbarView.backgroundColor = .primary100
        progressbarView.addSubview(progressValueView)
        
        progressValueView.snp.makeConstraints {
            $0.trailing.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset((UIScreen.width - 40) * (CGFloat(step-1)/4.0))
        }

        let activeCheck = UIImage.init(named: "comSelectActiveCheck")
        let active = UIImage.init(named: "comSelectActive")
        
        switch step {
        case 1:
            progress1Image.image = active
        case 2:
            progress1Image.image = activeCheck
            progress2Image.image = active
        case 3:
            progress1Image.image = activeCheck
            progress2Image.image = activeCheck
            progress3Image.image = active
        case 4:
            progress1Image.image = activeCheck
            progress2Image.image = activeCheck
            progress3Image.image = activeCheck
            progress4Image.image = active
        case 5:
            progress1Image.image = activeCheck
            progress2Image.image = activeCheck
            progress3Image.image = activeCheck
            progress4Image.image = activeCheck
            progress5Image.image = active
        default:()
        }
    }
    
    @IBAction func keyboardDownButton(_ sender: Any) {
        view.endEditing(true)
    }
}

