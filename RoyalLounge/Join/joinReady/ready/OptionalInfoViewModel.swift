//
//  OptionalInfoViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionalInfoViewModel: BaseViewModel {
    var myValue: Observable<[String]>!
    private let _field1 = BehaviorRelay<String>.init(value: "")
    private let _field2 = BehaviorRelay<String>.init(value: "")
    private let _field3 = BehaviorRelay<String>.init(value: "")
    private let _field4 = BehaviorRelay<String>.init(value: "")
    private let _field5 = BehaviorRelay<String>.init(value: "")
    
    let validatedField1: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let validatedField2: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let validatedField3: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let validatedField4: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let validatedField5: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    
    let nextEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishRelay<OptionalInfoViewController.OptionalInfoType?>()
    
    init(type: OptionalInfoViewController.OptionalInfoType,
         field1: Observable<String>,
         field2: Observable<String>,
         field3: Observable<String>,
         field4: Observable<String>,
         field5: Observable<String>,
         nextButtonTap: Observable<Void>) {
        
        super.init()
        Validation.shared.updateProhibitWord()
        
        field1.bind(to: _field1).disposed(by: bag)
        field2.bind(to: _field2).disposed(by: bag)
        field3.bind(to: _field3).disposed(by: bag)
        field4.bind(to: _field4).disposed(by: bag)
        field5.bind(to: _field5).disposed(by: bag)
        
        switch type {
        case .hobby:
            myValue = Repository.shared.graphQl
                .getHobby(userNo: MainInformation.shared.userNoAes!)
        case .interest:
            myValue = Repository.shared.graphQl
                .getInterest(userNo: MainInformation.shared.userNoAes!)
        case .have:
            myValue = Repository.shared.graphQl
                .getHave(userNo: MainInformation.shared.userNoAes!)
        case .want:
            myValue = Repository.shared.graphQl
                .getWant(userNo: MainInformation.shared.userNoAes!)
        default:
            myValue = Repository.shared.graphQl
                .getHobby(userNo: MainInformation.shared.userNoAes!)
        }
        
        // 버튼 활성화
        myValue
            .subscribe(onNext: { list in
                let validFields = [self.validatedField1, self.validatedField2, self.validatedField3, self.validatedField4, self.validatedField5]
                let fields = [self._field1, self._field2, self._field3, self._field4, self._field5]
                list.enumerated().forEach { idx, str in
                    let validField = validFields[idx]
                    validField.accept(Validation.shared.validate(optional: str))
                    let field = fields[idx]
                    field.accept(str)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        // 유저필드 변화 감지
        field1.map{
            let result = Validation.shared.prohibitCheck(word: $0)
            if case .ok = result { return Validation.shared.validate(optional: $0) }
            return result }
            .bind(to: validatedField1)
            .disposed(by: bag)
        
        field2.map{
            let result = Validation.shared.prohibitCheck(word: $0)
            if case .ok = result { return Validation.shared.validate(optional: $0) }
            return result }
            .bind(to: validatedField2)
            .disposed(by: bag)
        
        field3.map{
            let result = Validation.shared.prohibitCheck(word: $0)
            if case .ok = result { return Validation.shared.validate(optional: $0) }
            return result }
            .bind(to: validatedField3)
            .disposed(by: bag)
        
        field4.map{
            let result = Validation.shared.prohibitCheck(word: $0)
            if case .ok = result { return Validation.shared.validate(optional: $0) }
            return result }
            .bind(to: validatedField4)
            .disposed(by: bag)
        
        field5.map{
            let result = Validation.shared.prohibitCheck(word: $0)
            if case .ok = result { return Validation.shared.validate(optional: $0) }
            return result }
            .bind(to: validatedField5)
            .disposed(by: bag)
        
        Observable
            .combineLatest(validatedField1, validatedField2, validatedField3, validatedField4, validatedField5) {
                let fields = [$0, $1, $2, $3, $4]
                if (fields.filter{ $0.isFail }).count > 0 { return false }
                let valid = (fields.filter{ $0.isValid }).count >= 3
                return valid }
            .distinctUntilChanged()
            .bind(to: nextEnabled)
            .disposed(by: bag)
        
        let typeAndValues = Observable.combineLatest(_field1, _field2, _field3, _field4, _field5) {
            return [$0, $1, $2, $3, $4]
        }
        
        nextButtonTap
            .withLatestFrom(typeAndValues)
            .flatMapLatest { values -> Observable<OptionalInfoViewController.OptionalInfoType?> in
                let filteredValues = values.compactMap{$0}.filter{$0.length > 0}
                let userNo = MainInformation.shared.userNoAes!
                switch type {
                case .hobby:
                    return Repository.shared.graphQl
                        .updateHobbyList(userNo: userNo, list: filteredValues)
                        .filter{ $0 == true }
                        .map{ _ in type.nextType }
                case .interest:
                    return Repository.shared.graphQl
                        .updateInterestList(userNo: userNo, list: filteredValues)
                        .filter{ $0 == true }
                        .map{ _ in type.nextType }
                case .have:
                    return Repository.shared.graphQl
                        .updateHaveList(userNo: userNo, list: filteredValues)
                        .filter{ $0 == true }
                        .map{ _ in type.nextType }
                case .want:
                    return Repository.shared.graphQl
                        .updateWantList(userNo: userNo, list: filteredValues)
                        .filter{ $0 == true }
                        .map{ _ in type.nextType }
                default:
                    return Repository.shared.graphQl
                        .updateHobbyList(userNo: userNo, list: filteredValues)
                        .filter{ $0 == true }
                        .map{ _ in type.nextType }
                }}
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
}

