//
//  ReadyViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class ReadyViewController: LoggedBaseViewController {

    @IBOutlet weak var topInformationLabel: UILabel!
    @IBOutlet weak var gradientView: UIImageView!
    @IBOutlet weak var animationGradientTop: NSLayoutConstraint!
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var addProfileWrapView: UIView!
    @IBOutlet weak var addProfileButton: UIButton!
    @IBOutlet weak var addProfileCrownWrapView: UIView!
    @IBOutlet weak var addProfileCrownLabel: UILabel!
    
    @IBOutlet weak var addUnivCoWrapView: UIView!
    @IBOutlet weak var addUnivCoButton: UIButton!
    @IBOutlet weak var addUnivCoCrownWrapView: UIView!
    @IBOutlet weak var addUnivCoCrownLabel: UILabel!
    
    @IBOutlet weak var avoidButton: UIButton!
    
    @IBOutlet weak var custommerCenterButton: UIButton!
    
    var viewModel = ReadyViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addProfileWrapView.layer.cornerRadius = 6.0
        addUnivCoWrapView.layer.cornerRadius = 6.0
        avoidButton.layer.cornerRadius = 6.0
        
        addProfileCrownWrapView.layer.cornerRadius = 15.0
        addUnivCoCrownWrapView.layer.cornerRadius = 15.0
        
        if UIScreen.underSE {
            self.topInformationLabel.font = UIFont.spoqaHanSansBold(ofSize: 14)
            self.addProfileButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
            self.addProfileButton.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
            self.addUnivCoButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
            self.addUnivCoButton.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
            self.avoidButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
        }
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }

        addProfileButton.rx.tap
            .rxTouchAnimation(button: addProfileWrapView, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                
                if self.viewModel.isRegProfile { toast(message: "등록이 완료되어 승인 요청중입니다", seconds: 1.5); return }
                let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .hobby, isEditMode: false)

                let navi = UINavigationController.init(rootViewController: vc)
                self.modal(navi, animated: true)
            })
            .disposed(by: bag)
        
        addUnivCoButton.rx.tap
            .rxTouchAnimation(button: addUnivCoWrapView, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.viewModel.isRegUnivCo { toast(message: "등록이 완료되어 승인 요청중입니다", seconds: 1.5); return }
                let vc = RLStoryboard.join.univCoViewController(isEditMode: false)
                self.push(vc)
            })
            .disposed(by: bag)
        
        avoidButton.rx.tap
            .rxTouchAnimation(button: avoidButton, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                //            // 지인피하기
                let vc = RLStoryboard.join.avoidViewController()
                self.push(vc)
            })
            .disposed(by: bag)
        
        delay(0.5) {
            self.animate()
        }
        
        viewModel.crown
            .subscribe(onNext: { crowns in
                self.addProfileCrownLabel.text = "x\(crowns.profile) 적립"
                self.addProfileCrownWrapView.isHidden = crowns.profile == 0
                
                self.addUnivCoCrownLabel.text = "x\(crowns.univCo) 적립"
                self.addUnivCoCrownWrapView.isHidden = crowns.univCo == 0
            })
            .disposed(by: bag)
        
//        custommerCenterButton.underLine()
        custommerCenterButton.rx.tap
            .rxTouchAnimation(button: custommerCenterButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let myNickname = self.viewModel.nickname
                let userId = self.viewModel.userId
                let mbNo = self.viewModel.mbNo
                RLMailSender.shared.sendEmail(.문의_가입유저(nickname: myNickname, userId: userId, mbNo: mbNo), finishCallback: { _ in
                    self.dismiss(animated: true, completion: nil)
                })
            }, onError: { err in
                
            })
            .disposed(by: bag)
    }

    var isFirst = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        hideNavigationControllerBottomLine()
        
        if isFirst { isFirst = false; return }
        delay(0.1){ self.viewModel.refreshCrownCount() }
    }
    
    func animate() {
        let downPadding: CGFloat = 109.0
        let downTime = 1.4
        let upPadding: CGFloat = 15.0
        let upTime = 1.7
        
        UIView.animate(withDuration: downTime, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.3, options: [.curveEaseIn], animations: {
            self.animationGradientTop.constant = downPadding
            self.view.layoutIfNeeded()
        }, completion: nil)

        UIView.animate(withDuration: upTime, delay: downTime + 0.3, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.3, options: [.curveEaseIn], animations: {
            self.animationGradientTop.constant = upPadding
            self.view.layoutIfNeeded()
        }, completion: { _ in
            delay(0.4) {
                self.gradientView.alpha = 1.0
                self.animate()
            }
        })
    }
}
