
//
//  ReadyViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ReadyViewModel: BaseViewModel {
    
    var isRegProfile = false
    var isRegUnivCo = false
    var crown = PublishSubject<(profile: Int, univCo: Int)>.init()
    var nickname: String = ""
    var userId: String = ""
    var mbNo: String = "입력해주세요."
    
    override init() {
        super.init()
        refreshCrownCount()
        
        if let userNoAes = MainInformation.shared.userNoAes {
            Repository.shared.graphQl
                .getEmailInfo(userNo: userNoAes)
                .subscribe(onNext: {
                    guard let result = $0 else { return }
                    self.nickname = result.nickname
                    self.userId = result.userId.aesDecrypted!
                    self.mbNo = result.mbNo.aesDecrypted!
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }
    }
    
    func refreshCrownCount() {
        Repository.shared.graphQl
            .getReadyCrown(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                if let _crown = $0 {
                    self.isRegProfile = _crown.joinAddProfileCrown == 0
                    self.isRegUnivCo = _crown.joinUnivCoCrown == 0
                    self.crown.on(.next((profile: _crown.joinAddProfileCrown, univCo: _crown.joinUnivCoCrown)))
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)

    }
}
