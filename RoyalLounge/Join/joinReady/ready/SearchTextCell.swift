//
//  SearchTextCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/11.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SearchTextCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setItem(item: UnivCoSearchModel) {
        if item.isEmpty {
            let fullStr = "\(item.searchText) 추가하기"
            let attrStr = NSMutableAttributedString.init(string: fullStr)
            let detectedRange = (fullStr.uppercased() as NSString).range(of: item.searchText.uppercased())
            attrStr.addAttributes([.foregroundColor: UIColor.successColor], range: detectedRange)
            titleLabel.attributedText = attrStr
            return
        }else{
            titleLabel.text = ""
        }
        
        if item.name.uppercased().contains(item.searchText.uppercased()) {
            let fullStr = item.name
            let attrStr = NSMutableAttributedString.init(string: fullStr)
            let detectedRange = (fullStr.uppercased() as NSString).range(of: item.searchText.uppercased())
            attrStr.addAttributes([.foregroundColor: UIColor.successColor], range: detectedRange)
            titleLabel.attributedText = attrStr
        }else{
            titleLabel.text = item.name
        }
    }
}
