//
//  UnivCoData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit
import RxDataSources

// Tableview Cell
struct UnivCoDetailSection {
    var items: [SpecDetailCellModel]
}

extension UnivCoDetailSection: SectionModelType {
    init(original: UnivCoDetailSection, items: [SpecDetailCellModel]) {
        self = original
        self.items = items
    }
}

struct UniversityCert {
    var name, univNo: String
    var univStatus, univRejectMsg: String?
    var univRejectYn: Bool?
}

struct CompanyCert {
    var name, coNo: String
    var companyStatus, companyRejectMsg: String?
    var companyRejectYn: Bool?
    
}

struct UnivCoModel {
    enum UnivCoType {
        case univ
        case co
    }
    
    var univCoType: UnivCoType = .univ
    var univ: UniversityCert?
    var co: CompanyCert?
    var univDataList: [SpecPhotoData] = []
    var coDataList: [SpecPhotoData] = []
    
    var serverDataBaseValue = (univName: "", coName: "")
    
    var isCurrentTypeReady: Bool {
        return (univCoType == .univ) ? univ?.univStatus == Str.ready : co?.companyStatus == Str.ready
    }
    
    var currentDataList: [SpecPhotoData] {
        return (univCoType == .univ) ? univDataList : coDataList
    }
    
    // reject인경우 reject항목 제거여부 판단하기!!
    var buttonStateEnable: Bool {
        if univCoType == .univ {
            if univ?.univStatus == Str.ready { return false }
            if univ?.name == serverDataBaseValue.univName { return false }
            return univ != nil && univDataList.count > 0
        }else{
            if co?.companyStatus == Str.ready { return false }
            if co?.name == serverDataBaseValue.coName { return false }
            return co != nil && coDataList.count > 0
        }
    }
    
    var imageDataList: [Data] {
        return (univCoType == .univ ? univDataList : coDataList).compactMap{ tuple -> Data in
            if let data = tuple.imageData { return data }
            if let url = tuple.photoUrl { return try! Data(contentsOf: URL.init(string: url)!) }
            return Data()
        }
    }
}
