//
//  UnivCoViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SnapKit

class UnivCoViewController: ClearNaviLoggedBaseViewController {

    @IBOutlet weak var tapWrapView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var exampleWrapView: UIView!
    @IBOutlet weak var profileExampleLabel: UIView!// 프로필(예)
    @IBOutlet weak var step01WrapView: UIView!
    @IBOutlet weak var searchWrapView: UIView!

    @IBOutlet weak var step01ImageView: UIImageView!
    @IBOutlet weak var step01Label: UITextField!
    @IBOutlet weak var step01SearchButton: UIButton!
    
    @IBOutlet weak var step02WrapView: UIView!
    @IBOutlet weak var step02Select1Label: UILabel!
    @IBOutlet weak var step02Select2Label: UILabel!
    @IBOutlet weak var step02Select3Label: UILabel!
    
    @IBOutlet weak var univSelectionLine: UIView!
    @IBOutlet weak var univButton: TabMenuButton!
    
    @IBOutlet weak var coSelectionLine: UIView!
    @IBOutlet weak var coButton: TabMenuButton!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var bottomInformationLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    
    var isEditMode = false
    var univCoType: UnivCoModel.UnivCoType = .univ
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<UnivCoDetailSection>
    
    let nextButton = RLNextButton.init(title: "승인 요청")
    var refreshCallback: (()->Void)?
    
    private var rejectView: RejectView?
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item in
            let cell: AddPhotoCell = tableView.dequeueReusable(for: indexPath)
            if case let .addPhoto(placeholders, addImages) = item {
                let isRegistered = self.viewModel.univCoData.value.isCurrentTypeReady
                cell.setImage(placeholders: placeholders, addImages: addImages, isRegistered: isRegistered)
                cell.modifyPhoto = { idx, image, data in
                    self.viewModel.modifyPhoto(idx: idx, image: image, imageData: data)
                }
                cell.addPhoto = { image, data in
                    self.viewModel.addPhoto(image: image, imageData: data)
                }
                cell.remove = { idx in
                    self.viewModel.removePhoto(idx: idx)
                }
            }
            return cell
        })
        return dataSource
    }
    
    var viewModel: UnivCoViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        univSelectionLine.layer.cornerRadius = 6.0
        coSelectionLine.layer.cornerRadius = 6.0
        exampleWrapView.layer.cornerRadius = 6.0
        profileExampleLabel.layer.cornerRadius = 4.0
        step01WrapView.layer.cornerRadius = 4.0
        searchWrapView.layer.cornerRadius = 6.0
        searchWrapView.setBorder(width: 1.0, color: .gray10)
        step02WrapView.layer.cornerRadius = 4.0
        
        tabSelectedUI(type: univCoType)
        
        if UIScreen.underSE {
            bottomInformationLabel.font = UIFont.spoqaHanSansRegular(ofSize: 10)
        }
        if isEditMode {
            let barButton = RLBarButtonItem(barType: .close)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.dismiss(animated: true, completion: nil)
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = barButton
        }else{
            let barButton = RLBarButtonItem(barType: .back)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.pushBack()
                })
                .disposed(by: bag)
            self.navigationItem.leftBarButtonItem = barButton
        }
        
        step01SearchButton
            .rx.tap
            .rxTouchAnimation(button: step01SearchButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.viewModel.univCoData.value.isCurrentTypeReady { return }
                let vc = RLStoryboard.join.univSearchViewController(type: self.viewModel.univCoData.value.univCoType)
                vc.refreshCallBack = { tuple in
                    if let (name, no) = tuple {
                        self.viewModel.setStep01(name: name, no: no)
                    }
                }
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: false, completion: nil)
            })
            .disposed(by: bag)
        
        univButton.rx.tap
            .rxTouchAnimation(button: univButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                delay(0.0) { self.mainTableView.setContentOffset(.zero, animated: true) }
                if self.viewModel.univCoData.value.univCoType == .univ { return }
                self.tabSelectedUI(type: .univ)
            })
            .disposed(by: bag)
        
        coButton.rx.tap
            .rxTouchAnimation(button: coButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                delay(0.0) { self.mainTableView.setContentOffset(.zero, animated: true) }
                if self.viewModel.univCoData.value.univCoType == .co { return }
                self.tabSelectedUI(type: .co)
            })
            .disposed(by: bag)
        
        viewModel = UnivCoViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            univCoType: univCoType,
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
            })
            .disposed(by: bag)
        
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        nextButton.setTitle("승인 요청", for: .normal)
        
        rejectView = RejectView(rejectMsg: "가나다라마바사\n아자차카타파하!!", isUpArrow: true)
        footerView.addSubview(rejectView!)
        rejectView!.snp.updateConstraints{
            $0.top.equalToSuperview().offset(-10)
            $0.leading.trailing.width.equalToSuperview()
        }
        
        viewModel.rejectInfo
            .subscribe(onNext: { info in
                self.rejectView?.rejectMsg = info.msg
                self.rejectView?.isHidden(!info.display, animated: false)
            })
            .disposed(by: bag)
        
        viewModel.univCoData
            .subscribe(onNext: { univCo in
                if univCo.univCoType == .univ {
                    self.step01Label.text = univCo.univ?.name
                    self.setNameFieldUI(by: univCo.univ?.univStatus, nameReject: univCo.univ?.univRejectYn)
                }else{
                    self.step01Label.text = univCo.co?.name
                    self.setNameFieldUI(by: univCo.co?.companyStatus, nameReject: univCo.co?.companyRejectYn)
                }
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "학교/직장 인증 추가"
        hideNavigationControllerBottomLine()
    }
    
    func tabSelectedUI(type: UnivCoModel.UnivCoType) {
        
        viewModel?.tapChange(type: type)
        
        if type == .univ {
            self.univButton.selected()
            self.coButton.normal()
            self.univSelectionLine.isHidden = false
            self.coSelectionLine.isHidden = true
            
            titleLabel.text = "프로필에 학교명과 함께\n‘인증’뱃지가 부여됩니다."
            
            step01Label.placeholder = "학교명을 입력하세요"
            
            step02Select1Label.text = "재학증명서 또는 졸업증명서"
            step02Select2Label.text = "학생증"
            step02Select3Label.text = "본인 성명과 소속이 표시된 포털 접속 화면"
            bottomInformationLabel.text = "인증 자료는 외부에 절대 공개되지 않으며, 학교 인증을 위해 사용 후\n탈퇴 시 파기됩니다."
        }else{
            self.univButton.normal()
            self.coButton.selected()
            self.univSelectionLine.isHidden = true
            self.coSelectionLine.isHidden = false
            
            titleLabel.text = "프로필에 직장명과 함께\n‘인증’뱃지가 부여됩니다."
            
            step01Label.placeholder = "직장명을 입력하세요"
            
            step02Select1Label.text = "명함 또는 사원증"
            step02Select2Label.text = "재직증명서"
            step02Select3Label.text = "그 외 재직을 증명할 수 있는 서류"
            bottomInformationLabel.text = "인증 자료는 외부에 절대 공개되지 않으며, 직장 인증을 위해 사용 후\n탈퇴 시 파기됩니다."
        }
    }
    
    let disableImage = UIImage.init(named: "icons24PxSearchDisable")
    let enableImage = UIImage.init(named: "icons24PxSearch")
    
    func setNameFieldUI(by status: String?, nameReject: Bool?) {
        guard let _status = status else {
            // 최초 초기값
            self.nextButton.setTitle("심사 요청", for: .normal)
            self.step01Label.textColor = .gray10
            self.step01ImageView.image = disableImage
            return
        }
        if _status == Str.ready {
            self.nextButton.setTitle("심사중", for: .normal)
            self.step01Label.textColor = .gray10
            self.step01ImageView.image = disableImage
            self.searchWrapView.setBorder(width: 1.0, color: .gray10)
        }else if _status == Str.normal {
            self.nextButton.setTitle("심사 완료", for: .normal)
            self.step01Label.textColor = .dark
            self.step01ImageView.image = enableImage
            self.searchWrapView.setBorder(width: 1.0, color: .gray10)
        }else if _status == Str.reject {
            self.nextButton.setTitle("심사 완료", for: .normal)
            self.step01Label.textColor = .dark
            self.step01ImageView.image = enableImage
            if let _nameReject = nameReject, _nameReject == true {
                self.searchWrapView.setBorder(width: 3.0, color: .errerColor)
            }else{
                self.searchWrapView.setBorder(width: 1.0, color: .gray10)
            }
        }else{
            // 수정중인 상태 (_status == Str.editing)
            self.nextButton.setTitle("심사 요청", for: .normal)
            self.step01Label.textColor = .dark
            self.step01ImageView.image = enableImage
            self.searchWrapView.setBorder(width: 1.0, color: .gray10)
        }
        
    }
}

extension UnivCoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class TabMenuButton: UIButton {
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    func selected() {
        self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16)
        self.setTitleColor(.primary100, for: .normal)
        self.setTitleColor(.primary100, for: .highlighted)
    }
    
    func normal() {
        self.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 16)
        self.setTitleColor(.gray50, for: .normal)
        self.setTitleColor(.gray50, for: .highlighted)
    }
}
