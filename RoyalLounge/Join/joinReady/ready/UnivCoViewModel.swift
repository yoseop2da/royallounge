//
//  UnivCoViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import RxDataSources

class UnivCoViewModel: BaseViewModel {
    
    private let sections = BehaviorRelay<[UnivCoDetailSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let nextButtonEnabled = BehaviorRelay<Bool>.init(value: false)
    let rejectInfo = BehaviorRelay<(display: Bool, msg: String?)>.init(value: (display: false, msg: nil))
    
    var univCoData = BehaviorRelay<UnivCoModel>.init(value: UnivCoModel.init())
    var joinCoCrown = 0
    var joinUnivCrown = 0
    
    init(userNo: String, univCoType: UnivCoModel.UnivCoType, nextButtonTap: Observable<Void>) {
        super.init()
        
        let addImages: [SpecPhotoData] = []
        sections.accept([UnivCoDetailSection.init(items: [SpecDetailCellModel.addPhoto(placeholders: [.must,.optional,.optional], addImages: addImages)])])
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .subscribe(onNext: { crown in
                self.joinUnivCrown = crown?.joinUniv ?? 0
                self.joinCoCrown = crown?.joinCo ?? 0
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getMemberUnivAndCo(userNo: userNo)
            .subscribe(onNext: {
                guard let result = $0 else { return }
                let univPhotos = result.univPhotos?.compactMap{ $0 }.map { SpecPhotoData.init(image: nil, photoUrl: $0.photoUrl, isReject: $0.rejectYn, imageData: nil, rejectMsg: $0.rejectMsg)} ?? []
                let coPhotos = result.companyPhotos?.compactMap{ $0 }.map { SpecPhotoData.init(image: nil, photoUrl: $0.photoUrl, isReject: $0.rejectYn, imageData: nil, rejectMsg: $0.rejectMsg) } ?? []
                let univ = (result.univ == nil || result.univNo == nil) ? nil : UniversityCert.init(name: result.univ!, univNo: result.univNo!, univStatus: $0?.univStatus, univRejectMsg: result.univRejectMsg, univRejectYn: result.univRejectYn)
                let co = (result.company == nil || result.companyNo == nil) ? nil : CompanyCert.init(name: result.company!, coNo: result.companyNo!, companyStatus: $0?.companyStatus, companyRejectMsg: result.companyRejectMsg, companyRejectYn: result.companyRejectYn)
                self.univCoData.accept(UnivCoModel.init(univCoType: univCoType, univ: univ, co: co, univDataList: univPhotos, coDataList: coPhotos, serverDataBaseValue: (univName: $0?.univ ?? "", coName: $0?.company ?? "")))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
            
        univCoData.subscribe(onNext: { univCo in
            self.sections.accept([UnivCoDetailSection.init(items: [SpecDetailCellModel.addPhoto(placeholders: [.must,.optional,.optional], addImages: univCo.currentDataList)])])
            
            var rejectList: [String] = []
            if univCo.univCoType == .univ {
                if let univStatus = univCo.univ?.univStatus, univStatus == Str.reject {
                    if let univRejectMsg = univCo.univ?.univRejectMsg {
                        rejectList.append(univRejectMsg)
                    }
                    let univDataList = univCo.univDataList
                    univDataList.filter{$0.isReject}.forEach{
                        if let rejectMsg = $0.rejectMsg, !rejectList.contains(rejectMsg) {
                            rejectList.append(rejectMsg)
                        }
                    }
                }
            }else{
                if let companyStatus = univCo.co?.companyStatus, companyStatus == Str.reject {
                    if let companyRejectMsg = univCo.co?.companyRejectMsg {
                        rejectList.append(companyRejectMsg)
                    }
                    let coDataList = univCo.coDataList
                    coDataList.filter{$0.isReject}.forEach{
                        if let rejectMsg = $0.rejectMsg, !rejectList.contains(rejectMsg) {
                            rejectList.append(rejectMsg)
                        }
                    }
                }
            }
//            ● •·・
            let rejectMsg = rejectList.removeDuplicates().map{ " • \($0)" }.joined(separator: "\n")
            self.rejectInfo.accept((display: rejectList.count > 0, msg: rejectMsg))
        })
        .disposed(by: bag)
        
        univCoData.map { $0.buttonStateEnable }
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)

        nextButtonTap
            .withLatestFrom(univCoData)
            .flatMapLatest{ univCo -> Observable<Bool> in
                if univCo.univCoType == .univ {
                    return Repository.shared.main.addUnivPhoto(userNo: userNo, univNo: univCo.univ!.univNo, imageDatas: univCo.imageDataList)
                }else{
                    return Repository.shared.main.addCompanyPhoto(userNo: userNo, coNo: univCo.co!.coNo, imageDatas: univCo.imageDataList)
                }
            }
            .subscribe(onNext: { _ in
                var newUnivCo = self.univCoData.value
                if newUnivCo.univCoType == .univ {
                    if newUnivCo.univ?.univStatus == nil {
                        toast(message: "승인 시 크라운 \(self.joinUnivCrown)개 무료 적립", seconds: 1.5, actionAfter: nil)
                    }else{
                        toast(message: "승인 요청되었습니다", seconds: 1.5, actionAfter: nil)
                    }
                    newUnivCo.univ?.univStatus = Str.ready
                }else{
                    if newUnivCo.co?.companyStatus == nil {
                        toast(message: "승인 시 크라운 \(self.joinCoCrown)개 무료 적립", seconds: 1.5, actionAfter: nil)
                    }else{
                        toast(message: "승인 요청되었습니다", seconds: 1.5, actionAfter: nil)
                    }
                    newUnivCo.co?.companyStatus = Str.ready
                }
                self.univCoData.accept(newUnivCo)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }

    func setStep01(name: String, no: String) {
        var newUnivCo = univCoData.value
        if newUnivCo.univCoType == .univ {
            newUnivCo.univ = UniversityCert.init(name: name, univNo: no, univStatus: Str.editing)
        }else{
            newUnivCo.co = CompanyCert.init(name: name, coNo: no, companyStatus: Str.editing)
        }
        univCoData.accept(newUnivCo)
    }

    private func addPhotoPrivate(idx: Int? = nil, image: UIImage?, imageData: Data) {
        var newUnivCo = univCoData.value
        if newUnivCo.univCoType == .univ {
            if let idx = idx {
                newUnivCo.univDataList[idx] = SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil)
            }else{
                newUnivCo.univDataList.append(SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil))
            }
            
            univCoData.accept(newUnivCo)
        }else{
            if let idx = idx {
                newUnivCo.coDataList[idx] = SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil)
            }else{
                newUnivCo.coDataList.append(SpecPhotoData.init(image: image, photoUrl: nil, isReject: false, imageData: imageData, rejectMsg: nil))
            }
            univCoData.accept(newUnivCo)
        }
    }
    
    func modifyPhoto(idx: Int, image: UIImage?, imageData: Data) {
        addPhotoPrivate(idx: idx, image: image, imageData: imageData)
    }
    
    func addPhoto(image: UIImage?, imageData: Data) {
        addPhotoPrivate(idx: nil, image: image, imageData: imageData)
    }
    
    func removePhoto(idx: Int) {
        var newUnivCo = univCoData.value
        if newUnivCo.univCoType == .univ {
            newUnivCo.univDataList.remove(at: idx)
            univCoData.accept(newUnivCo)
        }else{
            newUnivCo.coDataList.remove(at: idx)
            univCoData.accept(newUnivCo)
        }
    }
    
    func tapChange(type: UnivCoModel.UnivCoType) {
        var newUnivCo = univCoData.value
        newUnivCo.univCoType = type
        univCoData.accept(newUnivCo)
    }
}
