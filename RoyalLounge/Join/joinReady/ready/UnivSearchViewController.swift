//
//  UnivSearchViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/11.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import RxDataSources
import RxCocoa
import RxSwift

class UnivSearchViewController: LoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<UnivCoSearchSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var searchWrapView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    
    var refreshCallBack: (((keyword: String, no: String)?)->Void)?
    var type: UnivCoModel.UnivCoType = .univ
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, tableView, indexPath, item -> SearchTextCell in
            let cell: SearchTextCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item: item)
            return cell
        })
        
        return dataSource
    }
    
    var viewModel: UnivSearchViewModel!
    var isEditMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        searchWrapView.layer.cornerRadius = 6.0
        searchWrapView.setBorder(width: 1.0, color: .gray10)
//        let barButton = RLBarButtonItem.init(barType: .back)
//        barButton.rx.tap
//            .subscribe(onNext: { _ in
//                self.dismiss(animated: false, completion: nil)
//            })
//            .disposed(by: bag)
//        self.navigationItem.leftBarButtonItem = barButton
        
        searchTextField.placeholder = (type == .univ) ? "학교명을 입력하세요" : "직장명을 입력하세요"
        
        searchTextField.rx.text.orEmpty
            .debounce(RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { text in
                self.viewModel.search(text: text)
            })
            .disposed(by: bag)
        
        viewModel = UnivSearchViewModel.init(type: type)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { (keyword, no) in
                self.dismiss(animated: false, completion: {
                    self.refreshCallBack?(self.viewModel?.selectedValue)
                })
            })
            .disposed(by: bag)
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)

        mainTableView.rx.modelSelected(UnivCoSearchModel.self)
            .subscribe(onNext: { model in
                if model.isEmpty {
                    self.viewModel.addItem(keyword: model.searchText)
                }else{
                    self.viewModel.selectItem(item: model)
                }
            })
            .disposed(by: bag)
        
        cancelButton.rx.tap
            .rxTouchAnimation(button: cancelButton, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.dismiss(animated: false, completion: nil)
            })
        .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height
                    //                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 88)
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        searchTextField.becomeFirstResponder()
    }
}

extension UnivSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
}
