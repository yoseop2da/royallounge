//
//  UnivSearchViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

class UnivSearchViewModel: BaseViewModel {
    lazy var sectionData = sections.asObservable()
    var sections = BehaviorRelay<[UnivCoSearchSection]>.init(value: [])
    let nextSuccess = PublishRelay<(keyword: String, no: String)>()
    
    private var fullDataSections = BehaviorRelay<[UnivCoSearchSection]>.init(value: [])
    var type: UnivCoModel.UnivCoType = .univ
    
    init(type: UnivCoModel.UnivCoType) {
        super.init()
        self.type = type
    }
    
    var selectedValue: (keyword: String, no: String)?
    
    func addItem(keyword: String) {
        if self.type == .univ {
            Repository.shared.graphQl
                .addUnivKeyword(keyword: keyword)
                .subscribe(onNext: { result in
                    self.selectedValue = result//(keyword: result.keyword, no: result.no)
                    self.nextSuccess.accept(result)
                })
                .disposed(by: bag)
        }else{
            Repository.shared.graphQl
            .addCompanyKeyword(keyword: keyword)
            .subscribe(onNext: { result in
                self.selectedValue = result//(keyword: result.keyword, no: result.no)
                self.nextSuccess.accept(result)
            })
            .disposed(by: bag)
        }
        
    }
    
    func selectItem(item: UnivCoSearchModel) {
        selectedValue = (keyword: item.name, no: item.searchNo)
        self.nextSuccess.accept((keyword: item.name, no: item.searchNo))
    }
    
    func search(text: String) {
        if type == .univ {
            
            Repository.shared.graphQl
                .searchUniv(text: text)
                .subscribe(onNext: {
                    let result = $0.compactMap{ UnivCoSearchModel.init(name: $0?.name, searchNo: $0?.univNo, searchText: text)}
                    if result.count > 0 {
                        self.sections.accept([UnivCoSearchSection.init(header: "", items: result)])
                    }else{
                        self.sections.accept([UnivCoSearchSection.init(header: "", items: [UnivCoSearchModel.init(empty: true, searchText: text)])])
                    }
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }else{
            Repository.shared.graphQl
                .searchCompany(text: text)
                .subscribe(onNext: {
                    let result = $0.compactMap{ UnivCoSearchModel.init(name: $0?.name, searchNo: $0?.coNo, searchText: text)}
                    if result.count > 0 {
                        self.sections.accept([UnivCoSearchSection.init(header: "", items: result)])
                    }else{
                        self.sections.accept([UnivCoSearchSection.init(header: "", items: [UnivCoSearchModel.init(empty: true, searchText: text)])])
                    }
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: bag)
        }
    }
}

struct UnivCoSearchSection {
    var header: String
    var items: [UnivCoSearchModel]
}

extension UnivCoSearchSection: SectionModelType {
    typealias Item = UnivCoSearchModel
    init(original: UnivCoSearchSection, items: [Item]) {
        self = original
        self.items = items
    }
}

class UnivCoSearchModel {
    var searchText: String = ""
    var searchNo: String = ""
    var name: String = ""
    
    var isEmpty = false
    
    init(empty: Bool, searchText text: String) {
        isEmpty = true
        searchText = text
        name = text
    }
    
    init?(name: String?, searchNo: String?, searchText: String) {
        guard let _name = name else { return nil }
        guard let _searchNo = searchNo else { return nil }
        self.name = _name
        self.searchNo = _searchNo
        self.searchText = searchText
    }
}

