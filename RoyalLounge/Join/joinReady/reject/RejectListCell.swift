//
//  RejectListCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RejectListCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.spoqaHanSansRegular(ofSize: 16)
//        backgroundColor = .primary100
//        layer.cornerRadius = 6.0
//        layer.borderWidth = 1.0
//        layer.borderColor = UIColor.gray10.cgColor
    }
    
    func setModel(item: RejectModel) {
        titleLabel.text = item.title
    }

}
