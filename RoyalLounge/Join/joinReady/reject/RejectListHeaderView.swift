//
//  RejectListHeaderView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RejectListHeaderView: UICollectionReusableView {
    @IBOutlet weak var headerLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }

}
