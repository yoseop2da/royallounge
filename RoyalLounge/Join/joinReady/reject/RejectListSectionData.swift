//
//  RejectListSectionData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxDataSources

struct RejectData {
    var header: String = ""
    var list: [RejectModel] = []
    
    init?(data: GetRejectGroupListQuery.Data.MemberRejectGroupList?) {
        guard let list = data?.rejectList else { return nil }
        guard let header = data?.rejectHeader else { return nil }
        let rejectModels = list.compactMap{$0}.compactMap{RejectModel.init(data: $0)}
        if rejectModels.count == 0 { return nil }
        self.list = rejectModels
        self.header = header
    }
}


struct RejectModel {
    var title: String
    var specNo: String?
//    var stepSeq: Int?
    var interviewSeq: Int?
    var rejectType: RejectType
    
    init?(data: GetRejectGroupListQuery.Data.MemberRejectGroupList.RejectList?) {
        guard let rejectType = data?.rejectType else { return nil }
        guard let title = data?.title else { return nil }
        self.rejectType = rejectType
        self.title = title

        if let specNo = data?.specNo {
            self.specNo = specNo
        }
        if let interviewSeq = data?.interviewSeq {
            self.interviewSeq = interviewSeq
        }
    }
}

struct RejectListSection {
    var header: String
    var items: [RejectModel]
}

extension RejectListSection: SectionModelType {
    typealias Item = RejectModel
    init(original: RejectListSection, items: [Item]) {
        self = original
        self.items = items
    }
}
