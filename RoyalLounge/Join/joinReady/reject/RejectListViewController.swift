//
//  RejectListViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class RejectListViewController: ClearNaviLoggedBaseViewController {
    
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<RejectListSection>

    @IBOutlet weak var collectionView: UICollectionView!
    
    var isEditMode: Bool = false
    var viewModel: RejectListViewModel!
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, collectionView, indexPath, item -> RejectListCell in
            let cell: RejectListCell = collectionView.dequeueReusable(for: indexPath)
            cell.setModel(item: item)
            return cell
        })
        
        dataSource.configureSupplementaryView = { dataSource, collectionView, kind, indexPath in
            if kind == UICollectionView.elementKindSectionHeader {
                let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: RejectListHeaderView.className, for: indexPath) as! RejectListHeaderView
                section.headerLabel?.text = self.viewModel.sections.value[indexPath.section].header
                return section
            }else{
                return UICollectionReusableView()
            }
        }
        return dataSource
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.headerReferenceSize = CGSize(width: collectionView.frame.size.width, height: 72)
        flowLayout.itemSize = CGSize(width: UIScreen.width, height: 56)
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.collectionViewLayout = flowLayout
            
        collectionView.rx.modelSelected(RejectModel.self)
            .throttle(.milliseconds(1000), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { item in
                if item.rejectType == .spec {
                    let vc = RLStoryboard.join.specListViewController(isEditMode: true, backType: .back)
                    self.push(vc)
                }else if item.rejectType == .nickname {
                    let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .nickname, isEditMode: true)
                    self.push(vc)
                }else if item.rejectType == .job {
                    let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .job, isEditMode: true)
                    self.push(vc)
                }else if item.rejectType == .interview {
                    let seq = item.interviewSeq!
                    let vc = RLStoryboard.join.interviewViewController(interViewSeq: seq, isEditMode: true)
                    self.push(vc)
                }else if item.rejectType == .aboutMe {
                    let vc = RLStoryboard.join.aboutMeViewController(isEditMode: true)
                    self.push(vc)
                }else if item.rejectType == .photo {
                    let vc = RLStoryboard.join.photoViewController(isEditMode: true, isReject: true)
                    self.push(vc)
                }
            })
            .disposed(by: bag)
        
        viewModel = RejectListViewModel.init(userNo: MainInformation.shared.userNoAes!)
//        viewModel.userStatusChanged
//            .subscribe(onNext: { joinStep  in
//                Coordinator.shared.moveBy(joinStep: joinStep)
//            })
//            .disposed(by: bag)
        
        self.viewModel.sectionData
          .bind(to: collectionView.rx.items(dataSource: dataSource))
          .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "반려 내역"
        if isFirstTime { isFirstTime = false; return }
        
        self.viewModel.refresh()
    }
}
