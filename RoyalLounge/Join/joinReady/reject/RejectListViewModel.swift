
//
//  RejectListViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RejectListViewModel: BaseViewModel {

    let sections = BehaviorRelay<[RejectListSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
//    var userStatusChanged = PublishRelay<MemberStatus>()
    var userNo: String!
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        loadData(userNo: userNo)
    }
    
    func refresh() {
        loadData(userNo: userNo)
    }
    
    func loadData(userNo: String) {
        Repository.shared.graphQl
            .getRejectList(userNo: userNo)
            .subscribe(onNext: { rejectList in
                if rejectList.count == 0 {
                    Coordinator.shared.moveByUserStatus()
                }
                self.sections.accept(rejectList.map{RejectListSection.init(header: $0.header, items: $0.list)})
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
//                alertNETWORK_ERR()
            })
            .disposed(by: bag)
    }
    
}
