//
//  RejectViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RejectViewController: LoggedBaseViewController {

    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var rejectListButton: UIButton!

    @IBOutlet weak var custommerCenterButton: UIButton!
    var viewModel: RejectViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        nextButton.layer.cornerRadius = 6.0
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }
        
        viewModel = RejectViewModel(userNo: MainInformation.shared.userNoAes!)
        
        rejectListButton.rx.tap
            .rxTouchAnimation(button: rejectListButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                // 추가프로필
                let vc = RLStoryboard.join.rejectListViewController()
                self.push(vc)
            })
            .disposed(by: bag)
        
//        custommerCenterButton.underLine()
        custommerCenterButton.rx.tap
            .rxTouchAnimation(button: custommerCenterButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let myNickname = self.viewModel.nickname
                let userId = self.viewModel.userId
                let mbNo = self.viewModel.mbNo
                RLMailSender.shared.sendEmail(.문의_가입유저(nickname: myNickname, userId: userId, mbNo: mbNo), finishCallback: { _ in
                    self.dismiss(animated: true, completion: nil)
                })
            }, onError: { err in
                
            })
            .disposed(by: bag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        hideNavigationControllerBottomLine()
    }
}
