//
//  RejectViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RejectViewModel: BaseViewModel {
    var nickname: String = ""
    var userId: String = ""
    var mbNo: String = "입력해주세요."
 
    init(userNo: String) {
        super.init()
        
        Repository.shared.graphQl
            .getEmailInfo(userNo: userNo)
            .subscribe(onNext: {
                guard let result = $0 else { return }
                self.nickname = result.nickname
                self.userId = result.userId.aesDecrypted!
                self.mbNo = result.mbNo.aesDecrypted!
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
}
