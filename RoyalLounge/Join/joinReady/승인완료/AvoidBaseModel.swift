//
//  AvoidBaseModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import RxDataSources
import RxSwift
import RxCocoa

class AvoidBaseModel: BaseViewModel {
    lazy var sectionData = sections.asObservable()
    var sections = BehaviorRelay<[AvoidSection]>.init(value: [])
    
    var registeredAvoidList = BehaviorRelay<[AvoidModel]>.init(value: [])
    var contactsList = BehaviorRelay<[AvoidModel]>.init(value: [])
    
    var headerTitle = ["차단 대기 번호", "차단 완료 번호"]
    
    var modified = false
    
    override init() {
        super.init()
        loadContactList()
            .map{ $0.compactMap{ AvoidModel.init(contact: $0) } }//.sorted { $0.name < $1.name }
            .bind(to: contactsList)
            .disposed(by: bag)
    }
    
    func refreshRegAvoidList() {
        Repository.shared.graphQl
            .getAvoidList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                let avoidList = $0
                    .compactMap{ AvoidModel.init(mbNo: $0.mbNo, name: $0.name, isSelected: true, avoidNo: $0.avoidNo) }
                    .sorted { $0.name < $1.name }
                self.registeredAvoidList.accept(avoidList)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
//        Repository.shared.graphQl
//            .getAvoidList(userNo: MainInformation.shared.userNoAes!)
//            .map { $0.compactMap{ AvoidModel.init(mbNo: $0.mbNo, name: $0.name, isSelected: true, avoidNo: $0.avoidNo) }.sorted { $0.name < $1.name }}
//            .bind(to: registeredAvoidList)
//            .disposed(by: bag)
    }
    
    func loadContactList() -> Observable<[CNContact]> {
        return Observable.create { observer in
            var contacts: [CNContact] = []
            let store = CNContactStore()
            let keysToFetch: [CNKeyDescriptor] = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                                                  CNContactImageDataKey as CNKeyDescriptor,
                                                  CNContactPhoneNumbersKey as CNKeyDescriptor]
            let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch)
            fetchRequest.sortOrder = CNContactSortOrder.userDefault
            store.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) in
                if granted {
                    do { try
                        store.enumerateContacts(with: fetchRequest, usingBlock: { (contact, stop) -> Void in
                            contacts.append(contact)
                        })
                    } catch let e as NSError {
                        Debug.print("[\(#function)][\(#line)] \(e.localizedDescription)")
                    }
                    observer.on(.next(contacts))
                } else {
                    mainAsync {
                        alertDialogTwoButton(title: "연락처 접근 권한을 허용해주세요.", message: "설정 화면으로 이동합니다.", okTitle: "확인", okAction: { _ in
                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: { _ in
                                UIViewController.keyController().navigationController?.popViewController(animated: true)
                            })
                        }, cancelTitle: "취소") { _ in
                            
                        }
                    }
                }
            })

            return Disposables.create()
        }
    }
}

struct AvoidSection {
    var header: String
    var items: [AvoidModel]
}

extension AvoidSection: SectionModelType {
    typealias Item = AvoidModel
    init(original: AvoidSection, items: [Item]) {
        self = original
        self.items = items
    }
}

class AvoidModel {
    var searchText: String = ""
    var avoidNo: String?
    var name: String = "" // fullName
    var mbNoOnlyNum: String { return numbers.first!.replacingOccurrences(of: "-", with: "") }

    var mbNoFormatted: String {
        let num = numbers.first!
        if num.length == 10 {
            let str0 = (num as NSString).substring(with: NSRange.init(location: 0, length: 3)) // 010
            let str1 = (num as NSString).substring(with: NSRange.init(location: 3, length: 3)) // 899
            let str2 = (num as NSString).substring(with: NSRange.init(location: 6, length: 4)) // 7677
            return "\(str0)-\(str1)-\(str2)"
        } else if num.length == 11 {
            let str0 = (num as NSString).substring(with: NSRange.init(location: 0, length: 3)) // 010
            let str1 = (num as NSString).substring(with: NSRange.init(location: 3, length: 4)) // 8999
            let str2 = (num as NSString).substring(with: NSRange.init(location: 7, length: 4)) // 7677
            return "\(str0)-\(str1)-\(str2)"
        }
        return num
    }
    var isSelected = false
    var isEmpty = false
    
    private var numbers: [String] = []
    init(empty: Bool) {
        isEmpty = true
    }
    init(mbNo: String, name: String, isSelected: Bool, avoidNo: String? = nil) {
        if self.numbers.count > 0 {
            self.numbers[0] = mbNo
        }else{
            self.numbers.append(mbNo)
        }
        
        self.name = name
        self.avoidNo = avoidNo
        self.isSelected = isSelected
        
//        print("초성 획득:[\(self.name)][\(makeChosung(name: self.name))]")
    }
    
    init?(contact: CNContact) {
        if let name = CNContactFormatter.string(from: contact, style: .fullName) {
            self.name = name
        } else {
            guard contact.phoneNumbers.count > 0 else { return nil }
            let number: CNLabeledValue = contact.phoneNumbers.first!
            let phone: CNPhoneNumber = number.value
            let digits = phone.value(forKey: "digits") as? String ?? ""
            self.name = digits
        }
//        print("초성 획득:[\(self.name)][\(makeChosung(name: self.name))]")
        contact.phoneNumbers.forEach {
            let phone = $0.value
            let digits = phone.value(forKey: "digits") as? String ?? ""
            let optDigits = digits.removeKoreanMobile82 // +82 -> 0 으로 변경
            if optDigits.isMobileNumber { self.numbers.append(optDigits) }
        }
        if self.numbers.count == 0 { return nil }
    }
    
    func makeChosung(name:String) -> String {
        return name.unicodeScalars
            .map { u in
                let iu = Int(u.value)
                guard iu >= 0xac00 && iu <= 0xd7a3 else { return String(u) }
                let index = (iu - 0xac00) / 28 / 21
                return String(UnicodeScalar(index + 0x1100)!)
        }.joined(separator: "")
    }
}
