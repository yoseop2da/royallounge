//
//  AvoidCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AvoidCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mbNoLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    
    private var bag = DisposeBag()
    var buttonTouched: ((Bool)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rightButton.setBorder(color: .brown75)
        rightButton.layer.cornerRadius = 6.0
        rightButton.clipsToBounds = true
        rightButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
    }
    
    @IBAction func buttonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.buttonTouched?(sender.isSelected)
        }
    }
    
    func setItem(item: AvoidModel) {
        
        if item.isEmpty {
            nameLabel.text = "검색 결과가 없습니다."
            mbNoLabel.text = ""
            rightButton.isHidden = true
            return
        }else{
            nameLabel.text = ""
            mbNoLabel.text = ""
            rightButton.isHidden = false
        }
        
        if item.name.contains(item.searchText) {
            // 이름
            let fullStr = item.name
            let attrStr = NSMutableAttributedString.init(string: fullStr)
            let detectedRange = (fullStr.uppercased() as NSString).range(of: item.searchText.uppercased())
            attrStr.addAttributes([.foregroundColor: UIColor.successColor], range: detectedRange)
            nameLabel.attributedText = attrStr
        }else{
            nameLabel.text = item.name
        }
        if item.mbNoOnlyNum.contains(item.searchText) {
            let fullStr = item.mbNoOnlyNum
            let attrStr = NSMutableAttributedString.init(string: fullStr)
            let detectedRange = (fullStr as NSString).range(of: item.searchText)
            attrStr.addAttributes([.foregroundColor: UIColor.successColor], range: detectedRange)
            
            let dashAttrStr = NSMutableAttributedString.init(string: "-")
            attrStr.insert(dashAttrStr, at: 3)
            attrStr.insert(dashAttrStr, at: 8)
            
            mbNoLabel.attributedText = attrStr
        }else{
            mbNoLabel.text = item.mbNoFormatted
        }
        
        rightButton.isSelected = item.isSelected
        rightButton.setTitle(item.isSelected ? "해제하기" : "차단하기", for: .normal)
        
        if item.isSelected {
            rightButton.setTitleColor(.brown100, for: .normal)
            rightButton.backgroundColor = .white
        }else{
            rightButton.setTitleColor(.white, for: .normal)
            rightButton.backgroundColor = .brown75
        }
    }

}
