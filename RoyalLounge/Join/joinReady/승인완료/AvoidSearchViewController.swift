//
//  AvoidSearchViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/05/08.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//
import UIKit
import Contacts
import ContactsUI
import RxDataSources
import RxCocoa
import RxSwift

class AvoidSearchViewController: LoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<AvoidSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var dimView: UIView!
    
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var searchWrapView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    var refreshCallBack: ((Bool)->Void)?
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, tableView, indexPath, item -> AvoidCell in
            let cell: AvoidCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item: item)
            cell.buttonTouched = { isSelected in
                print("----------\(item)")
//                self.viewModel.selected(item: item)
                if isSelected {
                    // 해제하기
                    self.viewModel.removeItem(item: item)
                }else{
                    // 차단하기
                    self.viewModel.addItem(item: item)
                }
                
            }
            return cell
        })
        
        return dataSource
    }
    
    var viewModel: AvoidSearchViewModel!
    var isEditMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchWrapView.layer.cornerRadius = 6.0
        searchWrapView.setBorder(width: 1.0, color: .gray10)
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        searchTextField.rx.text.orEmpty
            .debounce(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: { text in
                self.dimView.isHidden = text.length > 0
                self.viewModel.search(text: text)
            })
            .disposed(by: bag)
        
        viewModel = AvoidSearchViewModel.init(text: "")
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)

        cancelButton.rx.tap
            .rxTouchAnimation(button: cancelButton, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.dismiss(animated: false) {
                    self.refreshCallBack?(self.viewModel.modified)
                }
            })
        .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height
//                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 88)
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        searchTextField.becomeFirstResponder()
    }
}

extension AvoidSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init()
        let label = UILabel.init()
        let headerString = viewModel.searchHeaderTitle(idx: section)
        label.text = headerString
        label.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        label.textColor = .dark
        view.addSubview(label)
        
        label.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.top.bottom.equalToSuperview()
        }
        return view
    }
}
