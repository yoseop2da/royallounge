//
//  AvoidSearchViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AvoidSearchViewModel: AvoidBaseModel {

    private var fullDataSections = BehaviorRelay<[AvoidSection]>.init(value: [])

    var fullDataOnly = false
    
    init(text: String) {
        super.init()
        refreshRegAvoidList()
        
        Observable.combineLatest(registeredAvoidList, contactsList)
            .subscribe(onNext: { reg, contacts in
                let mbNos = reg.map{ $0.mbNoFormatted }
                let unReg = contacts
                    .filter{!mbNos.contains($0.mbNoFormatted)}
                self.headerTitle = ["차단 대기 번호 (\(unReg.count))", "차단 완료 번호 (\(reg.count))"]
                let fullData = [AvoidSection.init(header: self.headerTitle[0], items: unReg), AvoidSection.init(header: self.headerTitle[1], items: reg)]
                self.fullDataSections.accept(fullData)
                
                if self.fullDataOnly { return }
                self.sections.accept(fullData)
            })
            .disposed(by: bag)
    }
    
    func searchHeaderTitle(idx: Int) -> String {
        if sections.value.count > 1 {
            return self.headerTitle[idx]
        }else{
            if let header = sections.value.first?.header {
                return header
            }else{
                return "검색 결과"
            }
        }
    }
    
    func search(text: String) {
        guard text.count > 0 else {
            sections.accept(
                fullDataSections.value
                    .map { avoidSection -> AvoidSection in
                    let newItems = avoidSection.items.map{ model -> AvoidModel in
                        let _model = model
                        _model.searchText = ""
                        return _model
                    }
                    return AvoidSection.init(header: avoidSection.header, items: newItems)
                }
            )
            return
        }

        let result = fullDataSections.value
            .map{ $0.items }
            .flatMap{$0}
            .filter {
                return $0.mbNoOnlyNum.contains(text) || $0.name.contains(text) }
            .map{ model -> AvoidModel in
                let new = model
                new.searchText = text
                return new }
            .sorted { $0.name < $1.name }
        
        if result.count > 0 {
            sections.accept([AvoidSection.init(header: "검색 결과 (\(result.count))", items: result)])
        }else{
            sections.accept([AvoidSection.init(header: "검색 결과 (0)", items: [AvoidModel.init(empty: true)])])
        }
    }
    
    func addItem(item: AvoidModel) {
        modified = true
        Repository.shared.graphQl
            .updateAvoidList(userNo: MainInformation.shared.userNoAes!, avoidList: [item])
            .map { $0.compactMap{ AvoidModel.init(mbNo: $0.mbNo, name: $0.name, isSelected: true, avoidNo: $0.avoidNo) }.sorted { $0.name < $1.name }.first(where: {$0.name == item.name })}
            .subscribe(onNext: { newItem in
                if let new = newItem, var avoidSection = self.sections.value.first {
                    var searchList = avoidSection.items
                    if let idx = searchList.firstIndex(where: {$0.name == item.name}) {
                        new.searchText = item.searchText
                        searchList[idx] = new
                        avoidSection.items = searchList
                        self.sections.accept([avoidSection])
                    }
                }
                self.fullDataOnly = true
                self.refreshRegAvoidList()
            })
            .disposed(by: bag)
    }
    
    func removeItem(item: AvoidModel) {
        guard let avoidNo = item.avoidNo else { return }
        modified = true
        Repository.shared.graphQl
            .removeAvoidList(userNo: MainInformation.shared.userNoAes!, avoidNoList: [avoidNo])
//            .map { $0.compactMap{ AvoidModel.init(mbNo: $0.mbNo, name: $0.name, isSelected: true, avoidNo: $0.avoidNo) }.sorted { $0.name < $1.name }.first(where: {$0.name == item.name })}
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in
                if var avoidSection = self.sections.value.first {
                    var searchList = avoidSection.items
                    if let idx = searchList.firstIndex(where: {$0.name == item.name}) {
                        item.isSelected.toggle()
                        searchList[idx] = item
                        avoidSection.items = searchList
                        self.sections.accept([avoidSection])
                    }
                }
                self.fullDataOnly = true
                self.refreshRegAvoidList()
            })
        .disposed(by: bag)
    }
}


