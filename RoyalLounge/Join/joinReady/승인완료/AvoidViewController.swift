//
//  AvoidViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class AvoidViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<AvoidSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var searchWrapView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, tableView, indexPath, item -> AvoidCell in
            let cell: AvoidCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item: item)
            cell.buttonTouched = { isSelected in
                print("----------\(item)")
                if isSelected { // 해제하기
                    self.viewModel.removeItems(items: [item])
                }else{ // 차단하기
                    self.viewModel.addItems(items: [item])
                }
            }
            return cell
        })
        
        return dataSource
    }
    
    var viewModel: AvoidViewModel!
    var isEditMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeBackAnyWhere()
        
        searchWrapView.layer.cornerRadius = 6.0
        searchWrapView.setBorder(width: 1.0, color: .gray10)
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = AvoidViewModel.init(text: "")
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
        
        searchButton.rx.tap
            .rxTouchAnimation(button: searchButton, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                UIView.animate(withDuration: 0.4, animations: {
                    let naviHeight = (self.navigationController?.navigationBar.frame.size.height ?? 0.0)
                    self.mainTableView.setContentOffset(CGPoint.init(x: 0, y: naviHeight + 88.0), animated: false)
                    self.view.layoutIfNeeded()
                }) { _ in
                    if #available(iOS 10.0, *) {
                        let generator = UIImpactFeedbackGenerator(style: .heavy)
                        generator.impactOccurred()
                    }
                }
                
                delay(0.30) {
                    let vc = RLStoryboard.join.avoidSearchViewController()
                    vc.refreshCallBack = { refresh in
                        self.mainTableView.setContentOffset(CGPoint.zero, animated: true)
                        if refresh {
                            self.viewModel.refreshRegAvoidList()
                        }
                    }
                    self.clearPresent(vc)
                }
            })
            .disposed(by: bag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}

extension AvoidViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 32
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 52))
        let label = UILabel.init()
        let headerString = viewModel.headerTitle[section]
        label.text = headerString
        label.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        label.textColor = .dark
        view.addSubview(label)
        label.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.top.bottom.equalToSuperview()
        }
        let button = UIButton.init()
        button.setTitle(section == 0 ? "모두 차단" : "모두 해제", for: .normal)
        button.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12.0)
        
        button.setTitleColor(headerString.contains("(0)") ? .gray25 : .brown100, for: .normal)
        view.addSubview(button)
        button.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(0)
            $0.width.equalTo(87)
            $0.top.bottom.equalToSuperview()
        }
        button.rx.tap
            .rxTouchAnimation(button: button, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if section == 0 {
                    self.viewModel.addAll()
                }else{
                    self.viewModel.removeAll()
                }
            })
            .disposed(by: bag)
        return view
    }
}
