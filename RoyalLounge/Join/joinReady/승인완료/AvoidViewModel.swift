//
//  AvoidViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import RxDataSources
import RxSwift
import RxCocoa

class AvoidViewModel: AvoidBaseModel {
    
    init(text: String) {
        super.init()
        refreshRegAvoidList()
        
        Observable.combineLatest(registeredAvoidList, contactsList)
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { reg, contacts in
                RLProgress.shared.hideCustomLoading(delaySec: 0.3, completion: nil)
                let regMbNos = reg.map{ $0.mbNoFormatted }
                let unReg = contacts.filter{ return !regMbNos.contains($0.mbNoFormatted) }
                self.headerTitle = ["차단 대기 번호 (\(unReg.count))", "차단 완료 번호 (\(reg.count))"]
                let fullData = [AvoidSection.init(header: self.headerTitle[0], items: unReg), AvoidSection.init(header: self.headerTitle[1], items: reg)]
                print("================ 아는사람 피하기 리로드!![\(unReg.count)][\(reg.count)]")
                self.sections.accept(fullData)
            })
            .disposed(by: bag)
    }
    
    func addAll() {
        addItems(items: sections.value[0].items)
    }
    
    func removeAll() {
        removeItems(items: sections.value[1].items)
    }
    
    func addItems(items: [AvoidModel]) {
        guard items.count > 0 else { return }
        RLProgress.shared.showCustomLoading()
        self.modified = true
        Repository.shared.graphQl
            .updateAvoidList(userNo: MainInformation.shared.userNoAes!, avoidList: items)
            .map { $0.compactMap{ AvoidModel.init(mbNo: $0.mbNo, name: $0.name, isSelected: true, avoidNo: $0.avoidNo) }.sorted { $0.name < $1.name }}
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                self.registeredAvoidList.accept(result)
            })
            .disposed(by: self.bag)
    }

    func removeItems(items: [AvoidModel]) {
        guard items.count > 0 else { return }
        RLProgress.shared.showCustomLoading()
        modified = true
        Repository.shared.graphQl
            .removeAvoidList(userNo: MainInformation.shared.userNoAes!, avoidNoList: items.compactMap{$0.avoidNo})
            .map { $0.compactMap{ AvoidModel.init(mbNo: $0.mbNo, name: $0.name, isSelected: true, avoidNo: $0.avoidNo) }.sorted { $0.name < $1.name }}
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                self.registeredAvoidList.accept(result)
            })
            .disposed(by: bag)
    }
}
