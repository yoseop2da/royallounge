//
//  JoinPathCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class JoinPathCell: UITableViewCell {

    @IBOutlet weak var itemLabel: UILabel!
//    @IBOutlet weak var itemButton: UIButton!

    private var bag = DisposeBag()
    
    var selectItem: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        self.backgroundColor = .clear
        itemLabel.layer.cornerRadius = 6
        itemLabel.layer.borderWidth = 1.0
        itemLabel.layer.borderColor = UIColor.gray10.cgColor
        itemLabel.clipsToBounds = true
        
        layer.masksToBounds = false

        itemLabel.rx.tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
                self.itemLabel.touchAnimation {
                    self.selectItem?()
                }
            })
            .disposed(by: bag)
        
    }
    
    func setModel(item: AppOptionModel) {
        itemLabel.text = item.optionValue

        let textColor = item.isSelected ? .primary100 : UIColor.gray75
        itemLabel.textColor = textColor
        
        let borderColor = item.isSelected ? UIColor.primary100.cgColor : UIColor.gray10.cgColor
        itemLabel.layer.borderColor = borderColor
    }
}
