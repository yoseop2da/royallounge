//
//  JoinPathData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

struct JoinPathSection {
    var header: String
    var items: [AppOptionModel]
}

extension JoinPathSection: SectionModelType {
    typealias Item = AppOptionModel
    init(original: JoinPathSection, items: [Item]) {
        self = original
        self.items = items
    }
}
