//
//  JoinPathViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class JoinPathViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<JoinPathSection>
    
    @IBOutlet weak var crownLabel: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var completeCenterImageView: UIImageView!
    let nextButton = RLNextButton.init(title: "참여 완료")
    
    var viewModel: JoinPathViewModel!
    var closeCallback: (()->Void)?
    
    var isEditMode = false
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, tableView, indexPath, item -> JoinPathCell in
            let cell: JoinPathCell = tableView.dequeueReusable(for: indexPath)
                cell.setModel(item: item)
            cell.selectItem = {
                print("----------\(item)")
                self.viewModel.selected(item: item)
            }
            return cell
        })
        
        return dataSource
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }

        if isEditMode {
            let barButton = RLBarButtonItem.init(barType: .close)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.dismiss(animated: true, completion: nil)
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = barButton
        }else{
            let barButton = RLBarButtonItem.init(barType: .customButton(title: "SKIP", font: UIFont.spoqaHanSansBold(ofSize: 14), textColor: UIColor.dark))
            barButton.customButton?.rx.tap
                .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    self.viewModel.skip(userNo: MainInformation.shared.userNoAes!)
                        .subscribe(onNext: { success in
                            self.dismiss(animated: false) {
                                self.closeCallback?()
                            }
                        }, onError: { _ in
                            self.dismiss(animated: false) {
                                self.closeCallback?()
                            }
                        }).disposed(by: self.bag)
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = barButton
        }
        
        
        viewModel = JoinPathViewModel(
            userNo: MainInformation.shared.userNoAes!,
            nextButtonTap: nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance).asObservable())
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.dismiss(animated: false) {
                    self.closeCallback?()
                }
            })
            .disposed(by: bag)
        
        viewModel.crown
            .subscribe(onNext: { crown in
                self.crownLabel.text = "지금 참여 시 크라운 \(crown)개 적립"
        })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                CompleteView.init(type: .joinPath).show {
                    self.dismiss(animated: false) {
                        self.closeCallback?()
                    }
                }
            })
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        self.hideNavigationControllerBottomLine()
    }
}

extension JoinPathViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
