//
//  JoinPathViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class JoinPathViewModel: BaseViewModel {
    var sections = BehaviorRelay<[JoinPathSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    var selectValue = BehaviorRelay<AppOptionModel?>.init(value: nil)
    
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()

    let crown = PublishSubject<Int>.init()
    
    init(userNo: String, nextButtonTap: Observable<Void>) {
        super.init()
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .map{ $0?.joinPath ?? 0 }
            .subscribe(onNext: {
                self.crown.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Repository.shared.main
            .appOption(appOptionType: .joinPath)
            .map{ [JoinPathSection(header: "", items: $0)] }
            .subscribe(onNext: { list in
                let newItems = (list.first?.items.map{ $0 } ?? [])
                self.sections.accept([JoinPathSection.init(header: "", items: newItems)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        selectValue
            .map{ $0 != nil }
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)
        
        nextButtonTap
            .throttle(.milliseconds(300), latest: false, scheduler: MainScheduler.instance)
            .withLatestFrom(selectValue)
            .flatMapLatest { value -> Observable<Bool> in
                return Repository.shared.graphQl
                    .updateJoinpath(userNo: userNo, joinPath: value!.optionCD) }
            .bind(to: nextSuccess)
            .disposed(by: bag)
    }
    
    func selected(item: AppOptionModel) {
        if let firstSection = sections.value.first, firstSection.items.count > 0 {
            let items = firstSection.items
                .map { _item -> AppOptionModel in
                    var newItem = _item
                    newItem.isSelected = _item.optionCD == item.optionCD
                    return newItem
            }
            sections.accept([JoinPathSection.init(header: firstSection.header, items: items)])
            selectValue.accept(item)
        }
    }
    
    func skip(userNo: String) -> Observable<Bool> {
        return Repository.shared.graphQl
            .skipJoinPath(userNo: userNo)
    }
}

