
//
//  KakaoLinkShare.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class KakaoLinkShare: NSObject {

    static let TEMPLATE_ID = "36639"

    static func shareCustomLink(recoCd: String) {
        let args = ["recoCd": recoCd]
        KLKTalkLinkCenter.shared().sendCustom(withTemplateId: KakaoLinkShare.TEMPLATE_ID, templateArgs: args, success: { (warningMsg, argumentMsg) in
            // 성공
            print("warning message: \(String(describing: warningMsg))")
            print("argument message: \(String(describing: argumentMsg))")
        }, failure: { (error) in
            // 실패
            print("error \(error.localizedDescription)")
//            toast(message: "친구에게 공유하기 위해\n'카카오톡' 설치가 필요합니다", seconds: 1.5)
        })
    }
}
