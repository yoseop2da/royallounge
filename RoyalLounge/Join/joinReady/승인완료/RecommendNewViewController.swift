

//
//  RecommendNewViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RecommendNewViewController: LoggedBaseViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var crownTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var myRecoCodeLabel: UILabel!
    @IBOutlet weak var myRecoCodeButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var kakaoButton: UIButton!
    @IBOutlet weak var textFieldWrapView: UIView!
    
    // 화면 고정
    @IBOutlet weak var nextButtonInnerWrapView: UIView!
    
    // 하단 고정
    @IBOutlet weak var bgWhiteView: UIView!
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    var closeCallback: (()->Void)?
    
    let nextButton = RLNextButton.init(title: "추천인 코드 저장")
    
    let recoTextField: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "추천인 초대 코드", type: .big, textAlignment: .center)
        field.maxLength = 6
        field.keyboardType = .asciiCapable
        return field
    }()
    
    override var prefersStatusBarHidden: Bool {
         return true
    }
    
    var viewModel: RecommendViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rlBrown238
        
        bottomPadding.constant = -85.0
        kakaoButton.layer.cornerRadius = 20
        crownTitleLabel.layer.cornerRadius = 4
        
        nextButtonInnerWrapView.addSubview(self.nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        textFieldWrapView.addSubview(recoTextField)
        recoTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(textFieldWrapView)
        }
        
        recoTextField.textField.rx.text.orEmpty
            .subscribe(onNext: { str in
                self.recoTextField.text = str.uppercased()
            })
            .disposed(by: bag)
        
        myRecoCodeButton.rx.tap
            .subscribe(onNext: { _ in
                UIPasteboard.general.string = self.viewModel?.myRecoCd.value
                toast(message: "복사되었습니다", seconds: 1.5, isOnKeyboard: self.bottomPadding.constant > 0)
            })
            .disposed(by: bag)
        
        kakaoButton.rx.tap
            .rxTouchAnimation(button: kakaoButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if !UIApplication.shared.canOpenURL(URL(string: "kakaolink://")!) {
                    toast(message: "친구에게 공유하기 위해\n'카카오톡' 설치가 필요합니다", seconds: 1.5, isOnKeyboard: self.bottomPadding.constant > 0)
                    return
                }
                let recoCode = self.viewModel.myRecoCd.value
//                let title = "로얄라운지 (추천인 코드:{RECO_CODE})".replacingOccurrences(of: "{RECO_CODE}", with: recoCode)
//                let description = "대한민국 최상위 1%만을 위한\n프리미엄 인증 소개팅 앱"
                KakaoLinkShare.shareCustomLink(recoCd: recoCode)
            })
            .disposed(by: bag)
        
        viewModel = RecommendViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            fieldString: recoTextField.textObserver
                .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else if $0.resultCD == RLResultCD.E01000_NoMember.code {
                    return
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.dismiss(animated: false) {
                    self.closeCallback?()
                }
            })
            .disposed(by: bag)
        
        viewModel.myRecoCd
            .subscribe(onNext: { code in
                let paragraph = NSMutableParagraphStyle()
                paragraph.alignment = .center
                let fullString = code.uppercased()
                let attributedString = NSMutableAttributedString(string: fullString, attributes: [
                    .font: UIFont.spoqaHanSansBold(ofSize: 36),
                    .kern: 5.0,
                    .paragraphStyle: paragraph
                        
                ])
                self.myRecoCodeLabel.attributedText = attributedString
            })
            .disposed(by: bag)
        
        viewModel.doRecoCd
            .subscribe(onNext: { code in
                if let _code = code {
                    self.recoTextField.text = _code
                    self.recoTextField.isUserInteractionEnabled = false
                    self.textFieldWrapView.alpha = 0.4
                }
            })
            .disposed(by: bag)
        
        viewModel.receiveCount
            .subscribe(onNext: { count in
                let paragraph = NSMutableParagraphStyle()
                paragraph.alignment = .center
                let boldStr = "\(count)"
                let fullString = "초대 성공 \(boldStr)"
                let attributedString = NSMutableAttributedString(string: fullString, attributes: [
                    .font: UIFont.spoqaHanSansRegular(ofSize: 12),
                    .paragraphStyle: paragraph
                ])
                let range = (fullString as NSString).range(of: boldStr)
                attributedString.addAttribute(.font, value: UIFont.spoqaHanSansBold(ofSize: 12), range: range)
                self.countLabel.attributedText = attributedString
            })
            .disposed(by: bag)
            
        viewModel.validatedCode
            .bind(to: recoTextField.rx.setProfileTextResult)
            .disposed(by: bag)
        
        viewModel.crown
            .subscribe(onNext: { crown in
                self.crownTitleLabel.text = "크라운 \(crown)개 적립!"
                self.subTitleLabel.text = "나와 추천 친구 모두에게 크라운 \(crown)개 적립!"
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                
                self.recoTextField.isUserInteractionEnabled = false
                self.textFieldWrapView.alpha = 0.4
                
                toast(message: "크라운 \(self.viewModel.crown.value)개 무료 적립", seconds: 1.5, isOnKeyboard: self.bottomPadding.constant > 0)
                
                MainInformation.shared.updateMyCrownNew()
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    if keyboard.height == 0 {
                        self.bottomPadding.constant = -85.0
                        self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 919)
                        
                        self.nextButtonInnerWrapView.addSubview(self.nextButton)
                        self.nextButton.snp.makeConstraints {
                            $0.top.bottom.equalToSuperview()
                            $0.leading.equalToSuperview().offset(20)
                            $0.trailing.equalToSuperview().offset(-20)
                        }
                        self.nextButton.layer.cornerRadius = 6.0
                    }else{
                        // 이미 키보드 올라와있는경우 반응안하기.
                        if self.bottomPadding.constant == keyboard.height { return }
                        
                        self.bottomPadding.constant = keyboard.height
                        self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 830/*버튼 빠진만큰 줄여주기*/)
                        
                        self.nextButtonWrapView.addSubview(self.nextButton)
                        self.nextButton.snp.makeConstraints {
                            $0.top.bottom.equalToSuperview()
                            $0.leading.equalToSuperview()
                            $0.trailing.equalToSuperview()
                        }
                        self.nextButton.layer.cornerRadius = 0.0
                    }
                    
                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 120)
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func keyboardHideButton(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationControllerBottomLine()
    }
}
