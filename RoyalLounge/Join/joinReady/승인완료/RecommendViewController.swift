//
//  RecommendViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RecommendViewController: ClearNaviLoggedBaseViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
        
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var textFieldWrapView: UIView!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    var closeCallback: (()->Void)?
    
    let nextButton = RLNextButton.init(title: "추천인 코드 저장")
    
    let recoTextField: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "마이페이지 > 친구초대하기", type: .big, textAlignment: .center)
        field.maxLength = 6
        field.keyboardType = .asciiCapable
        return field
    }()
    
    var isEditMode: Bool = false
    
    var viewModel: RecommendViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }

        if isEditMode {
            let barButton = RLBarButtonItem.init(barType: .close)
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.dismiss(animated: true, completion: nil)
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = barButton
        }else{
            let barButton = RLBarButtonItem.init(barType: .customButton(title: "SKIP", font: UIFont.spoqaHanSansBold(ofSize: 14), textColor: UIColor.dark))
            barButton.customButton?.rx.tap
                .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    self.viewModel.skip(userNo: MainInformation.shared.userNoAes!)
                        .subscribe(onNext: { success in
                            self.dismiss(animated: false) {
                                self.closeCallback?()
                            }
                        }, onError: { _ in
                            self.dismiss(animated: false) {
                                self.closeCallback?()
                            }
                        }).disposed(by: self.bag)
                })
                .disposed(by: bag)
            self.navigationItem.rightBarButtonItem = barButton
        }
        
        textFieldWrapView.addSubview(recoTextField)
        recoTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(textFieldWrapView)
        }

        recoTextField.textField.rx.text.orEmpty
            .subscribe(onNext: { str in
                self.recoTextField.text = str.uppercased()
            })
            .disposed(by: bag)
        
        viewModel = RecommendViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            fieldString: recoTextField.textObserver
                .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )

        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else if $0.resultCD == RLResultCD.E01000_NoMember.code {
                    return
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.dismiss(animated: false) {
                    self.closeCallback?()
                }
            })
            .disposed(by: bag)
        
        viewModel.validatedCode
            .bind(to: recoTextField.rx.setProfileTextResult)
            .disposed(by: bag)

        viewModel.crown
            .subscribe(onNext: { crown in
                let paragraph = NSMutableParagraphStyle()
                paragraph.alignment = .center
                let crownStr = "크라운 \(crown)개가 적립"
                let fullString = "신규 회원이 추천인 코드를 입력하면, 신규 회원과\n기존 회원 모두에게 \(crownStr)됩니다"
                let attributedString = NSMutableAttributedString(string: fullString, attributes: [
                    .font: UIFont.spoqaHanSansLight(ofSize: 16),
                    .foregroundColor: UIColor.gray50,
                    .paragraphStyle: paragraph
                ])
                let range = (fullString as NSString).range(of: crownStr)
                attributedString.addAttribute(.font, value: UIFont.spoqaHanSansBold(ofSize: 16), range: range)
                self.subTitleLabel.attributedText = attributedString
            })
            .disposed(by: bag)

        viewModel.nextButtonEnabled
            .bind(to: nextButton.rx.buttonEnableState)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                CompleteView.init(type: .reco).show {
                    self.dismiss(animated: false) {
                        MainInformation.shared.updateMyCrownNew()
                        self.closeCallback?()
                    }
                }
            })
            .disposed(by: bag)

        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height
                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 100)
                    self.view.layoutIfNeeded()
                }
                
                if keyboard.height == 0 {
                    self.nextButton.roundCorner()
                }else{
                    self.nextButton.squareCorner()
                }
            })
            .disposed(by: bag)
    }
    
    @IBAction func keyboardHideButton(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationControllerBottomLine()
    }
}
