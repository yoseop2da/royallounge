
//
//  RecommendViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class RecommendViewModel: BaseViewModel {
    
    let crown = BehaviorRelay<Int>.init(value: 0)
    let validatedCode = PublishSubject<TextFieldResult>.init()
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    
    let myRecoCd: BehaviorRelay<String> = BehaviorRelay.init(value: "")
    let doRecoCd: BehaviorRelay<String?> = BehaviorRelay.init(value: nil)
    let receiveCount: BehaviorRelay<Int> = BehaviorRelay.init(value: 0)
    
    let fieldString: BehaviorRelay<String> = BehaviorRelay.init(value: "")
    
    init(userNo: String, fieldString: Observable<String>, nextButtonTap: Observable<Void>) {
            
        super.init()
     
        fieldString.bind(to: self.fieldString).disposed(by: bag)
        
        Repository.shared.graphQl
            .getRecommendInfo(userNo: userNo)
            .subscribe(onNext: {
                self.myRecoCd.accept($0.recoCd)
                self.doRecoCd.accept($0.doRecoCd)
                self.receiveCount.accept($0.receiveCount)
                
                if $0.doRecoCd != nil {
                    self.nextButtonEnabled.accept(false)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
            
        Repository.shared.main
            .getCrown(userNo: userNo)
            .map{ $0?.recoDo ?? 0 }
            .subscribe(onNext: {
                self.crown.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)

        fieldString
            .map{ $0.length > 0 }
            .distinctUntilChanged()
            .subscribe(onNext: { enable in
                if let _ = self.doRecoCd.value {
                    self.nextButtonEnabled.accept(false)
                }else{
                    self.nextButtonEnabled.accept(enable)
                }
                self.validatedCode.on(.next(TextFieldResult.empty))
            })
            .disposed(by: bag)
        
        nextButtonTap.withLatestFrom(fieldString)
            .flatMapLatest { Repository.shared.main.addRecommend(userNo: userNo, recoCd: $0.trimmed) }
            .subscribe(onNext: {
                guard $0 else { return }
                self.nextSuccess.on(.next(true))
                
                if self.fieldString.value.length > 0 {
                    self.nextButtonEnabled.accept(false)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.validatedCode.on(.next(TextFieldResult.failed(message: custom.errorMessage)))
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                
            })
            .disposed(by: bag)
    }
    
    func skip(userNo: String) -> Observable<Bool> {
        return Repository.shared.graphQl
            .skipReco(userNo: userNo)
    }
}
