//
//  WelcomePopup.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/12.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

import BottomPopup

class WelcomePopup: BottomPopupViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var nextButtonWrapView: UIView!
    
    let bag = DisposeBag()
    var closeCallback: (()->Void)?
    let nextButton = RLNextButton.init(title: "안심하고 이용하기")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.dismiss(animated: true) {
                    self.closeCallback?()
                }
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getNickname(userNo: MainInformation.shared.userNoAes!)
            .map{"\($0)님\n환영합니다"}
            .bind(to: titleLabel.rx.text)
            .disposed(by: bag)
        
        let fullStr = """
로얄라운지에는
단 한명의 알바 회원도 없음을

1억원의 민·형사상 법률 책임을
지고 보증합니다.
"""
        let attributedString = NSMutableAttributedString(string: fullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 16),
            .foregroundColor: UIColor.dark,
            .kern: 0.0
        ])
        let range = (fullStr as NSString).range(of: "1억원의 민·형사상 법률 책임")
        attributedString.addAttributes([
            .font: UIFont.spoqaHanSansBold(ofSize: 16.0),
            .foregroundColor: UIColor.primary100
        ], range: range)
        informationLabel.attributedText = attributedString
    }
    
    override var popupHeight: CGFloat { return wrapView.frame.height }
//    override var popupTopCornerRadius: CGFloat { return CGFloat(10) }
//    override var popupPresentDuration: Double { return 0.5 }
//    override var popupDismissDuration: Double { return 0.5 }
//    override var popupShouldDismissInteractivelty: Bool { return true }
//    override var popupDimmingViewAlpha: CGFloat { return 0.5 }
}
