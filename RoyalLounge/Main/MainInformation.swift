//
//  MainInformation.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MainInformation: NSObject {

    static let shared = MainInformation()
    private let bag = DisposeBag()
    
    var myCrown = BehaviorRelay<Int>.init(value: 0)
    var gender: GenderType {
        if let step = memberStatus.value {
            return step.gender
        }
        return .m
    }
    
    var outTester: Bool {
        if let step = memberStatus.value, let userType = step.userType {
            return userType == .outTester
        }
        return false
    }
    
    var isSleep: Bool {
        if let step = memberStatus.value {
            return step.status == .sleep
        }
        return false
    }
    var userNoAes: String? {
        get { return RLUserDefault.shared.userNoAes }
        set (value) { RLUserDefault.shared.userNoAes = value }
    }
    
    var isJoinOrReject: Bool {
        if let step = memberStatus.value {
            return step.status == .join || step.status == .reject
        }
        return false
    }
    
    var isReady: Bool {
        if let step = memberStatus.value {
            return step.status == .ready
        }
        return false
    }
    
    var redDot = BehaviorRelay<RedDotResult>.init(value: RedDotResult.init(noticeCenterYn: false, noticeBBSYn: false, eventBBSYn: false, appVersionYn: false, photoRejectYn: false, specRejectYn: false, cardBoxYn: false, liveMatchYn: false))
    var memberStatus = BehaviorRelay<MemberStatus?>.init(value: nil)
    
    private let dateformatter = DateFormatter()
    private var liveMatchViewDate: String? {
        let send = RLUserDefault.shared.sendListRequestDate
        let receive = RLUserDefault.shared.receiveListRequestDate
        
        guard let _send = send, let _receive = receive else { return nil }
        
        dateformatter.locale = Locale.init(identifier: "ko_KR")
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let sendDate = dateformatter.date(from: _send),
           let receiveDate = dateformatter.date(from: _receive) {
            return sendDate.timeIntervalSince(receiveDate) > 0 ? _receive : _send
        }
        if let _send = send { return _send }
        if let _receive = receive { return _receive }
        return nil
    }
    
    override init() {
        super.init()
        updateMyCrownNew()
        updateMemberStatus()
        updateRedDot()
    }
    
    func canPayable(toPayCrown: Int) -> Bool {
        return myCrown.value >= toPayCrown
    }
    
    func updateMyCrownNew() {
        guard let userNo = self.userNoAes else { return }
        Repository.shared.graphQl
            .getMyCrown(userNo: userNo)
            .subscribe(onNext: { crown in
                self.myCrown.accept(crown)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func logout() {
        if let userNoAes = userNoAes {
            self.userNoAes = nil
            Repository.shared.main
                .logout(userNo: userNoAes)
                .subscribe(onNext: { success in
//                            guard success else { return }
                },onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    if custom.resultCD == RLResultCD.NETWORK_ERROR.code {
                        alertNETWORK_ERR(msg: custom.errorMessage) { }
                    }else if custom.resultCD == RLResultCD.E00000_Error.code {
                        alertERR_E0000Dialog()
                    }else{
                    
                    }
                    Debug.print("--------------[Fail] 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃")
                    // 로그인페이지로 이동.
                    Coordinator.shared.toSplash()
                    // 캐시 데이터 지우기
                    RLUserDefault.shared.removeCache()
                }, onCompleted: {
                    Debug.print("--------------[Complete] 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃 로그아웃")
                    // 로그인페이지로 이동.
                    Coordinator.shared.toSplash()
                    // 캐시 데이터 지우기
                    RLUserDefault.shared.removeCache()
                })
                .disposed(by: self.bag)
        }
    }

    func updateRedDot() {
        guard let userNo = self.userNoAes else { return }
        Repository.shared.main.redDot(userNo: userNo,
            noticeBbsViewDate: RLUserDefault.shared.noticeBbsRequestDate,
            eventBbsViewDate: RLUserDefault.shared.eventBbsRequestDate,
            liveMatchViewDate: self.liveMatchViewDate)
            .subscribe(onNext: { dot in
                self.redDot.accept(dot)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func updateMemberStatus() {
        guard let userNo = self.userNoAes else { return }
        Repository.shared.graphQl
            .getMemberStatus(userNo: userNo)
            .subscribe(onNext: { memberStatus in
                self.memberStatus.accept(memberStatus)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
}
