//
//  MainNoticePopup.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class MainNoticePopup: UIViewController {

    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var contentLabel: UILabel! // hidden됨 영역만 잡아줌
    @IBOutlet weak var contentTextView: UITextView!
    
    var input: (title: String, content: String, noticeNo: String)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wrapView.layer.cornerRadius = 10
        
        if let (title, content, _) = input {
            let attrFullStr = "\(title)\n\n\(content)"
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            let attributedString = NSMutableAttributedString(string: attrFullStr, attributes: [
                .font: UIFont.spoqaHanSansRegular(ofSize: 16),
                .foregroundColor: UIColor.dark,
                .kern: 0.0,
                .paragraphStyle: paragraph
                ])
            let range1 = (attrFullStr as NSString).range(of: title)
            attributedString.addAttributes([
                .font: UIFont.spoqaHanSansBold(ofSize: 16)], range: range1)
            contentLabel.attributedText = attributedString
            contentTextView.attributedText = attributedString
        }
    }
    
    @IBAction func leftButtonTouched(_ sender: Any) {
        if let (_, _, noticeNo) = input {
            RLUserDefault.shared.lastNoticeId = noticeNo
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func rightButtonTouched(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
