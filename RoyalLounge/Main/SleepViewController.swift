
//
//  SleepViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SleepViewController: MainViewController {

    @IBOutlet weak var sleepButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rlBrown238
        sleepButton.layer.cornerRadius = 6
    }
    
    @IBAction func buttonTouched(_ sender: Any) {
        Repository.shared.main
            .sleep(userNo: MainInformation.shared.userNoAes!, sleepYn: false)
            .filter{ $0 == true }
            .subscribe(onNext: { success in
                toast(message: "계정이 활성화 되었습니다", seconds: 1.5) {
                    Coordinator.shared.moveByUserStatus()
                }
            }, onError: { _ in
                
            })
        .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
