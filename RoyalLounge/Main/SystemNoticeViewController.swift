//
//  SystemNoticeViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SystemNoticeViewController: ClearNaviLoggedBaseViewController {
    
    var content: String = ""
    @IBOutlet weak var contentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rlBrown238
        
        contentLabel.text = content
    }
    
    @IBAction func buttonTouched(_ sender: Any) {
        RLMailSender.shared.sendEmail(.문의_미가입유저, finishCallback: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}
