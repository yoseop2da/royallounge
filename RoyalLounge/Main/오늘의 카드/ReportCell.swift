//
//  ReportCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }

}
