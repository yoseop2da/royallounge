
//
//  ReportData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

struct ReportSection {
    var items: [ReportModel]
}

extension ReportSection: SectionModelType {
    typealias Item = ReportModel
    init(original: ReportSection, items: [Item]) {
        self = original
        self.items = items
    }
}
