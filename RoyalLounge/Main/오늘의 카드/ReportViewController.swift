
//
//  ReportViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class ReportViewController: ClearNaviLoggedBaseViewController {
    enum ReportType {
        case report
        case question
    }
    typealias DataSource = RxTableViewSectionedReloadDataSource<ReportSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var viewModel: ReportViewModel!
    var info: (cardUserNo: String, userNickname: String)!
    
    var reportType: ReportType = .report
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, tableView, indexPath, item -> ReportCell in
            let cell: ReportCell = tableView.dequeueReusable(for: indexPath)
            cell.setData(title: item.name, description: item.description)
            return cell
        })

        return dataSource
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "이 회원 신고하기"
        
        let barButton = RLBarButtonItem.init(barType: .close)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItem = barButton
        
        mainTableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.mainTableView.deselectRow(at: indexPath, animated: true)
            }).disposed(by: bag)
        
        mainTableView.rx.modelSelected(ReportModel.self)
            .subscribe(onNext: { report in
                let myNickname = self.viewModel.nickname
                let userId = self.viewModel.userId
                let mbNo = self.viewModel.mbNo
                if self.reportType == .question {
                    RLMailSender.shared.sendEmail(.문의_가입유저(nickname: myNickname, userId: userId, mbNo: mbNo), finishCallback: { _ in
                        self.dismiss(animated: true, completion: nil)
                    })
                }else {
                    RLMailSender.shared.sendEmail(.신고하기(nickname: myNickname, userId: userId, mbNo: mbNo, cardNickname: self.info!.userNickname, reportNickname: report.name), finishCallback: { isSent in
                        if isSent {
                            // API Call
                            self.viewModel.sendEmailSuccess(reportedUserNo: self.info!.cardUserNo, reportGroupNo: report.reportGroupNo)
                            return
                        }
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                
            })
            .disposed(by: bag)
        viewModel = ReportViewModel(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            }).disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
    }
}

extension ReportViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
