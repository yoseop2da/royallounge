
//
//  NoticeViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class NoticeViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<NoticeSection>

    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            switch item {
            case .title(let title, let period, let isOpen, let idx):
                let cell: EventDetailTtitleCell = tableView.dequeueReusable(for: indexPath)
                cell.setItem(title: title, period: period, isOpen: isOpen)
                return cell
            case .image(let imageUrl):
                let cell: EventDetailImageCell = tableView.dequeueReusable(for: indexPath)
                cell.setImageUrl(imageUrl)
                return cell
            case .text(let content):
                let cell: EventDetailTextCell = tableView.dequeueReusable(for: indexPath)
                cell.setText(content)
                return cell
            }
        })
        return dataSource
    }
    
    var viewModel: NoticeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = NoticeViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView.rx
            .itemSelected
//            .throttle(.milliseconds(1000), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { indexPath in
                self.viewModel.open(indexPath: indexPath)
                self.mainTableView.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: bag)
        mainTableView.rx
            .setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "공지사항"
//        self.viewModel?.reloadData()
        hideNavigationControllerBottomLine()
    }
}

extension NoticeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellItem = viewModel.getItem(indexPath: indexPath)
        if case .image(let imageUrl) = cellItem {
            var imageHeight: CGFloat = 0.0
            let leftRightPadding: CGFloat = 32.0
            if let url = URL.init(string: imageUrl),
                let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil),
                let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                if let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as? Int,
                    let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as? Int {
                    imageHeight = (UIScreen.width - (leftRightPadding*2)) * (CGFloat(pixelHeight) / CGFloat(pixelWidth))
                    return imageHeight
                }
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
