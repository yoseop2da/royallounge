//
//  NoticeViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class NoticeViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[NoticeSection]>.init(value: [])
    private let noticeListResult = BehaviorRelay<NoticeListResult?>.init(value: nil)
    
    var userNo: String!
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        reloadData()
        
        noticeListResult
            .filter{ $0 != nil }
            .subscribe(onNext: { result in
                var cellTypeList: [EventDetailCellType] = []
                result!.list.enumerated().forEach{ idx, value in
                    cellTypeList.append(.title(title: value.title, period: value.regDate, isOpen: value.isOpen, idx: idx))
                    if value.isOpen {
                        if value.noticeType == "IMAGE" {
                            if let imageURL = value.imageURL {
                                cellTypeList.append(.image(imageUrl: imageURL))
                            }
                        }else if value.noticeType == "IMAGE_BOTTOM" {
                            cellTypeList.append(.text(content: value.content))
                            if let imageURL = value.imageURL {
                                cellTypeList.append(.image(imageUrl: imageURL))
                            }
                        }else if value.noticeType == "IMAGE_TOP" {
                            if let imageURL = value.imageURL {
                                cellTypeList.append(.image(imageUrl: imageURL))
                            }
                            cellTypeList.append(.text(content: value.content))
                        }else if value.noticeType == "TEXT" {
                            cellTypeList.append(.text(content: value.content))
                        }
                    }
                }
                self.sections.accept([NoticeSection.init(hasNext: result!.hasNext, items: cellTypeList)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func reloadData() {
        Repository.shared.main
            .getNoticeList(userNo: self.userNo, lastNoticeNo: nil)
            .subscribe(onNext: {
                self.noticeListResult.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func getItem(indexPath: IndexPath) -> EventDetailCellType {
        return sections.value[indexPath.section].items[indexPath.row]
    }
    
    func open(indexPath: IndexPath) {
        let cellItem = self.getItem(indexPath: indexPath)
        guard case .title(_,_,_,let changeIdx) = cellItem else { return }
        guard var newResult = noticeListResult.value else { return }
        var list = newResult.list
        list.enumerated().forEach { idx, notice in
            var new = notice
            if idx == changeIdx {
                new.isOpen.toggle()
            }else{
                new.isOpen = false
            }
            list[idx] = new
        }
        newResult.list = list
        noticeListResult.accept(newResult)
    }
}


// Tableview Cell
struct NoticeSection {
    var hasNext: Bool = false
    var items: [EventDetailCellType]
}

extension NoticeSection: SectionModelType {
    init(original: NoticeSection, items: [EventDetailCellType]) {
        self = original
        self.items = items
    }
}
