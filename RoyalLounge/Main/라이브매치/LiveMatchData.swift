//
//  LiveMatchData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxDataSources


struct LiveMatchHistorySection {
    var header: String
    var receiveNewCardYn: Bool
    var type: String
    var sendNewCardYn: Bool
    
    var items: [LiveMatchListCardModel]
    var hasNext: Bool
}


extension LiveMatchHistorySection: SectionModelType {
    init(original: LiveMatchHistorySection, items: [LiveMatchListCardModel]) {
        self = original
        self.items = items
    }
}

