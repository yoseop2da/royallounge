//
//  LiveMatchHistoryCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class LiveMatchHistoryCell: UICollectionViewCell {

    @IBOutlet weak var whiteWrapView: UIView!
    @IBOutlet weak var specImageView: UIImageView!
    @IBOutlet weak var specCountLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileGradientView: UIImageView!
    @IBOutlet weak var dDayWrapView: UIView!
    @IBOutlet weak var dDayLabel: UILabel!
    
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var photoEvalView: UIView!
    
    @IBOutlet weak var gradientView: UIView!
    
    var item: LiveMatchListCardModel?
    var moveCardDetailAction: (()->Void)?
    var openCardAction: (()->Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        whiteWrapView.layer.cornerRadius = 6.0
        whiteWrapView.clipsToBounds = true
        whiteWrapView.setBorder(width: 1.0, color: .gray10)
        dDayWrapView.layer.cornerRadius = 4.0
        photoEvalView.layer.cornerRadius = 4.0
        
        dDayWrapView.setShadow(radius: 0.5, color: .lightGray)
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.backgroundColor = .gray25
        
        self.gradientView.isHidden = true
        
        nicknameLabel.text = ""
    }
    
    func setData(_ card: LiveMatchListCardModel, focusType: LiveMatchHistoryViewModel.FocusType) {
        item = card

        self.specImageView.isHidden = card.specCount == 0
        self.specCountLabel.isHidden = card.specCount == 0
        self.specCountLabel.text = "\(card.specCount)"
        
        if card.matchNo != nil {
            nicknameLabel.text = card.nickname
            
            whiteWrapView.setBorder(width: 2.0, color: .brown100)
            self.profileImageView.setImage(urlString: card.photoURL, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"))
        }else{
            nicknameLabel.text = card.title
            
            whiteWrapView.setBorder(width: 1.0, color: .gray10)
            if focusType == .receive {
                self.profileImageView.setBlurImage(urlString: card.photoURL, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"))
            }else{
                self.profileImageView.setImage(urlString: card.photoURL, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"))
            }
            
        }
        
        dDayLabel.text = "D-\(card.dDay)"
        dDayWrapView.isHidden = card.dDay > 2
        
        photoEvalView.isHidden = !card.photoEvalYn
        
    }
    
    @IBAction func cardTouched(_ sender: Any) {
        if let card = self.item {
            if card.matchNo != nil {
                whiteWrapView.touchAnimation{
                    self.moveCardDetailAction?()
                }
            }else{
                self.openCardAction?()
            }
        }
    }

    func setBaseInfo(urlString: String, openYn: Bool, specCount: Int, dDay: Int) {
        
    }
}
