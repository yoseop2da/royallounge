//
//  LiveMatchHistoryHeader.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Lottie

class LiveMatchHistoryHeader: UICollectionReusableView {
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var info1WrapView: UIView!
    @IBOutlet weak var info2WrapView: UIView!
    @IBOutlet weak var info3WrapView: UIView!
    @IBOutlet weak var info1Label: UILabel!
    @IBOutlet weak var info2Label: UILabel!
    @IBOutlet weak var info3Label: UILabel!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var sendTabButton: UIButton!
    @IBOutlet weak var receiveTabButton: UIButton!
    
    @IBOutlet weak var sendTabRedDot: RedDotView!
    @IBOutlet weak var receiveTabRedDot: RedDotView!
    
    @IBOutlet weak var emptyViewHeight: NSLayoutConstraint!
    
    var findNewAction: (()->Void)?
    var receiveTabAction: (()->Void)?
    var sendTabAction: (()->Void)?
    private var lottiView: AnimationView?
    private var liveMatchButton: LiveMatchButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gradientView.backgroundColor = .clear
//        gradientView.setHistoryResultGradient()
        info1WrapView.layer.cornerRadius = 6.0
        info2WrapView.layer.cornerRadius = 6.0
        info3WrapView.layer.cornerRadius = 6.0
        buttonView.backgroundColor = .clear
        
        gradientView.removeAllSubView()
        
        let lottiView = AnimationView.init(name: "03_motion_background")
        self.lottiView = lottiView
        let margin: CGFloat = 40
        lottiView.frame = CGRect.init(x: -margin, y: -margin, width: gradientView.bounds.width + (margin * 2), height: gradientView.bounds.height + (margin * 2))
        lottiView.loopMode = .loop
        gradientView.addSubview(lottiView)
        lottiView.play()
        if liveMatchButton == nil {
            liveMatchButton = LiveMatchButton.init()
            buttonView.addSubview(liveMatchButton)
            liveMatchButton.snp.makeConstraints {
                $0.top.bottom.leading.trailing.equalToSuperview()
            }
        }
        
        liveMatchButton.touchAction = {
            self.findNewAction?()
        }
        emptyView.isHidden = true
    }
    
    func setData(
        crown: Int,
        evalCount: Int?,
        info2: String,
        info3: String,
        gender: GenderType,
        progressYn: Bool,
        focusType: LiveMatchHistoryViewModel.FocusType,
        sendRedDotYn: Bool,
        receiveRedDotYn: Bool) {
        
        liveMatchButton.crown = crown
        if progressYn {
            liveMatchButton.setFindingButton()
        }else{
            liveMatchButton.setNormalButton(text: evalCount == nil ? "나에게 호감있는 이성찾기" : "더 찾아보기")
        }
        
        if let _evalCount = evalCount {
            info1WrapView.isHidden = false
            let info1fullStr = "총 \(_evalCount)명의 이성 참여"
            let info1Attr = NSMutableAttributedString.init(string: info1fullStr, attributes: [.font : UIFont.spoqaHanSansLight(ofSize: 16.0)])
            info1Attr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 16.0)], range: (info1fullStr as NSString).range(of: "\(_evalCount)"))
            info1Label.attributedText = info1Attr
            
            let info2fullStr = "평균 \(info2)"
            let info2Attr = NSMutableAttributedString.init(string: info2fullStr, attributes: [.font : UIFont.spoqaHanSansLight(ofSize: 16.0)])
            info2Attr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 16.0)], range: (info2fullStr as NSString).range(of: "\(info2)"))
            info2Label.attributedText = info2Attr
            
            let info3fullStr = gender == .m ? "\(info3) 직업" : "\(info3) 스펙 보유"
            let info3Attr = NSMutableAttributedString.init(string: info3fullStr, attributes: [.font : UIFont.spoqaHanSansLight(ofSize: 16.0)])
            info3Attr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 16.0)], range: (info3fullStr as NSString).range(of: "\(info3)"))
            info3Label.attributedText = info3Attr
        } else {
            info1WrapView.isHidden = true
            
            let info2fullStr = "나에게 호감있는 이성회원을"
            let info2Attr = NSMutableAttributedString.init(string: info2fullStr, attributes: [.font : UIFont.spoqaHanSansLight(ofSize: 16.0)])
            info2Attr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 16.0)], range: (info2fullStr as NSString).range(of: "나에게 호감"))
            info2Attr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 16.0)], range: (info2fullStr as NSString).range(of: "이성회원"))
            info2Label.attributedText = info2Attr
            
            let info3fullStr = "실시간으로 찾아드려요!"
            let info3Attr = NSMutableAttributedString.init(string: info3fullStr, attributes: [.font : UIFont.spoqaHanSansLight(ofSize: 16.0)])
            info3Attr.addAttributes([.font : UIFont.spoqaHanSansBold(ofSize: 16.0)], range: (info3fullStr as NSString).range(of: "실시간"))
            info3Label.attributedText = info3Attr
        }
        
        let isSendTab = focusType == .send
        self.sendTabButton.titleLabel?.font = isSendTab ? UIFont.spoqaHanSansBold(ofSize: 16) : UIFont.spoqaHanSansRegular(ofSize: 16)
        self.receiveTabButton.titleLabel?.font = isSendTab ? UIFont.spoqaHanSansRegular(ofSize: 16) : UIFont.spoqaHanSansBold(ofSize: 16)
        self.sendTabButton.setTitleColor(isSendTab ? .primary100 : UIColor.gray50, for: .normal)
        self.receiveTabButton.setTitleColor(isSendTab ? UIColor.gray50 : .primary100, for: .normal)
        
        sendTabRedDot.isHidden = !sendRedDotYn
        receiveTabRedDot.isHidden = !receiveRedDotYn
    }
    
    func playLotti() {
        // 그라데이션
        if let lotti = self.lottiView {
            if !lotti.isAnimationPlaying {
                lotti.play()
            }
        }
        // 찾는중
        liveMatchButton.findingLottiPlay()
    }
    
    func displayEmptyLabel(show: Bool, isSendTab: Bool) {
        if UIScreen.underSE {
            emptyViewHeight.constant = 100.0
        }else{
            emptyViewHeight.constant = 200.0
        }
        if show {
            emptyView.isHidden = false
        }else{
            emptyView.isHidden = true
        }
        
        if isSendTab {
            emptyLabel.text = "라이브 매칭에서 호감가는 회원에게\n하트를 눌러보세요"
        }else{
            let fullStr = "[나에게 호감있는 이성찾기]로\n나를 좋아하는 이성을 찾아보세요"
            let attr = NSMutableAttributedString.init(string: fullStr)
            attr.addAttributes([NSAttributedString.Key.font : UIFont.spoqaHanSansBold(ofSize: 14)], range: (fullStr as NSString).range(of: "[나에게 호감있는 이성찾기]"))
            emptyLabel.attributedText = attr
        }
    }
    
    @IBAction func sendTabButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.sendTabAction?()
        }
    }
    
    @IBAction func receiveTabButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.receiveTabAction?()
        }
    }
}


class LiveMatchHistoryFooter: UICollectionReusableView {
     
}
