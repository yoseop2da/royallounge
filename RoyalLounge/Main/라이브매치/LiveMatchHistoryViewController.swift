//
//  LiveMatchHistoryViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class LiveMatchHistoryViewController: ClearNaviLoggedBaseViewController {
    // Rx걷어내기, 더보기시 화면이 위로 포커싱되는 문제 발생.
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<LiveMatchHistorySection>
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topHeaderView: UIView!
    
    @IBOutlet weak var sendTabButton: UIButton!
    @IBOutlet weak var receiveTabButton: UIButton!

    @IBOutlet weak var sendRedDot: RedDotView!
    @IBOutlet weak var receiveRedDot: RedDotView!
    
    private var viewModel: LiveMatchHistoryViewModel!
    private var refreshControl: UIRefreshControl!
    
    var outCallback: ( () -> Void )?
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, collectionView, indexPath, item -> LiveMatchHistoryCell in
            let cell: LiveMatchHistoryCell = collectionView.dequeueReusable(for: indexPath)
            cell.setData(item, focusType: self.viewModel.focusType.value)
            cell.moveCardDetailAction = {
                guard let matchNo = item.matchNo else { return }
                let vc = RLStoryboard.todayCard.cardDetailViewController(matchNo: matchNo) { refreshType in
                    switch refreshType {
                    case .deleted:
                        // 카테고리 리프래시
                        self.viewModel.reLoadData()
                        MainInformation.shared.updateMyCrownNew()
                    default: ()
                    }
                }
                self.push(vc)
            }
            cell.openCardAction = {
                if item.photoEvalYn {
                    toast(message: "심사승인이 완료되면 열람할 수 있어요", seconds: 1.5)
                    return
                }
                let needCrown = self.viewModel.focusType.value == .send ? self.viewModel.needCrownSend : self.viewModel.needCrownReceive
                if needCrown.canPayable() {
                    RLPopup.shared.showNormal(parent: self, title: "호감 카드 즉시 확인하기", description: "크라운 \(needCrown)개가 차감됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", rightAction: {
                        self.viewModel.openCard(liveMatchHistNo: item.liveMatchHistNo)
                    })
                }else{
                    RLPopup.shared.showMoveStore(needCrown: needCrown, reasonStr: "호감 카드를 즉시 확인하기 위해", action: {
                        let vc = RLStoryboard.main.storeViewController()
                        self.push(vc)
                    })
                }
            }
            
            return cell
        })
        
        dataSource.configureSupplementaryView = { dataSource, collectionView, kind, indexPath in
            if kind == UICollectionView.elementKindSectionHeader {
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LiveMatchHistoryHeader.className, for: indexPath) as! LiveMatchHistoryHeader
                var crown: Int = 10
                var progressYn = false
                var evalCount: Int?
                var info2Text = ""
                var info3Text = ""
                if let result = self.viewModel.liveMatchResult.value {
                    // 크라운이 nil인 케이스는 진행중인 케이스.
                    if let _crown = result.crown { crown = _crown }
                    progressYn = result.progressYn
                    if let stats = result.stats {
                        evalCount = stats.evalCount
                        info2Text = "\(stats.text1)"
                        info3Text = "\(stats.text2)"
                    }
                }
                
                var sendRedDotYn = false
                var receiveRedDotYn = false
                if self.viewModel.redDot.value.0 {
                    if self.viewModel.focusType.value == .send {
                        sendRedDotYn = false
                    }else{
                        sendRedDotYn = true
                    }
                }
                if self.viewModel.redDot.value.1 {
                    if self.viewModel.focusType.value == .receive {
                        receiveRedDotYn = false
                    }else{
                        receiveRedDotYn = true
                    }
                }

                header.setData(crown: crown, evalCount: evalCount, info2: info2Text, info3: info3Text, gender: MainInformation.shared.gender, progressYn: progressYn, focusType: self.viewModel.focusType.value, sendRedDotYn: sendRedDotYn, receiveRedDotYn: receiveRedDotYn)
                
                header.findNewAction = {
                    if crown == 0 {
                        self.viewModel.findNewResult()
                        return
                    }
                    if crown.canPayable() {
                        RLPopup.shared.showNormal(description: "크라운 \(crown)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                            self.viewModel.findNewResult()
                        })
                    }else{
                        RLPopup.shared.showMoveStore {
                            // 스토어 이동
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        }
                    }
                }
                
                header.sendTabAction = {
                    if self.viewModel.focusType.value == .send { return }
                    self.viewModel.setCollectionViewData(with: .send)
                }
                header.receiveTabAction = {
                    if self.viewModel.focusType.value == .receive { return }
                    self.viewModel.setCollectionViewData(with: .receive)
                }
                
                if self.viewModel.focusType.value == .send {
                    let count = self.viewModel.sendListResult.value?.list.count ?? 0
                    print("by 빈화면 노출여부 : \(count == 0)")
                    header.displayEmptyLabel(show: count == 0, isSendTab: true)
//                        emptyView.isHidden = count > 0
                }else{
                    let count = self.viewModel.receiveListResult.value?.list.count ?? 0
                    print("for 빈화면 노출여부 : \(count == 0)")
                    header.displayEmptyLabel(show: count == 0, isSendTab: false)
//                    header.emptyView.isHidden = count > 0
                }
                return header
            }else{
                let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LiveMatchHistoryFooter.className, for: indexPath) as! LiveMatchHistoryFooter
                return footer
            }
        }
        return dataSource
    }
    
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.topHeaderView.alpha = 0.0
        self.swipeBackAnyWhere()
        
        refreshControl = UIRefreshControl()
        collectionView.refreshControl = refreshControl
        refreshControl.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: refreshControl!, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: refreshControl!, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 60).isActive = true
        
        refreshControl
            .rx.controlEvent(UIControl.Event.valueChanged)
            .subscribe(onNext: { [weak self] in
                self?.refreshControl.endRefreshing()
                self?.viewModel?.reLoadData()
                self?.viewModel?.loadResult()
                
            })
            .disposed(by: bag)
        
        let flowLayout = DragAndDropFlowLayout()
        let count = 3
        let headerHeight = CGFloat(477) + UIScreen.statusBarHeight // + UIScreen.safeAreaTop
        let padding = CGFloat(20)
        let innerPadding = UIScreen.underSE ? CGFloat(5) : CGFloat(12) //CGFloat(3 * count)
        let linePadding = CGFloat(12)
        let width = ((UIScreen.width - (CGFloat(count-1) * innerPadding) - (padding * 2)) / CGFloat(count)) - 1.0
        flowLayout.headerReferenceSize = CGSize(width: collectionView.frame.width, height: headerHeight)/*HEADER HEIGHT SIZE*/
        flowLayout.footerReferenceSize = CGSize(width: collectionView.frame.width, height: 60)/*HEADER HEIGHT SIZE*/
        flowLayout.itemSize = CGSize(width: width, height: width + 22)
        flowLayout.minimumLineSpacing = linePadding
        flowLayout.minimumInteritemSpacing = innerPadding
        flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: padding, bottom: 0, right: padding)
        collectionView.collectionViewLayout = flowLayout
        
        collectionView.contentInset = .zero
        
        backButton.rx.tap.rxTouchAnimation(button: backButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.pushBack(animated: true)
            })
            .disposed(by: bag)
        
        viewModel = LiveMatchHistoryViewModel.init(userNo: MainInformation.shared.userNoAes!)
        

        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.redDot
            .subscribe(onNext: { sendDot, receiveDot in
                self.sendRedDot.isHidden = true
                self.receiveRedDot.isHidden = true
                
                if sendDot {
                    if self.viewModel.focusType.value == .send {
                        self.sendRedDot.isHidden = true
                    }else{
                        self.sendRedDot.isHidden = false
                    }
                }
                if receiveDot {
                    if self.viewModel.focusType.value == .receive {
                        self.receiveRedDot.isHidden = true
                    }else{
                        self.receiveRedDot.isHidden = false
                    }
                }
            }).disposed(by: bag)
        
        viewModel.focusType
            .subscribe(onNext: { type in
                let isSendTab = type == .send
                self.sendTabButton.titleLabel?.font = isSendTab ? UIFont.spoqaHanSansBold(ofSize: 16) : UIFont.spoqaHanSansRegular(ofSize: 16)
                self.receiveTabButton.titleLabel?.font = isSendTab ? UIFont.spoqaHanSansRegular(ofSize: 16) : UIFont.spoqaHanSansBold(ofSize: 16)
                self.sendTabButton.setTitleColor(isSendTab ? .primary100 : UIColor.gray50, for: .normal)
                self.receiveTabButton.setTitleColor(isSendTab ? UIColor.gray50 : .primary100, for: .normal)
                self.viewModel.updateRedDot()
            })
            .disposed(by: bag)
        
        viewModel.moveCardDetail
            .subscribe(onNext: { matchNo in
                let vc = RLStoryboard.todayCard.cardDetailViewController(matchNo: matchNo) { refreshType in
                    switch refreshType {
                    case .deleted:
                        // 카테고리 리프래시
                        self.viewModel.reLoadData()
                    default: ()
                    }
                }
                self.push(vc)
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        collectionView
            .rx.contentOffset
            .observeOn(MainScheduler.asyncInstance)
            .bind { contentOffset in
                let offsetY = contentOffset.y
                let add: CGFloat = UIScreen.hasNotch ? (77.0/*기존 호감리스트height*/ - 51.0/*상단으로 온경우 호감리스트height*/) : 0
                let offsetPosition = 344 + UIScreen.safeAreaTop - add
                if offsetY >= offsetPosition {
                    self.topHeaderView.alpha = 1.0
                }else{
                    self.topHeaderView.alpha = 0.0
                }
                
                // load more
                let contentHeight = self.collectionView.contentSize.height
                if offsetY > 0 && ((offsetY > contentHeight - self.collectionView.frame.height * 4)) && !self.viewModel.isLoading {
                    self.viewModel.loadMoreData()
                }}
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if let header = collectionView.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath.init(row: 0, section: 0)) as? LiveMatchHistoryHeader {
            header.playLotti()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.outCallback?()
    }
    
    @IBAction func sendTabButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            if self.viewModel.focusType.value == .send { return }
            self.viewModel.setCollectionViewData(with: .send)
        }
    }
    
    @IBAction func receiveTabButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            if self.viewModel.focusType.value == .receive { return }
            self.viewModel.setCollectionViewData(with: .receive)
        }
    }
    
}
//
//extension LiveMatchHistoryViewController: UIScrollViewDelegate, UICollectionViewDelegate {
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
////         + UIScreen.statusBarHeight
//        let add: CGFloat = UIScreen.hasNotch ? (77.0/*기존 호감리스트height*/ - 51.0/*상단으로 온경우 호감리스트height*/) : 0
//        let offsetPosition = 344 + UIScreen.safeAreaTop - add
//        if scrollView.contentOffset.y >= offsetPosition {
//            self.topHeaderView.alpha = 1.0
//        }else{
//            self.topHeaderView.alpha = 0.0
//        }
//
//        // load more
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//
//        if offsetY > 0 && ((offsetY > contentHeight - scrollView.frame.height * 4)) && !self.viewModel.isLoading {
//            self.viewModel.loadMoreData()
//        }
//    }
//}

class RedDotView: UIView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.layer.cornerRadius = 2.5
    }
    
}
