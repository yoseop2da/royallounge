
//
//  LiveMatchHistoryViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class LiveMatchHistoryViewModel: BaseViewModel {
    enum FocusType {
        case send
        case receive
    }
    var needCrownSend: Int = 5
    var needCrownReceive: Int = 5
    var focusType = BehaviorRelay<FocusType>.init(value: .send)
    
    var redDot = BehaviorRelay<(Bool, Bool)>.init(value: (false, false))
    var moveCardDetail: PublishSubject<String> = PublishSubject<String>.init()
    
    var liveMatchResult = BehaviorRelay<LiveMatchResult?>.init(value: nil)
    var sendListResult = BehaviorRelay<LiveMatchListResult?>.init(value: nil)
    var receiveListResult = BehaviorRelay<LiveMatchListResult?>.init(value: nil)
    var sections = BehaviorRelay<[LiveMatchHistorySection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let nextButtonEnabled = BehaviorRelay<Bool>.init(value: false)
    let nextSuccess = PublishSubject<Bool>.init()
    private var userNo: String!
    
    var isLoading = false
    
    private let dateformatter = DateFormatter()
    private var viewDate: String? {
        let send = RLUserDefault.shared.sendListRequestDate
        let receive = RLUserDefault.shared.receiveListRequestDate
        
        guard let _send = send, let _receive = receive else { return nil }
        
        dateformatter.locale = Locale.init(identifier: "ko_KR")
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let sendDate = dateformatter.date(from: _send),
            let receiveDate = dateformatter.date(from: _receive) {
            let gap = sendDate.timeIntervalSince(receiveDate)
            if gap > 0 {
                return _receive
            }else{
                return _send
            }
        }
        if let _send = send { return _send }
        if let _receive = receive { return _receive }
        return nil
    }
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        // result
        loadResult()
        
        // list
        if MainInformation.shared.gender == .m {
            self.focusType.accept(.send)
        }else{
            self.focusType.accept(.receive)
        }
        self.reLoadData()
        
        Observable.combineLatest(liveMatchResult, sendListResult, receiveListResult)
            .subscribe(onNext: { result, send, receive in
                if send == nil && receive == nil {
                    return
                }
                self.setCollectionViewData(with: nil)
            })
            .disposed(by: bag)
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .filter{$0 != nil}
            .subscribe(onNext: { crown in
                self.needCrownSend = crown!.liveMatchSendLikeDetail
                self.needCrownReceive = crown!.liveMatchReceiveLikeDetail
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    /// 내부에 가지고있는 redDot 정보 업데이트해주기
    func updateRedDot() {
        if self.focusType.value == .send {
            var newSendListResult = self.sendListResult.value
            newSendListResult?.sendNewCardYn = false
            self.updateSendListResult(newSendListResult)
            
            var newReceiveListResult = self.receiveListResult.value
            newReceiveListResult?.sendNewCardYn = false
            self.updateReceiveListResult(newReceiveListResult)
        }else{
            var newSendListResult = self.sendListResult.value
            newSendListResult?.sendNewCardYn = false
            self.updateSendListResult(newSendListResult)
            
            var newReceiveListResult = self.receiveListResult.value
            newReceiveListResult?.sendNewCardYn = false
            self.updateReceiveListResult(newReceiveListResult)
        }
    }
    
    /// 내가 호감보인 카드 결과 업데이트 & redDot 업데이트
    func updateSendListResult(_ result: LiveMatchListResult?) {
        self.redDot.accept((result?.sendNewCardYn ?? false, result?.receiveNewCardYn ?? false))
        self.sendListResult.accept(result)
    }
    
    /// 나에게 호감보인 카드 결과 업데이트 & redDot 업데이트
    func updateReceiveListResult(_ result: LiveMatchListResult?) {
        self.redDot.accept((result?.sendNewCardYn ?? false, result?.receiveNewCardYn ?? false))
        self.receiveListResult.accept(result)
    }
    
    /// 더찾기 결과 조회
    func loadResult() {
        Repository.shared.main
            .getLiveMatchResult(userNo: userNo)
            .subscribe(onNext: { result in
                self.liveMatchResult.accept(result)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    /// 화면 카드 데이터 새로 불러오기
    func reLoadData() {
        if self.focusType.value == .send {
            self.loadSendList(lastLiveMatchHistNo: nil)
        }else{
            self.loadReceiveList(lastLiveMatchHistNo: nil)
        }
    }
    
    /// 더보기 구현
    func loadMoreData() {
        guard !self.isLoading else { return }
        if self.focusType.value == .send {
            let sendResult = self.sendListResult.value
            if let hasNext = sendResult?.hasNext, hasNext == true {
                self.isLoading = true
                self.loadSendList(lastLiveMatchHistNo: sendResult?.list.last?.liveMatchHistNo)
            }
        }else{
            let receiveResult = self.receiveListResult.value
            if let hasNext = receiveResult?.hasNext, hasNext == true {
                self.isLoading = true
                self.loadReceiveList(lastLiveMatchHistNo: receiveResult?.list.last?.liveMatchHistNo)
            }
            
        }
    }
    
    /// 호감카드 더 찾기 신청
    func findNewResult() {
        Repository.shared.main
            .findLiveMatchNewResult(userNo: userNo)
            .subscribe(onNext: { _userNo in
                var new = self.liveMatchResult.value
                new?.progressYn = true
                self.liveMatchResult.accept(new)
                MainInformation.shared.updateMyCrownNew()
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
    }
    
    /// 카드 오픈하기
    /// - Parameter liveMatchHistNo: 서버로부터 받은 정보 전달
    func openCard(liveMatchHistNo: String) {
        mainAppDelegate.showBlockView()
        
        let cardType = focusType.value == .send ? "LIVE_MATCH_SEND_LIKE_DETAIL" : "LIVE_MATCH_RECEIVE_LIKE_DETAIL"
        Repository.shared.main
            .openLiveMatchCard(userNo: userNo, liveMatchHistNo: liveMatchHistNo, cardType: cardType)
            .subscribe(onNext: { (matchNo, errMsg) in
                mainAppDelegate.hideBlockView()
                if let msg = errMsg {
                    toast(message: msg, seconds: 1.5)
                    return
                }
                if let _matchNo = matchNo {
                    MainInformation.shared.updateMyCrownNew()
                    toast(message: "오늘의 카드에 추가되었습니다", seconds: 1.5)
                    self.moveCardDetail.on(.next(_matchNo))
                    
                    if self.focusType.value == .send {
                        var new = self.sendListResult.value
                        if let idx = new?.list.firstIndex(where: { $0.liveMatchHistNo == liveMatchHistNo}) {
                            new?.list[idx].matchNo = _matchNo
                            self.updateSendListResult(new)
                        }
                    }else{
                        var new = self.receiveListResult.value
                        if let idx = new?.list.firstIndex(where: { $0.liveMatchHistNo == liveMatchHistNo}) {
                            new?.list[idx].matchNo = _matchNo
                            self.updateReceiveListResult(new)
                        }
                    }
                }
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
    }
    
    
    /// 컬렉션뷰에 focustype에 맞는 데이터 넣어주기
    /// - Parameter focusType: 화면 탭시에 호출해주면서 focusType 업데이트
    func setCollectionViewData(with focusType: FocusType?) {
        if let type = focusType {
            self.focusType.accept(type)
        }
        if self.focusType.value == .send {
            setSendListData()
        }else{
            setReceiveData()
        }
    }
}

extension LiveMatchHistoryViewModel {
    
    private func loadSendList(lastLiveMatchHistNo: String?) {
        if let _lastLiveMatchHistNo = lastLiveMatchHistNo {
            Repository.shared.main
                .getLiveMatchList(userNo: userNo, lastLiveMatchHistNo: _lastLiveMatchHistNo, cardPagingType: "LIVE_MATCH_SEND", viewDate: self.viewDate)
                .subscribe(onNext: { result in
                    delay(0.5) { self.isLoading = false }
                    self.updateViewDate(isSend: true)
                    var new = self.sendListResult.value
                    new?.hasNext = result.hasNext
                    new?.sendNewCardYn = result.sendNewCardYn
                    new?.receiveNewCardYn = result.receiveNewCardYn
                    new?.list.append(contentsOf: result.list)
                    self.updateSendListResult(new)
                }, onError: {
                    delay(0.5) { self.isLoading = false }
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
        }else{
            Repository.shared.main
                .getLiveMatchList(userNo: userNo, lastLiveMatchHistNo: nil, cardPagingType: "LIVE_MATCH_SEND", viewDate: self.viewDate)
                .subscribe(onNext: { result in
                    self.updateViewDate(isSend: true)
                    MainInformation.shared.updateRedDot()
                    self.updateSendListResult(result)
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

                })
                .disposed(by: bag)
        }
    }
    
    private func loadReceiveList(lastLiveMatchHistNo: String?) {
        if let _lastLiveMatchHistNo = lastLiveMatchHistNo {
            Repository.shared.main
                .getLiveMatchList(userNo: userNo, lastLiveMatchHistNo: _lastLiveMatchHistNo, cardPagingType: "LIVE_MATCH_RECEIVE", viewDate: self.viewDate)
                .subscribe(onNext: { result in
                    delay(0.5) { self.isLoading = false }
                    self.updateViewDate(isSend: false)
                    var new = self.receiveListResult.value
                    new?.hasNext = result.hasNext
                    new?.sendNewCardYn = result.sendNewCardYn
                    new?.receiveNewCardYn = result.receiveNewCardYn
                    new?.list.append(contentsOf: result.list)
                    self.updateReceiveListResult(new)
                }, onError: {
                    delay(0.5) { self.isLoading = false }
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

                })
                .disposed(by: bag)
        }else{
            Repository.shared.main
                .getLiveMatchList(userNo: userNo, lastLiveMatchHistNo: nil, cardPagingType: "LIVE_MATCH_RECEIVE", viewDate: self.viewDate)
                .subscribe(onNext: { result in
                    self.updateViewDate(isSend: false)
                    MainInformation.shared.updateRedDot()
                    self.updateReceiveListResult(result)
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

                })
                .disposed(by: bag)
        }
    }
    
    /// sections에 Send List 데이터 넣어주기
    private func setSendListData() {
        let result = self.sendListResult.value
        
        if result == nil {
            loadSendList(lastLiveMatchHistNo: nil)
            return
        }

        self.sections.accept([
            LiveMatchHistorySection.init(
                header: "호감 카드 리스트",
                receiveNewCardYn: result?.receiveNewCardYn ?? false,
                type: result?.type ?? "",
                sendNewCardYn: result?.sendNewCardYn ?? false,
                items: result?.list ?? [],
                hasNext: result?.hasNext ?? false)
        ])
    }
    
    /// sections에 Receive List 데이터 넣어주기
    private func setReceiveData() {
        let result = self.receiveListResult.value

        if result == nil {
            loadReceiveList(lastLiveMatchHistNo: nil)
            return
        }
        
        self.sections.accept([
            LiveMatchHistorySection.init(
            header: "호감 카드 리스트",
            receiveNewCardYn: result?.receiveNewCardYn ?? false,
            type: result?.type ?? "",
            sendNewCardYn: result?.sendNewCardYn ?? false,
            items: result?.list ?? [],
            hasNext: result?.hasNext ?? false)
        ])
    }
    
    private func updateViewDate(isSend: Bool) {
        if isSend {
            RLUserDefault.shared.sendListRequestDate = Date.requestViewDate()
        }else{
            RLUserDefault.shared.receiveListRequestDate = Date.requestViewDate()
        }
    }
}
