//
//  LiveMatchViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxCocoa
import RxSwift
import CHIPageControl
import Lottie

class LiveMatchViewController: MainViewController {
    enum LiveMatchViewType {
        case loading
        case empty
        case complete
        case profile
    }
    
    @IBOutlet weak var newCardListBadge: UIView!
    
    // 빈화면 및 탐색화면
    @IBOutlet weak var emptyWrapView: UIView!
    @IBOutlet weak var emptyCardImageView: UIImageView!
    @IBOutlet weak var lottiWrapView: UIView!
    @IBOutlet weak var emptyTopLabel: UILabel!
    @IBOutlet weak var emptySubLabel: UILabel!
    
    // 카드 사진화면
    @IBOutlet weak var profileImageWrapView: UIView!
    @IBOutlet weak var profileScrollWrapView: UIView!

    // 하단 영역 - 프로필정보
    @IBOutlet weak var profileInfoView: UIView!
    @IBOutlet weak var specImageView: UIImageView!
    @IBOutlet weak var specCount: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var areaAgeLabel: UILabel!
    @IBOutlet weak var pageControl: CHIPageControlPuya!
    
    // 버튼
    @IBOutlet weak var listWrapView: UIView!
    @IBOutlet weak var listButton: UIButton!
//    @IBOutlet weak var listAnimationButton: UIButton!
    @IBOutlet weak var passButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var rollBackButton: UIButton!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var profileButtonsWrapView: UIView!
    
    private var lopttiView: AnimationView?
    private var previousPage: Int = 0
    
    var viewModel: LiveMatchViewModel!
    
    var idx: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        listWrapView.layer.cornerRadius = 6.0
//        listButton.layer.cornerRadius = 6.0
//        listAnimationButton.layer.cornerRadius = 6.0
        newCardListBadge.layer.cornerRadius = 2.5
        
        viewModel = LiveMatchViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.newBadge
            .subscribe(onNext: { isNew in
                self.newCardListBadge.isHidden = !isNew
            }).disposed(by: bag)
        viewModel.getReward
            .filter{$0 == true}
            .subscribe(onNext: { reward in
                if reward {
                    CompleteView.init(type: .liveMatchReward).show {
                    }
                }
            }).disposed(by: bag)
        
        viewModel.loading
            .filter{$0 == true}
            .subscribe(onNext: { needLoading in
                if needLoading {
                    self.setViewMode(.loading)
                }
            }).disposed(by: bag)
        
        viewModel.empty
            .filter{$0 == true}
            .subscribe(onNext: { isEmpty in
                if isEmpty {
                    self.setViewMode(.empty)
                }
            }).disposed(by: bag)
        
        viewModel.complete
            .filter{$0 == true}
            .subscribe(onNext: { isComplete in
                if isComplete {
                    self.setViewMode(.complete)
                }
            }).disposed(by: bag)
        
        viewModel.moveCardDetail
            .subscribe(onNext: { matchNo in
                let vc = RLStoryboard.todayCard.cardDetailViewController(matchNo: matchNo) { refreshType in
//                    if refreshType != .none {
//                        self.viewModel.refresh()
//                        MainInformation.shared.updateMyCrownNew()
//                    }
//                    self.viewModel.loadData()
                }
                self.tabBarController?.push(vc)
            }).disposed(by: bag)
        
        
        viewModel.card
            .subscribe(onNext: { cardInfo in
                guard let card = cardInfo else { return }
                self.pageControl.set(progress: 0, animated: true)
                self.profileScrollWrapView.subviews.forEach { $0.removeFromSuperview() }
                let profileScrollView = ProfileScrollView.init(imageUrls: card.photoURLList, statusLabelOn: false, bgColor: .dark, countLabelOn: false, touchActionOn: true)
                profileScrollView.backgroundColor = .clear
                self.profileScrollWrapView.addSubview(profileScrollView)
                profileScrollView.currentPageCallback = { idx in
                    self.pageControl.set(progress: idx, animated: true)
                }
                profileScrollView.touchAction = {
                    if self.viewModel.matchNo != nil {
                        self.viewModel.openCard()
                        return
                    }
                    
                    let needCrown = self.viewModel.needCardOpenCrown
                    if needCrown.canPayable() {
                        RLPopup.shared.showNormal(parent: self, title: "호감 카드 즉시 확인하기", description: "크라운 \(needCrown)개가 차감됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", rightAction: {
                            self.viewModel.openCard()
                        })
                    }else{
                        RLPopup.shared.showMoveStore(needCrown: needCrown, reasonStr: "호감 카드를 즉시 확인하기 위해", action: {
                            self.moveToStore()
                        })
                    }
                }
                
                self.specImageView.isHidden = card.specCount == 0
                self.specCount.isHidden = card.specCount == 0
                self.specCount.text = "\(card.specCount)"
                self.jobLabel.text = card.title
                self.areaAgeLabel.text = card.subTitle
                
                self.setViewMode(.profile)
                
            }, onError: { err in
                mainAppDelegate.hideBlockView()
            })
            .disposed(by: bag)
        
        listButton.rx.tap
            .rxTouchAnimation(button: listButton, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.liveMatch.liveMatchHistoryViewController()
                vc.outCallback = {
                    self.viewModel.loadData()
                }
                self.tabBarController?.push(vc)
            })
            .disposed(by: bag)
        
        passButton.rx.tap
            .rxTouchAnimation(button: passButton, throttleDuration: RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.viewModel.passAction()
            })
            .disposed(by: bag)
        
        likeButton.rx.tap
            .rxTouchAnimation(button: likeButton, throttleDuration: RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.viewModel.likeAction()
                // 효과 두번 주기
                self.listWrapView.wave(callback: {
                    self.listWrapView.wave(callback: {
//                        self.listWrapView.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
                    })
                })
//                delay(0.6) { self.listWrapView.wave(callback: nil) }
//                delay(1.2) { self.listWrapView.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0) }
//                self.listAnimationButton.wave(baseColor: UIColor.brown75, delay: 0.0) {
//                    self.listAnimationButton.wave(baseColor: UIColor.brown75, delay: 0.0) {
//                        self.listAnimationButton.wave(baseColor: UIColor.brown75, delay: 0.0, callback: nil)
//                    }
//                }
            })
            .disposed(by: bag)
        
        rollBackButton.rx.tap
            .rxTouchAnimation(button: rollBackButton, throttleDuration: .seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.viewModel.checkRollback()
                    .subscribe(onNext: { _ in
                        let needCrown = self.viewModel.needRollBackCrown
                        if needCrown.canPayable() {
                            RLPopup.shared.showNormal(parent: self, title: "PASS 되돌리기", description: "이전 카드로 되돌리기 위해 크라운 \(needCrown)개\n가 사용됩니다.\n\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", rightAction:  {
                                self.viewModel.rollbackAction()
                            })
                        }else{
                            RLPopup.shared.showMoveStore(needCrown: needCrown, reasonStr: "이전 카드로 되돌리기 위해", action: {
                                self.moveToStore()
                            })
                        }
                    }, onError: {
                        $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                        guard let custom = $0 as? CustomError else { return }
                        if custom.resultCD == RLResultCD.NETWORK_ERROR.code {
                            alertNETWORK_ERR(msg: custom.errorMessage) { }
                        }else if custom.resultCD == RLResultCD.E00000_Error.code {
                            alertERR_E0000Dialog()
                        }else{
                            toast(message: custom.errorMessage, seconds: 1.5) { }
                        }
                    })
                    .disposed(by: self.bag)
            })
            .disposed(by: bag)
        
        findButton.rx.tap
            .rxTouchAnimation(button: findButton, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.liveMatch.liveMatchHistoryViewController()
                self.tabBarController?.push(vc)
            })
            .disposed(by: bag)
        
        NotificationCenter.default.rx
            .notification(RLNotiType.applicationBecomeActive.notiName)
            .subscribe(onNext: { noti in
                if let lotti = self.lopttiView{
                    if !lotti.isAnimationPlaying {
                        lotti.play()
                    }
                }
            })
            .disposed(by: bag)
    }
    
    var lastDate = Date()
    func refreshData() {
        guard let _lastData = self.viewModel?.lastDate else { return }
        let seconds = Date().timeIntervalSince(_lastData)
        if seconds >= 10 {
            self.viewModel?.loadData()
        }
    }
    
    func buttonAction() {
        self.setViewMode(.loading)
        let array: [LiveMatchViewType] = [.empty, .complete, .profile]
        delay(1.5) {
            self.idx += 1
            if self.idx > 2 { self.idx = 0 }
            self.setViewMode(array[self.idx])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if let lotti = self.lopttiView {
            if !lotti.isAnimationPlaying {
                lotti.play()
            }
        }
        
    }

    override func detectedScreenShot() {
        super.detectedScreenShot()
        RLPopup.shared.detectCapture(rightAction: {})
    }
    
    func setViewMode(_ liveMatchViewType: LiveMatchViewType) {
        switch liveMatchViewType {
        case .loading:
            emptyWrapView.isHidden = false
            profileImageWrapView.isHidden = true
            profileInfoView.isHidden = true
            emptyCardImageView.image = UIImage.init(named: "03LiveMatchingImgEmptyCard182")
            
            emptyTopLabel.text = "탐색중 …"
            emptySubLabel.text = ""
            
            lottiWrapView.removeAllSubView()
            let lopttiView = AnimationView.init(name: "03_motion_empty_search")
            self.lopttiView = lopttiView
            lopttiView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 100)
            lopttiView.loopMode = .loop
            lottiWrapView.addSubview(lopttiView)
            lopttiView.play()
            
            findButton.isHidden = true
            profileButtonsWrapView.isHidden = false
        case .empty:
            emptyWrapView.isHidden = false
            profileImageWrapView.isHidden = true
            profileInfoView.isHidden = true
            emptyCardImageView.image = UIImage.init(named: "mainEmptyCardImg")
            
            emptyTopLabel.text = "더 이상 확인할 카드가 없어요"
            emptySubLabel.text = "잠시 후 다시 시도해주세요!"
            
            lottiWrapView.removeAllSubView()
            
            findButton.isHidden = false
            profileButtonsWrapView.isHidden = true
        case .complete:
            emptyWrapView.isHidden = false
            profileImageWrapView.isHidden = true
            profileInfoView.isHidden = true
            emptyCardImageView.image = UIImage.init(named: "03LiveMatchingImgEmptyCard182")
            
            emptyTopLabel.text = "오늘의 라이브 매칭을\n모두 완료하였어요!"
            emptySubLabel.text = "매일 밤 12시에 다시 시작됩니다 :)"
            
            lottiWrapView.removeAllSubView()
            let lopttiView = AnimationView.init(name: "03_motion_empty_watch")
            self.lopttiView = lopttiView
            lopttiView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 100)
            lopttiView.loopMode = .loop
            lottiWrapView.addSubview(lopttiView)
            lopttiView.play()
            
            findButton.isHidden = false
            profileButtonsWrapView.isHidden = true
        case .profile:
            emptyWrapView.isHidden = true
            profileImageWrapView.isHidden = false
            profileInfoView.isHidden = false
            
            findButton.isHidden = true
            profileButtonsWrapView.isHidden = false
        }
    }
    
//    override var prefersStatusBarHidden: Bool {
//         return true
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
}

