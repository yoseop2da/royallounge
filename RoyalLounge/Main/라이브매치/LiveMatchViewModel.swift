//
//  LiveMatchViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Lottie

class LiveMatchViewModel: BaseViewModel {

    var needCardOpenCrown: Int = 5
    var needRollBackCrown: Int = 5
    
    var newBadge = BehaviorRelay<Bool>.init(value: false)
    var getReward = BehaviorRelay<Bool>.init(value: false)
    var loading = BehaviorRelay<Bool>.init(value: false)
    var complete = BehaviorRelay<Bool>.init(value: false)
    var empty = BehaviorRelay<Bool>.init(value: false)
    var moveCardDetail: PublishSubject<String> = PublishSubject<String>.init()
    
    var card = BehaviorRelay<LiveMatchCardModel?>.init(value: nil)
    
    private var liveMatchHistNo: String?
    private let dateformatter = DateFormatter()
    private var viewDate: String? {
        let send = RLUserDefault.shared.sendListRequestDate
        let receive = RLUserDefault.shared.receiveListRequestDate
        
        guard let _send = send, let _receive = receive else { return nil }
        
        dateformatter.locale = Locale.init(identifier: "ko_KR")
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let sendDate = dateformatter.date(from: _send),
            let receiveDate = dateformatter.date(from: _receive) {
            let gap = sendDate.timeIntervalSince(receiveDate)
            if gap > 0 {
                return _receive
            }else{
                return _send
            }
        }
        if let _send = send { return _send }
        if let _receive = receive { return _receive }
        return nil
    }
    private var userNo: String!
    
    var matchNo: String?
    var lastDate: Date?
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        loadData()
        
        MainInformation.shared.updateMyCrownNew()
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .filter{$0 != nil}
            .subscribe(onNext: { crown in
                self.needRollBackCrown = crown!.liveMatchRevert
                self.needCardOpenCrown = crown!.liveMatchDetail
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func loadData() {
        self.loading.accept(true)
        Repository.shared.main
            .getLiveMatchCard(userNo: userNo, viewDate: self.viewDate)
            .subscribe(onNext: { liveMatchResult in
                self.lastDate = Date()
                
                self.newBadge.accept(liveMatchResult.newCardYn)
                self.liveMatchHistNo = liveMatchResult.liveMatchHistNo
                self.matchNo = liveMatchResult.matchNo
                
                if liveMatchResult.rewardYn {
                    MainInformation.shared.updateMyCrownNew()
                    self.getReward.accept(true)
                }
                
                if liveMatchResult.endYn {
                    self.complete.accept(true)
                    return
                }
                
                if let card = liveMatchResult.card {
                    self.card.accept(card)
                }else{
                    self.empty.accept(true)
                }
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func likeAction() {
        guard let _liveMatchHistNo = self.liveMatchHistNo else { return }
        mainAppDelegate.showBlockView()
        loading.accept(true)
        Repository.shared.main
            .addLiveMatchCardAction(userNo: userNo, viewDate: self.viewDate, liveMatchHistNo: _liveMatchHistNo, likeYn: true)
            .subscribe(onNext: { liveMatchResult in
                mainAppDelegate.hideBlockView()
                MainInformation.shared.updateRedDot()
                
                self.newBadge.accept(liveMatchResult.newCardYn)
                self.liveMatchHistNo = liveMatchResult.liveMatchHistNo
                self.matchNo = liveMatchResult.matchNo
                
                if liveMatchResult.rewardYn {
                    MainInformation.shared.updateMyCrownNew()
                    self.getReward.accept(true)
                }
                
                if liveMatchResult.endYn {
                    self.complete.accept(true)
                    return
                }
                
                if let card = liveMatchResult.card {
                    self.card.accept(card)
                }else{
                    self.empty.accept(true)
                }

            }, onError: {
                self.loadData()
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func passAction() {
        guard let _liveMatchHistNo = self.liveMatchHistNo else { return }
        
        mainAppDelegate.showBlockView()
        loading.accept(true)
        
        Repository.shared.main
            .addLiveMatchCardAction(userNo: userNo, viewDate: self.viewDate, liveMatchHistNo: _liveMatchHistNo, likeYn: false)
            .subscribe(onNext: { liveMatchResult in
                mainAppDelegate.hideBlockView()
                self.newBadge.accept(liveMatchResult.newCardYn)
                self.liveMatchHistNo = liveMatchResult.liveMatchHistNo
                self.matchNo = liveMatchResult.matchNo
                
                if liveMatchResult.rewardYn {
                    MainInformation.shared.updateMyCrownNew()
                    self.getReward.accept(true)
                }
                
                if liveMatchResult.endYn {
                    self.complete.accept(true)
                    return
                }
                
                if let card = liveMatchResult.card {
                    self.card.accept(card)
                }else{
                    self.empty.accept(true)
                }
            }, onError: {
                self.loadData()
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
    }
    
    
    func checkRollback() -> Observable<Bool> {
        return Repository.shared.main
            .checkLiveMatchValid(userNo: userNo)
    }
    
    func rollbackAction() {
        mainAppDelegate.showBlockView()
        loading.accept(true)
        Repository.shared.main
            .rollBackLiveMatchCard(userNo: userNo, viewDate: self.viewDate)
            .subscribe(onNext: { liveMatchResult in
                MainInformation.shared.updateMyCrownNew()
                mainAppDelegate.hideBlockView()

                self.newBadge.accept(liveMatchResult.newCardYn)
                self.liveMatchHistNo = liveMatchResult.liveMatchHistNo
                self.matchNo = liveMatchResult.matchNo
                
                if liveMatchResult.rewardYn {
                    self.getReward.accept(true)
                }
                
                if liveMatchResult.endYn {
                    self.complete.accept(true)
                    return
                }
                
                if let card = liveMatchResult.card {
                    self.card.accept(card)
                }else{
                    self.empty.accept(true)
                }

            }, onError: {
                self.loadData()
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
    }
    
    func openCard() {
        if let matchNo = self.matchNo {
            self.moveCardDetail.on(.next(matchNo))
            return
        }
        guard let _liveMatchHistNo = self.liveMatchHistNo else { return }
        mainAppDelegate.showBlockView()
        Repository.shared.main
            .openLiveMatchCard(userNo: userNo, liveMatchHistNo: _liveMatchHistNo, cardType: "LIVE_MATCH_DETAIL")
            .subscribe(onNext: { (_matchNo, errMsg) in
                mainAppDelegate.hideBlockView()
                MainInformation.shared.updateMyCrownNew()
                if let msg = errMsg {
                    toast(message: msg, seconds: 1.5)
                    return
                }
                if let no = _matchNo {
                    self.matchNo = no
                    toast(message: "오늘의 카드에 추가되었습니다", seconds: 1.5)
                    self.moveCardDetail.on(.next(no))
                    self.likeAction()
                }
            }, onError: { err in
                self.loadData()
                mainAppDelegate.hideBlockView()
                self.onRLError.on(.next((msg: err.localizedDescription, error: err, resultCD: nil)))
            })
            .disposed(by: bag)
    }
}

