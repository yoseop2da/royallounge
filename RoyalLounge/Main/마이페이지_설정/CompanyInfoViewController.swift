//
//  CompanyInfoViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class CompanyInfoViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<CompanyInfoSection>

    @IBOutlet weak var mainTableView: UITableView!

    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            let cell: CustomerCenterQuestionCell = tableView.dequeueReusable(for: indexPath)
            cell.setData(title: item.title, value: item.value)
            return cell
        })
        return dataSource
    }

    var viewModel: CompanyInfoViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton

        viewModel = CompanyInfoViewModel.init(userNo: MainInformation.shared.userNoAes!)

        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)

        // tableview가 groupped일경우 headerview가 자동 셋팅되어서 영역이 발생한다. 그걸 죽여주는코드!!
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        mainTableView.tableHeaderView = UIView(frame: frame)

//        mainTableView
//            .rx.setDelegate(self)
//            .disposed(by: bag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "사업자정보"
        hideNavigationControllerBottomLine()
    }
}

extension CompanyInfoViewController: UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 20
//    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return CGFloat.leastNormalMagnitude
//    }
}


class CompanyInfoViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[CompanyInfoSection]>.init(value: [])
    var userNo: String!
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        self.sections.accept([
            CompanyInfoSection.init(items: [
                CompanyInfoItemModel.init(title: "통신판매번호", value: "2020-고양일산동-1885호"),
                CompanyInfoItemModel.init(title: "사업자등록번호", value: "677-88-01393"),
                CompanyInfoItemModel.init(title: "신고현황", value: "통신판매업 신고"),
                CompanyInfoItemModel.init(title: "법인여부", value: "법인"),
                CompanyInfoItemModel.init(title: "상호", value: "(주) 리밋"),
                CompanyInfoItemModel.init(title: "대표자명", value: "최호승"),
                CompanyInfoItemModel.init(title: "대표 전화번호", value: "02-3142-1947 (이메일 고객센터만 운영)"),
                CompanyInfoItemModel.init(title: "판매방식", value: "기타"),
                CompanyInfoItemModel.init(title: "취급품목", value: "기타"),
                CompanyInfoItemModel.init(title: "전자우편", value: "help@royallounge.co.kr"),
                CompanyInfoItemModel.init(title: "신고일자", value: "2019년 11월 12일"),
                CompanyInfoItemModel.init(title: "사업장소재지 (도로명)", value: "경기도 고양시 일산동구 중앙로 1193, 670호"),
                CompanyInfoItemModel.init(title: "인터넷도메인", value: "www.limitkorea.co.kr"),
                CompanyInfoItemModel.init(title: "호스트 서버 소재지", value: "대한민국"),
                CompanyInfoItemModel.init(title: "호스트 서비스 제공자", value: "AWS코리아 (아마존웹서비스)"),
                CompanyInfoItemModel.init(title: "통신판매업 신고기관명", value: "고양시 일산동구청")
            ])
        ])
    }
}


// Tableview Cell
struct CompanyInfoSection {
    var items: [CompanyInfoItemModel]
}

extension CompanyInfoSection: SectionModelType {
    init(original: CompanyInfoSection, items: [CompanyInfoItemModel]) {
        self = original
        self.items = items
    }
}

struct CompanyInfoItemModel {
    var title: String
    var value: String
}


