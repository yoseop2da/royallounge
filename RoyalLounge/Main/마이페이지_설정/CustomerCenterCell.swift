//
//  CustomerCenterCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/23.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CustomerCenterCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        leftLabel.textColor = UIColor.dark
    }

    func setData(title: String) {
        leftLabel.text = title
    }
}

class CustomerCenterQuestionCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        
        
    }

    func initialize() {
        leftLabel.text = ""
//        versionButton.tintColor = .gray50
    }

    func setData(title: String, value: String) {
        initialize()
        self.leftLabel.text = title
        self.subLabel.text = value
    }
}
