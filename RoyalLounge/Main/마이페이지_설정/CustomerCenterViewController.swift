//
//  CustomerCenterViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class CustomerCenterViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<CustomerCenterSection>
 
    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            if case .question = item.cellType {
                let cell: CustomerCenterQuestionCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(title: item.title, value: "평일 오전 10시부터 저녁7시까지 운영합니다.(공휴일 휴무)")
                return cell
            }
            let cell: CustomerCenterCell = tableView.dequeueReusable(for: indexPath)
            cell.setData(title: item.title)
            return cell
        })
        return dataSource
    }
    
    var viewModel: CustomerCenterViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = CustomerCenterViewModel.init(userNo: MainInformation.shared.userNoAes!)

        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.tableviewReload
            .filter{ $0 == true }
            .subscribe(onNext: { _ in
                self.mainTableView.reloadData()
            })
            .disposed(by: bag)
        
        mainTableView.rx.modelSelected(CustomerCenterItemModel.self)
            .subscribe(onNext: { item in
                switch item.cellType {
                case .question(let nickname, let userId, let mbNo):
                    RLMailSender.shared.sendEmail(.문의_가입유저(nickname: nickname, userId: userId, mbNo: mbNo), finishCallback: nil)
                case .introduce:
                    showWebView(self, title: "", url: "https://www.routedate.com/")
                case .terms:
                    showWebView(self, title: "", url: "https://www.routedate.com/policy/service")
                case .personal:
                    showWebView(self, title: "", url: "https://www.routedate.com/policy/privacy")
                case .companyInfo:
                    let vc = RLStoryboard.myPage.companyInfoViewController()
                    self.push(vc)
                }
            })
            .disposed(by: bag)
        
        // tableview가 groupped일경우 headerview가 자동 셋팅되어서 영역이 발생한다. 그걸 없애주는코드
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        mainTableView.tableHeaderView = UIView(frame: frame)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "고객센터"
        hideNavigationControllerBottomLine()
    }
}

extension CustomerCenterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
