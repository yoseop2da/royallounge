//
//  CustomerCenterViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class CustomerCenterViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[CustomerCenterSection]>.init(value: [])
    let tableviewReload = PublishSubject<Bool>.init()
    var userNo: String!
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        Repository.shared.graphQl
            .getEmailInfo(userNo: userNo)
            .subscribe(onNext: {
                guard let info = $0 else { return }
                self.sections.accept([
                    CustomerCenterSection.init(header: "", items: [
                        CustomerCenterItemModel.init(title: "문의하기", cellType: .question(nickname: info.nickname, userId: info.userId.aesDecrypted!, mbNo: info.mbNo.aesDecrypted!))
                        ]),
                    CustomerCenterSection.init(header: "", items: [
                        CustomerCenterItemModel.init(title: "로얄라운지 소개", cellType: .introduce),
                        CustomerCenterItemModel.init(title: "이용약관", cellType: .terms),
                        CustomerCenterItemModel.init(title: "개인정보취급방침", cellType: .personal),
                        CustomerCenterItemModel.init(title: "사업자정보", cellType: .companyInfo),
                        
                ])])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
}


// Tableview Cell
struct CustomerCenterSection {
    var header: String
    var items: [CustomerCenterItemModel]
}

extension CustomerCenterSection: SectionModelType {
    init(original: CustomerCenterSection, items: [CustomerCenterItemModel]) {
        self = original
        self.items = items
    }
}

enum CustomerCenterCellType {
    case introduce //서비스 소개
    case terms //서비스 이용약관
    case personal //개인정보취급방침
    case companyInfo //사업자정보
    case question(nickname: String, userId: String, mbNo: String) //문의하기
}

struct CustomerCenterItemModel {
    var title: String
    var cellType: CustomerCenterCellType
}


