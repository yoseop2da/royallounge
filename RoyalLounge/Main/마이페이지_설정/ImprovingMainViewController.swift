//
//  ImprovingMainViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImprovingMainViewController: ClearNaviLoggedBaseViewController {

    @IBOutlet weak var goodButton: UIButton!
    @IBOutlet weak var bugButton: UIButton!
    @IBOutlet weak var newButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        swipeBackAnyWhere()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        goodButton.layer.cornerRadius = 6.0
        bugButton.layer.cornerRadius = 6.0
        newButton.layer.cornerRadius = 6.0
        
        goodButton.setBorder(color: .primary100)
        bugButton.setBorder(color: .primary100)
        newButton.setBorder(color: .primary100)
        
        goodButton.rx.tap
            .rxTouchAnimation(button: goodButton, scheduler: MainScheduler.instance)
            .bind {
                let vc = RLStoryboard.myPage.improvingSubViewController(type: .good)
                self.push(vc)
            }
            .disposed(by: bag)
        
        bugButton.rx.tap
            .rxTouchAnimation(button: bugButton, scheduler: MainScheduler.instance)
            .bind {
                let vc = RLStoryboard.myPage.improvingSubViewController(type: .bug)
                self.push(vc)
            }
            .disposed(by: bag)
        
        newButton.rx.tap
            .rxTouchAnimation(button: newButton, scheduler: MainScheduler.instance)
            .bind {
                let vc = RLStoryboard.myPage.improvingSubViewController(type: .new)
                self.push(vc)
            }
            .disposed(by: bag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
