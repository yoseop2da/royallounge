//
//  ImprovingSubViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ImprovingSubViewController: ClearNaviLoggedBaseViewController {

    enum ImproveType {
        case good
        case bug
        case new
    }
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoTopLabel: UILabel!
    @IBOutlet weak var infoBottomLabel: UILabel!
    
    @IBOutlet weak var topButton: UIButton!
    
    @IBOutlet weak var bottomWrapView: UIView!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var bottomCrownWrapView: UIView!
    @IBOutlet weak var bottomCrownLabel: UILabel!
    
    private var viewModel: ImprovingSubViewModel!
    var improveType: ImproveType = .good
    override func viewDidLoad() {
        super.viewDidLoad()

        swipeBackAnyWhere()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        bottomWrapView.layer.cornerRadius = 6.0
        topButton.layer.cornerRadius = 6.0
        bottomButton.layer.cornerRadius = 6.0
        bottomCrownWrapView.layer.cornerRadius = 15.0
        
        switch improveType {
        case .good:
            topButton.isHidden = false
            bottomCrownWrapView.isHidden = false
            imageView.image = UIImage.init(named: "illustations140PxEmoji01")
            infoTopLabel.text = "소중한 의견 감사합니다♥︎"
            infoBottomLabel.text = "더 발전하는 로얄라운지를 위해\n소중한 리뷰 부탁드려요"
            bottomButton.setTitle("친구 초대하기", for: .normal)
            topButton.rx.tap
                .rxTouchAnimation(button: topButton, scheduler: MainScheduler.instance)
                .bind {
                    guard let writeReviewURL = URL(string: "https://apps.apple.com/app/id1462243117?action=write-review")
                    else { fatalError("Expected a valid URL") }
                    UIApplication.shared.open(writeReviewURL, options: [:], completionHandler: nil)
                }
                .disposed(by: bag)
        case .bug:
            topButton.isHidden = true
            bottomCrownWrapView.isHidden = true
            imageView.image = UIImage.init(named: "illustations140PxEmoji02")
            infoTopLabel.text = "진심으로 사과 드립니다"
            infoBottomLabel.text = "아쉬운 점을 남겨주시면\n개선해 나가는 로얄라운지가 되겠습니다."
            bottomButton.setTitle("아쉬운점 남기러 가기", for: .normal)
        case .new:
            topButton.isHidden = true
            bottomCrownWrapView.isHidden = true
            imageView.image = UIImage.init(named: "illustations140PxEmoji03")
            infoTopLabel.text = "소중한 의견 감사합니다 :)"
            infoBottomLabel.text = "소중한 의견을 귀담아 더 발전하는\n로얄라운지가 되겠습니다."
            bottomButton.setTitle("의견 남기러 가기", for: .normal)
        }
        
        viewModel = ImprovingSubViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.crown
            .subscribe(onNext: { crown in
                guard case .good = self.improveType else { self.bottomCrownWrapView.isHidden = true; return }
                self.bottomCrownLabel.text = "x\(crown) 적립"
                self.bottomCrownWrapView.isHidden = crown == 0
            })
            .disposed(by: bag)
        
        bottomButton.rx.tap
            .rxTouchAnimation(button: bottomWrapView, scheduler: MainScheduler.instance)
            .bind {
                switch self.improveType {
                case .good:
                    let vc = RLStoryboard.join.recommendNewViewController()
                    self.modal(vc, animated: true)
                case .bug:
                    let myNickname = self.viewModel.nickname
                    let userId = self.viewModel.userId
                    let mbNo = self.viewModel.mbNo
                    RLMailSender.shared.sendEmail(.개선사항_버그(nickname: myNickname, userId: userId, mbNo: mbNo), finishCallback: { _ in
                        self.dismiss(animated: true, completion: nil)
                    })
                case .new:
                    let myNickname = self.viewModel.nickname
                    let userId = self.viewModel.userId
                    let mbNo = self.viewModel.mbNo
                    RLMailSender.shared.sendEmail(.개선사항_신규(nickname: myNickname, userId: userId, mbNo: mbNo), finishCallback: { _ in
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
            .disposed(by: bag)
    }
}
