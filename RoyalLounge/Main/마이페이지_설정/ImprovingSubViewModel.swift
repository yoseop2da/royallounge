//
//  ImprovingSubViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ImprovingSubViewModel: BaseViewModel {
    
    var crown = PublishSubject<Int>.init()
    var nickname: String = ""
    var userId: String = ""
    var mbNo: String = "입력해주세요."
    
    init(userNo: String) {
        super.init()
        Repository.shared.main
            .getCrown(userNo: userNo)
            .subscribe(onNext: { crown in
                self.crown.on(.next(crown?.recoDo ?? 40))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getEmailInfo(userNo: userNo)
            .subscribe(onNext: {
                guard let result = $0 else { return }
                self.nickname = result.nickname
                self.userId = result.userId.aesDecrypted!
                self.mbNo = result.mbNo.aesDecrypted!
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    
}
