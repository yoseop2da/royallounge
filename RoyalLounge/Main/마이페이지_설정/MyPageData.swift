//
//  MyPageData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

// Tableview Cell
struct MyPageSection {
    var header: String
    var items: [MyPageItemModel]
}

extension MyPageSection: SectionModelType {
    init(original: MyPageSection, items: [MyPageItemModel]) {
        self = original
        self.items = items
    }
}

enum MyPageCellType {
    case mySpec(isNewBadge: Bool)
    case recoCode
    case avoidMember
    case store
    case notice(isNewBadge: Bool)
    case event(isNewBadge: Bool)
    
    case royalRoungeInfo
    case customerCenter
    case settings(isNewBadge: Bool)
    
}
struct MyPageItemModel {
    var title: String
    var cellType: MyPageCellType
}


