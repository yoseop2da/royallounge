

//
//  MyPageNormalCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class MyPageNormalCell: MyPageCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setItem(_ item: MyPageItemModel) {
        super.setItem(item)
        switch item.cellType {
        case .mySpec(let isNewBadge):
            newBadge.isHidden = !isNewBadge
            iconImageView.image = UIImage.init(named: "icons24PxCertification02")
        case .recoCode:
            iconImageView.image = UIImage.init(named: "icons24PxInvite")
        case .avoidMember:
            iconImageView.image = UIImage.init(named: "icons24PxCutOff")
        case .store:
            iconImageView.image = UIImage.init(named: "icons24PxCrown")
        case .notice(let isNewBadge):
            newBadge.isHidden = !isNewBadge
            iconImageView.image = UIImage.init(named: "icons24PxNotice")
        case .event(let isNewBadge):
            newBadge.isHidden = !isNewBadge
            iconImageView.image = UIImage.init(named: "icons24PxEvent")
        default: break
        }
    }
    
}

class MyPageCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newBadge: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        newBadge.layer.cornerRadius = 2.0
    }
    
    func setItem(_ item: MyPageItemModel){
        titleLabel.text = item.title
        newBadge.isHidden = true
        
        switch item.cellType {
        case .settings(let isNewBadge):
            newBadge.isHidden = !isNewBadge
        default: break
        }
    }
}
