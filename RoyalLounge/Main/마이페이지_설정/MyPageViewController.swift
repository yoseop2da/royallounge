//
//  MyPageViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class MyPageViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<MyPageSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var profileEditButton: UIButton!
    @IBOutlet weak var profileRedDot: UIView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            switch item.cellType {
            case .royalRoungeInfo, .customerCenter, .settings:
                let cell: MyPageCell = tableView.dequeueReusable(for: indexPath)
                cell.setItem(item)
                return cell
            default:
                let cell: MyPageNormalCell = tableView.dequeueReusable(for: indexPath)
                cell.setItem(item)
                return cell
            }
        })
        return dataSource
    }
    
    private var viewModel: MyPageViewModel!

    var notificationBarbutton: RLBarButtonItem!
    var settingsBarbutton: RLBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileRedDot.layer.cornerRadius = 2.0
        profileEditButton.layer.cornerRadius = 4.0
        profileImageView.layer.cornerRadius = 32
        profileImageView.setBorder(width: 2.0, color: .primary100)
        swipeBackAnyWhere()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        
        notificationBarbutton = RLBarButtonItem.init(barType: .notification(action: {
            let vc = RLStoryboard.myPage.notificationListViewController()
            let navi = UINavigationController(rootViewController: vc)
            navi.modalPresentationStyle = .fullScreen
            self.modal(navi, animated: true)
        }))
//        settingsBarbutton = RLBarButtonItem.init(barType: .settings(action: {
//            let vc = RLStoryboard.myPage.settingsViewController()
//            self.push(vc)
//        }))
        self.navigationItem.rightBarButtonItems = [/*settingsBarbutton,*/ notificationBarbutton]
        
        MainInformation.shared.redDot
            .subscribe(onNext: { redDot in
                self.profileRedDot.isHidden = !redDot.photoRejectYn
                self.notificationBarbutton.notificationView?.isBadgeHidden = !redDot.noticeCenterYn
//                self.settingsBarbutton.settingsView?.isBadgeHidden = !redDot.appVersionYn
            })
            .disposed(by: bag)
        
        // 프로필 변경시점에 사진 업데이트 해주기
        NotificationCenter.default.rx.notification(RLNotiType.profileChanged.notiName)
            .subscribe(onNext: { noti in
                if let urlString = noti.object as? String {
                    self.profileImageView.setImage(urlString: urlString)
                }
            })
            .disposed(by: bag)
        
        profileEditButton.rx.tap
            .rxTouchAnimation(button: profileEditButton, scheduler: MainScheduler.instance)
            .bind {
                let vc = RLStoryboard.myPage.myCardDetailViewController(matchNo: RLUserDefault.shared.userNoAes!)
                self.push(vc)
            }
            .disposed(by: bag)
        
        viewModel = MyPageViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        mainTableView.rx
            .itemSelected
//            .modelSelected(MyPageItemModel.self)
            .subscribe(onNext: { indexPath in
                self.mainTableView.deselectRow(at: indexPath, animated: true)
                
                let item = self.viewModel.getItem(idx: indexPath.row)
                switch item.cellType {
                case .mySpec:
                    let vc = RLStoryboard.join.specListViewController(isEditMode: true, backType: .back)
                    self.push(vc)
                case .recoCode:
                    let vc = RLStoryboard.join.recommendNewViewController()
                    self.modal(vc, animated: true)
                case .avoidMember:
                    let vc = RLStoryboard.join.avoidViewController()
                    self.push(vc)
                case .store:
                    let vc = RLStoryboard.main.storeViewController()
                    self.push(vc)
                case .notice:
                    let vc = RLStoryboard.myPage.noticeViewController()
                    self.push(vc)
                case .event:
                    let vc = RLStoryboard.myPage.eventViewController()
                    self.push(vc)
                case .royalRoungeInfo:
                    let vc = RLStoryboard.myPage.improvingMainViewController()
                    self.push(vc)
                case .customerCenter:
                    let vc = RLStoryboard.myPage.customerCenterViewController()
                    self.push(vc)
                case .settings:
                    let vc = RLStoryboard.myPage.settingsViewController()
                    self.push(vc)
                }
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.myPageInfo
            .filter{ $0 != nil }
            .subscribe(onNext: { _myPageInfo in
                self.profileImageView.setImage(urlString: _myPageInfo?.photoUrl ?? "", placeholderImage: UIImage.init(named: "avataCirclePlaceHolder"))
                self.nicknameLabel.text = _myPageInfo?.nickname
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "내 정보"
    }
    
    @IBAction func profileButtonTouched(_ sender: Any) {
        showMyProfileActionSheet(self, previewCallback: {
            let vc = RLStoryboard.todayCard.cardDetailViewController(matchNo: nil) { refreshType in
                if refreshType != .none {
//                    self.viewModel.refresh()
                }
            }
            self.push(vc)
        }, editCallback: {
            let vc = RLStoryboard.myPage.myCardDetailViewController(matchNo: RLUserDefault.shared.userNoAes!)
            self.push(vc)
        })
    }
}
