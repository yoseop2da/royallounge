//
//  MyPageViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class MyPageViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[MyPageSection]>.init(value: [])
    private var userNo: String!
    
    let myPageInfo = BehaviorRelay<(photoUrl: String, nickname: String)?>.init(value: nil)
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        MainInformation.shared.updateRedDot()
        Repository.shared.graphQl
            .getMyPageInfo(userNo: userNo)
            .subscribe(onNext: {
                self.myPageInfo.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        MainInformation.shared.redDot
            .subscribe(onNext: { redDot in
                var items: [MyPageItemModel] = []
                items.append(MyPageItemModel.init(title: "내 뱃지 관리", cellType: .mySpec(isNewBadge: redDot.specRejectYn)))
                
                if !MainInformation.shared.outTester {
                    items.append(MyPageItemModel.init(title: "친구 초대하기", cellType: .recoCode))
                }
                items.append(MyPageItemModel.init(title: "아는사람 피하기 설정", cellType: .avoidMember))
                items.append(MyPageItemModel.init(title: "스토어", cellType: .store))
                if !MainInformation.shared.outTester {
                    items.append(MyPageItemModel.init(title: "공지사항", cellType: .notice(isNewBadge: redDot.noticeBBSYn)))
    //                이벤트 : 2차 업데이트 항목에서 배포예정
    //                items.append(MyPageItemModel.init(title: "이벤트", cellType: .event(isNewBadge: redDot.eventBBSYn)))
                }
                items.append(MyPageItemModel.init(title: "로얄라운지, 어떻게 개선할까요?", cellType: .royalRoungeInfo))
                items.append(MyPageItemModel.init(title: "고객센터", cellType: .customerCenter))
                items.append(MyPageItemModel.init(title: "설정", cellType: .settings(isNewBadge: redDot.appVersionYn)))
                self.sections.accept([MyPageSection.init(header: "", items: items)])
            })
            .disposed(by: bag)
    }
    
    func getItem(idx: Int) -> MyPageItemModel {
        return sections.value.first!.items[idx]
    }
}

