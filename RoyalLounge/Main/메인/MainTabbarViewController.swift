//
//  MainTabbarViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Firebase
import RxSwift

class MainTabbarViewController: UITabBarController {

    fileprivate var cardMainViewController: CardMainViewController?
    fileprivate var cardHistoryViewController: CardHistoryViewController?
    fileprivate var liveMatchViewController: LiveMatchViewController?

//    private var imageCache = NSCache<NSString, UIImage>()
    var currentTabbarIdx = 0
//    var isSleep = false
    private var ref: DatabaseReference!
    private var bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        initailize()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MainInformation.shared.redDot
            .subscribe(onNext: { redDot in
                if let tabbaritems = self.tabBar.items {
                    let tabbar1 = tabbaritems[1]
                    let tabbar2 = tabbaritems[2]
                    tabbar1.badgeValue = redDot.cardBoxYn ? "•" : nil
                    tabbar2.badgeValue = redDot.liveMatchYn ? "•" : nil
                    
                    if #available(iOS 10.0, *) {
                        [tabbar1, tabbar2].forEach {
                            $0.badgeColor = UIColor.clear
                            $0.setBadgeTextAttributes(
                                [NSAttributedString.Key.foregroundColor: UIColor.primary100,
                            NSAttributedString.Key.font: UIFont.spoqaHanSansRegular(ofSize: 20)], for: .normal)
                        }
                        
                    }
                }
            })
            .disposed(by: bag)
        
        ref = Database.database().reference()
        ref.observe(DataEventType.value) { snapShot in
            guard let json = snapShot.value else { return }
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
                let database = try JSONDecoder().decode(RealTimeDataBase.self, from: jsonData)
                let ios = database.prod.ios
                
                if ios.systemNotice.enable {
                    print("***************** 시스템 점검중!!!!!!!")
                    Coordinator.shared.systemNotice(content: ios.systemNotice.content)
                    return
                }
                
                if ios.appUpdate.enable {
                    if ios.appUpdate.updateType == "F" /*강제 업데이트*/ {
                        print("***************** 강제업데이트 필요 FFFF")
//                        ios.appUpdate.title
//                        ios.appUpdate.version
                        
                        if ios.appUpdate.version.isNewAppVersion {
                            RLPopup.shared.showNormal(title: "필수 업데이트 안내", description: "새로운 버전이 준비되었어요.\n지금 앱 업데이트 후 이용하세요.", enableBackGroundCloseAction: false, rightButtonTitle: "업데이트 하러가기", rightAutoClose: false, rightAction: {
                                // 업데이트 하러가기
                                self.moveToAppStore()
                            })
                        }
                        return
                    } else if ios.appUpdate.updateType == "C" /*사용자 선택 업데이트*/ {
                        print("***************** 강제업데이트 필요 CCCC")
//                        ios.appUpdate.title
//                        ios.appUpdate.version
                        if let cacheKey = RLUserDefault.shared.lastUpdateAppVersion {
                            // 다음에 했던 버전은 다시 띄우지 않는다
                            if cacheKey == ios.appUpdate.version { return }
                        }
                        if ios.appUpdate.version.isNewAppVersion {
                            RLPopup.shared.showNormal(title: "업데이트 안내", description: "새로운 버전이 준비되었어요.\n지금 앱 업데이트 후 이용하세요.", enableBackGroundCloseAction: false, leftButtonTitle: "다음에", rightButtonTitle: "업데이트 하러가기", leftAction: {
                                RLUserDefault.shared.lastUpdateAppVersion = ios.appUpdate.version
                            }, rightAction: {
                                self.moveToAppStore()
                            })
                        }
                        return
                    }
                }
            } catch let error {
                print(error)
            }
            
//            }
        }
    }
    
    func moveToAppStore() {
        let url = URL(string: ApplicationInfo.ITUNES_APPSTORE_LINK)
        guard UIApplication.shared.canOpenURL(url!) else {
            self.dismiss(animated: true) {
                toast(message: "앱스토어에서 앱 확인되지 않음", seconds: 1.5)
            }
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }

    func initailize() {
//        if isSleep {
//            setSleepAllTab()
//            self.tabBar.isHidden = true
//        } else {
            setAllTab()
//        }

//        self.tabBar.tintColor = .brown100
        guard let items = self.tabBar.items else {return}
        for tabbaritem in items {
            if #available(iOS 13.0, *) {
                tabbaritem.title = ""
                tabbaritem.imageInsets = UIEdgeInsets.zero
            } else {
                tabbaritem.title = ""
                tabbaritem.imageInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: -6, right: 0)
            }
        }
    }
 
    func moveTapOnCardHistoryViewController(idx: Int) {
        self.cardHistoryViewController?.moveToTab(idx)
    }
}

extension MainTabbarViewController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if currentTabbarIdx == tabBarController.selectedIndex { return }
        currentTabbarIdx = tabBarController.selectedIndex
        print("tabBarController.selectedIndex >>>>>>>>> \(tabBarController.selectedIndex)")
        
        // 호출 주기를 조절해줄 필요가 있음
        MainInformation.shared.updateRedDot()
        
        switch tabBarController.selectedIndex {
        case 0: self.cardMainViewController?.refreshData()
        case 1: self.cardHistoryViewController?.refreshData()
        case 2: self.liveMatchViewController?.refreshData()
        default: ()
        }
    }
}

private extension MainTabbarViewController {

    func setAllTab() {
        self.viewControllers?.forEach {
            if let vc = $0 as? LiveMatchViewController {
                self.liveMatchViewController = vc
            }
            
            if let vc = $0 as? UINavigationController {
                let viewController = vc.viewControllers[0]
                if let vc = viewController as? CardMainViewController {
                    self.cardMainViewController = vc
                } else if let vc = viewController as? CardHistoryViewController {
                    self.cardHistoryViewController = vc
                } else if let vc = viewController as? LiveMatchViewController {
                    // 여기 안탐!! 삭제 해도 무방함
                    self.liveMatchViewController = vc
                }
            }
        }
    }

//    func setSleepAllTab() {
//        let sleep1 = UINavigationController.init(rootViewController: RLStoryboard.main.sleepViewController())
//        let sleep2 = UINavigationController.init(rootViewController: RLStoryboard.main.sleepViewController())
//        let sleep3 = UINavigationController.init(rootViewController: RLStoryboard.main.sleepViewController())
//        self.viewControllers = [sleep1]
//
//        let imageNameList = ["todayInactive", "cardInactive", "likeInactive"]
//        for idx in 0..<self.viewControllers!.count {
//            let tabBarItem = (self.tabBar.items?[idx])! as UITabBarItem
//            tabBarItem.image = UIImage(named: imageNameList[idx])
//            tabBarItem.title = ""
//            tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
//        }
//    }

}
