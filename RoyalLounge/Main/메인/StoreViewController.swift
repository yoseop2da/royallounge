//
//  StoreViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SnapKit

class StoreViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<StoreSection>

    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            if indexPath.section == 0 {
                if let title = item.itemTitle, title == "이벤트" {
                    let cell: StoreEventCell = tableView.dequeueReusable(for: indexPath)
                    cell.setItem(item: item)
                    cell.cellTouched = {
                        self.viewModel.purchaseItem(productId: item.productId)
                    }
                    return cell
                }else{
                    let cell: StoreCell = tableView.dequeueReusable(for: indexPath)
                    cell.setItem(item: item)
                    cell.cellTouched = {
                        self.viewModel.purchaseItem(productId: item.productId)
                    }
                    return cell
                }
            }else{
                let cell: StoreFreeCell = tableView.dequeueReusable(for: indexPath)
                cell.setItem(item: item)
                cell.cellTouched = {
                    switch item.freeCrownType! {
                    case .joinAddProfile: self.addProfile()
                    case .recoDo: self.recommendCode()
                    case .joinUniv: self.univCo(univCoType: .univ)
                    case .joinCo: self.univCo(univCoType: .co)
                    case .joinPath: self.joinPath()
                    case .liveMatchEval: self.toLiveMatch()
                    case .cardEval: self.toCardMain()
                    }
                }
                return cell
            }
        })
        return dataSource
    }
    
    private var viewModel: StoreViewModel!
    var crownButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton

        let rBarButton = RLBarButtonItem.init(barType: .customButton(title: "크라운 내역", font: UIFont.spoqaHanSansBold(ofSize: 14), textColor: .brown75))
        rBarButton.customButton?.rx.tap
            .throttle(.milliseconds(1500), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.main.storeHistoryViewController()
                vc.focusFreeItems = {
                    if self.viewModel.hasFreeSection {
                        self.mainTableView.scrollToRow(at: IndexPath.init(row: 0, section: 1), at: .top, animated: true)
                    }
                }
                self.push(vc)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItems = [rBarButton]
        
        viewModel = StoreViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        MainInformation.shared.myCrown
            .subscribe(onNext: { crown in
                self.crownButton?.setTitle("\(crown)", for: .normal)
            }).disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel?.reloadData()
        hideNavigationControllerBottomLine()
    }
    
//    친구초대 > 추천인코드
    func recommendCode() {
        let vc = RLStoryboard.join.recommendNewViewController()
        self.modal(vc, animated: true)
    }
//    추가프로필 > 추가프로필
    func addProfile() {
        let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .hobby, isEditMode: false)

        let navi = UINavigationController.init(rootViewController: vc)
        self.modal(navi, animated: true)
    }
//    학교인증 > 학교/직장 인증
//    직장인증 > 학교/직장 인증
    func univCo(univCoType: UnivCoModel.UnivCoType) {
        let vc = RLStoryboard.join.univCoViewController(isEditMode: true, univCoType: univCoType)
        let navi = UINavigationController.init(rootViewController: vc)
        self.modal(navi, animated: true)
    }
//    가입경로 > 가입경로
    func joinPath() {
        let vc = RLStoryboard.join.joinPathViewController(isEditMode: true)
        let navi = UINavigationController(rootViewController: vc)
        vc.closeCallback = { }
        self.modal(navi, animated: true)
    }
//    라이브매칭평가 > 라이브매칭 탭으로 이동
    func toLiveMatch() {
        RLPopup.shared.showNormal(description: "라이브 매칭 평가 시 일정 횟수마다\n크라운을 무료로 적립할 수 있습니다.", isLeftButtonGray: true, leftButtonTitle: "닫기", rightButtonTitle: "라이브 매칭 평가하기", leftAction: {}, rightAction: {
            Coordinator.shared.toMain {
                Coordinator.shared.toThirdTab()
            }
        })
        
        
    }
//    카드별점평가 > 오늘의 카드탭으로 이동
    func toCardMain() {
        RLPopup.shared.showNormal(description: "오늘/이전 카드에서 별점 평가 시\n일정 횟수 당 무료 크라운이 지급됩니다.\n\n해당 평가는 회원님의 이상형 분석을 위한 매칭 기반 데이터로도 활용됩니다.", isLeftButtonGray: true, leftButtonTitle: "닫기", rightButtonTitle: "카드 평가하기", leftAction: {}, rightAction: {
            Coordinator.shared.toMain {
                Coordinator.shared.toFirstTab()
            }
        })
        
    }
}

extension StoreViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let bounds = self.navigationController!.navigationBar.bounds.height
        let headerHeight: CGFloat = 68.0
        if scrollView.contentOffset.y >= headerHeight {
            self.title = "스토어"
        }else{
            self.title = ""
        }

    }
}

extension StoreViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////        return UITableView.automaticDimension
//        let count = 2
//        let width = (UIScreen.width - 40.0 - CGFloat(15 * (count-1))) / CGFloat(count)
//        return width
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 0 ? 68.0 : 87.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let wrapView = UIView.init()
        let titleLabel = UILabel.init()
        titleLabel.textAlignment = .left
        titleLabel.text = viewModel.headerTitle(sectionIdx: section)
        titleLabel.textColor = .dark
        wrapView.addSubview(titleLabel)
        
        if section == 0 {
            titleLabel.font = UIFont.spoqaHanSansLight(ofSize: 22)
            
            titleLabel.snp.makeConstraints {
                $0.leading.equalToSuperview().offset(20)
                $0.top.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-12)
            }
            
            let button = UIButton.init()
            crownButton = button
            button.setImage(UIImage.init(named: "icons24PxCrown"), for: .normal)
            button.setTitle("\(MainInformation.shared.myCrown.value)", for: .normal)
            button.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 20)
            button.titleLabel?.textAlignment = .right
            button.setTitleColor(.primary100, for: .normal)
            button.contentHorizontalAlignment = .right
            button.contentEdgeInsets.right = 20
            button.imageEdgeInsets.right = 4
            button.isUserInteractionEnabled = false
            wrapView.addSubview(button)
            button.snp.makeConstraints {
                $0.trailing.equalToSuperview()
                $0.top.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-12)
                $0.width.equalTo(100)
            }
        }else{
            titleLabel.font = UIFont.spoqaHanSansBold(ofSize: 18)
            
            titleLabel.snp.makeConstraints {
                $0.leading.equalToSuperview().offset(20)
                $0.top.equalToSuperview().offset(40)
                $0.bottom.equalToSuperview().offset(-20)
            }
        }
        return wrapView
    }
}
