//
//  SettingsArrowCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/31.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SettingsArrowCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func initialize() {
        leftLabel.textColor = UIColor.dark
        leftLabel.text = ""
    }

    func setData(title: String, value: String? = nil) {
        initialize()
        leftLabel.text = title
        rightButton.setTitle(value, for: .normal)
    }
}
