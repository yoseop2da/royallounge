
//
//  SettingsData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

// Tableview Cell
struct SettingsSection {
    var header: String
    var items: [SettingsItemModel]
}

extension SettingsSection: SectionModelType {
    init(original: SettingsSection, items: [SettingsItemModel]) {
        self = original
        self.items = items
    }
}

enum SettingsCellType {
    case userId(userId: String)
    case password
    case mobileNumber(mbNo: String)
    
    case notiMore
    case sleep
    case version(isNewVersion: Bool)
    case logout
    
}
struct SettingsItemModel {
    var title: String
    var cellType: SettingsCellType
//    var crown: Int
//    var itemTitle, itemDescription: String? // 이벤트, 실속, 무료아이템
//    var korPrice: Int? // 한국가격
//    var freeCrownType: FreeCrownType? = nil
}
