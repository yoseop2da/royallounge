
//
//  SettingsNotiCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/22.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SettingsNotiCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    
    private var switchCallBack: ((Bool) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func setData(title: String, switchOn: Bool, switchAction: ((Bool)->Void)?) {
        switchCallBack = switchAction
        leftLabel.text = title
        switchButton.isOn = switchOn
    }
    @IBAction func switchButtonTouched(_ sender: UISwitch) {
        switchCallBack?(sender.isOn)
    }
}
