//
//  SettingsNotiData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation
import RxDataSources

// Tableview Cell
struct SettingsNotiSection {
    var items: [(setNo: String, title: String, sendYn: Bool)]
    
}

extension SettingsNotiSection: SectionModelType {
    init(original: SettingsNotiSection, items: [(setNo: String, title: String, sendYn: Bool)]) {
        self = original
        self.items = items
    }
}

