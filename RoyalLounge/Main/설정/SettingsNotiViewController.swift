
//
//  SettingsNotiViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/21.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

class SettingsNotiViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<SettingsNotiSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            let cell: SettingsNotiCell = tableView.dequeueReusable(for: indexPath)
            cell.setData(title: item.title, switchOn: item.sendYn) { isOn in
                self.viewModel.setPush(isOn: isOn, setNo: item.setNo)
            }
            return cell
        })
        return dataSource
    }
    
    private var viewModel: SettingsNotiViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()

        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = SettingsNotiViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        mainTableView.tableHeaderView = UIView(frame: frame)
        
        mainTableView.rx
            .setDelegate(self)
            .disposed(by: bag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "푸시 알림 설정"
        hideNavigationControllerBottomLine()
    }
}

extension SettingsNotiViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
