//
//  SettingsNotiViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

class SettingsNotiViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[SettingsNotiSection]>.init(value: [])
    
    private var userNo: String!
    private var pushList = BehaviorRelay<[(setNo: String, title: String, sendYn: Bool)]>.init(value: [])

    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        Repository.shared.graphQl
            .getMemberPushSetList(userNo: userNo)
            .subscribe(onNext: {
                self.pushList.accept($0)
                self.sections.accept([SettingsNotiSection.init(items: $0)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }

    func setPush(isOn: Bool, setNo: String) {
        Repository.shared.graphQl
            .setMemberPush(userNo: userNo, setNo: setNo, sendYn: isOn)
            .bind(to: pushList)
            .disposed(by: bag)
    }
}
