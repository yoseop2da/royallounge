//
//  SettingsPasswordViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/22.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class SettingsPasswordViewController: ClearNaviBaseViewController {

    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var currentPasswordWrapView: UIView!
    @IBOutlet weak var passwordWrapView: UIView!
    @IBOutlet weak var passwordConfirmWrapView: UIView!
    
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    let nextButton = RLNextButton.init(title: "비밀번호 변경")
    
    var auth: AuthModel!

    //영문+숫자+특수문자 조합 8~16자
    let currentPasswordTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "현재 비밀번호를 입력해주세요", title: "현재 비밀번호", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .next
        return field
    }()
    
    let passwordTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 입력해주세요", title: "비밀번호", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .next
        return field
    }()

    let passwordConfirmTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 한번 더 입력해주세요", title: "비밀번호 확인", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .done
        return field
    }()
    
    var viewModel: SettingsPasswordViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        titleLabel.text = "비밀번호 변경"
        nextButton.layer.cornerRadius = 6.0
        
        currentPasswordWrapView.addSubview(currentPasswordTextField)
        currentPasswordTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(currentPasswordWrapView)
        }
        passwordWrapView.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(passwordWrapView)
        }
        passwordConfirmWrapView.addSubview(passwordConfirmTextField)
        passwordConfirmTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(passwordConfirmWrapView)
        }
        
        currentPasswordTextField.returnKeyCallback = { self.passwordTextField.becomeFirst = true }
        passwordTextField.returnKeyCallback = { self.passwordConfirmTextField.becomeFirst = true }
//        passwordConfirmTextField.returnKeyCallback = { self.viewModel.change() }
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = SettingsPasswordViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            currentPassword: currentPasswordTextField.textObserver
            .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            newPassword: passwordTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            passwordConfirm: passwordConfirmTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance))
        
        viewModel.focusOnCurrentField
            .bind { focus in
                self.currentPasswordTextField.becomeFirst = focus }
            .disposed(by: bag)
        
        viewModel.currentTextFieldResult
            .bind(to: currentPasswordTextField.rx.setResult)
            .disposed(by: bag)
        
        viewModel.newPasswordTextFieldResult
            .bind(to: passwordTextField.rx.setResultPassword) //
            .disposed(by: bag)
        
        viewModel.newPasswordTextFieldResultConfirm
            .bind(to: passwordConfirmTextField.rx.setResultPassword)
            .disposed(by: bag)
        
        viewModel.passwordEnable
            .bind(to: nextButton.rx.buttonEnableState)//
            .disposed(by: bag)
        
        viewModel.nextAction
            .subscribe(onNext: { success in
                if success {
                    toast(message: "비밀번호 변경이 완료되었습니다", seconds: 1.5, isOnKeyboard: self.bottomPadding.constant != 0) {
                        self.pushBack()
                    }
                }else{
                    // 실패, 실패 케이스가 있남?
                }
            })
            .disposed(by: bag)
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.viewModel.change()
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                print("---------height: \(keyboard.height)")
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height == 0 ? 30 : keyboard.height
                    if UIScreen.underSE {
                        self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 50)
                    }
                    
                    
                    if keyboard.height == 0 {
                        self.nextButton.roundCorner()
                    }else{
                        self.nextButton.squareCorner()
                    }
                    
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
}
