//
//  SettingsPasswordViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsPasswordViewModel: BaseViewModel {
    private var _currentPassword: String = ""
    private var _newPassword: String = ""
    private var invalidResult: TextFieldResult?

    let focusOnCurrentField = PublishSubject<Bool>.init()
    let validatedCurrent: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let currentTextFieldResult: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let newPasswordTextFieldResult: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let newPasswordTextFieldResultConfirm: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    var passwordEnable: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextAction: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    
    private var userNo: String!
    init(userNo: String, currentPassword: Observable<String>,
        newPassword: Observable<String>, passwordConfirm: Observable<String>) {
        super.init()
        self.userNo = userNo
        currentPassword
            .subscribe(onNext: { text in
                print("==========text : \(text)")
                if self._currentPassword == text {
                    if let result = self.invalidResult {
                        self.currentTextFieldResult.accept(result)
                    }
                }else{
                    self.invalidResult = nil
                    self.currentTextFieldResult.accept(TextFieldResult.empty)
                    self.validatedCurrent.accept(text.length > 0)
                    self._currentPassword = text
                }
            })
            .disposed(by: bag)
        
        Observable.combineLatest(newPassword, passwordConfirm)
            .subscribe(onNext: {
                let resultPw = Validation.shared.validate(password: $0)
                self.newPasswordTextFieldResult.accept(resultPw)
                self._newPassword = $0
                
                let result = Validation.shared.validate(password: $0, confirmPassword: $1)
                self.newPasswordTextFieldResultConfirm.accept(result)
            })
            .disposed(by: bag)
        
        Observable
            .combineLatest(validatedCurrent, newPasswordTextFieldResult, newPasswordTextFieldResultConfirm) {
                $0 && $1.isValid && $2.isValid
            }
            .distinctUntilChanged()
            .bind(to: passwordEnable)
            .disposed(by: bag)
    }
    
    func change() {
        Repository.shared.main
            .changePassword(userNo: userNo, currentPassword: _currentPassword.aesEncrypted!, newPassword: _newPassword.aesEncrypted!)
            .subscribe(onNext: { success in
                self.nextAction.accept(success)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                let result = TextFieldResult.failed(message: custom.errorMessage)
                self.invalidResult = result
                self.focusOnCurrentField.on(.next(true))
                self.currentTextFieldResult.accept(result)
            })
            .disposed(by: bag)
    }
    
}
