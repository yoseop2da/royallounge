
//
//  SettingsUserIdCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsUserIdCell: UITableViewCell {
    
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var userIdLabel: UILabel!
    
    private let bag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(userId: String) {
        userIdLabel.text = userId
    }
}
