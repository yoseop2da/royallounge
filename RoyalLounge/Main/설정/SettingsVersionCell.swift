//
//  SettingsVersionCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/31.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SettingsVersionCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var newDot: UIView!
    @IBOutlet weak var versionButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func initialize() {
        leftLabel.text = ""
        subLabel.text = ""
        newDot.isHidden = true
        newDot.layer.cornerRadius = 2.0
//        versionButton.tintColor = .gray50
    }

    func setData(title: String, value: String, isServerNew: Bool) {
        initialize()
        self.leftLabel.text = title
        self.subLabel.text = isServerNew ? "업데이트가 필요합니다" : "최신버전입니다"
        self.versionButton.setTitle(value, for: .normal)
        versionButton.setTitleColor(isServerNew ? .dark : .gray50, for: .normal)
        newDot.isHidden = !isServerNew
    }
}
