
//
//  SettingsViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/31.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

class SettingsViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<SettingsSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            
            if case .version(let isNewVersion) = item.cellType {
                let cell: SettingsVersionCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(title: item.title, value: ApplicationInfo.appVersion, isServerNew: isNewVersion)
                return cell
            }
            if case .userId(let userId) = item.cellType {
                let cell: SettingsUserIdCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(userId: userId)
                return cell
            }
            
            if case .logout = item.cellType {
                let cell: SettingsLogoutCell = tableView.dequeueReusable(for: indexPath)
                return cell
            }
            
            let cell: SettingsArrowCell = tableView.dequeueReusable(for: indexPath)
            switch item.cellType {
            case .mobileNumber(let mbNo):
                cell.setData(title: item.title, value: mbNo)
            default: cell.setData(title: item.title, value: "")
            }
            
            return cell
        })
        return dataSource
    }
    
    private var viewModel: SettingsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = SettingsViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        mainTableView.rx.modelSelected(SettingsItemModel.self)
            .subscribe(onNext: { item in
                switch item.cellType {
                case .password:
                    let vc = RLStoryboard.myPage.settingsPasswordViewController()
                    self.push(vc)
                case .mobileNumber:
                    RLPopup.shared.showPhoneChangePopup(leftAction: {}, rightAction: {
                        let vc = RLStoryboard.splash.authViewController()
                        vc.authType = .changeMobileNo
                        vc.changeMobileNoCallback = { auth in
                            self.viewModel.updateMobileNo(mbNo: auth.mbNoAES)
                        }
                        self.push(vc)
                    })
                case .version(let isNewVersion):
                    if isNewVersion {
                        self.moveToAppStore()
                    }
                case .notiMore:
                    let vc = RLStoryboard.myPage.settingsNotiViewController()
                    self.push(vc)
                case .sleep:
                    let vc = RLStoryboard.myPage.settingsSleepViewController()
                    self.push(vc)
                case .logout:
                    RLNotificationCenter.shared.post(notiType: .logout)
                default: ()
                }
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        mainTableView.tableHeaderView = UIView(frame: frame)
        
        mainTableView.rx
            .setDelegate(self)
            .disposed(by: bag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "설정"
        hideNavigationControllerBottomLine()
        self.viewModel.updateUserInfo()
    }
    
    func moveToAppStore() {
        let url = URL(string: ApplicationInfo.ITUNES_APPSTORE_LINK)
        guard UIApplication.shared.canOpenURL(url!) else {
            self.dismiss(animated: true) {
                toast(message: "앱스토어에서 앱 확인되지 않음", seconds: 1.5)
            }
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
}

private extension SettingsViewController {
    func sleep(isOn: Bool, failCallBack: @escaping () -> Void) {
        if isOn {
            if MainInformation.shared.isSleep { return }
            self.viewModel.setSleep(isSleep: isOn, failCallback: {
                failCallBack()
            })
        } else {
            self.viewModel.setSleep(isSleep: isOn, failCallback: {
                failCallBack()
            })
        }
    }
}

extension SettingsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = self.viewModel.headerString(section: section)
        if title.length == 0 {
            return UIView()
        }else{
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 34))
            view.backgroundColor = .clear//UIColor.rtLightGray247
            let label = UILabel.init(frame: CGRect.init(x: 14, y: 0, width: UIScreen.width - 14.0, height: 34))
            view.addSubview(label)
            label.textColor = UIColor.gray50
            label.font = UIFont.spoqaHanSansRegular(ofSize: 12)
            label.text = title
            return view
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 20
        }else if section == 1 {
            return 34
        }else{
            return .leastNormalMagnitude
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
}
