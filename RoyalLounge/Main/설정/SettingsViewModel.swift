//
//  SettingsViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/31.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SettingsViewModel: BaseViewModel {
    enum NotiType: String {
        case cardToday = "CARD_TODAY" //: 오늘의카드 도착 푸시
        case receiveLikes = "RECV_LIKE" //: 받은 좋아요(상대방의 좋아요 카드 확인, 상대방의 슈퍼좋아요, 상대방의 좋아요)
        case receiveNewMessage = "RECV_NEW_MSG" //: 받은 새로운 메시지(매칭성공, 채팅메시지)

        case cardOppLikeView = "CARD_OPP_LIKE_VIEW" //: 상대방의 좋아요 카드 확인
        case cardOppSupLikeView = "CARD_OPP_SUP_LIKE_VIEW" //: 상대방의 슈퍼 좋아요 카드 확인
        case cardOppLikeSupLike = "CARD_OPP_LIKE_SUP_LIKE" //: 상대방의 좋아요 카드 확인

        case event = "EVENT" // 공지, 이벤트 푸시

        case cardOppHigh = "CARD_OPP_HIGH" //: 상대방의 높은점수
        case matchSuccess = "MATCH_SUCC" //: 매칭성공
        case chatMessage = "CHAT_MESSAGE" //: 채팅메시지

        var paramValue: String { return self.rawValue }
    }
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[SettingsSection]>.init(value: [])
    
    private var userNo: String!
    private var userInfo = BehaviorRelay<(userId: String, mbNo: String, nickname: String)?>.init(value: nil)
    private var latestAppVersion = BehaviorRelay<String>.init(value: "1.0.0")
//    Observable<>
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        updateUserInfo()

        Observable.zip(userInfo, latestAppVersion)
            .subscribe(onNext: { userInfo, version in
                guard let _userInfo = userInfo else { return }
                self.sections.accept([
                    SettingsSection.init(header: "", items: [
                        SettingsItemModel.init(title: "아이디", cellType: .userId(userId: _userInfo.userId)),
                        SettingsItemModel.init(title: "비밀번호 변경", cellType: .password),
                        SettingsItemModel.init(title: "휴대폰 번호 변경", cellType: .mobileNumber(mbNo: self.dashPhoneNumber(mbNo: _userInfo.mbNo)))
                    ]),
                    SettingsSection.init(header: "앱 설정", items: [
                        SettingsItemModel.init(title: "푸시 알림 설정", cellType: .notiMore),
                        SettingsItemModel.init(title: "계정 휴면 / 탈퇴", cellType: .sleep),
                        SettingsItemModel.init(title: "앱 버전", cellType: .version(isNewVersion: version.isNewAppVersion))
                    ]),
                    SettingsSection.init(header: "", items: [
                        SettingsItemModel.init(title: "", cellType: .logout)
                    ])
                ])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        
    }
    
    func dashPhoneNumber(mbNo: String) -> String {
        if mbNo.length == 10 {
            let str0 = (mbNo as NSString).substring(with: NSRange.init(location: 0, length: 3)) // 017
            let str1 = (mbNo as NSString).substring(with: NSRange.init(location: 3, length: 3)) // 999
            let str2 = (mbNo as NSString).substring(with: NSRange.init(location: 6, length: 4)) // 7777
            return "\(str0)-\(str1)-\(str2)"
        } else if mbNo.length == 11 {
            let str0 = (mbNo as NSString).substring(with: NSRange.init(location: 0, length: 3)) // 017
            let str1 = (mbNo as NSString).substring(with: NSRange.init(location: 3, length: 4)) // 9999
            let str2 = (mbNo as NSString).substring(with: NSRange.init(location: 7, length: 4)) // 7777
            return "\(str0)-\(str1)-\(str2)"
        }
        return mbNo
    }
    
    func updateUserInfo() {
        Repository.shared.graphQl
            .getSettingsInfo(userNo: userNo)
            .subscribe(onNext: {
                self.userInfo.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        Repository.shared.graphQl
            .getSettingsLatestAppVersion()
            .subscribe(onNext: {
                self.latestAppVersion.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    var appServerVersionIsNew: Bool?

    func cellType(indexPath: IndexPath) -> SettingsCellType {
        return sections.value[indexPath.section].items[indexPath.row].cellType
    }
    
    func headerString(section: Int) -> String {
        return sections.value[section].header
    }
    
    func setSleep(isSleep: Bool, failCallback: @escaping () -> Void) {
        Repository.shared.main
            .sleep(userNo: userNo, sleepYn: isSleep)
            .filter{ $0 == true }
            .subscribe(onNext: { success in
                Coordinator.shared.moveByUserStatus()
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

                failCallback()
            })
        .disposed(by: bag)
    }

    func updateMobileNo(mbNo: String) {
        Repository.shared.graphQl
            .updateMobileNo(userNo: userNo, mbNo: mbNo)
            .subscribe(onNext: { _ in
//                var newUserInfo = self.userInfo.value
//                newUserInfo?.mbNo = mbNo.aesDecrypted ?? ""
//                self.userInfo.accept(newUserInfo)
            }, onError: { err in
                
            })
        .disposed(by: bag)
    }
}
