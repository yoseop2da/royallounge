//
//  InAppHandler.swift
//  Route
//
//  Created by yoseop park on 02/05/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import StoreKit

enum InAppHandlerAlertType {
    case disabled
    case deferred
    case failed
    case purchased
    case purchasing
    case didReceive

    func message() -> String {
        switch self {
        case .disabled: return "구매 결제를 위한 설정이 잠겨 있습니다.\n설정 > 일반 > 차단\n해당경로에 진입하여 확인해주세요."
        case .failed: return "구매가 취소/실패 되었습니다."
        case .deferred: return "부모님의 수락이 필요한 상태입니다."
        case .purchased: return "You've successfully bought this purchase!"
        case .purchasing: return "구매를 시작합니다."
        case .didReceive: return "인앱 아이템 정보를 받아왔습니다."
        }
    }
}

class InAppHandler: NSObject {
    static let shared = InAppHandler()
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
    private let bag = DisposeBag()

    fileprivate var purchaseStatusBlock: (((alertType: InAppHandlerAlertType, successMessage: String)) -> Void)?
    fileprivate var restoreBlock: (() -> Void)?

    var inappReciveBlock: (() -> Void)?

    func purchaseMyProduct(productId: String, callback: @escaping (((alertType: InAppHandlerAlertType, successMessage: String)) -> Void)) {
        self.purchaseStatusBlock = callback

        if iapProducts.count == 0 { return }
        if SKPaymentQueue.canMakePayments() {
            guard let product = skproduct(from: productId) else {
                purchaseStatusBlock?((alertType: .failed, successMessage: ""))
                return
            }
            mainAsync {
                let payment = SKPayment(product: product)
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(payment)
            }
        } else {
            purchaseStatusBlock?((alertType: .disabled, successMessage: ""))
        }
    }

    func getPrice(inAppItemId: String) -> String {
        if SKPaymentQueue.canMakePayments() {
            if let product = skproduct(from: inAppItemId) {
                let priceFormatter: NumberFormatter = NumberFormatter.init()
                priceFormatter.formatterBehavior = .behavior10_4
                priceFormatter.numberStyle = .currency
                priceFormatter.locale = product.priceLocale
                guard let price = priceFormatter.string(from: product.price) else {
                    return ""
                }
                return price
            }
        }
        return ""
    }

    // transaction이 끝나지 않은 이전 구매목록 확인.
    func restorePurchase(callBack: @escaping () -> Void = {}) {
        self.restoreBlock = callBack
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts(productIDs: [String]) {
        // 구매 가능한 아이템 리스트 받아오기
        if let productIdentifiers = self.productIdentifiers(from: productIDs) as? Set<String> {
            self.productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
            self.productsRequest.delegate = self
            self.productsRequest.start()
        }
    }

    func productIdentifiers(from: [String]) -> NSSet {
        let productIdentifiers = NSMutableSet()
        for productID in from { productIdentifiers.add(productID) }
        return productIdentifiers
    }

    func skproduct(from: String) -> SKProduct? {
        for product in iapProducts where product.productIdentifier == from { return product }
        return nil
    }
}

extension InAppHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    // MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("===============response.products Count : \(response.products.count)")
        if response.products.count > 0 {
            iapProducts = response.products
            purchaseStatusBlock?((alertType: .didReceive, successMessage: ""))
        }
    }

    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        if queue.transactions.count > 0 {
            for transaction in queue.transactions where transaction.transactionState == .purchased {
                Debug.print("+++++++++++++++++++ 복원중입니다~~~~~~~~~~~~~~[\(transaction.payment.productIdentifier)]")
                self.finishTransactionSuccess(transaction: transaction, isRestore: true)
            } // for
        } else {
            Debug.print("+++++++++++++++++++ 복원할 아이템이 없습니다~~~~~~~~~~~~~~")
            RLUserDefault.shared.isFihishInappTransaction = true
        }
    }

    // MARK: - IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction: AnyObject in transactions {
            guard let trans = transaction as? SKPaymentTransaction else { return }
            //                mainAsync { RTProgress.shared.hideProgress() }
            switch trans.transactionState {
            case .purchasing:
                //                    mainAsync { RTProgress.shared.showProgress() }
                // 결제 시작
                RLUserDefault.shared.isFihishInappTransaction = false
                purchaseStatusBlock?((alertType: .purchasing, successMessage: ""))
            case .purchased:
                self.finishTransactionSuccess(transaction: trans)
            case .failed:
                RLUserDefault.shared.isFihishInappTransaction = true
                SKPaymentQueue.default().finishTransaction(trans)
                purchaseStatusBlock?((alertType: .failed, successMessage: ""))
            case .deferred:
                RLUserDefault.shared.isFihishInappTransaction = true
                purchaseStatusBlock?((alertType: .deferred, successMessage: ""))
            case .restored:
                SKPaymentQueue.default().finishTransaction(trans)
                RLUserDefault.shared.isFihishInappTransaction = true
            @unknown default:
                ()
            }
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        RLUserDefault.shared.isFihishInappTransaction = true
        print("===================================\(error)")
    }

    // 구매 후 처리 : 구매에 따른 처리
    func finishTransactionSuccess(transaction: SKPaymentTransaction, isRestore: Bool = false) {
        // 구매 영수증 체크
        print("Date_구매 영수증 체크: \(Date())")
        ReceiptHandler.shared.validateReceipt(appURL: Bundle.main.appStoreReceiptURL) { receiptData in
            print("Date_구매 영수증 데이터받어옴: \(Date())")
            self.requestSendReceipt(productId: transaction.payment.productIdentifier, receipt: receiptData, successCallback: {
                print("Date_서버 등록 이후: \(Date())")
                RLUserDefault.shared.isFihishInappTransaction = true
                SKPaymentQueue.default().finishTransaction(transaction)
                if isRestore {
                    self.restoreBlock?()
                } else {
                    self.purchaseStatusBlock?((alertType: .purchased, successMessage: "resultMsg"))
                }
            })
        }
    }

    func requestSendReceipt(productId: String, receipt: String?, successCallback: @escaping () -> Void) {
        let userNo = MainInformation.shared.userNoAes!
        Repository.shared.main
            .getItemNo(userNo: userNo, productId: productId)
            .subscribe(onNext: { itemNo in
                Repository.shared.main
                    .addInappReceipt(userNo: userNo, itemNo: itemNo, receipt: receipt)
                    .subscribe(onNext: { updatedCrown in
                        MainInformation.shared.myCrown.accept(updatedCrown)
                        successCallback()
                    }, onError: {
                        $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                        guard let custom = $0 as? CustomError else { return }
                        if custom.resultCD == RLResultCD.NETWORK_ERROR.code {
                            alertNETWORK_ERR(msg: custom.errorMessage) { }
                        }else if custom.resultCD == RLResultCD.E00000_Error.code {
                            alertERR_E0000Dialog()
                        }else{
                            toast(message: custom.errorMessage, seconds: 1.5) { }
                        }
                        RLUserDefault.shared.isFihishInappTransaction = false
                    })
                    .disposed(by: self.bag)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                RLUserDefault.shared.isFihishInappTransaction = false
            })
            .disposed(by: bag)
    }
}
