//
//  ReceiptHandler.swift
//  Route
//
//  Created by yoseop park on 02/05/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
//import Crashlytics

class ReceiptHandler: NSObject {
    static let shared = ReceiptHandler()
    func validateReceipt(appURL appStoreReceiptURL: URL?, onCompletion: @escaping (String?) -> Void) {
        guard let receipt = createReceiptData(appStoreReceiptURL) else {
            onCompletion(nil)
            return
        }
        onCompletion(receipt)
    }

    fileprivate func createReceiptData(_ appStoreReceiptURL: URL?) -> String? {
        guard let receiptURL = appStoreReceiptURL, let receipt = try? Data(contentsOf: receiptURL) else { return nil }

        let lastReceiptData = receipt.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return lastReceiptData
    }

//    func noticeToFabric(_ message: String, _ inAppId: String, _ isRestore: Bool) {
//        let email = mainInformation.userInformation?.userId ?? ""
//        Answers.logCustomEvent(withName: message, customAttributes: [ "userEmail": "\(email)|\(inAppId)|\(Date().nowString())|\(isRestore ? "Restore": "Buy")"])
//    }
}
