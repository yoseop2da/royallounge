//
//  StoreData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

// Tableview Cell
struct StoreSection {
    var header: String
    var items: [StoreItemModel]
}

extension StoreSection: SectionModelType {
    init(original: StoreSection, items: [StoreItemModel]) {
        self = original
        self.items = items
    }
}

struct StoreItemModel {
    var itemNo, productId: String?
    var crown: Int
    var itemTitle, itemDescription: String? // 이벤트, 실속, 무료아이템
    var korPrice: Int? // 한국가격
    var freeCrownType: FreeCrownType? = nil
    let discount: String?
}


// Tableview Cell
struct StoreHistorySection {
    var header: String
    var items: [StoreHistoryModel]
    var hasNext: Bool
}

extension StoreHistorySection: SectionModelType {
    init(original: StoreHistorySection, items: [StoreHistoryModel]) {
        self = original
        self.items = items
    }
}
