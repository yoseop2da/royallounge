//
//  StoreEventCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/31.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class StoreEventCell: UITableViewCell {
    
    @IBOutlet weak var containerWrapView: UIView!
    @IBOutlet weak var crownLabel: UILabel!
    @IBOutlet weak var eventWrapView: UIView!
    @IBOutlet weak var eventLabel: UILabel!
    @IBOutlet weak var korPriceLabel: UILabel!
    
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var eventDurationLabel: UILabel!
    var cellTouched: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        eventWrapView.layer.cornerRadius = 4.0
        containerWrapView.layer.cornerRadius = 6.0
        containerWrapView.setBorder(color: UIColor.gray10.withAlphaComponent(0.7))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setItem(item: StoreItemModel) {
        crownLabel.text = "\(item.crown)"
        if let event = item.itemTitle {
            
            containerWrapView.setBorder(color: .primary100)
            eventWrapView.isHidden = false
            eventLabel.text = event
        }else{
            containerWrapView.setBorder(color: UIColor.gray10.withAlphaComponent(0.7))
            eventWrapView.isHidden = true
            eventLabel.text = ""
        }
        
        if let duration = item.itemDescription {
            eventDurationLabel.text = "\(duration)"
        }else{
            eventDurationLabel.text = ""
        }
        
        
        if let price = item.korPrice {
            korPriceLabel.text = "\(String.toKorPrice(intValue: price))원"
        }
        
        if let discount = item.discount{
            discountView.isHidden = false
            discountLabel.text = discount
        }else{
            discountView.isHidden = true
        }
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        self.touchAnimation{
            self.cellTouched?()
        }
    }
    
}
