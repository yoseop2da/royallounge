
//
//  StoreFreeCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

//class : UITableViewCell {

class StoreFreeCell: UITableViewCell {

    @IBOutlet weak var containerWrapView: UIView!
    @IBOutlet weak var crownLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    
    var cellTouched: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        containerWrapView.layer.cornerRadius = 6.0
        containerWrapView.setBorder(color: UIColor.gray10.withAlphaComponent(0.7))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setItem(item: StoreItemModel) {
        crownLabel.text = "\(item.crown)"
        
        titleLabel.text = item.itemTitle ?? ""
        
        if let desc = item.itemDescription {
            titleHeight.constant = 18.0
            subTitleLabel.text = desc
        }else{
            titleHeight.constant = 34.0
            subTitleLabel.text = ""
        }
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        self.touchAnimation{
            self.cellTouched?()
        }
    }
    
}
