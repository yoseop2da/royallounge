//
//  StoreHistoryCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class StoreHistoryCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var useLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var crownLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setItem(item: StoreHistoryModel) {
        
//        let dateformatter = DateFormatter()
//        dateformatter.locale = Locale.init(identifier: "ko_KR")
//        dateformatter.dateFormat = "yyyy.MM.dd HH:mm"
//        let date = dateformatter.date(from: item.regDate)
//        dateformatter.dateFormat = "yyyy/MM/dd HH:mm"
        dateLabel.text = item.regDate//dateformatter.string(from: date!)//item.regDate
        
        useLabel.text = item.useType
        titleLabel.text = item.name
        crownLabel.text = "\(item.signType)\(item.crown)"
        
        crownLabel.textColor = item.signType == "-" ? .errerColor : .successColor
    }

}
