//
//  StoreHistoryViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SnapKit

class StoreHistoryViewController: ClearNaviLoggedBaseViewController {
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<StoreHistorySection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var freeCrownButton: UIButton!
    
    var focusFreeItems: (()->Void)?
    
    private var dataSource: DataSource {
            let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> StoreHistoryCell in
                let cell = tableView.dequeueReusableCell(withIdentifier: StoreHistoryCell.className, for: indexPath) as! StoreHistoryCell
                cell.setItem(item: item)
                return cell
            })
            return dataSource
        }
    
    var viewModel: StoreHsitoryViewModel!
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        freeCrownButton.layer.cornerRadius = 6.0
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
          
        refreshControl = UIRefreshControl()
        mainTableView.refreshControl = refreshControl
        
        refreshControl
            .rx.controlEvent(UIControl.Event.valueChanged)
            .subscribe(onNext: { [weak self] in
                self?.refreshControl.endRefreshing()
                self?.viewModel?.reLoadData()
            })
            .disposed(by: bag)
        
        viewModel = StoreHsitoryViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.isListEmpty
            .subscribe(onNext: { isEmpty in
                self.emptyView.isHidden = !isEmpty
                self.mainTableView.alwaysBounceVertical = !isEmpty
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView.rx
            .willDisplayCell
            .subscribe(onNext: {
                print("----------$0.indexPath.row : \($0.indexPath.row)")
                guard let result = self.viewModel.result.value else { return }
                if result.list.count - $0.indexPath.row == 3 /*result count보다 3개 작은 시점에서 새로 호출하겠다*/ {
//                    if self.mainTableView.visibleCells.contains($0.cell) {
                        self.viewModel.loadMoreData()
//                    }
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "크라운 내역"
        
        hideNavigationControllerBottomLine()
    }
    
    @IBAction func findFreeButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.pushBack()
            delay(0.5) {
                self.focusFreeItems?()
            }
        }
    }
}

extension StoreHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 68.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let wrapView = UIView.init()
        let titleLabel = UILabel.init()
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.spoqaHanSansLight(ofSize: 22)
        titleLabel.text = "보유 크라운"
        titleLabel.textColor = .dark
        wrapView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-12)
        }
        if section == 0 {
            let crownButton = UIButton.init()
            crownButton.setImage(UIImage.init(named: "icons24PxCrown"), for: .normal)
            crownButton.setTitle("\(MainInformation.shared.myCrown.value)", for: .normal)
            crownButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 20)
            crownButton.titleLabel?.textAlignment = .right
            crownButton.setTitleColor(.primary100, for: .normal)
            crownButton.contentHorizontalAlignment = .right
            crownButton.contentEdgeInsets.right = 20
            crownButton.imageEdgeInsets.right = 4
            crownButton.isUserInteractionEnabled = false
            wrapView.addSubview(crownButton)
            crownButton.snp.makeConstraints {
                $0.trailing.equalToSuperview()
                $0.top.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-12)
                $0.width.equalTo(100)
            }
        }
        return wrapView
    }
}
