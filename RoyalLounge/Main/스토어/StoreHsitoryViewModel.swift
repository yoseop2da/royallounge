//
//  StoreHsitoryViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class StoreHsitoryViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[StoreHistorySection]>.init(value: [])
    let result = BehaviorRelay<StoreHistoryListResult?>.init(value: nil)
    let isListEmpty = BehaviorRelay<Bool>.init(value: true)

    private var userNo: String!
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        reLoadData()
        
        result
            .filter{ $0 != nil }
            .subscribe(onNext: {
                self.isListEmpty.accept($0!.list.count == 0) //(true)//
                self.sections.accept([StoreHistorySection.init(header: "크라운 내역", items: $0!.list, hasNext: $0!.hasNext)])
            }, onError: { err in
                
            })
            .disposed(by: bag)
    }
    
    func reLoadData() {
        Repository.shared.main
            .getStoreHistoryList(userNo: userNo)
            .subscribe(onNext: { result in
                self.result.accept(result)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
    }
    
    func headerTitle(sectionIdx idx: Int) -> String {
        return sections.value[idx].header
    }
    
    func purchaseItem() {
        let productId: String = ""
        InAppHandler.shared.purchaseMyProduct(productId: productId, callback: { result in
            switch result.alertType {
            case .disabled, .deferred, .failed:
                toast(message: result.alertType.message(), seconds: 1.0)
            case .purchasing: break
            case .purchased:
                //                 구매 성공
                toast(message: "결제가 완료되었습니다.", seconds: 1.0)
            case .didReceive: ()
            }
        })
    }
    
    /// 더보기 구현
    func loadMoreData() {
        let result = self.result.value
        if let hasNext = result?.hasNext, hasNext == true {
            Repository.shared.main
                .getStoreHistoryList(userNo: userNo, lastCrownNo: result?.list.last?.crownNo)
                .subscribe(onNext: { result in
                    var new = self.result.value
                    new?.hasNext = result.list.count == 0 ? false : result.hasNext
                    new?.list.append(contentsOf: result.list)
                    self.result.accept(new)
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

                })
                .disposed(by: bag)
            
        }
    }
    
}
