
//
//  StoreViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class StoreViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[StoreSection]>.init(value: [])
    var hasFreeSection = false
    
    var userNo: String!
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        reloadData()
        // 복원로직
//        if !RLUserDefault.shared.isFihishInappTransaction {
//            restore()
//        }
    }
    
    func restore() {
        Debug.print("++++++++++++ 복원진행중.....")
        InAppHandler.shared.restorePurchase{
            toast(message: "이전 결제에서 지급되지 않은 크라운이\n정상 지급되었습니다.\n(구매내역 복원 완료)", seconds: 2.5)
            MainInformation.shared.updateMyCrownNew()
        }
    }
    
    func reloadData() {
        MainInformation.shared.updateMyCrownNew()
        
        Repository.shared.main
            .getStoreItemList(userNo: self.userNo)
            .subscribe(onNext: { result in
                InAppHandler.shared.fetchAvailableProducts(productIDs: result.itemList.map{ $0.productID })
                
                var newSections = [StoreSection.init(header: "스토어", items:
                    result.itemList.map {
                        StoreItemModel.init(itemNo: $0.itemNo, productId: $0.productID, crown: $0.crown, itemTitle: $0.title, itemDescription: $0.description, korPrice: $0.price, discount: $0.discount)
                })]
                
                if let crownList = result.crownList, crownList.count > 0 {
                    self.hasFreeSection = true
                    newSections.append(StoreSection.init(header: "무료 크라운 받기", items:
                        result.crownList!.map {
                            StoreItemModel.init(itemNo: nil, productId: nil, crown: $0.crown, itemTitle: $0.title, itemDescription: $0.description, korPrice: nil, freeCrownType: $0.freeCrownType, discount: nil)
                    }))
                }
                self.sections.accept(newSections)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func headerTitle(sectionIdx idx: Int) -> String {
        return sections.value[idx].header
    }
    
    func purchaseItem(productId: String?) {
        guard let _productId = productId else { return }
        RLProgress.shared.showCustomLoading()
//        mainAsync {
            InAppHandler.shared.purchaseMyProduct(productId: _productId, callback: { result in
                switch result.alertType {
                case .disabled, .deferred, .failed:
                    RLProgress.shared.hideCustomLoading()
                    toast(message: result.alertType.message(), seconds: 1.0)
                case .purchasing: ()
                case .purchased:
                    print("Date_서버 등록 테이블뷰 리로드하기: \(Date())")
                    RLProgress.shared.hideCustomLoading()
                case .didReceive: ()
                }
            })
//        }
    }
}


