//
//  NotificationCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var cellTouched: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        iconImageView.layer.cornerRadius = 20.0
        iconImageView.setBorder(color: .gray10)
    }
    
    func setItem(_ item: NotificationModel) {
        iconImageView.image = nil
        
        if let content = item.content {
            titleLabel.text = "\(item.title)\n\(content)"
        }else{
            titleLabel.text = item.title
        }
        
        dateLabel.text = item.regDate
        let placeholder = UIImage.init(named: "avataCirclePlaceHolder")
        if let urlString = item.senderPhotoURL {
            mainAsync {
//                let pushType = PushType.convertType(from: item.pushType)
                if !item.viewYn /*돈주고 열어본 카드*/ {
                    self.iconImageView.setBlurImage(urlString: urlString, placeholderImage: placeholder)
                }else{
                    self.iconImageView.setImage(urlString: urlString, placeholderImage: placeholder)
                }
            }
        }else{
            iconImageView.image = UIImage.init(named: "avataCircleRoyalLounge")
        }
        
//        if let url = item.senderPhotoURL {
//            iconImageView.setImage(urlString: url, placeholderImage: UIImage.init(named: "avataCirclePlaceHolder"))
//        }else{
//            iconImageView.image = UIImage.init(named: "avataCircleRoyalLounge")
//        }
    }
    
    @IBAction func touchAction(_ sender: Any) {
        self.touchAnimation {
            self.cellTouched?()
        }
    }

}
