
//
//  NotificationData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

// Tableview Cell
struct NotificationSection {
    var header: String
    var hasNext: Bool = false
    var items: [NotificationModel]
}

extension NotificationSection: SectionModelType {
    init(original: NotificationSection, items: [NotificationModel]) {
        self = original
        self.items = items
    }
}

