
//
//  NotificationListViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class NotificationListViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<NotificationSection>

    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> NotificationCell in
            let cell: NotificationCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item)
            return cell
        })
        return dataSource
    }
    
    var viewModel: NotificationListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        let barButton = RLBarButtonItem.init(barType: .close)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItem = barButton
        
        mainTableView.rx
            .itemSelected
            .bind { indexPath in
                self.mainTableView.deselectRow(at: indexPath, animated: true)
                let item = self.viewModel.getItem(indexPath: indexPath)
                let pushType = PushType.convertType(from: item.pushType)
                switch pushType {
                case .joinIncomplete, .joinApproval, .joinRejcet, .memberSleepRelease, .statusBlock, .statusSuspend, .statusSuspendRelease: ()// 액션 없음
                case .cardLunchMatchOpen:
                    Coordinator.shared.toMain()
                case .cardOk, .cardSuperOk, .cardHigh, .cardEachOtherHigh:
                    // 메인 이동후 > 보관함 > 받은
                    Coordinator.shared.toMain{
                        Coordinator.shared.toSecondTab(idx: 0)
                    }
                case .cardOkSuccess, .cardSuperOkSuccess:
                    // 메인 이동후 > 보관함 > 성공
                    Coordinator.shared.toMain{
                        Coordinator.shared.toSecondTab(idx: 2)
                    }
                case .liveMatchComplete:
                    Coordinator.shared.toMain{
                        Coordinator.shared.toThirdTab()
                    }
                case .statusBlockOverAge, .adminLogout:
                    MainInformation.shared.logout()
                default: ()
                }
            }
            .disposed(by: bag)
        
        viewModel = NotificationListViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView.rx
            .setDelegate(self)
            .disposed(by: bag)
        
        mainTableView.rx
            .willDisplayCell
            .subscribe(onNext: {
                guard let result = self.viewModel.result.value else { return }
                if result.list.count - $0.indexPath.row == 3 /*result count보다 3개 작은 시점에서 새로 호출하겠다*/ {
//                    if self.mainTableView.visibleCells.contains($0.cell) {
                        self.viewModel.loadMoreData()
//                    }
                }
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "알림 내역"
//        self.viewModel?.reloadData()
        hideNavigationControllerBottomLine()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MainInformation.shared.updateRedDot()
    }
    
}

extension NotificationListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
