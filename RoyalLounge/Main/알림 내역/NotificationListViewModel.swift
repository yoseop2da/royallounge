//
//  NotificationListViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class NotificationListViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[NotificationSection]>.init(value: [])
    let result = BehaviorRelay<NotificationListResult?>.init(value: nil)
    // 빈화면 처리
//    let isListEmpty = BehaviorRelay<Bool>.init(value: true)
    
    private var userNo: String!
    init(userNo: String) {
        super.init()
        
        self.userNo = userNo
        
        result
            .filter{ $0 != nil }
            .subscribe(onNext: {
//                self.isListEmpty.accept($0!.list.count == 0)
                self.sections.accept([NotificationSection.init(header: "알림센터", hasNext: $0!.hasNext, items: $0!.list)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
        
        reloadData()
    }
    
    func reloadData() {
        Repository.shared.main
            .getNotificationList(userNo: self.userNo, lastNoticeNo: nil)
            .subscribe(onNext: { result in
                RLUserDefault.shared.noticeBbsRequestDate = Date.requestViewDate()
                self.result.accept(result)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    /// 더보기 구현
    func loadMoreData() {
        let result = self.result.value
        if let hasNext = result?.hasNext, hasNext == true {
            Repository.shared.main
                .getNotificationList(userNo: userNo, lastNoticeNo: result?.list.last?.noticeNo)
                .subscribe(onNext: { result in
                    var new = self.result.value
                    new?.hasNext = result.list.count == 0 ? false : result.hasNext
                    new?.list.append(contentsOf: result.list)
                    self.result.accept(new)
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
            
        }
    }
    
    func getItem(indexPath: IndexPath) -> NotificationModel {
        let item = sections.value[indexPath.section].items[indexPath.row]
        return item
    }
    
//    func headerTitle(sectionIdx idx: Int) -> String {
//        return sections.value[idx].header
//    }

}


