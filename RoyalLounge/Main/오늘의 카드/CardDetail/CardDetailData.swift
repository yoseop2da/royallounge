//
//  CardDetailData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/23.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

enum CardSectionType {
    case messageScoreAboutMe
    case profile
    case interview
    case keyword
}

enum CardCellType {
    case messageIntro(isSend: Bool)
    case matchSuccessIntro
    case phoneNumber
    case sendMessageText(message: String)
    case receiveMessageText(message: String)
    case messageSpace// 40
    case score
    case aboutMe
    
    case profileUnivCo
    case profileBasic(title: String?, value: String?)
    
    case cardPhoto(idx: Int)
    case interview(question: String?, answer: String?)
    
    case keywordTag(title: String, list: [String]?)
    case keywordJobCareer(title: String, value: String?)
}

enum CardType {
    case today //오늘의카드
    case past //지난소개카드
    case sendOk //좋아요보냄
    case receiveOk //좋아요받음
    case sendSuperOk //슈퍼좋아요보냄
    case receiveSuperOk //슈퍼좋아요받음
    case matchSuccessOK //매칭성공
    case matchSuccessSuperOK //매칭성공

    static func type(fromString: String) -> CardType {
        if fromString == "TODAY" { return .today }
        else if fromString == "PAST" { return .past }
        else if fromString == "SEND_OK" { return .sendOk }
        else if fromString == "RECEIVE_OK" { return .receiveOk }
        else if fromString == "SEND_SUPER_OK" { return .sendSuperOk }
        else if fromString == "RECEIVE_SUPER_OK" { return .receiveSuperOk }
        else if fromString == "SUCCESS_OK" { return .matchSuccessOK }
        else if fromString == "SUCCESS_SUPER_OK" { return .matchSuccessSuperOK }
        return .today
    }
}

enum ScoreType {
    case sendHigh //내가높은별점
    case bothHigh //서로높은별점
    case receiveHigh //나에게높은별점
    case none //
    
    static func type(fromString: String?) -> ScoreType {
        if fromString == "SEND_HIGH" { return .sendHigh }
        else if fromString == "BOTH_HIGH" { return .bothHigh }
        else if fromString == "RECEIVE_HIGH" { return .receiveHigh }
        return .none
    }
}

struct CardDetailRow {
    var cellType: CardCellType
    var card: CardDetailResult
}


struct CardDetailSection {
    var sectionType: CardSectionType
    var items: [CardDetailRow]
}

extension CardDetailSection: SectionModelType {
    init(original: CardDetailSection, items: [CardDetailRow]) {
        self = original
        self.items = items
    }
}

