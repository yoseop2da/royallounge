//
//  CardDetailRemovePopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class CardDetailRemovePopup: UIViewController {
    
    @IBOutlet weak var bottomMainView: UIView!
//    @IBOutlet weak var countLabel: UILabel! // 1/5회
    @IBOutlet weak var countInformationLabel: UILabel! // (월5회무료),(모두소진)
    @IBOutlet weak var removeButton: CrownButton!
    
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    private var ANIMATION_TIMEINTERVAL: TimeInterval = 0.2

    private var OUTBUTTON_HIDDEN: CGFloat = 0.0
    private var OUTBUTTON_SHOWN: CGFloat = 1.0

    private var BOTTOM_VIEW_HIDDEN: CGFloat = 500.0
    private var BOTTOM_VIEW_SHOWN: CGFloat = 0.0

    let bag = DisposeBag()
    var viewModel: CardDetailRemovePopupViewModel!
    var matchNo: String!
    var didTouchRemoveButton: (() -> Void)?
    var willMoveToStore: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainAsync {
            self.bottomMainView.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        
        initialize()
        
        viewModel = CardDetailRemovePopupViewModel.init(userNo: MainInformation.shared.userNoAes!, matchNo: matchNo)
        
        viewModel.countResult
            .subscribe(onNext: { result in
                // 카운트 체크
                let useCount: Int = result.useCount
                let mexCount: Int = result.maxCount
//                let fullStr = "\(useCount) / \(mexCount)회"
//                let attrStr = NSMutableAttributedString.init(string: fullStr, attributes: [.foregroundColor : UIColor.dark])
//                attrStr.addAttributes([.foregroundColor : .primary100], range: (fullStr as NSString).range(of: "\(useCount)"))
//                self.countLabel.attributedText = attrStr
                
                // 카운트 정보
                if useCount >= mexCount {
                    self.countInformationLabel.text = "(이번달 무료 휴면 전환을 모두 사용하였습니다)"
                    self.removeButton.crown = result.cardDelCrown
                }else{
                    self.countInformationLabel.text = "(월 \(mexCount)회 무료삭제 가능합니다.)"
                    self.removeButton.crown = 0
                }
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                self.dismiss {
                    self.didTouchRemoveButton?()
                }
            })
            .disposed(by: bag)
        
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.dismiss()
            })
            .disposed(by: bag)
        
        removeButton.setButton(type: .remove)
        
        removeButton.rx.tap
            .rxTouchAnimation(button: removeButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                // 무료시 바로 삭제
//                if self.removeButton.crown == 0 {
//                    self.viewModel.removeCard()
//                    return
//                }
                if self.removeButton.crownMode {
                    self.removeButton.setCrownCountButton()
                    // 부족시 팝업띄우기
                    if self.removeButton.crown.canPayable() {
                        RLPopup.shared.showNormal(description: "카드가 삭제됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                            // 삭제
                            self.viewModel.removeCard()
                        })
                    }else{
                        self.dismiss(animated: false) {
                            self.willMoveToStore?()
                        }
                    }
                }else{
                    self.removeButton.setCrownCountButton()
                }
            })
            .disposed(by: bag)
    }

    override var prefersStatusBarHidden: Bool {
        return !UIScreen.hasNotch
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomViewShowAnimation(timeInterval: ANIMATION_TIMEINTERVAL)
    }

    func initialize() {
        self.bottomPadding.constant = BOTTOM_VIEW_HIDDEN
    }
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.dismiss()
        }
    }
    @IBAction func backButtonTouched(_ sender: Any) {
        self.dismiss()
    }

    var startPositionY: CGFloat?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            startPositionY = position.y
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let _startPositionY = startPositionY {
            let position = touch.location(in: view)
            let gap = position.y - _startPositionY
            let isDownAction = gap > 0
            self.bottomPadding.constant = isDownAction ? gap : gap*(0.2)
            let percent = CGFloat(1.0 - (isDownAction ? ((gap)/BOTTOM_VIEW_HIDDEN) : 0.0))
            let colorAlpha = CGFloat(0.6/*MAX*/ * percent)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(colorAlpha)

        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if bottomPadding.constant > 150 {
            self.dismiss()
        } else {
            self.bottomViewShowAnimation(timeInterval: 0.1)
        }
    }
}

extension CardDetailRemovePopup {

    func dismiss(after: (() -> Void)? = nil) {
        bottomViewHideAnimation(after: after)
    }
    
    func bottomViewShowAnimation(timeInterval: TimeInterval) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = 0.0
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func bottomViewHideAnimation(after: (() -> Void)?) {
        UIView.animate(withDuration: self.ANIMATION_TIMEINTERVAL, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = self.BOTTOM_VIEW_HIDDEN
            self.view.backgroundColor = .clear
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: true, completion: { after?() })
        })
    }

}

