//
//  CardDetailRemovePopupViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardDetailRemovePopupViewModel: BaseViewModel {
    let countResult = PublishSubject<(useCount: Int, maxCount: Int, cardDelCrown: Int)>.init()
    let nextSuccess = PublishSubject<Bool>.init()
    
    private var userNo: String!
    private var matchNo: String!
    init(userNo: String, matchNo: String) {
        self.userNo = userNo
        self.matchNo = matchNo
        super.init()
        
        MainInformation.shared.updateMyCrownNew()
        
        let _countResult = Repository.shared.main
            .getAvailableCardDelCount(userNo: userNo)
        
        let _cardDelCrown = Repository.shared.main
            .getCrown(userNo: userNo)
            .filter{ $0 != nil }
            .map{ $0!.cardDel }
        
        Observable.combineLatest(_countResult, _cardDelCrown) { (useCount: $0.useCount, maxCount: $0.maxCount, cardDelCrown: $1) }
            .subscribe(onNext: {
                self.countResult.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func removeCard() {
        Repository.shared.main
            .removeCard(userNo: self.userNo, matchNo: self.matchNo)
            .subscribe(onNext: {
                MainInformation.shared.updateMyCrownNew()
                self.nextSuccess.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
        .disposed(by: bag)
    }
}
