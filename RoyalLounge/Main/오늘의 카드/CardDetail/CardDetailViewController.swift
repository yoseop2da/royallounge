//
//  CardDetailViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Toaster

class CardDetailViewController: LoggedBaseViewController {
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<CardDetailSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var topInfoLabel: UILabel!
    
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var badgeCountLabel: UILabel!
    
    @IBOutlet weak var badgeWrapView: UIView!
    @IBOutlet weak var badgeWrapViewTop: NSLayoutConstraint!
    @IBOutlet weak var badgeWrapViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var badgeDescriptionWrapView: UIView!
    @IBOutlet weak var badgeDescriptionLabel: UILabel!
    
    @IBOutlet weak var profileScrollWrapView: UIView!
    
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    // 하단 버튼뷰
    @IBOutlet weak var bottomButtonWrapView: UIView!
    @IBOutlet weak var bottomBoxViewHeight: NSLayoutConstraint!
    @IBOutlet weak var okGuideButton: UIButton!
    @IBOutlet weak var okButton: CrownButton!
    @IBOutlet weak var superOKButton: CrownButton!
    @IBOutlet weak var superOKLongButton: CrownButton!
    
    var matchNo: String?
    var isReceiveHigh: Bool = false // 내가 받은 높은 카드를 통해서 들어온 경우에만 true로 해주기
    //
    var outCallback: ((_ refreshType: CardDetailViewModel.CardRefreshType) -> Void)?
    
    private var profileScrollView: ProfileScrollView!
    private var specBadgeListView: SpecBadgeScrollView!
    private var isLottiPlayed: Bool = false
    private var reloadTagList: [String] = [] // tagView 완료 시점에 height다시 잡아주기위해 cell reload 땜빵코드,,,,,
    private var cacheMsg: String = ""
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            switch item.cellType {
            case .messageIntro(let isSend):
                let cell: MessageIntroCell = tableView.dequeueReusable(for: indexPath)
                if let cardType = item.card.cardTypeEnum {
                    cell.setData(isSend: isSend, cardType: cardType)
                }
                
                cell.play()
                return cell
            case .matchSuccessIntro:
                let cell: MatchSuccessIntroCell = tableView.dequeueReusable(for: indexPath)
                cell.play()
                return cell
            case .phoneNumber:
                let cell: PhoneNumberCell = tableView.dequeueReusable(for: indexPath)
                var phoneOpenCrown: Int = 0
                if let cardType = item.card.cardTypeEnum {
                    if cardType == .matchSuccessOK {
                        phoneOpenCrown = self.viewModel.crownInfo.value?.okConfirm ?? 5
                    }else if cardType == .matchSuccessSuperOK {
                        phoneOpenCrown = self.viewModel.crownInfo.value?.superOkConfirm ?? 0
                    }
                }
                if let dDay = item.card.dDay {
                    cell.setData(crown: phoneOpenCrown, mbNo: item.card.mbNo, dDay: dDay)
                }
                cell.openPhoneNumber = {
                    // 연락처 오픈 호출하기
                    if phoneOpenCrown == 0 {
                        self.viewModel.openPhoneNumber()
                        return
                    }
                    
                    if phoneOpenCrown.canPayable() {
                        RLPopup.shared.showNormal(description: "크라운 \(phoneOpenCrown)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                            self.viewModel.openPhoneNumber()
                        })
                    }else{
                        RLPopup.shared.showMoveStore {
                            // 스토어 이동
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        }
                    }
                }
                return cell
            case .sendMessageText(let message):
                let cell: SendMessageTextCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(message: message)
                return cell
            case .receiveMessageText(let message):
                let cell: ReceiveMessageTextCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(imageUrl: item.card.photoList.compactMap{$0}.first!, message: message)
                return cell
            case .messageSpace:
                let cell: MessageSpaceCell = tableView.dequeueReusable(for: indexPath)
                return cell
            case .score:
                let cell: ScoreCell = tableView.dequeueReusable(for: indexPath)
                cell.didTouchStarButton = { selectedStarPoint in
                    if let score = selectedStarPoint {
                        self.viewModel.addScore(score) {
                            if !$0 {
                                cell.initialStar()
                            }
                        }
                    }
                }
                return cell
            case .aboutMe:
                let cell: AboutMeCell = tableView.dequeueReusable(for: indexPath)
                cell.aboutMeLabel.text = item.card.aboutMe
                return cell
            case .profileUnivCo:
                let university = item.card.university
                let company = item.card.company
                if university != nil && company != nil {
                    let cell: ProfileUnivCo2Cell = tableView.dequeueReusable(for: indexPath)
                    cell.setCard(item.card)
                    return cell
                }else{
                    let cell: ProfileUnivCo1Cell = tableView.dequeueReusable(for: indexPath)
                    cell.setCard(item.card)
                    return cell
                }
            case .profileBasic(let title, let value):
                let cell: ProfileBasicCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(title: title ?? "", value: value ?? "")
                return cell
            case .cardPhoto(let idx):
                let cell: CardPhotoCell = tableView.dequeueReusable(for: indexPath)
                let urls = item.card.photoList.compactMap{$0}
                if urls.count > idx {
                    cell.profileImageView.setImage(urlString: urls[idx])
                }else{
                    cell.profileImageView.image = UIImage.init(named: "mainCardPlaceHolder375")
                }
                return cell
            case .interview(let question, let answer):
                let cell: InterViewCell = tableView.dequeueReusable(for: indexPath)
                cell.questionLabel.text = question
                cell.answerLabel.text = answer
                return cell
            case .keywordTag(let title, let list):
                let cell: KeywordTagCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(title: title, list: list)
                return cell
            case .keywordJobCareer(let title, let value):
                let cell: KeywordJobCareerCell = tableView.dequeueReusable(for: indexPath)
                
                cell.titleLabel.text = title
                cell.jobCareerLabel.text = value
                return cell
            }
        })
        return dataSource
    }
    
    var viewModel: CardDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.swipeBackAnyWhere()
        
        nicknameLabel.text = ""
        topInfoLabel.text = ""
        superOKLongButton.isHidden = true
        badgeImageView.isHidden = true
        badgeCountLabel.isHidden = true
        badgeDescriptionWrapView.layer.cornerRadius = 6
        badgeDescriptionWrapView.alpha = 0.0
        
        backButton.rx.tap
            .rxTouchAnimation(button: backButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if MainInformation.shared.outTester ||
                    self.matchNo == nil ||
                    !self.viewModel.isScoreZero {
                    self.pushBack()
                    return
                }
                
                // 평가 안한경우
                let vc = CardDetailOutPopup.init(nibName: CardDetailOutPopup.className, bundle: nil)
                vc.didTouchOutButton = { selectedStarPoint in
                    if let score = selectedStarPoint {
                        // API CALL
                        self.viewModel.addScore(score) {
                            if $0 {
                                self.pushBack()
                            }
                        }
                    } else {
                        self.pushBack()
                    }
                }
                self.clearPresent(vc)
            })
            .disposed(by: bag)
        
        reportButton.rx.tap
            .rxTouchAnimation(button: reportButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                guard let userNo = self.viewModel.cardDetail.value?.userNo else {
                    print("[Debug] userNo가 없습니다.")
                    return
                }
                let vc = RLStoryboard.todayCard.reportViewController(cardUserNo: userNo, cardNickname: self.viewModel.cardNickname)
                self.modal(UINavigationController.init(rootViewController: vc), animated: true)
            })
            .disposed(by: bag)
        
        removeButton.rx.tap
            .rxTouchAnimation(button: removeButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                guard let matchNo = self.viewModel.cardDetail.value?.matchNo else {
                    print("[Debug] matchNo가 없습니다.")
                    return
                }
                let vc = CardDetailRemovePopup.init(nibName: CardDetailRemovePopup.className, bundle: nil)
                vc.matchNo = matchNo
                vc.didTouchRemoveButton = {
                    self.viewModel.cardRefreshType = .deleted
                    self.pushBack()
                    toast(message: "삭제되었습니다", seconds: 1.5)
                }
                vc.willMoveToStore = {
                    RLPopup.shared.showMoveStore {
                        // 스토어 이동
                        let vc = RLStoryboard.main.storeViewController()
                        self.push(vc)
                    }
                }
                self.clearPresent(vc)
            })
            .disposed(by: bag)
        
        specBadgeListView = SpecBadgeScrollView.init(specList: [])
        specBadgeListView.touchAction = { spec in
            self.showBadgeToast(title: spec.title ?? "", desc: spec.description ?? "")
        }
        badgeWrapView.addSubview(specBadgeListView)
        if UIScreen.safeAreaTop > 44 {
            // iPhone 11이 48이고 그외 디바이스는 확인 불가능함
            badgeWrapViewTop.constant = UIScreen.safeAreaTop * (-1)
        }
        
        viewModel = CardDetailViewModel(userNo: MainInformation.shared.userNoAes!, matchNo: matchNo)
        
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.cardDetail
            .filter{ $0 != nil }.map{ $0! }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { card in
                if let cardType = card.cardTypeEnum {
                    self.setBottomButtons(cardType: cardType)
                }
                
                self.nicknameLabel.text = card.nickname
                self.topInfoLabel.text = card.topInfoString
                if let specList = card.specList {
                    self.specBadgeListView.updateSpecList(specList: specList)
                    self.badgeCountLabel.text = "\(specList.count)"
                    self.badgeImageView.isHidden = false
                    self.badgeCountLabel.isHidden = false
                }else{
                    self.specBadgeListView.updateSpecList(specList: [])
                    self.badgeImageView.isHidden = true
                    self.badgeCountLabel.isHidden = true
                    let reduceHeight: CGFloat = UIScreen.hasNotch ? 79.0 : 34.0
                    self.badgeWrapViewHeight.constant = 134 - reduceHeight
                    self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 195 - reduceHeight + UIScreen.width)
                    self.mainTableView.reloadData()
                }
                self.profileScrollWrapView.subviews.forEach { $0.removeFromSuperview() }
                let profileScrollView = ProfileScrollView.init(imageUrls: card.photoList.compactMap{$0})
                self.profileScrollWrapView.addSubview(profileScrollView)
                
                if !MainInformation.shared.outTester {
                    profileScrollView.drawScoreLabel(scoreType: card.scoreType)
                }
                
                if let crown = self.viewModel.crownInfo.value {
                    if self.isReceiveHigh {
                        self.okButton.crown = crown.okHigh
                    }else{
                        if let dDay = card.dDay {
                            if dDay < 7 {
                                self.okButton.crown = crown.okPast
                            }else{
                                self.okButton.crown = crown.ok
                            }
                        }
                    }
                    self.superOKButton.crown = crown.superOk
                    if let cardType = card.cardTypeEnum {
                        if cardType == .sendOk {
                            if let dDay = card.dDay {
                                if dDay < 7 {
                                    self.superOKLongButton.crown = crown.superOk
                                }else{
                                    self.superOKLongButton.crown = crown.superOkPast
                                }
                            }
                            
                        }else if cardType == .receiveOk {
                            self.superOKLongButton.crown = crown.okAccept
                        }else if cardType == .receiveSuperOk {
                            self.superOKLongButton.crown = crown.superOkAccept
                        }
                    }
                }
            })
            .disposed(by: bag)
        
        okGuideButton.rx.tap
            .rxTouchAnimation(button: okGuideButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.todayCard.oKGuideViewController()
                self.modal(vc, animated: true)
            })
            .disposed(by: bag)
        
        okButton.rx.tap
            .rxTouchAnimation(button: okButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.superOKButton.finishCrownButtonMode()
                if self.okButton.crownMode {
                    self.okButton.setCrownCountButton()
                    if self.okButton.crown.canPayable() {
                        // OK보내기
                        if let cardDetail = self.viewModel.cardDetail.value {
                            let vc = OkSendPopup.init(nibName: OkSendPopup.className, bundle: nil)
                            vc.cacheMessage = self.cacheMsg
                            vc.messageCacheCallback = { cacheMsg in
                                self.cacheMsg = cacheMsg
                            }
                            if self.isReceiveHigh {
                                vc.okType = .okHigh
                            }else{
                                if let dDay = cardDetail.dDay {
                                    if dDay < 7 {
                                        vc.okType = .okPast
                                    }else{
                                        vc.okType = .ok
                                    }
                                }
                                
                            }
                            vc.matchNo = cardDetail.matchNo!
                            vc.nickname = cardDetail.nickname
                            vc.didSendOK = { result in
                                CompleteView.init(type: .sendOk).show {
                                    self.viewModel.setResult(result)
                                }
                            }
                            self.clearPresent(vc)
                        }
                    }else{
                        RLPopup.shared.showMoveStore {
                            // 스토어 이동
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        }
                    }
                }else{
                    self.okButton.setCrownCountButton()
                }
            })
            .disposed(by: bag)
        
        superOKButton.rx.tap
            .rxTouchAnimation(button: superOKButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.okButton.finishCrownButtonMode()
                if self.superOKButton.crownMode {
                    self.superOKButton.setCrownCountButton()
                    if let cardDetail = self.viewModel.cardDetail.value {
                        if self.superOKButton.crown.canPayable() {
                            // 슈퍼OK 보내기
                            let vc = OkSendPopup.init(nibName: OkSendPopup.className, bundle: nil)
                            vc.cacheMessage = self.cacheMsg
                            vc.messageCacheCallback = { cacheMsg in
                                self.cacheMsg = cacheMsg
                            }
                            if let dDay = cardDetail.dDay {
                                if dDay < 7 {
                                    vc.okType = .superOkPast
                                }else{
                                    vc.okType = .superOk
                                }
                            }
                            vc.matchNo = cardDetail.matchNo!
                            vc.nickname = cardDetail.nickname
                            vc.didSendOK = { result in
                                CompleteView.init(type: .sendSuperOk).show {
                                    self.viewModel.setResult(result)
                                }
                            }
                            self.clearPresent(vc)
                        }else{
                            RLPopup.shared.showMoveStore {
                                // 스토어 이동
                                let vc = RLStoryboard.main.storeViewController()
                                self.push(vc)
                            }
                        }
                    }
                }else{
                    self.superOKButton.setCrownCountButton()
                }
            })
            .disposed(by: bag)
        
        superOKLongButton.rx.tap
            .rxTouchAnimation(button: superOKLongButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if let cardDetail = self.viewModel.cardDetail.value {
                    // 한번더슈퍼OK보내기 또는 수락하기
                    if self.superOKLongButton.crownMode {
                        self.superOKLongButton.setCrownCountButton()
                        if self.superOKLongButton.crown.canPayable() {
                            let vc = OkSendPopup.init(nibName: OkSendPopup.className, bundle: nil)
                            vc.cacheMessage = self.cacheMsg
                            vc.messageCacheCallback = { cacheMsg in
                                self.cacheMsg = cacheMsg
                            }
                            var type: CompleteView.CompleteType = .receiveSuperOk
                            if self.superOKLongButton.crownButtonType == .receiveOk {
                                vc.okType = .receiveOk
                                type = .receiveOk
                            } else if self.superOKLongButton.crownButtonType == .receiveSuperOk {
                                vc.okType = .receiveSuperOk
                                type = .receiveSuperOk
                            } else if self.superOKLongButton.crownButtonType == .sendSuperOkOneMore {
                                if let dDay = cardDetail.dDay {
                                    if dDay < 7 {
                                        vc.okType = .superOkOneMorePast
                                    }else{
                                        vc.okType = .superOkOneMore
                                    }
                                }
                                type = .sendSuperOk
                            }
                            
                            vc.matchNo = cardDetail.matchNo!
                            vc.nickname = cardDetail.nickname
                            vc.didSendOK = { result in
                                CompleteView.init(type: type).show {
                                    self.viewModel.setResult(result)
                                }
                            }
                            self.clearPresent(vc)
                        }else{
                            RLPopup.shared.showMoveStore {
                                // 스토어 이동
                                let vc = RLStoryboard.main.storeViewController()
                                self.push(vc)
                            }
                        }
                    }else{
                        self.superOKLongButton.setCrownCountButton()
                    }
                }
            })
            .disposed(by: bag)
        
        mainAsync {
            self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 195 + UIScreen.width)
            self.mainTableView.reloadData()
            if self.matchNo == nil {
                self.mainTableView.tableFooterView = nil
                self.bottomBoxViewHeight.constant = 0.0
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // http://redmine.routedate.com/redmine/issues/513 이슈 수정을 위한 처리인데,,, 이게 왜 이동되는지 모르겠네,,,,
        if let specList = viewModel.cardDetail.value?.specList {
            self.specBadgeListView.updateSpecList(specList: specList)
            self.badgeCountLabel.text = "\(specList.count)"
            self.badgeImageView.isHidden = false
            self.badgeCountLabel.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.outCallback?(self.viewModel.cardRefreshType)
    }
    
    override func detectedScreenShot() {
        super.detectedScreenShot()
        RLPopup.shared.detectCapture(rightAction: {})
    }
    
    @objc
    func finishSpecBadgeToast() {
        specBadgeToastTimer?.invalidate()
        specBadgeToastTimer = nil
        UIView.animate(withDuration: 1.0) {
            self.badgeDescriptionWrapView.alpha = 0.0
        }
    }
    
    var specBadgeToastTimer: Timer?
    func showBadgeToast(title: String, desc: String) {
        specBadgeToastTimer?.invalidate()
        specBadgeToastTimer = nil
        
        specBadgeToastTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(finishSpecBadgeToast), userInfo: nil, repeats: false)
        
        let fullStr = "\(title)\n\(desc)"
        let attrStr = NSMutableAttributedString.init(string: fullStr)
        let boldRange = (fullStr as NSString).range(of: title)
        attrStr.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 12)], range: boldRange)
        self.badgeDescriptionLabel.attributedText = attrStr
        self.badgeDescriptionWrapView.alpha = 0.0
        UIView.animate(withDuration: 0.3) {
            self.badgeDescriptionWrapView.alpha = 0.85
        }
    }
    
    func hideBadgeToast(animated: Bool) {
        if self.badgeDescriptionWrapView.alpha == 0.0 { return }
        if animated {
            UIView.animate(withDuration: 1.0) {
                self.badgeDescriptionWrapView.alpha = 0.0
            }
        }else{
            self.badgeDescriptionWrapView.alpha = 0.0
        }
    }
    
    func setBottomButtons(cardType: CardType) {
        bottomButtonWrapView.isHidden = false
        self.bottomButtonWrapView.alpha = 1.0
        
        okButton.isHidden = true
        superOKButton.isHidden = true
        superOKLongButton.isHidden = true
        
        switch cardType {
        case .today, .past:
            // [더블] 기본
            okButton.isHidden = false
            superOKButton.isHidden = false
            
            okButton.setButton(type: .sendOk)
            superOKButton.setButton(type: .sendSuperOk)
        case .sendOk:
            superOKLongButton.isHidden = false
            superOKLongButton.setButton(type: .sendSuperOkOneMore)
        case .receiveOk:
            superOKLongButton.isHidden = false
            superOKLongButton.setButton(type: .receiveOk)
        case .receiveSuperOk:
            superOKLongButton.isHidden = false
            superOKLongButton.setButton(type: .receiveSuperOk)
        case .sendSuperOk, .matchSuccessOK, .matchSuccessSuperOK:
            bottomBoxViewHeight.constant = 0.0 // 매치 성공인경우에만
        }
    }
}

extension CardDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hideBadgeToast(animated: false)
        if scrollView.contentOffset.y >= 180 {
            if !isLottiPlayed {
                if let isMatchedSuccess = viewModel.cardDetail.value?.isMatchedSuccess, isMatchedSuccess == true {
                    isLottiPlayed = true
                    let lottiView = MatchSuccessLottiView()
                    self.view.addSubview(lottiView)
                    lottiView.start(finishCallback: {
                        lottiView.removeFromSuperview()
                    })
                }
            }
        }
    }
}

extension CardDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if viewModel.showHeader(section: section) {
            return 143
        }
        return .zero
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return CardDetailHeaderView.init(image: viewModel.headerImage(section: section))
    }
}

private class CardDetailHeaderView: UIView {
    init(image: UIImage?) {
        super.init(frame: CGRect.zero)
//        let wrapView = UIView.init()
        self.backgroundColor = .clear
        let line = UIView.init()
        line.backgroundColor = UIColor.rlBrown230
        self.addSubview(line)
        
        let imageView = UIImageView.init(frame: .zero)
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.image = image
        self.addSubview(imageView)
        
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(60)
            $0.bottom.equalToSuperview().offset(-30)
            $0.trailing.equalToSuperview().offset(-20)
        }
        line.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(2)
            $0.centerY.equalTo(imageView.snp.centerY)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
