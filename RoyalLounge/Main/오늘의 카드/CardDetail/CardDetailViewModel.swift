//
//  CardDetailViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/23.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Toaster

class CardDetailViewModel: BaseViewModel {

    enum CardRefreshType {
        case changedScore // 카드정보: 스코어 -> 현재 카드정보업데니트
        case changedRefresh // 카드갱신 -> 받은/보낸/성공 모두 리로드
        case deleted // 현재 카드카테고리 갱신
        case none
    }
    let cardDetail = BehaviorRelay<CardDetailResult?>.init(value: nil)
    let sections = BehaviorRelay<[CardDetailSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let crownInfo = BehaviorRelay<CrownModel?>.init(value: nil)
    
    var cardNickname: String { cardDetail.value?.nickname ?? ""}
    var isScoreZero: Bool {
        if let score = cardDetail.value?.score {
            return score == 0
        }
        return true
    }
    var userNo: String!
    var matchNo: String?
    
    var cardRefreshType: CardRefreshType = .none
    var hasRemoved: Bool = false
    
    init(userNo: String, matchNo: String?) {
        super.init()
        self.userNo = userNo
        self.matchNo = matchNo
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .subscribe(onNext: {
                self.crownInfo.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        MainInformation.shared.updateMyCrownNew()
        
        if let _matchNo = matchNo {
            // 상대방 카드 조회
            Repository.shared.main
                .getCardDetail(userNo: userNo, matchNo: _matchNo)
                .subscribe(onNext: {
                    if let card = $0 {
                        self.cardDetail.accept(card)
                    }
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
        }else{
            // 내 카드 조회
            Repository.shared.main
                .getMyCardDetail(userNo: userNo)
                .subscribe(onNext: {
                    if let card = $0 {
                        self.cardDetail.accept(card)
                    }
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
                .disposed(by: bag)
        }
        
        cardDetail.filter{ $0 != nil }.map{ $0! }
            .map{ card in
                let interviewList = card.interviewList ?? []
                var datasource: [CardDetailSection] = []
                var items: [CardDetailRow] = []
                
                items.removeAll()
                if let cardType = card.cardTypeEnum {
                    switch cardType {
                    case .matchSuccessOK, .matchSuccessSuperOK:
                        // 매칭 성공
                        if let acceptMsg = card.acceptOkMsg {
                            items.append(CardDetailRow.init(cellType: .matchSuccessIntro, card: card))
                            items.append(CardDetailRow.init(cellType: .phoneNumber, card: card))
                            if let msg = card.sendOkMsg {
                                items.append(CardDetailRow.init(cellType: .sendMessageText(message: msg), card: card))
                                items.append(CardDetailRow.init(cellType: .receiveMessageText(message: acceptMsg), card: card))
                            }else if let msg = card.receiveOkMsg {
                                items.append(CardDetailRow.init(cellType: .receiveMessageText(message: msg), card: card))
                                items.append(CardDetailRow.init(cellType: .sendMessageText(message: acceptMsg), card: card))
                            }
                            items.append(CardDetailRow.init(cellType: .messageSpace, card: card))
                        }
                    case .sendOk, .sendSuperOk:
                        // 내가 보냄
                        if let msg = card.sendOkMsg {
                            items.append(CardDetailRow.init(cellType: .messageIntro(isSend: true), card: card))
                            items.append(CardDetailRow.init(cellType: .sendMessageText(message: msg), card: card))
                            items.append(CardDetailRow.init(cellType: .messageSpace, card: card))
                        }
                    case .receiveOk, .receiveSuperOk:
                        // 내가 받음
                        if let msg = card.receiveOkMsg {
                            items.append(CardDetailRow.init(cellType: .messageIntro(isSend: false), card: card))
                            items.append(CardDetailRow.init(cellType: .receiveMessageText(message: msg), card: card))
                            items.append(CardDetailRow.init(cellType: .messageSpace, card: card))
                        }
                    default: ()
                    }
                }
                
                if self.matchNo != nil {
                    if let score = card.score {
                        if score == 0 { items.append(CardDetailRow.init(cellType: .score, card: card)) }
                    }else{
                        items.append(CardDetailRow.init(cellType: .score, card: card))
                    }
                }
                items.append(CardDetailRow.init(cellType: .aboutMe, card: card))
                datasource.append(CardDetailSection.init(sectionType: .messageScoreAboutMe, items: items))
                
                //------------ profile
                items.removeAll()
                if card.university != nil || card.company != nil {
                    items.append(CardDetailRow.init(cellType: .profileUnivCo, card: card))
                }
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "나이", value: "\(card.age)"), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "키", value: "\(card.tall)cm"), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "체형", value: card.body), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "주량", value: card.drink), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "종교", value: card.religion), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "성격", value: card.charList.joined(separator: ", ")), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "지역", value: card.areaName), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "학력", value: card.edu), card: card))
                items.append(CardDetailRow.init(cellType: .profileBasic(title: "직업", value: card.job), card: card))
                datasource.append(CardDetailSection.init(sectionType: .profile, items: items))
                
                //------------ interView
                items.removeAll()
                (0..<5).forEach { idx in
                    if card.photoList.count > idx + 1 {
                        items.append(CardDetailRow.init(cellType: .cardPhoto(idx: idx+1), card: card))
                    }
                    if interviewList.count > idx {
                        let interview = interviewList[idx]
                        items.append(CardDetailRow.init(cellType: .interview(question: interview.question, answer: interview.answer), card: card))
                    }
                }
                datasource.append(CardDetailSection.init(sectionType: .interview, items: items))
                
                //------------ keyword
                items.removeAll()
                if let value = card.hobbyList { items.append(CardDetailRow.init(cellType: .keywordTag(title: "취미", list: value), card: card)) }
                if let value = card.interestList { items.append(CardDetailRow.init(cellType: .keywordTag(title: "관심사", list: value), card: card)) }
                if let value = card.haveList { items.append(CardDetailRow.init(cellType: .keywordTag(title: "I have", list: value), card: card)) }
                if let value = card.wantList { items.append(CardDetailRow.init(cellType: .keywordTag(title: "I want", list: value), card: card)) }
                if let value = card.jobCareer { items.append(CardDetailRow.init(cellType: .keywordJobCareer(title: "# Job Career", value: value), card: card)) }
                if items.count > 0 { datasource.append(CardDetailSection.init(sectionType: .keyword, items: items)) }
                return datasource
            }
            .bind(to: sections)
            .disposed(by: bag)
    }
    
    func setResult(_ result: CardDetailResult) {
        MainInformation.shared.updateMyCrownNew()
        
        cardRefreshType = .changedRefresh
//        self.cardDetail.accept(result)
        var _cardDetail = self.cardDetail.value
        _cardDetail?.score = result.score
        _cardDetail?.scoreType = result.scoreType
        _cardDetail?.cardType = result.cardType
        _cardDetail?.receiveOkMsg = result.receiveOkMsg
        _cardDetail?.sendOkMsg = result.sendOkMsg
        _cardDetail?.acceptOkMsg = result.acceptOkMsg
        self.cardDetail.accept(_cardDetail)
    }
    
    func addScore(_ score: Int, callBack: ((Bool) -> Void)?) {
        guard let matchNo = matchNo else { return }
        mainAppDelegate.showBlockView()
        Repository.shared.main
            .addScore(userNo: userNo, matchNo: matchNo, score: score)
            .subscribe(onNext: { result in
                mainAppDelegate.hideBlockView()
                self.cardRefreshType = .changedScore
                if result.rewardYn {
                    MainInformation.shared.updateMyCrownNew()
                    toast(message: "크라운 1개 적립되었습니다", seconds: 1.5)
                }
                var _cardDetail = self.cardDetail.value
                _cardDetail?.score = result.cardDetail.score
                _cardDetail?.scoreType = result.cardDetail.scoreType
                self.cardDetail.accept(_cardDetail)
                callBack?(true)
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                callBack?(false)
            })
        .disposed(by: bag)
    }
    
    func openPhoneNumber() {
        guard let matchNo = matchNo else { return }
        Repository.shared.main
            .openMbNo(userNo: userNo, matchNo: matchNo)
            .subscribe(onNext: { result in
                MainInformation.shared.updateMyCrownNew()
                self.cardRefreshType = .none
                var _cardDetail = self.cardDetail.value
                _cardDetail?.mbNo = result.mbNo
                self.cardDetail.accept(_cardDetail)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func showHeader(section: Int) -> Bool {
        return sections.value[section].sectionType != CardSectionType.messageScoreAboutMe
    }
    
    func headerImage(section: Int) -> UIImage? {
        let type = sections.value[section].sectionType
        if type == .profile {
            return UIImage.init(named: "userInfoProfile")
        }else if type == .interview {
            return UIImage.init(named: "userInfoInterview")
        }else if type == .keyword {
            return UIImage.init(named: "userInfoKeyword")
        }
        return nil
    }
}
