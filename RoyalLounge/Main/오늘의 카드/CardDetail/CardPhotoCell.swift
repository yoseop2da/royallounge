//
//  CardPhotoCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardPhotoCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
