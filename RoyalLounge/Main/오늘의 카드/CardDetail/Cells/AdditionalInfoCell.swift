//
//  AdditionalInfoCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AdditionalInfoCell: ProfileUnivCoBaseCell {

    @IBOutlet weak var crownWrapView: UIView!
    @IBOutlet weak var emptyWrapView: UIView!
    
    @IBOutlet weak var crownLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        crownWrapView.layer.cornerRadius = 15.0
    }
    
    func setCrown(_ crown: Int = 5) {
        crownLabel.text = "x\(crown) 적립"
    }
}
