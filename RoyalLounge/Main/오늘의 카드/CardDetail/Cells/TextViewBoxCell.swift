//
//  TextViewBoxCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class TextViewBoxCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var wrapView: UIView!
    
    var bag = DisposeBag()
//    var myInfoEditAction: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapView.layer.cornerRadius = 6.0
//        textView.textContainerInset.top = 0
//        textView.rx.tapGesture()
//            .filter{ $0.state == .ended }
//            .bind { gesture in
//                print("tapGesture.state : \(gesture.state)")
//                self.myInfoEditAction?()
//            }
//            .disposed(by: bag)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
