//
//  KeywordJobCareerCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class KeywordJobCareerCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var jobCareerLabel: UILabel!
    @IBOutlet weak var wrapView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapView.layer.cornerRadius = 6.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
