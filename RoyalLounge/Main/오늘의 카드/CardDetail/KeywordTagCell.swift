//
//  KeywordTagCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class KeywordTagCell: UITableViewCell {

    enum KeywordTagType {
        case hobby
        case interest
        case have
        case want
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var tagListViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tagListView.backgroundColor = .clear
        tagListView.textFont = UIFont.spoqaHanSansBold(ofSize: 14.0)
        tagListView.paddingX = 24
        tagListView.paddingY = 8
        tagListView.marginX = 10
        tagListView.marginY = 20
        tagListView.cornerRadius = 15
        tagListView.borderWidth = 1.0
        tagListView.borderColor = UIColor.brown75
        tagListView.textColor = UIColor.brown75
        tagListView.tagBackgroundColor = UIColor.clear
        tagListView.tagHighlightedBackgroundColor = UIColor.gray50
        tagListView.tagSelectedBackgroundColor = UIColor.brown75
        
        tagListView.alignment = .left
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(title: String, list: [String]?) {
        titleLabel.text = "# \(title)"
        self.tagListView.removeAllTags()
        if let list = list {
            let _ = self.tagListView.addTags(list)
            self.tagListView.tagViews
                .forEach {
                    $0.isUserInteractionEnabled = false
                    $0.isSelected = true
                }
        }
        self.tagListViewHeight.constant = self.tagListView.intrinsicContentSize.height
    }
}
