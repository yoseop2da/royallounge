//
//  MatchSuccessIntroCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class MatchSuccessIntroCell: UITableViewCell {
    
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var heartImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        let fullStr = "매칭 성공을 축하드립니다!"
        let attrStr = NSMutableAttributedString.init(string: fullStr, attributes:
            [.font : UIFont.spoqaHanSansBold(ofSize: 22), .foregroundColor : UIColor.gray75])
        attrStr.addAttributes([.foregroundColor : UIColor.primary100], range: (fullStr as NSString).range(of: "매칭 성공"))
        descLabel.attributedText = attrStr
        
        startAnimate()
    }
    
    func startAnimate() {
        // timer 달아서 처리해줘야 바운스가 계속 됨!!!!
        self.heartImageView.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
        delay(0.1) {
            UIView.animate(withDuration: 1.5, delay: 0.0, options: [.autoreverse, .repeat], animations: {
                self.heartImageView.transform = .identity
            }) { _ in
                
            }
        }
    }
    
    func play() {
        startAnimate()
    }
    
}
