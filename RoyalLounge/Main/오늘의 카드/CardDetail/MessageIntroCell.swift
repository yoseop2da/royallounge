//
//  MessageIntroCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Lottie

class MessageIntroCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var lottiWrapView: UIView!
    private var lopttiView: AnimationView?
//    motion_ok_30frame
//    motion_super_ok_65frame
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lottiWrapView.backgroundColor = .clear
    }

    func play() {
        if let lotti = self.lopttiView {
            if !lotti.isAnimationPlaying {
                lotti.play()
            }
        }
    }
    
    func setData(isSend: Bool, cardType: CardType) {
        var okStr: String = "OK"
        var lottiName: String = "motion_ok_30frame"
        
        switch cardType {
        case .sendSuperOk, .receiveSuperOk:
            okStr = "슈퍼OK"
            lottiName = "motion_super_ok_65frame"
        case .sendOk, .receiveOk:
            okStr = "OK"
            lottiName = "motion_ok_30frame"
        default:()
        }
        let fullStr = isSend ? "내가 \(okStr) 보낸 상대입니다." : "나에게 \(okStr) 보낸 상대입니다."
        let attrStr = NSMutableAttributedString.init(string: fullStr, attributes:
            [.font : UIFont.spoqaHanSansBold(ofSize: 22), .foregroundColor : UIColor.gray75])
        attrStr.addAttributes([.foregroundColor : UIColor.primary100], range: (fullStr as NSString).range(of: okStr))
        descLabel.attributedText = attrStr
        
//        if self.lopttiView == nil {
        lottiWrapView.removeAllSubView()
        let lopttiView = AnimationView.init(name: lottiName)
        self.lopttiView = lopttiView
        lopttiView.frame = CGRect.init(x: 0, y: 0, width: 120 * 2, height: 90 * 2)
        lopttiView.center = CGPoint.init(x: 60, y: 45)
        lopttiView.loopMode = .loop
        lottiWrapView.addSubview(lopttiView)
//        }
        
        play()
    }
}
