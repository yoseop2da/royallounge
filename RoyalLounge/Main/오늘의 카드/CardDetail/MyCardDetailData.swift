//
//  MyCardDetailData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources

enum MyInfoType {
    case nickname
    case tall
    case body
    case drink
    case religion
    case charList
    case area
    case edu
    case job
    
    case aboutMe
    case interView(seq: Int)
    case jobCareer
    
    case hobby
    case interest
    case have
    case want
    
    case additionalInfo
}

enum MyCardCellType {
    case profileUnivCo(crown: Int)
    case space
    case profileBasic(title: String?, value: String?, myInfoType: MyInfoType)

    case keywordTag(title: String, list: [String]?, myInfoType: MyInfoType)
    
    case textViewBox(title: String, value: String?, myInfoType: MyInfoType)
    
    case additionalInfo(crown: Int)
}

struct MyCardDetailRow {
    var cellType: MyCardCellType
    var card: CardDetailEditResult
}


struct MyCardDetailSection {
//    var sectionType: MyCardSectionType
    var items: [MyCardDetailRow]
}

extension MyCardDetailSection: SectionModelType {
    init(original: MyCardDetailSection, items: [MyCardDetailRow]) {
        self = original
        self.items = items
    }
}

