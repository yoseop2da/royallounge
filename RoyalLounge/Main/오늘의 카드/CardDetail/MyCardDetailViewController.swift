//
//  MyCardDetailViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Toaster

class MyCardDetailViewController: LoggedBaseViewController {
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<MyCardDetailSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileScrollWrapView: UIView!
    
    @IBOutlet weak var photoStatusLabel: UILabel!
    @IBOutlet weak var photoGuideLabel: UILabel!
    
    var matchNo: String!
    private var profileScrollView: ProfileScrollView!
    private var reloadTagList: [String] = [] // tagView 완료 시점에 height다시 잡아주기위해 cell reload 땜빵코드,,,,,
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> UITableViewCell in
            switch item.cellType {
            case .profileUnivCo(let crown):
                let cell: ProfileUnivCoEditCell = tableView.dequeueReusable(for: indexPath)
                cell.setCard(item.card, crown: crown)
                cell.editButtonAction = { type in
                    let vc = RLStoryboard.join.univCoViewController(isEditMode: false, univCoType: type)
                    self.push(vc)
                }
                return cell
            case .profileBasic(let title, let value, _):
                let cell: ProfileBasicCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(title: title ?? "", value: value ?? "")
                return cell
            case .space:
                let cell: MessageSpaceCell = tableView.dequeueReusable(for: indexPath)
                return cell
            case .keywordTag(let title, let list, _):
                let cell: KeywordTagCell = tableView.dequeueReusable(for: indexPath)
                cell.setData(title: title, list: list)
                return cell
            case .textViewBox(let title, let value, _):
                let cell: TextViewBoxCell = tableView.dequeueReusable(for: indexPath)
                cell.titleLabel.text = title
                cell.valueLabel.text = value
                return cell
            case .additionalInfo(let crown):
                let cell: AdditionalInfoCell = tableView.dequeueReusable(for: indexPath)
                cell.setCrown(crown)
                return cell
            }
        })
        return dataSource
    }
    
    var viewModel: MyCardDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoStatusLabel.layer.cornerRadius = 4.0
        photoStatusLabel.clipsToBounds = true
        photoGuideLabel.layer.cornerRadius = 6.0
        photoGuideLabel.clipsToBounds = true
        
        self.swipeBackAnyWhere()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = MyCardDetailViewModel(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.sectionData
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        profileScrollWrapView.rx.tapGesture()
            .filter{ $0.state == .ended }
            .bind { _ in
                let vc = RLStoryboard.join.photoViewController(isEditMode: true)
                self.push(vc)}
            .disposed(by: bag)
        
        viewModel.cardDetail
            .filter{ $0 != nil }.map{ $0! }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { card in
                self.profileScrollWrapView.subviews.forEach { $0.removeFromSuperview() }
                let profileScrollView = ProfileScrollView.init(imageUrls: card.photoList.compactMap{$0})
                self.profileScrollWrapView.addSubview(profileScrollView)
                if let photoStatus = card.photoStatus {
                    self.photoStatusLabel.text = card.photoStatusName
                    if photoStatus == Str.reject {
                        self.photoStatusLabel.isHidden = false
                        self.photoGuideLabel.backgroundColor = UIColor.errerColor.withAlphaComponent(0.3)
                        self.photoStatusLabel.backgroundColor = UIColor.errerColor.withAlphaComponent(0.75)
                    }else if photoStatus == Str.ready {
                        self.photoStatusLabel.isHidden = false
                        self.photoGuideLabel.backgroundColor = UIColor.dark.withAlphaComponent(0.3)
                        self.photoStatusLabel.backgroundColor = UIColor.dark.withAlphaComponent(0.75)
                    }else if photoStatus == Str.normal {
                        self.photoStatusLabel.isHidden = true
                        self.photoGuideLabel.backgroundColor = UIColor.dark.withAlphaComponent(0.3)
                    }
                }else{
                    self.photoStatusLabel.isHidden = true
                    self.photoGuideLabel.backgroundColor = UIColor.dark.withAlphaComponent(0.3)
                }
            })
            .disposed(by: bag)
        
        mainTableView.rx.itemSelected
            .subscribe(onNext: { indexPath in
                let type = self.viewModel.getMyInfoType(idx: indexPath.row)
                switch type {
                case .nickname:
                    let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .nickname, isEditMode: true)
                    self.push(vc)
                case .tall:
                    let vc = RLStoryboard.join.tallViewController(isEditMode: true)
                    self.push(vc)
                case .body:
                    let vc = RLStoryboard.join.profileListViewController(optionType: MainInformation.shared.gender == .m ? .bodyM : .bodyW, isEditMode: true)
                    self.push(vc)
                case .drink:
                    let vc = RLStoryboard.join.profileListViewController(optionType: .drink, isEditMode: true)
                    self.push(vc)
                case .religion:
                    let vc = RLStoryboard.join.profileListViewController(optionType: .religion, isEditMode: true)
                    self.push(vc)
                case .charList:
                    let vc = RLStoryboard.join.profileTagViewController(optionType: MainInformation.shared.gender == .m ? .charM : .charW, isEditMode: true)
                    self.push(vc)
                case .area:
                    let vc = RLStoryboard.join.areaViewController(isEditMode: true)
                    self.push(vc)
                case .edu:
                    let vc = RLStoryboard.join.profileListViewController(optionType: .education, isEditMode: true)
                    self.push(vc)
                case .job:
                    let vc = RLStoryboard.join.profileTextFieldViewController(viewType: .job, isEditMode: true)
                    self.push(vc)
                case .aboutMe:
                    let vc = RLStoryboard.join.aboutMeViewController(isEditMode: true)
                    self.push(vc)
                case .interView(let seq):
                    let vc = RLStoryboard.join.interviewViewController(interViewSeq: seq, isEditMode: true)
                    self.push(vc)
                case .hobby:
                    let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .hobby, isEditMode: true)
                    self.push(vc)
                case .interest:
                    let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .interest, isEditMode: true)
                    self.push(vc)
                case .have:
                    let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .have, isEditMode: true)
                    self.push(vc)
                case .want:
                    let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .want, isEditMode: true)
                    self.push(vc)
                case .jobCareer:
                    let vc = RLStoryboard.join.jobCareerViewController(isEditMode: true)
                    self.push(vc)
                case .additionalInfo:
                    if let cardDetail = self.viewModel.cardDetail.value {
                        if cardDetail.hobbyList == nil {
                            let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .hobby, isEditMode: false)
                            let navi = UINavigationController.init(rootViewController: vc)
                            self.modal(navi, animated: true)
                            return
                        }
                        if cardDetail.interestList == nil {
                            let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .interest, isEditMode: false)
                            let navi = UINavigationController.init(rootViewController: vc)
                            self.modal(navi, animated: true)
                            return
                        }
                        if cardDetail.haveList == nil {
                            let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .have, isEditMode: false)
                            let navi = UINavigationController.init(rootViewController: vc)
                            self.modal(navi, animated: true)
                            return
                        }
                        if cardDetail.wantList == nil {
                            let vc = RLStoryboard.join.optionalInfoViewController(optionalInfoType: .want, isEditMode: false)
                            let navi = UINavigationController.init(rootViewController: vc)
                            self.modal(navi, animated: true)
                            return
                        }
                        if cardDetail.jobCareer == nil {
                            let vc = RLStoryboard.join.jobCareerViewController(isEditMode: false)
                            let navi = UINavigationController.init(rootViewController: vc)
                            self.modal(navi, animated: true)
                            return
                        }
                    }
                default: break
                }
            })
            .disposed(by: bag)
        
        mainAsync {
            self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: UIScreen.width)
            self.mainTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "프로필 수정"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.viewModel.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func detectedScreenShot() {
        super.detectedScreenShot()
        RLPopup.shared.detectCapture(rightAction: {})
    }
}

extension MyCardDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let wrapView = UIView.init()
    //        wrapView.backgroundColor = .clear
    //        return wrapView
    //    }
}
