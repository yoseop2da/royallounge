//
//  MyCardDetailViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Toaster

class MyCardDetailViewModel: BaseViewModel {

    let cardDetail = BehaviorRelay<CardDetailEditResult?>.init(value: nil)
    let sections = BehaviorRelay<[MyCardDetailSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let crownInfo = BehaviorRelay<CrownModel?>.init(value: nil)
    
    var cardNickname: String { cardDetail.value?.nickname ?? ""}
    
    var userNo: String!
    
    var hasRemoved: Bool = false
    
    init(userNo: String) {
        super.init()
        self.userNo = userNo

        reloadData()
        
        Repository.shared.main
            .getCrown(userNo: userNo)
            .subscribe(onNext: {
                self.crownInfo.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        Observable.combineLatest(cardDetail, crownInfo)
            .subscribe(onNext: { result, crown in
                guard let card = result else { return }
                guard let crown = crown else { return }
                let interviewList = card.interviewList ?? []
                var _sections: [MyCardDetailSection] = []
                var items: [MyCardDetailRow] = []
                
                //------------
                items.append(MyCardDetailRow.init(cellType: .profileUnivCo(crown: crown.joinUniv + crown.joinCo), card: card))
                
                //------------ profile
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "닉네임", value: "\(card.nickname)", myInfoType: .nickname), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "키", value: "\(card.tall)cm", myInfoType: .tall), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "체형", value: card.body, myInfoType: .body), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "주량", value: card.drink, myInfoType: .drink), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "종교", value: card.religion, myInfoType: .religion), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "성격", value: card.charList.joined(separator: ", "), myInfoType: .charList), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "지역", value: card.areaName, myInfoType: .area), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "학력", value: card.edu, myInfoType: .edu), card: card))
                items.append(MyCardDetailRow.init(cellType: .profileBasic(title: "직업", value: card.job, myInfoType: .job), card: card))

                items.append(MyCardDetailRow.init(cellType: .space, card: card))

                //------------ aboutMe
                items.append(MyCardDetailRow.init(cellType: .textViewBox(title: "자기소개", value: card.aboutMe, myInfoType: .aboutMe), card: card))
                
                //------------ interview
                (0..<interviewList.count).forEach { idx in
                    items.append(MyCardDetailRow.init(cellType: .textViewBox(title: interviewList[idx].question, value: interviewList[idx].answer, myInfoType: .interView(seq: idx+1)), card: card))
                }
                
                //------------ add profile
                if let value = card.hobbyList { items.append(MyCardDetailRow.init(cellType: .keywordTag(title: "취미", list: value, myInfoType: .hobby), card: card)) }
                if let value = card.interestList { items.append(MyCardDetailRow.init(cellType: .keywordTag(title: "관심사", list: value, myInfoType: .interest), card: card)) }
                if let value = card.haveList { items.append(MyCardDetailRow.init(cellType: .keywordTag(title: "I have", list: value, myInfoType: .have), card: card)) }
                if let value = card.wantList { items.append(MyCardDetailRow.init(cellType: .keywordTag(title: "I want", list: value, myInfoType: .want), card: card)) }
                if let value = card.jobCareer { items.append(MyCardDetailRow.init(cellType: .textViewBox(title: "# Job Career", value: value, myInfoType: .jobCareer), card: card)) }

                if card.hobbyList == nil || card.interestList == nil || card.haveList == nil || card.wantList == nil || card.jobCareer == nil {
                    items.append(MyCardDetailRow.init(cellType: .additionalInfo(crown: crown.joinAddProfile), card: card))
                }
                
                
                _sections.append(MyCardDetailSection.init(items: items))
                
                self.sections.accept(_sections)
            })
            .disposed(by: bag)
    }
    
    func reloadData() {
        MainInformation.shared.updateMyCrownNew()
        
        Repository.shared.main
            .getMyCardDetailEdit(userNo: userNo)
            .subscribe(onNext: {
                if let card = $0 {
                    self.cardDetail.accept(card)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func setResult(_ result: CardDetailResult) {
        MainInformation.shared.updateMyCrownNew()
        var new = self.cardDetail.value
        new?.receiveOkMsg = result.receiveOkMsg
        new?.sendOkMsg = result.sendOkMsg
        new?.acceptOkMsg = result.acceptOkMsg
        self.cardDetail.accept(new)
    }
    
    func getMyInfoType(idx: Int) -> MyInfoType? {
        guard let cell = sections.value.first?.items[idx] else { return nil }
        if case .profileBasic(_, _,let myInfoType) = cell.cellType {
            return myInfoType
        }
        if case .textViewBox(_, _,let myInfoType) = cell.cellType {
            return myInfoType
        }
        if case .keywordTag(_, _,let myInfoType) = cell.cellType {
            return myInfoType
        }
        if case .additionalInfo = cell.cellType {
            return .additionalInfo
        }
        
        return nil
    }
}
