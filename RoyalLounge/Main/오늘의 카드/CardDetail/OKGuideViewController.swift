//
//  OKGuideViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class OKGuideViewController: UIViewController {
    enum OkType {
        case ok
        case superOK
    }
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerWrapView: UIView!
    @IBOutlet weak var contentsImageView: UIImageView!
    
    @IBOutlet weak var mainImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var superOKWrapView: UIView!
    @IBOutlet weak var superOKButton: UIButton!
    
    @IBOutlet weak var okWrapView: UIView!
    @IBOutlet weak var okButton: UIButton!
    
    private let bag = DisposeBag()
    var okType: OkType = .superOK

//    let okImage = UIImage.init(named: "infoMainContentsOk")
//    let superOkImage = UIImage.init(named: "infoMainContentsSuperok")
    var okImage: UIImage!
    var superOkImage: UIImage!
    
    let okNorImage = UIImage.init(named: "infoMainTabOk")
    let okSelImage = UIImage.init(named: "infoMainTabOkSel")
    let superOkNorImage = UIImage.init(named: "infoMainTabSuperok")
    let superOkSelImage = UIImage.init(named: "infoMainTabSuperokSel")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.okImage = UIImage.init(named: "infoMainContentsOk")
//        self.superOkImage = UIImage.init(named: "infoMainContentsSuperok")
        

            if let url = URL.init(string: "https://image.royallounge.co.kr/guide/guide_ok.png") {
                if let data = try? Data(contentsOf: url) {
                    self.okImage = UIImage.init(data: data)
                }
            }
            
            if let url = URL.init(string: "https://image.royallounge.co.kr/guide/guide_super_ok.png") {
                if let data = try? Data(contentsOf: url) {
                    self.superOkImage = UIImage.init(data: data)
                }
            }
        mainAsync {
            self.superOKWrapView.roundCorners([.topLeft], radius: 10)
            self.okWrapView.roundCorners([.topRight], radius: 10)
            self.reloadView(okType: self.okType)
        }
        
        superOKButton.rx.tap
            .rxTouchAnimation(button: superOKButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.reloadView(okType: .superOK)
            }).disposed(by: bag)
        
        okButton.rx.tap
            .rxTouchAnimation(button: okButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.reloadView(okType: .ok)
            }).disposed(by: bag)
    }
    
    @IBAction func closeButtonTouched(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func reloadView(okType: OkType) {
        if okType == .ok {
            okWrapView.backgroundColor = .creamwhiteBG
            superOKWrapView.backgroundColor = .rlBrown215
            okButton.setImage(okSelImage, for: .normal)
            superOKButton.setImage(superOkNorImage, for: .normal)
            contentsImageView.image = self.okImage
        }else{
            okWrapView.backgroundColor = .rlBrown215
            superOKWrapView.backgroundColor = .creamwhiteBG
            okButton.setImage(okNorImage, for: .normal)
            superOKButton.setImage(superOkSelImage, for: .normal)
            contentsImageView.image = self.superOkImage
        }
        mainAsync {
            self.headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.width, height: self.height(okType: okType))
            self.mainTableView.reloadData()
        }
    }
    
    func height(okType: OkType) -> CGFloat {
        let topPadding: CGFloat = 50.0
        let topImageHeight: CGFloat = UIScreen.width * (366.0/375.0)
        let menuBarHeight: CGFloat = 75.0
        
        let image = okType == .ok ? self.okImage : self.superOkImage
        let mainImageHeight: CGFloat = (UIScreen.width - 40.0) * (image!.size.height / image!.size.width)
        self.mainImageViewHeight.constant = mainImageHeight
        
        let bottomPadding: CGFloat = 45.0
        let headerHeight: CGFloat = topPadding + topImageHeight + menuBarHeight + mainImageHeight + bottomPadding
        return headerHeight
    }

    override var prefersStatusBarHidden: Bool {
         return true
    }
}
