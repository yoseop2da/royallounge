//
//  OkSendPopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OkSendPopup: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textWrapView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeHolder: UILabel!
    
    @IBOutlet weak var limitTextLabel: UILabel!
    
    @IBOutlet weak var bottomMainView: UIView!
    @IBOutlet weak var okButtonWrapView: UIView!
    @IBOutlet weak var keyboardOnButtonWrapView: UIView!
    @IBOutlet weak var keyboardOnButtonWrapViewBottom: NSLayoutConstraint!
    
    var nextButton: RLNextButton!
    
    @IBOutlet weak var wrapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    private var ANIMATION_TIMEINTERVAL: TimeInterval = 0.2
    private var OUTBUTTON_HIDDEN: CGFloat = 0.0
    private var OUTBUTTON_SHOWN: CGFloat = 1.0
    private var BOTTOM_VIEW_HIDDEN: CGFloat = 610.0
    private var BOTTOM_VIEW_SHOWN: CGFloat = 0.0
    private var keyboardHeight: CGFloat = 0
    
    private var viewModel: OkSendPopupViewModel!
    
    var cacheMessage: String = ""
    var nickname: String = ""
    var okType: OKType = .ok
    var matchNo: String = ""
    var didSendOK: ((CardDetailResult) -> Void)?
    var messageCacheCallback: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = cacheMessage
        
        mainAsync {
            self.bottomMainView.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        if okType == .ok || okType == .okPast || okType == .okHigh {
            titleLabel.text = "매력적인 \(nickname)님에게\n\(okType.stringValue) 메시지 카드를 보내세요."
            nextButton = RLNextButton.init(title: "OK 보내기", bgColor: .brown25, titleColor: .brown100)
        }else if okType == .superOk || okType == .superOkPast {
            titleLabel.text = "매력적인 \(nickname)님에게\n\(okType.stringValue) 메시지 카드를 보내세요."
            let fullStr = "슈퍼OK 보내기 (상대 무료 수락 가능)"
            let attrStr = NSMutableAttributedString.init(string: fullStr, attributes: [
                .font: UIFont.spoqaHanSansBold(ofSize: 16.0),
                .foregroundColor: UIColor.white
            ])
            attrStr.addAttributes([.font : UIFont.boldSystemFont(ofSize: 12.0), .foregroundColor: UIColor.white.withAlphaComponent(0.6)], range: (fullStr as NSString).range(of: "(상대 무료 수락 가능)"))
            nextButton = RLNextButton.init(attributedText: attrStr, bgColor: .brown100)
        }else if okType == .receiveOk {
            titleLabel.text = "매력적인 \(nickname)님에게\n 수락 메시지 카드를 보내세요."
            nextButton = RLNextButton.init(title: "상대의 OK 수락하기", bgColor: .brown25, titleColor: .brown100)
            self.limitTextLabel.text = "최소 5자"
        }else if okType == .receiveSuperOk {
            titleLabel.text = "매력적인 \(nickname)님에게\n 수락 메시지 카드를 보내세요."
            nextButton = RLNextButton.init(title: "상대의 슈퍼OK 무료 수락하기", bgColor: .brown100, titleColor: .white)
            self.limitTextLabel.text = "최소 5자"
        }else if okType == .superOkOneMore || okType == .superOkOneMorePast {
            titleLabel.text = "매력적인 \(nickname)님에게\n\(okType.stringValue) 메시지 카드를 보내세요."
            let fullStr = "한번 더 슈퍼OK 보내기 (상대 무료 수락 가능)"
            let attrStr = NSMutableAttributedString.init(string: fullStr, attributes: [
                .font: UIFont.spoqaHanSansBold(ofSize: 16.0),
                .foregroundColor: UIColor.white,
            ])
            attrStr.addAttributes([.font : UIFont.boldSystemFont(ofSize: 12.0), .foregroundColor: UIColor.white.withAlphaComponent(0.6)], range: (fullStr as NSString).range(of: "(상대 무료 수락 가능)"))
            nextButton = RLNextButton.init(attributedText: attrStr, bgColor: .brown100)
        }
        okButtonWrapView.addSubview(nextButton)
        
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        initialize()

        viewModel = OkSendPopupViewModel(
            okType: self.okType,
            userNo: MainInformation.shared.userNoAes!,
            matchNo: self.matchNo,
            okMessage: textView.rx.text.orEmpty.asObservable(),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: .seconds(2), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: {
                })
            })
            .disposed(by: bag)
        
        textView.rx.text.orEmpty.asObservable()
            .subscribe(onNext: { msg in
                self.messageCacheCallback?(msg)
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { result in
                if let result = result {
                    self.textView.resignFirstResponder()
                    self.dismiss(animated: false) {
                        self.messageCacheCallback?("")
                        self.didSendOK?(result)
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.validated
            .subscribe(onNext: { result in
                print("result : \(result)")
                switch result {
                case .empty, .failed:
                    self.limitTextLabel.textColor = .primary100
                    self.textView.textColor = .rlBlack21
                case .failedProhibit:
                    self.limitTextLabel.textColor = .primary100
                    self.textView.textColor = result.textViewTextColor
                default:
                    self.limitTextLabel.textColor = .gray75
                    self.textView.textColor = .rlBlack21
                }
            })
            .disposed(by: bag)
        
        viewModel.showToastMessage
            .subscribe(onNext: { message in
                toast(message: message, seconds: 1.5, isOnKeyboard: self.bottomPadding.constant != 0)
            })
            .disposed(by: bag)
        
        textWrapView.layer.cornerRadius = 6.0
        textWrapView.setBorder(color: .gray10)
        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: 4, bottom: 0, right: 4)
        textView.rx.text.orEmpty
            .subscribe(onNext: { text in
                let count = text.count
                self.placeHolder.isHidden = count > 0
                
                var minCount: Int = 10
                if self.okType == .receiveOk || self.okType == .receiveSuperOk {
                    self.limitTextLabel.text = "최소 5자"
                    minCount = 5
                }else{
                    self.limitTextLabel.text = "최소 10자"
                    minCount = 10
                }
                
                if count < minCount {
                    self.limitTextLabel.textColor = .primary100
                }else if count >= 500 {
                    self.limitTextLabel.text = "최대 500자"
                    self.limitTextLabel.textColor = UIColor.gray75
                    self.textView.text = (self.textView.text as NSString).substring(with: NSMakeRange(0, 500))
                }else{
                    // 정상케이스
                    self.limitTextLabel.textColor = UIColor.gray75
                }
//                if text.containsEmoji() {
//                    self.textView.text = (text.removingEmoji() as String)
//                }
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                self.keyboardHeight = keyboard.height
                let keyboardHide = keyboard.height == 0
                UIView.animate(withDuration: keyboard.duration) {
                    self.keyboardOnButtonWrapViewBottom.constant = keyboardHide ? 0 : keyboard.height
                    self.keyboardOnButtonWrapView.isHidden = keyboardHide
                    
                    self.bottomPadding.constant = keyboardHide ? 0 : (-keyboard.height + 56.0)
                    let _wrapViewHeight: CGFloat = keyboardHide ? 610.0 : 394.0 + 56.0
                    self.wrapViewHeight.constant = _wrapViewHeight
                    self.BOTTOM_VIEW_HIDDEN = _wrapViewHeight
                    
                    if keyboardHide {
                        self.okButtonWrapView.addSubview(self.nextButton)
                        self.nextButton.snp.makeConstraints {
                            $0.top.bottom.equalToSuperview()
                            $0.leading.equalToSuperview().offset(20)
                            $0.trailing.equalToSuperview().offset(-20)
                        }
                        self.nextButton.roundCorner()
                    }else{
                        self.keyboardOnButtonWrapView.addSubview(self.nextButton)
                        self.nextButton.snp.makeConstraints {
                            $0.top.bottom.leading.trailing.equalToSuperview()
                        }
                        self.nextButton.squareCorner()
                    }
                    
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    override var prefersStatusBarHidden: Bool {
        return !UIScreen.hasNotch
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomViewShowAnimation(timeInterval: ANIMATION_TIMEINTERVAL)
    }

    func initialize() {
        self.bottomPadding.constant = BOTTOM_VIEW_HIDDEN
    }
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: {
            })
        }
    }
    @IBAction func backButtonTouched(_ sender: Any) {
        self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: nil)
    }

    var startPositionY: CGFloat?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            startPositionY = position.y
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let _startPositionY = startPositionY {
            let position = touch.location(in: view)
            let gap = position.y - _startPositionY
            if gap < 0 { return }
            
            let isDownAction = gap > 0
            self.bottomPadding.constant =  (isDownAction ? gap : gap*(0.2)) - (self.keyboardHeight > 0 ? self.keyboardHeight - 56.0 : self.keyboardHeight + 0.0)
            
            print("* gap : \(gap)|| \(self.bottomPadding.constant)")
            let percent = CGFloat(1.0 - (isDownAction ? ((gap)/BOTTOM_VIEW_HIDDEN) : 0.0))
            let colorAlpha = CGFloat(0.6/*MAX*/ * percent)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(colorAlpha)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.keyboardHeight > 0 && bottomPadding.constant > 50{
            self.dismiss(animated: true, completion: nil)
            return
        }else{
            if bottomPadding.constant > 180 {
                self.dismiss(animated: true, completion: nil)
                return
            }
        }
        
        self.bottomViewShowAnimation(timeInterval: 0.1)
        
    }
}

extension OkSendPopup {

    func bottomViewShowAnimation(timeInterval: TimeInterval) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = 0.0 - (self.keyboardHeight > 0 ? self.keyboardHeight - 56.0 : self.keyboardHeight + 0.0)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func bottomViewHideAnimation(timeInterval: TimeInterval, dismiss: Bool, after: (() -> Void)?) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = self.BOTTOM_VIEW_HIDDEN
            self.view.backgroundColor = .clear
            self.view.layoutIfNeeded()
        }, completion: { _ in
            if dismiss {
                self.dismiss(animated: true, completion: { after?() })
            } else {
                after?()
            }
        })
    }

}
