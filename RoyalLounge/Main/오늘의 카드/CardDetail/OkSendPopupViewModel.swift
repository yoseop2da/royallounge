
//
//  OkSendPopupViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

enum OKType {
    case ok // OK 보내기
    case okPast // 지난카드 OK 보내기
    case okHigh // 내가 받은 높은점수 OK 보내기
    
    case superOk // 슈퍼OK 보내기
    case superOkPast // 지난카드 슈퍼OK 보내기
    case superOkOneMore // 한번 더 슈퍼OK 보내기
    case superOkOneMorePast // 한번 더 슈퍼OK 보내기
    
    case receiveOk // OK 수락하기
    case receiveSuperOk // 슈퍼OK 수락하기
    
    
    var stringValue: String {
        switch self {
        case .ok, .okPast, .okHigh: return "OK"  // OK 보내기
        case .superOk, .superOkPast, .superOkOneMore, .superOkOneMorePast: return "슈퍼OK" // 슈퍼OK 보내기
        case .receiveOk: return "OK" // OK 수락하기
        case .receiveSuperOk: return "슈퍼OK" // 슈퍼OK 수락하기
        }
    }
}

class OkSendPopupViewModel: BaseViewModel {

    let showToastMessage = PublishSubject<String>.init()
    
    let crown: BehaviorRelay<CrownModel?> = BehaviorRelay.init(value: nil)
    
    let validated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let nextSuccess = PublishSubject<CardDetailResult?>.init()
    
    var okType: OKType = .ok
    var userNo: String!
    var matchNo: String!
    
    init(okType: OKType,
         userNo: String,
         matchNo: String,
         okMessage: Observable<String>,
         nextButtonTap: Observable<Void>) {
        super.init()
        
        self.okType = okType
        self.userNo = userNo
        self.matchNo = matchNo
        
        Repository.shared.main
            .getCrown(userNo: self.userNo)
            .subscribe(onNext: {
                self.crown.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        okMessage
            .distinctUntilChanged()
            .map{
                let result = Validation.shared.prohibitCheck(word: $0)
                if case .ok = result { return Validation.shared.validate(okMessage: $0, okType: okType) }
                return result }
            .bind(to: validated)
            .disposed(by: bag)
            
        nextButtonTap.withLatestFrom(Observable.combineLatest(okMessage, validated))
            .subscribe(onNext: { msg, validate in
                if case .failedProhibit(_) = validate { self.showToastMessage.on(.next("비속어 또는 연락처가 포함되어 있어요")); return }
                if !validate.isValid { self.showToastMessage.on(.next("최소 10자 이상 메시지를 입력해주세요")); return }
                // 리프래시해주면서 보낸메세지 또는 수락 메세지 업데이트 해주기!!!
                // 카드상세에 수락화면 구현하기
                if let crown = self.getCrown(okType: okType) {
                    if crown == 0 {
                        self.send(userNo: self.userNo, matchNo: self.matchNo, msg: msg)
                    }else{
                        RLPopup.shared.showNormal(description: "크라운 \(crown)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                            // API Call
                            self.send(userNo: self.userNo, matchNo: self.matchNo, msg: msg)
                        })
                    }
                }else{
                    // 크라운 못가지고오는 케이스 기본처리 뭐로 해줄지 정해야함
                }
                
            })
            .disposed(by: bag)
    }
    
    func getCrown(okType: OKType) -> Int? {
        let okCrown = self.crown.value?.ok
        let okPastCrown = self.crown.value?.okPast
        let okHighCrown = self.crown.value?.okHigh
        let superOkCrown = self.crown.value?.superOk
        let superOkPastCrown = self.crown.value?.superOkPast
        let receiveOkCrown = self.crown.value?.okAccept
        let receiveSuperOkCrown = self.crown.value?.superOkAccept
        
        switch okType {
        case .ok: if let value = okCrown { return value }
        case .okPast: if let value = okPastCrown { return value }
        case .okHigh: if let value = okHighCrown { return value }
        case .superOk, .superOkOneMore: if let value = superOkCrown { return value }
        case .superOkPast, .superOkOneMorePast: if let value = superOkPastCrown { return value }
        case .receiveOk: if let value = receiveOkCrown { return value }
        case .receiveSuperOk: if let value = receiveSuperOkCrown { return value }
        }
        
        Repository.shared.main
            .getCrown(userNo: self.userNo)
            .subscribe(onNext: {
                self.crown.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        return nil
    }
    
    func send(userNo: String, matchNo: String, msg: String) {
        mainAppDelegate.showBlockView()
        switch okType {
        case .ok, .okHigh, .okPast, .receiveOk:
            Repository.shared.main
                .sendOK(userNo: userNo, matchNo: matchNo, msg: msg, okType: self.okType)
                .subscribe(onNext: { (result) in
                    mainAppDelegate.hideBlockView()
                    MainInformation.shared.updateMyCrownNew()
                    MainInformation.shared.updateRedDot()
                    self.nextSuccess.on(.next(result))
                }, onError: {
                    mainAppDelegate.hideBlockView()
                    
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
            .disposed(by: bag)
        case .superOk, .superOkPast, .superOkOneMore, .superOkOneMorePast, .receiveSuperOk:
            Repository.shared.main
                .sendSuperOk(userNo: userNo, matchNo: matchNo, msg: msg, okType: self.okType)
                .subscribe(onNext: { (result) in
                    mainAppDelegate.hideBlockView()
                    MainInformation.shared.updateMyCrownNew()
                    MainInformation.shared.updateRedDot()
                    self.nextSuccess.on(.next(result))
                }, onError: {
                    mainAppDelegate.hideBlockView()
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    guard let custom = $0 as? CustomError else { return }
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                })
            .disposed(by: bag)
        }
    }
}
