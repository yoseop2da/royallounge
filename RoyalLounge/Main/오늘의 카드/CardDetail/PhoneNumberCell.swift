//
//  PhoneNumberCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class PhoneNumberCell: UITableViewCell {

    @IBOutlet weak var dDayWrapView: UIView!
    @IBOutlet weak var dDayLabel: UILabel!
    
    @IBOutlet weak var mobileNoButton: MobileNoButton!
    
    private var mbNo: String?
    private let bag = DisposeBag()
    
    var openPhoneNumber: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mobileNoButton.setNormalButton(mbNo: nil)
        dDayWrapView.layer.cornerRadius = 4.0
        dDayWrapView.setShadow(radius: 0.5, color: .lightGray)
        
        dDayWrapView.isHidden = true
    }
    
    func setData(crown: Int, mbNo: String?, dDay: Int) {
        self.mbNo = mbNo
        self.mobileNoButton.crown = crown
        mobileNoButton.setNormalButton(mbNo: mbNo)
        
        // 표시 안하기로함
//        dDayLabel.text = "D-\(dDay)"
//        let mode = self.mobileNoButton.buttonMode
//        if case .opened = mode {
//            dDayWrapView.isHidden = false
//        }else{
//            dDayWrapView.isHidden = true
//        }
    }

    @IBAction func buttonTouched(_ sender: UIButton) {
        let mode = self.mobileNoButton.buttonMode
//        if case .opened = mode { return }
        sender.touchAnimation {
            switch mode {
            case .normal:
                self.mobileNoButton.setCrownCountButton()
            case .crown:
                self.mobileNoButton.setCrownCountButton()
                self.openPhoneNumber?()
            case .opened:
                if let mbNo = self.mbNo {
                    //문자열을 클립보드에 복사
                    UIPasteboard.general.string = mbNo
                    toast(message: "휴대폰번호가 복사되었습니다", seconds: 1.5)
                }
            }
        }
    }
    
}
