//
//  ProfileBasicCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ProfileBasicCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var valueWrapView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = ""
        valueLabel.text = ""
        if valueWrapView != nil {
            valueWrapView.layer.cornerRadius = 3.0
        }
    }

    func setData(title: String, value: String) {
        titleLabel.text = title
        valueLabel.text = value
    }
}
