//
//  ProfileUnivCoCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ProfileUnivCo1Cell: ProfileUnivCoBaseCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setCard(_ card: CardDetailResult) {
        if let university = card.university {
            titleLabel.text = "학교"
            valueLabel.text = university
            return
        }
        
        if let company = card.company {
            titleLabel.text = "직장"
            valueLabel.text = company
        }
    }
}

class ProfileUnivCo2Cell: ProfileUnivCoBaseCell {

    @IBOutlet weak var universityLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCard(_ card: CardDetailResult) {
        if let university = card.university, let company = card.company {
            universityLabel.text = university
            companyLabel.text = company
        }
    }
}

class ProfileUnivCoEditCell: ProfileUnivCoBaseCell {

    @IBOutlet weak var universityWrapView: UIView!
    @IBOutlet weak var companyWrapView: UIView!
    
    @IBOutlet weak var universityLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    @IBOutlet weak var universityMarkImageView: UIImageView!
    @IBOutlet weak var companyMarkImageView: UIImageView!
    
    @IBOutlet weak var crownWrapView: UIView!
    @IBOutlet weak var emptyWrapView: UIView!
    
    @IBOutlet weak var universityStatusLabel: UILabel!
    @IBOutlet weak var companyStatusLabel: UILabel!
    
    @IBOutlet weak var crownLabel: UILabel!
    var editButtonAction: ((UnivCoModel.UnivCoType)->Void)?
    
    let certified = UIImage.init(named: "icons24PxCertification")
    let notCertified = UIImage.init(named: "icons24PxCertificationPlus")
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        crownWrapView.layer.cornerRadius = 15.0
        
        universityWrapView.layer.cornerRadius = 3.0
        companyWrapView.layer.cornerRadius = 3.0
        
        universityStatusLabel.layer.cornerRadius = 4.0
        companyStatusLabel.layer.cornerRadius = 4.0
        
        universityStatusLabel.clipsToBounds = true
        companyStatusLabel.clipsToBounds = true
    }
    
    func setCard(_ card: CardDetailEditResult, crown: Int = 6) {
        crownLabel.text = "x\(crown) 적립"
        let university = card.university
        let company = card.company
        
        emptyWrapView.isHidden = (university != nil || company != nil)
        if let university = university?.university {
            universityLabel.text = university
            universityMarkImageView.image = certified
        }else{
            universityLabel.text = "학교 인증 추가"
            universityMarkImageView.image = notCertified
        }
        if let company = company?.company {
            companyLabel.text = company
            companyMarkImageView.image = certified
        }else{
            companyLabel.text = "직장 인증 추가"
            companyMarkImageView.image = notCertified
        }
        // 심사중, 반려 표시하기
        // READY, REJECT, NORMAL
        universityWrapView.backgroundColor = UIColor.brown25
        if let universityStatus = university?.universityStatus,
           let universityStatusName = university?.universityStatusName {
            universityStatusLabel.isHidden = false
            universityStatusLabel.text = universityStatusName
            if universityStatus == Str.ready {
                universityStatusLabel.backgroundColor = UIColor.dark.withAlphaComponent(0.75) // 심사중
            }else if universityStatus == Str.reject {
                universityStatusLabel.backgroundColor = UIColor.errerColor.withAlphaComponent(0.75) // 반려
                universityWrapView.backgroundColor = UIColor.errerColor.withAlphaComponent(0.1) // 반려
            }else if universityStatus == Str.normal {
                universityStatusLabel.isHidden = true
            }
        }else{
            universityStatusLabel.isHidden = true
        }
        
        companyWrapView.backgroundColor = UIColor.brown25
        if let companyStatus = company?.companyStatus,
           let companyStatusName = company?.companyStatusName {
            companyStatusLabel.isHidden = false
            companyStatusLabel.text = companyStatusName
            if companyStatus == Str.ready {
                companyStatusLabel.backgroundColor = UIColor.dark.withAlphaComponent(0.75) // 심사중
            }else if companyStatus == Str.reject {
                companyStatusLabel.backgroundColor = UIColor.errerColor.withAlphaComponent(0.75) // 반려
                companyWrapView.backgroundColor = UIColor.errerColor.withAlphaComponent(0.1) // 반려
            }else if companyStatus == Str.normal {
                companyStatusLabel.isHidden = true
            }
        }else{
            companyStatusLabel.isHidden = true
        }
        
        //

    }
    @IBAction func universityButtonTouched(_ sender: Any) {
        universityLabel.touchAnimation{
            self.editButtonAction?(.univ)
        }
    }
    
    @IBAction func companyButtonTouched(_ sender: Any) {
        companyLabel.touchAnimation{
            self.editButtonAction?(.co)
        }
    }
}

class ProfileUnivCoBaseCell: UITableViewCell {

    @IBOutlet weak var wrapView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapView.layer.cornerRadius = 6.0
        wrapView.setBorder(color: .gray10)
    }
}
