//
//  ReceiveMessageTextCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ReceiveMessageTextCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var messageWrapView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        profileImageView.setBorder(color: UIColor.gray10)
        profileImageView.image = UIImage.init(named: "test")
        
        mainAsync {
            self.messageWrapView.roundCorners([.topLeft, .topRight, .bottomRight], radius: 20)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(imageUrl: String, message: String) {
        profileImageView.setImage(urlString: imageUrl)
        messageLabel.text = message
    }
}
