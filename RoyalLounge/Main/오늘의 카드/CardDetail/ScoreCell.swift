//
//  ScoreCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ScoreCell: UITableViewCell {

    @IBOutlet weak var starView: UIView!
    var ratingView: RatingView!
    var didTouchStarButton: ((Int?)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        addRatingView()
    }
    
    func addRatingView() {
        mainAsync {
            let ratingViewFrame = CGRect(x: 0, y: 0, width: UIScreen.width - 40.0, height: 40)
            self.ratingView = RatingView.init(frame: ratingViewFrame, starButtonBeginCallback: {
                mainAppDelegate.showBlockView()
            }, starButtonEndCallback: { starPointInt in
                mainAppDelegate.hideBlockView()
                self.didTouchStarButton?(starPointInt)
            })
            self.starView.addSubview(self.ratingView)
        }
    }
    
    func initialStar() {
        self.ratingView.starButtonInitialize()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
