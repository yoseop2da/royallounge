//
//  SendMessageTextCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/02.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SendMessageTextCell: UITableViewCell {

    @IBOutlet weak var messageWrapView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    private var isCornerRound = false
    func setData(message: String) {
        messageLabel.text = message
        mainAsync {
            self.messageWrapView.roundCorners([.topLeft, .topRight, .bottomLeft], radius: 20)
        }
    }
}
