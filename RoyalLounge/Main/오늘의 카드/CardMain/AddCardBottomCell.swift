//
//  AddCardBottomCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import SnapKit

class AddCardBottomCell: UICollectionViewCell, iCarouselDataSource, iCarouselDelegate {
    @IBOutlet var carousel: iCarousel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!

    var list: [AddCardModel] = []
    var itemAction: ((AddCardModel)->Void)?
    var moreAction: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 15.0
        bgView.clipsToBounds = true
        moreButton.layer.cornerRadius = 6.0
        moreButton.clipsToBounds = true
        
        carousel.delegate = self
        carousel.dataSource = self
    }

    func setList(list: [AddCardModel], nickname: String) {
        self.list = list
        nicknameLabel.text = "\(nickname)님을 위한"
        carousel.reloadData()
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return list.count
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let item = list[index]
        var itemView: CarouselItemView
        if let view = view as? CarouselItemView {
            itemView = view
        } else {
            itemView = CarouselItemView.init(title: "title", subTitle: "subTitle", imageUrl: item.imageUrl)
        }
        return itemView
    }
    
//    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
//        print("carousel.itemWidth : \(carousel.itemWidth)")
//        return carousel.itemWidth
//        return 200.0
//    }
    
//    func carousel(_ carousel: iCarousel, itemTransformForOffset offset: CGFloat, baseTransform transform: CATransform3D) -> CATransform3D {
//    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == .spacing { return value * 1.3 }
        else if option == .radius { return value * 1.1 }
        else if option == .angle { return value * 0.9 }
        return value
    }
    
    @IBAction func moreButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.moreAction?()
        }
    }

    func carousel(_ carousel: iCarousel, shouldSelectItemAt index: Int) -> Bool {
        let itemView = carousel.itemView(at: index)
        let item = list[index]
        itemView?.touchAnimation {
            self.itemAction?(item)
        }
        return true
    }
}

class CarouselItemView: UIView {
    let titleLabel = UILabel()
    let subTitleLabel = UILabel()
    let imageView = UIImageView()
    
    init(title: String, subTitle: String, imageUrl: String) {
        let width = UIScreen.width
        let cardWidth = width * (160.0 / 375.0)
        super.init(frame: CGRect(x: 0, y: 0, width: cardWidth, height: cardWidth*(17.0/16.0)))
        backgroundColor = UIColor.white
//        setBorder(color: UIColor.gray10)
        layer.cornerRadius = 6.0
        self.setShadow(radius: 3.0, offsetX: 0.0, offsetY: 0.0, color: UIColor.black.withAlphaComponent(0.2))
        
        
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        self.addSubview(imageView)
        
        titleLabel.font = UIFont.spoqaHanSansBold(ofSize: 20.0)
        titleLabel.textColor = UIColor.gray75
        
        subTitleLabel.font = UIFont.spoqaHanSansRegular(ofSize: 12.0)
        subTitleLabel.textColor = UIColor.gray50
        
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 6.0
        imageView.layer.masksToBounds = true
        
        titleLabel.text = title
        subTitleLabel.text = subTitle
        imageView.setImage(urlString: imageUrl)
        
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalTo(12)//Superview().offset(12)
            $0.height.equalTo(29)
        }
        
        subTitleLabel.snp.makeConstraints {
            $0.leading.equalTo(12)//Superview().offset(12)
            $0.top.equalTo(titleLabel.snp.bottom).offset(2)
            $0.height.equalTo(18)
        }
        
        imageView.snp.makeConstraints {
//            $0.trailing.equalToSuperview()
//            $0.bottom.equalTo(-12)
//            $0.width.equalTo(86)
//            $0.height.equalTo(90)
            $0.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
