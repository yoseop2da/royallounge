//
//  AddCardTopCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AddCardTopCell: UICollectionViewCell {
    @IBOutlet weak var whiteWrapView: UIView!
    @IBOutlet weak var arrowWrapView: UIView!
   
    var moreAction: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        whiteWrapView.layer.cornerRadius = 10.0
        arrowWrapView.layer.cornerRadius = arrowWrapView.frame.height / 2.0
    }
    @IBAction func moreButtonTouched(_ sender: Any) {
        whiteWrapView.touchAnimation{
            self.moreAction?()
        }
    }
}
