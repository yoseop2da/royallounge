//
//  BannerCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import CHIPageControl

class BannerCell: UICollectionViewCell {

    typealias DataSource = RxCollectionViewSectionedReloadDataSource<BannerListModel>
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: CHIPageControlPuya!

    private var previousPage = 0
    
    private var dataSource: DataSource {
        let dataSource = DataSource(configureCell: { dataSource, collectionView, indexPath, item -> BannerInnerCell in
            let cell: BannerInnerCell = collectionView.dequeueReusable(for: indexPath)
            cell.setBanner(item)
            return cell
        })
        return dataSource
    }
    
    private lazy var bag = DisposeBag()
    private let viewModel = BannerCellViewModel()
    private var sourceObservable: Disposable?
    private var bannerList: [BannerModel] = []
    
    var modelSelected: ((BannerModel)->Void)?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let flowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.width
        flowLayout.scrollDirection = .horizontal
        flowLayout.headerReferenceSize = CGSize.zero
        flowLayout.itemSize = CGSize(width: width, height: 78)
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets.zero
        collectionView.collectionViewLayout = flowLayout
        collectionView.delegate = self
        
        collectionView.rx.modelSelected(BannerModel.self)
            .throttle(.milliseconds(1000), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { banner in
                self.collectionView.touchAnimation{
                    self.modelSelected?(banner)
                }
            })
            .disposed(by: bag)
        
        self.viewModel.sectionData
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
    }
    
    func setBannerList(_ bannerList: [BannerModel]) {
        self.bannerList = bannerList
        pageControl.numberOfPages = bannerList.count
        viewModel.refresh(bannerList: bannerList)
        startTimer()
        
    }
    func startTimer() {
        guard self.bannerList.count > 0 else { return }
        stopTimer()
        sourceObservable = Observable<Int>
            .interval(.seconds(3), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.previousPage += 1
                if self.bannerList.count > 0 {
                    let row = self.previousPage % self.bannerList.count
                    self.collectionView.selectItem(at: IndexPath.init(row: row, section: 0), animated: true, scrollPosition: .left)
                }
            })
    }
    
    func stopTimer() {
        sourceObservable?.dispose()
    }

}

extension BannerCell: UIScrollViewDelegate, UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.bounds.width
        let pageFraction = scrollView.contentOffset.x/pageWidth

        let page = Int((round(pageFraction)))
        if previousPage != page {
            previousPage = page
            pageControl.set(progress: page, animated: true)
            startTimer()
        }
    }
}


class BannerCellViewModel: BaseViewModel {
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[BannerListModel]>.init(value: [])
    
    func refresh(bannerList: [BannerModel]) {
        sections.accept([BannerListModel.init(items: bannerList)])
    }
}

