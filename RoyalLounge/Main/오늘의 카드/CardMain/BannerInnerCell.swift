//
//  BannerInnerCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class BannerInnerCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImageView.layer.cornerRadius = 10.0
        bannerImageView.clipsToBounds = true
        bannerImageView.layer.masksToBounds = true
        bannerImageView.contentMode = .scaleAspectFill
    }
    
    func setBanner(_ banner: BannerModel) {
        bannerImageView.setImage(urlString: banner.bannerImageURL)
    }
}
