
//
//  CardBaseCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardBaseCell: UICollectionViewCell {
    @IBOutlet weak var whiteWrapView: UIView!
    @IBOutlet weak var specImageView: UIImageView!
    @IBOutlet weak var specCountLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNotOpenImageView: UIImageView!
    @IBOutlet weak var profileGradientView: UIImageView!
    @IBOutlet weak var dDayWrapView: UIView!
    @IBOutlet weak var dDayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        whiteWrapView.layer.cornerRadius = 10.0
        whiteWrapView.clipsToBounds = true
        whiteWrapView.setBorder(width: 1.0, color: .gray10)
        dDayWrapView.layer.cornerRadius = 4.0
        dDayWrapView.setShadow(radius: 0.5, color: .lightGray)
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.backgroundColor = .gray25
        profileNotOpenImageView.isHidden = false
        profileNotOpenImageView.contentMode = .scaleAspectFit
    }
    
    func setBaseInfo(urlString: String, openYn: Bool, specCount: Int, dDay: Int) {
        self.profileImageView.setImage(urlString: urlString, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"))
        self.specImageView.isHidden = specCount == 0
        self.specCountLabel.isHidden = specCount == 0
        self.specCountLabel.text = "\(specCount)"
        
        if openYn {
            profileNotOpenImageView.isHidden = true
        }else{
            profileNotOpenImageView.isHidden = false
        }
        
        dDayLabel.text = "D-\(dDay)"
        dDayWrapView.isHidden = dDay > 2
    }
}
