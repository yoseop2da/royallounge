
//
//  CardCell2.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardCell: CardBaseCell {
    @IBOutlet weak var jobAgeLabel: UILabel!
    
    var item: CardModel?
    var cardAction: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        jobAgeLabel.text = ""
    }
    
    func setCard(card: CardModel) {
        item = card
        self.setBaseInfo(urlString: card.photoUrl, openYn: card.openYn, specCount: card.specCount, dDay: card.dDay)
        jobAgeLabel.text = "\(card.job)·\(card.age)"
    }

    @IBAction func cardTouched(_ sender: Any) {
        if let card = self.item {
            if card.openYn {
                whiteWrapView.touchAnimation{
                    self.cardAction?()
                }
            }else{
//                self.flip {
                    self.cardAction?()
//                }
            }
        }
    }
}

