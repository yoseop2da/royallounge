//
//  CardFooterView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardFooterView: UICollectionReusableView {
        
    @IBOutlet weak var moreButton: UIButton!
    
    var moreAction: (()->Void)?
    
    @IBAction func moreButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{ self.moreAction?() }
    }
    
    func showMore() {
        moreButton.isHidden = false
    }
    
    func hideMore() {
        moreButton.isHidden = true
    }
}
