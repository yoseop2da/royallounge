
//
//  CardHeaderView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardHeaderView: UICollectionReusableView {
        
    @IBOutlet weak var nicknameLabel: UILabel!
    
    func setNickname(_ nickname: String) {
        nicknameLabel.text = ""
        let fullStr = "\(nickname)님을 위한 오늘의 카드"
        let attributedString = NSMutableAttributedString(string: fullStr, attributes: [
            .font: UIFont.spoqaHanSansRegular(ofSize: 14.0),
            .foregroundColor: UIColor.dark,
            .kern: 0.0
        ])
        let range = (fullStr as NSString).range(of: "\(nickname)님")
        attributedString.addAttribute(.font, value: UIFont.spoqaHanSansBold(ofSize: 18), range: range)
        
        nicknameLabel.attributedText = attributedString
    }
    
    func setHeaderTitle(_ title: String) {
        nicknameLabel.text = title
    }
}
