//
//  CardMainModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

enum TodaySectionType {
    case topAddCard // TOP_ADDCARD
    case today  //(headerTitle: String, list: [CardModel])
    case todayEmpty //(headerTitle: String, list: [CardModel])
    case popular(list: [AddCardModel])
    case pastNotOpen    //(headerTitle: String, list: [CardModel], hasNext: Bool)
    case pastOpen   //(headerTitle: String, list: [CardModel], hasNext: Bool)
    case banner(bannerList: [BannerModel])
    case space
}

// Tableview Cell
struct TodaySection {
    var type: TodaySectionType
    var header: String
    var hasNext: Bool? = nil
    var cardPagingType: String // 서버에서 받은 타입
    var items: [TodayItem]
}

extension TodaySection: SectionModelType {
    init(original: TodaySection, items: [TodayItem]) {
        self = original
        self.items = items
    }
}

struct AddCardModel {
    var imageUrl: String
    var cardNo: String
    var optionType: OptionType?
    // 추가카드 전체에서 사용
    var title: String = ""
    var isEmptyCard: Bool = false
    
    init(isEmptyCard: Bool) {
        self.imageUrl = ""
        self.cardNo = ""
        self.optionType = nil
        self.isEmptyCard = true
    }
    
    init(title: String) {
        self.imageUrl = ""
        self.cardNo = ""
        self.optionType = nil
        self.title = title
    }
    
//    init(imageUrl: String, cardNo: String, optionType: String) {
//        self.imageUrl = imageUrl
//        self.cardNo = cardNo
//        self.optionType = optionType
//    }
    
    init?(card001Item: Card001Item){
        guard let _imageUrl = card001Item.imageURL else { return nil }
        guard let _cardNo = card001Item.cardNo else { return nil }
        guard let _optionType = card001Item.optionType else { return nil }
        self.imageUrl = _imageUrl
        self.cardNo = _cardNo
        switch _optionType {
        case "AREA": self.optionType = .area
        case "GENERAL": self.optionType = .general
        case "RELIGION": self.optionType = .religion
        case "SALARY": self.optionType = .salary
        case "STYLE": self.optionType = .style
        default: self.optionType = nil
        }
    }
    
    init?(json: [String: AnyObject]) {
        guard let _imageUrl = json["imageUrl"] as? String else { return nil }
        guard let _cardNo = json["cardNo"] as? String else { return nil }
        guard let _optionType = json["optionType"] as? String else { return nil }
        
        self.imageUrl = _imageUrl
        self.cardNo = _cardNo
        switch _optionType {
        case "AREA": self.optionType = .area
        case "GENERAL": self.optionType = .general
        case "RELIGION": self.optionType = .religion
        case "SALARY": self.optionType = .salary
        case "STYLE": self.optionType = .style
        default: self.optionType = .general
        }
    }
}

struct TodayItem {
    var card: CardModel? = nil
    var isEmpty: Bool = false
}

struct CardModel {
    var photoUrl: String
    var job: String
    var age: Int
    var specCount: Int
    var dDay: Int
    var openYn: Bool
    var matchNo: String

    init?(card001Item: Card001Item){
        guard let _photoUrl = card001Item.photoUrl else { return nil }
        guard let _job = card001Item.job else { return nil }
        guard let _age = card001Item.age else { return nil }
        guard let _specCount = card001Item.specCount else { return nil }
        guard let _dDay = card001Item.dDay else { return nil }
        guard let _openYn = card001Item.openYn else { return nil }
        guard let _matchNo = card001Item.matchNo else { return nil }
        
        self.photoUrl = _photoUrl
        self.job = _job
        self.age = _age
        self.specCount = _specCount
        self.dDay = _dDay
        self.openYn = _openYn
        self.matchNo = _matchNo
    }
    
    init?(json: [String: AnyObject]) {
        guard let _photoUrl = json["photoUrl"] as? String else { return nil }
        guard let _job = json["job"] as? String else { return nil }
        guard let _age = json["age"] as? Int else { return nil }
        guard let _specCount = json["specCount"] as? Int else { return nil }
        guard let _dDay = json["dDay"] as? Int else { return nil }
        guard let _openYn = json["openYn"] as? Bool else { return nil }
        guard let _matchNo = json["matchNo"] as? String else { return nil }
        
        self.photoUrl = _photoUrl
        self.job = _job
        self.age = _age
        self.specCount = _specCount
        self.dDay = _dDay
        self.openYn = _openYn
        self.matchNo = _matchNo
    }
}

// CollectionView Cell
struct BannerListModel {
    var items: [BannerModel]
}
extension BannerListModel: SectionModelType {
    typealias Item = BannerModel
    init(original: BannerListModel, items: [Item]) {
        self = original
        self.items = items
    }
}

enum ActionType: String {
    case outUrl = "OUT_URL" // : 외부 URL
    case detail = "DETAIL" // : 상세
    case insideApp = "INSIDE_APP" // : 앱 내 연결
    
    static func convertType(from value: String) -> ActionType {
        if value == ActionType.outUrl.rawValue { return .outUrl }
        if value == ActionType.detail.rawValue { return .detail }
        if value == ActionType.insideApp.rawValue { return .insideApp }
        return .outUrl
    }
}

enum InAppType: String {
    case store = "STORE" // : 스토어
    case myPage = "MY_PAGE" // : 마이 페이지
    case todayCard = "TODAY_CARD" // : 오늘의 카드
    case addCard = "ADD_CARD" // : 추가카드
    case cardHistory = "CARD_BOX"  // : 카드 보관함
    case liveMatch = "LIVE_MATCH" // : 라이브 매칭
    static func convertType(from value: String) -> InAppType {
        if value == InAppType.store.rawValue { return .store }
        if value == InAppType.myPage.rawValue { return .myPage }
        if value == InAppType.todayCard.rawValue { return .todayCard }
        if value == InAppType.addCard.rawValue { return .addCard }
        if value == InAppType.cardHistory.rawValue { return .cardHistory }
        if value == InAppType.liveMatch.rawValue { return .liveMatch }
        return .store
    }
}

struct BannerModel {
    var eventNo: String?
    var actionType: ActionType
    var inAppType: InAppType?
    
    var bannerImageURL: String
    var linkUrl: String? = nil
    

    init?(card001Item: Card001Item) {
        guard let _bannerImageURL = card001Item.bannerImageURL else { return nil }
        self.bannerImageURL = _bannerImageURL
        self.eventNo = card001Item.eventNo
        if let _actionType = card001Item.actionType {
            self.actionType = ActionType.convertType(from: _actionType)
        }else{
            self.actionType = .detail // 기획확정이 되지 않아 임시로 처리중...
        }
        if let _inAppType = card001Item.inAppType { self.inAppType = InAppType.convertType(from: _inAppType) }
        if let _linkUrl = card001Item.linkURL { self.linkUrl = _linkUrl }
    }
}
