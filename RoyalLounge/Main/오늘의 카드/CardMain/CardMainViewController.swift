//
//  CardMainViewController.swift
//  RoyalRounge
//
//  Created by yoseop park on 2019/12/20.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
//import StoreKit
//
//extension CardMainViewController: SKStoreProductViewControllerDelegate {
//    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
//}

class CardMainViewController: MainViewController {
    
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<TodaySection>
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, collectionView, indexPath, item -> UICollectionViewCell in
            let type = self.viewModel.sectionType(section: indexPath.section)
            switch type {
            case .topAddCard:
                let cell: AddCardTopCell = collectionView.dequeueReusable(for: indexPath)
                cell.moreAction = {
                    self.moveToAddCardMain()
                }
                return cell
            case .popular(let list):
                let cell: AddCardBottomCell = collectionView.dequeueReusable(for: indexPath)
                let nickname = self.viewModel.headerInfo(section: indexPath.section)
                cell.setList(list: list, nickname: nickname)
                cell.itemAction = { item in
                    guard let optionType = item.optionType else { return }
                    var popup: OptionBasePopup = OptionSpecPopup.init(nibName: OptionSpecPopup.className, bundle: nil)
                    switch optionType {
                    case .area:
                        popup = OptionAreaPopup.init(nibName: OptionAreaPopup.className, bundle: nil)
                    case .general:
                        popup = OptionSpecPopup.init(nibName: OptionSpecPopup.className, bundle: nil)
                    case .religion:
                        popup = OptionReligionPopup.init(nibName: OptionReligionPopup.className, bundle: nil)
                    case .salary:
                        popup = OptionSalaryPopup.init(nibName: OptionSalaryPopup.className, bundle: nil)
                    case .style:
                        popup = OptionStylePopup.init(nibName: OptionStylePopup.className, bundle: nil)
                    }
                    popup.cardNo = item.cardNo
                    popup.optionType = optionType
                    popup.willMoveToStore = { needCrown in
                        RLPopup.shared.showMoveStore(needCrown: needCrown, reasonStr: "추가 카드를 즉시 확인하기 위해", action: {
                            self.dismiss(animated: true) {
                                self.moveToStore()
                            }
                        })
                    }
                    popup.willRefresh = {
                        collectionView.setContentOffset(.zero, animated: true)
                        self.viewModel.refresh()
                    }
                    self.tabBarController?.clearPresent(popup)
                }
                cell.moreAction = {
                    let vc = RLStoryboard.main.addCardMainViewController()
                    vc.willMoveToStore = {
                        // 스토어 이동
                        self.moveToStore()
                    }
                    vc.willRefresh = {
                        collectionView.setContentOffset(.zero, animated: true)
                        self.viewModel.refresh()
                    }
                    self.modal(UINavigationController.init(rootViewController: vc), animated: true)
                }
                return cell
            case .todayEmpty:
                let cell: TodayEmptyCell = collectionView.dequeueReusable(for: indexPath)
                return cell
            case .today, .pastNotOpen, .pastOpen:
                let card = item.card!
                let cell: CardCell = collectionView.dequeueReusable(for: indexPath)
                cell.setCard(card: card)
                cell.cardAction = {
                    if card.openYn {
                        let vc = RLStoryboard.todayCard.cardDetailViewController(matchNo: card.matchNo) { refreshType in
                            if refreshType != .none {
                                self.viewModel.refresh()
                            }
                        }
                        self.tabBarController?.push(vc)
                    }else{
                        self.viewModel.openCard(card, type: type)
                    }
                }
                return cell
            case .banner(let bannerList):
                let cell: BannerCell = collectionView.dequeueReusable(for: indexPath)
                cell.setBannerList(bannerList)
                cell.modelSelected = { banner in
                    print("banner: \(banner)")
                    if banner.actionType == .outUrl {
                        if let urlStr = banner.linkUrl {
                            let url = URL(string: urlStr)
                            guard UIApplication.shared.canOpenURL(url!) else {
                                toast(message: "연결할 수 없습니다", seconds: 1.5); return
                            }
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url!)
                            }
                        }
                    }else if banner.actionType == .detail {
                        if let eventNo = banner.eventNo {
                            let vc = RLStoryboard.myPage.eventDetailViewController(eventNo: eventNo)
                            self.push(vc)
                        }
                    }else if banner.actionType == .insideApp {
                        if let inAppType = banner.inAppType {
                            switch inAppType {
                            case .store:
                                self.moveToStore()
                            case .myPage:
                                self.moveToMyPage()
                            case .todayCard: ()
                            case .addCard:
                                self.moveToAddCardMain()
                            case .cardHistory:
                                Coordinator.shared.toSecondTab()
                            case .liveMatch:
                                Coordinator.shared.toThirdTab()
                            }
                        }
                    }
                }
                return cell
            case .space:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpaceCell", for: indexPath)
                return cell
            }
        })
        
        dataSource.configureSupplementaryView = { dataSource, collectionView, kind, indexPath in
            let type = self.viewModel.sectionType(section: indexPath.section)
            
            if kind == UICollectionView.elementKindSectionHeader {
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CardHeaderView.className, for: indexPath) as! CardHeaderView
                let headerInfo = self.viewModel.headerInfo(section: indexPath.section)
                switch type {
                case .today, .todayEmpty: header.setNickname(headerInfo)
                case .pastOpen, .pastNotOpen: header.setHeaderTitle(headerInfo)
                default: ()
                }
                return header
            }else{
                let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CardFooterView.className, for: indexPath) as! CardFooterView
                switch type {
                case .pastOpen, .pastNotOpen:
                    footer.showMore()
                    footer.moreAction = {
                        self.viewModel.moreCard(type: type)
                    }
                default: footer.hideMore()
                }
                return footer
            }
        }
        return dataSource
    }
    var refreshControl: UIRefreshControl!
    var viewModel: CardMainViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        delay(1.0) {
//            if #available(iOS 10.3, *) {
//                let viewController = SKStoreProductViewController()
//                viewController.delegate = self
//                let parameters = [SKStoreProductParameterITunesItemIdentifier: "1462243117"]
//                viewController.loadProduct(withParameters: parameters, completionBlock: nil)
//                self.present(viewController, animated: true, completion: nil)
////                SKStoreReviewController.requestReview()
//            } else {
//                // Fallback on earlier versions
//            }
////            guard let writeReviewURL = URL(string: "https://apps.apple.com/app/id1462243117?action=write-review")
////            else { fatalError("Expected a valid URL") }
////            UIApplication.shared.open(writeReviewURL, options: [:], completionHandler: nil)
//        }
        
        self.view.backgroundColor = UIColor.rlOrangeBG238
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.collectionView!.addSubview(refreshControl)
        
        viewModel = CardMainViewModel(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.newUserPopup
            .subscribe(onNext: { tuple in
            let (joinPath, reco, notice) = tuple
                if !joinPath {
                    self.showJoinPathViewController { self.showRecommendViewController { self.showNotice(notice) } }; return
                }
                if !reco {
                    self.showRecommendViewController { self.showNotice(notice) }; return
                }
                self.showNotice(notice)
        })
        .disposed(by: bag)
        
        viewModel.sectionData
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func refreshData() {
        refreshControl?.endRefreshing()
        viewModel?.refresh()
    }
    
    override func detectedScreenShot() {
        super.detectedScreenShot()
        RLPopup.shared.detectCapture(rightAction: {})
    }
    
    func moveToAddCardMain() {
        let vc = RLStoryboard.main.addCardMainViewController()
        vc.willMoveToStore = {
            // 스토어 이동
            self.moveToStore()
        }
        vc.willRefresh = {
            self.collectionView.setContentOffset(.zero, animated: true)
            self.viewModel.refresh()
        }
        self.modal(UINavigationController.init(rootViewController: vc), animated: true)
    }
        
    func showJoinPathViewController(closeCallback: (()->Void)?) {
        let vc = RLStoryboard.join.joinPathViewController(isEditMode: false)
        let navi = UINavigationController(rootViewController: vc)
        vc.closeCallback = closeCallback
        self.modal(navi, animated: true)
    }
    
    func showRecommendViewController(closeCallback: (()->Void)?) {
        if MainInformation.shared.outTester {
            closeCallback?()
            return
        }
        
        let vc = RLStoryboard.join.recommendViewController(isEditMode: false)
        let navi = UINavigationController(rootViewController: vc)
        vc.closeCallback = {
            // 환영팝업
            delay(0.1) {
                let vc = RLStoryboard.join.welcomePopup()
                vc.closeCallback = closeCallback
                self.tabBarController?.present(vc, animated: true, completion: nil)
            }
        }
        self.modal(navi, animated: true)
    }
    
    func showNotice(_ notice: GetMainInfoQuery.Data.MainInfo.MainNotice?) {
        if MainInformation.shared.outTester { return }
        if let _notice = notice {
            // notice 띄우기
            RLPopup.shared.showNotice(title: _notice.title, content: _notice.content, noticeNo: _notice.noticeNo)
        }
    }
}

extension CardMainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch self.viewModel.sectionType(section: indexPath.section) {
        case .topAddCard:
            let width: CGFloat = UIScreen.width
            let height: CGFloat = 98
            return CGSize(width: width, height: height)
        case .popular:
            let width: CGFloat = UIScreen.width
            let height: CGFloat = 40/*top*/ + 391 + 20/*bottom*/
            return CGSize(width: width, height: height)
        case .today:
            let width: CGFloat = ((UIScreen.width - (20/*left*/ + 20/*right*/ + 15/*inner*/)) / 2.0) - 1.0
            let height: CGFloat = width + 26
            return CGSize(width: width, height: height)
        case .todayEmpty:
            let width: CGFloat = UIScreen.width
            let height: CGFloat = 318
            return CGSize(width: width, height: height)
        case .pastNotOpen, .pastOpen:
            let width: CGFloat = ((UIScreen.width - (20 + 20 + 15)) / 2.0) - 1.0
            let height: CGFloat = width
            return CGSize(width: width, height: height)
        case .banner:
            let width: CGFloat = UIScreen.width
            let height: CGFloat = 138
            return CGSize(width: width, height: height)
        case .space:
            let width: CGFloat = UIScreen.width
            let height: CGFloat = 60
            return CGSize(width: width, height: height)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch self.viewModel.sectionType(section: section) {
        case .topAddCard, .todayEmpty, .popular, .banner, .space:
            return UIEdgeInsets.zero
        case .today, .pastNotOpen, .pastOpen:
            return UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch self.viewModel.sectionType(section: section) {
        case .topAddCard, .popular, .banner, .space:
            return CGSize.zero
        case .today, .todayEmpty, .pastNotOpen, .pastOpen:
            return CGSize(width: UIScreen.width, height: 87)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        switch viewModel.sectionType(section: section) {
        case .topAddCard, .todayEmpty, .popular, .today, .space:
            return CGSize.zero
        case .pastNotOpen, .pastOpen:
            if viewModel.hasNext(section: section) {
                return CGSize(width: UIScreen.width, height: 58)
            }
            return CGSize.zero
        case .banner:
            return CGSize(width: UIScreen.width, height: 60)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if case .today = self.viewModel.sectionType(section: section) {
            return 30
        }
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
