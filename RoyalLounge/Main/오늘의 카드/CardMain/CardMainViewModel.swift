//
//  CardMainViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class CardMainViewModel: BaseViewModel {
    
    var sections = BehaviorRelay<[TodaySection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    let newUserPopup = PublishSubject<(isJoinPath: Bool, isReco: Bool, mainNotice: GetMainInfoQuery.Data.MainInfo.MainNotice?)>.init()
    var userNo: String!
     
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        self.sections.accept(
            [TodaySection.init(type: .todayEmpty, header: "", cardPagingType: "", items: [TodayItem.init(isEmpty: true)])]
        )
        refresh()
        
        // 메인 로드 이후 팝업 발생
        delay(0.3) {
            Repository.shared.graphQl
                .getMainInfo(userNo: userNo)
                .subscribe(onNext: { mainInfo in
                    let isJoinPathReg = mainInfo?.joinPath ?? false
                    let isRecoReg = mainInfo?.reco ?? false
                    self.newUserPopup.on(.next((isJoinPath: isJoinPathReg, isReco: isRecoReg, mainNotice: mainInfo?.mainNotice)))
                }, onError: {
                    $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                })
                .disposed(by: self.bag)
        }
    }

    @objc
    func refresh() {
        Repository.shared.main
            .getCardMainSections(userNo: userNo)
            .subscribe(onNext: { result in
                self.sections.accept(result)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    func sectionType(section: Int) -> TodaySectionType {
        let data = sections.value[section]
        return data.type
    }
    
    func headerInfo(section: Int) -> String {
        let data = sections.value[section]
        return data.header
    }
    
    func hasNext(section: Int) -> Bool {
        let data = sections.value[section]
        return data.hasNext ?? false
    }
    
    func openCard(_ card: CardModel, type: TodaySectionType) {
        if card.openYn { return }
        
        Repository.shared.main
            .openCard(userNo: userNo, matchNo: card.matchNo)
            .subscribe(onNext: {
                guard $0 else { return }
                var newCard = card
                newCard.openYn = true
                var allSections = self.sections.value
                if let sectionIndex = allSections.firstIndex(where: {
                    switch type {
                    case .today: if case .today = $0.type { return true }
                    case .pastOpen: if case .pastOpen = $0.type { return true }
                    case .pastNotOpen: if case .pastNotOpen = $0.type { return true }
                    default: return false
                    }
                    return false }) {
                    var section = allSections[sectionIndex]
                    if let index = section.items.firstIndex(where: { $0.card?.matchNo == card.matchNo }) {
                        section.items[index].card = newCard
                    }
                    allSections[sectionIndex] = section
                }
                self.sections.accept(allSections)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            }).disposed(by: bag)
    }
    
    func moreCard(type: TodaySectionType) {
        var allSections = sections.value
        if let sectionIndex = allSections.firstIndex(where: {
            switch type {
            case .pastOpen: if case .pastOpen = $0.type { return true }
            case .pastNotOpen: if case .pastNotOpen = $0.type { return true }
            default: return false }
            return false })
        {
            var section: TodaySection = allSections[sectionIndex]
            if let lastMatchNo = section.items.last?.card?.matchNo {
                Repository.shared.main
                    .moreCardList(userNo: userNo, lastMatchNo: lastMatchNo, cardPastType: section.cardPagingType)
                    .subscribe(onNext: { result in
                        section.hasNext = result.hasNext
                        section.items.append(contentsOf: result.items)
                        allSections[sectionIndex] = section
                        self.sections.accept(allSections)
                    }, onError: {
                        $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                        guard let custom = $0 as? CustomError else { return }
                        self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                    })
                    .disposed(by: bag)
            }
        }
    }
    
    
    
    
}
