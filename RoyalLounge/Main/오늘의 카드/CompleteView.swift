//
//  CompleteView.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/12.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CompleteView: NSObject {
    enum CompleteType {
        case joinPath
        case reco
        case sendOk
        case sendSuperOk
        case receiveOk
        case receiveSuperOk
        case liveMatchReward
    }
    private var mainView: UIView!
    private var imageView: UIImageView!
    
    init(type: CompleteType) {
        super.init()
        mainView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: UIScreen.height))
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        mainAppDelegate.window?.addSubview(mainView)
        
        imageView = UIImageView.init()
        if type == .joinPath {
            imageView.image = UIImage.init(named: "popupJoinPath")
        }else if type == .reco {
            imageView.image = UIImage.init(named: "popupReco")
        }else if type == .sendOk {
            imageView.image = UIImage.init(named: "popupOK")
        }else if type == .sendSuperOk {
            imageView.image = UIImage.init(named: "popupSuperOK")
        }else if type == .receiveOk {
            imageView.image = UIImage.init(named: "popupOppOK")
        }else if type == .receiveSuperOk {
            imageView.image = UIImage.init(named: "popupOppSuperOK")
        }else if type == .liveMatchReward {
            imageView.image = UIImage.init(named: "03LiveMatchingCirclePopup")
        }
        
        
        mainView.addSubview(imageView)
        
        imageView.snp.makeConstraints {
            $0.center.equalTo(self.mainView.snp.center)
        }
    }
    
    func show(closeCallback: (() -> Void)?) {
        self.mainView.isHidden = false
        self.imageView.alpha = 0.0
        self.imageView.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
        mainAppDelegate.window?.bringSubviewToFront(self.mainView)
        UIView.animate(withDuration: 0.35) {
            self.imageView.alpha = 1.0
            self.imageView.transform = CGAffineTransform.init(scaleX: 1.05, y: 1.05)
        }
        delay(0.5) {
            UIView.animate(withDuration: 0.1) {
                self.imageView.transform = CGAffineTransform.identity
            }
        }
        delay(1.5) {
            self.hide()
            closeCallback?()
        }
    }
    
    private func hide() {
        self.mainView.isHidden = true
        self.imageView.alpha = 0.0
    }
}
