//
//  ReportViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class ReportViewModel: BaseViewModel {
    let sections = BehaviorRelay<[ReportSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    var nickname: String = ""
    var userId: String = ""
    var mbNo: String = "입력해주세요."
    
    let nextSuccess = PublishSubject<Bool>.init()
    
    init(userNo: String) {
        super.init()
        Repository.shared.main.getReportReasonList()
            .subscribe(onNext: { result in
                self.sections.accept([ReportSection.init(items: result)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
        .disposed(by: bag)
        
        Repository.shared.graphQl
            .getEmailInfo(userNo: userNo)
            .subscribe(onNext: {
                guard let result = $0 else { return }
                self.nickname = result.nickname
                self.userId = result.userId.aesDecrypted!
                self.mbNo = result.mbNo.aesDecrypted!
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func sendEmailSuccess(reportedUserNo: String, reportGroupNo: String) {
        Repository.shared.main
            .sendReport(userNo: MainInformation.shared.userNoAes!, reportedUserNo: reportedUserNo, reportGroupNo: reportGroupNo)
            .subscribe(onNext: {
                self.nextSuccess.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
}
