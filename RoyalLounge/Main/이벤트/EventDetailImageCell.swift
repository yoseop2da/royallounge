//
//  EventDetailImageCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class EventDetailImageCell: UITableViewCell {

    @IBOutlet weak var eventImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImageUrl(_ url: String) {
        eventImageView.setImage(urlString: url)
    }

}
