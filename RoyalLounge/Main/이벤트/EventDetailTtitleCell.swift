//
//  EventDetailTtitleCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class EventDetailTtitleCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    let upImage = UIImage.init(named: "icon16PxArrowUp")
    let downImage = UIImage.init(named: "icon16PxArrowDown")
    override func awakeFromNib() {
        super.awakeFromNib()
        arrowImageView?.tintColor = .gray50
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setItem(title: String, period: String, isOpen: Bool = false) {
        titleLabel.text = title
        dateLabel.text = period
        if let imageView = arrowImageView {
            imageView.image = isOpen ? upImage : downImage
        }
    }

}
