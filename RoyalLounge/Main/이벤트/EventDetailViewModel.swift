//
//  EventDetailViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class EventDetailViewModel: BaseViewModel {
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[EventDetailSection]>.init(value: [])
    let result = BehaviorRelay<EventMainModel?>.init(value: nil)
    
    var userNo: String!
    var eventNo: String!
    init(userNo: String, eventNo: String) {
        super.init()
        self.userNo = userNo
        self.eventNo = eventNo
        reloadData()
        
        result
            .filter{$0 != nil}
            .subscribe(onNext: { _result in
                let result = _result!
                var cellTypeList: [EventDetailCellType] = []
                
                cellTypeList.append(.title(title: result.title, period: result.period, isOpen: false, idx: 0))
                
                if result.contentType == "IMAGE" {
                    if let imageURL = result.imageURL {
                        cellTypeList.append(.image(imageUrl: imageURL))
                    }
                }else if result.contentType == "IMAGE_BOTTOM" {
                    cellTypeList.append(.text(content: result.content))
                    if let imageURL = result.imageURL {
                        cellTypeList.append(.image(imageUrl: imageURL))
                    }
                }else if result.contentType == "IMAGE_TOP" {
                    if let imageURL = result.imageURL {
                        cellTypeList.append(.image(imageUrl: imageURL))
                    }
                    cellTypeList.append(.text(content: result.content))
                }else if result.contentType == "TEXT" {
                    cellTypeList.append(.text(content: result.content))
                }
                self.sections.accept([EventDetailSection.init(items: cellTypeList)])
            })
            .disposed(by: bag)
    }
    
    func getItem(indexPath: IndexPath) -> EventDetailCellType {
        return sections.value[indexPath.section].items[indexPath.row]
    }
    
    func reloadData() {
        Repository.shared.main
            .getEventDetail(userNo: userNo, eventNo: eventNo)
            .subscribe(onNext: {
                self.result.accept($0)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
        
//        EventDetailCellType
    }
    
//    func headerTitle(sectionIdx idx: Int) -> String {
//        return sections.value[idx].header
//    }
}

enum EventDetailCellType {
    case title(title: String, period: String, isOpen: Bool/*공지사항에서만 사용*/, idx: Int/*공지사항에서만 사용*/)
    case image(imageUrl: String)
    case text(content: String)
}
// Tableview Cell
struct EventDetailSection {
    var items: [EventDetailCellType]
}

extension EventDetailSection: SectionModelType {
    init(original: EventDetailSection, items: [EventDetailCellType]) {
        self = original
        self.items = items
    }
}

