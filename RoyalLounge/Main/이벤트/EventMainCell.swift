//
//  EventMainCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/16.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class EventMainCell: UITableViewCell {

    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImageView.layer.cornerRadius = 10.0
        bannerImageView.clipsToBounds = true
        bannerImageView.layer.masksToBounds = true
        bannerImageView.contentMode = .scaleAspectFill
    }
    
    func setItem(_ item: EventMainModel) {
        bannerImageView.setImage(urlString: item.bannerImageURL)
        dateLabel.text = item.period
        
        endLabel.isHidden = true
        if let endYn = item.endYn {
            endLabel.text = item.endName ?? "종료"
            endLabel.isHidden = !endYn
        }
    }

}
