//
//  EventViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class EventViewController: ClearNaviLoggedBaseViewController {

    typealias DataSource = RxTableViewSectionedReloadDataSource<EventSection>

    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> EventMainCell in
            let cell: EventMainCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item)
            return cell
        })
        return dataSource
    }
    
    var viewModel: EventViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swipeBackAnyWhere()
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton

        viewModel = EventViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView.rx
            .itemSelected
//            .modelSelected(EventMainModel.self)
            .throttle(.milliseconds(1000), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { indexPath in
                self.mainTableView.deselectRow(at: indexPath, animated: true)
                let item = self.viewModel.getItem(indexPath: indexPath)
                if let end = item.endYn, end == true { return }
                
                if item.actionTypeEnum == .outUrl {
                    if let urlStr = item.linkURL {
                        let url = URL(string: urlStr)
                        guard UIApplication.shared.canOpenURL(url!) else {
                            toast(message: "연결할 수 없습니다", seconds: 1.5); return
                        }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url!)
                        }
                    }
                }else if item.actionTypeEnum == .detail {
                    let vc = RLStoryboard.myPage.eventDetailViewController(eventNo: item.eventNo)
                    self.push(vc)
                }else if item.actionTypeEnum == .insideApp {
                    if let inAppType = item.inAppTypeEnum {
                        switch inAppType {
                        case .store:
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        case .myPage: ()
                            self.pushBack() // 뒤로가기하면 마이페이지
                        case .todayCard, .addCard:
                            Coordinator.shared.toMain {
                                Coordinator.shared.toFirstTab()
                            }
                        case .cardHistory:
                            Coordinator.shared.toMain {
                                Coordinator.shared.toSecondTab()
                            }
                        case .liveMatch:
                            Coordinator.shared.toMain {
                                Coordinator.shared.toThirdTab()
                            }
                        }
                    }
                }
            })
            .disposed(by: bag)
        
        mainTableView.rx
            .setDelegate(self)
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "이벤트"
//        self.viewModel?.reloadData()
        hideNavigationControllerBottomLine()
    }
}

extension EventViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
