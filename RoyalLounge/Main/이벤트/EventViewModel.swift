//
//  EventViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class EventViewModel: BaseViewModel {

    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[EventSection]>.init(value: [])
    
    var userNo: String!
    init(userNo: String) {
        super.init()
        self.userNo = userNo
        
        MainInformation.shared.updateRedDot()
        
        reloadData()
    }
    
    func getItem(indexPath: IndexPath) -> EventMainModel {
        return sections.value[indexPath.section].items[indexPath.row]//.eventNo
    }
    
    func reloadData() {
        Repository.shared.main
            .getEventList(userNo: self.userNo, lastEventNo: nil)
            .subscribe(onNext: { result in
                RLUserDefault.shared.eventBbsRequestDate = Date.requestViewDate()
                self.sections.accept([EventSection.init(header: "이벤트", hasNext: result.hasNext, items: result.list)])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
}

// Tableview Cell
struct EventSection {
    var header: String
    var hasNext: Bool = false
    var items: [EventMainModel]
}

extension EventSection: SectionModelType {
    init(original: EventSection, items: [EventMainModel]) {
        self = original
        self.items = items
    }
}

