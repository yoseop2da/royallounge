//
//  AddCardCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/23.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AddCardCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    
    private var bag = DisposeBag()
    var selectAction: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 10.0)
        selectButton.backgroundColor = .brown25
        selectButton.setTitleColor(.brown75, for: .normal)
        selectButton.layer.cornerRadius = 4.0
        
        self.rx.tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
//                self.itemLabel.touchAnimation {
                    self.selectAction?()
//                }
            })
            .disposed(by: bag)
    }

    func setItem(item: AddCard) {
        nameLabel.text = item.title
//        let bgColor = item.isSelected ? UIColor.gray25 : UIColor.gray10
        backgroundColor = .white
    }
    
    @IBAction func selectButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.selectAction?()
        }
    }
    
}
