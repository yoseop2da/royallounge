//
//  AddCardData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

struct AddCardSection {
    var cardGroupNo: String
    var title: String
    var subTitle: String
    var optionType: OptionType?
    var cardNo: String?
    var items: [AddCard]
    var isOpened: Bool
}

extension AddCardSection: SectionModelType {
    typealias Item = AddCard
    init(original: AddCardSection, items: [Item]) {
        self = original
        self.items = items
    }
}

