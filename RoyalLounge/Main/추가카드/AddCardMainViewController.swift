//
//  AddCardMainViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/23.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class AddCardMainViewController: LoggedBaseViewController {
    
    typealias DataSource = RxTableViewSectionedReloadDataSource<AddCardSection>
    
    @IBOutlet weak var mainTableView: UITableView!
    var willMoveToStore: (() -> Void)?
    var willRefresh: (() -> Void)?
    
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, tableView, indexPath, item -> AddCardCell in
            let cell: AddCardCell = tableView.dequeueReusable(for: indexPath)
            cell.setItem(item: item)
            cell.selectAction = {
                if let cardNo = item.cardNo, let optionType = item.optionType {
                    self.openOption(cardNo: cardNo, optionType: optionType)
                }
            }
            return cell
        })
        
        return dataSource
    }
    
    var viewModel: AddCardMainViewModel!
    var isEditMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.creamwhiteBG
        
        let rBarButton = RLBarButtonItem(barType: .close)
        rBarButton.rx.tap
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItem = rBarButton
        
        viewModel = AddCardMainViewModel.init()
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                guard success else { return }
                if self.isEditMode {
                    // 뒤로가기
                }else {
                    // 다음 스텝으로 이동
                    let vc = RLStoryboard.join.profileTagViewController(optionType: (MainInformation.shared.gender == .m ? .charM : .charW), isEditMode: false)
                    self.push(vc)
                }
            })
            .disposed(by: bag)
        
        viewModel.sectionData
            .observeOn(MainScheduler.instance)
            .bind(to: mainTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        mainTableView
            .rx.setDelegate(self)
            .disposed(by: bag)
        
        //        mainTableView.rx.itemSelected
        //            .subscribe(onNext: { indexPath in
        //                self.viewModel.add(indexPath: indexPath)
        //            })
        //            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        hideNavigationControllerBottomLine()
        naviColorToBackgroundColor()
    }
}

extension AddCardMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.isOpened(section: indexPath.section) {
            return UITableView.automaticDimension
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init()
        let titleLabel = UILabel.init()
        let subTitleLabel = UILabel.init()
        let isSelected = viewModel.isSelected(section: section)
        titleLabel.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        titleLabel.text = viewModel.headerTitle(section: section)
        titleLabel.textColor = .primary100
        headerView.addSubview(titleLabel)
        
        headerView.rx.tapGesture()
            .filter{ $0.state == .ended }
            .subscribe(onNext: { _ in
                let item = self.viewModel.item(section: section)
                if let cardNo = item.cardNo, let optionType = item.optionType {
                    self.openOption(cardNo: cardNo, optionType: optionType)
                }
            })
            .disposed(by: bag)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(16)
            $0.leading.equalToSuperview().offset(31)
            $0.height.equalTo(20)
        }
        
        subTitleLabel.font = UIFont.spoqaHanSansRegular(ofSize: 14.0)
        subTitleLabel.text = viewModel.subTitle(section: section)
        subTitleLabel.textColor = .gray75
        headerView.addSubview(subTitleLabel)
        subTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(42)
            $0.leading.equalToSuperview().offset(31)
            $0.height.equalTo(18)
        }
        
        if viewModel.isOneDepth(section: section) {
            // 하위메뉴 없음
            let button = UIButton.init()
            button.setTitle("선택", for: .normal)
            button.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 10.0)
            button.backgroundColor = .brown25
            button.setTitleColor(.brown75, for: .normal)
            button.layer.cornerRadius = 4.0
            
            headerView.addSubview(button)
            button.snp.makeConstraints {
                $0.width.equalTo(43)
                $0.height.equalTo(28)
                $0.centerY.equalToSuperview()
                $0.trailing.equalToSuperview().offset(-30)
            }
            button.rx.tap
                .rxTouchAnimation(button: button, throttleDuration: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    let item = self.viewModel.item(section: section)
                    if let cardNo = item.cardNo, let optionType = item.optionType {
                        self.openOption(cardNo: cardNo, optionType: optionType)
                    }
                })
                .disposed(by: bag)
        }else{
            let button = UIButton.init()
            headerView.addSubview(button)
            button.snp.makeConstraints { $0.leading.trailing.top.bottom.equalToSuperview() }
            button.rx.tap
                .throttle(1.0, scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    self.viewModel.openCloseSection(section: section)
                })
                .disposed(by: bag)
            
            if viewModel.canOpen(section: section) {
                let imgView = UIImageView.init()
                imgView.tintColor = .dark
                imgView.image = UIImage.init(named: isSelected ? "icon16PxArrowUp" : "icon16PxArrowDown")
                headerView.addSubview(imgView)
                imgView.snp.makeConstraints {
                    $0.width.equalTo(18)
                    $0.height.equalTo(16)
                    $0.centerY.equalToSuperview()
                    $0.trailing.equalToSuperview().offset(-32)
                }
            }
        }
        
        let line = UIView.init()
        line.backgroundColor = .gray10
        headerView.addSubview(line)
        line.snp.makeConstraints {
            $0.height.equalTo(1)
            $0.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        return headerView
    }
    
    func openOption(cardNo: String, optionType: OptionType) {
        var popup: OptionBasePopup = OptionSpecPopup.init(nibName: OptionSpecPopup.className, bundle: nil)
        switch optionType {
        case .area:
            popup = OptionAreaPopup.init(nibName: OptionAreaPopup.className, bundle: nil)
        case .general:
            popup = OptionSpecPopup.init(nibName: OptionSpecPopup.className, bundle: nil)
        case .religion:
            popup = OptionReligionPopup.init(nibName: OptionReligionPopup.className, bundle: nil)
        case .salary:
            popup = OptionSalaryPopup.init(nibName: OptionSalaryPopup.className, bundle: nil)
        case .style:
            popup = OptionStylePopup.init(nibName: OptionStylePopup.className, bundle: nil)
        }
        popup.cardNo = cardNo
        popup.optionType = optionType
        popup.willMoveToStore = { needCrown in
            RLPopup.shared.showMoveStore(needCrown: needCrown, reasonStr: "추가 카드를 즉시 확인하기 위해", action: {
                self.dismiss(animated: true) {
                    self.willMoveToStore?()
                }
            })
        }
        popup.willRefresh = {
            self.dismiss(animated: true) {
                self.willRefresh?()
            }
        }
        self.clearPresent(popup)
    }
}
