
//
//  AddCardMainViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AddCardMainViewModel: BaseViewModel {
    let myValue = PublishSubject<String>.init() // areaNo
    
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[AddCardSection]>.init(value: [])
    
    let nextSuccess = PublishSubject<Bool>.init()
    
    override init() {
        super.init()
        
        Repository.shared.main
            .additionalCardList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: {
                self.sections.accept($0.map{ cardGroup -> AddCardSection in
                    if cardGroup.addCards.count == 0 {
                        return AddCardSection.init(cardGroupNo: cardGroup.cardGroupNo, title: cardGroup.title, subTitle: cardGroup.subTitle, optionType: cardGroup.optionType, cardNo: cardGroup.cardNo, items: [AddCard.init(isEmptyCard: true)], isOpened: false)
                    }else{
                        return AddCardSection.init(cardGroupNo: cardGroup.cardGroupNo, title: cardGroup.title, subTitle: cardGroup.subTitle, optionType: cardGroup.optionType, cardNo: cardGroup.cardNo, items: cardGroup.addCards, isOpened: false)
                    }
                })
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func openCloseSection(section: Int) {
        var dataList = sections.value
        var data = dataList[section]
        data.isOpened = !data.isOpened
        dataList[section] = data
        sections.accept(dataList)
    }

    func add(section: Int) {
        let data = sections.value
        let item = data[section]
        print("추가카드 : \(item)")
    }

    func item(section: Int) -> AddCardSection {
        return sections.value[section]
    }
        
    func headerTitle(section: Int) -> String {
        let data = sections.value
        return data[section].title
    }
    
    func subTitle(section: Int) -> String {
        let data = sections.value
        return data[section].subTitle
    }
    
    func isOneDepth(section: Int) -> Bool {
        let data = sections.value
        let items = data[section].items
        return items.count == 1 && items.first!.isEmptyCard
    }
    
    func isSelected(section: Int) -> Bool {
        let data = sections.value
        let value = data[section]
        return value.isOpened
    }
    
    func isOpened(section: Int) -> Bool {
        let data = sections.value
        let value = data[section]
        if value.items.count == 0 { return false }
        return value.isOpened
    }
    
    func canOpen(section: Int) -> Bool {
        let data = sections.value
        return data[section].items.count > 0
    }
}

