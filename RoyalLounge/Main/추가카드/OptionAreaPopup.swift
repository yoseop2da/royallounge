//
//  OptionAreaPopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OptionAreaPopup: OptionBasePopup {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainGroupWrapView: UIView!
    @IBOutlet weak var mainGroupArrow: UIImageView!
    @IBOutlet weak var mainGroupLabel: UILabel!
    @IBOutlet weak var mainGroupButton: UIButton!
    @IBOutlet weak var mainGroupTableView: UITableView!
    @IBOutlet weak var mainGroupTableviewBottom: NSLayoutConstraint! // 221
    
    @IBOutlet weak var subGroupWrapView: UIView!
    @IBOutlet weak var subGroupArrow: UIImageView!
    @IBOutlet weak var subGroupLabel: UILabel!
    @IBOutlet weak var subGroupButton: UIButton!
    @IBOutlet weak var subGroupTableView: UITableView!
    @IBOutlet weak var subGroupTableviewBottom: NSLayoutConstraint! // 165

    var viewModel: OptionAreaViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
        
        titleLabel.text = ""
        self.mainGroupArrow.tintColor = .dark
        
        let nibName = UINib(nibName: OptionAreaCell.className, bundle: nil)
        mainGroupTableView.register(nibName, forCellReuseIdentifier: OptionAreaCell.className)
        subGroupTableView.register(nibName, forCellReuseIdentifier: OptionAreaCell.className)
        
        mainGroupTableView.layer.cornerRadius = 6.0
        mainGroupWrapView.layer.cornerRadius = 6.0
        subGroupTableView.layer.cornerRadius = 6.0
        subGroupWrapView.layer.cornerRadius = 6.0
    
        mainGroupTableView.setBorder(color: .gray10)
        mainGroupWrapView.setBorder(color: .gray10)
        subGroupTableView.setBorder(color: .gray10)
        subGroupWrapView.setBorder(color: .gray10)
        
        mainGroupButton.rx.tap
            .rxTouchAnimation(button: mainGroupButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.hideSubTableView()
                
                if self.isShownMainTableview() {
                    self.hideMainTableView()
                    return
                }
                self.showMainTableView()
            })
            .disposed(by: bag)
        
        subGroupButton.rx.tap
            .rxTouchAnimation(button: subGroupButton, throttleDuration: RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.hideMainTableView()
                
                if self.isShownSubTableview() {
                    self.hideSubTableView()
                    return
                }
                self.showSubTableView()
            })
            .disposed(by: bag)
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.viewModel.needCrown.value.canPayable() {
                    RLPopup.shared.showNormal(parent: self, description: "크라운 \(self.viewModel.needCrown.value)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                        self.viewModel.addCard(minAge: self.selectedMinAge, maxAge: self.selectedMaxAge)
                    })
                }else{
                    self.dismiss {
                        self.willMoveToStore?(self.viewModel.needCrown.value)
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel = OptionAreaViewModel.init(
            optionType: optionType,
            userNo: MainInformation.shared.userNoAes!,
            cardNo: cardNo)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        RLPopup.shared.showNormal(parent: self, description: msg, rightButtonTitle: "확인")
                    }
                }
                self.dismiss()
            })
            .disposed(by: bag)
        
        viewModel.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: bag)

        viewModel.selectedMainArea
            .subscribe(onNext: { area in
                self.mainGroupLabel.text = area?.name
                if area != nil {
                    self.mainGroupTableView.reloadData()
                    self.viewModel.setSubArea(0)
                }
            })
            .disposed(by: bag)
        
        viewModel.selectedSubArea
            .subscribe(onNext: { area in
                self.subGroupLabel.text = area?.name
                if area != nil {
                    self.subGroupTableView.reloadData()
                }
            })
            .disposed(by: bag)
        
        viewModel.minAge
            .subscribe(onNext: { age in
                self.slider.minValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.maxAge
            .subscribe(onNext: { age in
                self.slider.maxValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                self.dismiss {
                    toast(message: "오늘의 카드에 추가되었습니다", seconds: 1.5)
                    self.willRefresh?()
                }
            })
            .disposed(by: bag)
        
        mainGroupTableView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width - 40, height: 6))
        subGroupTableView.tableHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width - 40, height: 6))
    }
}

extension OptionAreaPopup {
    func isShownMainTableview() -> Bool {
        return self.mainGroupTableviewBottom.constant == 221
    }
    
    func showMainTableView() {
        self.mainGroupTableView.contentInset.top = 0
        self.mainGroupTableView.contentOffset = .zero
        
        UIView.animate(withDuration: 0.3) {
            self.mainGroupTableviewBottom.constant = 221 // 221
            self.view.layoutIfNeeded()
            self.mainGroupTableView.reloadData()
            self.mainGroupArrow.image = UIImage.init(named: "icon16PxArrowUp")
        }
    }
    
    func hideMainTableView() {
        UIView.animate(withDuration: 0.3) {
            self.mainGroupTableviewBottom.constant = 0
            self.view.layoutIfNeeded()
            self.mainGroupArrow.image = UIImage.init(named: "icon16PxArrowDown")
        }
    }
    
    func isShownSubTableview() -> Bool {
        return self.subGroupTableviewBottom.constant == 165
    }
    
    func showSubTableView() {
        self.subGroupTableView.contentInset.top = 0
        self.subGroupTableView.contentOffset = .zero
        
        UIView.animate(withDuration: 0.3) {
            self.subGroupTableviewBottom.constant = 165
            self.view.layoutIfNeeded()
            self.subGroupTableView.reloadData()
            self.subGroupArrow.image = UIImage.init(named: "icon16PxArrowUp")
        }
    }
    
    func hideSubTableView() {
        UIView.animate(withDuration: 0.3) {
            self.subGroupTableviewBottom.constant = 0
            self.view.layoutIfNeeded()
            self.subGroupArrow.image = UIImage.init(named: "icon16PxArrowDown")
        }
    }
}

extension OptionAreaPopup: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainGroupTableView {
            return self.viewModel.areaGroupList.value.count
        }else{
            return self.viewModel.getSubList().count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OptionAreaCell = tableView.dequeueReusable(for: indexPath)
        if tableView == mainGroupTableView {
            let item = self.viewModel.areaGroupList.value[indexPath.row]
            cell.valueLabel.text = item.name
            if let selectedName = self.viewModel.selectedMainArea.value?.name {
                if selectedName == item.name {
                    // 컬러
                    cell.valueLabel.textColor = .dark
                }else{
                    cell.valueLabel.textColor = .rlLightGray155
                }
            }
        }else{
            let item = self.viewModel.getSubList()[indexPath.row]
            cell.valueLabel.text = item.name
            if let selectedName = self.viewModel.selectedSubArea.value?.name {
                if selectedName == item.name {
                    // 컬러
                    cell.valueLabel.textColor = .dark
                }else{
                    cell.valueLabel.textColor = .rlLightGray155
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideMainTableView()
        hideSubTableView()
        
        if tableView == mainGroupTableView {
            self.viewModel.setMainArea(indexPath.row)
        }else{
            self.viewModel.setSubArea(indexPath.row)
        }
    }
}
