//
//  OptionAreaViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionAreaViewModel: OptionBaseViewModel {
    
    let areaGroupList = BehaviorRelay<[AreaGroupListItem]>.init(value: [])
    let selectedMainArea = BehaviorRelay<AreaGroupListItem?>.init(value: nil)
    let selectedSubArea = BehaviorRelay<AreaListItem?>.init(value: nil)
    
    override init(optionType: OptionType, userNo: String, cardNo: String) {
        super.init(optionType: optionType, userNo: userNo, cardNo: cardNo)
        
        additionalCardOptionResult
            .subscribe(onNext: { result in
                self.areaGroupList.accept(result.areaGroupList ?? [])
                if let myAreaGroupNo = result.myAreaGroupNo,
                    let mainArea = result.areaGroupList?.first(where: { $0.areaGroupNo == myAreaGroupNo}),
                    let myAreaNo = result.myAreaNo,
                    let subArea = mainArea.areaList.first(where: { $0.areaNo == myAreaNo }) {
                    self.selectedMainArea.accept(mainArea)
                    self.selectedSubArea.accept(subArea)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: bag)
    }
    
    func getSubList() -> [AreaListItem] {
        guard let mainArea = self.selectedMainArea.value else { return [] }
        return mainArea.areaList
    }
    
    func setMainArea(_ idx: Int) {
        let new = areaGroupList.value[idx]
        self.selectedMainArea.accept(new)
    }
    
    func setSubArea(_ idx: Int) {
        let new = getSubList()[idx]
        self.selectedSubArea.accept(new)
    }
    
    func addCard(minAge: Int, maxAge: Int) {
        guard let areaNo = self.selectedSubArea.value?.areaNo else { return }
        mainAppDelegate.showBlockView()
        Repository.shared.main
            .addAdditionalCard(optionType: self.optionType, userNo: self.userNo, cardNo: self.cardNo, minAge: minAge, maxAge: maxAge, bodyCdList: nil, tallCdList: nil, religionCdList: nil, salaryCdList: nil, areaNo: areaNo)
            .subscribe(onNext: { result in
                MainInformation.shared.updateMyCrownNew()
                RLUserDefault.shared.optionAreaMinAge = minAge
                RLUserDefault.shared.optionAreaMaxAge = maxAge
                mainAppDelegate.hideBlockView()
                self.nextSuccess.on(.next(true))
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
        .disposed(by: bag)
    }
}

