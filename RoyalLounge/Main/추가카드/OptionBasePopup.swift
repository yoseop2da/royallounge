//
//  OptionBasePopup.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/24.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RangeSeekSlider

class OptionBasePopup: UIViewController {
    
    @IBOutlet weak var slider: RangeSeekSlider!
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var nextButtonWrapView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    let bag = DisposeBag()
    
    private var ANIMATION_TIMEINTERVAL: TimeInterval = 0.2
    private var OUTBUTTON_HIDDEN: CGFloat = 0.0
    private var OUTBUTTON_SHOWN: CGFloat = 1.0
    private var BOTTOM_VIEW_HIDDEN: CGFloat = 500.0
    private var BOTTOM_VIEW_SHOWN: CGFloat = 0.0
    
    let nextButton = RLNextButton.init(title: "이 설정의 카드 1장 받기")
    
    var selectedMinAge: Int = 20
    var selectedMaxAge: Int = 49
    
    var willMoveToStore: ((Int) -> Void)?
    var willRefresh: (() -> Void)?
    var cardNo: String!
    var optionType: OptionType!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButtonWrapView.addSubview(nextButton)
        nextButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        slider.delegate = self
        slider.minLabelFont = UIFont.spoqaHanSansBold(ofSize: 16)
        slider.maxLabelFont = UIFont.spoqaHanSansBold(ofSize: 16)
        slider.minDistance = 1.0
        slider.minValue = 20
        slider.maxValue = 49
        
        switch optionType {
        case .area:
            slider.selectedMinValue = CGFloat(RLUserDefault.shared.optionAreaMinAge)
            slider.selectedMaxValue = CGFloat(RLUserDefault.shared.optionAreaMaxAge)
            self.selectedMinAge = RLUserDefault.shared.optionAreaMinAge
            self.selectedMaxAge = RLUserDefault.shared.optionAreaMaxAge
        case .general:
            slider.selectedMinValue = CGFloat(RLUserDefault.shared.optionGeneralMinAge)
            slider.selectedMaxValue = CGFloat(RLUserDefault.shared.optionGeneralMaxAge)
            self.selectedMinAge = RLUserDefault.shared.optionGeneralMinAge
            self.selectedMaxAge = RLUserDefault.shared.optionGeneralMaxAge
        case .religion:
            slider.selectedMinValue = CGFloat(RLUserDefault.shared.optionReligionMinAge)
            slider.selectedMaxValue = CGFloat(RLUserDefault.shared.optionReligionMaxAge)
            self.selectedMinAge = RLUserDefault.shared.optionReligionMinAge
            self.selectedMaxAge = RLUserDefault.shared.optionReligionMaxAge
        case .salary:
            slider.selectedMinValue = CGFloat(RLUserDefault.shared.optionSalaryMinAge)
            slider.selectedMaxValue = CGFloat(RLUserDefault.shared.optionSalaryMaxAge)
            self.selectedMinAge = RLUserDefault.shared.optionSalaryMinAge
            self.selectedMaxAge = RLUserDefault.shared.optionSalaryMaxAge
        case .style:
            slider.selectedMinValue = CGFloat(RLUserDefault.shared.optionStyleMinAge)
            slider.selectedMaxValue = CGFloat(RLUserDefault.shared.optionStyleMaxAge)
            self.selectedMinAge = RLUserDefault.shared.optionStyleMinAge
            self.selectedMaxAge = RLUserDefault.shared.optionStyleMaxAge
            
        case .none:
            slider.selectedMinValue = CGFloat(RLUserDefault.shared.optionGeneralMinAge)
            slider.selectedMaxValue = CGFloat(RLUserDefault.shared.optionGeneralMaxAge)
            self.selectedMinAge = RLUserDefault.shared.optionGeneralMinAge
            self.selectedMaxAge = RLUserDefault.shared.optionGeneralMaxAge
        }

        mainAsync {
            self.wrapView.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return !UIScreen.hasNotch
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomViewShowAnimation(timeInterval: ANIMATION_TIMEINTERVAL)
    }

    func initialize() {
        self.bottomPadding.constant = BOTTOM_VIEW_HIDDEN
    }
    
    @IBAction func backButtonTouched(_ sender: Any) {
        self.dismiss()
    }
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.dismiss()
        }
    }
    
    var startPositionY: CGFloat?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            startPositionY = position.y
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let _startPositionY = startPositionY {
            let position = touch.location(in: view)
            let gap = position.y - _startPositionY
            let isDownAction = gap > 0
            self.bottomPadding.constant = isDownAction ? gap : gap*(0.2)
            let percent = CGFloat(1.0 - (isDownAction ? ((gap)/BOTTOM_VIEW_HIDDEN) : 0.0))
            let colorAlpha = CGFloat(0.6/*MAX*/ * percent)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(colorAlpha)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if bottomPadding.constant > 150 {
            self.dismiss()
        } else {
            self.bottomViewShowAnimation(timeInterval: 0.1)
        }
    }
}

extension OptionBasePopup {
    func dismiss(after: (() -> Void)? = nil) {
        bottomViewHideAnimation(after: after)
    }
    
    func bottomViewShowAnimation(timeInterval: TimeInterval) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = 0.0
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func bottomViewHideAnimation(after: (() -> Void)?) {
        UIView.animate(withDuration: self.ANIMATION_TIMEINTERVAL, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = self.BOTTOM_VIEW_HIDDEN
            self.view.backgroundColor = .clear
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: true, completion: { after?() })
        })
    }
}

// MARK: - RangeSeekSliderDelegate

extension OptionBasePopup: RangeSeekSliderDelegate {

    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        // min
        self.selectedMinAge = Int(round(minValue))
        // max
        self.selectedMaxAge = Int(round(maxValue))
        
        print("\(self.selectedMinAge) ~ \(self.selectedMaxAge)|| \(minValue) ~ \(maxValue)")
    }

    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }

    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}

class OptionSelectButton: UIButton {
    init() {
        super.init(frame: .zero)
        self.layer.cornerRadius = 6.0
        self.backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        self.layer.cornerRadius = 6.0
        self.backgroundColor = .white
    }
    
    func selected() {
        self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
        self.setTitleColor(.primary100, for: .normal)
        self.setTitleColor(.primary100, for: .highlighted)
        self.setBorder(color: .primary100)
    }
    
    func normal() {
        self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
        self.setTitleColor(.gray75, for: .normal)
        self.setTitleColor(.gray75, for: .highlighted)
        self.setBorder(color: .gray10)
    }
}
