
//
//  OptionBaseViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionBaseViewModel: BaseViewModel {
    let title = PublishSubject<String>.init()
    let subTitle = PublishSubject<String>.init()
    let needCrown = BehaviorRelay<Int>.init(value: 0)
    let minAge = BehaviorRelay<Int>.init(value: 20)
    let maxAge = BehaviorRelay<Int>.init(value: 49)
    
    let nextSuccess = PublishSubject<Bool>.init()
//    let errorMsg = PublishSubject<String>.init()
    
    let additionalCardOptionResult = PublishSubject<CardOptionResult>.init()
    
    var optionType: OptionType!
    var userNo: String!
    var cardNo: String!
    
    init(optionType: OptionType, userNo: String, cardNo: String) {
        super.init()
        
        self.optionType = optionType
        self.userNo = userNo
        self.cardNo = cardNo
        
        MainInformation.shared.updateMyCrownNew()
        
        Repository.shared.main
            .getAdditionalCardOption(userNo: userNo, cardNo: cardNo)
            .subscribe(onNext: { result in
                self.title.on(.next(result.title))
                self.subTitle.on(.next(result.subTitle ?? ""))
                self.needCrown.accept(result.crown)
                self.minAge.accept(result.minAge)
                self.maxAge.accept(result.maxAge)
                self.additionalCardOptionResult.on(.next(result))
                
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
    }
}
