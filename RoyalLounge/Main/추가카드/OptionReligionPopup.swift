//
//  OptionReligionPopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OptionReligionPopup: OptionBasePopup {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var religion0Button: OptionSelectButton!
    @IBOutlet weak var religion1Button: OptionSelectButton!
    @IBOutlet weak var religion2Button: OptionSelectButton!
    @IBOutlet weak var religion3Button: OptionSelectButton!
    @IBOutlet weak var religion4Button: OptionSelectButton!
    
    var viewModel: OptionReligionViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.viewModel.needCrown.value.canPayable() {
                    RLPopup.shared.showNormal(parent: self, description: "크라운 \(self.viewModel.needCrown.value)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                        self.viewModel.addCard(minAge: self.selectedMinAge, maxAge: self.selectedMaxAge)
                    })
                }else{
                    self.dismiss {
                        self.willMoveToStore?(self.viewModel.needCrown.value)
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel = OptionReligionViewModel.init(
            optionType: optionType,
            userNo: MainInformation.shared.userNoAes!,
            cardNo: cardNo)
        
        viewModel.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: bag)
        
        viewModel.religionList
            .subscribe(onNext: { list in
                let bodyButtons = [self.religion0Button, self.religion1Button, self.religion2Button, self.religion3Button, self.religion4Button]
                list.enumerated().forEach { idx, item in
                    let button = bodyButtons[idx]
                    button?.setTitle(item.name, for: .normal)
                    button?.tag = idx
                    button?.normal()
                }
                self.viewModel.selectedReligionIdxList.forEach { idx in
                    bodyButtons[idx]?.selected()
                }
            })
            .disposed(by: bag)
        
        viewModel.minAge
            .subscribe(onNext: { age in
                self.slider.minValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.maxAge
            .subscribe(onNext: { age in
                self.slider.maxValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                self.dismiss {
                    toast(message: "오늘의 카드에 추가되었습니다", seconds: 1.5)
                    self.willRefresh?()
                }
            })
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        RLPopup.shared.showNormal(parent: self, description: msg, rightButtonTitle: "확인")
                    }
                }
                self.dismiss()
            })
            .disposed(by: bag)
    }
    
    @IBAction func religionButtonTouched(_ sender: OptionSelectButton) {
        sender.touchAnimation{
            if self.viewModel.updateReligionIdx(idx: sender.tag) {
                sender.selected()
            }else{
                sender.normal()
            }
        }
    }
    
}

