//
//  OptionReligionViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionReligionViewModel: OptionBaseViewModel {
   
    let religionList = BehaviorRelay<[OptionListItem]>.init(value: [])
    var selectedReligionIdxList: [Int] = [0]//RLUserDefault.shared.optionReligionIdxList
    
    override init(optionType: OptionType, userNo: String, cardNo: String) {
        super.init(optionType: optionType, userNo: userNo, cardNo: cardNo)
    
        additionalCardOptionResult
            .subscribe(onNext: { result in
                if let myReligionCd = result.myReligionCd,
                    let myValueIdx = result.religionList?.firstIndex(where: { $0.value == myReligionCd}) {
                    self.selectedReligionIdxList = [myValueIdx]
                }
                self.religionList.accept(result.religionList ?? [])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            }).disposed(by: bag)

        
//        let myValue = Repository.shared.graphQl
//            .getReligion(userNo: MainInformation.shared.userNoAes!)
        
//        Observable.combineLatest(options, myValue)
//            .subscribe(onNext: { _options, myReligion in
//
//            })
//            .disposed(by: bag)
        
    }
    
    
    func updateReligionIdx(idx: Int) -> Bool {
        if selectedReligionIdxList.contains(idx) {
            if selectedReligionIdxList.count == 1 { return true } // 1개남은경우 선택 해제 안되도록
            selectedReligionIdxList.removeAll { $0 == idx }
            return false
        }else{
            selectedReligionIdxList.append(idx)
            selectedReligionIdxList.sort()
            return true
        }
    }
    
    func addCard(minAge: Int, maxAge: Int) {
        mainAppDelegate.showBlockView()
        
        let religionCdList: [String] = selectedReligionIdxList.map { religionList.value[$0].value }
        
        Repository.shared.main
            .addAdditionalCard(optionType: self.optionType, userNo: self.userNo, cardNo: self.cardNo, minAge: minAge, maxAge: maxAge,
                               bodyCdList: nil, tallCdList: nil, religionCdList: religionCdList, salaryCdList: nil, areaNo: nil)
            .subscribe(onNext: { result in
                MainInformation.shared.updateMyCrownNew()
//                RLUserDefault.shared.optionReligionIdxList = self.selectedReligionIdxList
                RLUserDefault.shared.optionReligionMinAge = minAge
                RLUserDefault.shared.optionReligionMaxAge = maxAge
                mainAppDelegate.hideBlockView()
                self.nextSuccess.on(.next(true))
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
}
