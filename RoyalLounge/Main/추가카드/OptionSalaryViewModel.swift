//
//  OptionSalaryViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionSalaryViewModel: OptionBaseViewModel {
    
    let salaryList = BehaviorRelay<[OptionListItem]>.init(value: [])
    var selectedSalaryIdxList: [Int] = [0]//RLUserDefault.shared.optionSalaryIdxList
    
    override init(optionType: OptionType, userNo: String, cardNo: String) {
       super.init(optionType: optionType, userNo: userNo, cardNo: cardNo)
        
        additionalCardOptionResult
            .subscribe(onNext: { result in
                self.salaryList.accept(result.salaryList ?? [])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            }).disposed(by: bag)

    }
    
    
    func updateSalaryIdx(idx: Int) -> Bool {
        if selectedSalaryIdxList.contains(idx) {
            if selectedSalaryIdxList.count == 1 { return true } // 1개남은경우 선택 해제 안되도록
            selectedSalaryIdxList.removeAll { $0 == idx }
            return false
        }else{
            selectedSalaryIdxList.append(idx)
            selectedSalaryIdxList.sort()
            return true
        }
    }
    
    func addCard(minAge: Int, maxAge: Int) {
        mainAppDelegate.showBlockView()
        
        let salaryCdList: [String] = selectedSalaryIdxList.map { salaryList.value[$0].value }
        
        Repository.shared.main
            .addAdditionalCard(optionType: self.optionType, userNo: self.userNo, cardNo: self.cardNo, minAge: minAge, maxAge: maxAge,
                               bodyCdList: nil, tallCdList: nil, religionCdList: nil, salaryCdList: salaryCdList, areaNo: nil)
            .subscribe(onNext: { result in
                MainInformation.shared.updateMyCrownNew()
//                RLUserDefault.shared.optionSalaryIdxList = self.selectedSalaryIdxList
                RLUserDefault.shared.optionSalaryMinAge = minAge
                RLUserDefault.shared.optionSalaryMaxAge = maxAge
                mainAppDelegate.hideBlockView()
                self.nextSuccess.on(.next(true))
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
        .disposed(by: bag)
    }
}
