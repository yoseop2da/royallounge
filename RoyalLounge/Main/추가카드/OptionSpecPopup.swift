//
//  OptionSpecPopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OptionSpecPopup: OptionBasePopup {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    var viewModel: OptionSpecViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
        
        titleLabel.text = ""
        subTitleLabel.text = ""
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.viewModel.needCrown.value.canPayable() {
                    RLPopup.shared.showNormal(parent: self, description: "크라운 \(self.viewModel.needCrown.value)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                        self.viewModel.addCard(minAge: self.selectedMinAge, maxAge: self.selectedMaxAge)
                    })
                }else{
                    self.dismiss {
                        self.willMoveToStore?(self.viewModel.needCrown.value)
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel = OptionSpecViewModel.init(
            optionType: optionType,
            userNo: MainInformation.shared.userNoAes!,
            cardNo: cardNo)
        
        viewModel.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: bag)
        
        self.subTitleLabel.text = "\n\n" // 초기 설정을 안해줄경우 오토레이아웃이 깨짐... 추후에 찾아보자....
        
        viewModel.subTitle
            .bind(to: subTitleLabel.rx.text)
            .disposed(by: bag)
        
        viewModel.minAge
            .subscribe(onNext: { age in
                self.slider.minValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.maxAge
            .subscribe(onNext: { age in
                self.slider.maxValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                self.dismiss {
                    toast(message: "오늘의 카드에 추가되었습니다", seconds: 1.5)
                    self.willRefresh?()
                }
            })
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        RLPopup.shared.showNormal(parent: self, description: msg, rightButtonTitle: "확인")
                    }
                }
                self.dismiss()
            })
            .disposed(by: bag)
    }
    
}
