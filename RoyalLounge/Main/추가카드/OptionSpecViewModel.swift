
//
//  OptionSpecViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionSpecViewModel: OptionBaseViewModel {
    
    override init(optionType: OptionType, userNo: String, cardNo: String) {
        super.init(optionType: optionType, userNo: userNo, cardNo: cardNo)

//        additionalCardOptionResult
//            .subscribe(onNext: { result in
//                
//            }, onError: {
//                
//            }).disposed(by: bag)
        
    }
    
    func addCard(minAge: Int, maxAge: Int) {
        mainAppDelegate.showBlockView()
        Repository.shared.main
            .addAdditionalCard(optionType: self.optionType, userNo: self.userNo, cardNo: self.cardNo, minAge: minAge, maxAge: maxAge,
                               bodyCdList: nil, tallCdList: nil, religionCdList: nil, salaryCdList: nil, areaNo: nil)
            .subscribe(onNext: { result in
                MainInformation.shared.updateMyCrownNew()
                RLUserDefault.shared.optionGeneralMinAge = minAge
                RLUserDefault.shared.optionGeneralMaxAge = maxAge
                mainAppDelegate.hideBlockView()
                
                self.nextSuccess.on(.next(true))
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
        .disposed(by: bag)
    }
}
