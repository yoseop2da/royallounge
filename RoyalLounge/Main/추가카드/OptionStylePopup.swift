//
//  OptionStylePopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OptionStylePopup: OptionBasePopup {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tall0Button: OptionSelectButton!
    @IBOutlet weak var tall1Button: OptionSelectButton!
    @IBOutlet weak var tall2Button: OptionSelectButton!
    
    @IBOutlet weak var bodyStyle0Button: OptionSelectButton!
    @IBOutlet weak var bodyStyle1Button: OptionSelectButton!
    @IBOutlet weak var bodyStyle2Button: OptionSelectButton!
    @IBOutlet weak var bodyStyle3Button: OptionSelectButton!
    @IBOutlet weak var bodyStyle4Button: OptionSelectButton!
    
    var viewModel: OptionStyleViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: .milliseconds(1500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if self.viewModel.needCrown.value.canPayable() {
                    RLPopup.shared.showNormal(parent: self, description: "크라운 \(self.viewModel.needCrown.value)개가 사용됩니다.\n계속 하시겠습니까?", leftButtonTitle: "취소", rightButtonTitle: "확인", leftAction: {}, rightAction: {
                        self.viewModel.addCard(minAge: self.selectedMinAge, maxAge: self.selectedMaxAge)
                    })
                }else{
                    self.dismiss {
                        self.willMoveToStore?(self.viewModel.needCrown.value)
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel = OptionStyleViewModel.init(
            optionType: optionType,
            userNo: MainInformation.shared.userNoAes!,
            cardNo: cardNo)
        
        viewModel.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: bag)
        
        viewModel.tallList
            .subscribe(onNext: { list in
                let tallButtons = [self.tall0Button, self.tall1Button, self.tall2Button]
                list.enumerated().forEach { idx, item in
                    let button = tallButtons[idx]
                    button?.setTitle(item.name, for: .normal)
                    button?.tag = idx
                    button?.normal()
                }
                self.viewModel.selectedTallIdxList.forEach { idx in
                    tallButtons[idx]?.selected()
                }
            })
            .disposed(by: bag)
        
        viewModel.bodyStyleList
            .subscribe(onNext: { list in
                let bodyButtons = [self.bodyStyle0Button, self.bodyStyle1Button, self.bodyStyle2Button, self.bodyStyle3Button, self.bodyStyle4Button]
                list.enumerated().forEach { idx, item in
                    let button = bodyButtons[idx]
                    button?.setTitle(item.name, for: .normal)
                    button?.tag = idx
                    button?.normal()
                }
                self.viewModel.selectedBodyStyleIdxList.forEach { idx in
                    bodyButtons[idx]?.selected()
                }
            })
            .disposed(by: bag)
        
        viewModel.minAge
            .subscribe(onNext: { age in
                self.slider.minValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.maxAge
            .subscribe(onNext: { age in
                self.slider.maxValue = CGFloat(age)
            })
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { success in
                self.dismiss {
                    toast(message: "오늘의 카드에 추가되었습니다", seconds: 1.5)
                    self.willRefresh?()
                }
            })
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        RLPopup.shared.showNormal(parent: self, description: msg, rightButtonTitle: "확인")
                    }
                }
                self.dismiss()
            })
            .disposed(by: bag)
    }
    
    @IBAction func tallButtonTouched(_ sender: OptionSelectButton) {
        sender.touchAnimation{
            if self.viewModel.updateTallIdx(idx: sender.tag) {
                sender.selected()
            }else{
                sender.normal()
            }
        }
    }
    
    @IBAction func bodyStyleButtonTouched(_ sender: OptionSelectButton) {
        sender.touchAnimation{
            if self.viewModel.updateBodyStyleIdx(idx: sender.tag) {
                sender.selected()
            }else{
                sender.normal()
            }
        }
    }
}
