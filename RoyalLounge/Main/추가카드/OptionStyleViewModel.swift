//
//  OptionStyleViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class OptionStyleViewModel: OptionBaseViewModel {
    
    let bodyStyleList = BehaviorRelay<[OptionListItem]>.init(value: [])
    let tallList = BehaviorRelay<[OptionListItem]>.init(value: [])
    
    var selectedBodyStyleIdxList: [Int] = [0]//RLUserDefault.shared.optionBodyStyleIdxList
    var selectedTallIdxList: [Int] = [0]//RLUserDefault.shared.optionTallIdxList
    
    override init(optionType: OptionType, userNo: String, cardNo: String) {
        super.init(optionType: optionType, userNo: userNo, cardNo: cardNo)
        
        additionalCardOptionResult
            .subscribe(onNext: { result in
                self.tallList.accept(result.tallList ?? [])
                self.bodyStyleList.accept(result.styleList ?? [])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            }).disposed(by: bag)
        
    }
    
    func updateTallIdx(idx: Int) -> Bool {
        if selectedTallIdxList.contains(idx) {
            if selectedTallIdxList.count == 1 { return true } // 1개남은경우 선택 해제 안되도록
            selectedTallIdxList.removeAll { $0 == idx }
            return false
        }else{
            selectedTallIdxList.append(idx)
            selectedTallIdxList.sort()
            return true
        }
    }
    
    func updateBodyStyleIdx(idx: Int) -> Bool {
        if selectedBodyStyleIdxList.contains(idx) {
            if selectedBodyStyleIdxList.count == 1 { return true } // 1개남은경우 선택 해제 안되도록
            selectedBodyStyleIdxList.removeAll { $0 == idx }
            return false
        }else{
            selectedBodyStyleIdxList.append(idx)
            selectedBodyStyleIdxList.sort()
            return true
        }
    }
    
    func addCard(minAge: Int, maxAge: Int) {
        let bodyCdList: [String] = selectedBodyStyleIdxList.map { bodyStyleList.value[$0].value }
        let tallCdList: [String] = selectedTallIdxList.map { tallList.value[$0].value }
        
        mainAppDelegate.showBlockView()
        Repository.shared.main
            .addAdditionalCard(optionType: self.optionType, userNo: self.userNo, cardNo: self.cardNo, minAge: minAge, maxAge: maxAge,
                               bodyCdList: bodyCdList, tallCdList: tallCdList, religionCdList: nil, salaryCdList: nil, areaNo: nil)
            .subscribe(onNext: { result in
                MainInformation.shared.updateMyCrownNew()
//                RLUserDefault.shared.optionBodyStyleIdxList = self.selectedBodyStyleIdxList
//                RLUserDefault.shared.optionTallIdxList = self.selectedTallIdxList
                RLUserDefault.shared.optionStyleMinAge = minAge
                RLUserDefault.shared.optionStyleMaxAge = maxAge
                mainAppDelegate.hideBlockView()
                self.nextSuccess.on(.next(true))
            }, onError: {
                mainAppDelegate.hideBlockView()
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
        .disposed(by: bag)
    }
}
