//
//  CardHistoryData.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

//
// 받은 카드 / 보낸 카드 / 성공 카드
//CardHistoryMainSection:
//[
//    // 슈퍼OK 카드 / OK 카드 / 나에게 높은 별점을 주었어요 / 내가 높은 별점을 주었어요 / 매칭 성공
//    CardHistoryGroupSection:
//    [
//
//        HistoryGroupSection:
//        [
//              // 카드
//              MainCardModel
//        ]
//    ]
//]


// 대분류
// 받은 카드 / 보낸 카드 / 성공 카드
struct CardHistoryMainSection {
    var header: String
    var items: [CardHistoryGroupSection]
}

extension CardHistoryMainSection: SectionModelType {
    init(original: CardHistoryMainSection, items: [CardHistoryGroupSection]) {
        self = original
        self.items = items
    }
}

//// 중분류
struct CardHistoryGroupSection {
    var historyType: CardHistoryGroupType
    var items: [HistoryGroupSection]
}

extension CardHistoryGroupSection: SectionModelType {
    init(original: CardHistoryGroupSection, items: [HistoryGroupSection]) {
        self = original
        self.items = items
    }
}
enum CardHistoryGroupType {
    case receive
    case send
    case success
    
    var emptyLabelString: String {
        if self == .receive {
            return "나를 좋아하는 카드가 표시됩니다"
        }else if self == .send {
            return "내가 좋아하는 카드가 표시됩니다"
        }else if self == .success {
            return "매칭에 성공한 카드가 표시됩니다"
        }
        return "카드가 표시됩니다"
    }
}
enum HistoryCardListType {
    case sendOk
    case sendSuperOk
    case receiveOk
    case receiveSuperOk
    case sendHigh
    case receiveHigh
    case matchSuccess
}

// SuperOK, OK, High
struct HistoryGroupSection {
    var header: String
    var historyCardListType: HistoryCardListType
    var cardPagingType: String // 서버에서 받은 타입
    var items: [HistoryGroup]
    var hasNext: Bool = false
}

extension HistoryGroupSection: SectionModelType {
    init(original: HistoryGroupSection, items: [HistoryGroup]) {
        self = original
        self.items = items
    }
}

struct HistoryGroup {
    var items: [MainCardModel]
}

extension HistoryGroup: SectionModelType {
    init(original: HistoryGroup, items: [MainCardModel]) {
        self = original
        self.items = items
    }
}
