//
//  CardHistoryMainCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class CardHistoryMainCell: UICollectionViewCell {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emtpyLabel: UILabel!
    @IBOutlet weak var emptyViewButton: UIButton!
    
    private var viewModel = CardHistoryMainCellViewModel()
    private var bag = DisposeBag()

    var openCardAction: ((MainCardModel, HistoryCardListType)->Void)?
    var moveCardDetailAction: ((MainCardModel, HistoryCardListType) -> Void)?
    var emptyButtonAction: (()->Void)?
    var refreshDataAction: (()->Void)?
    var moreCardAction: ((String, String, HistoryCardListType)->Void)?
    
    private var historyGroupSection: CardHistoryGroupSection!
    private var refreshControl: UIRefreshControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        emptyViewButton.layer.cornerRadius = 6.0
        refreshControl = UIRefreshControl()
        mainTableView.addSubview(refreshControl)
        refreshControl
            .rx.controlEvent(UIControl.Event.valueChanged)
            .subscribe(onNext: { [weak self] in
                self?.refreshControl.endRefreshing()
                self?.refreshDataAction?()
                self?.scrollToTop()
            })
            .disposed(by: bag)
    }
    
    func setData(data: CardHistoryGroupSection) {
        print("data : \(data.items.count)")
        if data.items.count == 0 {
            //bringSubviewToFront(emptyView)
            emtpyLabel.text = data.historyType.emptyLabelString
            emptyView.isHidden = false
            mainTableView.isHidden = true
        }else{
            //sendSubviewToBack(emptyView)
            emptyView.isHidden = true
            mainTableView.isHidden = false
        }
        historyGroupSection = data
        viewModel.refresh(item: data)
        
        mainTableView.reloadData()
    }
    
    func hideSuperOKGuideView() {
        if let cell = mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? CardSuperOKHistoryCell {
            cell.hideSuperOKGuideView()
        }
    }
    
    func scrollToTop() {
        mainTableView.setContentOffset(.zero, animated: true)
    }
    
    @IBAction func emptyViewButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.emptyButtonAction?()
        }
    }
}

extension CardHistoryMainCell: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideSuperOKGuideView()
    }
}

extension CardHistoryMainCell: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.sections.value.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.sections.value[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = self.viewModel.historyCardListType(idx: indexPath.section)
        let item = self.viewModel.sections.value[indexPath.section].items[indexPath.row]
        if type == .sendSuperOk || type == .receiveSuperOk {
            let cell: CardSuperOKHistoryCell = tableView.dequeueReusable(for: indexPath)
            cell.setData(data: item)
            
            cell.openCardAction = { idx in
                let historyCard = item.items[idx]
                self.openCardAction?(historyCard, type)
            }
            cell.moveCardDetailAction = { idx in
                let historyCard = item.items[idx]
                self.moveCardDetailAction?(historyCard, type)
            }
            return cell
        }else{
            let cell: CardNormalHistoryCell = tableView.dequeueReusable(for: indexPath)
            let historyCard = item.items[0]
            cell.setCard(historyCard, historyType: self.historyGroupSection.historyType, isHigh: type == .sendHigh || type == .receiveHigh)
            
            cell.openCardAction = {
                self.openCardAction?(historyCard, type)
            }
            cell.moveCardDetailAction = {
                self.moveCardDetailAction?(historyCard, type)
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let type = self.viewModel.historyCardListType(idx: indexPath.section)
        if type == .sendSuperOk || type == .receiveSuperOk {
            return 347
        }
        return 180
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let type = self.viewModel.historyCardListType(idx: section)
        if type == .sendSuperOk || type == .receiveSuperOk {
            return CGFloat.leastNormalMagnitude
        }
        return 77
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let type = self.viewModel.historyCardListType(idx: section)
        if type == .sendSuperOk || type == .receiveSuperOk {
            return CGFloat.leastNormalMagnitude
        }

        if self.viewModel.historyCardHasNext(idx: section) {
            return 58
        }else{
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let type = self.viewModel.historyCardListType(idx: section)
        if type == .sendSuperOk || type == .receiveSuperOk {
            return UIView()
        }
        
        let wrapView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 77))
        wrapView.backgroundColor = .clear
        
        let titleLabel = UILabel.init()
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.spoqaHanSansBold(ofSize: 18)
        titleLabel.text = viewModel.historyCardHeader(idx: section)
        titleLabel.textColor = .dark
        wrapView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(20)
            $0.top.equalToSuperview().offset(40)
            $0.height.equalTo(27)
            $0.bottom.equalToSuperview().offset(-10)
        }
        return wrapView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let type = self.viewModel.historyCardListType(idx: section)
        if type == .sendSuperOk || type == .receiveSuperOk {
            return UIView()
        }
        
        let wrapView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.width, height: 58))
        wrapView.backgroundColor = .clear
        
        if self.viewModel.historyCardHasNext(idx: section) {
            let moreButton = UIButton.init()
            moreButton.setTitle("더보기", for: .normal)
            moreButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
            moreButton.setTitleColor(.primary100, for: .normal)
            moreButton.setImage(UIImage.init(named: "icon16PxArrowDownOrange"), for: .normal)
            moreButton.semanticContentAttribute = .forceRightToLeft
            wrapView.addSubview(moreButton)
            moreButton.snp.makeConstraints {
                $0.top.bottom.leading.trailing.equalToSuperview()
            }
            moreButton.rx.tap
                .rxTouchAnimation(button: moreButton, scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    self.viewModel.moreCard(section: section)
                    let lastMatchNo = self.viewModel.lastMatchNo(section: section)
                    let cardPagingType = self.viewModel.cardPagingType(section: section)
                    
                    self.moreCardAction?(lastMatchNo, cardPagingType, type)
                    
                })
                .disposed(by: bag)
        }
        
        return wrapView
    }
}

class CardHistoryMainCellViewModel: BaseViewModel {
    lazy var sectionData = sections.asObservable()
    var sections = BehaviorRelay<[HistoryGroupSection]>.init(value: [])
    
    func historyCardListType(idx: Int) -> HistoryCardListType {
        return sections.value[idx].historyCardListType
    }
    
    func historyCardHasNext(idx: Int) -> Bool {
        return sections.value[idx].hasNext
    }
    
    func historyCardHeader(idx: Int) -> String {
        return sections.value[idx].header
    }
    
    override init() {
        super.init()
    }
    
    func refresh(item: CardHistoryGroupSection) {
        sections.accept(item.items)
    }
    
    func lastMatchNo(section: Int) -> String {
        let matchNo = sections.value[section].items.last?.items.last?.matchNo
        return matchNo ?? ""
    }
    func cardPagingType(section: Int) -> String {
        let cardPagingType = sections.value[section].cardPagingType
        return cardPagingType
    }
    func moreCard(section: Int) {
        let matchNo = sections.value[section].items.last?.items.last?.matchNo
        print("request more lastMatchNo: \(matchNo ?? "")")
    }
}
