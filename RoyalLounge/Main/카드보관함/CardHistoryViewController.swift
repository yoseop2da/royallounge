//
//  CardHistoryViewController.swift
//  RoyalRounge
//
//  Created by yoseop park on 2019/12/20.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class CardHistoryViewController: MainViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var receiveCardTapButton: UIButton!
    @IBOutlet weak var sendCardTapButton: UIButton!
    @IBOutlet weak var matchedCardTapButton: UIButton!
    
    @IBOutlet weak var selectWrapView: UIView!
    @IBOutlet weak var selectLine: UIView!
    @IBOutlet weak var selectLineLeading: NSLayoutConstraint!
    
    private var previousPage = 0
    
    private var viewModel: CardHistoryViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainAsync {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.itemSize = CGSize(width: UIScreen.width - 1, height: self.collectionView.frame.height)
            flowLayout.minimumLineSpacing = 0
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.sectionInset = .zero
            flowLayout.sectionInset.right = 0
            self.collectionView.collectionViewLayout = flowLayout
//            self.selectLine.roundCorners([.topLeft, .topRight], radius: 100.0)
            self.selectLine.layer.cornerRadius = 6.0
        }
        
        viewModel = CardHistoryViewModel(text: "")
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.receiveCardRefresh
            .subscribe(onNext: { result in
                if !result { return }
                self.collectionView.reloadItems(at: [IndexPath.init(row: 0, section: 0)])
            })
            .disposed(by: bag)
        viewModel.sendCardRefresh
            .subscribe(onNext: { result in
                if !result { return }
                self.collectionView.reloadItems(at: [IndexPath.init(row: 0, section: 1)])
            })
            .disposed(by: bag)
        
        viewModel.successCardRefresh
            .subscribe(onNext: { result in
                if !result { return }
                self.collectionView.reloadItems(at: [IndexPath.init(row: 0, section: 2)])
            })
            .disposed(by: bag)
        
        receiveCardTapButton.rx.tap
            .rxTouchAnimation(button: receiveCardTapButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.moveToTab(0)
            })
        .disposed(by: bag)
        
        sendCardTapButton.rx.tap
            .rxTouchAnimation(button: sendCardTapButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.moveToTab(1)
            })
        .disposed(by: bag)
        
        matchedCardTapButton.rx.tap
            .rxTouchAnimation(button: matchedCardTapButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.moveToTab(2)
            })
        .disposed(by: bag)
        
        selectTab(0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func detectedScreenShot() {
        super.detectedScreenShot()
        RLPopup.shared.detectCapture(rightAction: {})
    }
    
    func moveToTab(_ idx: Int) {
        if self.previousPage == idx {
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: self.previousPage)) as! CardHistoryMainCell
            cell.scrollToTop()
            return
        }
        
        self.collectionView.setContentOffset(CGPoint.init(x: UIScreen.width * CGFloat(idx), y: 0), animated: true)
    }
    
    func refreshData() {
        viewModel?.refresh(byHistoryType: .receive)
        viewModel?.refresh(byHistoryType: .send)
        viewModel?.refresh(byHistoryType: .success)
    }
    
    private func selectTab(_ idx: Int) {
        let width = UIScreen.width / 3.0
        let padding = (width - selectLine.frame.width) / 2.0
        UIView.animate(withDuration: 0.2) {
            self.selectLineLeading.constant = (width * CGFloat(idx)) + padding
            self.view.layoutIfNeeded()
        }
        
        receiveCardTapButton.setTitleColor(.gray50, for: .normal)
        receiveCardTapButton.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
        sendCardTapButton.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
        sendCardTapButton.setTitleColor(.gray50, for: .normal)
        matchedCardTapButton.titleLabel?.font = UIFont.spoqaHanSansRegular(ofSize: 16.0)
        matchedCardTapButton.setTitleColor(.gray50, for: .normal)
        
        if idx == 0 {
            receiveCardTapButton.setTitleColor(.primary100, for: .normal)
            receiveCardTapButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        }else if idx == 1 {
            sendCardTapButton.setTitleColor(.primary100, for: .normal)
            sendCardTapButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        }else if idx == 2 {
            matchedCardTapButton.setTitleColor(.primary100, for: .normal)
            matchedCardTapButton.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 16.0)
        }
            
    }
}

extension CardHistoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CardHistoryMainCell = collectionView.dequeueReusable(for: indexPath)
        
        let historyType = self.viewModel.cellItem(indexPath).historyType
        cell.setData(data: self.viewModel.cellItem(indexPath))
        cell.refreshDataAction = {
            self.viewModel.refresh(byHistoryType: historyType)
        }
        
        cell.moreCardAction = { lastMatchNo, cardPagingType, listType in
            self.viewModel.moreCard(lastMatchNo: lastMatchNo, cardPagingType: cardPagingType, listType: listType, reloadCallback: {
                cell.setData(data: self.viewModel.cellItem(indexPath))
            })
        }
        
        cell.emptyButtonAction = {
            Coordinator.shared.toFirstTab()
        }

        cell.openCardAction = { historyCard, type in
            self.viewModel.openCard(historyCard, historyCardListType: type, reloadCallback: {
                cell.setData(data: self.viewModel.cellItem(indexPath))
            })
        }
        
        cell.moveCardDetailAction = { historyCard, historyCardListType in
            let vc = RLStoryboard.todayCard.cardDetailViewController(matchNo: historyCard.matchNo) { refreshType in
                switch refreshType {
                case .none:
                    if !historyCard.openYn {
                        // 닫혀있는 경우만
                        // 카드 정보만 업데이트
                        self.viewModel.refresh(byHistoryType: historyType, reloadCallback: {
                            cell.setData(data: self.viewModel.cellItem(indexPath))
                        })
                    }
                case .deleted:
                    // 카테고리 리프래시
                    self.viewModel.refresh(byHistoryType: historyType)
                case .changedScore:
                    // 카드 정보만 업데이트
//                    self.viewModel.refresh(byHistoryType: historyType, reloadCallback: {
//                        cell.setData(data: self.viewModel.cellItem(indexPath))
//                    })
                    // 전체 업데이트
                    self.refreshData()
                case .changedRefresh:
                    // 전체 업데이트
                    self.refreshData()
                }
            }
            
            vc.isReceiveHigh = historyCardListType == .receiveHigh
            self.tabBarController?.push(vc)
        }
        return cell
    }
}

extension CardHistoryViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {    
        let pageWidth = scrollView.bounds.width
        let pageFraction = scrollView.contentOffset.x/pageWidth
        
        let page = Int((round(pageFraction)))
        if previousPage != page {
            previousPage = page
            selectTab(page)
            
            if viewModel == nil { return }
            if let cell = collectionView.cellForItem(at: IndexPath.init(row: 0, section: page)) as? CardHistoryMainCell {
                cell.hideSuperOKGuideView()
            }
        }
    }
}
