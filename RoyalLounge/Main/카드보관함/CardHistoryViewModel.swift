//
//  CardHistoryViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/30.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardHistoryViewModel: BaseViewModel {
    
    var receiveCardResult = BehaviorRelay<CardHistoryResult?>.init(value: nil)
    var sendCardResult = BehaviorRelay<CardHistoryResult?>.init(value: nil)
    var successCardResult = BehaviorRelay<CardHistoryResult?>.init(value: nil)
    
    var receiveCardRefresh = BehaviorRelay<Bool>.init(value: false)
    var sendCardRefresh = BehaviorRelay<Bool>.init(value: false)
    var successCardRefresh = BehaviorRelay<Bool>.init(value: false)
    
    var sections = BehaviorRelay<[CardHistoryMainSection]>.init(value: [])
    lazy var sectionData = sections.asObservable()
    
    init(text: String) {
        super.init()

        self.refreshReceive()
        self.refreshSend()
        self.refreshSuccess()
        
        Observable.combineLatest(receiveCardResult, sendCardResult, successCardResult)
            .subscribe(onNext: { receiveGroups, sendGroups, successGroups in
                var receiveHistoryGroupSections: [HistoryGroupSection] = []
                var sendHistoryGroupSections: [HistoryGroupSection] = []
                var successHistoryGroupSections: [HistoryGroupSection] = []
                
                if let _receiveGroups = receiveGroups {
                    if let superOkCard = _receiveGroups.superOkCard {
                        if superOkCard.list.count > 0 {
                            receiveHistoryGroupSections.append(HistoryGroupSection.init(header: "슈퍼OK 카드", historyCardListType: .receiveSuperOk, cardPagingType: superOkCard.type, items: [HistoryGroup.init(items: superOkCard.list)]))
                        }
                    }
                    if let okCard = _receiveGroups.okCard {
                        if okCard.list.count > 0 {
                            receiveHistoryGroupSections.append(HistoryGroupSection.init(header: "OK 카드", historyCardListType: .receiveOk, cardPagingType: okCard.type, items: okCard.list.map{ HistoryGroup.init(items:[$0]) }, hasNext: okCard.hasNext))
                        }
                    }
                    if let highScoreCard = _receiveGroups.highScoreCard {
                        if highScoreCard.list.count > 0 {
                            let headerStr = MainInformation.shared.outTester ? "나에게 높은 호감을 주었어요" : "나에게 높은 별점을 주었어요"
                            receiveHistoryGroupSections.append(HistoryGroupSection.init(header: headerStr, historyCardListType: .receiveHigh, cardPagingType: highScoreCard.type, items: highScoreCard.list.map{ HistoryGroup.init(items:[$0]) }, hasNext: highScoreCard.hasNext))
                        }
                    }
                }
                
                if let _sendGroups = sendGroups {
                    if let superOkCard = _sendGroups.superOkCard {
                        if superOkCard.list.count > 0 {
                            sendHistoryGroupSections.append(HistoryGroupSection.init(header: "슈퍼OK 카드", historyCardListType: .sendSuperOk, cardPagingType: superOkCard.type, items: [HistoryGroup.init(items: superOkCard.list)]))
                        }
                    }
                    if let okCard = _sendGroups.okCard {
                        if okCard.list.count > 0 {
                            sendHistoryGroupSections.append(HistoryGroupSection.init(header: "OK 카드", historyCardListType: .sendOk, cardPagingType: okCard.type, items: okCard.list.map{ HistoryGroup.init(items:[$0]) }, hasNext: okCard.hasNext))
                        }
                    }
                    if let highScoreCard = _sendGroups.highScoreCard {
                        if highScoreCard.list.count > 0 {
                            let headerStr = MainInformation.shared.outTester ? "내가 높은 호감을 주었어요" : "내가 높은 별점을 주었어요"
                            sendHistoryGroupSections.append(HistoryGroupSection.init(header: headerStr, historyCardListType: .sendHigh, cardPagingType: highScoreCard.type, items: highScoreCard.list.map{ HistoryGroup.init(items:[$0]) }, hasNext: highScoreCard.hasNext))
                        }
                    }
                }
                
                if let _successGroups = successGroups {
                    if let successCard = _successGroups.successCard {
                        if successCard.list.count > 0 {
                            successHistoryGroupSections.append(HistoryGroupSection.init(header: "매칭 성공", historyCardListType: .matchSuccess, cardPagingType: successCard.type, items: successCard.list.map{ HistoryGroup.init(items:[$0]) }, hasNext: successCard.hasNext))
                        }
                    }
                }
                self.sections.accept([
                    CardHistoryMainSection.init(header: "받은 카드", items: [CardHistoryGroupSection.init(historyType: .receive, items: receiveHistoryGroupSections)]),
                    CardHistoryMainSection.init(header: "보낸 카드", items: [CardHistoryGroupSection.init(historyType: .send, items: sendHistoryGroupSections)]),
                    CardHistoryMainSection.init(header: "성공 카드", items: [CardHistoryGroupSection.init(historyType: .success, items: successHistoryGroupSections)])
                ])
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func cellItem(_ indexPath: IndexPath) -> CardHistoryGroupSection {
        return sections.value[indexPath.section].items[indexPath.row]
    }
    
    func refresh(byHistoryType type: CardHistoryGroupType, reloadCallback: (()->Void)? = nil) {
        switch type {
        case .receive: self.refreshReceive(reloadCallback: reloadCallback)
        case .send: self.refreshSend(reloadCallback: reloadCallback)
        case .success: self.refreshSuccess(reloadCallback: reloadCallback)
        }
    }

    func moreCard(lastMatchNo: String, cardPagingType: String, listType: HistoryCardListType, reloadCallback: (()->Void)?) {
        Repository.shared.main
            .getHistoryCardMore(userNo: MainInformation.shared.userNoAes!, lastMatchNo: lastMatchNo, cardPagingType: cardPagingType)
            .subscribe(onNext: { historyCard in
                if listType == .receiveSuperOk {
                    var newResult = self.receiveCardResult.value
                    newResult?.superOkCard?.hasNext = historyCard.hasNext
                    newResult?.superOkCard?.list.append(contentsOf: historyCard.list)
                    self.receiveCardResult.accept(newResult)
                }else if listType == .receiveOk {
                    var newResult = self.receiveCardResult.value
                    newResult?.okCard?.hasNext = historyCard.hasNext
                    newResult?.okCard?.list.append(contentsOf: historyCard.list)
                    self.receiveCardResult.accept(newResult)
                }else if listType == .receiveHigh {
                    var newResult = self.receiveCardResult.value
                    newResult?.highScoreCard?.hasNext = historyCard.hasNext
                    newResult?.highScoreCard?.list.append(contentsOf: historyCard.list)
                    self.receiveCardResult.accept(newResult)
                }else if listType == .sendSuperOk {
                    var newResult = self.sendCardResult.value
                    newResult?.superOkCard?.hasNext = historyCard.hasNext
                    newResult?.superOkCard?.list.append(contentsOf: historyCard.list)
                    self.sendCardResult.accept(newResult)
                }else if listType == .sendOk {
                    var newResult = self.sendCardResult.value
                    newResult?.okCard?.hasNext = historyCard.hasNext
                    newResult?.okCard?.list.append(contentsOf: historyCard.list)
                    self.sendCardResult.accept(newResult)
                }else if listType == .sendHigh {
                    var newResult = self.sendCardResult.value
                    newResult?.highScoreCard?.hasNext = historyCard.hasNext
                    newResult?.highScoreCard?.list.append(contentsOf: historyCard.list)
                    self.sendCardResult.accept(newResult)
                }else if listType == .matchSuccess {
                    var newResult = self.successCardResult.value
                    newResult?.successCard?.hasNext = historyCard.hasNext
                    newResult?.successCard?.list.append(contentsOf: historyCard.list)
                    self.successCardResult.accept(newResult)
                }
                reloadCallback?()
                print("===================================historyCardhistoryCardhistoryCard \(historyCard)")
            }, onError: { err in
                
            })
        .disposed(by: bag)
    }
    
    func openCard(_ card: MainCardModel, historyCardListType: HistoryCardListType, reloadCallback: (()->Void)?) {
        if card.openYn { return }
        Repository.shared.main
            .openCard(userNo: MainInformation.shared.userNoAes!, matchNo: card.matchNo)
            .subscribe(onNext: { success in
                guard success else { return }
                var newCard = card
                newCard.openYn = true
                switch historyCardListType {
                case .receiveSuperOk:
                    var result = self.receiveCardResult.value!
                    if let idx = result.superOkCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.superOkCard!.list[idx] = newCard
                    }
                    self.receiveCardResult.accept(result)
                case .receiveOk:
                    var result = self.receiveCardResult.value!
                    if let idx = result.okCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.okCard!.list[idx] = newCard
                    }
                    self.receiveCardResult.accept(result)
                case .receiveHigh:
                    var result = self.receiveCardResult.value!
                    if let idx = result.highScoreCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.highScoreCard!.list[idx] = newCard
                    }
                    self.receiveCardResult.accept(result)
                case .sendSuperOk:
                    var result = self.sendCardResult.value!
                    if let idx = result.superOkCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.superOkCard!.list[idx] = newCard
                    }
                    self.sendCardResult.accept(result)
                case .sendOk:
                    var result = self.sendCardResult.value!
                    if let idx = result.okCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.okCard!.list[idx] = newCard
                    }
                    self.sendCardResult.accept(result)
                case .sendHigh:
                    var result = self.sendCardResult.value!
                    if let idx = result.highScoreCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.highScoreCard!.list[idx] = newCard
                    }
                    self.sendCardResult.accept(result)
                case .matchSuccess:
                    var result = self.successCardResult.value!
                    if let idx = result.successCard!.list.firstIndex(where:{ $0.matchNo == card.matchNo }) {
                        result.successCard!.list[idx] = newCard
                    }
                    self.successCardResult.accept(result)
                }
                reloadCallback?()
            },onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            }).disposed(by: bag)
    }
    
    private func refreshReceive(reloadCallback: (()->Void)? = nil) {
        Repository.shared.main
            .getHistoryReceiveCardList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: { result in
                self.receiveCardResult.accept(result)
                MainInformation.shared.updateRedDot()
                if let callback = reloadCallback {
                    callback()
                }else{
                    self.receiveCardRefresh.accept(true)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    private func refreshSend(reloadCallback: (()->Void)? = nil) {
        Repository.shared.main
            .getHistorySendCardList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: { result in
                self.sendCardResult.accept(result)
                if let callback = reloadCallback {
                    callback()
                }else{
                    self.sendCardRefresh.accept(true)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    private func refreshSuccess(reloadCallback: (()->Void)? = nil) {
        Repository.shared.main
            .getHistorySuccessMatchCardList(userNo: MainInformation.shared.userNoAes!)
            .subscribe(onNext: { result in
                self.successCardResult.accept(CardHistoryResult.init(type: result.type, superOkCard: nil, okCard: nil, highScoreCard: nil, successCard: result))
                if let callback = reloadCallback {
                    callback()
                }else{
                    self.successCardRefresh.accept(true)
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
    
    func setTestData() {
        let card1 = MainCardModel.init(nickname: "초밥 맨", photoUrl: "https://upload.wikimedia.org/wikipedia/commons/a/a8/Suzy_at_a_fansigning_event%2C_31_January_2017_01.jpg",
                       job: "간호사", age: 25, areaName: "서울", specCount: 3, dDay: 7, openYn: false, matchNo: "fLffV8y2q9YN9LoGuRpkjg==", bothHighYn: false)
        let card2 = MainCardModel.init(nickname: "와썹맨", photoUrl: "https://img-s-msn-com.akamaized.net/tenant/amp/entityid/AAHGN5F.img?h=0&w=720&m=6&q=60&u=t&o=f&l=f&x=277&y=282",
        job: "선생님", age: 26, areaName: "경기", specCount: 1, dDay: 5, openYn: true, matchNo: "F2_xFlLYGfNe9_Y2UXgGhA==", bothHighYn: true)
        let card3 = MainCardModel.init(nickname: "요맨", photoUrl: "https://img.hankyung.com/photo/201905/03.19745981.1.jpg",
        job: "개발자", age: 30, areaName: "제주도", specCount: 2, dDay: 2, openYn: true, matchNo: "p67VXWh-BB5ywCITGDlIIQ==", bothHighYn: false)
        
        let 받은카드: [HistoryGroupSection] = [
            HistoryGroupSection.init(header: "슈퍼OK 카드", historyCardListType: .receiveSuperOk, cardPagingType: "", items: [
                HistoryGroup.init(items: [card3, card2, card1, card2, card3]) //
            ], hasNext: true),
            HistoryGroupSection.init(header: "OK 카드", historyCardListType: .receiveOk, cardPagingType: "", items: [
                HistoryGroup.init(items: [card2]),
                HistoryGroup.init(items: [card3]),
                HistoryGroup.init(items: [card1]),
                HistoryGroup.init(items: [card2]),
                HistoryGroup.init(items: [card1])
            ], hasNext: true),
            HistoryGroupSection.init(header: MainInformation.shared.outTester ? "나에게 높은 호감을 주었어요" : "나에게 높은 별점을 주었어요", historyCardListType: .receiveHigh, cardPagingType: "", items: [
                HistoryGroup.init(items: [card2, card3])
            ], hasNext: true)]
        
        let 보낸카드: [HistoryGroupSection] = [
            HistoryGroupSection.init(header: "슈퍼OK 카드", historyCardListType: .sendSuperOk, cardPagingType: "", items: [
                HistoryGroup.init(items: [card1, card2, card3, card2, card1])
            ], hasNext: true),
            HistoryGroupSection.init(header: "OK 카드", historyCardListType: .sendOk, cardPagingType: "", items: [
                HistoryGroup.init(items: [card1]),
                HistoryGroup.init(items: [card2]),
                HistoryGroup.init(items: [card3])
            ], hasNext: true),
            HistoryGroupSection.init(header: MainInformation.shared.outTester ? "내가 높은 호감을 주었어요" : "내가 높은 별점을 주었어요", historyCardListType: .sendHigh, cardPagingType: "", items: [
                HistoryGroup.init(items: [card1]),
                HistoryGroup.init(items: [card2]),
                HistoryGroup.init(items: [card3])
            ], hasNext: true)
        ]
        
        let 성공카드: [HistoryGroupSection] = [
            HistoryGroupSection.init(header: "매칭 성공", historyCardListType: .matchSuccess, cardPagingType: "", items: [
                HistoryGroup.init(items: [card1]),
                HistoryGroup.init(items: [card2]),
                HistoryGroup.init(items: [card3])
            ], hasNext: true)
        ]
        self.sections.accept([
            CardHistoryMainSection.init(header: "받은 카드", items: [CardHistoryGroupSection.init(historyType: .receive, items: 받은카드)]),
            CardHistoryMainSection.init(header: "보낸 카드", items: [CardHistoryGroupSection.init(historyType: .send, items: 보낸카드)]),
            CardHistoryMainSection.init(header: "성공 카드", items: [CardHistoryGroupSection.init(historyType: .success, items: 성공카드)]),
        ])
    }
}
