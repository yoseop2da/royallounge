//
//  CardNormalHistoryCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class CardNormalHistoryCell: UITableViewCell {

    @IBOutlet weak var whiteWrapView: UIView!
    @IBOutlet weak var specImageView: UIImageView!
    @IBOutlet weak var specCountLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNotOpenImageView: UIImageView!
    @IBOutlet weak var profileGradientView: UIImageView!
    @IBOutlet weak var dDayWrapView: UIView!
    @IBOutlet weak var dDayLabel: UILabel!
    
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var subInfoLabel: UILabel!
    @IBOutlet weak var bothHighLabel: UILabel!
    @IBOutlet weak var actionButton: HistoryActionButton!
    
    var item: MainCardModel?
    var moveCardDetailAction: (()->Void)?
    var openCardAction: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        whiteWrapView.layer.cornerRadius = 10.0
        whiteWrapView.clipsToBounds = true
        whiteWrapView.setBorder(width: 1.0, color: .gray10)
        dDayWrapView.layer.cornerRadius = 4.0
        dDayWrapView.setShadow(radius: 0.5, color: .lightGray)
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.backgroundColor = .gray25
        profileNotOpenImageView.isHidden = false
        profileNotOpenImageView.contentMode = .scaleAspectFit
        
        bothHighLabel.layer.cornerRadius = 4.0
        bothHighLabel.layer.masksToBounds = true
        
        if MainInformation.shared.outTester {
            bothHighLabel.isHidden = true
        }
        
    }
    
    func setCard(_ card: MainCardModel, historyType: CardHistoryGroupType, isHigh: Bool) {
        item = card
        
        self.setBaseInfo(urlString: card.photoUrl, openYn: card.openYn, specCount: card.specCount, dDay: card.dDay)
        
        nicknameLabel.text = card.nickname
        subInfoLabel.text = "\(card.age)ㅣ\(card.areaName ?? "대한민국")ㅣ\(card.job)"
        
        actionButton.setHistoryType(historyType)
        
        if isHigh {
            actionButton.isHidden = true
            bothHighLabel.isHidden = !card.bothHighYn
        }else{
            actionButton.isHidden = false
            bothHighLabel.isHidden = true
        }
    }
    
    @IBAction func cardTouched(_ sender: Any) {
        if let card = self.item {
            if card.openYn {
                whiteWrapView.touchAnimation{
                    self.moveCardDetailAction?()
                }
            }else{
                self.openCardAction?()
            }
        }
    }
    
    @IBAction func cardOutSideTouched(_ sender: Any) {
        if let card = self.item {
            if card.openYn {
                self.touchAnimation{
                    self.moveCardDetailAction?()
                }
            }else{
                self.openCardAction?()
            }
        }
    }
    
    @IBAction func actionButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.moveCardDetailAction?()
        }
    }
    
    private func setBaseInfo(urlString: String, openYn: Bool, specCount: Int, dDay: Int) {
        self.profileImageView.setImage(urlString: urlString, placeholderImage: UIImage.init(named: "mainCardPlaceHolder"))
        self.specImageView.isHidden = specCount == 0
        self.specCountLabel.isHidden = specCount == 0
        self.specCountLabel.text = "\(specCount)"
        
        if openYn {
            profileNotOpenImageView.isHidden = true
        }else{
            profileNotOpenImageView.isHidden = false
        }
        
        dDayLabel.text = "D-\(dDay)"
        dDayWrapView.isHidden = dDay > 2
    }

}

class HistoryActionButton: UIButton {
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    func setHistoryType(_ type: CardHistoryGroupType) {
        switch type {
        case .receive:
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
            self.setTitleColor(.brown100, for: .normal)
            self.backgroundColor = .brown25
            self.setTitle("OK 수락", for: .normal)
            self.layer.cornerRadius = 6.0
        case .send:
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
            self.setTitleColor(.white, for: .normal)
            self.backgroundColor = .brown100
            self.setTitle("한번 더 슈퍼OK 보내기", for: .normal)
            self.layer.cornerRadius = 6.0
        case .success:
            self.titleLabel?.font = UIFont.spoqaHanSansBold(ofSize: 12)
            self.setTitleColor(.white, for: .normal)
            self.backgroundColor = .brown75
            self.setTitle("연락처 확인", for: .normal)
            self.layer.cornerRadius = 6.0
        }
    }
}
