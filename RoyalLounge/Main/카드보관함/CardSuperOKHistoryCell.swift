//
//  CardSuperOKHistoryCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/13.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class CardSuperOKHistoryCell: UITableViewCell {

    typealias DataSource = RxCollectionViewSectionedReloadDataSource<HistoryGroup>
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var badgeDescriptionWrapView: UIView!
    @IBOutlet weak var badgeDescriptionLabel: UILabel!
    @IBOutlet weak var guideImageView: UIImageView!
    
    @IBOutlet weak var progressWrapView: UIView!
    @IBOutlet weak var progressTrailing: NSLayoutConstraint!
    private var previousPage = 0
    private var viewModel = CardSuperOKHistoryCellViewmodel()
    private var bag = DisposeBag()
//    var modelSelected: ((HistoryGroup)->Void)?
    var openCardAction: ((Int)->Void)?
    var moveCardDetailAction: ((Int)->Void)?
    private var dataSource: DataSource {
        let dataSource = DataSource.init(configureCell: { dataSource, collectionView, indexPath, item -> UICollectionViewCell in
            let cell: HistorySuperOKCardCell = collectionView.dequeueReusable(for: indexPath)
            cell.setCard(item)
            cell.openCardAction = {
                self.openCardAction?(indexPath.row)
            }
            cell.moveCardDetailAction = {
                self.moveCardDetailAction?(indexPath.row)
            }
            return cell
        })
        
        return dataSource
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        progressWrapView.isHidden = true
        
        badgeDescriptionWrapView.layer.cornerRadius = 6
        badgeDescriptionWrapView.alpha = 0.0
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 200, height: 200)
        flowLayout.minimumLineSpacing = 16
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = .zero
        flowLayout.sectionInset.left = 128//155
        flowLayout.sectionInset.right = 16
        
        self.collectionView.collectionViewLayout = flowLayout
        
//        collectionView.rx.modelSelected(HistoryGroup.self)
//            .throttle(1.0, latest: false, scheduler: MainScheduler.instance)
//            .subscribe(onNext: { model in
//                self.modelSelected?(model)
//            })
//            .disposed(by: bag)
        
        viewModel.sectionData
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
    }

    func setData(data: HistoryGroup) {
        viewModel.refresh(data: data)
        if data.items.count >= 2 {
            self.progressWrapView.isHidden = false
            let progress = 100 / CGFloat(data.items.count)
            setProgress(percent: progress)
            self.collectionView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
        }else{
            self.progressWrapView.isHidden = true
            self.collectionView.setContentOffset(CGPoint.init(x: -27, y: 0), animated: true)
        }
    }
    
    func hideSuperOKGuideView() {
        hideBadgeToast(animated: false)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func superOKInfoButtonTouched(_ sender: UIButton) {
        sender.touchAnimation{
            self.showBadgeToast(title: "슈퍼OK란?", desc: "정말 놓치고 싶지 않은 상대에게 진심을 담아 마음을 전하고 싶을 때 사용할 수 있는 특별한 OK 권입니다.(무료 수락 가능)")
        }
    }
    
    func setProgress(percent: CGFloat) {
        progressWrapView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.progressTrailing.constant = 100 - percent
            self.layoutIfNeeded()
        }
    }
    
    @objc
    func finishSpecBadgeToast() {
        specBadgeToastTimer?.invalidate()
        specBadgeToastTimer = nil
        UIView.animate(withDuration: 1.0) {
            self.badgeDescriptionWrapView.alpha = 0.0
        }
    }
    
    var specBadgeToastTimer: Timer?
    func showBadgeToast(title: String, desc: String) {
        specBadgeToastTimer?.invalidate()
        specBadgeToastTimer = nil
        
        specBadgeToastTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(finishSpecBadgeToast), userInfo: nil, repeats: false)
        
        let fullStr = "\(title)\n\(desc)"
        let attrStr = NSMutableAttributedString.init(string: fullStr)
        let boldRange = (fullStr as NSString).range(of: title)
        attrStr.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 12)], range: boldRange)
        self.badgeDescriptionLabel.attributedText = attrStr
        self.badgeDescriptionWrapView.alpha = 0.0
        UIView.animate(withDuration: 0.3) {
            self.badgeDescriptionWrapView.alpha = 0.85
        }
    }
    
    func hideBadgeToast(animated: Bool) {
        if self.badgeDescriptionWrapView.alpha == 0.0 { return }
        if animated {
            UIView.animate(withDuration: 1.0) {
                self.badgeDescriptionWrapView.alpha = 0.0
            }
        }else{
            self.badgeDescriptionWrapView.alpha = 0.0
        }
    }
    
}

extension CardSuperOKHistoryCell: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideSuperOKGuideView()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetX = scrollView.contentOffset.x
        let pageWidth: CGFloat = 200.0
        let pageFraction = offsetX / pageWidth
        
        // 특정 offset 이후 숨기기.
        guideImageView.isHidden = offsetX > 150

        let page = Int((round(pageFraction)))
        if previousPage != page {
            previousPage = page
            if viewModel.cardCount >= 2 {
                let progress = 100 * CGFloat(page + 1) / CGFloat(viewModel.cardCount)
                self.setProgress(percent: progress)
            }
        }
    }
}

class CardSuperOKHistoryCellViewmodel: BaseViewModel {
    lazy var sectionData = sections.asObservable()
    private var sections = BehaviorRelay<[HistoryGroup]>.init(value: [])

    var cardCount: Int { return sections.value.first?.items.count ?? 0 }
    override init() {
        super.init()
    }
    
    func refresh(data: HistoryGroup) {
        sections.accept([data])
    }
}
