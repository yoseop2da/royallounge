//
//  HistorySuperOKCardCell.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class HistorySuperOKCardCell: CardBaseCell {
    
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var subInfoLabel: UILabel!
    
    @IBOutlet weak var gradientView: UIView!
    
    var item: MainCardModel?
    var moveCardDetailAction: (()->Void)?
    var openCardAction: (()->Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.gradientView.setSuperOKCardGradient()
    }
    
    func setCard(_ card: MainCardModel) {
        item = card
        
        self.setBaseInfo(urlString: card.photoUrl, openYn: card.openYn, specCount: card.specCount, dDay: card.dDay)
        
        nicknameLabel.text = card.nickname
        subInfoLabel.text = "\(card.age)ㅣ\(card.areaName ?? "대한민국")ㅣ\(card.job)"
    }
    
    @IBAction func cardTouched(_ sender: Any) {
        if let card = self.item {
            if card.openYn {
                whiteWrapView.touchAnimation{
                    self.moveCardDetailAction?()
                }
            }else{
                self.openCardAction?()
            }
        }
    }
    
    @IBAction func actionButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            // OK수락
        }
    }

}
