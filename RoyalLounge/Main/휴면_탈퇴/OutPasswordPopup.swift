//
//  OutPasswordPopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OutPasswordPopup: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textFieldWrapView: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bottomMainView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    private var ANIMATION_TIMEINTERVAL: TimeInterval = 0.2
    private var OUTBUTTON_HIDDEN: CGFloat = 0.0
    private var OUTBUTTON_SHOWN: CGFloat = 1.0
    private var BOTTOM_VIEW_HIDDEN: CGFloat = 610.0
    private var BOTTOM_VIEW_SHOWN: CGFloat = 0.0
    private var keyboardHeight: CGFloat = 0
    
    var didTouchNextButton: (() -> Void)?
    
    private var viewModel: OutPasswordPopupViewModel!
    
    let defaultTextField: RLNoTitleTextField = {
        let field = RLNoTitleTextField.init(placeholder: "비밀번호를 입력해주세요", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .done

        return field
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldWrapView.addSubview(defaultTextField)
        defaultTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        mainAsync {
            self.bottomMainView.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        initialize()
        
        viewModel = OutPasswordPopupViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            fieldString: defaultTextField.textObserver.debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance))

        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: nil)
            })
            .disposed(by: bag)
        
        defaultTextField.textObserver
            .subscribe(onNext: {
                self.nextButton.enableState(isEnable: $0.length > 0)
            })
            .disposed(by: bag)
        
        viewModel.validatedPassword
            .bind(to: defaultTextField.rx.setResult)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { result in
                guard result else { return }
                self.defaultTextField.resignFirstResponder()
                self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: {
                    self.didTouchNextButton?()
                })
            })
            .disposed(by: bag)

        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                self.keyboardHeight = keyboard.height
                let keyboardHide = keyboard.height == 0
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboardHide ? 0 : (-keyboard.height)
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    override var prefersStatusBarHidden: Bool {
        return !UIScreen.hasNotch
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomViewShowAnimation(timeInterval: ANIMATION_TIMEINTERVAL)
    }

    func initialize() {
        self.bottomPadding.constant = BOTTOM_VIEW_HIDDEN
    }
    
    @IBAction func nextButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.viewModel.save()
        }
    }
    @IBAction func backButtonTouched(_ sender: Any) {
        self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: nil)
    }

    var startPositionY: CGFloat?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            startPositionY = position.y
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let _startPositionY = startPositionY {
            let position = touch.location(in: view)
            let gap = position.y - _startPositionY
            if gap < 0 { return }
            
            let isDownAction = gap > 0
            self.bottomPadding.constant =  (isDownAction ? gap : gap*(0.2)) - (self.keyboardHeight)
            
            print("* gap : \(gap)|| \(self.bottomPadding.constant)")
            let percent = CGFloat(1.0 - (isDownAction ? ((gap)/BOTTOM_VIEW_HIDDEN) : 0.0))
            let colorAlpha = CGFloat(0.6/*MAX*/ * percent)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(colorAlpha)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.keyboardHeight > 0 && bottomPadding.constant > 50{
            self.dismiss(animated: true, completion: nil)
            return
        }else{
            if bottomPadding.constant > 180 {
                self.dismiss(animated: true, completion: nil)
                return
            }
        }
        
        self.bottomViewShowAnimation(timeInterval: 0.1)
        
    }
}

extension OutPasswordPopup {

    func bottomViewShowAnimation(timeInterval: TimeInterval) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = 0.0 - (self.keyboardHeight)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func bottomViewHideAnimation(timeInterval: TimeInterval, dismiss: Bool, after: (() -> Void)?) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = self.BOTTOM_VIEW_HIDDEN
            self.view.backgroundColor = .clear
            self.view.layoutIfNeeded()
        }, completion: { _ in
            if dismiss {
                self.dismiss(animated: true, completion: { after?() })
            } else {
                after?()
            }
        })
    }
}

class OutPasswordPopupViewModel: BaseViewModel {
    let validatedPassword: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    
    // input
    let inputFieldValue: BehaviorRelay<String> = BehaviorRelay.init(value: "")
    let nextSuccess = PublishSubject<Bool>.init()
    
    private var userNo: String!
    
    init(userNo: String, fieldString: Observable<String>) {
        super.init()
        self.userNo = userNo

        fieldString
            .bind(to: self.inputFieldValue)
            .disposed(by: bag)
        
        // 필드 입력값 변경시 필드 컬러 초기화해주기 위해 empty로 셋팅해줌.
        inputFieldValue.map { _ in TextFieldResult.empty }
            .bind(to: validatedPassword)
            .disposed(by: bag)
    }
    
    func save() {
        Repository.shared.main
            .checkPassword(userNo: userNo, currentPassword: inputFieldValue.value.aesEncrypted!)
            .subscribe(onNext: { success in
                self.nextSuccess.on(.next(success))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                if custom.resultCD == RLResultCD.NETWORK_ERROR.code || custom.resultCD == RLResultCD.E00000_Error.code {
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                }else{
                    self.validatedPassword.accept(TextFieldResult.failed(message: custom.errorMessage))
                }
            })
            .disposed(by: bag)
    }
}
