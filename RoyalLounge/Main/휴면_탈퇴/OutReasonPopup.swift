//
//  OutReasonPopup.swift
//  Route
//
//  Created by yoseop park on 24/06/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class OutReasonPopup: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textWrapView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeHolder: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bottomMainView: UIView!
    
//    @IBOutlet weak var textFieldWrapView: UIView!
    
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    private var ANIMATION_TIMEINTERVAL: TimeInterval = 0.2
    private var OUTBUTTON_HIDDEN: CGFloat = 0.0
    private var OUTBUTTON_SHOWN: CGFloat = 1.0
    private var BOTTOM_VIEW_HIDDEN: CGFloat = 610.0
    private var BOTTOM_VIEW_SHOWN: CGFloat = 0.0
    private var keyboardHeight: CGFloat = 0
    
    private var viewModel: OutReasonPopupViewModel!
    var didSuccessOut: (()->Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainAsync {
            self.bottomMainView.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        initialize()
        
        viewModel = OutReasonPopupViewModel.init(
            userNo: MainInformation.shared.userNoAes!,
            fieldString: textView.rx.text.orEmpty.debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance))

        textView.rx.text.orEmpty
            .scan("") { //(previous, new) -> String in
                self.placeHolder.isHidden = $1.length > 0
                self.nextButton.enableState(isEnable: $1.length > 0)
                if $1.length > 500 {
                    return $0 ?? String($1.prefix(500))
                } else {
                    return $1
                }
            }
            .subscribe(textView.rx.text)
            .disposed(by: bag)
        
        viewModel.nextSuccess
            .subscribe(onNext: { result in
                guard result else { return }
                self.textView.resignFirstResponder()
                self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: {
                    self.didSuccessOut?()
                })
            })
            .disposed(by: bag)

        viewModel.validated
            .subscribe(onNext: { result in
                switch result {
                case .empty, .failed:
                    self.textView.textColor = .rlBlack21
                case .failedProhibit:
                    self.textView.textColor = result.textViewTextColor
                default:
                    self.textView.textColor = .rlBlack21
                }
            })
            .disposed(by: bag)
      
        textWrapView.layer.cornerRadius = 6.0
        textWrapView.setBorder(color: .gray10)
        textView.textContainerInset = UIEdgeInsets.init(top: 4, left: 4, bottom: 0, right: 4)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                self.keyboardHeight = keyboard.height
                let keyboardHide = keyboard.height == 0
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboardHide ? 0 : (-keyboard.height)
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    override var prefersStatusBarHidden: Bool {
        return !UIScreen.hasNotch
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomViewShowAnimation(timeInterval: ANIMATION_TIMEINTERVAL)
    }

    func initialize() {
        self.bottomPadding.constant = BOTTOM_VIEW_HIDDEN
    }
    
    @IBAction func outButtonTouched(_ sender: UIButton) {
        sender.touchAnimation {
            self.viewModel.save()
        }
    }
    
    @IBAction func backButtonTouched(_ sender: Any) {
        self.bottomViewHideAnimation(timeInterval: self.ANIMATION_TIMEINTERVAL, dismiss: true, after: nil)
    }

    var startPositionY: CGFloat?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: view)
            startPositionY = position.y
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let _startPositionY = startPositionY {
            let position = touch.location(in: view)
            let gap = position.y - _startPositionY
            if gap < 0 { return }
            
            let isDownAction = gap > 0
            self.bottomPadding.constant =  (isDownAction ? gap : gap*(0.2)) - (self.keyboardHeight)
            
            print("* gap : \(gap)|| \(self.bottomPadding.constant)")
            let percent = CGFloat(1.0 - (isDownAction ? ((gap)/BOTTOM_VIEW_HIDDEN) : 0.0))
            let colorAlpha = CGFloat(0.6/*MAX*/ * percent)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(colorAlpha)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.keyboardHeight > 0 && bottomPadding.constant > 50{
            self.dismiss(animated: true, completion: nil)
            return
        }else{
            if bottomPadding.constant > 180 {
                self.dismiss(animated: true, completion: nil)
                return
            }
        }
        
        self.bottomViewShowAnimation(timeInterval: 0.1)
        
    }
}

extension OutReasonPopup {

    func bottomViewShowAnimation(timeInterval: TimeInterval) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = 0.0 - (self.keyboardHeight)
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func bottomViewHideAnimation(timeInterval: TimeInterval, dismiss: Bool, after: (() -> Void)?) {
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.bottomPadding.constant = self.BOTTOM_VIEW_HIDDEN
            self.view.backgroundColor = .clear
            self.view.layoutIfNeeded()
        }, completion: { _ in
            if dismiss {
                self.dismiss(animated: true, completion: { after?() })
            } else {
                after?()
            }
        })
    }

}

class OutReasonPopupViewModel: BaseViewModel {
    let validated: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    
    // input
    let inputFieldValue: BehaviorRelay<String> = BehaviorRelay.init(value: "")
    let nextSuccess = PublishSubject<Bool>.init()
    
    private var userNo: String!
    
    init(userNo: String, fieldString: Observable<String>) {
        super.init()
        self.userNo = userNo

        fieldString
            .bind(to: self.inputFieldValue)
            .disposed(by: bag)
        
        // 필드 입력값 변경시 필드 컬러 초기화해주기 위해 empty로 셋팅해줌.
        inputFieldValue
            .distinctUntilChanged()
            .map { _ in TextFieldResult.empty }
            .bind(to: validated)
            .disposed(by: bag)
    }
    
    func save() {
        guard inputFieldValue.value.count > 0 else { return }
        Repository.shared.main
            .outWithReason(userNo: userNo, outReason: inputFieldValue.value)
            .subscribe(onNext: { success in
                self.nextSuccess.on(.next(success))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                if custom.resultCD == RLResultCD.NETWORK_ERROR.code || custom.resultCD == RLResultCD.E00000_Error.code {
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                }else{
                    self.validated.accept(TextFieldResult.failed(message: custom.errorMessage))
                }
            })
            .disposed(by: bag)
    }
}
