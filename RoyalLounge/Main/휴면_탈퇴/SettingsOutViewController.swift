//
//  SettingsOutViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class SettingsOutViewController: ClearNaviLoggedBaseViewController {

    @IBOutlet weak var sleepButton: UIButton!
    @IBOutlet weak var outButton: UIButton!
    
    var viewModel: SettingsOutViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        sleepButton.layer.cornerRadius = 6.0
        
        if MainInformation.shared.isSleep {
            sleepButton.setTitle("계정 활성화하기", for: .normal)
            
            sleepButton.setTitleColor(.primary100, for: .normal)
            sleepButton.setTitleColor(.primary100, for: .highlighted)
            sleepButton.setBorder(color: .primary100)
            sleepButton.backgroundColor = .white
        }else{
            sleepButton.setTitle("휴면계정 설정하기", for: .normal)
            sleepButton.setTitleColor(.white, for: .normal)
            sleepButton.setTitleColor(.white, for: .highlighted)
            sleepButton.setBorder(color: .white)
            sleepButton.backgroundColor = .primary100
        }
        
        viewModel = SettingsOutViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        sleepButton.rx.tap
            .rxTouchAnimation(button: sleepButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if MainInformation.shared.isSleep {
                    self.viewModel.setSleepRelease()
                }else{
                    // 팝업 처리
                    let vc = SleepPopup.init(nibName: SleepPopup.className, bundle: nil)
                    vc.willMoveToStore = {
                        RLPopup.shared.showMoveStore {
                            // 스토어 이동
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        }
                    }
                    self.clearPresent(vc)
                }
            })
            .disposed(by: bag)
        
        outButton.rx.tap
            .rxTouchAnimation(button: outButton, scheduler: MainScheduler.instance)
            .flatMapLatest{ return self.viewModel.checkLiveMatch() }
            .subscribe(onNext: {
                if $0 {
                    RLPopup.shared.liveMatching(leftAction: {}, rightAction: {
                        self.outProcess()
                    })
                    return
                }
                self.outProcess()
                
            })
            .disposed(by: bag)
        
        swipeBackAnyWhere()

        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "탈퇴 안내"
        hideNavigationControllerBottomLine()
    }
    
    func outProcess() {
        // 평가 안한경우
        let vc = OutGuidePopup.init(nibName: OutGuidePopup.className, bundle: nil)
        vc.didTouchNextButton = {
            let vc = OutPasswordPopup.init(nibName: OutPasswordPopup.className, bundle: nil)
            vc.didTouchNextButton = {
                let vc = OutReasonPopup.init(nibName: OutReasonPopup.className, bundle: nil)
                vc.didSuccessOut = {
                    // 진짜 탈퇴됨.
                    RLPopup.shared.showNormal(description: "탈퇴가 완료되었으며, 개인정보보호정책에 따라 회원님의 정보는 안전하게 삭제됩니다.\n\n탈퇴 후 언제든 재가입이 가능합니다.\n감사합니다.", rightButtonTitle: "확인", rightAction: {
                        MainInformation.shared.logout()
                    })
                }
                self.clearPresent(vc)
            }
            self.clearPresent(vc)
        }
        self.clearPresent(vc)
    }
    
}

class SettingsOutViewModel: SettingsSleepViewModel {
    
    override init(userNo: String) {
        super.init(userNo: userNo)
    }
    
    func checkLiveMatch() -> Observable<Bool> {
        return Repository.shared.main
            .isLiveMatchSearching(userNo: userNo)
            .filter{ $0 != nil }
            .map{$0!}
    }

}
