
//
//  SettingsSleepViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/22.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class SettingsSleepViewController: ClearNaviLoggedBaseViewController {

    @IBOutlet weak var sleepButton: UIButton!
    @IBOutlet weak var outButton: UIButton!
    @IBOutlet weak var bottomInfoWrapView: UIView!
    
    var viewModel: SettingsSleepViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        sleepButton.layer.cornerRadius = 6.0
        bottomInfoWrapView.layer.cornerRadius = 6.0
        
        if MainInformation.shared.isSleep {
            sleepButton.setTitle("계정 활성화하기", for: .normal)
            
            sleepButton.setTitleColor(.primary100, for: .normal)
            sleepButton.setTitleColor(.primary100, for: .highlighted)
            sleepButton.setBorder(color: .primary100)
            sleepButton.backgroundColor = .white
        }else{
            sleepButton.setTitle("휴면계정 설정하기", for: .normal)
            sleepButton.setTitleColor(.white, for: .normal)
            sleepButton.setTitleColor(.white, for: .highlighted)
            sleepButton.setBorder(color: .white)
            sleepButton.backgroundColor = .primary100
        }
        
        viewModel = SettingsSleepViewModel.init(userNo: MainInformation.shared.userNoAes!)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        sleepButton.rx.tap
            .rxTouchAnimation(button: sleepButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                if MainInformation.shared.isSleep {
                    self.viewModel.setSleepRelease()
                }else{
                    // 팝업 처리
                    let vc = SleepPopup.init(nibName: SleepPopup.className, bundle: nil)
                    vc.willMoveToStore = {
                        RLPopup.shared.showMoveStore {
                            // 스토어 이동
                            let vc = RLStoryboard.main.storeViewController()
                            self.push(vc)
                        }
                    }
                    self.clearPresent(vc)
                    
                }
                
            })
            .disposed(by: bag)
        
        outButton.rx.tap
            .rxTouchAnimation(button: outButton, scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.myPage.settingsOutViewController()
                self.push(vc)
            })
            .disposed(by: bag)
        
        swipeBackAnyWhere()

        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "휴면 설정"
        hideNavigationControllerBottomLine()
    }
    
}
