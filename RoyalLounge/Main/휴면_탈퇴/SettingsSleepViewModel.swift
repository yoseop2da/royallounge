//
//  SettingsSleepViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/11/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift

class SettingsSleepViewModel: BaseViewModel {
    
    var userNo: String!
    init(userNo: String) {
        super.init()
        self.userNo = userNo
    }
    
    func setSleepRelease() {
        Repository.shared.main
            .sleep(userNo: userNo, sleepYn: false)
            .filter{ $0 == true }
            .subscribe(onNext: { success in
                toast(message: "계정이 활성화 되었습니다", seconds: 1.0) {
                    Coordinator.shared.moveByUserStatus()
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
        .disposed(by: bag)
    }

}
