//
//  SleepPopupViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class SleepPopupViewModel: BaseViewModel {
    let countResult = PublishSubject<(useCount: Int, maxCount: Int, sleepCrown: Int)>.init()
    
    private var userNo: String!

    init(userNo: String) {
        self.userNo = userNo
        super.init()
        
        MainInformation.shared.updateMyCrownNew()
        
        Repository.shared.main
            .getSleepCount(userNo: userNo)
            .map{(useCount: $0.useCount, maxCount: $0.maxCount, sleepCrown: $0.crown ?? 3)}
            .subscribe(onNext: {
                self.countResult.on(.next($0))
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))

            })
            .disposed(by: bag)
        
    }
  
    func sleep() {
        checkLiveMatch()
            .subscribe(onNext: { searchingLiveMatch in
                if searchingLiveMatch {
                    RLPopup.shared.liveMatching(leftAction: {}, rightAction: {
                        self.requestSleep()
                    })
                }else{
                    self.requestSleep()
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            })
            .disposed(by: self.bag)
    }
    
    private func requestSleep() {
        Repository.shared.main
            .sleep(userNo: userNo, sleepYn: true)
            .filter{ $0 == true }
            .subscribe(onNext: { success in
                toast(message: "휴면계정 상태로 전환되었습니다", seconds: 1.0) {
                    Coordinator.shared.moveByUserStatus()
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
        .disposed(by: bag)
    }
    
    private func checkLiveMatch() -> Observable<Bool> {
        return Repository.shared.main
            .isLiveMatchSearching(userNo: userNo)
            .filter{ $0 != nil }
            .map{$0!}
    }
}
