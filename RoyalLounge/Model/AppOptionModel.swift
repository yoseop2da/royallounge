//
//  AppOptionResponseModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

enum AppOptionType: String {
    case bodyM = "BODY_M"
    case bodyW = "BODY_W"
    case charM = "CHAR_M"
    case charW = "CHAR_W"
    case drink = "DRINK"
    case religion = "RELIGION"
    case education = "EDUCATION"
//    case interView = "INTERVIEW_QUESTION"
    case joinPath = "JOIN_PATH"
    case age = "AGE"
    
    
    var code: String { self.rawValue }
    
    //    닉네임>키>체형>주량>종교>거주지>성격>학교>직업>인터뷰>자기소개
    var nextType: AppOptionType? {
        switch self {
        case .bodyM, .bodyW:
            return .drink
        case .drink:
            return .religion
        default:
            return nil
        }
    }
}

struct AppOptionResponseModel: Codable {
    let resultCD, resultMsg: String
    let result: [AppOptionModel]

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct AppOptionModel: Codable {

    let groupCD, groupName: String
    let groupDesc: String?
    let optionCD, optionName, optionValue: String
    let optionDesc: String?
    
    var isSelected = false

    enum CodingKeys: String, CodingKey {
        case groupCD = "groupCd"
        case groupName, groupDesc
        case optionCD = "optionCd"
        case optionName, optionValue, optionDesc
    }
}

// MARK: Convenience initializers

extension AppOptionResponseModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(AppOptionResponseModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension AppOptionModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(AppOptionModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable {
    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
