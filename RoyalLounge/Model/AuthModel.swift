//
//  AuthModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AuthModel {
    let SUCCESS = "B000"
    var resultCd: String = "" // B000
    var resultMsg: String = "" //본인인증 완료
    var nameAES: String = "" //박요섭
    var gender: String = "" //M
    private var birthday: String = "" //19860106
    var birthdayAES: String = "" //19860106
    var localYn: String = "" //Y
    var koreanYn: Bool = true
    var telCo: String = "" //01
    var mbNoAES: String = "" //01089997677
    var ciAES: String = "" //ickEbutKH/fsADJv65R4QyP4iswUT4uk3BI+ISuB+lLJKcZ9c9vud1+dcZY48AW1IUPr5UAaxExnnUDvXY3ulg==

    private var today = Date()
    var userAge: Int {
        let birthStr = self.birthday
        let index = birthStr.index(birthStr.startIndex, offsetBy: 4)
        let yearStr = birthStr[..<index]
        let userBirthYear = Int(yearStr)!
        let age = (today.year() - userBirthYear) + 1
        return age
    }
    
    var isUserLimitedMinAge: Bool {
        return userAge < 20//CommonMessage.message.min_age
    }
    
    var isUserLimitedMaxAge: Bool {
        return userAge > 50//CommonMessage.message.max_age
    }

    convenience init?(messageBody: Any) {
        if let bodyStr = messageBody as? String, let data = bodyStr.data(using: .utf8) {
            let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: AnyObject]
            self.init(json: json)
        } else {
            return nil
        }
    }

    init?(json: [String: AnyObject]) {
        if let value = json["resultCd"] as? String {
            if value != SUCCESS { return nil }
            self.resultCd = value
        }
        if let value = json["resultMsg"] as? String { self.resultMsg = value }
        if let value = json["name"] as? String { self.nameAES = value.aesEncrypted! }
        if let value = json["gender"] as? String { self.gender = value }
        if let value = json["birthday"] as? String {
            self.birthday = value
            self.birthdayAES = value.aesEncrypted! }
        if let value = json["localYn"] as? String {
            self.localYn = value
            self.koreanYn = value == "Y"
        }
        if let value = json["telCo"] as? String { self.telCo = value }
        if let value = json["mbNo"] as? String { self.mbNoAES = value.aesEncrypted! }
        if let value = json["ci"] as? String { self.ciAES = value.aesEncrypted! }
    }
}
