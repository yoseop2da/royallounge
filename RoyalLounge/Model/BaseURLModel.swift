//
//  BaseURLModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/14.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

struct BaseURLModel: Codable {
    let resultCD, resultMsg: String
    let result: [BaseURL]

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct BaseURL: Codable {
    let name, host: String
}

// MARK: Convenience initializers

extension BaseURLModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(BaseURLModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension BaseURL {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(BaseURL.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

//{
//    "resultCd": "0000",
//    "resultMsg": "성공",
//    "result": [
//    {
//    "name": "운영",
//    "host": "https://api.routedate.com"
//    },
//    {
//    "name": "스테이징",
//    "host": "https://stg-api.routedate.com"
//    },
//    {
//    "name": "개발",
//    "host": "https://dev-api.routedate.com"
//    },
//    {
//    "name": "로컬(손성목)",
//    "host": "http://218.234.69.132:8081"
//    },
//    {
//    "name": "로컬(남시훈)",
//    "host": "http://218.234.69.132:8082"
//    }
//    ]
//}
