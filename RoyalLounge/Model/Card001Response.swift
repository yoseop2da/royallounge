
//
//  Card001Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/24.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct Card001Response: Codable {
    let resultCD, resultMsg: String
    let result: [Card001Result?]?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct Card001Result: Codable {
    let type: String
    let title, subTitle: String?
    let list: [Card001Item]?
    let hasNext: Bool?
}

struct Card001Item: Codable {
    let eventNo: String?
    let matchNo: String?
    let photoUrl: String?
    let job: String?
    let age, specCount, dDay: Int?
    let openYn: Bool?
    let cardNo, optionType, imageURL, bannerImageURL: String?
    let type, actionType, inAppType, linkURL: String?

    enum CodingKeys: String, CodingKey {
        case eventNo
        case matchNo
        case photoUrl
        case job, age, specCount, dDay, openYn, cardNo, optionType
        case imageURL = "imageUrl"
        case bannerImageURL = "bannerImageUrl"
        case type, actionType, inAppType
        case linkURL = "linkUrl"
    }
}

// MARK: Convenience initializers

extension Card001Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Card001Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Card001Result {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Card001Result.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Card001Item {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Card001Item.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
