
//
//  CardHistoryResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/29.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct CardHistoryResponse: Codable {
    let resultCD, resultMsg: String
    let result: CardHistoryResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct CardHistoryResult: Codable {
    let type: String
    var superOkCard, okCard, highScoreCard, successCard: HistoryCard?
}

struct HistoryCard: Codable {
    let type: String
    var list: [MainCardModel]
    var hasNext: Bool
}

struct MainCardModel: Codable {
    let matchNo, photoUrl, job, nickname: String
    let age, specCount, dDay: Int
    var openYn: Bool
    let areaName: String?
    let bothHighYn: Bool

    enum CodingKeys: String, CodingKey {
        case matchNo
        case photoUrl
        case job, nickname, age, specCount, dDay, openYn, areaName, bothHighYn
    }
    
    init(nickname: String, photoUrl: String, job: String, age: Int, areaName: String, specCount: Int, dDay: Int, openYn: Bool, matchNo: String, bothHighYn: Bool) {
        self.nickname = nickname
        self.photoUrl = photoUrl
        self.job = job
        self.age = age
        self.areaName = areaName
        self.specCount = specCount
        self.dDay = dDay
        self.openYn = openYn
        self.matchNo = matchNo
        self.bothHighYn = bothHighYn
    }
}

// MARK: Convenience initializers

extension CardHistoryResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardHistoryResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension CardHistoryResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardHistoryResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension HistoryCard {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(HistoryCard.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension MainCardModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(MainCardModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

