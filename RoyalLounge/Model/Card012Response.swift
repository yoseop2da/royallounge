//
//  Card012Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/30.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct Card012Response: Codable {
    let resultCD, resultMsg: String
    let result: HistoryCard?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

// MARK: Convenience initializers

extension Card012Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Card012Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

//{
//    "resultCd": "0000",
//    "resultMsg": "성공",
//    "result": {
//        "type": "RECEIVE_SUPER_OK",
//        "list": [
//            {
//                "matchNo": "BWC_rO1_DHcjDigLfOT8Vg==",
//                "photoUrl": "https://profile.royallounge.co.kr/photo/test-w-155.jpg?d=800x800&Expires=1596087812&Signature=elYFY1Jyz7Dj7I8fUDTiqAwBH6j8wHBpYK2lCCc3kSVxovwqD1mzwvvvPbYHYLw57m6iMQGbTe6MFf19qwTcuYByPgi4DqAP7jKoXBNyHTOV9ucFtoFn5DI7nZRTlqxTUWs~1~SfyFG8F5Pa95LaSI6UHRpDhJbhkPLLl3P~veUI2bV8NLOH9k5KE-zeKnS-fDv8DlKie2ixwyq-8q8M~hJIxc5BY22D04CkQ-RwRbHV8xkusJhQNqVcGAhQidXKJyTOLxlggZvL3irNOn~6VnTg60BLIOSB4qvY-8~M6c6kbNq6TlE7b-8etCVp~ebOU5VuqYqv6zNFSDqjVjn~BQ__&Key-Pair-Id=APKAIIHT7FVRWGJ3G34A",
//                "job": "회사원",
//                "nickname": "Yjj90",
//                "areaName": "서울",
//                "age": 31,
//                "specCount": 0,
//                "dDay": 6,
//                "openYn": false,
//                "bothHighYn": false
//            }
//        ],
//        "hasNext": false
//    }
//}
