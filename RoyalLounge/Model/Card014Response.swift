//
//  Card014Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/24.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct Card014Response: Codable {
    let resultCD, resultMsg: String
    let result: CardOptionResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct CardOptionResult: Codable {
    let title: String
    let subTitle: String?
    let minAge, maxAge, crown: Int
    let myAreaGroupNo, myAreaNo, myReligionCd: String?
    let areaGroupList: [AreaGroupListItem]?
    let salaryList, religionList, tallList, styleList: [OptionListItem]?
}

struct AreaGroupListItem: Codable {
    let areaGroupNo: String
    let name: String
    let areaList: [AreaListItem]
}

struct AreaListItem: Codable {
    let areaNo: String
    let name: String
}

struct OptionListItem: Codable {
    let name: String
    let value: String
}

// MARK: Convenience initializers

extension Card014Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Card014Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension CardOptionResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardOptionResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension AreaGroupListItem {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(AreaGroupListItem.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension AreaListItem {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(AreaListItem.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension OptionListItem {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(OptionListItem.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
