
//
//  Card016Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/24.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct Card016Response: Codable {
    let resultCD, resultMsg: String
    let result: [AddCardGroup]?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct AddCardGroup: Codable {
    let cardGroupNo: String
    let title, subTitle: String
    let cardNo: String?
    let optionType: OptionType?
    let addCards: [AddCard]
}

struct AddCard: Codable {
    let cardNo: String?
    let optionType: OptionType?
    let title: String?
    let subTitle: String?
    let iconURL: String?
    
    var isEmptyCard: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case cardNo, optionType, title, subTitle
        case iconURL = "iconUrl"
    }
    
    init(isEmptyCard: Bool) {
        cardNo = nil
        optionType = nil
        title = nil
        subTitle = nil
        iconURL = nil
        self.isEmptyCard = isEmptyCard
    }
}

enum OptionType: String, Codable {
    case area = "AREA"
    case general = "GENERAL"
    case religion = "RELIGION"
    case salary = "SALARY"
    case style = "STYLE"
}

// MARK: Convenience initializers

extension Card016Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Card016Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension AddCardGroup {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(AddCardGroup.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension AddCard {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(AddCard.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

