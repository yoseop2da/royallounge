//
//  CardDetailEditResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct CardDetailEditResponse: Codable {
    let resultCD, resultMsg: String
    let result: CardDetailEditResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct CardDetailEditResult: Codable {
    let matchNo, userNo: String?
    let photoList: [String?]
    var photoStatus: String?
    var photoStatusName: String?
    var specList: [CardDetailSpec]?
    let areaName, job: String
    let tall: Int
    let nickname: String
    let age: Int
    let aboutMe: String
    let body: String
    var sendOkMsg, acceptOkMsg, receiveOkMsg: String?
    let drink, religion: String
    let charList: [String]
    let edu: String
    let interviewList: [Interview]?
    
    let university: University?
    let company: Company?
    let hobbyList, interestList, haveList, wantList: [String]?
    var jobCareer, mbNo: String?
    
    var topInfoString: String {
        return "\(age)ㅣ\(areaName)ㅣ\(job)"
    }
}

struct University: Codable {
    let university, universityStatus, universityStatusName: String
}

struct Company: Codable {
    let company, companyStatus, companyStatusName: String
}

// MARK: Convenience initializers

extension CardDetailEditResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardDetailEditResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension CardDetailEditResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardDetailEditResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension University {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(University.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Company {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Company.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
