//
//  CardDetailResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct CardDetailResponse: Codable {
    let resultCD, resultMsg: String
    let result: CardDetailResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct CardDetailResult: Codable {
    let matchNo, userNo: String?
    var cardType: String?
    var scoreType: String?
    let photoList: [String?]
    var specList: [CardDetailSpec]?
    let areaName, job: String
    let tall: Int
    let nickname: String
    let age: Int
    let dDay: Int?
    let aboutMe: String
    var score: Int?
    let body: String
    var sendOkMsg, acceptOkMsg, receiveOkMsg: String?
    let drink, religion: String
    let charList: [String]
    let edu: String
    let interviewList: [Interview]?
    
    let university, company: String?
    let hobbyList, interestList, haveList, wantList: [String]?
    var jobCareer, mbNo: String?
    
    var topInfoString: String {
        return "\(age)ㅣ\(areaName)ㅣ\(job)"
    }
    var isMatchedSuccess: Bool {
        let type = cardTypeEnum
        return type == .matchSuccessOK || type == .matchSuccessSuperOK
    }
    
    var cardTypeEnum: CardType? {
        guard let _cardType = cardType else { return nil }
        return CardType.type(fromString: _cardType)
    }
}

struct CardDetailSpec: Codable {
    let title, description: String?
}

struct Interview: Codable {
    let question, answer: String
}

// MARK: Convenience initializers

extension CardDetailResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardDetailResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension CardDetailResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardDetailResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension CardDetailSpec {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(CardDetailSpec.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Interview {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Interview.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
