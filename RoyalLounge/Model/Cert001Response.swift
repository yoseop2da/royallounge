//
//  Cert001Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

struct Cert001Response: Codable {
    let resultCD, resultMsg: String
    let result: Cert001Result?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct Cert001Result: Codable {
    let outYn: Bool
    let day: Int?
    let userID, recoveryMsg, userNo: String?

    enum CodingKeys: String, CodingKey {
        case outYn, day
        case userID = "userId"
        case userNo, recoveryMsg
    }
}

// MARK: Convenience initializers

extension Cert001Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Cert001Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Cert001Result {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Cert001Result.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

