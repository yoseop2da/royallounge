
//
//  Crown001Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/08.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct Crown001Response: Codable {
    let resultCD, resultMsg: String
    let result: [String: Int]

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

// MARK: Convenience initializers

extension Crown001Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Crown001Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
