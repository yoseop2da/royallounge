//
//  CrownModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/07/09.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct CrownModel {
    var tall: Int = 5 // [사용] 키 수정
    var nickname: Int = 30 // [사용] 닉네임 수정
    
    var joinAddProfile: Int = 5 // [보상] 추가 프로필 등록 완료
    var joinUniv: Int = 3 // [보상] 학교인증 완료
    var joinCo: Int = 3 // [보상] 직장인증 완료
    var joinPath: Int = 1 // [보상] 가입경로 설문조사 완료
    var recoDo: Int = 40 // [보상] 추천인코드 등록
    var recoReceive: Int = 40 // [보상] 추천인코드 받음
    var cardEval: Int = 1 // [적립] 별점 매력평가 참여
    
    var ok: Int = 5 // [사용] OK 보내기
    var okPast: Int = 5 // [사용] 지난 OK 보내기
    var okHigh: Int = 5 // [사용] 높은 별점 OK 보내기
    var okAccept: Int = 5 // [사용] OK 수락하기
    var okConfirm: Int = 20 // [사용] OK 성공 연락처 확인하기
    
    var superOk: Int = 20 // [사용] 슈퍼 OK 보내기
    var superOkPast: Int = 20 // [사용] 지난 슈퍼 OK 보내기
    var superOkAccept: Int = 0 // [사용] 슈퍼 OK 수락하기
    var superOkConfirm: Int = 0 // [사용] 슈퍼 OK 성공 연락처 확인하기

    var cardDel: Int = 5 // [사용] 카드 삭제하기
    var sleep: Int = 5 // [사용] 카드 삭제하기
    
    var liveMatchRevert: Int = 5 // [사용] 카드 되돌리기
    var liveMatchDetail: Int = 35 // [사용] 카드 즉시 확인
    var liveMatchReqAdd: Int = 0 // [사용] 나에게 호감있는 이성찾기
    var liveMatchSendLikeDetail: Int = 35 // [사용] 내가 보낸 좋아요 확인
    var liveMatchReceiveLikeDetail: Int = 5 // [사용] 라이브 매칭 참여
    var liveMatchEval: Int = 1 // [사용] 라이브 매칭 참여
//    var liveMatchAdd: Int = 0 // [사용] 라이브 매칭 참여
    
    
    init?(json: [String: Int]) {
        if let value = json["TALL"] { tall = value }
        if let value = json["NICKNAME"] { nickname = value }
        
        if let value = json["JOIN_ADD_PROFILE"] { joinAddProfile = value }
        if let value = json["JOIN_UNIV"] { joinUniv = value }
        if let value = json["JOIN_CO"] { joinCo = value }
        if let value = json["JOIN_PATH"] { joinPath = value }
        if let value = json["RECO_DO"] { recoDo = value }
        if let value = json["RECO_RECEIVE"] { recoReceive = value }
        if let value = json["CARD_EVAL"] { cardEval = value }
        
        if let value = json["OK"] { ok = value }
        if let value = json["OK_PAST"] { okPast = value }
        if let value = json["OK_HIGH"] { okHigh = value }
        if let value = json["OK_ACCEPT"] { okAccept = value }
        if let value = json["OK_MB_NO_CONFIRM"] { okConfirm = value }
        
        
        if let value = json["SUPER_OK"] { superOk = value }
        if let value = json["SUPER_OK_PAST"] { superOkPast = value }
        if let value = json["SUPER_OK_ACCEPT"] { superOkAccept = value }
        if let value = json["SUPER_OK_MB_NO_CONFIRM"] { superOkConfirm = value }
        
        if let value = json["CARD_DEL"] { cardDel = value }
        if let value = json["SLEEP"] { sleep = value }
        

        if let value = json["LIVE_MATCH_REVERT"] { liveMatchRevert = value }
        if let value = json["LIVE_MATCH_DETAIL"] { liveMatchDetail = value }
        if let value = json["LIVE_MATCH_REQ_ADD"] { liveMatchReqAdd = value }
        if let value = json["LIVE_MATCH_SEND_LIKE_DETAIL"] { liveMatchSendLikeDetail = value }
        if let value = json["LIVE_MATCH_RECEIVE_LIKE_DETAIL"] { liveMatchReceiveLikeDetail = value }
        if let value = json["LIVE_MATCH_EVAL"] { liveMatchEval = value }
        
//        if let value = json["LIVE_MATCH_ADD"] { liveMatchAdd = value }
    }
}
   

//"LIVE_MATCH_ADD" : 10,
//"EVENT_NOTI" : 30,
//"RECEIVE_HIGH_SCORE_OPEN" : 5,
