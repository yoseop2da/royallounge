//
//  EventListResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/08.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct EventListResponse: Codable {
    let resultCD, resultMsg: String
    let result: EventListResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct EventListResult: Codable {
    let list: [EventMainModel]
    let hasNext: Bool
}

struct EventMainModel: Codable {
    let eventNo, title, contentType, content: String
    let endYn: Bool?
    let imageURL: String?
    let bannerImageURL: String
    let inAppType, linkURL: String?
    let actionType, period: String
    let endName: String?
    
    var actionTypeEnum: ActionType {
        return ActionType.convertType(from: actionType)
    }
    var inAppTypeEnum: InAppType? {
        if let value = inAppType {
            return InAppType.convertType(from: value)
        }
        return nil
    }
    enum CodingKeys: String, CodingKey {
        case eventNo, title, contentType, content, endYn
        case imageURL = "imageUrl"
        case bannerImageURL = "bannerImageUrl"
        case actionType, inAppType
        case linkURL = "linkUrl"
        case period, endName
    }
}

// MARK: Convenience initializers

extension EventListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(EventListResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension EventListResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(EventListResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension EventMainModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(EventMainModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
