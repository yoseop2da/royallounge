
//
//  LiveMatchCardResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/19.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

//

struct LiveMatchCardResponse: Codable {
    let resultCD, resultMsg: String
    let result: LiveMatchCardResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct LiveMatchCardResult: Codable {
    let liveMatchHistNo: String?
    let matchNo: String?
    let newCardYn, endYn, rewardYn: Bool
    let card: LiveMatchCardModel?
}

struct LiveMatchCardModel: Codable {
    let userNo: String
    let photoURLList: [String]
    let title, subTitle: String
    let specCount: Int

    enum CodingKeys: String, CodingKey {
        case userNo
        case photoURLList = "photoUrlList"
        case title, subTitle, specCount
    }
}

// MARK: Convenience initializers

extension LiveMatchCardResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchCardResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension LiveMatchCardResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchCardResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension LiveMatchCardModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchCardModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

