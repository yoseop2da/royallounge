//
//  LiveMatchListResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/20.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct LiveMatchListResponse: Codable {
    let resultCD, resultMsg: String
    let result: LiveMatchListResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct LiveMatchListResult: Codable {
    var sendNewCardYn, receiveNewCardYn: Bool
    var type: String
    var hasNext: Bool
    var list: [LiveMatchListCardModel]
}

struct LiveMatchListCardModel: Codable {
    let liveMatchHistNo: String
    var matchNo: String?
    let userNo, nickname: String
    let specCount: Int
    let title: String
    let dDay: Int
    let photoEvalYn: Bool
    let photoURL: String
    

    enum CodingKeys: String, CodingKey {
        case liveMatchHistNo, matchNo, userNo, nickname, title, specCount, photoEvalYn, dDay
        case photoURL = "photoUrl"
    }
}

// MARK: Convenience initializers

extension LiveMatchListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchListResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension LiveMatchListResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchListResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension LiveMatchListCardModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchListCardModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
