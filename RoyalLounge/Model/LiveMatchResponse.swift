//
//  LiveMatchResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/18.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

//{
//    "resultCd": "0000",
//    "resultMsg": "성공",
//    "result": {
//        "stats": {
//            "evalCount": 100,
//            "text1": "29세, 163cm, 볼륨있는스타일",
//            "text2": "필라테스강사, 승무원"
//        },
//        "crown": 10,
//        "progressYn": false
//    }
//}

struct LiveMatchResponse: Codable {
    let resultCD, resultMsg: String
    let result: LiveMatchResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct LiveMatchResult: Codable {
    let stats: StatsModel?
    let crown: Int?
    var progressYn: Bool
}

struct StatsModel: Codable {
    let evalCount: Int
    let text1, text2: String
}

// MARK: Convenience initializers

extension LiveMatchResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension LiveMatchResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(LiveMatchResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension StatsModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(StatsModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
