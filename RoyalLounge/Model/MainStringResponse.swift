import Foundation

//{
//    "resultCd": "0000",
//    "resultMsg": "성공",
//    "result": "Bjx26qOG7CEl-uNo0etThg=="
//}

struct MainStringResponse: Codable {
    let result: String?
    let resultCD, resultMsg: String

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

// MARK: Convenience initializers

extension MainStringResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(MainStringResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
