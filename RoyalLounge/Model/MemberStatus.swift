//
//  MemberStatus.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/06/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import Apollo

struct MemberStatus {
    var status: StatusType
    var step: Int
    var gender: GenderType
    var userType: UserType?
    init?(member: GetJoinStepQuery.Data.Member?) {
        guard let _member = member else { return nil }
        self.status = _member.status ?? .join
        self.step = _member.joinStep
        self.gender = _member.gender ?? .m
        self.userType = _member.userType
    }
}

