
//
//  NoticeListResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/08.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct NoticeListResponse: Codable {
    let resultCD, resultMsg: String
    let result: NoticeListResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct NoticeListResult: Codable {
    var list: [NoticeModel]
    let hasNext: Bool
}

struct NoticeModel: Codable {
    let noticeNo, noticeType, title, content: String
    let imageURL: String?
    let regDate: String
    var isOpen: Bool = false

    enum CodingKeys: String, CodingKey {
        case noticeNo, noticeType, title, content
        case imageURL = "imageUrl"
        case regDate
    }
}

// MARK: Convenience initializers

extension NoticeListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(NoticeListResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension NoticeListResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(NoticeListResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension NoticeModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(NoticeModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
