
//
//  NoticeListResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/07.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct NotificationListResponse: Codable {
    let resultCD, resultMsg: String
    let result: NotificationListResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct NotificationListResult: Codable {
    var list: [NotificationModel]
    var hasNext: Bool
}

struct NotificationModel: Codable {
    let title: String
    let content: String?
    let noticeNo, pushType, regDate: String
    let senderUserNo, senderPhotoURL, matchNo: String?
    let viewYn: Bool

    enum CodingKeys: String, CodingKey {
        case noticeNo, senderUserNo
        case senderPhotoURL = "senderPhotoUrl"
        case title, content, pushType, matchNo, regDate, viewYn
    }
}

// MARK: Convenience initializers

extension NotificationListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(NotificationListResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension NotificationListResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(NotificationListResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension NotificationModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(NotificationModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
