
//
//  Photo001Response.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/03/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation
struct Photo001Response: Codable {
    let resultCD, resultMsg: String
    let result: [Photo001Result]

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct Photo001Result: Codable {
    let chgYn, detectYn: Bool
    let photoNo: String
    let photoStatus, photoStatusName: String
    let regDate, rejectMsg, rejectDate, chgDate: String?
    
    let photoSeq: Int
    let photoURL: String

    enum CodingKeys: String, CodingKey {
        case photoNo, photoSeq
        case photoStatus, photoStatusName
        case photoURL = "photoUrl"
        case detectYn, rejectMsg, rejectDate, chgYn, chgDate, regDate
    }
}

// MARK: Convenience initializers

extension Photo001Response {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Photo001Response.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Photo001Result {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Photo001Result.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

//{
//    "resultCd": "0000",
//    "resultMsg": "성공",
//    "result": [
//        {
//            "photoNo": "nrXDMYjh5WHkYGhCx7odJw==",
//            "photoSeq": 1,
//            "photoUrl": "https://profile.royallounge.co.kr/URL?d=800x800&Expires=1584510872&Signature=FQ820B8EBHO19bZL1uO0eJ15xHMlhrwzx6ntPqLYELzw~UYKBlfIHkzXxAMs6a4CslLfCEm-lP9LT4kG-0yLZrm78RowMW9cUZc3l6UxoZwVxTRZe~FcvCKkEDu6AUtaAu-EqjmHF1kCkRW2cjapdrGZbdAGSR~EQJqQ8bTVBOhP2o9zOBrIZfnTsJIgjeGlzIDbKuffc9ka1w5Ws82q1xRvcqkVDlTWgiwhb8f7tVFf7v8n8uGSL6bhjUpgwPeeZYztmsm6lFtmkYj18DKhXHW6zIChSbRB1Qg7B5UKMEfIzhX1E3RkltZL37D6mtlnNOAe3Wp07I3MCssl0CDTQg__&Key-Pair-Id=APKAIIHT7FVRWGJ3G34A",
//            "detectYn": true,
//            "rejectYn": false,
//            "rejectMsg": null,
//            "rejectDate": null,
//            "chgYn": false,
//            "chgDate": null,
//            "regDate": "2020-03-17T08:37:04.000+0000"
//        }
//    ]
//}
