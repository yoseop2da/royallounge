//
//  RedDotResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct RedDotResponse: Codable {
    let resultCD, resultMsg: String
    let result: RedDotResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct RedDotResult: Codable {
    let noticeCenterYn: Bool // 알림센터
    let noticeBBSYn: Bool // 공지사항
    let eventBBSYn: Bool // 이벤트
    let appVersionYn: Bool // 앱 버전
    let photoRejectYn: Bool // 사진리젝
    let specRejectYn: Bool // 스펙리젝
    
    let cardBoxYn: Bool // 탭2
    let liveMatchYn: Bool // 탭3
    
    enum CodingKeys: String, CodingKey {
        case noticeCenterYn
        case noticeBBSYn = "noticeBbsYn"
        case eventBBSYn = "eventBbsYn"
        case appVersionYn, photoRejectYn, specRejectYn, cardBoxYn, liveMatchYn
        
    }
    
    var isProfileAllFalse: Bool {
        !noticeCenterYn && !noticeBBSYn && !eventBBSYn && !appVersionYn && !photoRejectYn && !specRejectYn
    }
}

// MARK: Convenience initializers

extension RedDotResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(RedDotResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension RedDotResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(RedDotResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
