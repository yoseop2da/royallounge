//
//  StoreHistoryListResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

// Generated with quicktype
// For more options, try https://app.quicktype.io

import Foundation

struct StoreHistoryListResponse: Codable {
    let resultCD, resultMsg: String
    let result: StoreHistoryListResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct StoreHistoryListResult: Codable {
    var list: [StoreHistoryModel]
    var hasNext: Bool
}

struct StoreHistoryModel: Codable {
    let crownNo, crownType, name: String
    let useType: String
    let signType: String
    let crown: Int
    let regDate: String
}

// MARK: Convenience initializers

extension StoreHistoryListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(StoreHistoryListResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension StoreHistoryListResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(StoreHistoryListResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension StoreHistoryModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(StoreHistoryModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
