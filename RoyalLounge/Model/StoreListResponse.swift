//
//  StoreListResponse.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/08/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

struct StoreListResponse: Codable {
    let resultCD, resultMsg: String
    let result: StoreListResult?

    enum CodingKeys: String, CodingKey {
        case resultCD = "resultCd"
        case resultMsg, result
    }
}

struct StoreListResult: Codable {
    let itemList: [InappItemModel]
    let crownList: [FreeCrownModel]?
}

struct FreeCrownModel: Codable {
    let crownType: String
    let crown: Int
    let title: String
    let description: String?
    
    var freeCrownType: FreeCrownType {
        return FreeCrownType.type(from: crownType)
    }
}

enum FreeCrownType: String {
    case joinAddProfile = "JOIN_ADD_PROFILE"// : 친구 초대하기
    case recoDo = "RECO_DO"// : 추가 프로필 등록
    case joinUniv = "JOIN_UNIV"// : 학교 인증
    case joinCo = "JOIN_CO"// : 직장 인증
    case joinPath = "JOIN_PATH"// : 가입경로 선택
    case liveMatchEval = "LIVE_MATCH_EVAL" //: 라이브매칭 평가
    case cardEval = "CARD_EVAL"// : 카드 별점 평가"
    
    static func type(from: String) -> FreeCrownType {
        switch from {
        case FreeCrownType.joinAddProfile.rawValue: return .joinAddProfile
        case FreeCrownType.recoDo.rawValue: return .recoDo
        case FreeCrownType.joinUniv.rawValue: return .joinUniv
        case FreeCrownType.joinCo.rawValue: return .joinCo
        case FreeCrownType.joinPath.rawValue: return .joinPath
        case FreeCrownType.liveMatchEval.rawValue: return .liveMatchEval
        case FreeCrownType.cardEval.rawValue: return .cardEval
        default:
            return .cardEval
        }
    }
}

struct InappItemModel: Codable {
    let itemNo, productID: String
    let crown: Int
    let title, description: String?
    let price: Int
    let discount: String?

    enum CodingKeys: String, CodingKey {
        case itemNo
        case productID = "productId"
        case crown, title, description, price, discount
    }
}

// MARK: Convenience initializers

extension StoreListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(StoreListResponse.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension StoreListResult {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(StoreListResult.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension FreeCrownModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(FreeCrownModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension InappItemModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(InappItemModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
