//
//  TokenModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/01/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

//{
//    "access_token": "09f2772a-eff9-4e62-b7c2-6fa84c76fe9e",
//    "token_type": "bearer",
//    "refresh_token": "8c018f8b-c25e-48ff-8814-ee864028681c",
//    "expires_in": 299,
//    "scope": "read write"
//}

struct TokenModel: Codable {
    let accessToken, tokenType, refreshToken: String
    let expiresIn: Int
    let scope: String
    var expiredDate: Date?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case refreshToken = "refresh_token"
        case expiresIn = "expires_in"
        case scope
        case expiredDate
    }
    
    var isExpired: Bool {
        if let expiredDate = self.expiredDate {
            return expiredDate <= Date()
        }
        return false
    }
    
    func saveUserDefaults() {
        // 앱내에 저장하기
        var new = self
        new.expiredDate = Date().addingTimeInterval(TimeInterval(expiresIn))
        RLUserDefault.shared.token = new
        print("====================== New Token : \(new)")
    }
}

// MARK: Convenience initializers

extension TokenModel {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(TokenModel.self, from: data) else { return nil }
        self = me
    }

    init?(_ json: String, using encoding: String.Encoding = .utf8) {
        guard let data = json.data(using: encoding) else { return nil }
        self.init(data: data)
    }

    init?(fromURL url: String) {
        guard let url = URL(string: url) else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        self.init(data: data)
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

    var json: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    
}
