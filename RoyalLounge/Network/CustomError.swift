//
//  CustomError.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/10.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

protocol ErrorProtocol: LocalizedError {

    var title: String? { get }
    var code: Int { get }
}

struct CustomError: ErrorProtocol {
    var title: String?
    var code: Int
    var resultCD: String
    var errorMessage: String { return _description }

    private var _description: String

    init(title: String = "Error", description: String, code: Int = 9999, resultCD: String = "GRAPHQL") {
        self.title = title
        self._description = description
        self.code = code
        self.resultCD = resultCD
    }
}
