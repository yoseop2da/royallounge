//
//  GraphQlNetwork.swift
//  GraphQl
//
//  Created by yoseop park on 2020/01/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation
import Apollo
import RxSwift
import RxCocoa

// MARK: - Singleton Wrapper

class GraphQlNetwork {
    static let shared = GraphQlNetwork()
    lazy var bag = DisposeBag()
    
    private(set) lazy var apollo: ApolloClient = {
        // The cache is necessary to set up the store, which we're going to hand to the provider
        let url = URL(string: "\(RLUserDefault.shared.baseUrlString)graphql")!
        
        let cache = InMemoryNormalizedCache()
        let store = ApolloStore(cache: cache)
        let client = URLSessionClient()
        let provider = NetworkInterceptorProvider.init(client: client, store: store)
                        
        let requestChainTransport = RequestChainNetworkTransport(interceptorProvider: provider, endpointURL: url)
        return ApolloClient(networkTransport: requestChainTransport, store: store)
    }()
    
    init() {
//        networkTransport.delegate = self
    }

    func fetch<Query: GraphQLQuery>(query: Query, errorHandle: Bool = true, displayLog: Bool = true) -> Observable<Query.Data?> {
        print("⚪⚪⚪⚪⚪⚪⚪⚪ GraphQl Request [query][\(query.operationName)]")
        return Observable.create { observer in
            self.apollo.fetch(query: query, cachePolicy: .fetchIgnoringCacheCompletely) { result in
                switch result {
                case .success(let graphQLResult):
                    if let errors = graphQLResult.errors?.first {
                        let message = errors.message ?? ""
                        guard let extensions = errors.extensions,
                           let errorMsg = extensions["errorMsg"] as? String,
                           let errorCode = extensions["errorCode"] as? String else {
                            print("😱😱😱😱😱😱😱😱 GraphQl Error\nmessage : \(message)")
                            observer.on(.error(CustomError.init(description: message)))
                            observer.on(.completed)
                            alertERR_E0000Dialog()
                            return
                        }
                        print("😱😱😱😱😱😱😱😱 GraphQl Error\nmessage : \(message)\nerrorCode : \(errorCode)\nerrorMsg : \(errorMsg)")
                        
                        if errorCode == RLResultCD.E00000_Error.code {
                            observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: errorCode)))
                        }else{
                            observer.on(.error(CustomError.init(description: errorMsg)))
                        }
                        observer.on(.completed)
                        alertERR_E0000Dialog()
                        return
                    }
                    
                    if let data = graphQLResult.data {
                        if displayLog {
                            print("🔵🔵🔵🔵🔵🔵🔵🔵 GraphQl Response [\(query.operationName)]\n\(data.jsonObject)\n")
                        }
                        observer.on(.next(data))
                    }
                case .failure(let error):
                    // Network or response format errors
                    print("😱😱😱😱😱😱😱😱 GraphQl Fetch Error [\(query.operationName)]: \(error)")
                    if errorHandle {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                    }
                    observer.on(.completed)
                    
                    alertNETWORK_ERR()
                }
            }
            return Disposables.create()
        }
    }
    
    func perform<Mutation: GraphQLMutation>(mutation: Mutation, errorHandle: Bool = true, displayLog: Bool = true) -> Observable<Mutation.Data?>{
        print("⚪⚪⚪⚪⚪⚪⚪⚪ GraphQl Request [mutation][\(mutation.operationName)]")
        return Observable.create { observer in
            self.apollo.perform(mutation: mutation) { result in
                switch result {
                case .success(let graphQLResult):
                    if let errors = graphQLResult.errors?.first {
                        let message = errors.message ?? ""
                        guard let extensions = errors.extensions,
                           let errorMsg = extensions["errorMsg"] as? String,
                           let errorCode = extensions["errorCode"] as? String else {
                            print("😱😱😱😱😱😱😱😱 GraphQl Error [mutation][\(mutation.operationName)]\nmessage: \(message)")
                            observer.on(.error(CustomError.init(description: message)))
                            observer.on(.completed)
                            alertERR_E0000Dialog()
                            return
                        }
                        print("😱😱😱😱😱😱😱😱 GraphQl Error [mutation][\(mutation.operationName)]\nerrorMsg : \(message)\nerrorCode : \(errorCode)\nerrorMsg : \(errorMsg)")
                        
                        if errorCode == RLResultCD.E00000_Error.code {
                            observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: errorCode)))
                        }else{
                            observer.on(.error(CustomError.init(description: errorMsg)))
                        }
                        observer.on(.completed)
                        alertERR_E0000Dialog()
                        return
                    }
                    
                    if let data = graphQLResult.data {
                        if displayLog {
                            print("🔵🔵🔵🔵🔵🔵🔵🔵 GraphQl Response [\(mutation.operationName)]\n\(data.jsonObject)\n")
                        }
                        observer.on(.next(data))
                    }
                    
                case .failure(let error):
                    // Network or response format errors
                    print("😱😱😱😱😱😱😱😱 GraphQl Fetch Error [\(mutation.operationName)]: \(error)")
                    if errorHandle {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                    }
                    observer.on(.completed)
                    alertNETWORK_ERR()
                }
            }
            
            return Disposables.create()
        }
    }
    
    func performNoResponse<Mutation: GraphQLMutation>(mutation: Mutation) {
        print("⚪⚪⚪⚪⚪⚪⚪⚪ GraphQl Request [mutation][\(mutation.operationName)]")
        self.apollo.perform(mutation: mutation) { _ in
//            print("Response 처리할 필요 없는 호출!!")
        }
    }
}


//https://www.apollographql.com/docs/ios/initialization/#setting-up-apollointerceptor-chains-with-interceptorprovider
class NetworkInterceptorProvider: LegacyInterceptorProvider {
    override func interceptors<Operation: GraphQLOperation>(for operation: Operation) -> [ApolloInterceptor] {
        var interceptors = super.interceptors(for: operation)
        interceptors.insert(UserManagementInterceptor(), at: 0)
        return interceptors
    }
}

class UserManagementInterceptor: ApolloInterceptor {
    let bag = DisposeBag()
    
    func interceptAsync<Operation: GraphQLOperation>(
        chain: RequestChain,
        request: HTTPRequest<Operation>,
        response: HTTPResponse<Operation>?,
        completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        
        if let receivedResponse = response {
            if receivedResponse.httpResponse.statusCode == 401 {
                self.retryToken(to: request, chain: chain, response: response, completion: completion)
                return
            }
        }
        
        if let token = RLUserDefault.shared.token {
            if token.isExpired {
                self.retryToken(to: request, chain: chain, response: response, completion: completion)
            }else{
                self.addTokenAndProceed(token.accessToken, to: request, chain: chain, response: response, completion: completion)
            }
        }
    }
    
    private func retryToken<Operation: GraphQLOperation>(
        to request: HTTPRequest<Operation>,
        chain: RequestChain,
        response: HTTPResponse<Operation>?,
        completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        Repository.shared.main
            .refreshToken()
            .retry(3)
            .asObservable()
            .subscribe(onNext: { newToken in
                newToken.saveUserDefaults()
                self.addTokenAndProceed(newToken.accessToken,
                                        to: request,
                                        chain: chain,
                                        response: response,
                                        completion: completion)
            }, onError: { error in
                print("===============================\(error)")
                RLUserDefault.shared.token = nil
                chain.handleErrorAsync(error,
                                       request: request,
                                       response: response,
                                       completion: completion)
            })
            .disposed(by: bag)
    }
    
    /// Helper function to add the token then move on to the next step
    private func addTokenAndProceed<Operation: GraphQLOperation>(
        _ accessToken: String,
        to request: HTTPRequest<Operation>,
        chain: RequestChain,
        response: HTTPResponse<Operation>?,
        completion: @escaping (Result<GraphQLResult<Operation.Data>, Error>) -> Void) {
        request.addHeader(name: "Authorization", value: "Bearer \(accessToken)")
        chain.proceedAsync(request: request,
                           response: response,
                           completion: completion)
    }
}
