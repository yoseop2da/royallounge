//
//  MainService.swift
//  RoyalRounge
//
//  Created by yoseop park on 2020/01/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

import Moya
/*
 getSomething - 조회
 addSomething - 추가
 updateSomething - 수정
 removeSomething - 제거
 */
enum MainService {
    case baseURlSelect
    
    case auth_001_requestToken(params: [String: Any])
    case auth_002_refreshToken(params: [String: Any])
    case msg_001_getMessage
    
    case appOption(appOptionCode: String)
    case app_002(params: [String: Any])
    
    case cert_001(params: [String: Any])
    case account_001(params: [String: Any])
    case account_002(params: [String: Any])
    case account_003(params: [String: Any])
    case account_004(params: [String: Any])
    
    case join_001(params: [String: Any])
    case join_002(params: [String: Any])
    case join_003(params: [String: Any], imageDatas: [Data])
    case join_004(params: [String: Any])
    
    case photo_001(params: [String: String], imageData: Data)
    case photo_002(params: [String: String], imageData: Data)
    case photo_003(params: [String: Any])
    case photo_004(params: [String: Any])
    
    case reco_001(params: [String: Any])
    
    case univ_001(params: [String: Any], imageDatas: [Data])
    case company_001(params: [String: Any], imageDatas: [Data])
    
    case card_001(params: [String: Any])
    case card_002(params: [String: Any])
    case card_003(params: [String: Any])
    case card_004(params: [String: Any])
    case card_005(params: [String: Any])
    case card_006(params: [String: Any])
    case card_007(params: [String: Any])
    case card_008(params: [String: Any])
    case card_009(params: [String: Any])
    case card_010(params: [String: Any])
    case card_011(params: [String: Any])
    case card_012(params: [String: Any])
    case card_013(params: [String: Any])
    case card_014(params: [String: Any])
    case card_015(params: [String: Any])
    
    case liveMatch_001(params: [String: Any])
    case liveMatch_002(params: [String: Any])
    case liveMatch_003(params: [String: Any])
    case liveMatch_004(params: [String: Any])
    case liveMatch_005(params: [String: Any])
    case liveMatch_006(params: [String: Any])
    case liveMatch_007(params: [String: Any])
    case liveMatch_008(params: [String: Any])
    
    case store_001(params: [String: Any])
    case store_002(params: [String: Any])
    case store_003(params: [String: Any])
    
    case nc_001(params: [String: Any])
    
    case bbs_001(params: [String: Any])
    case bbs_002(params: [String: Any])
    case bbs_003(params: [String: Any])
    
//    case attend_001(params: [String: Any])
    
    case report_001
    case report_002(params: [String: Any])
    
    case dot_001(params: [String: Any])
    
    case sleep_001(params: [String: Any])
    case sleep_002(params: [String: Any])
    
    case pwd_001(params: [String: Any])
    case pwd_002(params: [String: Any])
    case spec_001(params: [String: Any])
    
    case profile_001(params: [String: Any])
    case profile_002(params: [String: Any])
    
    case out_001(params: [String: Any])
    case out_002(params: [String: Any])
    
    
    
    case testAes
}

extension MainService: TargetType {
    var baseURL: URL {
        switch self {
        case .baseURlSelect:
            return URL(string: "https://gx30iytv1e.execute-api.ap-northeast-2.amazonaws.com/v1/test/servers")!
        case .auth_001_requestToken, .auth_002_refreshToken:
            return URL(string: Repository.shared.main.authBaseUrl)!
        default:
            return URL(string: Repository.shared.main.baseUrl)!
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .baseURlSelect, .testAes, .appOption, .report_001 : return .get
        default       : return .post
        }
    }

    var path                        : String {
        switch self {
        case .baseURlSelect         : return ""
        case .auth_001_requestToken,
             .auth_002_refreshToken : return "oauth/token"

        case .msg_001_getMessage    : return "msg"
        case .appOption(let code)   : return "app/option/list/\(code)"
        case .app_002             : return "app/crown"
            
        case .cert_001              : return "cert/check"
        case .account_001              : return "account"
        case .account_002              : return "account/pwd/mod"
        case .account_003             : return "account/login"
        case .account_004             : return "account/logout"
            
        case .join_001              : return "join/add"
        case .join_002              : return "join/nickname/check"
        case .join_003              : return "join/spec/add"
        case .join_004              : return "join/spec/lookCertYn/add"
            
        case .photo_001             : return "photo/add"
        case .photo_002             :return "photo/mod"
        case .photo_003             :return "photo/del"
        case .photo_004             :return "photo/order/mod"
        
        case .reco_001              : return "reco/add"
        
        case .univ_001             : return "university/add"
        case .company_001             : return "company/add"
            
        case .card_001              : return "card/today"
        case .card_002              : return "card/open/add"
        case .card_003              : return "card/detail"
        case .card_004              : return "card/score/add"
        case .card_005              : return "card/ok/add"
        case .card_006              : return "card/superOk/add"
        case .card_007              : return "card/mbNo/confirm/add"
        case .card_008              : return "card/del/count"
        case .card_009              : return "card/del"
        case .card_010              : return "card/receive"
        case .card_011              : return "card/send"
        case .card_012              : return "card/paging"
        case .card_013              : return "card/addCard/list"
        case .card_014              : return "card/addCard/option"
        case .card_015              : return "card/addCard/add"
        
        case .liveMatch_001         : return "liveMatch/result"
        case .liveMatch_002         : return "liveMatch/add"
        case .liveMatch_003         : return "liveMatch/"
        case .liveMatch_004         : return "liveMatch/like/add"
        case .liveMatch_005         : return "liveMatch/revert"
        case .liveMatch_006         : return "liveMatch/detail"
        case .liveMatch_007         : return "liveMatch/paging"
        case .liveMatch_008         : return "liveMatch/revert/check"
            
        case .store_001         : return "store/item/list"
        case .store_002         : return "store/hist/paging"
        case .store_003         : return "store/purchase/result/add"
            
        case .nc_001             : return "noticeCenter/paging"
        case .bbs_001             : return "bbs/notice/paging"
        case .bbs_002             : return "bbs/event/paging"
        case .bbs_003             : return "bbs/event/detail"
            
//        case .attend_001             : return "attend/add"
            
            
            
            
        case .report_001            : return "report/group/list"
        case .report_002            : return "report/add"
            
        case .dot_001                 : return "redDot"
        case .sleep_001                 : return "sleep/add"
        case .sleep_002                 : return "sleep/count"
            
        case .pwd_001                 : return "password/mod"
        case .pwd_002                 : return "password/check"
        
        case .spec_001                 : return "spec/display/mod"
        
        case .profile_001               : return "profile/preview"
        case .profile_002               : return "profile"
            
        case .out_001               : return "out/add"
        case .out_002               : return "out/recovery/add"
            
            
             
            
            
            
            
            
            
            
        case .testAes               : return "test/aes/enc/aaa"
        }
    }

    var sampleData: Data {
        return "Half measures are as bad as nothing at all.".utf8Encoded
    }

    var task: Task {
        switch self {
        case .auth_001_requestToken(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody, urlParameters: [:])
        case .auth_002_refreshToken(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody, urlParameters: [:])
        case .msg_001_getMessage:
            return .requestParameters(parameters: [String: Any](), encoding: JSONEncoding.default)
        case .cert_001(let params),
             .account_001(let params), .account_002(let params), .account_003(let params), .account_004(let params),
             .join_001(let params), .join_002(let params), .join_004(let params),
             .photo_003(let params), .photo_004(let params),
             .reco_001(let params),
             
             .card_001(let params), .card_002(let params), .card_003(let params), .card_004(let params),
             .card_005(let params), .card_006(let params), .card_007(let params), .card_008(let params),
             .card_009(let params), .card_010(let params), .card_011(let params), .card_012(let params),
             .card_013(let params), .card_014(let params), .card_015(let params),
            
             .liveMatch_001(let params), .liveMatch_002(let params), .liveMatch_003(let params), .liveMatch_004(let params),
             .liveMatch_005(let params), .liveMatch_006(let params), .liveMatch_007(let params), .liveMatch_008(let params),
             
             .store_001(let params), .store_002(let params), .store_003(let params),
             
             .nc_001(let params), .bbs_001(let params), .bbs_002(let params), .bbs_003(let params),
             
//             .attend_001(let params),
                
        .app_002(let params), .report_002(let params), .dot_001(let params), .sleep_001(let params), .sleep_002(let params), .pwd_001(let params)
        , .pwd_002(let params), .spec_001(let params), .profile_001(let params), .profile_002(let params), .out_001(let params), .out_002(let params)
        :
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .testAes, .baseURlSelect, .appOption, .report_001:
            return .requestPlain
        case .join_003(let params, let imageDatas):
            //            userNo    String
            //            specNo    Int
            //            stepType    String
            //            photoList    List<MultipartFile>

            let formData: [MultipartFormData] = imageDatas.map{
                MultipartFormData(provider: .data($0), name: "photoList", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            // 멀티로 이미지 전송시 참조.
            return .uploadCompositeMultipart(formData, urlParameters: params)
        case .univ_001(let params, let imageDatas):
            let formData: [MultipartFormData] = imageDatas.map{
                MultipartFormData(provider: .data($0), name: "photoList", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            // 멀티로 이미지 전송시 참조.
            return .uploadCompositeMultipart(formData, urlParameters: params)
        case .company_001(let params, let imageDatas):
            let formData: [MultipartFormData] = imageDatas.map{
                MultipartFormData(provider: .data($0), name: "photoList", fileName: "photo.jpg", mimeType: "image/jpeg")
            }
            // 멀티로 이미지 전송시 참조.
            return .uploadCompositeMultipart(formData, urlParameters: params)
            
        case .photo_001(let params, let imageData):
            let multipartData = MultipartFormData(provider: .data(imageData), name: "photo", fileName: "picture.jpg", mimeType: "image/jpg")
            let userNoData = params["userNo"]!.data(using: .utf8)!
            let userNo = MultipartFormData(provider: .data(userNoData), name: "userNo")
            return .uploadMultipart([multipartData, userNo])
        case .photo_002(let params, let imageData):
            let multipartData = MultipartFormData(provider: .data(imageData), name: "photo", fileName: "picture.jpg", mimeType: "image/jpg")
            
            let userNoData = params["userNo"]!.data(using: .utf8)!
            let userNo = MultipartFormData(provider: .data(userNoData), name: "userNo")
            
            let photoNoData = params["photoNo"]!.data(using: .utf8)!
            let photoNo = MultipartFormData(provider: .data(photoNoData), name: "photoNo")
            return .uploadMultipart([multipartData, userNo, photoNo])
//        case .gallery_005_add(let params, let imageData):
//            let multipartData = MultipartFormData(provider: .data(imageData), name: "photo", fileName: "picture.jpg", mimeType: "image/jpg")
//
//            let userNoData = params[USER_NO_KEY]!.data(using: .utf8)!
//            let userNo = MultipartFormData(provider: .data(userNoData), name: USER_NO_KEY)
//
//            let contentKeyData = params[CONTENT_KEY]!.data(using: .utf8)!
//            let contentKey = MultipartFormData(provider: .data(contentKeyData), name: CONTENT_KEY)
//            return .uploadMultipart([multipartData, userNo, contentKey])
        }
    }

    var headers: [String: String]? {
        var headerField = [String: String]()
        switch self {
        case .auth_001_requestToken, .auth_002_refreshToken:
            let username = "royallounge"
            let password = "fhdiffkdnswl1230!"
            let credentialData = "\(username):\(password)".data(using: .utf8)!
            let base64Credentials = credentialData.base64EncodedString()
            headerField["Authorization"] = "Basic \(base64Credentials)"
//            printHeader(headerField)
            return headerField
        case .cert_001, .account_001, .account_002, .account_003, .out_002:
            headerField["Accept"] = "application/json"
            headerField["Content-type"] = "application/json"
            headerField["X-Device-Type"] = "I"
            headerField["X-App-Ver"] = ApplicationInfo.appVersion
//            printHeader(headerField)
            return headerField
        default:
            headerField["Accept"] = "application/json"
            headerField["Content-type"] = "application/json"
            headerField["X-Device-Type"] = "I"
            headerField["X-App-Ver"] = ApplicationInfo.appVersion
            
            if let accessToken = RLUserDefault.shared.token?.accessToken { headerField["Authorization"] = "Bearer \(accessToken)" }
//            printHeader(headerField)
            return headerField
        }
    }

    var validate: Bool {
         return true
    }

//    func printHeader(_ headerField: Any) {
//        Debug.networkHeaderPrint("""
//
//            ┏[ Request Header Field ]
//            ┃
//            ┃ \(headerField)
//            ┃
//            ┗🌼🌼🌼🌼🌼
//
//            """)
//    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
