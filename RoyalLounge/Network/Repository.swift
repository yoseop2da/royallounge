//
//  Repository.swift
//  RoyalRounge
//
//  Created by yoseop park on 2020/01/15.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

import Apollo
import Alamofire

import Moya

class Repository {
    static let shared = Repository()
    
    lazy var main = RepositoryMain()//url: "https://dev-api.royallounge.co.kr"
    lazy var graphQl = RepositoryGraphQl()
    lazy var bag = DisposeBag()
    
    /**
     GraphQlRepository
     - allMember
     */
    
    /**
     MainRepository
     - 
     */
}
