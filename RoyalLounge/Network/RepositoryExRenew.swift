//
//  RepositoryExRenew.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/03.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

//MARK: – Renew token
public extension PrimitiveSequence where TraitType == SingleTrait, Element == Response {
    
    func commonErrorMsgPopup() -> Single<Element> {
        return catchError{ error in
            if case MoyaError.statusCode(let response) = error {
                if response.statusCode < 400 && response.statusCode >= 500 {
//                    #if DEBUG
                    alertDialogOneButton(title: "디버그모드",message: error.localizedDescription, okAction:{ _ in
                        //                                return Single.error(error)
                    })
//                    #endif
                }
            }
            return Single.error(error)
        }
    }
    /**
     401 발생시 리프레시 토큰
     */
    func retryWithAuthIf401() -> Single<Element> {
        return catchError { error -> PrimitiveSequence<SingleTrait, Response> in
            if let moyaError: MoyaError = error as? MoyaError, let response: Response = moyaError.response {
                if response.statusCode == 401 {
                    guard let refreshToken = RLUserDefault.shared.token?.refreshToken else {
                        return Single.error(error)
                    }
                    return self.retryAuth(refreshToken)
                }
            }
            return Single.error(error)
        }.retryWithTimeOut()
    }
    
    func retryWithTimeOut() -> Single<Element> {
        return catchError { error -> PrimitiveSequence<SingleTrait, Response> in
            if let moyaError: MoyaError = error as? MoyaError, let response: Response = moyaError.response {
                // 우리쪽 서버 에러!!
                print("--------moyaError: \(moyaError)||\(response.description)----------")
                alertERR_E0000Dialog()
            }else{
                // TimeOut - 네트워크에러
                print("--------TimeOut----------")
                alertNETWORK_ERR()
            }
            return Single.error(error)
        }
    }

    private func retryAuth(_ refreshToken: String) -> Single<Element> {
        return retryWhen { errorObservable in
            Observable.zip(errorObservable, Observable.range(start: 1, count: 3), resultSelector: { $1 })
                .flatMap { i -> PrimitiveSequence<SingleTrait, TokenModel> in
                    return Repository.shared.main
                        .refreshToken()
                        .catchError { error in
                            if case MoyaError.statusCode(let response) = error {
                                if response.statusCode > 399 && response.statusCode < 500 {
                                    // 토큰 못가지고옴 > 로그아웃 > 다시 로그인
                                    self.failRefreshAuth()
                                    RLUserDefault.shared.token = nil
                                }
                            }
                            return Single.error(error)
                    }
                    .flatMap({ token -> PrimitiveSequence<SingleTrait, TokenModel> in
                        token.saveUserDefaults() // 토큰 갱신
                        return Single.just(token)
                    })
            }
        }
    }
    
    private func failRefreshAuth() {
        //        Utils.resetToLoginScreen()
        let ac = UIAlertController(title: "토큰이 만료되었습니다.\n다시 로그인 해주세요....", message: NSLocalizedString("auth.error", comment: ""), preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "다시 로그인하기", style: .default, handler: { _ in
            print("로그아웃 처리해서 유저가 다시 로그인하게 해주기")
        }))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            UIApplication.shared.keyWindow?.rootViewController?.present(ac, animated: true, completion: nil)
        }
    }
    
}
