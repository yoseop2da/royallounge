//
//  RepositoryGraphQl.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/01/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Apollo
import RxSwift
import RxCocoa
//import Alamofire
//import Moya

extension Repository {
    class RepositoryGraphQl {

        // 가입단계
        func getMemberStatus(userNo: String) -> Observable<MemberStatus?>  {
            let query = GetJoinStepQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{ MemberStatus.init(member: $0?.member) }
        }
        
        func updateNickname(userNo: String, nickname: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.nickname = nickname
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        // 휴대폰 번호 업데이트 
        func updateMbNo(userNo: String, mobileNo: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.mbNo = mobileNo // 암호화된것 넘기기
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        // 닉네임
        func getNickname(userNo: String) -> Observable<String>  {
            let query = GetNicknameQuery.init(userNo: userNo)
            return GraphQlNetwork.shared
                .fetch(query: query)
                .map{ $0?.member?.nickname ?? "" }
        }
        
        // 닉네임
        func getNicknameWithRejectMsg(userNo: String) -> Observable<(nickname: String, rejectMsg: String?)>  {
            let query = GetNicknameQuery.init(userNo: userNo)
            return GraphQlNetwork.shared
                .fetch(query: query)
                .map{ return (nickname: $0?.member?.nickname ?? "", rejectMsg: $0?.member?.nicknameRejectMsg) }
        }
        
        func updateAreaNo(userNo: String, areaNo: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.areaNo = areaNo
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        func getAreaNo(userNo: String) -> Observable<String>  {
            let query = GetAreaNoQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { return $0?.member?.areaNo ?? "" }
        }
        
        func getAreaList() -> Observable<[(name: String, areaList: [(name: String, areaNo: String)])]>  {
            let query = GetAreaListQuery.init()
            return GraphQlNetwork.shared.fetch(query: query)
                .map {
                    if let area = $0?.areaGroupList {
                        return area.compactMap{$0}
                            .compactMap { _area -> (name: String, areaList: [(name: String, areaNo: String)])? in
                                guard let name = _area.name, let areaList = _area.areaList else { return nil }
                                let items = areaList.compactMap{ subArea -> (name: String, areaNo: String)? in
                                    if let name = subArea?.name, let areaNo = subArea?.areaNo {
                                        return (name: name, areaNo: areaNo)
                                    }else{
                                        return nil
                                    }
                                }
                                return (name: name, areaList: items)
                        }
                    }
                    return []
            }
        }
        
        // 키
        func getTall(userNo: String) -> Observable<(tall: String, nickname: String)>  {
            let query = GetTallQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    let tall = $0?.member?.tall
                    return (tall: tall == nil ? "": "\(tall!)", nickname: $0?.member?.nickname ?? "")
            }
        }
        
        func updateTall(userNo: String, tall: Int) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.tall = tall
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        func updateMobileNo(userNo: String, mbNo: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.mbNo = mbNo
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        // 체형
        func getBody(userNo: String) -> Observable<(bodyCd: String, nickname: String)>  {
            let query = GetBodyCdQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map {
                    if let member = $0?.member, let nickname = member.nickname {
                        let bodyCd = member.bodyCd ?? ""
                        return (bodyCd: bodyCd, nickname: nickname)
                    }
                    return (bodyCd: "", nickname: "")
            }
        }
        
        func updateBody(userNo: String, bodyCd: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.bodyCd = bodyCd
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        //drink
        func getDrink(userNo: String) -> Observable<(drinkCd: String, nickname: String)>  {
            let query = GetDrinkCdQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map {
                    if let member = $0?.member, let nickname = member.nickname {
                        let drinkCd = member.drinkCd ?? ""
                        return (drinkCd: drinkCd, nickname: nickname)
                    }
                    return (drinkCd: "", nickname: "")
            }
        }
        
        func updateDrink(userNo: String, drinkCd: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.drinkCd = drinkCd
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        // religion
        func getReligion(userNo: String) -> Observable<(religionCd: String, nickname: String)>  {
            let query = GetReligionCdQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map {
                    if let member = $0?.member, let nickname = member.nickname {
                        let religionCd = member.religionCd ?? ""
                        return (religionCd: religionCd, nickname: nickname)
                    }
                    return (religionCd: "", nickname: "")
            }
        }
        
        func updateReligion(userNo: String, religionCd: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.religionCd = religionCd
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        func updateCharList(userNo: String, list: [String]) -> Observable<Bool> {
            var input = CharacterMutationModelInput.init()
            input.userNo = userNo
            input.list = list.map{ CharacterModelInput.init(charCd: $0) }
            let mutation = CharListMutation.init(charListInput: input)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMemberCharacterList { return success }
                    return false
            }
        }
        
        // 성격
        func getCharCdList(userNo: String) -> Observable<[String]>  {
            let query = GetCharListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map {
                    if let charList = $0?.memberCharacterList {
                        return charList.compactMap {$0?.charCd}
                    }
                    return []
            }
        }
        
        //education
        func getEducation(userNo: String) -> Observable<(eduCd: String, nickname: String)>  {
            let query = GetEduCdQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let member = $0?.member, let nickname = member.nickname {
                        let eduCd = member.eduCd ?? ""
                        return (eduCd: eduCd, nickname: nickname)
                    }

                    return (eduCd: "", nickname: "")
            }
        }
        
        func updateEducation(userNo: String, eduCd: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.eduCd = eduCd
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }

        func getJob(userNo: String) -> Observable<(job: String, rejectMsg: String?)>  {
            let query = GetJobQuery.init(userNo: userNo)
            return GraphQlNetwork.shared
                .fetch(query: query)
                .map{
                    if let member = $0?.member {
                        return (job: member.job ?? "", rejectMsg: member.jobRejectMsg)
                    }
                    return (job: "", rejectMsg: nil)
            }
        }
        
        func updateJob(userNo: String, job: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.job = job
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        func updateInterview(userNo: String, interview: String, questionNo: Int) -> Observable<Bool> {
            var input = InterviewMutationModelInput.init()
            input.userNo = userNo
            input.interview = interview
            input.questionNo = "\(questionNo)"
            let mutation = InterviewMutation.init(interviewInput: input)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMemberInterview { return success }
                    return false
            }
        }

        func getInterView(userNo: String) -> Observable<[GetInterviewQuery.Data.MemberInterviewList]>  {
            let query = GetInterviewQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let list = $0?.memberInterviewList {
                        return list.compactMap{$0}
                    }
                    return []
            }
        }
        
        // aboutMe
        func getAboutMe(userNo: String) -> Observable<(aboutMe: String, rejectMsg: String?)>  {
            let query = GetAboutMeQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let member = $0?.member {
                        return (aboutMe: member.aboutMe ?? "", rejectMsg: member.aboutMeRejectMsg)
                    }
                    return (aboutMe: "", rejectMsg: nil)
            }
        }
        
        func updateAboutMe(userNo: String, aboutMe: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.aboutMe = aboutMe
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }

        func updateHobbyList(userNo: String, list: [String]) -> Observable<Bool> {
            var input = HobbyMutationModelInput.init()
            input.userNo = userNo
            input.list = list.map { HobbyModelInput.init(hobby: $0) }
            let mutation = HobbyListMutation.init(hobbyListInput: input)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMemberHobbyList { return success }
                    return false
            }
        }
        
        func getHobby(userNo: String) -> Observable<[String]>  {
            let query = GetHobbyListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let list = $0?.memberHobbyList {
                        return list.compactMap{$0?.hobby}
                    }
                    return []
            }
        }
        
        func updateInterestList(userNo: String, list: [String]) -> Observable<Bool> {
            var input = InterestMutationModelInput.init()
            input.userNo = userNo
            input.list = list.map { InterestModelInput.init(interest: $0) }
            let mutation = InterestListMutation.init(interestListInput: input)
            
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMemberInterestList { return success }
                    return false
            }
        }
        
        func getInterest(userNo: String) -> Observable<[String]>  {
            let query = GetInterestListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let list = $0?.memberInterestList {
                        return list.compactMap{$0?.interest}
                    }
                    return []
            }
        }
        
        func updateHaveList(userNo: String, list: [String]) -> Observable<Bool> {
            var input = HaveMutationModelInput.init()
            input.userNo = userNo
            input.list = list.map { HaveModelInput.init(have: $0) }
            let mutation = HaveListMutation.init(haveListInput: input)
            
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMemberHaveList { return success }
                    return false
            }
        }
        
        func getHave(userNo: String) -> Observable<[String]>  {
            let query = GetHaveListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let list = $0?.memberHaveList {
                        return list.compactMap{$0?.have}
                    }
                    return []
            }
        }
        
        func updateWantList(userNo: String, list: [String]) -> Observable<Bool> {
            var input = WantMutationModelInput.init()
            input.userNo = userNo
            input.list = list.map { WantModelInput.init(want: $0) }
            let mutation = WantListMutation.init(wantListInput: input)
            
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { $0?.saveMemberWantList ?? false }
        }
        
        func getWant(userNo: String) -> Observable<[String]>  {
            let query = GetWantListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let list = $0?.memberWantList {
                        return list.compactMap{$0?.want}
                    }
                    return []
            }
        }

        func updateJobCareer(userNo: String, jobCareer: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.jobCareer = jobCareer
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { $0?.saveMember ?? false }
        }
        
        func getJobCareer(userNo: String) -> Observable<String> {
            let query = GetJobCareerQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{ $0?.member?.jobCareer ?? "" }
        }
        
        func getPhotoList(userNo: String) -> Observable<[GetPhotoListQuery.Data.MemberPhotoList]>  {
            let query = GetPhotoListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let photo = $0?.memberPhotoList {
                        return photo.compactMap{$0}
                    }
                    return []
            }
        }
        
        func completePhoto(userNo: String) -> Observable<Bool> {
            let mutation = CompletePhotoMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMemberPhotoEnd { return success }
                    return false
            }
        }
        
        // 가입경로
        func updateJoinpath(userNo: String, joinPath: String) -> Observable<Bool> {
            var member = MemberMutationModelInput.init()
            member.userNo = userNo
            member.joinPathCd = joinPath
            let mutation = MemberInfoMutation.init(memberInput: member)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let success = $0?.saveMember { return success }
                    return false
            }
        }
        
        
//        func getJoinPathCd(userNo: String) -> Observable<String>  {
//            let query = GetJoinPathQuery.init(userNo: userNo)
//            return GraphQlNetwork.shared.fetch(query: query)
//                .map{ $0?.member?.joinPathCd ?? "" }
//        }
        
        // 지인피하기
        func updateAvoidList(userNo: String, avoidList: [AvoidModel]) -> Observable<[(avoidNo: String, name: String, mbNo: String)]> {
            var input = MemberAvoidMutationModelInput.init()
            input.userNo = userNo
            input.list = avoidList.map {
                var model = MemberAvoidModelInput.init()
                model.name = $0.name.aesEncrypted!
                model.mbNo = $0.mbNoFormatted.aesEncrypted!
                model.avoidNo = $0.avoidNo
                return model
            }
            let mutation = AvoidListMutation.init(avoidListInput: input)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let list = $0?.saveMemberAvoidList {
                        return list.compactMap{(avoidNo: $0?.avoidNo ?? "", name: $0?.name?.aesDecrypted! ?? "", mbNo: $0?.mbNo?.aesDecrypted! ?? "")}
                    }
                    return []
            }
        }
        
        func removeAvoidList(userNo: String, avoidNoList: [String]) -> Observable<[(avoidNo: String, name: String, mbNo: String)]> {
            var memberAvoid = MemberAvoidMutationModelInput.init()
            memberAvoid.userNo = userNo
            memberAvoid.list = avoidNoList.map{ MemberAvoidModelInput.init(avoidNo: $0) }
            let mutation = DelMemberAvoidMutation.init(memberAvoidInput: memberAvoid)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    if let list = $0?.deleteMemberAvoidList {
                        return list.compactMap{(avoidNo: $0?.avoidNo ?? "", name: $0?.name?.aesDecrypted! ?? "", mbNo: $0?.mbNo?.aesDecrypted! ?? "")}
                    }
                    return []
            }
        }
        
        func getAvoidList(userNo: String) -> Observable<[(avoidNo: String, name: String, mbNo: String)]>  {
            let query = GetMemberAvoidListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let list = $0?.memberAvoidList {
                        return list.compactMap{(avoidNo: $0?.avoidNo ?? "", name: $0?.name?.aesDecrypted! ?? "", mbNo: $0?.mbNo?.aesDecrypted! ?? "")}
                    }
                    return []
            }
        }
        
//        func getRecommendCd(userNo: String) -> Observable<String>  {
//            let query = GetRecoCdQuery.init(userNo: userNo)
//            return GraphQlNetwork.shared.fetch(query: query)
//                .map{ $0?.member?.recoCd ?? "" }
//        }
        
        func updateDevice(userNo: String, pushId: String) {
            var input = DeviceMutationModelInput.init()
            input.appVersion = ApplicationInfo.appVersion
            input.pushId = pushId.aesEncrypted
            input.uuid = ApplicationInfo.adIdentifier.aesEncrypted
            input.osVersion = ApplicationInfo.systemVersion //"1.2.3"
            input.userNo = userNo
            input.modelName = ApplicationInfo.deviceModel  // "iphone7"
            input.deviceType = "I"
            
            let mutation = AddDeviceMutation.init(deviceInput: input)
            GraphQlNetwork.shared.performNoResponse(mutation: mutation)
        }
        
        func getSpecGroup(userNo: String) -> Observable<[GetSpecGroupQuery.Data.MemberSpecGroupList]>  {
            let query = GetSpecGroupQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    $0?.memberSpecGroupList?.compactMap{$0} ?? []
            }
        }
        
        func getSpecOption(specNo: String) -> Observable<(title: String, description: String)>  {
            let query = GetSpecOptionQuery.init(specNo: specNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{ (title: $0?.specOption?.title ?? "", description: $0?.specOption?.description ?? "") }
        }
        
        func getSpecPhoto(specNo: String, userNo: String, stepSeq: Int) -> Observable<[(photoNo: String, photoSeq: Int, photoUrl: String, stepSeq: Int, rejectMsg: String, isReject: Bool)]>  {
            let query = GetSpecPhotoQuery.init(specNo: specNo, userNo: userNo, stepSeq: stepSeq)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let specPhoto = $0?.memberSpecPhotoList {
                        return specPhoto.compactMap { photo -> (photoNo: String, photoSeq: Int, photoUrl: String, stepSeq: Int, rejectMsg: String, isReject: Bool)? in
                            guard let photoNo = photo?.photoNo,
                                let photoSeq = photo?.photoSeq,
                                let photoUrl = photo?.photoUrl,
                                let stepSeq = photo?.stepSeq else { return nil }
                            return (photoNo: photoNo, photoSeq: photoSeq, photoUrl: photoUrl, stepSeq: stepSeq, rejectMsg: photo?.rejectMsg ?? "", isReject: photo?.rejectMsg != nil)
                        }
                    }
                    return []
            }
        }

        func removeSpec(userNo: String, specNo: String) -> Observable<Bool> {
            let mutation = DelSpecMutation.init(userNo: userNo, specNo: specNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { $0?.deleteMemberSpec ?? false }
        }

        func completeSpec(userNo: String) -> Observable<Bool> {
            let mutation = CompleteSpecMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { $0?.saveMemberSpecEnd ?? false }
        }
        
        func completeSpecReject(userNo: String) -> Observable<Bool> {
            let mutation = CompleteSpecRejectMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map{ $0?.saveMemberSpecRejectEnd ?? false
            }
        }
        
        func completeMemberPhotoReject(userNo: String) -> Observable<Bool> {
            let mutation = CompleteMemberPhotoRejectMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map{ $0?.saveMemberPhotoRejectEnd ?? false }
        }
        
        func cancelSpec(userNo: String) -> Observable<Bool> {
            let mutation = CancelSpecMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map{ $0?.saveMemberSpecCancel ?? false }
        }
        
        func getProhibitWordList() -> Observable<[String]>  {
            let query = GetProhibitWordQuery.init()
            return GraphQlNetwork.shared.fetch(query: query, displayLog: false)
                .map{
                    if let prohibitWords = $0?.prohibitWordList {
                        return prohibitWords.compactMap{$0.map{$0.word!}}
                    }
                    return [] }
        }
        
        func getRejectList(userNo: String) -> Observable<[RejectData]> {
            let query = GetRejectGroupListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    if let rejectList = $0?.memberRejectGroupList {
                        return rejectList.compactMap{ RejectData.init(data: $0) }
                    }
                    return []
            }
        }
        
        func getReadyCrown(userNo: String) -> Observable<GetReadyCrownQuery.Data.Ready?> {
            let query = GetReadyCrownQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { return $0?.ready }
        }
        
        func getMemberUnivAndCo(userNo: String) -> Observable<GetMemberUnivAndCoQuery.Data.MemberUniversityAndCompany?> {
            let query = GetMemberUnivAndCoQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { return $0?.memberUniversityAndCompany }
        }
        
        func searchUniv(text: String) -> Observable<[GetUnivListQuery.Data.UniversityList?]>  {
            let query = GetUnivListQuery.init(keyword: text)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { $0?.universityList ?? [] }
        }
        
        func searchCompany(text: String) -> Observable<[GetCompanyListQuery.Data.CompanyList?]>  {
            let query = GetCompanyListQuery.init(keyword: text)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { return $0?.companyList ?? [] }
        }
        
        /// 가입 경로, 추천인 코드 팝업 발생체크
        func getMainInfo(userNo: String) -> Observable<GetMainInfoQuery.Data.MainInfo?>  {
            let query = GetMainInfoQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { $0?.mainInfo }
        }
        
        func skipReco(userNo: String) -> Observable<Bool> {
            let mutation = SkipRecoMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { $0?.saveMemberRecoSkip ?? false }
        }
        
        func skipJoinPath(userNo: String) -> Observable<Bool> {
            let mutation = SkipJoinPathMutation.init(userNo: userNo)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { $0?.saveMemberJoinPathSkip ?? false }
        }
        
        func getEventNotiMessage() -> Observable<String>  {
            let query = GetEventNotiMessageQuery.init()
            return GraphQlNetwork.shared.fetch(query: query)
                .map { $0?.eventNotiMessage ?? "1만원" }
        }
        
        func addUnivKeyword(keyword: String) -> Observable<(keyword: String, no: String)> {
            let mutation = AddUnivKeywordMutation.init(keyword: keyword)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    (keyword: keyword, no: $0?.saveUniversityKeyword?.univNo ?? "")
            }
        }
        
        func addCompanyKeyword(keyword: String) -> Observable<(keyword: String, no: String)> {
            let mutation = AddCompanyKeywordMutation.init(keyword: keyword)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map { return (keyword: keyword, no: $0?.saveCompanyKeyword?.coNo ?? "") }
        }
        
        /// 내 크라운 조회
        func getMyCrown(userNo: String) -> Observable<Int> {
            let query = GetMyCrownQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { return $0?.member?.crown ?? 0 }
        }
        
        /// 이메일 보내기시 필요한 정보 조회
        func getEmailInfo(userNo: String) -> Observable<(userId: String, mbNo: String, nickname: String)?>  {
            let query = GetEmailInfoQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map {
                    if let userId = $0?.member?.userId, let mbNo = $0?.member?.mbNo, let nickname = $0?.member?.nickname {
                        return (userId: userId, mbNo: mbNo, nickname: nickname)
                    }
                    return nil
            }
        }
        
        func getMyPhotoUrl(userNo: String) -> Observable<String?> {
            let query = GetMyPhotoUrlQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map { $0?.member?.photoUrl }
        }

        /// 설정
        func getSettingsInfo(userNo: String) -> Observable<(userId: String, mbNo: String, nickname: String)>  {
            let query = GetSettingsInfoQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    (userId: $0?.member?.userId?.aesDecrypted ?? ""
                        ,mbNo: $0?.member?.mbNo?.aesDecrypted ?? ""
                        ,nickname: $0?.member?.nickname ?? "")
            }
        }
        
        func getSettingsLatestAppVersion() -> Observable<String>  {
            let query = GetLatestAppVersionQuery.init(deviceType: "I")
            return GraphQlNetwork.shared.fetch(query: query)
                .map{ $0?.latestAppVersion ?? "1.0.0" }
        }
        
        /// 내 추천인코드 관련 정보
        func getRecommendInfo(userNo: String) -> Observable<(recoCd: String, doRecoCd: String?, receiveCount: Int)>  {
            let query = GetMemberRecoCdQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    (recoCd: $0?.memberRecoCd?.recoCd ?? "",
                     doRecoCd: $0?.memberRecoCd?.doRecoCd,
                     receiveCount: $0?.memberRecoCd?.receiveCount ?? 0)
            }
        }
        
        func getMyPageInfo(userNo: String) -> Observable<(photoUrl: String, nickname: String)>  {
            let query = GetMyPageInfoQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{
                    (photoUrl: $0?.member?.photoUrl ?? "",
                     nickname: $0?.member?.nickname ?? "")
            }
        }
        
        func getMemberPushSetList(userNo: String) -> Observable<[(setNo: String, title: String, sendYn: Bool)]>  {
            let query = GetMemberPushSetListQuery.init(userNo: userNo)
            return GraphQlNetwork.shared.fetch(query: query)
                .map{ $0?.memberPushSetList?.compactMap{
                    guard let setNo = $0?.setNo else { return nil }
                    guard let title = $0?.title else { return nil }
                    guard let sendYn = $0?.sendYn else { return nil }
                    return (setNo: setNo, title: title, sendYn: sendYn)
                    } ?? [] }
        }
        
        func setMemberPush(userNo: String, setNo: String, sendYn: Bool) -> Observable<[(setNo: String, title: String, sendYn: Bool)]> {
            var input = PushSetMutationModelInput.init()
            input.userNo = userNo
            input.setNo = setNo
            input.sendYn = sendYn
            let mutation = SaveMemberPushSetMutation.init(input: input)
            return GraphQlNetwork.shared.perform(mutation: mutation)
                .map {
                    $0?.saveMemberPushSet?.compactMap{
                        guard let setNo = $0?.setNo else { return nil }
                        guard let title = $0?.title else { return nil }
                        guard let sendYn = $0?.sendYn else { return nil }
                        return (setNo: setNo, title: title, sendYn: sendYn)
                        } ?? [] }
        }
        
        
        
        
        
    }
}
