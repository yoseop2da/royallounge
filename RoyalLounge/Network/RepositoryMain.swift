//
//  RepositoryMain.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/01/28.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

import Alamofire
import Moya

extension Repository {
    class RepositoryMain {
        
        var baseUrl: String { RLUserDefault.shared.baseUrlString }
        var authBaseUrl: String { RLUserDefault.shared.authBaseUrlString }
        lazy var bag = DisposeBag()
        
        //        init(url: String) {
        //            updateUrl(url: url)
        //        }
        
        func updateUrl(url: String) {
            RLUserDefault.shared.baseUrlString = url
        }
        
//        func updateAuthUrl(url: String) {
//            RLUserDefault.shared.authBaseUrlString = url
//        }
        
        fileprivate lazy var provider: MoyaProvider = {
            MoyaProvider<MainService>.init(session: DefaultAlamofireSession.shared, plugins: [NetworkLoggerPlugin.init(configuration: NetworkLoggerPlugin.Configuration.init(logOptions: .verbose))])
        }()
        
        private class DefaultAlamofireSession: Alamofire.Session {
            static let shared: DefaultAlamofireSession = {
                let configuration = URLSessionConfiguration.default
                configuration.timeoutIntervalForRequest = 30 // as seconds, you can set your request timeout
                configuration.timeoutIntervalForResource = 30 // as seconds, you can set your resource timeout
                configuration.requestCachePolicy = .useProtocolCachePolicy
                return DefaultAlamofireSession(configuration: configuration)
            }()
        }
        
        private func JSONResponseDataFormatter(_ data: Data) -> Data {
            do {
                let dataAsJSON = try JSONSerialization.jsonObject(with: data)
                return try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
            } catch {
                return data // fallback to original data if it can't be serialized.
            }
        }
        
        func age() -> Observable<(min: Int, max: Int)> {
            let response = self.provider.rx.request(.appOption(appOptionCode: AppOptionType.age.code))
                .filterSuccessfulStatusCodes()
                .map(AppOptionResponseModel.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        let list = $0.result
                        let min = Int(list.filter{ $0.optionCD == "MIN_AGE" }.first?.optionValue ?? "50")!
                        let max = Int(list.filter{ $0.optionCD == "MAX_AGE" }.first?.optionValue ?? "20")!
                        observer.on(.next((min: min, max: max)))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.next((min: 20, max: 50)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func appOption(appOptionType: AppOptionType) -> Observable<[AppOptionModel]> {
            let response = self.provider.rx.request(.appOption(appOptionCode: appOptionType.code))
                .filterSuccessfulStatusCodes()
                .map(AppOptionResponseModel.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getToken(userNoAes: String?, userId: String, password: String) -> Observable<TokenModel?> {
            guard let _userNoAes = userNoAes else { return Observable.just(nil)}
            let params = ["username": userId,
                          "password": password,
                          "grant_type": "password"]
            let response = self.provider.rx.request(.auth_001_requestToken(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(TokenModel.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: { token in
                    RLUserDefault.shared.userId = userId
                    MainInformation.shared.userNoAes = _userNoAes
                    token.saveUserDefaults()
                    observer.on(.next(token))
                    
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        // Main API 호출시 리프레시
        func refreshToken() -> Single<TokenModel> {
            guard let token = RLUserDefault.shared.token,
                let userId = RLUserDefault.shared.userId
                else {
                    let err = CustomError.init(description: "Token Empty", code: 9999)
                    return Single.error(err)
            }
            let params = ["username": userId,
                          "refresh_token": token.refreshToken,
                          "grant_type": "refresh_token"]
            return self.provider.rx
                .request(.auth_002_refreshToken(params: params))
                .filterSuccessfulStatusCodes()
                .map(TokenModel.self)
        }
        
        func getServerList() -> Observable<[BaseURL]> {
            let response = self.provider.rx.request(.baseURlSelect)
                .filterSuccessfulStatusCodes()
                .map(BaseURLModel.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    observer.on(.next($0.result))
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func join(params: [String: Any]) -> Observable<String?> {
            let response = self.provider.rx.request(.join_001(params: params))
                .filterSuccessfulStatusCodes()
                //                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))

                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func checkAlreadyMember(params: [String: Any]) -> Observable<Cert001Response> {
            let response = self.provider.rx.request(.cert_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(Cert001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func findId(params: [String: Any]) -> Observable<Account001Result?> {
            let response = self.provider.rx.request(.account_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(Account001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func changePassword(userNo: String, currentPassword: String, newPassword: String) -> Observable<Bool> {
            let params = [
                "userNo": userNo,
                "pwd": currentPassword,
                "newPwd": newPassword
            ]
            let response = self.provider.rx.request(.pwd_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func checkPassword(userNo: String, currentPassword: String) -> Observable<Bool> {
            let params = [
                "userNo": userNo,
                "pwd": currentPassword
            ]
            let response = self.provider.rx.request(.pwd_002(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func specDisplay(memberSpecNo: String, displayYn: Bool) -> Observable<Bool> {
            let params = [
                "memberSpecNo": memberSpecNo,
                "displayYn": displayYn
            ] as [String : Any]
            let response = self.provider.rx.request(.spec_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func changePassword(params: [String: Any]) -> Observable<Bool> {
            let response = self.provider.rx.request(.account_002(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func checkAccount(userId: String, password: String) -> Observable<(userId: String, password: String, result: Login001Result?)> {
            let response = self.provider.rx.request(.account_003(params: ["userId": userId.aesEncrypted!, "pwd": password.aesEncrypted!]))
                .filterSuccessfulStatusCodes()
                .map(LoginResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next((userId: userId, password: password, result: $0.result)))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func logout(userNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.account_004(params: ["userNo": userNo]))
                .filterSuccessfulStatusCodes()
                //                .retryWithAuthIf401()
                .map(LoginResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                        observer.on(.completed)
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func checkNickname(nickname: String, userNo: String) -> Observable<TextFieldResult> {
            let localCheck = Validation.shared.validate(nickname: nickname)
            guard case .ok = localCheck else { return .just(localCheck) }
            
            let response = self.provider.rx.request(.join_002(params: ["nickname": nickname, "userNo": userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(TextFieldResult.ok(message: "")))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.next(TextFieldResult.failed(message: $0.resultMsg)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addSpecFace(userNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.join_004(params: ["userNo": userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addSpecPhoto(userNo: String, specNo: String, stepSeq: Int, imageDatas: [Data]) -> Observable<Bool> {
            let params = ["userNo": userNo, "specNo": specNo, "stepSeq": stepSeq] as [String : Any]
            let response = self.provider.rx.request(.join_003(params: params, imageDatas: imageDatas))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addUnivPhoto(userNo: String, univNo: String, imageDatas: [Data]) -> Observable<Bool> {
            let params = ["userNo": userNo, "univNo": univNo] as [String : Any]
            let response = self.provider.rx.request(.univ_001(params: params, imageDatas: imageDatas))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addCompanyPhoto(userNo: String, coNo: String, imageDatas: [Data]) -> Observable<Bool> {
            let params = ["userNo": userNo, "coNo": coNo] as [String : Any]
            let response = self.provider.rx.request(.company_001(params: params, imageDatas: imageDatas))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addPhoto(userNo: String, imageData: Data) -> Observable<[Photo001Result]> {
            let response = self.provider.rx.request(.photo_001(params: ["userNo": userNo], imageData: imageData))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Photo001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func modifyPhoto(userNo: String, photoNo: String, imageData: Data) -> Observable<[Photo001Result]>  {
            let response = self.provider.rx.request(.photo_002(params: ["userNo": userNo, "photoNo": photoNo], imageData: imageData))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Photo001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func removePhoto(userNo: String, photoNo: String) -> Observable<Bool>  {
            let response = self.provider.rx.request(.photo_003(params: ["userNo": userNo, "photoNo": photoNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func updateOrderPhoto(userNo: String, photoNoList: [String]) -> Observable<Bool>  {
            let response = self.provider.rx.request(.photo_004(params: ["userNo": userNo, "photoNoList": photoNoList]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        //
        func addRecommend(userNo: String, recoCd: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.reco_001(params: ["userNo": userNo, "recoCd": recoCd]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getCardMainSections(userNo: String) -> Observable<[TodaySection]> {
            let result = self.getCardMainList(userNo: userNo)
            let nickname = Repository.shared.graphQl.getNickname(userNo: userNo)
            return Observable.combineLatest(result, nickname)
                .map{_result, _nickname in
                    var todaySectionList: [TodaySection] = []
                    todaySectionList = _result.compactMap{ model -> TodaySection? in
                        let type = model.type
                        if type == "TOP_ADDCARD" {
                            return TodaySection.init(type: .topAddCard, header: "", cardPagingType: type, items: [TodayItem.init(isEmpty: true)])
                        }else if type == "TODAY" {
                            guard let _list = model.list else { return nil }
                            let itemList: [TodayItem] = _list
                                .compactMap { CardModel.init(card001Item: $0) }
                                .map{ TodayItem.init(card: $0)}
                            if itemList.count == 0 {
                                return TodaySection.init(type: .todayEmpty, header: _nickname, cardPagingType: type, items: [TodayItem.init(isEmpty: true)])
                            }else{
                                guard let _hasNext = model.hasNext else { return nil }
                                return TodaySection.init(type: .today, header: _nickname, hasNext: _hasNext, cardPagingType: type, items: itemList)
                            }
                        }else if type == "POPULAR" {
                            guard let _list = model.list else { return nil }
                            let addList: [AddCardModel] = _list.compactMap{ AddCardModel.init(card001Item: $0) }
                            return TodaySection.init(type: .popular(list: addList), header: _nickname, cardPagingType: type, items: [TodayItem.init(isEmpty: true)])
                        }else if type == "PAST_NOT_OPEN" {
                            guard let _list = model.list else { return nil }
                            let itemList: [TodayItem] = _list
                                .compactMap { CardModel.init(card001Item: $0) }
                                .map{ TodayItem.init(card: $0)}
                            if itemList.count == 0 { return nil }
                            guard let _hasNext = model.hasNext else { return nil }
                            return TodaySection.init(type: .pastNotOpen, header: "열어보지 않은 지난 카드", hasNext: _hasNext, cardPagingType: type, items: itemList)
                        }else if type == "PAST_OPEN" {
                            guard let _list = model.list else { return nil }
                            let itemList: [TodayItem] = _list
                                .compactMap { CardModel.init(card001Item: $0) }
                                .map{ TodayItem.init(card: $0)}
                            if itemList.count == 0 { return nil }
                            guard let _hasNext = model.hasNext else { return nil }
                            return TodaySection.init(type: .pastOpen, header: "열어본 지난 카드", hasNext: _hasNext, cardPagingType: type, items: itemList)
                        }else if type == "BANNER" {
                            guard let _list = model.list, _list.count > 0 else { return nil }
                            let bannerList: [BannerModel] = _list.compactMap{ BannerModel.init(card001Item: $0) }
                            return TodaySection.init(type: .banner(bannerList: bannerList), header: "", cardPagingType: type, items: [TodayItem.init(isEmpty: true)])
                        }
                        return nil
                    }
                    todaySectionList.append(TodaySection.init(type: .space, header: "", cardPagingType: "", items: [TodayItem.init(isEmpty: true)]))
                    return todaySectionList }
        }
            
        
        func getCardMainList(userNo: String) -> Observable<[Card001Result]> {
            let response = self.provider.rx.request(.card_001(params: ["userNo": userNo]))
            .filterSuccessfulStatusCodes()
            .retryWithAuthIf401()
            .map(Card001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result.compactMap{$0}))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }

        func moreCardList(userNo: String, lastMatchNo: String, cardPastType: String) -> Observable<(hasNext: Bool, items: [TodayItem])> {
            let response = self.provider.rx.request(.card_012(params: ["userNo": userNo, "matchNo": lastMatchNo, "cardPagingType": cardPastType]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Card002Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        let model = $0.result
                        let _list = model.list ?? []
                        let itemList: [TodayItem] = _list
                            .compactMap { CardModel.init(card001Item: $0) }
                            .map{ TodayItem.init(card: $0)}
                        let _hasNext = model.hasNext ?? false
                        observer.on(.next((hasNext: _hasNext, items: itemList)))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func openCard(userNo: String, matchNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.card_002(params: ["userNo": userNo, "matchNo": matchNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getMyCardDetail(userNo: String) -> Observable<CardDetailResult?> {
            let response = self.provider.rx.request(.profile_001(params: ["userNo": userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardDetailResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getMyCardDetailEdit(userNo: String) -> Observable<CardDetailEditResult?> {
            let response = self.provider.rx.request(.profile_002(params: ["userNo": userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardDetailEditResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func outWithReason(userNo: String, outReason: String) -> Observable<Bool> {
            let params = [
                "userNo": userNo,
                "outReason": outReason
            ]
            let response = self.provider.rx.request(.out_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func recoveryFromOut(userNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.out_002(params: ["userNo": userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithTimeOut()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getCardDetail(userNo: String, matchNo: String) -> Observable<CardDetailResult?> {
            let response = self.provider.rx.request(.card_003(params: ["userNo": userNo, "matchNo": matchNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardDetailResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getCrown(userNo: String) -> Observable<CrownModel?> {
            let response = self.provider.rx.request(.app_002(params: ["userNo": userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Crown001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(CrownModel.init(json: $0.result)))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addScore(userNo: String, matchNo: String, score: Int) -> Observable<Card005Result> {
            let response = self.provider.rx.request(.card_004(params: ["userNo": userNo, "matchNo": matchNo, "score": score]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Card005Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }    
        }
        
        func sendOK(userNo: String, matchNo: String, msg: String, okType: OKType) -> Observable<CardDetailResult> {
            var cardType: String = "TODAY"
            var cardOkType: String = "SEND"
            if okType == .ok {
                cardType = "TODAY" // (오늘의 카드)
            }else if okType == .okPast {
                cardType = "PAST" // (지난 카드)
            }else if okType == .okHigh {
                cardType = "RECEIVE_HIGH" // (받은 높음저수 카드)
            }else if okType == .receiveOk {
                cardType = "ACCEPT" // (받은 높은점수 카드)
                cardOkType = "ACCEPT"
            }
            let response = self.provider.rx.request(.card_005(params: ["userNo": userNo, "matchNo": matchNo, "msg": msg, "cardType": cardType, "cardOkType" : cardOkType]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardDetailResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func sendSuperOk(userNo: String, matchNo: String, msg: String, okType: OKType) -> Observable<CardDetailResult> {
            var cardOkType: String = "SEND"
            if okType == .superOk || okType == .superOkPast || okType == .superOkOneMore || okType == .superOkOneMorePast {
                cardOkType = "SEND" // (오늘의 카드)
            }else if okType == .receiveSuperOk {
                cardOkType = "ACCEPT"
            }
            
            let response = self.provider.rx.request(.card_006(params: ["userNo": userNo, "matchNo": matchNo, "msg": msg, "cardSuperOkType": cardOkType]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardDetailResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func openMbNo(userNo: String, matchNo: String) -> Observable<CardDetailResult> {
            let response = self.provider.rx.request(.card_007(params: ["userNo": userNo, "matchNo": matchNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardDetailResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getReportReasonList() -> Observable<[ReportModel]> {
            let response = self.provider.rx.request(.report_001)
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Report001Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func sendReport(userNo: String, reportedUserNo: String, reportGroupNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.report_002(params: [
                "reporterUserNo": userNo,
                "reportedUserNo": reportedUserNo,
                "reportGroupNo": reportGroupNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }

        func getAvailableCardDelCount(userNo: String) -> Observable<CountResult> {
            let response = self.provider.rx.request(.card_008(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CountResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func removeCard(userNo: String, matchNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.card_009(params: ["userNo": userNo, "matchNo": matchNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        /// 받은 카드 조회
        func getHistoryReceiveCardList(userNo: String) -> Observable<CardHistoryResult> {
            let response = self.provider.rx.request(.card_010(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardHistoryResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        /// 받은 카드 페이징 조회
        func getHistoryCardMore(userNo: String, lastMatchNo: String?, cardPagingType: String) -> Observable<HistoryCard> {
            var params = ["userNo" : userNo, "cardPagingType": cardPagingType]
            if let _lastMatchNo = lastMatchNo {
                params["matchNo"] = _lastMatchNo
            }
            let response = self.provider.rx.request(.card_012(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Card012Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        /// 보낸 카드 조회
        func getHistorySendCardList(userNo: String) -> Observable<CardHistoryResult> {
            let response = self.provider.rx.request(.card_011(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CardHistoryResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        
        /// 성공 카드 조회
        func getHistorySuccessMatchCardList(userNo: String) -> Observable<HistoryCard> {
            return self.getHistoryCardMore(userNo: userNo, lastMatchNo: nil, cardPagingType: "SUCCESS")
        }
        
        /// 추가카드 리스트 전체 불러오기
        func additionalCardList(userNo: String) -> Observable<[AddCardGroup]> {
            let response = self.provider.rx.request(.card_013(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Card016Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }

        func getAdditionalCardOption(userNo: String, cardNo: String) -> Observable<CardOptionResult> {
            let response = self.provider.rx.request(.card_014(params: ["userNo" : userNo, "cardNo": cardNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(Card014Response.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }

                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        
        func addAdditionalCard(optionType: OptionType, userNo: String, cardNo: String, minAge: Int, maxAge: Int, /*필수*/
            bodyCdList: [String]?, /*스타일, 연봉*/
            tallCdList: [String]?, /*스타일*/
            religionCdList: [String]?, /*종교*/
            salaryCdList: [String]?, /*연봉*/
            areaNo: String? /*지역*/) -> Observable<Bool> {
            var params: [String: Any] = ["userNo" : userNo, "cardNo": cardNo, "minAge": minAge, "maxAge": maxAge]
            switch optionType {
            case .area: if let value = areaNo { params["areaNo"] = value }
            case .general: ()
            case .religion: if let value = religionCdList { params["religionCdList"] = value }
            case .salary: if let value = salaryCdList { params["salaryCdList"] = value }
            case .style:
                if let value = bodyCdList { params["bodyCdList"] = value }
                if let value = tallCdList { params["tallCdList"] = value }
            }
            let response = self.provider.rx.request(.card_015(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)

            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getLiveMatchResult(userNo: String) -> Observable<LiveMatchResult?> {
            let response = self.provider.rx.request(.liveMatch_001(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(LiveMatchResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next($0.result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func isLiveMatchSearching(userNo: String) -> Observable<Bool?> {
            return getLiveMatchResult(userNo: userNo).map{ $0?.progressYn }
        }
        
        func findLiveMatchNewResult(userNo: String) -> Observable<String> {
            let response = self.provider.rx.request(.liveMatch_002(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getLiveMatchCard(userNo: String, viewDate: String?) -> Observable<LiveMatchCardResult> {
            var params = ["userNo" : userNo]
            if let _viewDate = viewDate {
                params["viewDate"] = _viewDate
            }
            let response = self.provider.rx.request(.liveMatch_003(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(LiveMatchCardResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addLiveMatchCardAction(userNo: String, viewDate: String?, liveMatchHistNo: String, likeYn: Bool) -> Observable<LiveMatchCardResult> {
            var params: [String : Any] = ["userNo": userNo, "liveMatchHistNo": liveMatchHistNo, "likeYn": likeYn]
            params["viewDate"] = viewDate ?? Date.requestViewDate()
            let response = self.provider.rx.request(.liveMatch_004(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(LiveMatchCardResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func rollBackLiveMatchCard(userNo: String, viewDate: String?) -> Observable<LiveMatchCardResult> {
            var params = ["userNo" : userNo]
            params["viewDate"] = viewDate ?? Date.requestViewDate()
            let response = self.provider.rx.request(.liveMatch_005(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(LiveMatchCardResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }

        func openLiveMatchCard(userNo: String, liveMatchHistNo: String, cardType: String) -> Observable<(String?, String?)> {
            let response = self.provider.rx.request(.liveMatch_006(params: ["userNo" : userNo, "liveMatchHistNo": liveMatchHistNo, "cardType": cardType]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(LiveMatchOpenResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next((result.matchNo, nil)))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        
        /*
         cardPagingType : (LIVE_MATCH_RECEIVE - 나에게 / LIVE_MATCH_SEND - 내가)
         */
        func getLiveMatchList(userNo: String, lastLiveMatchHistNo: String?, cardPagingType: String, viewDate: String?) -> Observable<LiveMatchListResult> {
            var params = ["userNo" : userNo, "cardPagingType": cardPagingType]
            if let liveMatchHistNo = lastLiveMatchHistNo {
                params["liveMatchHistNo"] = liveMatchHistNo
            }
//            if let _viewDate = viewDate {
//                params["viewDate"] = _viewDate
//            }
            params["viewDate"] = viewDate ?? Date.requestViewDate()
            let response = self.provider.rx.request(.liveMatch_007(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(LiveMatchListResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                    
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func checkLiveMatchValid(userNo: String) -> Observable<Bool> {
            let response = self.provider.rx.request(.liveMatch_008(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getItemNo(userNo: String, productId: String) -> Observable<String> {
            return Repository.shared.main
                .getStoreItemList(userNo: userNo)
                .map{ result -> String in
                    let new = result.itemList.first(where:{ $0.productID == productId })!
                    return new.itemNo }
        }
        
        func getStoreItemList(userNo: String) -> Observable<StoreListResult> {
            let response = self.provider.rx.request(.store_001(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(StoreListResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getStoreHistoryList(userNo: String, lastCrownNo: String? = nil) -> Observable<StoreHistoryListResult> {
            var params = ["userNo" : userNo]
            if let _lastCrownNo = lastCrownNo { params["crownNo"] = _lastCrownNo }
            let response = self.provider.rx.request(.store_002(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(StoreHistoryListResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func addInappReceipt(userNo: String, itemNo: String, receipt: String?) -> Observable<Int> {
            let response = self.provider.rx.request(.store_003(params: ["userNo" : userNo, "itemNo": itemNo, "receipt": receipt ?? "No Receipt"]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(StoreAddReceiptResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result.crown))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getNotificationList(userNo: String, lastNoticeNo: String?) -> Observable<NotificationListResult> {
            var params = ["userNo" : userNo]
            if let _lastNoticeNo = lastNoticeNo {
                params["noticeNo"] = _lastNoticeNo
            }
            
            let response = self.provider.rx.request(.nc_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(NotificationListResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getNoticeList(userNo: String, lastNoticeNo: String?) -> Observable<NoticeListResult> {
            var params = ["userNo" : userNo]
            if let _lastNoticeNo = lastNoticeNo {
                params["noticeNo"] = _lastNoticeNo
            }
            
            let response = self.provider.rx.request(.bbs_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(NoticeListResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getEventList(userNo: String, lastEventNo: String?) -> Observable<EventListResult> {
            var params = ["userNo" : userNo]
            if let _lastEventNo = lastEventNo {
                params["eventNo"] = _lastEventNo
            }
            
            let response = self.provider.rx.request(.bbs_002(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(EventListResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getEventDetail(userNo: String, eventNo: String) -> Observable<EventMainModel> {
            let response = self.provider.rx.request(.bbs_003(params:  ["userNo" : userNo, "eventNo": eventNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(EventDetailResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
         
        func redDot(userNo: String, noticeBbsViewDate: String?, eventBbsViewDate: String?, liveMatchViewDate: String?) -> Observable<RedDotResult> {
            var params = ["userNo" : userNo]
            params["noticeBbsViewDate"] = noticeBbsViewDate ?? Date.requestViewDate()
            params["eventBbsViewDate"] = eventBbsViewDate ?? Date.requestViewDate()
            params["liveMatchViewDate"] = liveMatchViewDate ?? Date.requestViewDate()
            
            let response = self.provider.rx.request(.dot_001(params: params))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(RedDotResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func sleep(userNo: String, sleepYn: Bool) -> Observable<Bool> {
            let response = self.provider.rx.request(.sleep_001(params:  ["userNo" : userNo, "sleepYn": sleepYn]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(MainStringResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        observer.on(.next(true))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        func getSleepCount(userNo: String) -> Observable<CountResult> {
            let response = self.provider.rx.request(.sleep_002(params: ["userNo" : userNo]))
                .filterSuccessfulStatusCodes()
                .retryWithAuthIf401()
                .map(CountResponse.self)
            
            return Observable.create { observer in
                response.subscribe(onSuccess: {
                    if $0.resultCD == RLResultCD.SUCCESS.code {
                        guard let result = $0.result else { return }
                        observer.on(.next(result))
                    }else if $0.resultCD == RLResultCD.E00000_Error.code {
                        observer.on(.error(CustomError.init(description: SERVER_ERROR_E0000_MESSAGE, resultCD: $0.resultCD)))
                    }else{
                        observer.on(.error(CustomError.init(description: $0.resultMsg, resultCD: $0.resultCD)))
                    }
                },onError: { error in
                    error.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                    observer.on(.error(CustomError.init(description: SERVER_ERROR_NETWORK_MESSAGE, resultCD: RLResultCD.NETWORK_ERROR.code)))
                })
                .disposed(by: self.bag)
                return Disposables.create()
            }
        }
        
        
    }
}

