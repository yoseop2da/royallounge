//
//  PushModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/04/06.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

enum PushType: String {
    case joinIncomplete = "JOIN_INCOMPLETE" // 다음 작성 프로필 항목
    case joinApproval = "JOIN_APPROVAL" // 오늘의카드 메인
    case joinRejcet = "JOIN_REJECT" // [A4_2_심사 반려]화면
    
    case cardLunchMatchOpen = "CARD_LUNCH_MATCH_OPEN" // 오늘의카드 메인
    case memberSleepRelease = "MEMBER_SLEEP_RELEASE" // 오늘의카드 메인
    
    case statusBlock = "STATUS_BLOCK" // 로그인 메인
    case statusSuspend = "STATUS_SUSPEND" // 로그인 메인
    case statusSuspendRelease = "STATUS_SUSPEND_RELEASE" // 로그인 메인
    
    case cardSuperOk = "CARD_SUPER_OK" // 카드 보관함 > 받은 카드 리스트
    case cardOk = "CARD_OK" // 카드 보관함 > 받은 카드 리스트
    case cardHigh = "CARD_HIGH" // 카드 보관함 > 받은 카드 리스트
    case cardEachOtherHigh = "CARD_EACH_OTHER_HIGH" // 카드 보관함 > 받은 카드 리스트
    case cardOkSuccess = "CARD_OK_SUCCESS" // 카드 보관함 > 성공 카드 리스트
    case cardSuperOkSuccess = "CARD_SUPER_OK_SUCCESS" // 카드 보관함 > 성공 카드 리스트
    
    case liveMatchComplete = "LIVE_MATCH_COMPLETE" // 라이브 매칭 메인
    case adminLogout = "ADMIN_LOGOUT"

    // 아직 없음
    case statusBlockOverAge = "STATUS_BLOCK_OVER_AGE"
    case statusOut = "STATUS_OUT"
    
    
    
    
//    case cardToday = "CARD_TODAY" // >> 오늘의 매칭 || 오늘의 카드 탭으로 이동
//    case cardOppLikeView = "CARD_OPP_LIKE_VIEW" // >> 상대방의 좋아요 확인 || 카드상세로 이동
//    case cardOppSupLikeView = "CARD_OPP_SUP_LIKE_VIEW" //: 상대방의 슈퍼 좋아요 카드 확인
//    case cardOppLike = "CARD_OPP_LIKE" // >> 상대방의 좋아요 || 카드상세로 이동
//    case cardOppSupLike = "CARD_OPP_SUP_LIKE" // >> 상대방의 슈퍼좋아요 || 카드상세로 이동
//    case cardOppHigh = "CARD_OPP_HIGH" // >> 상대방의 높은점수 || 나를 좋아하는 카드 탭으로 이동 ( 높은 별점 받은 섹션 )
//    case matchSuccess = "MATCH_SUCC" // >> 매칭 성공 || 채팅룸으로 이동
//    case evaluationOppHigh = "EVAL_OPP_HIGH" // >> 평가 높은점수 || 나를 좋아하는 카드 탭으로 이동 ( 높은 별점 받은 섹션 )
//    case evaluationEnd = "EVAL_END" // >> 평가 완료 || 내 점수 확인 페이지로 이동

    case none = "NONE" // 아무케이스도 아님

    var logValue: String { return self.rawValue }

    static func convertType(from value: String) -> PushType {
        switch value {
        case PushType.joinIncomplete.logValue: return .joinIncomplete
        case PushType.joinApproval.logValue: return .joinApproval
        case PushType.joinRejcet.logValue: return .joinRejcet
        
        case PushType.cardLunchMatchOpen.logValue: return .cardLunchMatchOpen
        case PushType.memberSleepRelease.logValue: return .memberSleepRelease
        
        case PushType.statusBlock.logValue: return .statusBlock
        case PushType.statusSuspend.logValue: return .statusSuspend
        case PushType.statusSuspendRelease.logValue: return .statusSuspendRelease
        
        case PushType.cardOk.logValue: return .cardOk
        case PushType.cardSuperOk.logValue: return .cardSuperOk
        case PushType.cardHigh.logValue: return .cardHigh
        case PushType.cardEachOtherHigh.logValue: return .cardEachOtherHigh
        case PushType.cardOkSuccess.logValue: return .cardOkSuccess
        case PushType.cardSuperOkSuccess.logValue: return .cardSuperOkSuccess
        case PushType.liveMatchComplete.logValue: return .liveMatchComplete
        case PushType.adminLogout.logValue: return .adminLogout
            
        case PushType.statusBlockOverAge.logValue: return .statusBlockOverAge
        case PushType.statusOut.logValue: return .statusOut
            
        default: return .none
        }
    }

}

class PushModel: NSObject {

    var title: String?
    var message: String = ""
    var roomNo: String?
    var oppUserNo: String?
    var oppNickname: String?
    var matchNo: String?
    var pushType: PushType = .none
    var mediaUrl: String?

    var displayCustomPopup: Bool {
        return false
//            self.pushType == .joinApproval
//            || self.pushType == .joinRejcet
//            || self.pushType == .statusSuspendRelease
//            || self.pushType == .memNormalReject
//            || self.pushType == .memReport

    }

    var attrStr: NSAttributedString {
        if let _oppNickname = oppNickname {
            let fullStr = "\(_oppNickname)님의 메세지 :\n\(message)"
            let attrStr = NSMutableAttributedString.init(string: fullStr)
            let boldUserRange = (fullStr as NSString).range(of: _oppNickname)
            attrStr.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 12)], range: boldUserRange)
            return attrStr
        }

        if let _title = title {
            let fullStr = "\(_title)님의 메세지 :\n\(message)"
            let attrStr = NSMutableAttributedString.init(string: fullStr)
            let boldUserRange = (fullStr as NSString).range(of: _title)
            attrStr.addAttributes([.font: UIFont.spoqaHanSansBold(ofSize: 12)], range: boldUserRange)
            return attrStr
        }

        let attrStr = NSMutableAttributedString.init(string: message)
        return attrStr
    }

    init(json: [AnyHashable: Any]) {
        super.init()
        guard let result = json as? [String: AnyObject] else { return }

        // aps
        guard let aps = result["aps"] as? [String: AnyObject] else { return }

        let BackgroundPushValue = "1"
        if let value = aps["content-available"] as? String, value == BackgroundPushValue {
            // background push
            self.backgroundPushSettings(result: result)
        } else {
            self.normalPushSettings(result: result, aps: aps)
        }

        print("-------------- push ---------------\n\(self)")
    }

    func normalPushSettings(result: [String: AnyObject], aps: [String: AnyObject]) {
        if let alert = aps["alert"] as? [String: AnyObject],
            let title = alert["title"] as? String,
            let body = alert["body"] as? String {
            self.title = title
            self.message = body
        }
        
        // 루트에서 사용하던 부분, 로라에서는 사용하지 않을 예정!!!!!
        if let value = result["gcm.notification.PUSH_TYPE"] as? String { self.pushType = PushType.convertType(from: value) }
        
        if let value = result["pushType"] as? String {
            self.pushType = PushType.convertType(from: value)
        }

        if let url = result["mediaUrl"] as? String {
            self.mediaUrl = url
        }

        if let stringDic = result["gcm.notification.ROUTE_DATA"] as? String {
            let data = stringDic.data(using: .utf8)!
            do {
                if let dic = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject] {
                    print(dic) // use the json here
                    if let value = dic["roomNo"] as? String { self.roomNo = value }

                    // 채팅 메세지일 경우에만 해당.
                    if let value = dic["oppNickname"] as? String {
                        self.title = value
                        self.oppNickname = value
                    }
                    if let value = dic["message"] as? String { self.message = value }

                    if let value = dic["oppUserNo"] as? String { self.oppUserNo = value }
                    if let value = dic["matchNo"] as? String { self.matchNo = value }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }

    func backgroundPushSettings(result: [String: AnyObject]) {
        if let value = result["PUSH_TYPE"] as? String { self.pushType = PushType.convertType(from: value) }
        if let value = result["pushType"] as? String {
            self.pushType = PushType.convertType(from: value)
        }
        if let value = result["title"] as? String {
            self.title = value
        }
        if let stringDic = result["ROUTE_DATA"] as? String {
            let data = stringDic.data(using: .utf8)!
            do {
                if let dic = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject] {
                    print(dic) // use the json here
                    if let value = dic["roomNo"] as? String { self.roomNo = value }
//                    // 채팅 메세지일 경우에만 해당.
//                    if let value = dic["sender"] as? String { self.title = value }
//                    if let value = dic["message"] as? String { self.message = value }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
}

extension PushModel {
    override var description: String {
        return """

        ┏[ PushModel ]━━━━━━━━━━
        ┃
        ┃ * title : \(title ?? "")
        ┃ * message : \(message)
        ┃ * pushType : \(pushType.logValue)
        ┃ * roomNo : \(roomNo ?? "There is no roomNo.")
        ┃ * oppUserNo : \(oppUserNo ?? "null")
        ┃ * oppNickname : \(oppNickname ?? "null")
        ┃ * matchNo : \(matchNo ?? "null")
        ┃ * mediaUrl : \(mediaUrl ?? "null")
        ┃
        ┗👾👾👾👾👾

        """
    }
}
