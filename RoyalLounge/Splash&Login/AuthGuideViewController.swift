//
//  AuthGuideViewController.swift
//  Route
//
//  Created by yoseop park on 16/09/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit

class AuthGuideViewController: BaseViewController {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.layer.cornerRadius = 6.0
        
        let barButton = RLBarButtonItem(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack(animated: true)
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        let rBarButton = RLBarButtonItem(barType: .close)
        rBarButton.rx.tap
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItem = rBarButton
        let fontSize: CGFloat = UIScreen.underSE ? 13.0 : 16.0
        let fullStr = "로얄라운지는 국내 최대 신용평가기관인\nKCB 코리아크레딧뷰의 시스템을 통한 철저한 \n본인인증을 거치고 있습니다.\n\n로얄라운지의 모든 유저는 본인 인증 시스템을 \n통해 인증된 실명, 나이, 연락처를 사용하고 있으며\n \n단 한명의 허위/알바회원도 존재하지 않으므로 \n안심하시기 바랍니다."
        let attributedString = NSMutableAttributedString(string: fullStr, attributes: [
            .font: UIFont.spoqaHanSansLight(ofSize: fontSize),
            .foregroundColor: UIColor.gray50
        ])
        attributedString.addAttribute(.font, value: UIFont.spoqaHanSansBold(ofSize: fontSize), range: (fullStr as NSString).range(of: "단 한명의 허위/알바회원"))
        infoLabel.attributedText = attributedString
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { vc in
                let vc = RLStoryboard.splash.authViewController()
                vc.authType = .join
            self.push(vc)
        })
        .disposed(by: bag)
    }

    @IBAction func nextButtonTouched(_ sender: Any) {
        
    }
}
