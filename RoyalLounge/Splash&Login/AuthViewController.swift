//
//  AuthViewController.swift
//  Route
//
//  Created by yoseop park on 16/09/2019.
//  Copyright © 2019 HSOCIETY. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import RxCocoa
import RxWebKit

class AuthViewController: ClearNaviBaseViewController {

    enum AuthType {
        case join
        case findAccount
        case changeMobileNo
    }
    
    @IBOutlet weak var webViewContainerView: UIView!

    private var webView: WKWebView!
    private let webUrl = "https://kcb.royallounge.co.kr/kcb/popup2"
    private let SCRIPT_MESSAGE_HANDLER = "kcbResult"
    private var indicator: UIActivityIndicatorView?
    private var createWebView: WKWebView?
    var viewModel = AuthViewModel()
    
    var authType: AuthType = .join
    var changeMobileNoCallback: ((AuthModel)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let count = self.navigationController?.viewControllers.count {
            let isModal = count == 1
            let barButton: RLBarButtonItem!
            
            if isModal {
                barButton = RLBarButtonItem.init(barType: .close)
                self.navigationItem.rightBarButtonItem = barButton
            } else {
                barButton = RLBarButtonItem.init(barType: .back)
                self.navigationItem.leftBarButtonItem = barButton
            }
            
            barButton.tintColor = UIColor.dark
            barButton.rx.tap
                .subscribe(onNext: { _ in
                    self.closeButtonAction()
                })
                .disposed(by: bag)
        }
        
        // 자바 스크립트 콜백 연결
        let userContent = WKUserContentController()
        userContent.add(self, name: SCRIPT_MESSAGE_HANDLER)
        let config = getConfig(userContent: userContent)
        webView = WKWebView(frame: .zero, configuration: config)
        webView.uiDelegate = self
        self.view.addSubview(webView)

        // 인디케이터
        indicator = UIActivityIndicatorView.init(style: .gray)
        self.view.addSubview(indicator!)
        mainAsync {
            self.webView.frame = self.webViewContainerView.bounds
            self.indicator?.center = self.view.center
        }

        let url = URL(string: webUrl)
        let request = URLRequest(url: url!)
        webView.load(request)
        
        webView.rx
            .didCommitNavigation
            .subscribe(onNext: { [weak self] (webView, navigation) in
            self?.indicator?.isHidden = false
            self?.indicator?.startAnimating()
            })
            .disposed(by: bag)
        
        webView.rx
            .didFinishNavigation
            .subscribe(onNext: { [weak self] (webView, navigation) in
                self?.indicator?.isHidden = true
                self?.indicator?.stopAnimating()
            })
        .disposed(by: bag)

        webView.rx
            .javaScriptAlertPanel
            .subscribe(onNext: { [weak self] (webView, message, frame, handler) in
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                alertController.addAction( UIAlertAction.init(title: "확인", style: .default, handler: { _ in
                    handler()
                }))
                self?.modal(alertController, animated: true)
            })
        .disposed(by: bag)
        
        webView.rx
            .javaScriptConfirmPanel
            .subscribe(onNext: { [weak self] (webView, message, frame, handler) in
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction.init(title: "확인", style: .default, handler: { _ in handler(true)
                    self?.closeButtonAction()
                }))
                alertController.addAction(UIAlertAction.init(title: "취소", style: .default, handler: { _ in
                    handler(false)
                }))
                self?.modal(alertController, animated: true)
            })
        .disposed(by: bag)
        
        webView.rx
            .decidePolicyNavigationAction
            .subscribe(onNext: { (webView, action, handler) in
                if let surl = action.request.url?.absoluteString {
                    print("탐색..... [\(surl)]")
                }
                handler(.allow) // 탐색 허용
            })
        .disposed(by: bag)
        
        viewModel.successAuth
            .subscribe(onNext: { auth in
                if self.authType == .join {
                    let vc = RLStoryboard.join.acceptTermsViewController(auth: auth)
                    self.push(vc)
                }else if self.authType == .findAccount {
                    let vc = RLStoryboard.splash.findIDViewController(auth: auth)
                    self.push(vc)
                }else if self.authType == .changeMobileNo {
                    self.changeMobileNoCallback?(auth)
                    self.closeButtonAction()
                }
            })
            .disposed(by: bag)
        
        viewModel.alreadyMemberObserver
            .subscribe(onNext: { email, auth in
                if self.authType == .findAccount {
                    let vc = RLStoryboard.splash.findIDViewController(auth: auth)
                    self.push(vc)
                }else{
                    self.dismiss(animated: true) {
                        RLNotificationCenter.shared.post(notiType: .splash, object: SplashViewController.CallbackType.toLoginPopup(email: email))
                    }
                }
            })
            .disposed(by: bag)
        
        viewModel.outRecovery
            .subscribe(onNext: {
                guard $0 == true else { return }
                self.dismiss(animated: true) {
                    toast(message: "계정이 복구되었습니다", seconds: 1.0)
                    RLNotificationCenter.shared.post(notiType: .splash, object: SplashViewController.CallbackType.toLogin)
                }
            })
            .disposed(by: bag)
        
        viewModel.limitAgeMemberObserver
            .subscribe(onNext: { min, max in
                print("=========min : \(min)")
                print("=========max : \(max)")
            })
            .disposed(by: bag)
        
        viewModel.outMemberObserver
            .subscribe(onNext: { day, userId, userNo, recoveryMsg in
                RLPopup.shared.showOutMember(attrFullStr: recoveryMsg, day: day, userId: userId, rightAction: {
                    // 계정 활성화하기
                    self.viewModel.recoveryFromOut(userNo: userNo)
                })
            })
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.closeButtonAction()
            })
            .disposed(by: bag)
    }
    
    func getConfig(userContent: WKUserContentController) -> WKWebViewConfiguration {
        let config = WKWebViewConfiguration()
        config.userContentController = userContent

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        config.preferences = preferences
        return config
    }
}

private extension AuthViewController {
    func authCheckFinished(auth: AuthModel) {
        if authType == .join {
            self.viewModel.checkMember(auth: auth)
        }else if authType == .findAccount {
            self.viewModel.checkMember(auth: auth)
        }else if authType == .changeMobileNo {
            self.viewModel.successAuth.on(.next(auth))
        }
    }
    
    @objc func closeButtonAction() {
        if let count = self.navigationController?.viewControllers.count {
            let isEnterByPush = count > 1
            if isEnterByPush {
                self.pushBack(animated: true)
                return
            }
        }

        self.dismiss(animated: true, completion: nil)
    }    
}

extension AuthViewController: WKScriptMessageHandler {
    /*
     자바스크립트 SCRIPT_MESSAGE_HANDLER 콜백 구현
     본인인증 완료시 데이터를 보내면 여기서 받는다.
     */
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == SCRIPT_MESSAGE_HANDLER {
            Debug.print(message.body)
            // string 포멧으로 내려올 경우(지금 이렇게 내려옴)
            if let strJson = message.body as? String, let auth = AuthModel.init(messageBody: strJson) {
                authCheckFinished(auth: auth)
                return
            }
            // json 포멧으로 내려올 경우
            if let json = message.body as? [String: AnyObject], let auth = AuthModel.init(json: json) {
                authCheckFinished(auth: auth)
                return
            }
        }
        // popup3
        closeButtonAction()
    }
}

extension AuthViewController: WKUIDelegate {
    /*
     웹뷰에서 팝업으로 화면으로 이동시
     새로운 웹뷰로 url을 띄워준다.
     */
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        //뷰를 생성하는 경우
        let frame = UIScreen.main.bounds
        let newWebView = WKWebView(frame: frame, configuration: configuration)
        createWebView = newWebView
        newWebView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        newWebView.uiDelegate = self
        view.addSubview(newWebView)
        
        return createWebView!
    }
    
    func webViewDidClose(_ webView: WKWebView) {
        if webView == createWebView {
            createWebView?.removeFromSuperview()
            createWebView = nil
        }
    }
}
