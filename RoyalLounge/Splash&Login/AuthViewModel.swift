//
//  AuthViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class AuthViewModel: BaseViewModel {
    var successAuth = PublishSubject<AuthModel>()
    var outRecovery = PublishSubject<Bool>()
    var alreadyMemberObserver = PublishSubject<(email: String, auth: AuthModel)>()
    var limitAgeMemberObserver = PublishSubject<(min: Int, max: Int)>()
    var outMemberObserver = PublishSubject<(day: Int, userId: String, userNo: String, recoveryMsg: String)>()
    
    func checkMember(auth: AuthModel) {
        Repository.shared.main
            .checkAlreadyMember(params:
                ["ci": auth.ciAES, "birthday": auth.birthdayAES])
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                if let result = $0.result {
                    if result.outYn {
                        // 탈퇴 회원
                        guard let day = result.day,
                        let userID = result.userID,
                        let userNo = result.userNo else { return }
                        self.outMemberObserver.on(.next((day: day, userId: userID, userNo: userNo, recoveryMsg: result.recoveryMsg ?? "개인정보보호정책에 따라 탈퇴 계정 재활성화 및 계정 복구가 가능한 기간입니다.")))
                    }else{
                        // 미가입 회원
                        self.successAuth.on(.next(auth))
                    }
                }else{
                    // 해당케이스는 발생할 수 없지만 예외처리해주기.
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                if custom.resultCD == RLResultCD.E01001_AlreadyMember.code {
                    self.alreadyMemberObserver.on(.next((email: custom.errorMessage, auth: auth)))
                }else if custom.resultCD == RLResultCD.E01004_AgeLimit.code {
                    Repository.shared.main.age().bind(to: self.limitAgeMemberObserver).disposed(by: self.bag)
                }else {
                    self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
                }
                
            })
            .disposed(by: bag)
    }
    
    func recoveryFromOut(userNo: String) {
        Repository.shared.main
            .recoveryFromOut(userNo: userNo)
            .subscribe(onNext: {
                if $0 { self.outRecovery.on(.next($0)) }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
    }
}
