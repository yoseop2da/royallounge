//
//  FindAccountViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class FindAccountViewController: ClearNaviBaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var custommerCenterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "계정 찾기"
        subTitleLabel.text = "로얄라운지는 안전하고 믿을 수 있는\n소개팅 앱을 제공하고자\nKCB 본인인증 절차를 진행합니다."
        nextButton.layer.cornerRadius = 6.0
        //        custommerCenterButton.underLine()
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        custommerCenterButton.rx.tap
            .rxTouchAnimation(button: custommerCenterButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                RLMailSender.shared.sendEmail(.문의_미가입유저, finishCallback: nil)
            }, onError: { err in
                
            })
            .disposed(by: bag)
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.splash.authViewController()
                vc.authType = .findAccount
                self.push(vc)
            })
            .disposed(by: bag)
    }
}
