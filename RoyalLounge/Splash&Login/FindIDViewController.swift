//
//  FindIDViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FindIDViewController: ClearNaviBaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var memberBox: UIView!
    @IBOutlet weak var noMemberBox: UIView!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var joinYearLabel: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var custommerCenterButton: UIButton!
    
    var viewModel: FindIDViewModel!
    
    var auth: AuthModel!
    var isMember: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "계정 찾기"
        subTitleLabel.text = "회원님의 아이디(이메일) 정보입니다"
        subTitleLabel.isHidden = true

        boxView.setBorder(width: 1.0, color: UIColor.rlOrange126)
        boxView.layer.cornerRadius = 6.0
        boxView.backgroundColor = .white
        nextButton.layer.cornerRadius = 6.0
        //        custommerCenterButton.underLine()
        noMemberBox.isHidden = true
        memberBox.isHidden = true
        
        emailLabel.text = auth.nameAES
        
        viewModel = FindIDViewModel(
            auth: auth,
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBackTo(viewControllerClass: FindAccountViewController.self)
            })
            .disposed(by: bag)
        
        viewModel.memberTuple
            .subscribe(onNext: { (isMember, memberInfo) in
                if isMember {
                    self.subTitleLabel.isHidden = false
                    self.noMemberBox.isHidden = true
                    self.memberBox.isHidden = false
                    self.emailLabel.text = memberInfo.email
                    self.joinYearLabel.text = "\(memberInfo.joinDate) 가입"
                    
//                    self.joinYearLabel.text = "\(memberInfo.joinDate.dateFormater(newFormat: "yyyy-MM-dd")) 가입"
                    self.nextButton.setTitle("비밀번호 변경하기", for: .normal)
                } else {
                    self.subTitleLabel.isHidden = true
                    self.noMemberBox.isHidden = false
                    self.memberBox.isHidden = true
                    self.nextButton.setTitle("회원가입", for: .normal)
                }
            })
            .disposed(by: bag)
        
        viewModel.joinAction
            .subscribe(onNext: { success in
                if success {
                    self.pushBackTo(viewControllerClass: SplashViewController.self)
                    RLNotificationCenter.shared.post(notiType: .splash, object: SplashViewController.CallbackType.toJoin)
                }
            })
            .disposed(by: bag)
        
        viewModel.findPwAction
            .subscribe(onNext: { auth in
                let vc = RLStoryboard.splash.findPasswordViewController(auth: auth)
                self.push(vc)
            })
            .disposed(by: bag)
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBackTo(viewControllerClass: FindAccountViewController.self)
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        custommerCenterButton.rx.tap
            .rxTouchAnimation(button: custommerCenterButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                RLMailSender.shared.sendEmail(.문의_미가입유저, finishCallback: nil)
            }, onError: { err in
                
            })
            .disposed(by: bag)
    }

}
