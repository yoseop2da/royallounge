//
//  FindIDViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class FindIDViewModel: BaseViewModel {
    
//    var noMember = PublishSubject<AuthModel>()
//    var alreadyMemberObserver = PublishSubject<(email: String, joinDate: String)>()
    
    let memberTuple = BehaviorRelay<(isMember: Bool, memberInfo: (email: String, joinDate: String))>.init(value: (isMember: false, memberInfo: (email: "", joinDate: "")))
    
    let joinAction = PublishRelay<Bool>()
    let findPwAction = PublishRelay<AuthModel>()
    
    init(auth: AuthModel,
         nextButtonTap: Observable<Void>) {
        super.init()
        
        Repository.shared.main
            .findId(params: ["ci": auth.ciAES])
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                if let result = $0 {
                    // 가입됨
                    self.memberTuple.accept((isMember: true, memberInfo: (email: result.id ?? "", joinDate: result.joinDate ?? "")))
                }else{
                    // 가입 없음
                    self.memberTuple.accept((isMember: false, memberInfo: (email: "", joinDate: "")))
                }
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                guard let custom = $0 as? CustomError else { return }
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            })
            .disposed(by: bag)
        
        nextButtonTap
            .map{ self.memberTuple.value.isMember }
            .subscribe(onNext: { isMember in
                if isMember {
                    self.findPwAction.accept(auth)
                }else{
                    self.joinAction.accept(true)
                }
            })
            .disposed(by: bag)
    }
}

