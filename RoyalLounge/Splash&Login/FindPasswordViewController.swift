//
//  FindPasswordViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/27.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class FindPasswordViewController: ClearNaviBaseViewController {

    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var passwordWrapView: UIView!
    @IBOutlet weak var passwordConfirmWrapView: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var custommerCenterButton: UIButton!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    var auth: AuthModel!

    let passwordTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 입력해주세요", title: "비밀번호", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .next
        return field
    }()

    let passwordConfirmTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 한번 더 입력해주세요", title: "비밀번호 확인", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .done
        return field
    }()
    
    var viewModel: FindPasswordViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "비밀번호 변경"
        nextButton.layer.cornerRadius = 6.0
        
        passwordWrapView.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(passwordWrapView)
        }
        passwordConfirmWrapView.addSubview(passwordConfirmTextField)
        passwordConfirmTextField.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalTo(passwordConfirmWrapView)
        }
        
        passwordTextField.returnKeyCallback = { self.passwordConfirmTextField.becomeFirst = true }
        
//        mainAsync {
//            self.headerView.frame = self.mainTableView.frame
//            self.mainTableView.reloadData()
//        }
        
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = FindPasswordViewModel.init(
            auth: auth,
            password: passwordTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            passwordConfirm: passwordConfirmTextField.textObserver
                .debounce(.milliseconds(100), scheduler: MainScheduler.instance),
            nextButtonTap: nextButton.rx.tap
                .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.newPasswordTextFieldResult
            .bind(to: passwordTextField.rx.setResult)
            .disposed(by: bag)
        
        viewModel.newPasswordTextFieldResultConfirm
            .bind(to: passwordConfirmTextField.rx.setResult)
            .disposed(by: bag)
        
        viewModel.passwordEnable
            .bind(to: nextButton.rx.buttonEnableState)//
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.nextAction
            .subscribe(onNext: { _ in
                toast(message: "비밀번호 변경이 완료되었습니다", seconds: 1.5) {
                    self.pushBackTo(viewControllerClass: LoginViewController.self)
                }
            })
            .disposed(by: bag)
        
        custommerCenterButton.underLine()
        custommerCenterButton.rx.tap
            .rxTouchAnimation(button: custommerCenterButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                RLMailSender.shared.sendEmail(.문의_미가입유저, finishCallback: nil)
            }, onError: { err in
                
            })
            .disposed(by: bag)
        
        keyboardHeight()
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { keyboard in
            UIView.animate(withDuration: keyboard.duration) {
                self.bottomPadding.constant = keyboard.height
                self.view.layoutIfNeeded()
            }
        })
        .disposed(by: bag)
    }
}
