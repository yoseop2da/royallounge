//
//  FindPasswordViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class FindPasswordViewModel: BaseViewModel {
    let newPasswordTextFieldResult: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    let newPasswordTextFieldResultConfirm: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    var passwordEnable: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    let nextAction: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    
    private var _newPassword: String = ""
    private var auth: AuthModel!
    
    init(auth: AuthModel,
         password: Observable<String>,
         passwordConfirm: Observable<String>,
         nextButtonTap: Observable<Void>) {
        super.init()
        self.auth = auth
        
        Observable.combineLatest(password, passwordConfirm)
            .subscribe(onNext: {
                let resultPw = Validation.shared.validate(password: $0)
                self.newPasswordTextFieldResult.accept(resultPw)
                self._newPassword = $0
                let result = Validation.shared.validate(password: $0, confirmPassword: $1)
                self.newPasswordTextFieldResultConfirm.accept(result)
            })
            .disposed(by: bag)
        
        Observable
            .combineLatest(newPasswordTextFieldResult, newPasswordTextFieldResultConfirm) {
                $0.isValid && $1.isValid
            }
            .distinctUntilChanged()
            .bind(to: passwordEnable)
            .disposed(by: bag)
        
        nextButtonTap
            .subscribe(onNext: {
                self.change()
            })
            .disposed(by: bag)
    }
    
    func change() {
        Repository.shared.main
            .changePassword(params: ["ci": auth.ciAES, "pwd": _newPassword.aesEncrypted!])
            .subscribe(onNext: { success in
                self.nextAction.accept(success)
            }, onError: {
                $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
//                guard let custom = $0 as? CustomError else { return }
//                let result = TextFieldResult.failed(message: custom.errorMessage)
//                self.invalidResult = result
//                self.focusOnCurrentField.on(.next(true))
//                self.currentTextFieldResult.accept(result)
            })
            .disposed(by: bag)
    }   
}
