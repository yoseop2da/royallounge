//
//  Intro1ViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/25.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class Intro1ViewController: ClearNaviBaseViewController {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bottomLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.layer.cornerRadius = 6.0
        
        if UIScreen.underSE {
            bottomLabel.font = UIFont.spoqaHanSansRegular(ofSize: 14)
        }
        
        let barButton = RLBarButtonItem(barType: .closeWhite)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItem = barButton
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.splash.intro2ViewController()
                self.push(vc)
            })
            .disposed(by: bag)
    }
}
