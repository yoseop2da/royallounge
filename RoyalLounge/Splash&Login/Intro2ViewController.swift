//
//  Intro2ViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/10/26.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit

class Intro2ViewController: ClearNaviBaseViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.layer.cornerRadius = 6.0
        
        let backButton = RLBarButtonItem(barType: .back)
        backButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = backButton
        
        let barButton = RLBarButtonItem(barType: .close)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        self.navigationItem.rightBarButtonItem = barButton
        
        nextButton.rx.tap
            .rxTouchAnimation(button: nextButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.splash.intro3ViewController()
                self.push(vc)
            })
            .disposed(by: bag)
    }
}
