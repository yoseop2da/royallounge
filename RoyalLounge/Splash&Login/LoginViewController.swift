//
//  LoginViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class LoginViewController: ClearNaviBaseViewController {
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!

    @IBOutlet weak var idWrapView: UIView!
    @IBOutlet weak var passwordWrapView: UIView!

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var findPWButton: UIButton!
    @IBOutlet weak var custommerCenterButton: UIButton!
    
    let idTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "이메일 주소를 입력해주세요", title: "아이디")
        field.keyboardType = .emailAddress
        field.returnKeyType = .next
        return field
    }()

    let passwordTextField: RLTitleTextField = {
        let field = RLTitleTextField.init(placeholder: "비밀번호를 입력해주세요", title: "비밀번호", isPassword: true)
        field.keyboardType = .default
        field.isSecureTextEntry = true
        field.returnKeyType = .done
        return field
    }()

    var viewModel: LoginViewModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // TESTCODE
//        idTextField.text = "g@g.net"
//        passwordTextField.text = "qwer1234!"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        idTextField.returnKeyCallback = { self.passwordTextField.becomeFirst = true }
        
        titleLabel.text = "로그인"
        loginButton.layer.cornerRadius = 6.0
        
        idWrapView.addSubview(idTextField)
        idTextField.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        passwordWrapView.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { $0.top.leading.trailing.bottom.equalToSuperview() }
        
        mainAsync {
            self.headerView.frame = self.mainTableView.frame
            self.mainTableView.reloadData()
        }
                
        let barButton = RLBarButtonItem.init(barType: .back)
        barButton.rx.tap
            .subscribe(onNext: { _ in
                self.pushBack()
            })
            .disposed(by: bag)
        self.navigationItem.leftBarButtonItem = barButton
        
        viewModel = LoginViewModel.init(
            userName: idTextField.textObserver
                .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance),
            password: passwordTextField.textObserver
                .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance),
            nextButtonTap: loginButton.rx.tap
                .rxTouchAnimation(button: loginButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
                .asObservable()
        )
        
        viewModel.onRLError
            .subscribe(onNext: {
                if $0.resultCD == RLResultCD.NETWORK_ERROR.code {
                    if let msg = $0.msg {
                        alertNETWORK_ERR(msg: msg) { }
                    }
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }else{
                    if let msg = $0.msg {
                        toast(message: msg, seconds: 1.5) { }
                    }
                }
                self.pushBack()
            })
            .disposed(by: bag)
        
        viewModel.nextButtonEnabled
            .bind(to: loginButton.rx.mainButtonEnableState)
            .disposed(by: bag)

//        viewModel.loginSuccess
//            .subscribe(onNext: { joinStep  in
//                Coordinator.shared.moveBy(joinStep: joinStep)
//            })
//            .disposed(by: bag)
        
        viewModel.warningClearObserver
            .subscribe(onNext: { clear in
                guard clear else { return }
                self.warningLabel.text = ""
            })
            .disposed(by: bag)
        
        viewModel.noMemberFailObserver
            .bind(to: warningLabel.rx.setResult)
            .disposed(by: bag)
        
        viewModel.iDPWFailObserver
            .bind(to: warningLabel.rx.setResult)
            .disposed(by: bag)
        
        viewModel.blockUserObserver
            .subscribe(onNext: { errorMsg in
                RLPopup.shared.showStopPopup(leftAction: {
                    RLMailSender.shared.sendEmail(.서비스영구정지, finishCallback: nil)
                }, rightAction: nil)
            })
            .disposed(by: bag)
        
        viewModel.limitAgeMemberObserver
            .subscribe(onNext: { ageTuple in
                RLPopup.shared.showAgeLimitPopup(minAge: ageTuple.min, maxAge: ageTuple.max, leftAction: {
                    
                    RLMailSender.shared.sendEmail(.서비스나이제한, finishCallback: nil)
                }, rightAction: nil)
            })
            .disposed(by: bag)

        viewModel.suspendUserObserver
            .subscribe(onNext: { (suspendDate, suspendDays) in
                RLPopup.shared.showSuspend(day: suspendDays, date: suspendDate.dateFormater(newFormat: "yyyy년 MM월 dd일 까지"), leftAction: {
                    RLMailSender.shared.sendEmail(.서비스이용정지, finishCallback: nil)
                }, rightAction: nil)
            })
            .disposed(by: bag)
        
        findPWButton.rx.tap
            .rxTouchAnimation(button: findPWButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.splash.findAccountViewController()
                self.push(vc)
        })
        .disposed(by: bag)
        
        //        custommerCenterButton.underLine()
        custommerCenterButton.rx.tap
            .rxTouchAnimation(button: custommerCenterButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                RLMailSender.shared.sendEmail(.문의_미가입유저, finishCallback: nil)
            }, onError: { err in
                
            })
            .disposed(by: bag)
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { keyboard in
                UIView.animate(withDuration: keyboard.duration) {
                    self.bottomPadding.constant = keyboard.height
                    self.mainTableView.contentOffset = CGPoint(x: 0, y: keyboard.height == 0 ? keyboard.height : 88)
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
}
