//
//  LoginViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewModel: BaseViewModel {
    
    private let validatedEmail: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    private let validatedPassword: BehaviorRelay<TextFieldResult> = BehaviorRelay.init(value: .empty)
    private let userIdAndPassword: BehaviorRelay<(userId: String, password: String)?> = BehaviorRelay.init(value: nil)
         
    let nextButtonEnabled: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    
    // 계정 체크
    var warningClearObserver = PublishRelay<Bool>()
    // 회원아님
    var noMemberFailObserver = PublishRelay<TextFieldResult>()
    // ID PW 틀림
    var iDPWFailObserver = PublishRelay<TextFieldResult>()
    // 영구정지유저
    var blockUserObserver = PublishRelay<String>()
    // 일시정지유저
    var suspendUserObserver = PublishRelay<(suspendDate: String, suspendDays: Int)>()
    // 나이제한
    var limitAgeMemberObserver = PublishSubject<(min: String, max: String)>()
    
    init(userName: Observable<String>,
         password: Observable<String>,
         nextButtonTap: Observable<Void>) {
        
        super.init()
        // 유저필드 변화 감지
        userName
            .map { Validation.shared.validate(loginEmail: $0) }
            .bind(to: validatedEmail)
            .disposed(by: bag)
        
        password
            .map { Validation.shared.validate(loginPassword: $0) }
            .bind(to: validatedPassword)
            .disposed(by: bag)
        
        Observable.combineLatest(validatedEmail, validatedPassword) { $0.isValid && $1.isValid }
            .distinctUntilChanged()
            .bind(to: nextButtonEnabled)
            .disposed(by: bag)
        
        Observable.combineLatest(userName, password) { $0.count > 0 && $1.count > 0 }
            .bind(to: warningClearObserver)
            .disposed(by: bag)
        
        Observable.combineLatest(userName, password) { (userId: $0, password: $1)}
            .bind(to: userIdAndPassword)
            .disposed(by: bag)
        
        nextButtonTap
            .subscribe(onNext: {
                self.login()
            })
            .disposed(by: bag)
    }
    
    func login() {
        guard let tuple = userIdAndPassword.value else { return }
        Repository.shared.main
            .checkAccount(userId: tuple.userId, password: tuple.password)
            .observeOn(MainScheduler.instance)
        .subscribe(onNext: {
            if let suspendEndDate = $0.result?.suspendEndDate, let suspendDays = $0.result?.suspendDays {
                self.suspendUserObserver.accept((suspendDate: suspendEndDate, suspendDays: suspendDays))
                return
            }

            if let userNo = $0.result?.userNo {
                Repository.shared.main
                    .getToken(userNoAes: userNo, userId: $0.userId, password: $0.password)
                    .observeOn(MainScheduler.instance)
                    .subscribe(onNext: { joinStep in
                        Coordinator.shared.moveByUserStatus()
                    }, onError: {
                        $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
                        alertNETWORK_ERR()
                    })
                    .disposed(by: self.bag)
                return
            }
        }, onError: {
            $0.printLog(position: "\((#file.components(separatedBy: "/")).last ?? "")|\(#line)|\(#function)")
            guard let custom = $0 as? CustomError else { return }
            if custom.resultCD == RLResultCD.E01002_blockMember.code {
                self.blockUserObserver.accept(custom.errorMessage)
            }else if custom.resultCD == RLResultCD.E01004_AgeLimit.code {
                Repository.shared.main
                    .appOption(appOptionType: .age)
                    .subscribe(onNext: {
                        let min = $0.first(where: {$0.optionCD == "MIN_AGE"}).map{$0.optionValue}!
                        let max = $0.first(where: {$0.optionCD == "MAX_AGE"}).map{$0.optionValue}!
                        self.limitAgeMemberObserver.on(.next((min: min, max: max)))
                    })
                    .disposed(by: self.bag)
            }else if custom.resultCD == RLResultCD.E01005_IDPWError.code {
                self.iDPWFailObserver.accept(TextFieldResult.failed(message: custom.errorMessage))
            }else if custom.resultCD == RLResultCD.E01000_NoMember.code {
                self.noMemberFailObserver.accept(TextFieldResult.failed(message: custom.errorMessage))
            }else{
                self.onRLError.on(.next((msg: custom.errorMessage, error: $0, resultCD: custom.resultCD)))
            }
        })
        .disposed(by: bag)
    }
}
