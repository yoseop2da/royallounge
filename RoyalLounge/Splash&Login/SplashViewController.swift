//
//  SplashViewController.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SplashViewController: BaseViewController {

    enum CallbackType {
        case toLogin
        case toLoginPopup(email: String)
        case toJoin
    }
    @IBOutlet weak var bgGradientImageView: UIImageView!
    @IBOutlet weak var logoMImageView: UIImageView!
    @IBOutlet weak var logoSImageView: UIImageView!
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var viewModel: SplashViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 6.0
        
        //        // TESTCODE !!!!!!
//        delay(1.5) {
//            let json = ["resultCd":"B000","resultMsg":"본인인증 완료","name":"박요섭","gender":"M","birthday":"19860106","localYn":"Y","telCo":"01","mbNo":"01089997677","ci":"ickEbutKH/fsADJv65R4QyP4iswUT4uk3BI+ISuB+lLJKcZ9c9vud1+dcZY48AW1IUPr5UAaxExnnUDvXY3ulg=="]
//            if let auth = AuthModel.init(json: json as [String: AnyObject]) {
//                let vc = RLStoryboard.splash.findPasswordViewController(auth: auth)
//                self.push(vc)
//            }
//        }
//        delay(1.5) {
//            Coordinator.shared.toMain()
//            Coordinator.shared.toReject()
//            Coordinator.shared.toReady()
//            RLPopup.shared.showEvent(leftAction: nil, rightAction: nil)
//
//            let json: [String: AnyObject] = ["resultCd":"B000","resultMsg":"본인인증 완료","name":"박요섭","gender":"M","birthday":"19860106","localYn":"Y","telCo":"01","mbNo":"01089997677","ci":"ickEbutKH/fsADJv65R4QyP4iswUT4uk3BI+ISuB+lLJKcZ9c9vud1+dcZY48AW1IUPr5UAaxExnnUDvXY3ulg=="] as [String: AnyObject]
//            if let auth = AuthModel.init(json: json) {
//                let vc = RLStoryboard.splash.findPasswordViewController(auth: auth)
//                self.push(vc)
//
//                    let vc = RLStoryboard.join.accountViewController(
//                        auth: auth,
//                        eventNotiYn: true,
//                        infoPeriod: "YEAR")
//                    self.push(vc)
//            }
//        }
        
        splashAnimation()

        viewModel = SplashViewModel()
        
        viewModel.splashAnimationFinish
            .subscribe(onNext: {
                guard $0 else { return }
                self.showLoginView()
            })
            .disposed(by: bag)
        
        viewModel.onRLError
            .subscribe(onNext: {
                if let msg = $0.msg {
                    toast(message: msg, seconds: 1.5)
                }else if $0.resultCD == RLResultCD.E00000_Error.code {
                    alertERR_E0000Dialog()
                }
                self.showLoginView()
            })
            .disposed(by: bag)
        
        loginButton.rx.tap
            .rxTouchAnimation(button: self.loginButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                let vc = RLStoryboard.splash.loginViewController()
                self.push(vc)
            })
            .disposed(by: bag)
        
        self.signUpButton.rx.tap
            .rxTouchAnimation(button: signUpButton, throttleDuration: RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { _ in
                self.join()
            })
            .disposed(by: bag)
        
        RLNotificationCenter.shared.addObserver(observer: self, notiType: .splash, selector: #selector(notiAction(_:)))
    }
    
    func showLoginView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.bgGradientImageView.alpha = 1.0
            self.logoSImageView.alpha = 1.0
            self.loginView.alpha = 1.0
        }, completion: { _ in
            #if DEBUG
            self.showServerSwitch()
            #else
            #endif
        })
    }
    
    @objc func notiAction(_ noti: Notification) {
        if let type = noti.object as? SplashViewController.CallbackType{
            switch type {
            case .toLogin:
                self.push(RLStoryboard.splash.loginViewController())
            case .toLoginPopup(let email):
                RLPopup.shared.showAlreadyMember(email: email, rightAction: {
                    self.push(RLStoryboard.splash.loginViewController())
                })
            case .toJoin:
                self.join()
            }
        }
    }
    
    func join() {
        let vc = RLStoryboard.splash.intro1ViewController()
        let navi = UINavigationController(rootViewController: vc)
        navi.modalPresentationStyle = .fullScreen
        self.modal(navi, animated: true)
    }
    
    func splashAnimation() {
        logoMImageView.alpha = 0.0
        bgGradientImageView.alpha = 0.0
        logoSImageView.alpha = 0.0
        loginView.alpha = 0.0
        
        let logoShowTime = 0.5
        let logoHideTime = 1.5
        delay(logoShowTime) {
            UIView.animate(withDuration: 0.5) {
                self.logoMImageView.alpha = 1.0
            }
        }
        delay(logoHideTime) {
            UIView.animate(withDuration: 0.5, animations: {
                self.logoMImageView.alpha = 0.0
            }, completion: { _ in
                
            })
        }
        delay(logoShowTime + logoHideTime) {
            self.viewModel.requestMemberStatus()
        }
    }
    
    func showServerSwitch() {
        Repository.shared.main
            .getServerList()
            .subscribe(onNext: { baseUrls in
                //
                let alert = UIAlertController.init(title: "서버 설정", message: "", preferredStyle: .actionSheet/*.alert*/)
                baseUrls.forEach { baseUrl in
                    alert.addAction(UIAlertAction(title: baseUrl.name, style: .default, handler: { _ in
                        print("host : \(baseUrl.host)")
                        Repository.shared.main.updateUrl(url: baseUrl.host)
                    }))
                }
                self.present(alert, animated: true, completion: nil)
            })
            .disposed(by: bag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}
