//
//  SplashViewModel.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/09/04.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SplashViewModel: BaseViewModel {

    let splashAnimationFinish = PublishSubject<Bool>()
    override init() {
        super.init()
    }
    
    func requestMemberStatus() {
        if MainInformation.shared.userNoAes != nil {
            Coordinator.shared.moveByUserStatus(errorCallback: {
                self.splashAnimationFinish.onNext(true)
            })
        }else{
            self.splashAnimationFinish.onNext(true)
        }
    }
}
