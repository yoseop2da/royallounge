//
//  UIStoryBoardExtension.swift
//  RoyalLounge
//
//  Created by yoseop park on 2020/02/17.
//  Copyright © 2020 HSOCIETY. All rights reserved.
//

import UIKit
typealias SplashStoryboardExtension = UIStoryboard
typealias JoinStoryboardExtension = UIStoryboard

typealias MainStoryboardExtension = UIStoryboard
typealias TodayCardStoryboardExtension = UIStoryboard
typealias LiveMatchStoryExtension = UIStoryboard

typealias MyPageStoryboardExtension = UIStoryboard
typealias ThirdStoryboardExtension = UIStoryboard
typealias ChattingMainExtension = UIStoryboard

extension UIStoryboard {
    func instantiateViewController<T: UIViewController>() -> T{
        return self.instantiateViewController(withIdentifier: T.className) as! T
    }
}

extension SplashStoryboardExtension {

    func splashViewController() -> SplashViewController {
        let vc: SplashViewController = self.instantiateViewController()
        return vc
    }
    
    func loginViewController() -> LoginViewController {
        let vc: LoginViewController = self.instantiateViewController()
        return vc
    }
    
    func findAccountViewController() -> FindAccountViewController {
        let vc: FindAccountViewController = self.instantiateViewController()
        return vc
    }
    
    func intro1ViewController() -> Intro1ViewController {
        let vc: Intro1ViewController = self.instantiateViewController()
        return vc
    }
    
    func intro2ViewController() -> Intro2ViewController {
        let vc: Intro2ViewController = self.instantiateViewController()
        return vc
    }
    
    func intro3ViewController() -> Intro3ViewController {
        let vc: Intro3ViewController = self.instantiateViewController()
        return vc
    }

    func authGuideViewController() -> AuthGuideViewController {
        let vc: AuthGuideViewController = self.instantiateViewController()
        return vc
    }
    
    func authViewController() -> AuthViewController {
        let vc: AuthViewController = self.instantiateViewController()
        return vc
    }

    func acceptTermsViewController() -> AcceptTermsViewController {
        let vc: AcceptTermsViewController = self.instantiateViewController()
        return vc
    }
    
    func acceptTermsViewController(auth: AuthModel) -> AcceptTermsViewController {
        let vc: AcceptTermsViewController = self.instantiateViewController()
        vc.auth = auth
        return vc
    }
    
    func findIDViewController(auth: AuthModel) -> FindIDViewController {
        let vc: FindIDViewController = self.instantiateViewController()
        vc.auth = auth
        return vc
    }
    
    func findPasswordViewController(auth: AuthModel) -> FindPasswordViewController {
        let vc: FindPasswordViewController = self.instantiateViewController()
        vc.auth = auth
        return vc
    }
}

extension JoinStoryboardExtension {
    
    // MARK: - 회원가입 1단계
    func accountViewController(auth: AuthModel, eventNotiYn: Bool, infoPeriod: String) -> AccountViewController {
        let vc: AccountViewController = self.instantiateViewController()
        vc.joinInfo = (auth: auth, eventNotiYn: eventNotiYn, infoPeriod: infoPeriod)
        return vc
    }
    
    func specDetailViewController(groupTitle: String, specNo: String, stepSeq: Int, isStep01Edited: Bool, specStatus: SpecModel.SpecStatus, containRejectInSpec: Bool) -> SpecDetailViewController {
        let vc: SpecDetailViewController = self.instantiateViewController()
        vc.info = (title: groupTitle,
                   specNo: specNo,
                   stepSeq: stepSeq,
                   isStep01Edited: isStep01Edited,
                   specStatus: specStatus,
                   containRejectInSpec: containRejectInSpec)
        return vc
    }
    
    // MARK: - 회원가입 2단계
    
    func specWomanViewController(isEditMode: Bool) -> SpecWomanViewController {
        let vc: SpecWomanViewController = self.instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    func specListViewController(isEditMode: Bool, backType: SpecListViewController.BackType) -> SpecListViewController {
        let vc: SpecListViewController = self.instantiateViewController()
        vc.isEditMode = isEditMode
        vc.backType = backType
        return vc
    }
    
    // MARK: - 회원가입 3단계
    func profileTextFieldViewController(viewType: ProfileTextFieldViewController.ViewType, isEditMode: Bool, backType: ProfileTextFieldViewController.BackType = .empty) -> ProfileTextFieldViewController {
        let vc: ProfileTextFieldViewController = instantiateViewController()
        vc.viewType = viewType
        vc.isEditMode = isEditMode
        vc.backType = backType
        return vc
    }

    func tallViewController(isEditMode: Bool) -> TallViewController {
        let vc: TallViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    func profileListViewController(optionType: AppOptionType, isEditMode: Bool) -> ProfileListViewController {
        let vc: ProfileListViewController = instantiateViewController()
        vc.optionType = optionType
        vc.isEditMode = isEditMode
        
        return vc
    }

    func areaViewController(isEditMode: Bool) -> AreaViewController {
        let vc: AreaViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    func profileTagViewController(optionType: AppOptionType, isEditMode: Bool) -> ProfileTagViewController {
        let vc: ProfileTagViewController = instantiateViewController()
        vc.optionType = optionType
        vc.isEditMode = isEditMode
        return vc
    }
    
    // MARK: - 회원가입 4단계
    
    /// 인터뷰페이지로 이동
    /// - Parameters:
    ///   - interViewSeq: 1~3
    ///   - isEditMode: 수정모드
    /// - Returns: 인터뷰페이지
    func interviewViewController(interViewSeq: Int, isEditMode: Bool) -> InterviewViewController {
        let vc: InterviewViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        vc.interViewSeq = interViewSeq
        return vc
    }
    
    func aboutMeViewController(isEditMode: Bool) -> AboutMeViewController {
        let vc: AboutMeViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    // MARK: - 회원가입 5단계
    func photoViewController(isEditMode: Bool, isReject: Bool = false) -> PhotoViewController {
        let vc: PhotoViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        vc.isReject = isReject
        return vc
    }
    
    // MARK: - 심사대기
    func optionalInfoViewController(optionalInfoType: OptionalInfoViewController.OptionalInfoType, isEditMode: Bool) -> OptionalInfoViewController {
        let vc: OptionalInfoViewController = instantiateViewController()
        vc.optionalInfoType = optionalInfoType
        vc.isEditMode = isEditMode
        return vc
    }
    
    func jobCareerViewController(isEditMode: Bool) -> JobCareerViewController {
        let vc: JobCareerViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    func univCoViewController(isEditMode: Bool, univCoType: UnivCoModel.UnivCoType = .univ) -> UnivCoViewController {
        let vc: UnivCoViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        vc.univCoType = univCoType
        return vc
    }
    
    func univSearchViewController(type: UnivCoModel.UnivCoType) -> UnivSearchViewController {
        let vc: UnivSearchViewController = instantiateViewController()
        vc.type = type
        return vc
    }
    
    func readyViewController() -> ReadyViewController {
        let vc: ReadyViewController = instantiateViewController()
        return vc
    }
    
    func joinPathViewController(isEditMode: Bool) -> JoinPathViewController {
        let vc: JoinPathViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    func welcomePopup() -> WelcomePopup {
        let vc: WelcomePopup = instantiateViewController()
        return vc
    }
    
    func avoidViewController() -> AvoidViewController {
        let vc: AvoidViewController = instantiateViewController()
        return vc
    }
    
    func avoidSearchViewController() -> AvoidSearchViewController {
        let vc: AvoidSearchViewController = instantiateViewController()
        return vc
    }
    
    func recommendViewController(isEditMode: Bool) -> RecommendViewController {
        let vc: RecommendViewController = instantiateViewController()
        vc.isEditMode = isEditMode
        return vc
    }
    
    func recommendNewViewController() -> RecommendNewViewController {
        let vc: RecommendNewViewController = instantiateViewController()
        return vc
    }
    
    func rejectViewController() -> RejectViewController {
        let vc: RejectViewController = instantiateViewController()
        return vc
    }
    
    func rejectListViewController() -> RejectListViewController {
        let vc: RejectListViewController = instantiateViewController()
        return vc
    }
    
    func sleepViewController() -> SleepViewController {
        let vc: SleepViewController = instantiateViewController()
        return vc
    }
    
    func systemNoticeViewController(content: String) -> SystemNoticeViewController {
        let vc: SystemNoticeViewController = instantiateViewController()
        vc.content = content
        return vc
    }
}


extension MainStoryboardExtension {
    // MainStoryboard
    func mainTaBbarViewController() -> MainTabbarViewController {
        let vc: MainTabbarViewController = instantiateViewController()
        return vc
    }

    func addCardMainViewController() -> AddCardMainViewController {
        let vc: AddCardMainViewController = instantiateViewController()
        return vc
    }
    
    func myPageViewController() -> MyPageViewController {
        let vc: MyPageViewController = instantiateViewController()
        return vc
    }

    func storeViewController() -> StoreViewController {
        let vc: StoreViewController = instantiateViewController()
        return vc
    }
    
    func storeHistoryViewController() -> StoreHistoryViewController {
        let vc: StoreHistoryViewController = instantiateViewController()
        return vc
    }
}

extension TodayCardStoryboardExtension {
    
    /// 카드상세보기
    /// - Parameters:
    ///   - matchNo: 상대방 카드 조회시에 넣어주고, 내카드 조회시에는 null로 처리
    ///   - outCallback: 카드에서 나올때 리프레시 처리 및 액션
    /// - Returns: 카드상세페이지
    func cardDetailViewController(matchNo: String?, outCallback: ((CardDetailViewModel.CardRefreshType)->Void)?) -> CardDetailViewController {
        let vc = self.instantiateViewController(withIdentifier: CardDetailViewController.className) as! CardDetailViewController
        vc.matchNo = matchNo
        vc.outCallback = outCallback
        
        return vc
    }
    
    func myCardDetailViewController(matchNo: String) -> MyCardDetailViewController {
        let vc = self.instantiateViewController(withIdentifier: MyCardDetailViewController.className) as! MyCardDetailViewController
        vc.matchNo = matchNo
        return vc
    }

    func oKGuideViewController() -> OKGuideViewController {
        let vc = self.instantiateViewController(withIdentifier: OKGuideViewController.className) as! OKGuideViewController
        return vc
    }
    
    func reportViewController(cardUserNo: String, cardNickname: String) -> ReportViewController {
        let vc: ReportViewController = instantiateViewController()
        vc.info = (cardUserNo: cardUserNo, userNickname: cardNickname)
        return vc
    }
}

extension LiveMatchStoryExtension {
    func liveMatchHistoryViewController() -> LiveMatchHistoryViewController {
        let vc: LiveMatchHistoryViewController = instantiateViewController()
        return vc
    }
}

extension MyPageStoryboardExtension {

    func improvingMainViewController() -> ImprovingMainViewController {
        return self.instantiateViewController(withIdentifier: ImprovingMainViewController.className) as! ImprovingMainViewController
    }
    
    func improvingSubViewController(type: ImprovingSubViewController.ImproveType) -> ImprovingSubViewController {
        let vc = self.instantiateViewController(withIdentifier: ImprovingSubViewController.className) as! ImprovingSubViewController
        vc.improveType = type
        return vc
    }
    
    func customerCenterViewController() -> CustomerCenterViewController {
        let vc: CustomerCenterViewController = instantiateViewController()
        return vc
    }
    
    func companyInfoViewController() -> CompanyInfoViewController {
        let vc: CompanyInfoViewController = instantiateViewController()
        return vc
    }
    
    func settingsViewController() -> SettingsViewController {
        let vc: SettingsViewController = instantiateViewController()
        return vc
    }
    
    func settingsNotiViewController() -> SettingsNotiViewController {
        let vc: SettingsNotiViewController = instantiateViewController()
        return vc
    }
    
    func settingsPasswordViewController() -> SettingsPasswordViewController {
        let vc: SettingsPasswordViewController = instantiateViewController()
        return vc
    }
    
    func notificationListViewController() -> NotificationListViewController {
        let vc: NotificationListViewController = instantiateViewController()
        return vc
    }
    
    func noticeViewController() -> NoticeViewController {
        let vc: NoticeViewController = instantiateViewController()
        return vc
    }
    
    func eventViewController() -> EventViewController {
        let vc: EventViewController = instantiateViewController()
        return vc
    }
    
    func eventDetailViewController(eventNo: String) -> EventDetailViewController {
        let vc: EventDetailViewController = instantiateViewController()
        vc.eventNo = eventNo
        return vc
    }
    
    func settingsSleepViewController() -> SettingsSleepViewController {
        let vc: SettingsSleepViewController = instantiateViewController()
        return vc
    }
    
    func settingsOutViewController() -> SettingsOutViewController {
        let vc: SettingsOutViewController = instantiateViewController()
        return vc
    }
}
